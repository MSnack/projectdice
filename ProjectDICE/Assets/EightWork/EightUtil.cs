﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.Core;
using UnityEngine.Events;

namespace EightWork
{
    public static class SystemIndex
    {
        public const int InvalidInt = -1;
        public const float InvalidFloat = -1.0f;
        public const double InvalidDouble = -1.0f;
    }

    public enum EightSystemType
    {
        MsgMgr,
        SceneMgr,

    }

    public class EightUtil
    {
        public static bool IsLoadCompleteDevice => _device == null ? false : _device.IsLoadComplate;
        public static event UnityAction LoadCompleteDevice
        {
            add
            {
                if (_device == null)
                    return;

                _device.LoadComplate += value;
            }

            remove
            {
                _device.LoadComplate -= value;
            }
        }

        private static EightDevice _device = null;
        public static T GetCore<T>() where T : EightCore
        {
            if (_device == null)
                return null;

            return _device.GetCore<T>();
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void OnInitializedEightCore()
        {
            var setting = Resources.Load<EightSetting>("Core/CoreSetting/EightSetting");
            if (setting == null)
                Debug.Log("Err");
            else
                Debug.Log("Success");

            if (setting.UseSetting == false)
                return;

            GameObject deviceObject = new GameObject("EightDevice");
            _device = deviceObject.AddComponent<EightDevice>();
            GameObject.DontDestroyOnLoad(deviceObject);

            _device.CreateCoreInitialize(setting);
        }
    }
}