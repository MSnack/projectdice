﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using EightWork;

[CustomPropertyDrawer(typeof(ReadOnlyAttribute), true)]
public class ReadOnlyDrawer : PropertyDrawer
{

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        bool disabled = true;
        switch (((ReadOnlyAttribute)attribute).runtimeOnly)
        {
            case ReadOnlyType.FULLY_DISABLED:
                disabled = true;
                break;
            case ReadOnlyType.DISABLE_RUNTIME:
                disabled = Application.isPlaying;
                break;
            case ReadOnlyType.DISABLE_EDITOR:
                disabled = !Application.isPlaying;
                break;
        }

        using (var scope = new EditorGUI.DisabledGroupScope(disabled))
        {
            EditorGUI.PropertyField(position, property, label, true);
        }
    }

}