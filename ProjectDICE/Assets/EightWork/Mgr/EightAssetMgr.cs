﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.Events;

namespace EightWork
{
    public class EightAssetMgr : Core.EightCore
    {
        [SerializeField]
        private AssetLabelReference _baseLable = null;

        [SerializeField, ReadOnly]
        private long _downloadSize = -1;
        public long DownloadSize => _downloadSize;

        public event UnityAction<float> DownLoadEvent = null;
        public event UnityAction OnDownloadComplete = null;

        [SerializeField, ReadOnly]
        private bool _isDownloading = false;
        public bool IsDownloading => _isDownloading;

        [SerializeField, ReadOnly]
        private bool _isDownloadComplete = false;
        public bool IsDownloadComplete => _isDownloadComplete;

        public override void InitializedCore()
        {
            StartCoroutine(DownloadCompleteChecker());
        }

        private IEnumerator DownloadCompleteChecker()
        {
            while (true)
            {
                yield return new WaitUntil(() => IsDownloadComplete == true);

                OnDownloadComplete?.Invoke();
                OnDownloadComplete = null;
            }
        }

        public void AskDownload(UnityAction<long> downloadSizeEvent = null)
        {
            Addressables.LoadResourceLocationsAsync(_baseLable.labelString).Completed += (handle) =>
            {
                Debug.Log(handle.Result);
                Addressables.Release(handle);
            };
            Addressables.GetDownloadSizeAsync(_baseLable.labelString).Completed += (handle) =>
            {
                _downloadSize = handle.Result;
                if (_downloadSize <= 0)
                    _isDownloadComplete = true;

                downloadSizeEvent?.Invoke(_downloadSize);
                Addressables.Release(handle);
            };
        }

        public void DownloadAssets(UnityAction<float> downloadEvent = null, UnityAction downloadComplete = null)
        {
            if (_isDownloading == true)
                return;

            DownLoadEvent += downloadEvent;
            OnDownloadComplete += downloadComplete;

            var handle = Addressables.DownloadDependenciesAsync(_baseLable.labelString);
            StartCoroutine(DownloadRoutine(handle));
            _isDownloading = true;
        }

        private IEnumerator DownloadRoutine(AsyncOperationHandle handle)
        {
            while (handle.IsDone == false)
            {
                DownLoadEvent?.Invoke(handle.PercentComplete);
                yield return null;
            }
            Addressables.Release(handle);

            DownLoadEvent = null;
            _isDownloading = false;
            _isDownloadComplete = true;
        }
    }
}
