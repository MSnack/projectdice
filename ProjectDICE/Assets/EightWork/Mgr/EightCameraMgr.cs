﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.Core;

namespace EightWork
{
    public class EightCameraMgr : EightCore
    {
        [SerializeField]
        private Camera _eightCamera = null;
        public Camera EightCamera => _eightCamera;

        [SerializeField]
        private CameraEffect _effect = null;
        public CameraEffect Effect => _effect;

        public override void InitializedCore()
        {

        }

        protected override void RegisterMsg()
        {
            base.RegisterMsg();
        }

        protected override void UnRegisterMsg()
        {
            base.UnRegisterMsg();
        }
    }
}