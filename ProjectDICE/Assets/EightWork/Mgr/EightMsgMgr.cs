﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.Core;
using System.Linq.Expressions;

namespace EightWork
{
    public struct EightMsgContent
    {
        public int Index { get; private set; }
        public int Value { get; private set; }
        public string Text { get; private set; }

        public EightMsgContent(int index = SystemIndex.InvalidInt, string text = null, int value = 0)
        {
            Index = index;
            Text = text;
            Value = value;
        }

        public EightMsgContent(int index, int value)
        {
            Index = index;
            Text = null;
            Value = value;

        }

        public EightMsgContent(string text = null, int index = SystemIndex.InvalidInt, int value = 0)
        {
            Index = index;
            Text = text;
            Value = value;
        }

        private static EightMsgContent _invalidEightMsgContent = new EightMsgContent();
        public static EightMsgContent InvalidEightMsgContent
        {
            get { return _invalidEightMsgContent; }
        }

        public static bool operator ==(EightMsgContent c1, EightMsgContent c2)
        {
            return c1.Equals(c2);
        }

        public static bool operator !=(EightMsgContent c1, EightMsgContent c2)
        {
            return !c1.Equals(c2);
        }

        public static bool IsEmpty(EightMsgContent EightMsgContent)
        {
            if (EightMsgContent.Index == SystemIndex.InvalidInt && EightMsgContent.Text == null)
                return true;

            return false;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (GetType() != obj.GetType())
                return false;

            EightMsgContent EightMsgContent = (EightMsgContent)obj;

            if (Index == EightMsgContent.Index)
                return false;

            return Text == EightMsgContent.Text;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public delegate void EightMsgHandler(EightMsgContent eightMsgContent);

    public struct EightMsg
    {
        public EIGHT_MSG EightMSG { get; private set; }
        public EightMsgHandler EightMsgHandler { get; private set; }

        public EightMsg(EIGHT_MSG eightMsg, EightMsgHandler eightMsgHandler)
        {
            EightMSG = eightMsg;
            EightMsgHandler = eightMsgHandler;
        }
    }

    public enum EIGHT_MSG
    {
        CHANGE_SCENE_START,
        CHANGE_SCENE_END,
        ON_CHANGE_SCENE,

        ON_FADE_PAUSE,

        SERVER_ERROR_CODE,
    }

    public class EightMsgMgr : EightCore
    {
        private List<IEightMsgReceiver> _eightMsgReceivers = new List<IEightMsgReceiver>();
        private Dictionary<EIGHT_MSG, EightMsgHandler> _eightMsgHandlerMap = new Dictionary<EIGHT_MSG, EightMsgHandler>();

        private Dictionary<IEightMsgReceiver, List<EightMsg>> _receiveEventList = new Dictionary<IEightMsgReceiver, List<EightMsg>>();

        public override void InitializedCore()
        {
            for (int msgIndex = 0; msgIndex < System.Enum.GetValues(typeof(EIGHT_MSG)).Length; ++msgIndex)
                _eightMsgHandlerMap.Add((EIGHT_MSG)msgIndex, null);
        }

        public void RemoveMsgReceiver()
        {
            for (int receiverIndex = _eightMsgReceivers.Count - 1; receiverIndex >= 0; --receiverIndex)
            {
                if (_eightMsgReceivers[receiverIndex] == null)
                    continue;

                _eightMsgReceivers[receiverIndex].ClearMsg();
            }

            _eightMsgReceivers.Clear();
        }

        public void RegisterEvent(IEightMsgReceiver msgReceiver, EIGHT_MSG msg, EightMsgHandler eightMsgHandler)
        {
            if (_receiveEventList.ContainsKey(msgReceiver) == false)
                _receiveEventList.Add(msgReceiver, new List<EightMsg>());

            _receiveEventList[msgReceiver].Add(new EightMsg(msg, eightMsgHandler));
            _eightMsgHandlerMap[msg] += eightMsgHandler;
        }

        public void RemoveEvent(IEightMsgReceiver msgReceiver)
        {
            if (_receiveEventList.ContainsKey(msgReceiver) == false)
                return;

            List<EightMsg> EightMsgs = _receiveEventList[msgReceiver];
            for (int msgIndex = 0; msgIndex < EightMsgs.Count; ++msgIndex)
                RemoveEvent(EightMsgs[msgIndex]);
        }

        public void RegisterEvent(EIGHT_MSG msg, EightMsgHandler EightMsgHandler)
        {
            _eightMsgHandlerMap[msg] += EightMsgHandler;
        }

        public void RemoveEvent(EightMsg EightMsg)
        {
            _eightMsgHandlerMap[EightMsg.EightMSG] -= EightMsg.EightMsgHandler;
        }

        public void RemoveEvent(EIGHT_MSG msg, EightMsgHandler EightMsgHandler)
        {
            _eightMsgHandlerMap[msg] -= EightMsgHandler;
        }

        public void RegisterMsgReceiver(IEightMsgReceiver msgReceiver)
        {
            _eightMsgReceivers.Add(msgReceiver);
        }

        public void UnRegisterMsgReceiver(IEightMsgReceiver msgReceiver)
        {
            _eightMsgReceivers.Remove(msgReceiver);
        }

        public void OnEightMsg(EIGHT_MSG msg, EightMsgContent EightMsgContent)
        {
            if (_eightMsgHandlerMap[msg] != null)
                _eightMsgHandlerMap[msg].Invoke(EightMsgContent);
        }

        public void OnEightMsg(EIGHT_MSG msg)
        {
            if (_eightMsgHandlerMap[msg] != null)
                _eightMsgHandlerMap[msg].Invoke(EightMsgContent.InvalidEightMsgContent);
        }
    }
}