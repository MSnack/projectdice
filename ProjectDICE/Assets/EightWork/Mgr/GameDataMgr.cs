﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EightWork.Core;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace EightWork
{
    public class GameDataMgr : EightCore
    {
        
        public enum DataId
        {
            CardUpgradeData,
            BoxLocalData,
            DailyShopData,
            QuestLocalData,
            QuestSlotCountData,
            TierLocalData,
            LevelLocalData,
            BoxOpenTimeData,
            MatchingTipData,
            LoadingTipData,
        }

        private bool _isCsvLoaded = false;
        private bool _isPolicyLoaded = false;
        private bool _isConnectorLoaded = false;
        public bool IsLoaded => _isCsvLoaded && _isPolicyLoaded && _isConnectorLoaded;

        private static bool _isLoadFailed = false;
        public static bool IsLoadFailed
        {
            get => _isLoadFailed;
            set => _isLoadFailed = value;
        }

        [SerializeField]
        private AssetLabelReference _label = null;

        [SerializeField]
        private AssetLabelReference _policyLabel = null;

        private Dictionary<DataId, Dictionary<int, List<GameIndexIDData>>> _data =
            new Dictionary<DataId, Dictionary<int, List<GameIndexIDData>>>();

        private List<PolicyData> _policyData = new List<PolicyData>();

        [Header("Server Connector")]
        [SerializeField]
        private GameObject _connectors = null;
        [SerializeField, ReadOnly()]
        private List<ServerConnectorBase> _serverConnectors;
        
        public override void InitializedCore()
        {
        }

        public void OnLoadData()
        {
            LoadData();
            LoadPolicyData();
            StartCoroutine(ConnectorInit());
        }

        private IEnumerator ConnectorInit()
        {
            _serverConnectors = _connectors.GetComponentsInChildren<ServerConnectorBase>().ToList();
            
            foreach (var connector in _serverConnectors)
            {
                yield return new WaitUntil(() => connector.IsLoaded);
            }

            _isConnectorLoaded = true;
        }

        private void LoadData()
        {
            var loadHandle = Addressables.LoadAssetsAsync<TextAsset>(_label.labelString, null);
            loadHandle.Completed += (result) =>
            {
                var csvData = result.Result;
                if (csvData == null)
                {
                    Debug.Log("Fail Load CSV Data");
                    _isLoadFailed = true;
                    return;
                }

                foreach (var csvText in csvData)
                {
                    var str = csvText.text;
                    var csvName = csvText.name;

                    DataId key;
                    try
                    {
                        key = (DataId) Enum.Parse(typeof(DataId), csvName);
                    }
                    catch (ArgumentException e)
                    {
                        //퀘스트 데이터 예외처리
                        if (csvName.Equals("FixedQuestLocalData"))
                        {
                            key = DataId.QuestLocalData;
                            csvName = "QuestLocalData";
                        }
                        else continue;
                    }

                    if(Enum.IsDefined(typeof(DataId), key) == false) continue;
                    Type type = Type.GetType(csvName);
                    Dictionary<int, List<GameIndexIDData>> dataDic;
                    if (_data.ContainsKey(key) == false)
                    {
                        dataDic = new Dictionary<int, List<GameIndexIDData>>();
                        _data.Add(key, dataDic);
                    }
                    else
                    {
                        dataDic = _data[key];
                    }
                    CSVUtil.LoadCSVData(dataDic, str, type);
                }

                _isCsvLoaded = true;
            };

        }
        
        private void LoadPolicyData()
        {
            var loadHandle = Addressables.LoadAssetsAsync<TextAsset>(_policyLabel.labelString, null);
            loadHandle.Completed += (result) =>
            {
                var policyRawList = result.Result;
                if (policyRawList == null)
                {
                    Debug.Log("Fail Load Policy Data");
                    return;
                }

                foreach (var policyRaw in policyRawList)
                {
                    var str = policyRaw.text;
                    var name = policyRaw.name;

                    var data = new PolicyData
                    {
                        Name = name,
                        Data = str,
                    };
                    _policyData.Add(data);
                }

                _isPolicyLoaded = true;
            };

        }

        public Dictionary<int, List<GameIndexIDData>> GetCsvData(DataId id)
        {
            if(!_isCsvLoaded) return null;
            return _data.ContainsKey(id) == false ? null : _data[id];
        }

        public List<PolicyData> GetAllPolicyData()
        {
            return _policyData;
        }

        public PolicyData GetPolicyData(string name)
        {
            return _policyData.Find((x) => x.Name == name);
        }
    }

    public class PolicyData
    {
        public string Name;
        public string Data;
    }
}

