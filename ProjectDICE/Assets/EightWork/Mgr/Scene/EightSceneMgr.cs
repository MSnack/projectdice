﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.Core;
using UnityEngine.SceneManagement;

namespace EightWork
{
    public enum SCENE_TYPE
    {
        NULL = -1,
        LOBBY,
        BATTLE,
        NEW_LOBBY,
    }

    public class EightSceneMgr : EightCore
    {
        [SerializeField]
        private SceneSetting _sceneSetting = null;

        private Dictionary<SCENE_TYPE, string> _gameSceneIDToName = new Dictionary<SCENE_TYPE, string>();

        [SerializeField, ReadOnly]
        private SCENE_TYPE _currentScene = SCENE_TYPE.NULL;
        public SCENE_TYPE CurrentState
        {

            get { return _currentScene; }
            private set
            {
                _currentScene = value;
                OnEightMsg(EIGHT_MSG.ON_CHANGE_SCENE, new EightMsgContent((int)value));
            }
        }

        [SerializeField, ReadOnly]
        private SCENE_TYPE _reserveNextScene = SCENE_TYPE.NULL;

        [SerializeField, ReadOnly]
        private bool _loadSceneActivation = true;
        public bool LoadSceneActivation
        {
            get { return _loadSceneActivation; }
            set
            {
                _loadSceneActivation = value;
                if (_async != null)
                    _async.allowSceneActivation = _loadSceneActivation;
            }
        }

        private Coroutine _changeSceneRoutine = null;
        private bool _isChangeStageNow = false;

        private AsyncOperation _async = null;
        private float _loadingPercentage = 0;
        public float LoadingPercentage => _loadingPercentage;

        public override void InitializedCore()
        {
            _gameSceneIDToName = _sceneSetting.SettingData;
            InitScene(SceneManager.GetActiveScene().name);
        }

        private void InitScene(string sceneName)
        {
            foreach (var scene in _gameSceneIDToName)
            {
                if (scene.Value == sceneName)
                {
                    CurrentState = scene.Key;
                    return;
                }
            }

            CurrentState = SCENE_TYPE.NULL;
        }

        private void EndChangeScene(string sceneName)
        {
            _changeSceneRoutine = null;
            _isChangeStageNow = false;

            CurrentState = _reserveNextScene;
            _reserveNextScene = SCENE_TYPE.NULL;

            _async = null;
            
            OnEightMsg(EIGHT_MSG.CHANGE_SCENE_END);
        }

        public void ChangeScene(SCENE_TYPE gameScnene)
        {
            if (_isChangeStageNow == true)
                return;

            if (_gameSceneIDToName.ContainsKey(gameScnene) == false)
            {
                Debug.LogError("Change Scene Error : null");
                return;
            }

            _isChangeStageNow = true;

            if (_changeSceneRoutine != null)
            {
                StopCoroutine(_changeSceneRoutine);
                _changeSceneRoutine = null;
            }

            _reserveNextScene = gameScnene;

            OnEightMsg(EIGHT_MSG.CHANGE_SCENE_START);

            _changeSceneRoutine = StartCoroutine(ChangeSceneRoutine(_gameSceneIDToName[_reserveNextScene]));
        }

        private IEnumerator ChangeSceneRoutine(string strSceneName)
        {
            _async = SceneManager.LoadSceneAsync(strSceneName);
            _async.allowSceneActivation = LoadSceneActivation;
            _loadingPercentage = 0;

            while (!_async.isDone)
            {
                _loadingPercentage = _async.progress;
                yield return null;
            }

            _loadingPercentage = 100;

            EndChangeScene(strSceneName);
        }
    }
}