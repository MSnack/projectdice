﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.FSM;

namespace EightWork
{
    public abstract class EightSceneStateBase : EightFSMStateBase<SCENE_TYPE, EightSceneStateMgr> { }

    public class EightSceneStateMgr : EightFSMSystem<SCENE_TYPE, EightSceneStateBase>
    {
        protected override void RegisterMsg()
        {
            base.RegisterMsg();

            this.RegisterEvent(EIGHT_MSG.ON_CHANGE_SCENE, OnChangeScene);
        }

        private void OnChangeScene(EightMsgContent eightMsgContent)
        {
            SCENE_TYPE type = (SCENE_TYPE)eightMsgContent.Index;
            if (type == SCENE_TYPE.NULL)
                return;

            ChangeState(type);
        }

        protected override void UnRegisterMsg()
        {
            base.UnRegisterMsg();

            this.RemoveEvent(EIGHT_MSG.ON_CHANGE_SCENE, OnChangeScene);
        }
    }
}