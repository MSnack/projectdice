﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;


public class EightNullScene : EightSceneStateBase
{
    protected override void Initialize()
    {
        State = SCENE_TYPE.NULL;
    }

    public override void StartState()
    {
        //EightUtil.GetCore<EightCameraMgr>().EightCamera.orthographic = false;
    }

    public override void EndState()
    {

    }
}
