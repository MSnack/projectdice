﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class NewLobbySceneState : EightSceneStateBase
{
    protected override void Initialize()
    {
        State = SCENE_TYPE.NEW_LOBBY;
    }

    public override void StartState()
    {
        
    }

    public override void EndState()
    {
        
    }
}
