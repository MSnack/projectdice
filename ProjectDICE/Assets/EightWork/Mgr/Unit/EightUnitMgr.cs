﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.Core;

namespace EightWork
{
    public class EightUnitMgr : EightUnitFactory
    {
        public override void InitializedCore()
        {

        }

        protected override void RegisterMsg()
        {
            base.RegisterMsg();

            this.RegisterEvent(EIGHT_MSG.CHANGE_SCENE_START, OnChangeSceneStart);
        }

        protected virtual void OnChangeSceneStart(EightMsgContent eightMsgContent)
        {
            ReturnAllUnit();
        }

        protected override void UnRegisterMsg()
        {
            base.UnRegisterMsg();

            this.RemoveEvent(EIGHT_MSG.CHANGE_SCENE_START, OnChangeSceneStart);
        }

        public T FindUnit<T>(EightUnit unit) where T : EightUnit
        {
            if (unit == null)
                return null;

            if (UnitContainerPool.ContainsKey(unit.UnitType) == false)
                return null;

            foreach (var find in UnitContainerPool[unit.UnitType])
            {
                if (find == unit)
                {
                    if (find.GetType() == unit.GetType())
                        return find as T;
                    else
                    {
                        Debug.Log("Dont Same Type");
                        return null;
                    }
                }
            }

            return null;
        }

        public T[] FindUnitList<T>(UnitType type) where T : EightUnit
        {
            if (UnitContainerPool.ContainsKey(type) == false)
                UnitContainerPool[type] = new List<EightUnit>();

            if (UnitContainerPool[type].Count <= 0)
                return null;

            T[] unitList = new T[UnitContainerPool[type].Count];
            for(int i = 0; i < UnitContainerPool[type].Count; ++i)
            {
                unitList[i] = (T)UnitContainerPool[type][i];
            }
            return unitList;
        }

        public virtual void ReturnAllUnit()
        {
            foreach (var value in UnitContainerPool.Values)
            {
                for (int i = value.Count - 1; i >= 0; --i)
                {
                    ReturnUnit(value[i]);
                }
                value.Clear();
            }
        }
    }
}