﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightWork
{
    public class EightUnitPool : MonoBehaviour
    {
        private Dictionary<UnitType, Queue<EightUnit>> _reserveUnitPool = new Dictionary<UnitType, Queue<EightUnit>>();

        [SerializeField]
        private GameObject _poolParent = null;
        protected GameObject PoolParent => _poolParent;

        protected EightUnit[] GetUnitList(UnitType type)
        {
            if (_reserveUnitPool.ContainsKey(type) == false ||
                _reserveUnitPool[type].Count <= 0)
                return null;

            return _reserveUnitPool[type].ToArray();
        }

        public EightUnit GetUnit(UnitType type)
        {
            if (_reserveUnitPool.ContainsKey(type) == false ||
                _reserveUnitPool[type].Count <= 0)
                return null;

            EightUnit unit = _reserveUnitPool[type].Dequeue();
            if (unit == null)
                return null;

            unit.transform.SetParent(null);
            unit.gameObject.SetActive(true);

            return unit;
        }

        public void ReturnUnit(EightUnit unit)
        {
            if (unit == null)
                return;

            if (_reserveUnitPool.ContainsKey(unit.UnitType) == false)
                _reserveUnitPool[unit.UnitType] = new Queue<EightUnit>();

            unit.transform.position = Vector3.zero;
            unit.transform.rotation = Quaternion.identity;
            unit.transform.SetParent(PoolParent.transform);
            unit.gameObject.SetActive(false);

            unit.Release();

            _reserveUnitPool[unit.UnitType].Enqueue(unit);
        }
    }
}