﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.Core;

namespace EightWork
{
    public abstract class EightUnitFactory : EightCore
    {
        [SerializeField]
        protected EightUnitPool UnitPool = null;

        protected Dictionary<UnitType, List<EightUnit>> UnitContainerPool = new Dictionary<UnitType, List<EightUnit>>();

        protected TUnit GetUnit<TUnit>(UnitType type)
            where TUnit : EightUnit
        {
            var unit = UnitPool.GetUnit(type) as TUnit;
            if (unit == null)
            {
                GameObject unitObject = new GameObject();
                unit = unitObject.AddComponent<TUnit>();
                unit.OnInitialize();
            }

            if (unit.UnitType != type)
                Debug.LogError("Not Same Type");

            if (UnitContainerPool.ContainsKey(type) == false)
                UnitContainerPool[type] = new List<EightUnit>();

            UnitContainerPool[unit.UnitType].Add(unit);

            return unit;
        }

        protected void RegisterUnit(EightUnit unit)
        {
            if (UnitContainerPool.ContainsKey(unit.UnitType) == false)
                UnitContainerPool[unit.UnitType] = new List<EightUnit>();

            UnitContainerPool[unit.UnitType].Add(unit);
        }

        public virtual void ReturnUnit(EightUnit unit)
        {
            if (unit == null)
            {
                UnitContainerPool[unit.UnitType].Remove(unit);
                return;
            }

            UnitContainerPool[unit.UnitType].Remove(unit);
            UnitPool?.ReturnUnit(unit);
        }
    }
}