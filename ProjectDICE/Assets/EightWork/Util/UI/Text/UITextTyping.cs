﻿namespace EightWork.UI
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.UI;

    public class UITextTyping : MonoBehaviour
    {
        [Tooltip("글자 속도")]
        [SerializeField] private float speed = 0.05f;
        [SerializeField] private bool isSkipAllArea = true;
        [SerializeField] private bool isPlayOnStart = false;

        private Text uiText;
        private string originText = string.Empty;
        private Coroutine typingCoroutine = null;
        private int textCount = 0;
        private bool isTyping = false;

        private void Start()
        {
            uiText = this.GetComponent<Text>();
            if(isPlayOnStart)
                Typing();
        }

        private void Update()
        {
            if(isSkipAllArea)
            {
#if UNITY_EDITOR
                if (Input.GetMouseButtonDown(0) && isTyping)
                    Skip();
#endif

#if UNITY_ANDROID
                if (Input.touchCount > 0 && isTyping)
                    Skip();
#endif
            }
        }

        public void Typing()
        {
            originText = uiText.text;
            uiText.text = string.Empty;
            textCount = 0;
            isTyping = true;
            if (typingCoroutine == null)
                typingCoroutine = StartCoroutine(TypingEnumerator());
        }

        public void Typing(string originText)
        {
            this.originText = originText;
            Typing();
        }

        private IEnumerator TypingEnumerator()
        {
            while (textCount < originText.Length)
            {
                uiText.text += originText[textCount++];
                yield return new WaitForSeconds(speed);
            }

            typingCoroutine = null;
            isTyping = false;
        }

        public void Skip()
        {
            uiText.text = originText;
            StopCoroutine(typingCoroutine);
            typingCoroutine = null;
            isTyping = false;
        }
    }
}