﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxShadow : ModifiedShadow
{

    public override void ModifyVertices(List<UIVertex> verts)
    {
        if (!IsActive())
            return;
        
        var original = verts.Count;
        ApplyShadow(verts, effectColor, 0, original, effectDistance.x, effectDistance.y);
    }
}
