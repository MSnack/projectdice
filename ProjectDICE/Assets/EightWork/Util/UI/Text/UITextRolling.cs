﻿namespace EightWork.UI
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.UI;

    public class UITextRolling : MonoBehaviour
    {
        private Text _text;
        public Text Text
        {
            get
            {
                if (_text == null)
                    _text = GetComponent<Text>();
                return _text;
            }
            private set { }
        }
        private float _currentValue = 50f;
        private float _targetValue = 0f;
        [SerializeField]
        private AnimationCurve _textAc;

        private float _processTime = 0.0f;
        private IEnumerator _changeValueCoroutine = null;
        private float _finishTime = 1f;
        private readonly float ChangeValueCycle = 0.05f;

        private readonly string digitStr = "#,##0";

        private void Awake()
        {
            _text = this.GetComponent<Text>();
        }

        private void OnDisable()
        {
            SettingTextToTargetValue();
        }

        public void Initialize(float currentValue)
        {
            _currentValue = currentValue;
            SettingText(currentValue);
        }

        public void Initialize(float currentValue, float targetValue)
        {
            _currentValue = currentValue;
            _targetValue = targetValue;
            SettingText(currentValue);
        }

        /// <summary>
        /// 숫자 텍스트 연출
        /// </summary>
        /// <param name="targetValue">목표 숫자</param>
        /// <param name="finishTime">연출이 끝나는데 걸리는 시간</param>
        public void RollingText(float targetValue, float finishTime)
        {
            if (_currentValue == targetValue)
                return;

            _finishTime = finishTime;
            _textAc = AnimationCurve.Linear(0f, _currentValue, this._finishTime, targetValue);

            _processTime = 0.0f;
            if (_changeValueCoroutine != null)
            {
                StopCoroutine(_changeValueCoroutine);
                _changeValueCoroutine = null;
            }
            _changeValueCoroutine = RollingEnumerator();
            StartCoroutine(_changeValueCoroutine);
        }

        /// <summary>
        /// 숫자 텍스트 연출, 시작 값과 목표 값을 미리 초기화했을 시 사용
        /// </summary>
        /// <param name="finishTime"></param>
        public void RollingText(float finishTime)
        {
            RollingText(_targetValue, finishTime);
        }

        public void SettingText(float value)
        {
            _currentValue = value;
            _text.text = _currentValue.ToString(digitStr);
        }

        /// <summary>
        /// 목표 값으로 바로 변경
        /// </summary>
        public void SettingTextToTargetValue()
        {
            SettingText(_textAc.Evaluate(_finishTime));
            if (_changeValueCoroutine != null)
                StopCoroutine(_changeValueCoroutine);
        }

        private IEnumerator RollingEnumerator()
        {
            while (_processTime < _finishTime)
            {
                SettingText(_textAc.Evaluate(_processTime));

                yield return new WaitForSeconds(ChangeValueCycle);
                _processTime += ChangeValueCycle;
            }

            _currentValue = _textAc.Evaluate(_finishTime);
            SettingText(_currentValue);
        }
    }
}