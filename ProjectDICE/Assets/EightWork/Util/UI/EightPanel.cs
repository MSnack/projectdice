﻿using UnityEngine;

namespace EightWork.UI
{
    public class EightPanel : MonoBehaviour
    {
        private Rect _lastSafeArea = new Rect(0, 0, 0, 0);
        private RectTransform _panel;

        [SerializeField]
        private bool _ignoreLeft = false;
        [SerializeField]
        private bool _ignoreRight = false;
        [SerializeField]
        private bool _ignoreTop = false;
        [SerializeField]
        private bool _ignoreBottom = false;

        private void Awake()
        {
            _panel = GetComponent<RectTransform>();
            Refresh();
        }

        private void Update()
        {
            Refresh();
        }

        private void Refresh()
        {
            var safeArea = GetSafeArea();

            if (safeArea != _lastSafeArea)
                ApplySafeArea(safeArea);
        }

        private Rect GetSafeArea()
        {
            return Screen.safeArea;
        }

        private void ApplySafeArea(Rect r)
        {
            _lastSafeArea = r;

            var anchorMin = r.position;
            var anchorMax = r.position + r.size;
            if(!_ignoreLeft) anchorMin.x /= Screen.width;
            if(!_ignoreTop) anchorMin.y /= Screen.height;
            if(!_ignoreRight) anchorMax.x /= Screen.width;
            if(!_ignoreBottom) anchorMax.y /= Screen.height;
            _panel.anchorMin = anchorMin;
            _panel.anchorMax = anchorMax;
        }
    }
}