﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EightPath
{
#if UNITY_EDITOR
    public static readonly string UnityProjetPath = Application.dataPath.Replace("Assets", "");
    public static readonly string EditorPath = Application.dataPath + "/Editor";
    public static readonly string EditorResourcesPath = EditorPath + "/Resources";
    public static readonly string EditorBuildSettingFile = EditorResourcesPath + "/Json/BuildSetting.json";

    public static readonly string UnityAssetsPath = Application.dataPath;
    public static readonly string UnityResourcesPath = Application.dataPath + "/Resources";

    public static readonly string VersionDataFolderPath = Application.persistentDataPath + "/Version";
    public static readonly string VersionDataFilePath = VersionDataFolderPath + "/Version.json";
    public static readonly string VersionOfAWSFolder = Application.version;

    public static readonly bool IsDebugBundle = false;
#endif
}
