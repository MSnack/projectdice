﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

namespace EightWork.FSM
{
    public abstract class EightStateBase<TStateEnum> : MonoBehaviour
        where TStateEnum : System.Enum
    {
        [SerializeField, ReadOnly]
        private TStateEnum _state;

        public TStateEnum State
        {
            get { return _state; }
            protected set { _state = value; }
        }

        protected abstract void Initialize();
        public abstract void StartState();
        public abstract void EndState();
    }

    public abstract class EightFSMStateBase<TStateEnum, TInspectorFSMSystem> : EightStateBase<TStateEnum>
        where TStateEnum : System.Enum
        where TInspectorFSMSystem : MonoBehaviour
    {
        [SerializeField]
        private TInspectorFSMSystem _systemMgr = null;

        public TInspectorFSMSystem SystemMgr
        {
            get { return _systemMgr; }
        }

        protected virtual void Awake()
        {
            Initialize();
        }

        protected virtual void Start()
        {

        }

        protected virtual void OnEnable()
        {
            // base.OnEnable();
        }

        protected virtual void OnDisable()
        {
            // base.OnDisable();
        }

        protected virtual void Reset()
        {
            _systemMgr = GetComponent<TInspectorFSMSystem>();

            Initialize();
        }
    }
}
