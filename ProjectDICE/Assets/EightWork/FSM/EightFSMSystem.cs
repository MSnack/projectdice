﻿using System.Collections;
using System.Collections.Generic;
using EightWork.MsgSystem;
using Unity.Collections;
using UnityEngine;

namespace EightWork.FSM
{
    public interface EightFSMInterface<TStateEnum, BaseClass> where BaseClass : MonoBehaviour
        where TStateEnum : System.Enum
    {
        Dictionary<TStateEnum, BaseClass> StatePool { get; }

        TStateEnum StartState { get; }
        TStateEnum CurrState { get; }

        void ChangeState(TStateEnum change);
    }

    public abstract class EightFSMSystem<TStateEnum, TStateBase> : EightReceiver
        where TStateEnum : System.Enum
        where TStateBase : EightStateBase<TStateEnum>
    {
        public Dictionary<TStateEnum, TStateBase> StatePool { get; private set; }

        [SerializeField]
        public TStateEnum _startState;
        public TStateEnum StartState => _startState;

        [SerializeField, ReadOnly]
        private TStateEnum _currState;
        public TStateEnum CurrState => _currState;

        [SerializeField, ReadOnly(ReadOnlyType.DISABLE_RUNTIME)]
        private bool _useStartEvent = true;

        protected virtual void Awake()
        {
            StatePool = new Dictionary<TStateEnum, TStateBase>();
            InitPool();
        }

        protected override void OnLoadCompleteDevice()
        {
            base.OnLoadCompleteDevice();
            
            StartEvent();
        }

        protected virtual void Start()
        {
            if (_useStartEvent == true)
                EightUtil.LoadCompleteDevice += StartEvent;
            else
                StartEvent();
        }

        protected virtual void InitPool()
        {
            var states = GetComponents<TStateBase>();
            foreach (var state in states)
            {
                state.enabled = false;

                if (StatePool.ContainsKey(state.State) == true)
                    continue;

                StatePool[state.State] = state;
            }
        }

        public void ChangeState(TStateEnum change)
        {
            if (StatePool.ContainsKey(change) == false)
            {
                Debug.LogError("Dont have Key in InspectorFSMSystem");
                return;
            }

            if (StatePool.ContainsKey(_currState) == true)
                StatePool[_currState].EndState();

            foreach (var pair in StatePool)
            {
                pair.Value.enabled = false;
            }

            _currState = change;
            StatePool[change].enabled = true;
            StatePool[change].StartState();
        }

        protected virtual void StartEvent()
        {
            ChangeState(StartState);
        }
    }

    public abstract class EightFSMSystem<TStateEnum, TStateBase, TMsgSystem> : 
        EightMsgReceiver<TMsgSystem>, EightFSMInterface<TStateEnum, TStateBase>
        where TStateEnum : System.Enum
        where TMsgSystem : MsgSystem.EightMsgSystem 
        where TStateBase : EightStateBase<TStateEnum>
    {
        public Dictionary<TStateEnum, TStateBase> StatePool { get; private set; }

        [SerializeField]
        public TStateEnum _startState;
        public TStateEnum StartState => _startState;

        [SerializeField, ReadOnly]
        private TStateEnum _currState;
        public TStateEnum CurrState => _currState;

        protected virtual void Awake()
        {
            StatePool = new Dictionary<TStateEnum, TStateBase>();
            InitPool();
        }
        
        protected override void OnLoadCompleteDevice()
        {
            base.OnLoadCompleteDevice();
            
            StartEvent();
        }

        protected virtual void InitPool()
        {
            var states = GetComponents<TStateBase>();
            foreach (var state in states)
            {
                state.enabled = false;

                if (StatePool.ContainsKey(state.State) == true)
                    continue;

                StatePool[state.State] = state;
            }
        }

        public void ChangeState(TStateEnum change)
        {
            if (StatePool.ContainsKey(change) == false)
            {
                Debug.LogError("Dont have Key in InspectorFSMSystem");
                return;
            }

            if(StatePool.ContainsKey(_currState) == true)
                StatePool[_currState].EndState();

            foreach (var pair in StatePool)
            {
                pair.Value.enabled = false;
            }

            _currState = change;
            StatePool[change].enabled = true;
            StatePool[change].StartState();
        }

        protected override void StartEvent()
        {
            base.StartEvent();

            ChangeState(StartState);
        }
    }
}

