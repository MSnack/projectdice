﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightWork.MsgSystem
{
    public class EightMsgReceiver<TMsgSystem> : EightReceiver where TMsgSystem : EightMsgSystem
    {
        [SerializeField]
        private TMsgSystem _msgInterface = null;
        public TMsgSystem MsgInterface { get { return _msgInterface; } }

        protected virtual void Start()
        {
            if (MsgInterface != null)
                MsgInterface.StartEvent += StartEvent;
            else
                EightUtil.LoadCompleteDevice += StartEvent;
        }

        protected virtual void StartEvent() { }
    }
}