﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace EightWork.MsgSystem
{
    public abstract class EightMsgSystem : EightReceiver
    {
        private Coroutine _eventRoutine = null;
        private bool _isStartEvent = false;
        public bool IsStartEvent => _isStartEvent;

        private UnityAction _startAction = null;
        public event UnityAction StartEvent
        {
            add
            {
                if (value == null)
                    return;

                _startAction += value;
                StartEventRoutine();
            }

            remove { _startAction -= value; }
        }
        
        private void StartEventRoutine()
        {
            if (_isStartEvent == true)
                return;

            _eventRoutine = StartCoroutine(CoreCheck());
            _isStartEvent = true;
        }

        private void StopEventRoutine()
        {
            if (_isStartEvent == false)
                return;

            StopCoroutine(_eventRoutine);
            _eventRoutine = null;
            _isStartEvent = false;
        }

        private IEnumerator CoreCheck()
        {
            if (EightUtil.IsLoadCompleteDevice == false)
                yield return new WaitUntil(() => EightUtil.IsLoadCompleteDevice == true);
            else
                yield return null;

            _startAction?.Invoke();
            _startAction = null;

            StopEventRoutine();
        }
        
        protected virtual void Awake()
        {

        }

        protected virtual void Start()
        {

        }

        protected override void OnEnable()
        {
        }
    }

}
