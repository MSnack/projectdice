﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightWork
{
    public enum UnitType
    {
        Default,
        Character,
        Monster,
    }

    public abstract class EightUnit : EightReceiver
    {
        [SerializeField, ReadOnly]
        private UnitType _unitType = UnitType.Default;
        public UnitType UnitType
        {
            get { return _unitType; }
            protected set { _unitType = value; }
        }

        public virtual void OnInitialize()
        {
            _unitType = UnitType.Default;
        }

        public virtual void Release()
        {

        }

        protected virtual void Reset()
        {
            OnInitialize();
        }
    }
}