﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightWork
{
    public abstract class EightReceiver : EightMsgReceiver
    {
        protected virtual void OnLoadCompleteDevice()
        {

        }

        protected virtual void SetupComplete()
        {
            EightUtil.LoadCompleteDevice += OnLoadCompleteDevice;
        }
    }
}