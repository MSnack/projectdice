﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightWork
{
    public interface IEightMsgReceiver
    {
        void ClearMsg();
    }

    public abstract class EightMsgReceiver : MonoBehaviour, IEightMsgReceiver
    {
        private bool _isClearMsg = false;

        private static EightMsgMgr _msgMgr = null;
        private static EightMsgMgr MsgMgr
        {
            get
            {
                if (_msgMgr == null)
                    _msgMgr = EightUtil.GetCore<EightMsgMgr>();

                return _msgMgr;
            }
        }
        
        protected virtual void OnEnable()
        {
            _isClearMsg = false;
            RegisterMsgReceiver();
            RegisterMsg();
        }

        protected virtual void OnDisable()
        {
            if (_isClearMsg == false)
                ClearMsg();
        }

        public void ClearMsg()
        {
            _isClearMsg = true;

            UnRegisterMsg();
            UnRegisterMsgReciver();
        }

        protected void RegisterMsgReceiver()
        {
            if (MsgMgr != null)
                MsgMgr.RegisterMsgReceiver(this);
        }

        protected void UnRegisterMsgReciver()
        {
            if (MsgMgr != null)
                MsgMgr.UnRegisterMsgReceiver(this);
        }

        protected virtual void RegisterMsg() { }
        protected virtual void UnRegisterMsg()
        {
            if (MsgMgr != null)
                MsgMgr.RemoveEvent(this);
        }

        protected void RegisterEvent(EIGHT_MSG msg, EightMsgHandler gameMsgHandler)
        {
            if (MsgMgr != null)
                MsgMgr.RegisterEvent(this, msg, gameMsgHandler);
        }

        protected void RemoveEvent(EIGHT_MSG msg, EightMsgHandler gameMsgHandler)
        {
            if (MsgMgr != null)
                MsgMgr.RemoveEvent(msg, gameMsgHandler);
        }

        protected void OnEightMsg(EIGHT_MSG msg)
        {
            if (MsgMgr != null)
                MsgMgr.OnEightMsg(msg);
        }

        protected void OnEightMsg(EIGHT_MSG msg, EightMsgContent eightMsgContent)
        {
            if (MsgMgr != null)
                MsgMgr.OnEightMsg(msg, eightMsgContent);
        }
    }
}