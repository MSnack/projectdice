﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace EightWork
{
    public enum ReadOnlyType
    {
        FULLY_DISABLED,
        DISABLE_RUNTIME,
        DISABLE_EDITOR,
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    public class ReadOnlyAttribute : PropertyAttribute
    {
        public readonly ReadOnlyType runtimeOnly;

        public ReadOnlyAttribute(ReadOnlyType runtimeOnly = ReadOnlyType.FULLY_DISABLED)
        {
            this.runtimeOnly = runtimeOnly;
        }
    }
}