﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace EightWork.Core
{
    public class EightDevice : MonoBehaviour
    {
        [SerializeField, ReadOnly]
        private List<EightCore> _corePool = new List<EightCore>();
        public T GetCore<T>() where T : EightCore
        {
            foreach (var core in _corePool)
            {
                if (core.GetType() == typeof(T))
                {
                    return core as T;
                }
            }

            return null;
        }

        private bool _isLoadComplate = false;
        public bool IsLoadComplate => _isLoadComplate;

        private Coroutine _coreInit = null;
        private Coroutine _loadChecker = null;

        public event UnityAction LoadComplate = null;

        public void CreateCoreInitialize(EightSetting setting)
        {
            if (setting == null)
                return;

            if (_coreInit != null)
                StopCoroutine(_coreInit);

            _isLoadComplate = false;
            _coreInit = StartCoroutine(CreateCore(setting));
        }

        private IEnumerator CreateCore(EightSetting setting)
        {
            ReleaseDevice();

            foreach (var core in setting.CoreList)
            {
                var copy = Instantiate(core, this.transform);
                _corePool.Add(copy);

                copy.InitializedCore();
                yield return null;
            }

            _isLoadComplate = true;

            if (_loadChecker != null)
                StopCoroutine(_loadChecker);

            _loadChecker = StartCoroutine(LoadComplateChecker());
        }

        private void ReleaseDevice()
        {
            foreach (var core in _corePool)
            {
                Destroy(core);
            }
            _corePool.Clear();
        }

        private IEnumerator LoadComplateChecker()
        {
            while (true)
            {
                if (_isLoadComplate == false)
                    yield return new WaitUntil(() => _isLoadComplate == true);
                else
                    yield return null;

                LoadComplate?.Invoke();
                LoadComplate = null;
            }
        }
    }
}