﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using RotaryHeart.Lib.SerializableDictionary;

namespace EightWork.Core
{
    [CreateAssetMenu(fileName = "New SceneSetting", menuName = "Setting/SceneSetting", order = 2)]
    public class SceneSetting : ScriptableObject
    {
        [System.Serializable]
        public class SceneSettingData : SerializableDictionaryBase<SCENE_TYPE, string> { }

        [SerializeField]
        private SceneSettingData _settingData = null;
        public Dictionary<SCENE_TYPE, string> SettingData => _settingData.Clone();

        public bool IsEmpty => _settingData.Count <= 0;
    }
}