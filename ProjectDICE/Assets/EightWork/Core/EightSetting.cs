﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightWork.Core
{
    [CreateAssetMenu(fileName = "New EightSetting", menuName = "Setting/EightSetting", order = 2)]
    public class EightSetting : ScriptableObject
    {
        [SerializeField]
        private string _corePath = null;

        [SerializeField]
        private bool _useSetting = true;
        public bool UseSetting => _useSetting;

        [SerializeField]
        private List<EightCore> _coreList = null;
        public EightCore[] CoreList => _coreList.ToArray();

        [ContextMenu("OnSettingCoreList")]
        private void OnSettingCoreList()
        {
            _coreList.Clear();
            _coreList.AddRange(Resources.LoadAll<EightCore>(_corePath));
        }
    }
}