﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightWork.Core
{
    public abstract class EightCore : EightReceiver
    {
        public abstract void InitializedCore();
    }
}