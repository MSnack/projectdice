#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("AgrYO+76/GgGhugaUVJK2WRPCuVwn9ZoLvxwDjAQm+tScXzYN9cI+82cirzivPC0Gj1eXzU3ZvkTaPH5x82JysbHzcDdwMbH2onGz4nc2swisCB3UOLFXK4Ci5mrQbGXUfmgeiuoqa+ggy/hL17KzayomShbmYOvv5m9r6r8raq6pOjZ2cXMifvGxt2DL+EvXqSoqKysqZnLmKKZoK+q/Jm4r6r8raO6o+jZ2cXMieDHyoeYHJMEXaanqTuiGIi/h918laRyy78eshQ66427g26mtB/kNffKYeIpvsvFzIna3cjHzcjbzYndzNvE2onI+8zFwMjHysyJxseJ3cHA2onKzNuGmShqr6GCr6isrK6rq5koH7MoGs4moR2JXmIFhYnG2R+WqJklHupmmSutEpkrqgoJqquoq6uoq5mkr6ABddeLnGOMfHCmf8J9C42KuF4IBTw306UN7iLyfb+emmJtpuRnvcB4GJnxRfOtmyXBGia0d8zaVs73zBWH6Q9e7uTWofeZtq+q/LSKrbGZv2nKmt5ek66F/0JzpoincxPasOYcrkXUkCoi+ol6kW0YFjPmo8JWglXTmSuo35mnr6r8tKaoqFatraqrqOzXtuXC+T/oIG3dy6K5KugumiMoYLDbXPSnfNb2MluMqhP8JuT0pFitr7qr/PqYupm4r6r8raO6o+jZ2aGCr6isrK6rqL+3wd3d2dqThobepjSUWoLggbNhV2ccEKdw97V/YpSPmY2vqvytorq06NnZxcyJ6szb3Sm9gnnA7j3foFddwiSH6Q9e7uTW1ugBMVB4Y881jcK4eQoSTbKDarafMOWE0R5EJTJ1Wt4yW9973pnmaJqf85nLmKKZoK+q/K2vuqv8+pi6ofeZK6i4r6r8tImtK6ihmSuorZndwcbbwN3QmL+Zva+q/K2quqTo2Y1LQnge2Xam7EiOY1jE0UROHL6+2cXMifvGxt2J6uiZt76kmZ+ZnZuJ6uiZK6iLmaSvoIMv4S9epKioqMXMieDHyoeYj5mNr6r8raK6tOjZrKmqK6imqZkrqKOrK6ioqU04AKDwDqyg1b7p/7i33XoeIoqS7gp8xuBx3zaavcwI3j1ghKuqqKmoCiuoicjHzYnKzNvdwM/AysjdwMbHidnZxcyJ6szb3cDPwMrI3cDGx4no3NCJyNra3MTM2onIysrM2d3Ix8rMnJuYnZman/O+pJqcmZuZkJuYnZmUj86JI5rDXqQrZndCCoZQ+sPyzYnGz4ndwcyJ3cHMx4nI2dnFwMrIr5mmr6r8tLqoqFatrJmqqKhWmbQm2ijJb7LyoIY7G1Ht4VnJkTe8XIWJyszb3cDPwMrI3cyJ2cbFwMrQF13aMkd7zaZi0OadcQuXUNFWwmGvqvy0p62/rb2CecDuPd+gV13CJNvIyt3AysyJ2t3I3czEzMfd2oeZwM/AysjdwMbHiejc3cHG28Dd0Jje3ofI2dnFzIfKxsSGyNnZxczKyKSvoIMv4S9epKiorKypqiuoqKn1tjhyt+75QqxE99AthEKfC/7l/EXdwM/AysjdzInL0InIx9CJ2cjb3bYsKiyyMJTunlsAMuknhX0YObtx+QMjfHNNVXmgrp4Z3NyI");
        private static int[] order = new int[] { 24,58,20,43,16,34,12,58,9,23,59,42,38,37,21,24,54,18,26,31,57,51,28,25,31,57,35,28,59,32,35,48,57,33,34,53,38,37,43,51,52,59,45,54,52,58,56,59,57,55,52,52,53,56,58,58,59,57,59,59,60 };
        private static int key = 169;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
