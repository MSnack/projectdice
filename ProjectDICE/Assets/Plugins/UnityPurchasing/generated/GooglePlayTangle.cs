#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("59cDADtg1GAzzArTK2SlHOnjaJfvFLbGVvc1V9dQFXUxEKlwbrpZxiE2k+2WsyeTVzPJrTYFmfybKDuK97FzZtBMFcmoZxiK24bHV/l9WJMCgY+AsAKBioICgYGANB/fxvqCTogIDcDCJzans1IFeWdioB+38+PbsAKBorCNhomqBsgGd42BgYGFgIPsmsl/NlKc4yd59VrafvbKRUoXIS+hSca1zGVmnHilTNFgDeXa0S/D8kLTDZqikdZw++c8B/ZrKB3CuP5nLJF3W0lwnWCfPvaSmBO7rsmvp+ZPHRWPpFmYjngpX8SuuMmQDvkm8rI+RHX1t4c7NMua7uGdHOY52YYkk2vvypCa3sAjhSDKvY5xamD6yxRL+xzIoWtGZYKDgYCB");
        private static int[] order = new int[] { 11,7,8,8,7,7,11,12,11,10,12,13,12,13,14 };
        private static int key = 128;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
