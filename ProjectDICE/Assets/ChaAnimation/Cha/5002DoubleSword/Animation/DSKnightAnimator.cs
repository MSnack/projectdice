﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DSKnightAnimator : StateMachineBehaviour {
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) { animator.SetBool("IsFlip", !animator.GetBool("IsFlip")); }
}
