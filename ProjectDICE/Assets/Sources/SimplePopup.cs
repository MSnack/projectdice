﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SimplePopup : MonoBehaviour
{
     [Header("Popup Texts")]

    [TextArea, SerializeField]
    private string _contextText = "";


    [Header("References")]

    [SerializeField]
    private Text _contextTextObject = null;

    private static UnityAction _closeAction;

    // Start is called before the first frame update
    void Start()
    {
        _closeAction += CloseEvent;
    }

    private void OnEnable()
    {
        RefreshPopupData();
    }

    private void RefreshPopupData()
    {
        _contextTextObject.text = _contextText;
    }

    public void SetData(string context)
    {
        
        _contextText = context;
        
        gameObject?.SetActive(true);
    }

    public void CloseEvent()
    {
        gameObject.SetActive(false);
    }

    public static void Close()
    {
        _closeAction?.Invoke();
    }
}
