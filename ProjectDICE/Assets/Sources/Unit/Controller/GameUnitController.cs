﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.FSM;
using EightWork;

public abstract class GameUnitStateBase
{
    public abstract void OnInit(GameUnitController manager);

    public virtual void StartState() { }
    public virtual void Update() { }
    public virtual void EndState() { }

    public virtual void Release() { }
}

public abstract class GameUnitController : EightReceiver
{
    private class DefaultState : GameUnitStateBase
    {
        public override void OnInit(GameUnitController manager)
        {
        }
    }

    private Dictionary<int, GameUnitStateBase> _unitStatePool = new Dictionary<int, GameUnitStateBase>();

    private int _currState = -1;
    private bool _isNoneState = true;

    private GameUnit _unit = null;
    public GameUnit Unit { get => _unit; protected set => _unit = value; }

    private bool _isChanged = false;

    protected virtual void Awake()
    {
        AddState(-1, new DefaultState());
        //OnInitializeState();
    }

    public virtual void OnInitializeState(GameUnit unit)
    {
        _unit = unit;
    }

    public void AddState(int key, GameUnitStateBase state)
    {
        state.OnInit(this);

        _unitStatePool[key] = state;
    }

    public void AddState<TEnum>(TEnum key, GameUnitStateBase state)
        where TEnum : System.Enum
    {
        state.OnInit(this);

        int intKey = System.Convert.ToInt32(key);
        _unitStatePool[intKey] = state;
    }

    public void ChangeState(int state)
    {
        if (_isNoneState == true)
            _isNoneState = false;
        else
            _unitStatePool[_currState].EndState();

        _currState = state;
        _unitStatePool[_currState].StartState();

        _isChanged = true;
    }

    public void ChangeState<TEnum>(TEnum state)
        where TEnum : System.Enum
    {
        ChangeState(System.Convert.ToInt32(state));
    }

    protected virtual void Update()
    {
        if (_isNoneState == false)
        {
            if (_isChanged == true)
            {
                _isChanged = false;
                return;
            }

            _unitStatePool[_currState].Update();
        }
    }

    public virtual void Release()
    {
        foreach (var state in _unitStatePool.Values)
        {
            state.Release();
        }
        _unitStatePool.Clear();
    }
}