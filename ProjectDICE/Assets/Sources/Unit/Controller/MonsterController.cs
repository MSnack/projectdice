﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using EightWork.FSM;

public class MonsterController : GameUnitController
{
    private abstract class MonsterStateBase : GameUnitStateBase
    {
        protected MonsterController Manager = null;

        public override void OnInit(GameUnitController manager)
        {
            Manager = manager as MonsterController;
        }

        public override void Release()
        {
            base.Release();
            Manager = null;
        }
    }

    private class MonsterIdleState : MonsterStateBase
    {
        public override void StartState()
        {
            base.StartState();
        }

        public override void Update()
        {
            base.Update();

            OnMoveCheck();
        }

        private void OnMoveCheck()
        {
            if (Manager.Unit.IsSkillReady)
            {
                Manager.Unit.UnitState = GameUnitState.Skill;
                return;
            }

            if (Manager.Unit.IsDontMove == true)
                return;

            if (Manager.Unit.TileRoadManager == null)
                return;

            if (Manager.Unit.CurrTileIndex != Manager.Unit.TileRoadManager.TileRoadList.Length - 1)
            {
                Manager.Unit.OnMove(Manager.Unit.TileRoadManager.TileRoadList[++Manager.Unit.CurrTileIndex].GetPositionForUnit());
                Manager.Unit.UnitState = GameUnitState.Run;
            }
            else
            {
                Manager.Unit.OnTileRoadEnd();
                Manager.Unit.UnitState = GameUnitState.Die;
            }
        }

        public override void EndState()
        {
            base.EndState();
        }
    }

    private class MonsterRunState : MonsterStateBase
    {
        public override void StartState()
        {
            base.StartState();

            Manager.Unit.EndMoveEvent += EndMoveEvent;
        }

        public override void EndState()
        {
            base.EndState();

            Manager.Unit.EndMoveEvent -= EndMoveEvent;
        }

        private void EndMoveEvent()
        {
            if (Manager.Unit.CurrTileIndex != Manager.Unit.TileRoadManager.TileRoadList.Length - 1)
            {
                Manager.Unit.OnMove(Manager.Unit.TileRoadManager.TileRoadList[++Manager.Unit.CurrTileIndex].GetPositionForUnit());
            }
            else
            {
                Manager.Unit.OnTileRoadEnd();
                Manager.Unit.UnitState = GameUnitState.Die;
            }
        }

        public override void Update()
        {
            base.Update();

            OnMoveCheck();

            //if (Manager.Unit.IsMove == false)
            //    Manager.Unit.OnNormal();
        }

        private void OnMoveCheck()
        {
            if (Manager.Unit.IsSkillReady)
            {
                Manager.Unit.UnitState = GameUnitState.Skill;
                StopMove();
                return;
            }

            if (Manager.Unit.IsDontMove == true)
            {
                StopMove();
                return;
            }
        }

        private void StopMove()
        {
            Manager.Unit.CurrTileIndex--;
            Manager.Unit.StopMove();
            Manager.Unit.OnNormal();
        }
    }

    private class MonsterDieState : MonsterStateBase
    {
        private void OnDieEvent(GameUnit unit)
        {
            EightUtil.GetCore<GameUnitMgr>().ReturnUnit(unit);
        }

        public override void StartState()
        {
            Manager.Unit.StopMove();
            Manager.Unit.OnDieEvent += OnDieEvent;
        }

        public override void EndState()
        {
            Manager.Unit.OnDieEvent -= OnDieEvent;
        }
    }

    private class MonsterSkillState : MonsterStateBase
    {
        private void OnEndAni(GameUnitState state)
        {
            Manager.Unit.UnitState = GameUnitState.Idle;
        }

        public override void StartState()
        {
            base.StartState();

            Manager.Unit.OnEndAniEvent += OnEndAni;
        }

        public override void EndState()
        {
            base.EndState();

            Manager.Unit.OnEndAniEvent -= OnEndAni;
        }
    }

    private class MonsterAttackState : MonsterStateBase
    {
        private void OnDieEvent(GameUnit unit)
        {
            EightUtil.GetCore<GameUnitMgr>().ReturnUnit(unit);
        }

        public override void StartState()
        {
            Manager.Unit.StopMove();
            Manager.Unit.OnDieEvent += OnDieEvent;
        }

        public override void EndState()
        {
            Manager.Unit.OnDieEvent -= OnDieEvent;
        }
    }

    private class MonsterSpawnState : MonsterStateBase
    {
        private void OnEndAni(GameUnitState state)
        {
            Manager.Unit.OnNormal();
        }

        public override void StartState()
        {
            base.StartState();

            Manager.Unit.OnEndAniEvent += OnEndAni;
        }

        public override void EndState()
        {
            base.EndState();

            Manager.Unit.OnEndAniEvent -= OnEndAni;
        }
    }

    public override void OnInitializeState(GameUnit unit)
    {
        base.OnInitializeState(unit);

        AddState(GameUnitState.Idle, new MonsterIdleState());
        AddState(GameUnitState.Run, new MonsterRunState());
        AddState(GameUnitState.Die, new MonsterDieState());
        AddState(GameUnitState.Skill, new MonsterSkillState());
        AddState(GameUnitState.Spawn, new MonsterSpawnState());

        unit.SetDamageEvent += SetDamageEvent;
        unit.OnCreateEvent += CreateEvent;
    }

    private void CreateEvent(GameUnit unit)
    {
        if (Unit.CurrTileIndex != Unit.TileRoadManager.TileRoadList.Length - 1)
        {
            var nextPos = Unit.TileRoadManager.TileRoadList[Unit.CurrTileIndex + 1].GetPositionForUnit();
            Unit.FlipCheck(nextPos);
        }
    }

    private void SetDamageEvent(int damage, bool isCritical)
    {
        EightUtil.GetCore<GameDamageFontMgr>().DrawDamageFont(damage, isCritical, transform.position);
    }

    public override void Release()
    {
        base.Release();

        if (Unit != null)
        {
            Unit.SetDamageEvent -= SetDamageEvent;
            Unit.OnCreateEvent -= CreateEvent;
        }

        Unit = null;
    }

    protected override void Update()
    {
        base.Update();

        if (Unit == null)
            return;

        if (Unit.SpineUnit != null)
        {
            float xDir = 1.0f;
            if (Unit.BattleManager != null &&
                Unit.BattleManager.ReceiverType == SocketDataType.Network)
            {
                xDir = -1.0f;
            }

            int xLayer = (int)(Unit.transform.position.x * xDir);
            int yLayer = (int)(Unit.transform.position.y * -10.0f);
            Unit.SpineUnit.SortingOrder = 9001 + yLayer + xLayer;
        }
    }
}