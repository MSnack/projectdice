﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class CharacterController : GameUnitController
{
    private abstract class CharacterStateBase : GameUnitStateBase
    {
        protected CharacterController Manager = null;

        public override void OnInit(GameUnitController manager)
        {
            Manager = manager as CharacterController;
        }
    }

    private class CharacterSpawnState : CharacterStateBase
    {
        public override void StartState()
        {
            base.StartState();

            Manager.Unit.OnEndAniEvent += OnStateEnd;
        }

        private void OnStateEnd(GameUnitState state)
        {
            Manager.Unit.UnitState = GameUnitState.Idle;
        }

        public override void Update()
        {
            base.Update();
        }

        public override void EndState()
        {
            Manager.Unit.OnEndAniEvent -= OnStateEnd;

            base.EndState();
        }
    }

    private class CharcaterIdleState : CharacterStateBase
    {
        public override void StartState()
        {
            base.StartState();
        }

        public override void Update()
        {
            base.Update();

            if (Manager.Unit.IsDontMove == true)
                return;

            if (Manager.Unit.IsSkillReady == true)
            {
                if (Manager.Unit.CheckTriggetTarget(UnitTriggerType.Skill) == true)
                {
                    Manager.Unit.UnitState = GameUnitState.Skill;
                    return;
                }
            }

            if (Manager.Unit.IsAttackReady == true)
            {

                if (Manager.Unit.CheckTriggetTarget(UnitTriggerType.Attack) == true)
                {
                    Manager.Unit.UnitState = GameUnitState.Attack;
                    return;
                }
            }

            //if (Manager.Unit._monsterContainerMgr == null)
            //    return;

            //var firstMonster = Manager.Unit._monsterContainerMgr.FirstMonster;
            //if (firstMonster != null)
            //{
            //    Manager._targetMonster = firstMonster;

            //    if (Manager.Unit.IsUseAttack == true)
            //        Manager.Unit.OnAttack(Manager._targetMonster);
            //        //Manager.Unit.UnitState = GameUnitState.Attack;
            //    //Manager.Unit.UnitState = GameUnitState.Attack;
            //    //Manager.ChangeState(CharacterState.Attack);
            //}
        }
    }

    private class CharacterAttackState : CharacterStateBase
    {
        private void OnEndAni(GameUnitState state)
        {
            Manager.Unit.UnitState = GameUnitState.Idle;
        }

        public override void StartState()
        {
            base.StartState();
            var unit = Manager.Unit;

            float speed = unit.AttackSpeedStat.GetStat(unit.UnitLevel, unit.UnitReinforce, unit.UnitStep);
            if (speed > 0)
                unit.SpineUnit.WrapFrame(speed);

            Manager.Unit.OnEndAniEvent += OnEndAni;
        }

        public override void EndState()
        {
            base.EndState();
            Manager.Unit.SpineUnit.UnWrapFrame();
            Manager.Unit.OnEndAniEvent -= OnEndAni;
        }
    }

    private class CharacterSkillState : CharacterStateBase
    {
        private void OnEndAni(GameUnitState state)
        {
            Manager.Unit.UnitState = GameUnitState.Idle;
        }

        public override void StartState()
        {
            base.StartState();

            Manager.Unit.OnEndAniEvent += OnEndAni;
        }

        public override void EndState()
        {
            base.EndState();

            Manager.Unit.OnEndAniEvent -= OnEndAni;

        }
    }

    public override void OnInitializeState(GameUnit unit)
    {
        base.OnInitializeState(unit);

        AddState(GameUnitState.Idle, new CharcaterIdleState());
        AddState(GameUnitState.Attack, new CharacterAttackState());
        AddState(GameUnitState.Skill, new CharacterSkillState());
        AddState(GameUnitState.Spawn, new CharacterSpawnState());
    }

    protected override void Update()
    {
        base.Update();

        if (Unit == null)
            return;

        if (Unit.SpineUnit != null)
        {
            float xDir = 1.0f;
            if (Unit.BattleManager != null &&
                Unit.BattleManager.ReceiverType == SocketDataType.Network)
            {
                xDir = -1.0f;
            }

            int xLayer = (int)(Unit.transform.position.x * xDir);
            int yLayer = (int)(Unit.transform.position.y * -10.0f);
            Unit.SpineUnit.SortingOrder = 9001 + yLayer + xLayer;
        }
    }
}

