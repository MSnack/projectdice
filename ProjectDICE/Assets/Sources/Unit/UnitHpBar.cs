﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class UnitHpBar : MonoBehaviour
{
    [SerializeField, ReadOnly]
    private GameUnit _targetUnit = null;

    [SerializeField]
    private Transform _hpBarObject = null;

    [SerializeField, ReadOnly]
    private float _maxHp = 0.0f;

    [SerializeField]
    private float _lerpMode = 2;

    public void OnInitHpBar(GameUnit unit)
    {
        _targetUnit = unit;
        _maxHp = unit.Hp;
    }

    private void Update()
    {
        if (_targetUnit == null)
            return;

        float resultValue = (float)_targetUnit.Hp / _maxHp;
        float lerp = Mathf.Lerp(_hpBarObject.localScale.x, resultValue, _lerpMode);
        if (lerp < 0.9999f)
            _hpBarObject.localScale = new Vector3(lerp, 1, 1);
        else
            _hpBarObject.localScale = new Vector3(resultValue, 1.0f, 1.0f);
    }

    private void OnEnable()
    {
        _hpBarObject.localScale = Vector3.one;
    }

    public void Release()
    {
        _targetUnit = null;
    }
}
