﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New CardUReinforceData", menuName = "Data/CardUReinforceData", order = 3)]
public class CardUReinforceData : ScriptableObject
{
    [SerializeField]
    private List<int> _dataList = null;
    public int[] DataList => _dataList.ToArray();
}
