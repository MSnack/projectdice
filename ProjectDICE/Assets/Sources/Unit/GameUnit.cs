﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.Events;
using UnityEngine.AddressableAssets;

public enum GameUnitState
{
    None = -1,
    Idle = 0,
    Run = 1,
    Attack = 2,
    Skill = 3,
    Die = 5,
    Spawn = 10,
}

public enum UnitTriggerType
{
    Create,
    Attack,
    Skill,
    Die,
}

public enum RegisterSkillType
{
    Caster,
    Target,
}

[System.Serializable]
public class StatProperty
{
    public enum StatCoeffType
    {
        Add,
        Multi,
    }

    [SerializeField]
    private StatCoeffType _coeffType = StatCoeffType.Add;
    public StatCoeffType CoeffType => _coeffType;

    [SerializeField]
    private float _statCoeff = 0.0f;
    public float StatCoeff => _statCoeff;

    public float GetStat(float defaultStat, int grade)
    {
        switch (_coeffType)
        {
            case StatCoeffType.Add: return AddCoeff(grade);
            case StatCoeffType.Multi: return MultiCoeff(defaultStat, grade);
        }

        return 0.0f;
    }

    private float AddCoeff(int grade)
    {
        return _statCoeff * grade;
    }

    private float MultiCoeff(float defaultStat, int grade)
    {
        return defaultStat * grade;
    }
}

[System.Serializable]
public class StatData
{
    [SerializeField]
    private float _defaultStat = 0.0f;
    public float DefaultStat => _defaultStat;

    [SerializeField]
    private StatProperty _levelCoeff = new StatProperty();

    [SerializeField]
    private StatProperty _upgradeCoeff = new StatProperty();

    [SerializeField]
    private StatProperty _mergeCoeff = new StatProperty();

    //[SerializeField]
    //private float _levelCoeff = 0.0f;
    //public float LevelCoeff => _levelCoeff;

    //[SerializeField]
    //private float _reinforceCoeff = 0.0f;
    //public float ReinforceCoeff => _reinforceCoeff;

    //[SerializeField]
    //private float _stepCoeff = 0.0f;
    //public float StepCoeff => _stepCoeff;

    public float GetStat(int level, int upgrade, int merge)
    {
        var levelStat = _levelCoeff.GetStat(_defaultStat, level);
        var upgradeStat = _upgradeCoeff.GetStat(_defaultStat + levelStat, upgrade);
        var mergeStat = _mergeCoeff.GetStat(levelStat + upgradeStat, merge);

        return _defaultStat + levelStat + upgradeStat + mergeStat;
        //return (_levelCoeff * level) + (_reinforceCoeff * upgrade) + (_stepCoeff * step) + _defaultStat;
    }
}

public class StatBuff
{
    private float _stat = 0.0f;
    public float Stat { get => _stat; set => _stat = value; }

    private bool _isPercent = false;
    public bool IsPercent => _isPercent;

    public StatBuff(float stat)
    {
        _stat = stat;
    }

    public StatBuff(float stat, bool isPercent)
    {
        _stat = stat;
        _isPercent = isPercent;
    }
}

public class UnitStat
{
    private StatData _statData = null;
    private List<StatBuff> _statBuffLIst = new List<StatBuff>();

    public float GetStat(int level, int upgrade, int step)
    {
        float ratio = 1.0f;
        float stat = _statData.GetStat(level, upgrade, step);
            foreach (var buff in _statBuffLIst)
        {
            if (buff.IsPercent == true)
                ratio *= buff.Stat;
            else
                stat += buff.Stat;
        }

        stat *= ratio;

        return stat;
    }

    public void AddStatBuff(StatBuff buff)
    {
        if (buff == null)
            return;

        _statBuffLIst.Add(buff);
    }

    public void RemoveStatBuff(StatBuff buff)
    {
        if (buff == null)
            return;

        _statBuffLIst.Remove(buff);
    }

    public void SetStatData(StatData data)
    {
        _statData = data;
    }
}

public class MergeData
{
    public int UnitID = -1;
    public int UnitStep = -1;
    public int CurrentSlotIndex = -1;
}

public class GameUnit : EightUnit
{
    [SerializeField, ReadOnly]
    private int _unitId = -1;
    public int UnitID { get => _proxyId > -1 ? _proxyId : _unitId; }

    private int _proxyId = -1;

    [SerializeField, ReadOnly]
    private GameUnitState _unitState = GameUnitState.None;
    public GameUnitState UnitState { get => _unitState; set { _unitState = value; ChangeState(value); } }

    [SerializeField]
    private GameUnitController _controller = null;
    public GameUnitController Controller
    {
        get { return _controller; }
        set
        {
            if (_controller != null)
            {
                Destroy(_controller);
                _controller = null;
            }

            _controller = value;
        }
    }

    [SerializeField]
    private GameSpineObject _spineUnit = null;
    public GameSpineObject SpineUnit
    {
        get { return _spineUnit; }
        set
        {
            if (_spineUnit != null)
            {
                EightUtil.GetCore<GameSkinMgr>().ReturnSkin(_spineUnit);
                _spineUnit = null;
            }

            _spineUnit = value;
            if (_spineUnit != null)
            {
                _spineUnit.transform.SetParent(transform);
            }
        }
    }

    private int _sp = 0;
    public int Sp => _sp;

    private List<GameEffectObject> _haveEffect = new List<GameEffectObject>();

    public BattleCardData BattleCardData
    {
        get
        {
            int id = _unitId;

            if (_proxyId > -1)
                id = _proxyId;

            if (_userManager == null)
                return null;

            return _userManager.UserData.GetUnitData(id);
        }
    }

    public int UnitLevel
    {
        get
        {
            BattleCardData data = BattleCardData;
            if (data == null)
                return 0;

            return data.Level;
        }
    }

    public int UnitReinforce
    {
        get
        {
            BattleCardData data = BattleCardData;
            if (data == null)
                return 0;

            return data.Reinforce;
        }
    }

    private int _unitStep = 0;
    public int UnitStep { get { return _unitStep; } set { _unitStep = value; } }

    [SerializeField, ReadOnly]
    private int _hp = -1;
    public int Hp { get { return _hp; } set { _hp = value; } }

    private UnitStat _combatStat = null;
    public UnitStat CombatStat => _combatStat;
    public int Combat
    {
        get
        {
            if (_combatStat == null)
                return 0;
    
            return (int)_combatStat.GetStat(UnitLevel, UnitReinforce, UnitStep);
        }
    }

    private UnitStat _criticalCombatStat = null;
    public UnitStat CriticalCombatStat => _criticalCombatStat;
    public float CriticalCombat
    {
        get
        {
            if (_criticalCombatStat == null)
                return 0;

            return _criticalCombatStat.GetStat(UnitLevel, UnitReinforce, UnitStep);
        }
    }

    private UnitStat _criticalPercentStat = null;
    public UnitStat CriticalPercentStat => _criticalPercentStat;
    public float CriticalPercent
    {
        get
        {
            if (_criticalPercentStat == null)
                return 0.0f;

            return _criticalPercentStat.GetStat(UnitLevel, UnitReinforce, UnitStep);
        }
    }

    private UnitStat _attackSpeedStat = null;
    public UnitStat AttackSpeedStat => _attackSpeedStat;
    private float _attackTime = 0.0f;

    public bool IsAttackReady
    {
        get
        {
            if (_attackSpeedStat == null)
                return false;

            float totalTime = _attackSpeedStat.GetStat(UnitLevel, UnitReinforce, UnitStep);

            if (totalTime <= 0.0f)
                totalTime = 0.0f;

            if (totalTime > _attackTime)
                return false;

            return true;
        }
    }

    private int _count = 0;
    public int Count { get => _count; set => _count = value; }

    private bool _isSkillReady = false;
    public bool IsSkillReady { get { return _isSkillReady; } set { _isSkillReady = value; } }

    private float _moveSpeed = 0.0f;
    public float MoveSpeed => _moveSpeed;

    private int _power = 0;
    public int Power => _power;

    public event UnityAction<GameUnitState> OnEndAniEvent = null;
    public event UnityAction OnAttackEvent = null;
    public event UnityAction<GameUnit, GameUnit, int> OnGiveDamageEvent = null;
    public event UnityAction OnSkillEvent = null;
    public event UnityAction<GameUnit> OnDieEvent = null;
    public event UnityAction<GameUnit> OnReturnEvent = null;
    public event UnityAction<GameUnit> OnReleaseEvent = null;
    public event UnityAction OnHitEvent = null;
    public event UnityAction<int> OnDamageEvent = null;
    public event UnityAction<GameUnit> OnHitDie = null;
    public event UnityAction<GameUnit> OnRoadEndDie = null;
    public event UnityAction<int, bool> SetDamageEvent = null;
    public event UnityAction<int> UpdateHpEvent = null;
    public event UnityAction<GameUnit, GameUnit> OnMergeEvent = null;
    public event UnityAction<GameUnit> OnCreateEvent = null;
    public event UnityAction EndMoveEvent = null;

    private Dictionary<UnitTriggerType, UnitTriggerList> _unitSkillTrigger = null;

    [SerializeField, ReadOnly]
    private BattleManager _battleManager = null;
    public BattleManager BattleManager { get => _battleManager; set => _battleManager = value; }

    [SerializeField, ReadOnly]
    private TileRoadMgr _tileRoadManager = null;
    public TileRoadMgr TileRoadManager { get => _tileRoadManager; set => _tileRoadManager = value; }

    [SerializeField, ReadOnly]
    private TileSlotMgr _tileSlotManager = null;
    public TileSlotMgr TileSlotManager { get => _tileSlotManager; set => _tileSlotManager = value; }

    [SerializeField, ReadOnly]
    private BattleUserManager _userManager = null;
    public BattleUserManager UserManager { get => _userManager; set => _userManager = value; }

    [SerializeField, ReadOnly]
    private BattleWaveController _waveController = null;
    public BattleWaveController WaveController { get => _waveController; set => _waveController = value; }

    private RandomValueController _unitRandomController = new RandomValueController();
    public RandomValueController UnitRandomController => _unitRandomController;

    [SerializeField, ReadOnly]
    private int _currTileIndex = -1;
    public int CurrTileIndex { get => _currTileIndex; set => _currTileIndex = value; }

    private bool _isDummyMonster = false;
    public bool IsDummyMonster { get => _isDummyMonster; set => _isDummyMonster = value; }

    private ImmuneList _immune = null;
    public ImmuneList Immune => _immune;

    private class UnitBuff
    {
        private List<GameBuff> _buffList = new List<GameBuff>();
        public GameBuff[] BuffList => _buffList.ToArray();
        public int BuffCount => _buffList.Count;

        public void AddBuff(GameBuff buff)
        {
            if (buff == null)
                return;

            _buffList.Add(buff);
        }

        public void RemoveBuff(GameBuff buff)
        {
            if (_buffList.Count <= 0)
                return;

            _buffList.Remove(buff);
        }

        public void ClearBuff()
        {
            _buffList.Clear();
        }
    }

    private List<BuffType> _dontBuffs = new List<BuffType>();

    public void AddDontBuff(BuffType type) { _dontBuffs.Add(type); }
    public void RemoveDontBuff(BuffType type) { _dontBuffs.Remove(type); }

    public bool IsDontBuff(BuffType type)
    {
        foreach (var bt in _dontBuffs)
        {
            if (bt == type)
                return true;
        }

        return false;
    }

    private Dictionary<BuffType, UnitBuff> _buffs = new Dictionary<BuffType, UnitBuff>();
    public void GetBuff(BuffType type, List<GameBuff> buffs)
    {
        if (IsHaveBuff(type) == true)
        {
            foreach (var buff in _buffs[type].BuffList)
                buffs.Add(buff);
        }
    }

    public GameBuff GetBuff(BuffType type, int tag)
    {   
        if(IsHaveBuff(type,tag) == true)
        {
            foreach(var buff in _buffs[type].BuffList)
            {
                if (buff.Asset.BuffTag == tag)
                    return buff;
            }
        }

        return null;
    }

    public bool IsHaveBuff(BuffType type)
    {
        if (_buffs.ContainsKey(type) == false ||
            _buffs[type].BuffCount <= 0)
            return false;

        return true;
    }

    public bool IsHaveBuff(BuffType type, int tag)
    {

        if (_buffs.ContainsKey(type) == false)
            return false;

        if (_buffs[type].BuffCount <= 0)
            return false;

        foreach (var buff in _buffs[type].BuffList)
        {
            if (buff.Asset.BuffTag == tag)
                return true;
        }

        return false;
    }

    public void OnGiveDamage(GameUnit target, int damage)
    {
        OnGiveDamageEvent?.Invoke(this, target, damage);
    }

    public void OnBuff(GameBuff buff)
    {
        if (buff == null)
            return;

        var type = buff.Asset.BuffType;

        if (IsDontBuff(type) == true)
            return;

        if (_buffs.ContainsKey(type) == false)
            _buffs.Add(type, new UnitBuff());

        GameEffectData effectData = null;
        if(buff.Asset.EffectData == null)
            effectData = GameUnitUtil.UnitSettingAsset.GetEffectData(type);
        else
            effectData = buff.Asset.EffectData;

        if (effectData != null)
        {
            var effect = EightUtil.GetCore<GameEffectMgr>().UseEffect(effectData);

            if(buff.Asset.IsFixedOrder == true)
            {
                effect.transform.SetParent(this.transform);
                effect.transform.localPosition = Vector3.zero;
            }
            else
                AddEffect(effect);

            effect.OnReturnEvent += () =>
            {
                if(_haveEffect != null)
                    _haveEffect.Remove(buff.BuffEffect);

                buff.BuffEffect = null;
            };
            effect.Clear();
            effect.ReStart();
            buff.BuffEffect = effect;
        }

        _buffs[type].AddBuff(buff);
    }

    public void RemoveBuff(GameBuff buff)
    {
        if (buff == null)
            return;

        if (_buffs.Count <= 0)
            return;

        if (!_buffs.ContainsKey(buff.Asset.BuffType))
            return;

        var type = buff.Asset.BuffType;
        _buffs[type].RemoveBuff(buff);
        if (buff.BuffEffect != null)
        {
            EightUtil.GetCore<GameEffectMgr>().ReturnEffect(buff.BuffEffect);
            buff.BuffEffect = null;
        }
    }

    public void ReleaseAllBuff()
    {
        if (_buffs != null)
        {
            foreach (var unitBuff in _buffs.Values)
            {
                var buffList = unitBuff.BuffList;
                for (int i = buffList.Length - 1; i >= 0; --i)
                {
                    var buff = buffList[i];
                    RemoveBuff(buff);
                }
            }
        }
        
        _buffs.Clear();
        _dontBuffs.Clear();
    }

    private Dictionary<RegisterSkillType, GameSkillActionReferencePool> _skillReferenceCounter = new Dictionary<RegisterSkillType, GameSkillActionReferencePool>();
    protected bool IsHaveSkillReference(RegisterSkillType type)
    {
        if (_skillReferenceCounter.ContainsKey(type) == false)
            return false;

        return _skillReferenceCounter[type].ReferenceCount > 0 ? true : false;
    }

    public void RegisterSkillAction(RegisterSkillType type, GameSkillAction skillAction)
    {
        if (_skillReferenceCounter.ContainsKey(type) == false)
            _skillReferenceCounter[type] = new GameSkillActionReferencePool();

        _skillReferenceCounter[type].Register(skillAction);
    }

    public bool IsCleanUnit => CheckCleanUnit();
    private bool CheckCleanUnit()
    {
        if (IsHaveSkillReference(RegisterSkillType.Caster) == true ||
            IsHaveSkillReference(RegisterSkillType.Target) == true)
            return false;

        return true;
    }

    protected virtual void ChangeState(GameUnitState state)
    {
        if (_controller != null)
            _controller.ChangeState(state);
        
        if (_spineUnit != null)
            _spineUnit.ChangeState(state);
    }

    public override void OnInitialize()
    {
        base.OnInitialize();

        if (Controller != null)
            Controller.OnInitializeState(this);

        OnSpineInitialize();

        OnTrigger(UnitTriggerType.Create);
        OnCreateEvent?.Invoke(this);
    }

    public void OnTileRoadEnd()
    {
        OnRoadEndDie?.Invoke(this);
    }

    public void OnSpineInitialize()
    {
        if (SpineUnit != null)
        {
            SpineUnit.OnInitialize();
            SpineUnit.OnAttackEvent += OnAttack;
            SpineUnit.OnSkillEvent += OnSkill;
            SpineUnit.OnEndAniEvent += OnEndAni;
            SpineUnit.OnDieEvent += OnDie;
            SpineUnit.OnDieTriggerEvent += OnDieTrigger;
        }
    }

    private void OnAttack()
    {
        if (IsAttackReady == false)
            return;

        OnAttackEvent?.Invoke();
        OnTrigger(UnitTriggerType.Attack);
        _attackTime = 0.0f;
    }

    public void OnSkill()
    {
        if (_isSkillReady)
        {
            OnSkillEvent?.Invoke();
            OnTrigger(UnitTriggerType.Skill);
            _isSkillReady = false;
        }
    }

    public void OnNormal()
    {
        UnitState = GameUnitState.Idle;
    }

    public void OnMerge(GameUnit target)
    {
        if (target == null)
            return;

        OnMergeEvent?.Invoke(this, target);
    }

    public void UpdateEffectPos()
    {
        foreach (var effect in _haveEffect)
        {
            effect.transform.position = transform.position;
            UpdateEffectLayer(effect,20);
        }
    }

    public void UpdateEffectLayer(GameEffectObject effect, int add)
    {
        if (effect == null || SpineUnit == null)
            return;

        effect.SortingOrder = SpineUnit.SortingOrder + add;
    }

    private Coroutine _moveRoutine = null;
    private bool _isMove = false;
    public bool IsMove => _isMove;

    private void StartMove(Vector3 targetPos)
    {
        if (_moveRoutine != null)
            StopMove();

        _moveRoutine = StartCoroutine(OnMoveRoutine(targetPos));
        _isMove = true;
    }

    public void StopMove()
    {
        if (_moveRoutine == null)
            return;

        StopCoroutine(_moveRoutine);
        _moveRoutine = null;
        _isMove = false;
    }

    public void OnMove(Vector3 targetPos)
    {
        if (UnitState == GameUnitState.Die)
            return;

        FlipCheck(targetPos);

        StartMove(targetPos);
    }

    public void FlipCheck(Vector3 targetPos)
    {
        var dir = targetPos - transform.position;
        if (dir.x < 0.0f)
            SpineUnit.IsFlip = true;
        else
            SpineUnit.IsFlip = false;
    }

    public void OnUnitDie()
    {
        if (UnitState == GameUnitState.Die)
            return;

        UnitState = GameUnitState.Die;
    }

    public void OnTimmerSkill(SkillAsset skill, TargetType targetType, float lifeTime, float cycleTime)
    {
        if (skill == null)
            return;

        StartCoroutine(TimmerRoutine(skill, targetType, lifeTime, cycleTime));
    }

    private IEnumerator TimmerRoutine(SkillAsset skill, TargetType targetType, float lifeTime, float cycleTime)
    {
        float currLife = 0.0f;
        float currCycle = 0.0f;

        while (true)
        {
            if (lifeTime != -1)
            {
                currLife += Time.fixedDeltaTime;
                if (currLife >= lifeTime)
                    break;
            }

            currCycle += Time.fixedDeltaTime;
            if (currCycle >= cycleTime)
            {
                GameUnit target = GetTargetType(targetType);
                if (target != null)
                {
                    EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(skill, this, target);
                }

                currCycle -= cycleTime;
            }

            yield return new WaitForFixedUpdate();
        }
    }

    private IEnumerator OnMoveRoutine(Vector3 targetPos)
    {
        while (true)
        {
            //if (UnitState != GameUnitState.Run)
            //    break;

            Vector3 pos = transform.position;

            float moveSpeed = _moveSpeed; //_moveSpeedStat.GetStat(UnitLevel, UnitReinforce, UnitStep);

            //슬로우 버프 
            if (_buffs.ContainsKey(BuffType.Slow) == true)
            {
                foreach (var buff in _buffs[BuffType.Slow].BuffList)
                {
                    moveSpeed *= 1/ ((SlowBuff)buff).SlowPower;
                }
            }

            //빙결 버프 
            if (_buffs.ContainsKey(BuffType.Frozen) == true)
            {
                foreach (var buff in _buffs[BuffType.Frozen].BuffList)
                {
                    moveSpeed *= 1/ ((FrozenBuff)buff).FrozenPower;
                }
            }

            Vector3 dir = (targetPos - pos).normalized;
            float dist = (targetPos - pos).magnitude;
            float speed = moveSpeed * Time.fixedDeltaTime;
            if (dist < speed)
            {
                float moveTo = speed - dist;
                pos += dir * moveTo;
                break;
            }
            else
                pos += dir * speed;

            if (SpineUnit != null)
            {
                if (dir.x > 0.0f)
                    SpineUnit.IsFlip = true;
                else
                    SpineUnit.IsFlip = false;
            }
            transform.position = pos;
            yield return new WaitForFixedUpdate();
        }

        EndMoveEvent?.Invoke();
        //OnNormal();
        //StopMove();
    }

    public void LookAtTarget(TargetType type)
    {
        var target = GetTargetType(type);
        if (target == null || target == this)
            return;

        var targetPos = target.transform.position;
        var unitPos = transform.position;

        if (targetPos.x > unitPos.x)
            SpineUnit.IsFlip = true;
        else if (targetPos.x < unitPos.x)
            SpineUnit.IsFlip = false;
    }

    private int _dontMoveCount = 0;
    public bool IsDontMove {
        get {
            if (_dontMoveCount > 0)
                return true;

            return false;
        }

        set { 
            if (value == true)
                ++_dontMoveCount;
            else
                --_dontMoveCount;
        }
    }

    private bool _isNoDamage = false;
    public bool IsNoDamage { get { return _isNoDamage; } set { _isNoDamage = value; } }

    private bool SetDamage(int damage, bool isCritical)
    {
        if (UnitState == GameUnitState.Die)
            return false;

        if (IsNoDamage == false)
        {
            _hp -= damage;
            if (_hp <= 0)
            {
                _hp = 0;
                UnitState = GameUnitState.Die;
                OnHitDie?.Invoke(this);
            }
            //var fontMgr = EightUtil.GetCore<GameDamageFontMgr>();
            //var fontObject = fontMgr.GetFontObject();
            //if (fontObject != null)
            //{
            //    //fontObject.OnDraw(fontMgr.DataAsset, transform.position + SpineUnit.CenterPivot, fontMgr.DataAsset.StartColor);
            //    fontObject.OnFontText(damage.ToString());
            //}

            SetDamageEvent?.Invoke(damage, isCritical);
            UpdateHpEvent?.Invoke(_hp);
            BattleManager?.MsgInterface?.LogSystem?.SendDamageLog(this, damage);
        }

        return true;
    }

    public bool OnDamage(int damage, bool isCritical)
    {
        if (SetDamage(damage, isCritical) == false)
            return false;

        OnDamageEvent?.Invoke(damage);
        OnHitEvent?.Invoke();

        return true;
    }

    public bool OnSubDamage(int damage, bool isCritical)
    {
        if (SetDamage(damage, isCritical) == false)
            return false;

        OnDamageEvent?.Invoke(damage);
        return true;
    }

    public bool OnNoEventDamage(int damage, bool isCritical)
    {
        return SetDamage(damage, isCritical);
    }

    private void OnTrigger(UnitTriggerType type)
    {
        if (_unitSkillTrigger == null)
            return;

        if (_unitSkillTrigger.ContainsKey(type) == false)
            return;

        var triggerList = _unitSkillTrigger[type].TriggetSlots;
        foreach (var trigger in triggerList)
        {
            GameUnit target = GetTargetType(_unitSkillTrigger[type].SkillTargetType);
            if (target == null)
                continue;

            EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(trigger, this, target);
        }
    }

    public bool CheckTriggetTarget(UnitTriggerType type)
    {
        if (_unitSkillTrigger == null)
            return false;

        if (_unitSkillTrigger.ContainsKey(type) == false)
            return false;

        GameUnit target = GetTargetType(_unitSkillTrigger[type].SkillTargetType);
        if (target == null)
            return false;

        return true;
    }

    public GameUnit GetTargetType(TargetType type)
    {
        switch (type)
        {
            case TargetType.Myself: return this;
            case TargetType.RoadFirstUnit: return TileRoadManager?.FirstUnit;
            case TargetType.RoadStrongUnit: return TileRoadManager?.StrongUnit;
            case TargetType.RoadWeakUnit: return TileRoadManager?.WeakUnit;
            case TargetType.RoadRandomUnit: return RandomTileUnit;
        }

        return null;
    }

    public GameUnit RandomTileUnit
    {
        get
        {
            if (_tileRoadManager == null)
                return null;

            //return _tileRoadManager.FirstUnit;

            var filterUnitList = TileRoadManager.TileUnitListAtLive;
            if (filterUnitList == null || filterUnitList.Length <= 0)
                return null;

            int randomIndex = UnitRandomController.GetNextValueRange(RandomValueType.RandomTileUnit, 0, filterUnitList.Length - 1);
            Debug.Log("RandomIndex : " + randomIndex.ToString() + " " + "UnitListCount : " + filterUnitList.Length);
            return filterUnitList[randomIndex];
        }
    }

    public void SetCharacterData(CharacterUnitData data, int grade)
    {
        _unitId = data.DataID;
        _proxyId = data.ProxyId;

        if (_combatStat == null)
            _combatStat = new UnitStat();

        _combatStat.SetStatData(data.Combat);

        if (_attackSpeedStat == null)
            _attackSpeedStat = new UnitStat();

        _attackSpeedStat.SetStatData(data.AttackSpeed);

        if (_criticalPercentStat == null)
            _criticalPercentStat = new UnitStat();

        _criticalPercentStat.SetStatData(data.CriticalPercent);

        if (_criticalCombatStat == null)
            _criticalCombatStat = new UnitStat();

        _criticalCombatStat.SetStatData(data.CriticalCombat);
      
        _unitSkillTrigger = data.SkillTriggerData;

        _unitStep = grade;

        data.MergeTriggerData?.OnTriggerEventInUnit(this);
    }

    public void SetMonsterData(MonsterUnitData data, BattleMonsterSpawner.MonsterInitData initData)
    {
        _unitId = data.DataID;
        _hp = initData.Hp;
        _moveSpeed = initData.Speed;
        _power = initData.Damage;
        _sp = initData.Sp;
        _currTileIndex = 0;

        _immune = data.Immune;

        _unitSkillTrigger = data.SkillTriggerData;
    }

    private void Update()
    {
        UpdateEffectPos();
    }

    private void FixedUpdate()
    {
        _attackTime += Time.fixedDeltaTime;
    }

    public void ReleaseAllEffect()
    {
        var effectMgr = EightUtil.GetCore<GameEffectMgr>();
        for (int i = _haveEffect.Count - 1; i >= 0; --i)
        {
            effectMgr.ReturnEffect(_haveEffect[i]);
        }
        _haveEffect.Clear();
    }

    public void AddEffect(GameEffectObject effect)
    {
        effect.transform.position = transform.position;
        effect.OnReturnEvent += () =>
        {
            _haveEffect.Remove(effect);
        };

        _haveEffect.Add(effect);
    }

    public void Return()
    {
        OnReturnEvent?.Invoke(this);
        OnReturnEvent = null;
    }

    public override void Release()
    {
        base.Release();

        _unitId = -1;

        CurrTileIndex = -1;

        _attackTime = 0.0f;

        _dontMoveCount = 0;

        _moveSpeed = 0.0f;

        _power = 0;
        _sp = 0;

        Controller?.ChangeState(-1);
        Controller?.Release();
        Controller = null;

        SpineUnit = null;

        _immune = null;

        _unitState = GameUnitState.Die;
        //_unitTeam = GameUnitTeam.Null;

        _unitSkillTrigger = null;

        TileRoadManager = null;
        TileSlotManager = null;
        UserManager = null;

        //HpBar = null;

        ReleaseAllBuff();
        ReleaseAllEffect();

        _combatStat = null;
        _attackSpeedStat = null;
        //_moveSpeedStat = null;
        _criticalCombatStat = null;
        _criticalPercentStat = null;

        _isSkillReady = false;

        StopAllCoroutines();

        _unitRandomController.Release();

        OnReleaseEvent?.Invoke(this);

        OnAttackEvent = null;
        OnSkillEvent = null;
        OnReleaseEvent = null;
        OnDieEvent = null;
        OnEndAniEvent = null;
        OnHitEvent = null;
        OnDamageEvent = null;
        OnReturnEvent = null;
        OnHitDie = null;
        OnRoadEndDie = null;
        SetDamageEvent = null;
        UpdateHpEvent = null;
        OnMergeEvent = null;
        OnCreateEvent = null;
        EndMoveEvent = null;
    }

    protected virtual void OnEndAni()
    {
        OnEndAniEvent?.Invoke(UnitState);
    }

    private void OnDieTrigger()
    {
        OnTrigger(UnitTriggerType.Die);
    }

    private void OnDie()
    {
        ReleaseAllBuff();

        OnDieEvent?.Invoke(this);
        OnDieEvent = null;
    }
}
