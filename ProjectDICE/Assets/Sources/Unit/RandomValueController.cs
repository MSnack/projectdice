﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RandomValueType
{
    Damage,
    Percent,
    Floor,
    Block,
    RandomTarget,
    Life,
    RandomDamage,
    RandomTileUnit,
    StockBuff,
    CurseBuff,
}

public class RandomValueController
{
    private System.Random _randomSystem = null;

    private Dictionary<RandomValueType, System.Random> _randomValues = new Dictionary<RandomValueType, System.Random>();

    private int _seed = 0;
    public int Seed => _seed;

    public void UpdateSeed(int seed)
    {
        _randomSystem = new System.Random(seed);

        UpdateRandomValues();
        _seed = seed;
    }

    public void UpdateRandomValues()
    {
        if (_randomSystem == null)
            return;

        foreach (RandomValueType type in System.Enum.GetValues(typeof(RandomValueType)))
        {
            _randomValues[type] = new System.Random(_randomSystem.Next(int.MaxValue));
        }
    }

    public float GetNextValue(RandomValueType type)
    {
        if (_randomValues.ContainsKey(type) == false)
        {
            Debug.Log("Not Have RandomSystem");
            return -1.0f;
        }

        return (float)_randomValues[type].NextDouble();
    }

    public int GetNextValueRange(RandomValueType type, int min, int max)
    {
        if (_randomValues.ContainsKey(type) == false)
        {
            Debug.Log("Not Have RandomSystem");
            return -1;
        }

        return _randomValues[type].Next(min, max);
        //return Mathf.FloorToInt(GetNextValue() * (max - min)) + min;
    }

    public float GetNextValueRange(RandomValueType type, float min, float max)
    {
        return GetNextValue(type) * (max - min) + min;
    }

    public void Release()
    {
        _randomSystem = null;
        _randomValues.Clear();
        _seed = 0;
    }

    //public void UpdateRandomValues(float[] randomValues)
    //{
    //    if (randomValues == null)
    //        return;

    //    ClearRandomValues();
    //    _randomValues.AddRange(randomValues);
    //}

    //public void ClearRandomValues()
    //{
    //    _randomValues.Clear();
    //    _index = 0;
    //}

    //public float GetNextValue()
    //{
    //    if (_randomValues.Count <= 0)
    //        return Random.Range(0.0f,1.0f);

    //    if (_randomValues.Count >= _index)
    //        _index = 0;

    //    return _randomValues[_index++];
    //}

    //public float[] GetValuesRange(int count)
    //{
    //    float[] values = new float[count];
    //    for (int i = 0; i < count; ++i)
    //    {
    //        values[i] = GetNextValue();
    //    }

    //    return values;
    //}
}
