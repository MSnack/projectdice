﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TargetType
{
    Null,
    Myself,
    RoadFirstUnit,
    RoadStrongUnit,
    RoadWeakUnit,
    RoadRandomUnit,
}

//[System.Serializable]
//public class UnitTriggerSlot
//{
//    [SerializeField]
//    private TargetType _targetType = TargetType.Null;
//    public TargetType SkillTargetType => _targetType;

//    [SerializeField]
//    private SkillAsset _skillAsset = null;
//    public SkillAsset Skill => _skillAsset;
//}

[System.Serializable]
public class UnitTriggerList
{
    [SerializeField]
    private TargetType _targetType = TargetType.Null;
    public TargetType SkillTargetType => _targetType;

    [SerializeField]
    private List<SkillAsset> _triggerSlots = null;
    public SkillAsset[] TriggetSlots => _triggerSlots.ToArray();
}