﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class GameUnitHpBarMgr : EightWork.Core.EightCore
{
    [SerializeField]
    private UnitHpBar _instance = null;

    [SerializeField]
    private int _reserveCount = 5;

    private Queue<UnitHpBar> _reserveHpBar = new Queue<UnitHpBar>();

    public override void InitializedCore()
    {

    }

    private bool ReserveHpBar()
    {
        if (_instance == null)
            return false;

        for (int i = 0; i < _reserveCount; ++i)
        {
            var copy = Instantiate(_instance, this.transform);
            copy.gameObject.SetActive(false);

            _reserveHpBar.Enqueue(copy);
        }

        return true;
    }

    public UnitHpBar GetHpBar()
    {
        if (_reserveHpBar.Count <= 0)
        {
            if (ReserveHpBar() == false)
                return null;
        }

        var hpBar = _reserveHpBar.Dequeue();
        hpBar.gameObject.SetActive(true);
        hpBar.transform.SetParent(null);

        return hpBar;
    }

    public void ReturnHpBar(UnitHpBar hpBar)
    {
        hpBar.gameObject.SetActive(false);
        hpBar.transform.SetParent(this.transform);

        hpBar.transform.position = Vector3.zero;
        hpBar.Release();

        _reserveHpBar.Enqueue(hpBar);
    }

    public void ReleaseAll()
    {
        var list = _reserveHpBar.ToArray();
        _reserveHpBar.Clear();
        for (int i = list.Length - 1; i >= 0; --i)
        {
            Destroy(list[i].gameObject);
        }
    }
}
