﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public abstract class ResourceObject
{
    public abstract Object Asset { get; }

    ~ResourceObject()
    {
        Release();
    }

    public abstract void Release();
}

public class AddressableResourceObjectT<TObject> : ResourceObject where TObject : Object
{
    private AsyncOperationHandle<TObject> _handle;
    protected AsyncOperationHandle<TObject> Handle => _handle;

    public override Object Asset => _handle.Result;
    public TObject TAsset => _handle.Result as TObject;

    public AddressableResourceObjectT(AsyncOperationHandle<TObject> handle)
    {
        _handle = handle;
    }

    public override void Release()
    {
        Addressables.Release(_handle);
    }
}

public class AddressableInstanceResourceObject : AddressableResourceObjectT<GameObject>
{
    public AddressableInstanceResourceObject(AsyncOperationHandle<GameObject> handle) : base(handle) { }

    public override void Release()
    {
        Addressables.ReleaseInstance(Handle);
    }
}