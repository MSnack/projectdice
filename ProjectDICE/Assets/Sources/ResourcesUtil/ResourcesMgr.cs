﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;
using System.Threading.Tasks;

public static class ResourcesMgr
{
    public static void LoadAddressableAsync<TObject>(string key, UnityAction<ResourceObject> callback)
        where TObject : Object
    {
        Addressables.LoadAssetAsync<TObject>(key).Completed += (handle) =>
        {
            var resourceObject = new AddressableResourceObjectT<TObject>(handle);
            callback?.Invoke(resourceObject);
        };
    }

    public static void LoadAddressableInstanceAsync(string key, UnityAction<ResourceObject> callback)
    {
        Addressables.InstantiateAsync(key).Completed += (handle) =>
        {
            var resourceObject = new AddressableInstanceResourceObject(handle);
            callback?.Invoke(resourceObject);
        };
    }

    public static void GetBoxObject(int id, UnityAction<ResourceObject> callback)
    {
        string path = "BoxObject/";
        string name = "Box" + id.ToString() + ".prefab";
        string key = path + name;

        LoadAddressableInstanceAsync(key, callback);
    }
}
