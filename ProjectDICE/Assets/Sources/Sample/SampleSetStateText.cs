﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class SampleSetStateText : MonoBehaviour
{
    [SerializeField, ReadOnly]
    private Text _text = null;

    [SerializeField, TextArea]
    private string _textInput = "";

    [SerializeField]
    private string _id = "";

    
    // Start is called before the first frame update
    void Start()
    {
        _id = gameObject.GetInstanceID().ToString();
        if (_text == null) _text = GetComponent<Text>();
        SampleFSMInterface.StateChanged += ChangeText;
    }

    void ChangeText(SampleFSMSystem.SampleFSMState state)
    {
        _text.text = _textInput + state.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Reset()
    {
        _text = GetComponent<Text>();
    }

    public void SetScene(bool ssss)
    {
        SceneManager.LoadScene(ssss ? "SampleOtherScene" : "SampleScene");
    }
}
