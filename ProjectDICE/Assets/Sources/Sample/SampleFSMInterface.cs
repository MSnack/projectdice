﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork.MsgSystem;
using UnityEngine;
using UnityEngine.Events;

public class SampleFSMInterface : EightMsgSystem
{
    public static UnityAction<SampleFSMSystem.SampleFSMState> StateChanged = null;

    protected override void Start()
    {
        base.Start();
    }

    private void OnDestroy()
    {
        StateChanged = null;
    }
}
