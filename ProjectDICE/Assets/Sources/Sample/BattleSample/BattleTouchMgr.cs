﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.AddressableAssets;

public class BattleTouchMgr : MonoBehaviour
{
    [SerializeField]
    private AssetLabelReference _alr = null;

    [SerializeField]
    private GameUnit _target = null;

    [SerializeField]
    private Vector2 _savePos = Vector2.zero;

    private void Awake()
    {
        Addressables.DownloadDependenciesAsync(_alr.labelString);
    }

    private void Update()
    {
        if (EightUtil.IsLoadCompleteDevice == false)
            return;

        if (_target == null)
        {
            if (Input.GetMouseButtonDown(0) == true)
            {
                Vector2 pos = EightUtil.GetCore<EightCameraMgr>().EightCamera.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero, 0.0f);
                if (hit.collider == null)
                    return;

                var target = hit.collider.gameObject.GetComponent<GameUnit>();

                if (target.IsDontMove == true)
                    return;

                _savePos = target.transform.position;
                _target = target;
            }
        }
        else
        {
            if (Input.GetMouseButtonUp(0) == true)
            {
                Vector2 pos = EightUtil.GetCore<EightCameraMgr>().EightCamera.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D[] hitList = Physics2D.RaycastAll(pos, Vector2.zero, 0.0f);
                if (hitList.Length > 0)
                {
                    foreach (var hit in hitList)
                    {
                        var target = hit.collider.gameObject.GetComponent<GameUnit>();
                        if (target == null)
                            continue;

                        if (target == _target)
                            continue;

                        EightUtil.GetCore<GameUnitMgr>().ReturnUnit(_target);
                        //Destroy(_target.gameObject);
                        break;
                    }
                }

                if (_target != null)
                {
                    _target.transform.position = _savePos;
                    _target = null;
                }

                return;
            }
            else if (Input.GetMouseButton(0) == true)
            {
                if (_target == null)
                    return;
                else
                {
                    if(_target.IsDontMove == true)
                    {
                        _target = null;
                        return;
                    }
 
                    Vector2 pos = EightUtil.GetCore<EightCameraMgr>().EightCamera.ScreenToWorldPoint(Input.mousePosition);
                    _target.transform.position = pos;
                }
            }
        }
    }
}
