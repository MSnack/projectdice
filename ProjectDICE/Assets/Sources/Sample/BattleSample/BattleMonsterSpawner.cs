﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using EightWork.MsgSystem;
using UnityEngine.Events;

public sealed class MonsterSpawnData : BattleSpawnData
{
    private int _unitId = -1;
    private BattleMonsterSpawner.MonsterInitData _initData;

    public MonsterSpawnData(int unitId, BattleMonsterSpawner.MonsterInitData initData)
    {
        _unitId = unitId;
        _initData = initData;
    }

    public override GameUnit InvokSpawn(BattleManager battleManager)
    {
        var unit = EightUtil.GetCore<GameUnitMgr>().CreateMonster<MonsterController>(_unitId, _initData, battleManager);
        if (unit == null)
            return null;

        return unit;
    }
}

public class BattleMonsterSpawner : EightMsgReceiver<BattleSceneInterface>
{
    public struct MonsterInitData
    {
        private int _hp;
        public int Hp => _hp;

        private int _damage;
        public int Damage => _damage;

        private float _speed;
        public float Speed => _speed;

        private int _sp;
        public int Sp => _sp;

        private BattleMonsterData.MonsterType _mType;
        public BattleMonsterData.MonsterType MType => _mType;
        public GameHpBarData.HpBarType HpType => GetHpBarData();

        public MonsterInitData(int hp, int damage, float speed, int sp, BattleMonsterData.MonsterType mType)
        {
            _hp = hp;
            _damage = damage;
            _speed = speed;
            _sp = sp;
            _mType = mType;
        }

        private GameHpBarData.HpBarType GetHpBarData()
        {
            switch (_mType)
            {
                case BattleMonsterData.MonsterType.Boss: return GameHpBarData.HpBarType.Boss;
                case BattleMonsterData.MonsterType.Spawn: return GameHpBarData.HpBarType.Monster;
                case BattleMonsterData.MonsterType.Summon: return GameHpBarData.HpBarType.Monster;
            }

            return GameHpBarData.HpBarType.Monster;
        }
    }

    private class BattleMonsterSpawnData
    {
        private BattleMonsterData _monsterData = null;
        public BattleMonsterData MonsterData => _monsterData;

        private int _spawnCount = 0;
        public int SpawnCount => _spawnCount;

        private float _timmer = 0.0f;
        public float Timmer => _timmer;

        public event UnityAction<BattleMonsterData> SpawnMonster = null;

        public BattleMonsterSpawnData(BattleMonsterData data)
        {
            _monsterData = data;
        }

        public void UpdateTime(BattleWaveData waveData)
        {
            _timmer += Time.fixedDeltaTime;

            var spawnTime = _monsterData.SpawnTime * waveData.SpawnScale;
            if (_timmer >= spawnTime)
                SpawnEvent(spawnTime);
        }

        private void SpawnEvent(float spawnTime)
        {
            _spawnCount += 1;
            _timmer -= spawnTime;
            SpawnMonster?.Invoke(_monsterData);
        }

        public void Release()
        {
            _monsterData = null;
            _spawnCount = 0;
            SpawnMonster = null;
        }
    }

    private List<BattleMonsterSpawnData> _monsterList = new List<BattleMonsterSpawnData>();

    [SerializeField]
    private BattleManager _battleManager = null;

    public event UnityAction<GameUnit> OnCreateMonster = null;
    public event UnityAction<GameUnit> OnBossCreateEvent = null;

    [SerializeField]
    private GameEffectDataAssetReference _monsterSpawnEffect = null;

    [SerializeField]
    private GameEffectDataAssetReference _bossSpawnEffect = null;

    private void Awake()
    {
        LoadEffect();
    }

    private void LoadEffect()
    {
        _monsterSpawnEffect?.LoadAssetAsync();
        _bossSpawnEffect?.LoadAssetAsync();
    }

    private void OnDestroy()
    {
        _monsterSpawnEffect?.ReleaseAsset();
        _bossSpawnEffect?.ReleaseAsset();
    }

    public void UpdateSpawnData(BattleMonsterData[] monsterDataList)
    {
        foreach (var spawnData in _monsterList)
        {
            spawnData.Release();
        }
        _monsterList.Clear();

        foreach (var data in monsterDataList)
        {
            var spawnData = new BattleMonsterSpawnData(data);
            spawnData.SpawnMonster += SpawnEvent;
            _monsterList.Add(spawnData);
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        _battleManager.WaveController.UpdateWaveTime += UpdateSpawnTime;
        _battleManager.MonsterController.UpdateSpawnMonsterDataEvent += UpdateSpawnData;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        _battleManager.MonsterController.UpdateSpawnMonsterDataEvent -= UpdateSpawnData;
        _battleManager.WaveController.UpdateWaveTime -= UpdateSpawnTime;
    }

    private void UpdateSpawnTime()
    {
        if (_battleManager.WaveController.IsDelayTime == true)
            return;

        var currWaveData = _battleManager.WaveController.GetCurrWaveData();
        foreach (var spawnData in _monsterList)
        {
            spawnData.UpdateTime(currWaveData);
        }
    }

    public MonsterInitData GetSpawnInit(BattleMonsterData monsterData, BattleStageData stageData, BattleWaveData waveData, int addHp = 0, int addSp = 0)
    {
        var waveHpScale = waveData != null ? waveData.HpScale : 1.0f;
        var stageHpScale = stageData != null ? stageData.HpScale : 1.0f;

        var hp = (int)(monsterData.Hp * waveHpScale * stageHpScale) + addHp;
        var speed = monsterData.Speed;
        var power = monsterData.Power;
        var sp = monsterData.Sp + addSp;
        var mType = monsterData.MType;

        var initData = new MonsterInitData(hp, power, speed, sp, mType);
        return initData;
    }

    private void SpawnEvent(BattleMonsterData data)
    {
        var stageData = _battleManager.StageController.GetCurrStageData();
        var waveData = _battleManager.WaveController.GetCurrWaveData();

        var initData = GetSpawnInit(data, stageData, waveData);
        var unit = EightUtil.GetCore<GameUnitMgr>()?.CreateMonster<MonsterController>(data.UnitId, initData, _battleManager);
        if (unit == null)
            return;

        unit.UnitState = GameUnitState.Spawn;

        var effect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_monsterSpawnEffect.Asset as GameEffectData);
        if (effect != null)
        {
            effect.transform.position = unit.transform.position + unit.SpineUnit.CenterPivot;
            float xDir = 1.0f;
            if (unit.BattleManager != null &&
                unit.BattleManager.ReceiverType == SocketDataType.Network)
            {
                xDir = -1.0f;
            }

            int xLayer = (int)(unit.transform.position.x * xDir);
            int yLayer = (int)(unit.transform.position.y * -10.0f);
            effect.SortingOrder = 9001 + yLayer + xLayer + 100;
        }

        OnCreateMonster?.Invoke(unit);
    }

    public void BossSpawnEvent(int totalHp, int totalSp)
    {
        var bossId = _battleManager.MonsterController.CurrBossId;
        var bossData = _battleManager.MonsterController.GetMonsterData(BattleMonsterData.MonsterType.Boss, bossId);
        var stageData = _battleManager.StageController.GetCurrStageData();
        var initData = GetSpawnInit(bossData, stageData, null, totalHp, totalSp);

        var unit = EightUtil.GetCore<GameUnitMgr>()?.CreateMonster<MonsterController>(bossData.UnitId, initData, _battleManager);
        if (unit == null)
            return;

        unit.UnitState = GameUnitState.Spawn;

        var effect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_bossSpawnEffect.Asset as GameEffectData);
        if (effect != null)
        {
            effect.transform.position = unit.transform.position + unit.SpineUnit.CenterPivot;
            float xDir = 1.0f;
            if (unit.BattleManager != null &&
                unit.BattleManager.ReceiverType == SocketDataType.Network)
            {
                xDir = -1.0f;
            }

            int xLayer = (int)(unit.transform.position.x * xDir);
            int yLayer = (int)(unit.transform.position.y * -10.0f);
            effect.SortingOrder = 9001 + yLayer + xLayer + 100;
        }

        OnBossCreateEvent?.Invoke(unit);
    }
}
