﻿using EightWork;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class BattleManager : BattleSocketReceiver<BattleSceneInterface>
{
    [SerializeField]
    private TileRoadMgr _tileRoadMgr = null;
    public TileRoadMgr TileRoadMgr => _tileRoadMgr;

    [SerializeField]
    private TileSlotMgr _tileSlotMgr = null;
    public TileSlotMgr TileSlotMgr => _tileSlotMgr;

    [SerializeField]
    private BattleUserManager _userManager = null;
    public BattleUserManager UserManager => _userManager;

    [SerializeField]
    private BattleStageController _stageController = null;
    public BattleStageController StageController => _stageController;

    [SerializeField]
    private BattleWaveController _waveController = null;
    public BattleWaveController WaveController => _waveController;

    [SerializeField]
    private BattleMonsterController _monsterController = null;
    public BattleMonsterController MonsterController => _monsterController;

    [SerializeField]
    private BattleMonsterSpawner _spwner = null;
    public BattleMonsterSpawner Spawner => _spwner;

    [SerializeField]
    private BattleBossControlCenter _bossControlCenter = null;
    public BattleBossControlCenter BossControlCenter => _bossControlCenter;

    [SerializeField]
    private BattleTouchController _touchController = null;
    public BattleTouchController TouchController => _touchController;

    [SerializeField]
    private BattleCharacterSpawner _characterSpawner = null;
    public BattleCharacterSpawner CharacterSpawner => _characterSpawner;

    public event UnityAction<RequestUnitData> OnCreateCharacterEvent = null;
    public event UnityAction<RequestUnitData> OnSpawnCharacterEvent = null;
    public event UnityAction<GameUnit> OnCreateMonsterEvent = null;
    public event UnityAction<RequestUpgradeData> UpgradeUnitEvent = null;
    public event UnityAction<bool> SyncBossEvent = null;
    public event UnityAction BattleLeaveSyncEvent = null;
    public event UnityAction BattleDisconnectEvent = null;
    //public event UnityAction<BattleInitUserData> InitUserEvent = null;

    private void Awake()
    {
        _userManager.SetEventManager(this);
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        //_waveController.CreateMonsterEvent += CreateMonsterEvent;
        //_waveController.CreateBossEvent += CreateBossEvent;
        Receiver.BattleDisconnectEvent += OnBattleDisconnect;
        Receiver.SpawnCharacterEvent += SpawnCharacterEvent;
        Receiver.CharacterEvent += CreateCharacterEvent;
        Receiver.CopyUnitEvent += CopyUnitEvent;
        // Receiver.MergeEvent += MergeEvent;
        Receiver.MergeUpgradeEvent += MergeUpgradeEvent;
        Receiver.BattleInitUserDataEvent += InitUserDataEvent;
        Receiver.UpgradeEvent += UpgradeEvent;
        Receiver.ReadyEvent += ReadyEvent;
        Receiver.BattleBossSyncEvent += BossClearSync;
        Receiver.BattleLeaveSyncEvent += BattleLeaveSync;
        _touchController.TouchTargetEvent += OnTouchTargetUnit;
        _touchController.TargetOffEvent += OnTouchTargetOff;
        Receiver.MergeCreateEvent+= MergeCreateEvent;
        Receiver.BattleLifeEvent += OnBattleLife;
        Receiver.RequestReadyEvent += OnRequestReadeyEvent;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        Receiver.BattleLifeEvent -= OnBattleLife;
        Receiver.MergeCreateEvent -= MergeCreateEvent;
        _touchController.TargetOffEvent -= OnTouchTargetOff;
        _touchController.TouchTargetEvent -= OnTouchTargetUnit;
        Receiver.BattleLeaveSyncEvent -= BattleLeaveSync;
        Receiver.BattleBossSyncEvent -= BossClearSync;
        Receiver.ReadyEvent -= ReadyEvent;
        Receiver.CharacterEvent -= CreateCharacterEvent;
        //_waveController.CreateBossEvent -= CreateBossEvent;
        // Receiver.MergeEvent -= MergeEvent;
        Receiver.CopyUnitEvent -= CopyUnitEvent;
        Receiver.MergeUpgradeEvent -= MergeUpgradeEvent;
        Receiver.BattleInitUserDataEvent -= InitUserDataEvent;
        Receiver.UpgradeEvent -= UpgradeEvent;
        Receiver.SpawnCharacterEvent -= SpawnCharacterEvent;
        Receiver.BattleDisconnectEvent -= OnBattleDisconnect;
        Receiver.RequestReadyEvent -= OnRequestReadeyEvent;
        //_waveController.CreateMonsterEvent -= CreateMonsterEvent;
    }

    private void OnRequestReadeyEvent(RequestBattleRequestReadyData data)
    {
        if (IsAvailableData(data) == true)
            return;

        MsgInterface.SocketManager.EmitPlayReady(UserManager.IsReady);
    }

    private void OnBattleLife(RequestBattleLIfeData data)
    {
        if (IsAvailableData(data) == false)
            return;

        if (data.Dir > 0)
            UserManager.OnRecovery(data.LIfe);
        else if (data.Dir < 0)
            UserManager.OnHit(data.LIfe);
    }

    private void OnBattleDisconnect(RequestBattleDisconnectData data)
    {
        if (IsAvailableData(data) == false)
            return;

        BattleDisconnectEvent?.Invoke();
    }

    private void OnTouchTargetUnit(GameUnit unit)
    {
        if (unit == null)
            return;

        if (ReceiverType != SocketDataType.Local)
            return;

        var unitData = EightUtil.GetCore<GameUnitDataMgr>().GetCharacaterData(unit.UnitID);
        if (unitData == null)
            return;

        var tileList = _tileSlotMgr.TilePool.TileList;
        foreach (var userTile in tileList)
        {
            userTile.SetGradationColor(_touchController.OnTargetColor);

            var tileUnit = userTile.Owner;
            if (tileUnit == null)
                continue;

            var mergeType = MergeTrigger.GetMergeType(unit, tileUnit);
            if (unitData.MergeTriggerData.IsHaveSkill(mergeType) == false)
            {
                tileUnit.SpineUnit.SkinColor = _touchController.OnTargetColor;
            }

            if (tileUnit == unit)
            {
                userTile.TileEffectImage = unitData.MergeSelectData.GetMergeSelectSprite(GameMergeSelectData.MergeSelectType.Select);
            }
            else if (tileUnit.UnitID == unit.UnitID && tileUnit.UnitStep == unit.UnitStep)
            {
                userTile.TileEffectImage = unitData.MergeSelectData.GetMergeSelectSprite(GameMergeSelectData.MergeSelectType.AllSameMerge);
            }
            else if (tileUnit.UnitID == unit.UnitID)
            {
                userTile.TileEffectImage = unitData.MergeSelectData.GetMergeSelectSprite(GameMergeSelectData.MergeSelectType.IdSameMerge);
            }
            else if (tileUnit.UnitStep == unit.UnitStep)
            {
                userTile.TileEffectImage = unitData.MergeSelectData.GetMergeSelectSprite(GameMergeSelectData.MergeSelectType.StepSameMerge);
            }
        }
    }

    private void OnTouchTargetOff()
    {
        if (ReceiverType != SocketDataType.Local)
            return;

        var tileList = _tileSlotMgr.TilePool.TileList;
        foreach (var userTile in tileList)
        {
            userTile.TileEffectImage = null;
            userTile.SetGradationColor(_touchController.DefaultColor);
            if (userTile.Owner == null)
                continue;
            userTile.Owner.SpineUnit.SkinColor = _touchController.DefaultColor;
        }
    }

    private void BattleLeaveSync(RequestBattleLeaveSyncData data)
    {
        if (IsAvailableData(data) == false)
            return;

        BattleLeaveSyncEvent?.Invoke();
    }

    private void BossClearSync(RequestBattleBossSyncData data)
    {
        if (IsAvailableData(data) == false)
            return;

        SyncBossEvent?.Invoke(data.SyncClear);
    }

    private void InitUserDataEvent(RequestBattleInitUserData data)
    {
        var userData = data.GetUserInfo(ReceiverType);
        if (userData == null)
            return;

        _userManager?.InitUserInfo(userData.UnitList, userData.NickName, userData.Trophy, userData.UserLevel);
    }

    private void ReadyEvent(RequestReadyData data)
    {
        if (IsAvailableData(data) == false)
            return;

        _userManager.IsReady = data.IsReady;
    }

    private void CreateCharacterEvent(RequestUnitData data)
    {
        if (IsAvailableData(data) == false)
            return;

        //CreateCharacter(data.UnitId, data.Grade, data.RandomSeed, data.SlotIndex);

        OnCreateCharacterEvent?.Invoke(data);
    }

    private void SpawnCharacterEvent(RequestUnitData data)
    {
        if (IsAvailableData(data) == false)
            return;

        _characterSpawner.OnSpawn(new SpawnUnitData(data.UnitId, data.SlotIndex, data.Grade, data.RandomSeed));
        //OnSpawnCharacterEvent?.Invoke(data);
        //CreateCharacter(data.UnitId, data.Grade, data.RandomSeed, data.SlotIndex);
    }

    private void UpgradeEvent(RequestUpgradeData data)
    {
        if (IsAvailableData(data) == false)
            return;

        UpgradeUnitEvent?.Invoke(data);
    }

    private void MergeCreateEvent(RequestMergeCreateData data)
    {
        if (IsAvailableData(data) == false)
            return;

        var mergeUnit = ((UserTile)_tileSlotMgr.GetTileSlot(data.MergeSlotId)).Owner;
        var targetUnit = ((UserTile)_tileSlotMgr.GetTileSlot(data.TargetSlotId)).Owner;

        targetUnit.TileSlotManager.ReturnSlot(targetUnit);
        EightUtil.GetCore<GameUnitMgr>().ReturnUnit(targetUnit);

        mergeUnit.TileSlotManager.ReturnSlot(mergeUnit);
        EightUtil.GetCore<GameUnitMgr>().ReturnUnit(mergeUnit);

        var deckList = UserManager.UserData.UnitList;
        if (deckList == null || deckList.Length <= 0)
            return;

        if (ReceiverType == SocketDataType.Local)
        {
            var slot = TileSlotMgr.ReserveSlot(data.MergeSlotId);
            if (slot == null)
                return;
        }

        _characterSpawner.OnSpawn(new SpawnUnitData(data.UnitId, data.MergeSlotId, data.Grade, data.RandomSeed));
    }

    private void MergeUpgradeEvent(RequestMergeUpgradeData data)
    {
        if (IsAvailableData(data) == false)
            return;

        var mergeUnit = ((UserTile)_tileSlotMgr.GetTileSlot(data.MergeSlotId)).Owner;
        var targetUnit = ((UserTile)_tileSlotMgr.GetTileSlot(data.TargetSlotId)).Owner;

        targetUnit.TileSlotManager.ReturnSlot(targetUnit);
        EightUtil.GetCore<GameUnitMgr>().ReturnUnit(targetUnit);

        mergeUnit.TileSlotManager.ReturnSlot(mergeUnit);
        EightUtil.GetCore<GameUnitMgr>().ReturnUnit(mergeUnit);

        if (ReceiverType == SocketDataType.Local)
        {
            var slot = TileSlotMgr.ReserveSlot(data.MergeSlotId);
            if (slot == null)
                return;
        }

        _characterSpawner.OnSpawn(new SpawnUnitData(data.UnitId, data.MergeSlotId, data.Grade, data.RandomSeed));
        //MsgInterface.SpawnCharacter(data.UnitId, data.Grade, slot.slotIndex, this);

        //var unit = EightUtil.GetCore<GameUnitMgr>().CreateCharacater<CharacterController>(data.UnitId, data.Grade, data.RandomSeed, this);
        //if (unit == null)
        //    return;

        //TileSlot slot = _tileSlotMgr.UseSlot(data.MergeSlotId, unit);
        //if (slot == null)
        //    return;

        //unit.transform.position = (Vector2)_tileSlotMgr.GetTileSlot(slot.slotIndex).GetPositionForUnit();
        //unit.CurrTileIndex = slot.slotIndex;
    }

    private void CopyUnitEvent(RequestCopyUnitData data)
    {
        if (IsAvailableData(data) == false)
            return;

        var targetUnit = ((UserTile)_tileSlotMgr.GetTileSlot(data.TargetSlotId)).Owner;

        targetUnit.TileSlotManager.ReturnSlot(targetUnit);
        EightUtil.GetCore<GameUnitMgr>().ReturnUnit(targetUnit);

        if (ReceiverType == SocketDataType.Local)
        {
            var slot = TileSlotMgr.ReserveSlot(data.TargetSlotId);
            if (slot == null)
                return;
        }

        _characterSpawner.OnSpawn(new SpawnUnitData(data.UnitId, data.TargetSlotId, data.Grade, data.RandomSeed));

        //var unit = EightUtil.GetCore<GameUnitMgr>().CreateCharacater<CharacterController>(data.UnitId, data.Grade, data.RandomSeed, this);
        //if (unit == null)
        //    return;

        //TileSlot slot = _tileSlotMgr.UseSlot(data.TargetSlotId, unit);
        //if (slot == null)
        //    return;

        //unit.transform.position = (Vector2)_tileSlotMgr.GetTileSlot(slot.slotIndex).GetPositionForUnit();
        //unit.CurrTileIndex = slot.slotIndex;
    }

    //private void MergeEvent(RequestMergeData data)
    //{
    //    if (IsAvailableData(data) == false)
    //        return;

    //    if (_tileSlotMgr.IsUseSlot(data.TargetSlotId) == false ||
    //        _tileSlotMgr.IsUseSlot(data.MergeSlotId) == false)
    //        return;

    //    var mergeUnit = ((UserTile)_tileSlotMgr.GetTileSlot(data.MergeSlotId)).Owner;
    //    var targetUnit = ((UserTile)_tileSlotMgr.GetTileSlot(data.TargetSlotId)).Owner;

    //    targetUnit.OnMerge(targetUnit, mergeUnit);
    //}

    private void CreateMonsterEvent(SpawnData data, int stage)
    {

    }

    private void CreateBossEvent(BossSpawnData data, int stage)
    {

    }
}
