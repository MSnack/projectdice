﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

using UnityEngine.AddressableAssets;

public class TestUnitCreator : EightReceiver
{
    [SerializeField]
    private int _useSkin = 1;

    [SerializeField]
    private AssetLabelReference _lableSample = null;

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
    }

    public void CreateUnitButton()
    {
        GameUnit unit = EightUtil.GetCore<GameUnitMgr>().CreateCharacater<CharacterController>(1, 0, 0, null);
        if (unit == null)
            return;

        unit.transform.SetParent(this.transform);
        unit.gameObject.SetActive(true);

        Vector3 pos = new Vector3(Random.Range(0.0f, 720.0f), Random.Range(0.0f, 1280.0f));
        unit.transform.position = pos;
        //var character = EightUtil.GetCore<GameUnitMgr>().CreateCharacater<CharacterController>(1);
        //character.transform.SetParent(this.transform);
        //character.transform.gameObject.SetActive(true);

        //Vector3 pos = new Vector3(Random.Range(0.0f, 720.0f), Random.Range(0.0f, 1280.0f));
        //character.transform.position = pos;
    }
}
