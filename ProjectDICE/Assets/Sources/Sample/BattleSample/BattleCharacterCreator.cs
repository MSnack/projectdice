﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using EightWork.MsgSystem;

public class BattleCharacterCreator : EightMsgReceiver<BattleSceneInterface>
{
    [SerializeField]
    private BattleManager _battleManager = null;

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }

    public void OnCreateUnit()
    {
        if (EightUtil.IsLoadCompleteDevice == false)
            return;

        if (_battleManager.UserManager.IsPossibleCreateUnit == false)
            return;

        if (_battleManager.TileSlotMgr.IsHaveEmptySlot == false)
            return;

        var deckList = _battleManager.UserManager.UserData.UnitList;
        if (deckList == null || deckList.Length <= 0)
            return;

        int deckIndex = Random.Range(0, deckList.Length);
        int charId = deckList[deckIndex].UnitID;

        MsgInterface.CreateCharacter(charId, 0, _battleManager);
    }
}
