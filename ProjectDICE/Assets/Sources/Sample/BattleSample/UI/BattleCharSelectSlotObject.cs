﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;

public class BattleCharSelectSlotObject : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField]
    private Image _unitImage = null;

    [SerializeField]
    private Image _cardFrameImage = null;

    [SerializeField]
    private Image _cardStarImage = null;

    [SerializeField]
    private Text _upgradeLevelText = null;

    [SerializeField]
    private Text _priceText = null;

    private int _price = 0;

    private bool _receiveUpgrade = false;
    public bool ReceiveUpgrade => _receiveUpgrade;

    [SerializeField]
    private Image _targetImage = null;

    [SerializeField]
    private float _scaleDuration = 0.1f;

    [SerializeField]
    private float _targetScale = 0.9f;

    [SerializeField]
    private float _defaultScale = 1.0f;

    private Vector3 _saveScale = Vector3.one;
    private float _currScale = 1.0f;

    private bool _isReserve = false;

    private Coroutine _scaleRoutine = null;
    private Coroutine _touchRoutine = null;

    private bool _isClick = false;
    private bool _isBlock = false;
    public bool IsBlock => _isBlock;

    private bool _isMax = false;
    public bool IsMax => _isMax;

    [SerializeField]
    private Color32 _offColor;

    [SerializeField]
    private Color32 _onColor;

    [SerializeField]
    private UnityEvent OnClickEvent = null;

    private void Awake()
    {
        _saveScale = transform.localScale;
        _currScale = _defaultScale;
        transform.localScale = _currScale * _saveScale;
        _isReserve = false;
        _isBlock = true;
    }

    public void OnInitSlot(BattleCardData data)
    {
        if (data == null)
            return;

        data.UpdateEvent += UpdateCardSlot;
        UpdateCardSlot(data);
    }

    public void ReserveSlot()
    {
        _receiveUpgrade = true;
    }

    public void UpdateCardSlot(BattleCardData data)
    {
        var unitData = EightUtil.GetCore<GameUnitDataMgr>().GetCharacaterData(data.UnitID);
        if (unitData == null)
            return;

        _unitImage.sprite = unitData.UnitPreviewSprite;
        _price = data.Reinforce;
        _upgradeLevelText.text = _price.ToString();
        int grade = unitData.Grade;
        _cardFrameImage.sprite = LobbyCardUtil.GetFrameSprite(grade - 1);
        _cardStarImage.sprite = LobbyCardUtil.GetStarSprite(grade - 1);

        int needSp = -1;
        if (unitData.ReinforceData.DataList.Length > data.Reinforce)
            needSp = unitData.ReinforceData.DataList[data.Reinforce];

        if (needSp == -1)
        {
            _priceText.text = "Max";
            OffSlot();
            _isMax = true;
        }
        else
            _priceText.text = needSp.ToString();

        _receiveUpgrade = false;
    }

    public void OnSlot()
    {
        if (_isMax == true)
            return;

        SetColor(_onColor);
        _isBlock = false;
    }

    private void SetColor(Color32 color)
    {
        var imageList = GetComponentsInChildren<Image>(true);
        foreach (var image in imageList)
        {
            if (image.sprite == null)
                continue;

            image.color = color;
        }
    }

    public void OffSlot()
    {
        SetColor(_offColor);
        _isBlock = true;
    }

    private IEnumerator ScaleRoutine()
    {
        while (true)
        {
            var upScale = Mathf.Lerp(_targetScale, _defaultScale, Time.deltaTime / _scaleDuration);
            if (_isReserve == false)
            {
                _currScale -= upScale;
                if (_currScale <= _targetScale)
                    _currScale = _targetScale;
            }
            else
            {
                _currScale += upScale;
                if (_currScale >= _defaultScale)
                    _currScale = _defaultScale;
            }

            var scale = _saveScale * _currScale;
            transform.localScale = scale;
            yield return null;
        }

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Left ||
            eventData.pointerEnter != _targetImage.gameObject ||
            _isBlock == true)
            return;

        if (_scaleRoutine == null)
        {
            _scaleRoutine = StartCoroutine(ScaleRoutine());
        }
        _isReserve = false;
        _isClick = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Left ||
            _isClick == false)
            return;

        if (eventData.pointerEnter == _targetImage.gameObject)
        {
            OnClickEvent?.Invoke();
        }

        _isReserve = true;
        _isClick = false;
    }
}
