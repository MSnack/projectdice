﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork.FSM;
using UnityEngine;

public class SampleFSMSystem : EightFSMSystem<SampleFSMSystem.SampleFSMState, SampleStateBase, SampleFSMInterface>
{
    public enum SampleFSMState
    {
        None,
        A,
    }

    protected override void InitPool()
    {
        base.InitPool();
    }

    protected override void StartEvent()
    {
        base.StartEvent();
    }

    protected override void Start()
    {
        base.Start();
    }
}
