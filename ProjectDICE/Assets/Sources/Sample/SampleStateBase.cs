﻿using System.Collections;
using System.Collections.Generic;
using EightWork.FSM;
using UnityEngine;

public abstract class SampleStateBase : EightFSMStateBase<SampleFSMSystem.SampleFSMState, SampleFSMSystem>
{
    [SerializeField]
    protected GameObject[] _activeObjects;

    [SerializeField]
    protected GameObject[] _inactiveObjects;


    public override void StartState()
    {
        SampleFSMInterface.StateChanged.Invoke(State);

        foreach (var obj in _activeObjects)
        {
            obj.SetActive(true);
        }
        
        foreach (var obj in _inactiveObjects)
        {
            obj.SetActive(false);
        }
    }
}
