﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

using UnityEngine.AddressableAssets;

public class SampleEffect : EightReceiver
{
    [SerializeField]
    private AssetReference _effectData = null;

    [SerializeField, ReadOnly]
    private GameEffectData _data = null;

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();

        _effectData.LoadAssetAsync<GameEffectData>().Completed += (result) =>
        {
            _data = result.Result;
            var mgr = EightUtil.GetCore<GameEffectMgr>();
            //mgr.UploadEffectAsset(_data);
            mgr.UseEffect(_data);
        };
    }

    public void UseEffect()
    {
        if (_data == null || EightUtil.IsLoadCompleteDevice == false)
            return;

        EightUtil.GetCore<GameEffectMgr>().UseEffect(_data);
    }
}
