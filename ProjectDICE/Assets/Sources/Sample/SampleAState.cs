﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAState : SampleStateBase
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void StartState()
    {
        base.StartState();
    }

    protected override void Initialize()
    {
        State = SampleFSMSystem.SampleFSMState.A;
    }

    public override void EndState()
    {
    }
}
