﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using socket.io;
using EightWork;
using UnityEngine.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class ChatSample : EightReceiver
{
    [SerializeField, ReadOnly]
    private Socket _socket = null;

    [SerializeField]
    private InputField _nickNameText = null;

    [SerializeField]
    private InputField _sendField = null;

    [SerializeField]
    private Text _msgText = null;

    [SerializeField]
    private GameObject _login = null;

    [SerializeField]
    private GameObject _chat = null;

    private bool _isLogin = false;

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        _socket = Socket.Connect("http://192.168.0.19:3000/");

        _socket.On(SystemEvents.connect, () =>
        {
            Debug.Log("Socket Connect!");

            JObject obj = new JObject();
            obj["token"] = EightUtil.GetCore<GameWebServerMgr>().ServerToken;

            _socket.EmitJson("login", obj.ToString());
        });

        _socket.On("login", (string data) =>
        {
            Debug.Log(data);
            _isLogin = true;

            _socket.Emit("matching");
        });

        //_socket.On("login", () =>
        //{
        //    Debug.Log("Login!");
        //    _isLogin = true;
        //});

        _socket.On("sendMsg", (string msg) =>
        {
            _msgText.text += "\n";
            string data = msg.Substring(1, msg.Length - 2);
            _msgText.text += data;
        });

        _msgText.text = null;
    }

    public void OnLogin()
    {
        if (_isLogin == false)
            return;

        JObject obj = new JObject();
        obj["roomId"] = 1;
        Debug.Log(obj.ToString());
        _socket.EmitJson("joinRoom", obj.ToString());

        _socket.Emit("sendMsg", _nickNameText.text + " is Join!");

        _login.SetActive(false);
        _chat.SetActive(true);
    }

    public void OnSendMsg()
    {
        string sendMsg = _nickNameText.text + " : " + _sendField.text;
        _socket.Emit("sendMsg", sendMsg);

        _sendField.text = "";
    }

    public void Logout()
    {
        _socket.Emit("sendMsg", _nickNameText.text + " is Leaved!");
        _socket.Emit("leaveRoom");

        Debug.Log("Logout");

        _nickNameText.text = null;

        _login.SetActive(true);
        _chat.SetActive(false);
    }
}
