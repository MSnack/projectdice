﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SampleChangeStateButton : MonoBehaviour
{

    [SerializeField]
    private SampleFSMSystem.SampleFSMState _changeState = SampleFSMSystem.SampleFSMState.None;

    [SerializeField]
    private SampleFSMSystem _fsmSystem = null;

    private Button _bt = null;
    
    // Start is called before the first frame update
    void Start()
    {
        _bt = GetComponent<Button>();
        _bt.onClick.AddListener(() => _fsmSystem.ChangeState(_changeState));
    }
    
}
