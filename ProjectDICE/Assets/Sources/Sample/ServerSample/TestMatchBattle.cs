﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestMatchBattle : MonoBehaviour
{
    [SerializeField]
    private GameObject _matchingPanel = null;

    [SerializeField]
    private Button _matchingButton = null;

    [SerializeField]
    private MatchSocket _matchSocket = null;

    public void OnMathcing()
    {
        if (GameSocketReceiver.IsLogin == false)
            return;

        _matchingPanel.SetActive(true);
        _matchingButton.gameObject.SetActive(false);

        _matchSocket.Matching();
    }

    public void OnMatchOut()
    {
        if (GameSocketReceiver.IsLogin == false)
            return;

        _matchingPanel.SetActive(false);
        _matchingButton.gameObject.SetActive(true);

        _matchSocket.MatchOut();
    }
}
