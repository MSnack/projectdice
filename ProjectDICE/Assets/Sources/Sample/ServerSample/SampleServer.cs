﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using socket.io;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using EightWork;
using Firebase.Auth;
using System.Threading.Tasks;
using UnityEngine.Networking;

public class SampleServer : MonoBehaviour
{
    [SerializeField]
    private string _serverUrl = null;

    [SerializeField]
    private Socket socket = null;

    [SerializeField, ReadOnly]
    private string _firebaseToken = null;

    [SerializeField, ReadOnly]
    private FirebaseAuth _firebaseAuth = null;

    [SerializeField, ReadOnly]
    private string _webServerToken = null;

    [System.Serializable]
    private class EightLogin
    {
        public int userId;
        public string UId;
        public string nickName;
        public string token;
    }

    [SerializeField, ReadOnly]
    private EightLogin userInfo = null;

    private void Awake()
    {
        _firebaseAuth = FirebaseAuth.DefaultInstance;
    }

    private void Start()
    {
        FirebaseLogin();
        EightWebLogin();
    }

    private void EightWebLogin()
    {
        StartCoroutine(EightLoginRoutine());
    }

    private IEnumerator EightLoginRoutine()
    {
        yield return new WaitUntil(() => EightUtil.IsLoadCompleteDevice == true);
        yield return new WaitUntil(() => _firebaseToken != null);
    }

    private void Request(string jsonData)
    {
        Debug.Log(jsonData);
        userInfo = JsonConvert.DeserializeObject<EightLogin>(jsonData);
        _webServerToken = userInfo.token;

        SocketServer();
    }

    private bool _successFirebaseToken = false;
    private void FirebaseLogin()
    {
        this.SignInAnonymous().ContinueWith(task =>
        {
            _firebaseAuth.CurrentUser.TokenAsync(true).ContinueWith(token =>
            {
                _firebaseToken = token.Result;
                Debug.Log("idToken : " + _firebaseToken);
                _successFirebaseToken = true;
            });
        });

        StartCoroutine(LoginRotuine());
    }

    public void OnLoginEvent()
    {
        if (_isConnect == false)
            return;

        JObject loginObject = new JObject();
        loginObject["token"] = userInfo.token;
        var loginJson = JsonConvert.SerializeObject(loginObject);
        Debug.Log(loginJson);

        socket.EmitJson("login", loginJson, (string data) =>
        {
            Debug.Log(data);
        });
    }

    private bool _isConnect = false;
    private void SocketServer()
    {
        socket = Socket.Connect(_serverUrl);

        socket.On(SystemEvents.connect, () =>
        {
            Debug.Log("connection");
            _isConnect = true;

            JObject loginObject = new JObject();
            loginObject["token"] = userInfo.token;
            var loginJson = JsonConvert.SerializeObject(loginObject);
            Debug.Log(loginJson);

            socket.EmitJson("test", loginJson);

            socket.EmitJson("login", loginJson);

            loginObject.RemoveAll();

            loginObject["roomId"] = 1;
            var roomData = JsonConvert.SerializeObject(loginObject);
            Debug.Log(roomData);

            socket.EmitJson("joinRoom", roomData, (string room) =>
            {
                Debug.Log(room);
            });

            socket.On("sendMsg", (string msg) =>
            {
                Debug.Log(msg);
            });

            socket.Emit("sendMsg", "AllSendMsg");
        });

        socket.On("disconnect", () =>
        {
            Debug.Log("disconnect");
        });

        //JObject loginObject = new JObject();
        //loginObject["token"] = userInfo.token;

        //socket.Emit("login", JsonConvert.SerializeObject(loginObject), (string data) =>
        //{
        //    Debug.Log("Login : " + data);

        //    JObject roomJson = new JObject();
        //    roomJson["roomId"] = 1;
        //    socket.Emit("joinRoom", JsonConvert.SerializeObject(roomJson), (string room) =>
        //    {
        //        Debug.Log("joinRoom : " + room);
        //    });

        //    socket.Emit("sendMsg", "TestData", (string msg) =>
        //    {
        //        Debug.Log("sendMsg : " + "TestData");
        //    });

        //    socket.On("sendMsg", (string msg) =>
        //    {
        //        Debug.Log("getMsg : " + msg);
        //    });

        //    socket.On("leaveRoom", () =>
        //    {
        //        Debug.Log("leaveRoom");
        //    });
        //});
    }

    IEnumerator LoginRotuine()
    {
        while (_successFirebaseToken == false) { yield return null; }

        UnityWebRequest www = UnityWebRequest.Post("http://localhost:3000/login/firebase/in/" + _firebaseToken, "");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            Debug.Log("Form upload complete!");
        }
    }

    public async Task SignInAnonymous()
    {
        await _firebaseAuth.SignInAnonymouslyAsync().ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInAnonymouslyAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });
    }
}
