﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

[System.Serializable]
public class TestDeck
{
    public int userId = -1;
    public int deckIndex = -1;
    public int deck1 = -1;
    public int deck2 = -1;
    public int deck3 = -1;
    public int deck4 = -1;
    public int deck5 = -1;
}

public class TestBatlteInfo
{
    public int userId = -1;
    public int win = -1;
    public int lose = -1;
    public int rating = -1;
}

public class WebSample : EightReceiver
{
    private GameWebRequestTranslator tranlator = new GameWebRequestTranslator();

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();

        StartCoroutine(CheckInit());
    }

    private IEnumerator CheckInit()
    {
        while (EightUtil.GetCore<GameUserMgr>().IsCompleteLogin == false)
        {
            yield return null;
        }
        //yield return new WaitUntil(() => EightUtil.GetCore<GameUserMgr>().IsComplateLogin == true);

        var userInfo = EightUtil.GetCore<GameUserMgr>().UserInfo;

        //tranlator.SetSendData("userId", userInfo.UserID);
        //tranlator.SetSendData("deckIndex", 1);
        //string query = string.Format("/{0}/{1}", userInfo.UserID, 1);

        tranlator.Request(GameWebRequestType.Get_UserBattleInfo, Request);
    }

    private void Request(GameWebRequestTranslator trans)
    {
        //var test = trans.GetRequestData<TestDeck>();
        //Debug.Log(test);

        //test.deck1 = 1;
        //test.deck2 = 1;
        //test.deck3 = 1;
        //tranlator.SetSendDataAtClass(test);

        var test = tranlator.GetRequestData<GameBattleInfo>();
        Debug.Log(JsonConvert.SerializeObject(test));

        //Debug.Log(JsonConvert.SerializeObject(test));
        //tranlator.Request(GameWebRequestType.Update_UserDeckInfo, UpdateRequest);
    }

    private void UpdateRequest(GameWebRequestTranslator trans)
    {
        Debug.Log("Update Success");
    }
}
