﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using socket.io;
using EightWork;
using UnityEngine.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using UnityEngine.Events;

public class MatchingSample : EightReceiver
{
    [SerializeField, ReadOnly]
    private Socket _socket = null;

    [SerializeField, ReadOnly]
    private bool _isLogin = false;

    [SerializeField, ReadOnly]
    private bool _isMatching = false;

    [SerializeField, ReadOnly]
    private int _battleRoomId = -1;

    [SerializeField]
    private UnityEvent _successMatching = null;

    [SerializeField]
    private UnityEvent _returnMatchRoom = null;

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();

        _socket = Socket.Connect("http://192.168.0.19:3000/");
        Debug.Log("Connect");

        _socket.On(SystemEvents.connect, () =>
        {
            Debug.Log("Socket Connect!");

            JObject obj = new JObject();
            obj["token"] = EightUtil.GetCore<GameWebServerMgr>().ServerToken;

            _socket.EmitJson("login", obj.ToString());
        });

        _socket.On("login", (string data) =>
        {
            Debug.Log(data);
            _isLogin = true;

            //_socket.Emit("matching");
        });

        _socket.On("JoinBattleRoom", (string id) =>
        {
            _battleRoomId = int.Parse(id);
            _successMatching.Invoke();

            _socket.Off("JoinBattleRoom");
        });
    }

    public void Matching()
    {
        if (_isLogin == false || 
            _isMatching == true)
            return;

        _socket.Emit("matching");
        _isMatching = true;
    }

    public void MatchOut()
    {
        if (_isLogin == false || _isMatching == false)
            return;

        _socket.Emit("matchOut");
        _isMatching = false;
    }
}
