﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleController : MonoBehaviour
{
    [Header("Charcter")]

    [SerializeField]
    private UserTilePool _userTilePool = null;


    [Header("Monster")]

    [SerializeField]
    private MonsterContainerMgr _monsterContainerMgr = null;

    [SerializeField]
    private MonsterTilePool _monsterTilePool = null;
}
