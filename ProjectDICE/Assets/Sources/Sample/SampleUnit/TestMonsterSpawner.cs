﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class TestMonsterSpawner : EightReceiver
{
    [SerializeField]
    private TileRoadMgr _tileRoadMgr = null;

    [SerializeField]
    private TileSlotMgr _tileSlotMgr = null;

    [SerializeField]
    private BattleUserManager _userManager = null;

    [SerializeField]
    private float _delay = 3.0f;

    [SerializeField, ReadOnly]
    private float _currTime = 3.0f;

    [SerializeField]
    private CharacterUnitData _spawnData;

    [SerializeField]
    private MonsterUnitData _monster;

    [SerializeField, ReadOnly]
    private bool _isReady = false;

    private void Awake()
    {
        SetupComplete();
        CheckMapDist();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();

        StartCoroutine(ReadyCheck());
    }

    private IEnumerator ReadyCheck()
    {
        EightUtil.GetCore<GameUnitDataMgr>().LoadGameUnitData();
        yield return new WaitUntil(() => EightUtil.GetCore<GameUnitDataMgr>().IsLoadCompleted == true);
        EightUtil.GetCore<GameSkinMgr>().LoadData();
        yield return new WaitUntil(() => EightUtil.GetCore<GameSkinMgr>().IsLoadComplete == true);
        EightUtil.GetCore<GameSkinMgr>().UploadAllSkin();
        yield return new WaitUntil(() => EightUtil.GetCore<GameSkinMgr>().IsAllUpload == true);
        _isReady = true;
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        StartCoroutine(SpawnTimmer());
    }

    private IEnumerator SpawnTimmer()
    {
        while (true)
        {
            if (_isReady == false)
                yield return new WaitUntil(() => _isReady == true);

            _currTime += Time.deltaTime;
            if (_currTime >= _delay)
            {
                if (CreateMonster() == false)
                    yield return new WaitUntil(() => CreateMonster() == true);
                _currTime -= _delay;
            }

            yield return null;
        }
    }

    public void CheckMapDist()
    {
        float dist = 0.0f;
        Vector3 lastPos = _tileRoadMgr.SpawnTile.GetPositionForUnit();
        foreach (var tile in _tileRoadMgr.TileRoadList)
        {
            var currPos = tile.GetPositionForUnit();
            float nowDist = (currPos - lastPos).magnitude;
            dist += nowDist;
            lastPos = currPos;
        }

        Debug.Log(dist);
    }

    private bool CreateMonster()
    {
        if (_monster == null)
            return false;

        var unit = EightUtil.GetCore<GameUnitMgr>().CreateMonster<MonsterController>(_monster.DataID, new BattleMonsterSpawner.MonsterInitData(100, 0, 100, 0, BattleMonsterData.MonsterType.Spawn), null,_tileRoadMgr, _tileSlotMgr, _userManager, null);
        if (unit == null)
            return false;

        unit.transform.position = _tileRoadMgr.SpawnTile.GetPositionForUnit();

        return true;
    }

    public void CreateCharacter()
    {
        if (EightUtil.IsLoadCompleteDevice == false)
            return;

        if (_spawnData == null)
            return;

        var unit = EightUtil.GetCore<GameUnitMgr>().CreateCharacater<CharacterController>(_spawnData.DataID, 0, 0,  _tileRoadMgr, _tileSlotMgr, _userManager, null);
        if (unit == null)
            return;

        var slot = _tileSlotMgr.ReserveRandomSlot();
        _tileSlotMgr.UseReserveSlot(slot.slotIndex, unit);
        if (slot == null)
            return;

        unit.transform.position = (Vector2)_tileSlotMgr.GetTileSlot(slot.slotIndex).GetPositionForUnit();
        unit.CurrTileIndex = slot.slotIndex;
        //_tileSlotMgr.RegisterSlot(unit);
    }
}
