﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using Firebase.Auth;

public class TileRoadMgr : MonoBehaviour
{
    [SerializeField]
    private MonsterTilePool _tilePool = null;
    public TileObjectBase[] TileRoadList => _tilePool.TilePath;

    public TileObjectBase SpawnTile => _tilePool.SpawnTile;
    public TileObjectBase GoalTile => _tilePool.GoalTile;

    private List<GameUnit> _tileUnitList = new List<GameUnit>();
    public int TileUnitCount => _tileUnitList.Count;

    [SerializeField]
    private BattleUserManager _battleUserManager = null;

    public GameUnit[] TileUnitList => _tileUnitList.ToArray();
    public GameUnit[] TileUnitListAtLive
    {
        get
        {
            List<GameUnit> liveList = new List<GameUnit>();
            foreach (var unit in _tileUnitList)
            {
                if (unit.UnitState == GameUnitState.Die ||
                    unit.UnitState == GameUnitState.Spawn)
                    continue;

                liveList.Add(unit);
            }

            return liveList.ToArray();
        }
    }

    private void Start()
    {
        _battleUserManager.UpdateLifeEvent += (hp, state) =>
        {
            if(state < 0) ((MonsterGoalTile) _tilePool.GoalTile).SetHitEvent();
        };
        
    }

    public void ClearRoadUnit()
    {
        foreach (var unit in _tileUnitList)
        {
            unit.OnUnitDie();
            //EightUtil.GetCore<GameUnitMgr>().ReturnUnit(unit);
        }

        //_tileUnitList.Clear();
    }

    private List<GameEffectObject> _roadEffect = new List<GameEffectObject>();
    public bool IsAlreadyRoadEffectID(int id)
    {
        foreach (var effect in _roadEffect)
            if (effect.AutoID == id)
                return true;

        return false;
    }

    public GameEffectObject CreateRoadEffect(GameEffectData data)
    {
        var useEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(data);
        if(useEffect == null)
            return null;

        Vector3 effectPos = new Vector3(0.0f, 0.0f, 0.0f);

        if (_tilePool.transform.parent.position.y >= 0)
            effectPos.y = 400.0f;
        else
            effectPos.y = -220.0f;

        useEffect.transform.position = effectPos;

        useEffect.Clear();
        useEffect.ReStart();

        _roadEffect.Add(useEffect);

        return useEffect;
    }

    public void ReturnRoadEffect(GameEffectObject effect)
    {
        if(_roadEffect.Remove(effect))
            EightUtil.GetCore<GameEffectMgr>().ReturnEffect(effect);
    }

    private Dictionary<int, List<GameEffectObject>> _roadTileEffect = new Dictionary<int, List<GameEffectObject>>();
    public bool IsAlreadyRoadTileEffectID(int tileId, int id)
    {
        if (_roadTileEffect.ContainsKey(tileId) == true)
        {
            foreach (var effect in _roadTileEffect[tileId])
            {
                if (effect.AutoID == id)
                    return true;
            }
        }

        return false;
    }

    public GameEffectObject CreateRoadTileEffect(int tileId, GameEffectObject effect)
    {
        var useEffect = effect;
        if (useEffect == null)
            return null;

        if (_roadTileEffect.ContainsKey(tileId) == false)
            _roadTileEffect.Add(tileId, new List<GameEffectObject>());

        _roadTileEffect[tileId].Add(useEffect);

        return useEffect;
    }

    public void ReturnRoadTileEffect(int tileId, GameEffectObject effect)
    {
        if (_roadTileEffect.ContainsKey(tileId) == false)
            return;

        _roadTileEffect[tileId].Remove(effect);

        if (_roadTileEffect[tileId].Count == 0)
            _roadTileEffect.Remove(tileId);
    }

    public bool IsUseUnit
    {
        get
        {
            bool isUseUnit = false;
            foreach (var unit in _tileUnitList)
            {
                if (unit.UnitState == GameUnitState.Die)
                    continue;

                isUseUnit = true;
            }

            return isUseUnit;
        }
    }

    private GameUnit _firstUnit = null;
    public GameUnit FirstUnit => _firstUnit;

    private GameUnit _strongUnit = null;
    public GameUnit StrongUnit => _strongUnit;

    private GameUnit _weakUnit = null;
    public GameUnit WeakUnit => _weakUnit;

    private void OnEnable()
    {
        StartCoroutine(UpdateUnit());
    }

    public void RegisterUnit(GameUnit unit)
    {
        unit.OnReleaseEvent += ReleaseUnit;
        _tileUnitList.Add(unit);
    }

    private void ReleaseUnit(GameUnit unit)
    {
        _tileUnitList.Remove(unit);
        unit.OnReleaseEvent -= ReleaseUnit;
    }

    private IEnumerator UpdateUnit()
    {
        while (true)
        {
            if (_tileUnitList.Count <= 0)
            {
                yield return new WaitForFixedUpdate();
                continue;
            }

            GameUnit firstUnit = null;
            GameUnit strongUnit = null;
            GameUnit weakUnit = null;

            var unitLIst = TileUnitListAtLive;
            foreach (var unit in unitLIst)
            {
                if (unit.UnitState == GameUnitState.Die)
                    continue;

                firstUnit = CheckFirstUnit(firstUnit, unit);
                strongUnit = CheckStrongUnit(strongUnit, unit);
                weakUnit = CheckWeakUnit(weakUnit, unit);
            }

            _firstUnit = firstUnit;
            _strongUnit = strongUnit;
            _weakUnit = weakUnit;

            yield return new WaitForFixedUpdate();
        }
    }

    private GameUnit CheckFirstUnit(GameUnit last, GameUnit curr)
    {
        if (last == null)
            return curr;

        if (last.CurrTileIndex < curr.CurrTileIndex)
            return curr;

        Vector3 tilePos = Vector3.zero;
        if (last.CurrTileIndex == TileRoadList.Length - 1)
            tilePos = TileRoadList[last.CurrTileIndex].GetPositionForUnit();
        else
            tilePos = TileRoadList[last.CurrTileIndex+1].GetPositionForUnit();

        float lastUnitDist = (tilePos - last.transform.position).magnitude;
        float currUnitDist = (tilePos - curr.transform.position).magnitude;

        if (lastUnitDist > currUnitDist)
            return curr;

        return last;
    }

    private GameUnit CheckStrongUnit(GameUnit last, GameUnit curr)
    {
        if (last == null)
            return curr;

        if (last.Hp < curr.Hp)
            return curr;

        return last;
    }

    private GameUnit CheckWeakUnit(GameUnit last, GameUnit curr)
    {
        if (last == null)
            return curr;

        if (last.Hp > curr.Hp)
            return curr;

        return last;
    }

    private void OnDisable()
    {
        for (int i = _tileUnitList.Count - 1; i >= 0; --i)
        {
            ReleaseUnit(_tileUnitList[i]);
        }
        _tileUnitList.Clear();
    }
}
