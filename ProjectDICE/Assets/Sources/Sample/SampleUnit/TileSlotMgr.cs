﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using EightWork.MsgSystem;

public class TileSlot
{
    public GameUnit unit = null;
    public GameEffectObject effect = null;
    public int slotIndex = -1;
    public bool block = false;
}

public class TileSlotMgr : EightMsgReceiver<BattleSceneInterface>
{
    [SerializeField]
    private BattleManager _manager = null;

    [SerializeField]
    private UserTilePool _userTilePool = null;
    public UserTilePool TilePool => _userTilePool;
    //public TileObjectBase[] CharacterTileList => _userTilePool.TileList;

    public int TileSize => _userTilePool.TileList.Length;

    private List<TileSlot> _useSlot = new List<TileSlot>();
    public TileSlot[] UseSlotList => _useSlot.ToArray();

    private List<TileSlot> _reserveSlot = new List<TileSlot>();
    private List<TileSlot> _noneSlot = new List<TileSlot>();

    [SerializeField]
    private List<GameEffectDataAssetReference> _sumonEffectDataList = null;

    [SerializeField]
    private List<GameEffectDataAssetReference> _tileEffectDataList = null;

    [SerializeField]
    private GameEffectDataAssetReference _upgradeSlotEffect = null;

    [SerializeField]
    private GameEffectDataAssetReference _upgradeCharacterEffect = null;

    //[SerializeField]
    //private List<GameEffectData> _sumonEffectDatas = new List<GameEffectData>();

    //[SerializeField]
    //private List<GameEffectData> _tileEffectDatas = new List<GameEffectData>();

    private void Awake()
    {
        LoadEffect();
        for (int i = 0; i < TileSize; ++i)
        {
            var slot = new TileSlot();
            slot.unit = null;
            slot.slotIndex = i;

            _noneSlot.Add(slot);
        }
    }

    private void LoadEffect()
    {
        foreach (var effectAsset in _sumonEffectDataList)
        {
            effectAsset.LoadAssetAsync();
        }

        foreach (var effectAsset in _tileEffectDataList)
        {
            effectAsset.LoadAssetAsync();
        }

        _upgradeSlotEffect.LoadAssetAsync();
        _upgradeCharacterEffect.LoadAssetAsync();
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        _manager.UpgradeUnitEvent -= UpgradeUnitSlotEvent;

        for (int i = _useSlot.Count - 1; i >= 0; --i)
        {
            ReturnSlot(_useSlot[i].unit);
        }
    }

    public bool IsUseSlot(int slotIndex)
    {
        foreach (var slot in _useSlot)
        {
            if (slot.slotIndex == slotIndex)
                return true;
        }

        return false;
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        _manager.UpgradeUnitEvent += UpgradeUnitSlotEvent;
    }

    private void OnDestroy()
    {
        Release();
    }

    private void UpgradeUnitSlotEvent(RequestUpgradeData data)
    {
        var unitId = data.UnitId;
        foreach (var useSlot in _useSlot)
        {
            var unit = useSlot.unit;
            if (unit == null)
                continue;

            if (unit.UnitID != unitId)
                continue;

            var tile = GetTileSlot(useSlot.slotIndex);
            if (tile == null)
                continue;

            float xDir = 1.0f;
            if (_manager != null && _manager.ReceiverType == SocketDataType.Network)
            {
                xDir = -1.0f;
            }

            Vector2 tilePos = tile.GetPositionForUnit();
            int xLayer = (int)(tilePos.x * xDir);
            int yLayer = (int)(tilePos.y * -10.0f);

            var slotEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_upgradeSlotEffect.Asset as GameEffectData);
            if (slotEffect == null)
                continue;

            slotEffect.transform.position = tilePos;
            slotEffect.SortingOrder = 8000 + yLayer + xLayer;

            var characterEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_upgradeCharacterEffect.Asset as GameEffectData);
            if (characterEffect == null)
                continue;

            characterEffect.transform.position = tilePos;
            characterEffect.SortingOrder = 9500 + yLayer + xLayer;
        }
    }

    private void Release()
    {
        foreach (var effectAsset in _sumonEffectDataList)
        {
            effectAsset.ReleaseAsset();
        }

        foreach (var effectAsset in _tileEffectDataList)
        {
            effectAsset.ReleaseAsset();
        }

        _upgradeSlotEffect.ReleaseAsset();
        _upgradeCharacterEffect.ReleaseAsset();
    }

    public bool IsHaveEmptySlot
    {
        get
        {
            if (_noneSlot.Count <= 0)
                return false;

            return true;
        }
    }

    public TileObjectBase GetTileSlot(int index)
    {
        return _userTilePool.TileList[index];
    }

    public GameUnit GetTileUnit(int index)
    {
        foreach (var slot in _useSlot)
        {
            if (slot.slotIndex == index)
                return slot.unit;
        }

        return null;
    }

    public TileSlot GetNoneSlot(int index)
    {
        foreach (var none in _noneSlot)
        {
            if (none.slotIndex == index)
            {
                return none;
            }
        }

        return null;
    }

    public TileSlot ReserveSlot(int index)
    {
        foreach (var none in _noneSlot)
        {
            if (none.slotIndex == index)
            {
                _noneSlot.Remove(none);
                _reserveSlot.Add(none);
                return none;
            }
        }

        return null;
    }

    public TileSlot ReserveRandomSlot()
    {
        if (_noneSlot.Count <= 0)
            return null;

        int randomIndex = Random.Range(0, _noneSlot.Count);

        var slot = _noneSlot[randomIndex];
        slot.block = true;
        _noneSlot.Remove(slot);
        _reserveSlot.Add(slot);

        return slot;
    }

    public TileSlot UseReserveSlot(int slotIndex, GameUnit unit)
    {
        var slot = GetReserveSlot(slotIndex);
        if (slot == null)
            return null;

        slot.unit = unit;
        SetSlotUnit(slot, unit);

        unit.OnReleaseEvent += ReturnSlot;

        _useSlot.Add(slot);
        _reserveSlot.Remove(slot);
        return slot;
    }

    public TileSlot GetReserveSlot(int slotIndex)
    {
        for (int i = _reserveSlot.Count - 1; i >= 0; --i)
        {
            var slot = _reserveSlot[i];
            if (slot.slotIndex == slotIndex)
                return slot;
        }
        return null;
    }

    public void BlockControll(int slotIndex, bool isBlock)
    {
        var slot = GetUseSlot(slotIndex);
        if (slot == null)
            return;

        slot.block = isBlock;
    }

    public void BlockControll(GameUnit unit, bool isBlock)
    {
        if (unit == null)
            return;

        BlockControll(unit.CurrTileIndex, isBlock);
    }

    public bool CheckBlock(int slotIndex)
    {
        var slot = GetUseSlot(slotIndex);
        if (slot == null)
            return true;

        return slot.block;
    }

    public bool CheckBlock(GameUnit unit)
    {
        if (unit == null)
            return true;

        return CheckBlock(unit.CurrTileIndex);
    }

    public TileSlot UseSlot(int slotIndex, GameUnit unit)
    {
        for (int i = _noneSlot.Count - 1; i >= 0; --i)
        {
            var slot = _noneSlot[i];
            if (slot.slotIndex == slotIndex)
            {
                slot.unit = unit;
                SetSlotUnit(slot, unit);

                unit.OnReleaseEvent += ReturnSlot;

                _useSlot.Add(slot);
                _reserveSlot.Remove(slot);
                _noneSlot.Remove(slot);

                return slot;
            }
        }
        return null;
    }

    public TileSlot GetUseSlot(int slotIndex)
    {
        for (int i = 0; i < _useSlot.Count; ++i)
        {
            var slot = _useSlot[i];
            if (slot.slotIndex == slotIndex)
                return slot;
        }

        return null;
    }

    public void SetSlotUnit(TileSlot slot, GameUnit unit)
    {
        if (slot == null)
            return;

        var tile = (UserTile)GetTileSlot(slot.slotIndex);
        tile.SetOwner(unit);
        slot.unit = unit;
        if (unit != null)
        {
            CreateTileEffect(slot);
            unit.OnReleaseEvent += ReturnSlot;
            slot.block = false;
        }
        else
            slot.block = true;

    }

    public void ReturnSlot(GameUnit unit)
    {
        for (int i = _useSlot.Count - 1; i >= 0; --i)
        {
            if (unit == _useSlot[i].unit)
            {
                var slot = _useSlot[i];

                SetSlotUnit(slot, null);

                if (slot.effect != null)
                {
                    EightUtil.GetCore<GameEffectMgr>().ReturnEffect(slot.effect);
                    slot.effect = null;
                }

                _noneSlot.Add(_useSlot[i]);
                _useSlot.RemoveAt(i);
                slot.block = false;
            }
        }
    }

    public void ResetAllUnitSeed()
    {
        foreach (var slot in _useSlot)
        {
            slot.unit?.UnitRandomController.UpdateRandomValues();
        }
    }

    public void CreateTileEffect(TileSlot slot)
    {
        if (slot.unit != null)
        {
            float xDir = 1.0f;
            if (slot.unit.BattleManager != null &&
                slot.unit.BattleManager.ReceiverType == SocketDataType.Network)
            {
                xDir = -1.0f;
            }

            EightUtil.GetCore<GameEffectMgr>().ReturnEffect(slot.effect);

            if (_sumonEffectDataList.Count > 0)
            {
                var sumonEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_sumonEffectDataList[slot.unit.UnitStep].Asset as GameEffectData);
                if (sumonEffect != null)
                {
                    sumonEffect.Clear();
                    sumonEffect.ReStart();
                    sumonEffect.transform.position = GetTileSlot(slot.slotIndex).GetPositionForUnit();
                    int xLayer = (int)(sumonEffect.transform.position.x * xDir);
                    int yLayer = (int)(sumonEffect.transform.position.y * -10.0f);
                    sumonEffect.SortingOrder = 9001 + yLayer + xLayer;
                }
            }

            if (_sumonEffectDataList.Count > 0)
            {
                var useEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_tileEffectDataList[slot.unit.UnitStep].Asset as GameEffectData);
                if (useEffect != null)
                {
                    useEffect.Clear();
                    useEffect.ReStart();
                    useEffect.transform.position = GetTileSlot(slot.slotIndex).GetPositionForUnit();
                    slot.effect = useEffect;
                    int xLayer = (int)(useEffect.transform.position.x * xDir);
                    int yLayer = (int)(useEffect.transform.position.y * -10.0f);
                    useEffect.SortingOrder = 9001 + yLayer + xLayer;
                }
            }
            else
                Debug.Log("TileSlotMgr::_tileEffectData count is null!!");
        }
    }
}
