﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using BestHTTP.SocketIO;

using EightWork;


//public sealed class TestJsonEncoder : BestHTTP.SocketIO.JsonEncoders.IJsonEncoder
//{
//    public List<object> Decode(string json)
//    {

//        //return Json.Decode(json) as List<object>;
//    }

//    public string Encode(List<object> obj)
//    {
//        return null;
//        //return Json.Encode(obj);
//    }

//    private 
//}

public class BestHTTPSocketSample : MonoBehaviour
{
    [SerializeField]
    private string _address = null;

    [SerializeField, ReadOnly]
    private SocketManager _manager = null;

    private void Awake()
    {
        _manager = new SocketManager(new System.Uri(_address));

        _manager.Socket.On(SocketIOEventTypes.Connect, (s, p, a) =>
        {
            Debug.Log("Connected!");

            
        });

        _manager.Socket.On(SocketIOEventTypes.Disconnect, (s, p, a) =>
        {
            Debug.Log("Disconnected!");

        }, false);

        _manager.Socket.On("testJoin", (s, p, a) =>
        {
            var data = a[0] as Dictionary<string, object>;
            //var json = JsonConvert.SerializeObject(p.Payload);
            //var jObject = new JObject(a);
            //var fasdf = Parse(a[0]);
            //string jsonStr = JsonConvert.SerializeObject(a);
            //var json = JsonConvert.DeserializeObject<JObject>(jsonStr);
            Debug.Log("join!");
            //Debug.Log("P : " + json);
            //Debug.Log(s);
            //Debug.Log(p);
            //Debug.Log(a);
        });

        var json = new JObject();
        json["test"] = "dnfmasdmf";
        json["post"] = 3213125412;
        _manager.Socket.Emit("smapleEmit", json.ToString());
    }

    private JObject Parse(object a)
    {
        if (a == null)
            return null;

        var json = new JObject(a);

        //foreach (var obj in a)
        //{
        //    json.Add(obj);
        //}

        return null;
    }

    private JToken GetToken(object a)
    {
        string data = (string)a;
        var token = data.Substring(1, data.Length - 1);
        var split = token.Split(',');

        return JToken.Parse(data);
    }
}
