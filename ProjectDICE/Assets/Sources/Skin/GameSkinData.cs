﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(fileName = "New GameSkinData", menuName = "Data/SkinData", order = 3)]
public class GameSkinData : ScriptableObject
{
    [SerializeField]
    private int _dataId = -1;
    public int DataId => _dataId;

    [SerializeField]
    private int _reserveCount = 5;
    public int ReserveCount => _reserveCount;

    [SerializeField]
    private AssetReferenceGameObject _skinAsset = null;
    public AssetReferenceGameObject SkinAsset => _skinAsset;
}