﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleResultRewardContent : MonoBehaviour
{

    [SerializeField]
    private RewardPopupSlotObject _prefabObject = null;

    [SerializeField]
    private UIAlphaController _alphaController = null;

    public void InitData(BattleResultReceiver resultReceiver)
    {
        int gold = resultReceiver.GetGold;
        int boxId = resultReceiver.GetBoxId;
        if (gold > 0)
        {
            var slot = Instantiate(_prefabObject, transform);
            var rewardInfo = new RewardItemInfo
            {
                ItemCategory = (int) ItemCategory.GOODS,
                UserId = 0,
                Id = (int) GoodsID.GOLD,
                Count = gold
            };
            slot.InitData(rewardInfo);
        }

        if (boxId > -1)
        {
            var slot = Instantiate(_prefabObject, transform);
            var rewardInfo = new RewardItemInfo
            {
                ItemCategory = (int) ItemCategory.BOX,
                UserId = 0,
                Id = boxId,
                Count = 1
            };
            slot.InitData(rewardInfo);
        }
        
        _alphaController.ReloadObjects();
        _alphaController.Alpha = 0;
    }
}
