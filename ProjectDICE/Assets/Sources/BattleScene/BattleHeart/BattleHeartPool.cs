﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleHeartPool : MonoBehaviour
{
    [SerializeField]
    private BattleHeart _data = null;

    [SerializeField]
    private int _reserveCount = 3;

    private Queue<BattleHeart> _battleHaertQueue = new Queue<BattleHeart>();

    private bool ReservePool()
    {
        if (_data == null)
            return false;

        for (int i = 0; i < _reserveCount; ++i)
        {
            var copy = Instantiate(_data, transform);
            if (copy == null)
                continue;

            copy.gameObject.SetActive(false);
            _battleHaertQueue.Enqueue(copy);
        }

        return true;
    }

    public BattleHeart GetBattleHeart()
    {
        if (_battleHaertQueue.Count <= 0)
        {
            if (ReservePool() == false)
                return null;
        }

        var heartObject = _battleHaertQueue.Dequeue();
        if (heartObject == null)
            return null;

        heartObject.OnDestroyEnd += ReturnBattleHeart;
        heartObject.gameObject.SetActive(true);
        return heartObject;
    }

    public void ReturnBattleHeart(BattleHeart heartObject)
    {
        if (heartObject == null)
            return;

        heartObject.Release();
        heartObject.transform.SetParent(transform);
        heartObject.gameObject.SetActive(false);
        _battleHaertQueue.Enqueue(heartObject);
    }
}