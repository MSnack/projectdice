﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleHeartController : HpController
{
    [SerializeField]
    private BattleHeartPool _heartPool = null;

    private List<BattleHeart> _heartList = new List<BattleHeart>();

    [SerializeField]
    private BattleRiskEventController _riskEventController = null;

    [SerializeField]
    private bool _isPlayer = false;
    
    public override void UpdateHp(int hp, int dir)
    {
        if (_heartPool == null || hp == _heartList.Count)
            return;

        if (hp >= _heartList.Count)
            AddHp(hp);
        else
            RemoveHp(hp);
    }

    private void AddHp(int hp)
    {
        var count = hp - _heartList.Count;
        for (int i = 0; i < count; ++i)
        {
            var heartObject = _heartPool.GetBattleHeart();
            if (heartObject == null)
                continue;

            heartObject.transform.SetParent(this.transform);
            heartObject.transform.SetSiblingIndex(_heartList.Count);

            heartObject.ChangeState(BattleHeart.HeartState.Create);

            _heartList.Add(heartObject);
        }
    }

    private void RemoveHp(int hp)
    {
        var count = _heartList.Count - hp;
        var targetIndex = _heartList.Count - count;
        for (int i = _heartList.Count - 1; i >= targetIndex; --i)
        {
            var heartObject = _heartList[i];
            heartObject.ChangeState(BattleHeart.HeartState.Destroy);
            _heartList.Remove(heartObject);
        }
        _riskEventController.OnRiskEvent(_isPlayer);
    }
}
