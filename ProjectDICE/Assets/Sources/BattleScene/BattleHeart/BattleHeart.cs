﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BattleHeart : MonoBehaviour
{
    public enum HeartState
    {
        Idle,
        Create,
        Destroy,
    }

    [SerializeField]
    private Animator _heartAnimator = null;

    public event UnityAction<BattleHeart> OnDestroyEnd = null;


    private void OnDestroyEndEvent()
    {
        OnDestroyEnd?.Invoke(this);
    }

    public void ChangeState(HeartState state)
    {
        if (_heartAnimator == null)
            return;

        _heartAnimator.SetInteger("State", (int)state);
    }

    private void ChangeIdle()
    {
        ChangeState(HeartState.Idle);
    }

    public void Release()
    {
        OnDestroyEnd = null;
    }
}
