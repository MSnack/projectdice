﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserTilePool : TilePool<UserTile>
{
    public class TilePos
    {
        private int _x = -1;
        public int X => _x;

        private int _y = -1;
        public int Y => _y;

        public TilePos(int x, int y)
        {
            _x = x;
            _y = y;
        }
    }

    [System.Serializable]
    private class TileLine
    {
        [SerializeField]
        private List<UserTile> _line = null;
        public UserTile[] Line => _line.ToArray();
    }

    [SerializeField]
    private List<TileLine> _tileMap = null;
    public UserTile[][] TileMap
    {
        get
        {
            var container = new List<UserTile[]>();
            foreach (var line in _tileMap)
            {
                container.Add(line.Line);
            }

            return container.ToArray();
        }
    }

    public void UpdateTileSprite(Sprite sprite)
    {

    }

    public TilePos GetTilePos(int index)
    {
        var target = _tilePool[index];
        for (int i = 0; i < _tileMap.Count; ++i)
        {
            var line = _tileMap[i].Line;
            for (int j = 0; j < line.Length; ++j)
            {
                var tile = line[j];
                if (tile == target)
                    return new TilePos(j, i);
            }
        }

        return null;
    }
}
