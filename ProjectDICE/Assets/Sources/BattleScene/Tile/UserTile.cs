﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

using UnityEngine.Events;

public class UserTile : TileObjectBase
{
    [SerializeField, ReadOnly()]
    private GameUnit _owner = null;
    public GameUnit Owner => _owner;

    [SerializeField]
    private float _gradationWeight = 1.0f;

    private Coroutine _colorRoutine = null;

    [SerializeField]
    private SpriteRenderer _tileEffectSpriteRenderer = null;
    private int count;

    public UserTile(int count)
    {
        this.count = count;
    }

    public Sprite TileEffectImage
    {
        get
        {
            if (_tileEffectSpriteRenderer == null)
                return null;

            return _tileEffectSpriteRenderer.sprite;
        }
        set => SetTileEffectSprite(value);
    }

    public void SetOwner(GameUnit owner)
    {
        _owner = owner;
    }

    [ContextMenu("SetTileEffectSpriteRenderer")]
    protected override void SetTileEffectSpriteRenderer()
    {
        var list = GetComponentsInChildren<SpriteRenderer>(true);
        foreach (var sr in list)
        {
            if(sr.gameObject.name == "TileEffect")
            {
                _tileEffectSpriteRenderer = sr;
                break;
            }
        }
    }

    public void SetGradationColor(Color targetColor)
    {
        if (_colorRoutine != null)
            StopCoroutine(_colorRoutine);

        _colorRoutine = StartCoroutine(GradationColorRoutine(targetColor));
    }

    private IEnumerator GradationColorRoutine(Color targetColor)
    {
        var startColor = TileColor;
        while (TileColor != targetColor)
        {
            TileColor = Color.Lerp(TileColor, targetColor, Time.deltaTime * _gradationWeight);
            yield return null;
        }

        _colorRoutine = null;
    }

    private void SetTileEffectSprite(Sprite sprite)
    {
        if (_tileEffectSpriteRenderer == null)
            return;

        _tileEffectSpriteRenderer.gameObject.SetActive(sprite == null ? false : true);
        _tileEffectSpriteRenderer.sprite = sprite;
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        TileEffectImage = TileEffectImage;
    }
}
