﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterGoalTile : MonsterTile
{
    [SerializeField]
    private Animator[] _lifeAnimators = null;

    [SerializeField]
    private GameObject _hitEffect = null;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetHitEvent()
    {
        foreach (var animator in _lifeAnimators)
        {
            if (animator.enabled == false) continue;
            animator.SetTrigger("Hit");
        }
        _hitEffect.SetActive(false);
        _hitEffect.SetActive(true);
    }
}
