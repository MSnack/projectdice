﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class MonsterSpawnTile : MonsterTile
{
    //[SerializeField]
    //private GameUnitTeam team = GameUnitTeam.Null;

    //[SerializeField]
    //private BattleSocket _battleSocket = null;

    //[SerializeField, ReadOnly(ReadOnlyType.EDITABLE_RUNTIME)]
    //private bool _isReceiver = false;

    //private MonsterTilePool _tilePool = null;

    protected override void Awake()
    {
        base.Awake();

        //_tilePool = transform.parent.GetComponent<MonsterTilePool>();
        SetupComplete();
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        //_battleSocket.MonsterEvent += CreateMonster;
    }

    //private void CreateMonster(RequestUnitData data)
    //{
    //    if (team == GameUnitTeam.LocalEnemy && data.isLocal == false)
    //        return;

    //    if (team == GameUnitTeam.OtherEnemy && data.isLocal == true)
    //        return;

    //    GameUnit unit = EightUtil.GetCore<GameUnitMgr>().CreateMonster<MonsterController>(data.unitId, team, 1);
    //    if (unit == null)
    //        return;

    //    unit.gameObject.SetActive(true);
    //    unit.transform.position = GetPositionForUnit();
    //    unit.MonsterTilePool = _tilePool;
    //}

    protected override void OnDisable()
    {
        base.OnDisable();

        //_battleSocket.MonsterEvent -= CreateMonster;
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();

        //_isCreate = true;
    }

    //public bool CreateMonster()
    //{
    //    if (_isCreate == false)
    //        return false;

    //    GameUnit unit = EightUtil.GetCore<GameUnitMgr>().CreateMonster<MonsterController>(3000, GameUnitTeam.LocalEnemy, 1);
    //    if (unit == null)
    //        return false;

    //    unit.gameObject.SetActive(true);
    //    unit.transform.position = GetPositionForUnit();
    //    unit.MonsterTilePool = _tilePool;
    //    return true;
    //}

    //float _maxTime = 3.0f;
    //float _time = 3.0f;
    //bool _isCreate = false;
    //private void Update()
    //{
    //    if (_isReceiver == true || _battleSocket.IsBattleReady == false)
    //        return;

    //    _time += Time.deltaTime;
    //    if (_time >= _maxTime)
    //    {
    //        Debug.Log("MonsterTime!");
    //        _time -= _maxTime;
    //        _battleSocket.CreateMonster(3000);
    //    }
    //}
}
