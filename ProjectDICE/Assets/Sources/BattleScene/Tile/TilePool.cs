﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TilePool<TTileObjectClass> : MonoBehaviour where TTileObjectClass : TileObjectBase
{
    [SerializeField]
    protected List<TTileObjectClass> _tilePool = new List<TTileObjectClass>();
    public TTileObjectClass[] TileList => _tilePool.ToArray();
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPool(List<TTileObjectClass> pool)
    {
        _tilePool = new List<TTileObjectClass>(pool);
    }

    public void SetPool(List<TileObjectBase> pool)
    {
        _tilePool = new List<TTileObjectClass>((IEnumerable<TTileObjectClass>) pool);
    }
}
