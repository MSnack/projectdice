﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterTilePool : TilePool<MonsterTile>
{

    [SerializeField]
    private List<MonsterTile> _tilePath = null;
    public MonsterTile[] TilePath => _tilePath.ToArray();
    public TileObjectBase[] DefaultTileList
    {
        get
        {
            List<TileObjectBase> tileList = new List<TileObjectBase>(TilePath);
            tileList.Remove(_spawnTile);
            tileList.Remove(_goalTile);

            return tileList.ToArray();
        }
    }

    [SerializeField]
    private MonsterSpawnTile _spawnTile = null;
    public TileObjectBase SpawnTile => _spawnTile;

    [SerializeField]
    private MonsterGoalTile _goalTile = null;
    public TileObjectBase GoalTile => _goalTile;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetTilePath(List<MonsterTile> path)
    {
        _tilePath = new List<MonsterTile>(path);
        _spawnTile = (MonsterSpawnTile) _tilePath.Find(
            (x) => x.GetType() == typeof(MonsterSpawnTile));
        _goalTile = (MonsterGoalTile) _tilePath.Find(
            (x) => x.GetType() == typeof(MonsterGoalTile));
    }
}
