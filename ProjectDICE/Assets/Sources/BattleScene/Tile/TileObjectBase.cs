﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public abstract class TileObjectBase : EightReceiver
{

    [SerializeField]
    protected Vector2 _posID;

    [SerializeField]
    protected int _ID = SystemIndex.InvalidInt;

    public Vector2 PosId => _posID;
    public int ID => _ID;

    protected UnityAction _onTouched = null;
    protected RectTransform _rectTransform = null;

    [SerializeField]
    private SpriteRenderer _spriteRendererComponent = null;
    //public SpriteRenderer SpriteRendererComponent => _spriteRendererComponent;

    [SerializeField]
    private Color _color = Color.white;
    public Color TileColor { get => _color; set => SetColor(value); }

    protected virtual void Awake()
    {
        _rectTransform = (RectTransform) transform;
    }

    [ContextMenu("SetSpriteRenderer")]
    private void SetSpriteRenderer()
    {
        _spriteRendererComponent = GetComponentInChildren<SpriteRenderer>();
    }

    protected abstract void SetTileEffectSpriteRenderer();

    private void SetColor(Color color)
    {
        _color = color;
        //var tile = GetComponentInChildren<SpriteRenderer>(true);
        //if (tile == null)
        //    return;

        if (_spriteRendererComponent == null)
            return;

        _spriteRendererComponent.color = color;
    }

    public void UpdateSprite(Sprite sprite)
    {
        if (_spriteRendererComponent == null)
            return;

        _spriteRendererComponent.sprite = sprite;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Reset()
    {
        SetSpriteRenderer();
        SetTileEffectSpriteRenderer();
    }

    protected virtual void OnDrawGizmos()
    {
        SetColor(_color);
    }

    public void SetData(Vector2 posId, int id)
    {
        _posID = posId;
        _ID = id;
    }

    public Vector3 GetPositionForUnit(bool isLocalPosition = false)
    {
        Vector3 position = isLocalPosition ? transform.localPosition : transform.position;
        Vector2 renewSize = _rectTransform.sizeDelta * transform.lossyScale;
        renewSize.x = 0;
        renewSize.y /= 6;
        
        return new Vector3(position.x + renewSize.x, position.y + renewSize.y, position.z);
    }
}
