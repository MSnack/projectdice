﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleEmoticonController : BattleSocketReceiver<BattleSceneInterface>
{
    protected override void OnEnable()
    {
        base.OnEnable();

        Receiver.EmoticonEvent += OnEmoticonEvent;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        Receiver.EmoticonEvent -= OnEmoticonEvent;
    }

    private void OnEmoticonEvent(RequestEmoticonData data)
    {
        if (IsAvailableData(data) == false)
            return;


    }
}
