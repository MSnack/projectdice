﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCardList : MonoBehaviour
{
    [SerializeField]
    private BattleUserManager _battleManager = null;

    [SerializeField]
    private BattleManager _eventManager = null;

    [SerializeField]
    private List<EnemyCardSlot> _cardSlotList = null;

    public void OnEnable()
    {
        _battleManager.OnInitEvent += InitEvent;
    }

    private void InitEvent(BattleInitUserData data)
    {
        var unitLIst = data.UnitList;
        for (int i = 0; i < unitLIst.Length; ++i)
            _cardSlotList[i].Init(unitLIst[i]);
    }
}
