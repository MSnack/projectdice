﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BanButton : MonoBehaviour
{
    private enum BanState
    {
        Ban,
        NotBan
    };

    private BanState _state;

    [SerializeField]
    private Image _banIcon = null;

    [SerializeField]
    private NetworkEmoticon _emotion = null;

    public void OnEnable()
    {
        if (_banIcon == null)
            return;

        _banIcon.enabled = false;
        _state = BanState.NotBan;
    }

    public void Click()
    {
        if ( _banIcon == null)
            return;

        if (_emotion == null)
            return;

        if (_state == BanState.NotBan)
        {
            _banIcon.enabled = true;
            _emotion.IsBan = true;
            _state = BanState.Ban;
            return;
        }

        _banIcon.enabled = false;
        _emotion.IsBan = false;
        _state = BanState.NotBan;
    }
}
