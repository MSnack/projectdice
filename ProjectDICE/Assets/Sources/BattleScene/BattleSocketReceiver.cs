﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using EightWork.MsgSystem;

public enum SocketDataType
{
    None,
    Local,
    Network,
}

public class BattleSocketReceiver<MsgInterface> : EightMsgReceiver<MsgInterface> where MsgInterface : EightMsgSystem
{
    [SerializeField]
    private SocketDataType _receiveType = SocketDataType.None;
    public SocketDataType ReceiverType => _receiveType;

    [SerializeField]
    private BattleSocket _receiver = null;
    public BattleSocket Receiver => _receiver;

    protected bool IsAvailableData(BattleSocketRequestData data)
    {
        if (data.ReceiveType != ReceiverType)
            return false;

        return true;
    }
}
