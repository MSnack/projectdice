﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.MsgSystem;
using EightWork;
using UnityEngine.Events;
using RotaryHeart.Lib.SerializableDictionary;

public class BattleStageData
{
    private int _stageIndex = -1;
    public int StageIndex => _stageIndex;

    private float _hpScale = 1.0f;
    public float HpScale => _hpScale;

    public BattleStageData(int index, float hpScale)
    {
        _stageIndex = index;
        _hpScale = hpScale;
    }
}

public class BattleStageController : EightMsgReceiver<BattleSceneInterface>
{
    [SerializeField]
    private BattleSocket _socket = null;

    private Dictionary<int, BattleStageData> _stageData = new Dictionary<int, BattleStageData>();

    [SerializeField, ReadOnly]
    private int _currStage = 1;
    public int CurrStage => _currStage;

    [SerializeField, ReadOnly]
    private bool _isLoadData = false;
    public bool IsLoadData => _isLoadData;

    [SerializeField]
    private UnityEvent _updateStageEvent = null;
    public event UnityAction UpdateStageEvent
    {
        add => _updateStageEvent.AddListener(value);
        remove => _updateStageEvent.RemoveListener(value);
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        _socket.UpdateBattleStageEvent += UpdateBattleStageData;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        _socket.UpdateBattleStageEvent -= UpdateBattleStageData;
    }

    private void UpdateBattleStageData(RequestBattleStageData data)
    {
        if (_isLoadData == true || data == null)
            return;

        _stageData.Clear();

        var dataList = data.StageDataList;
        foreach (var stageData in dataList)
        {
            _stageData[stageData.StageIndex] = stageData;
        }

        _isLoadData = true;
    }

    public BattleStageData GetCurrStageData()
    {
        if (_stageData.ContainsKey(_currStage) == false)
            return null;

        return _stageData[_currStage];
    }

    public void UpdateNextStage()
    {
        if (_stageData.ContainsKey(_currStage + 1) == false)
            return;

        _currStage += 1;
        _updateStageEvent?.Invoke();
    }

    public void OnLoadData()
    {
        if (_isLoadData == true)
            return;

        _socket.EmitUpdateBattleStageData();
    }
}
