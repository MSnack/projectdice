﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleSpawnButtonController : MonoBehaviour
{
    [SerializeField]
    private BattleUserManager _userManager = null;

    [SerializeField]
    private Text _spText = null;

    [SerializeField]
    private Text _priceText = null;

    [SerializeField]
    private string _spFormat = "{0}<color=#D47DEE>/{1}</color>";

    [SerializeField]
    private Animator _spAnimator = null;

    [SerializeField]
    private GameObject _spButtonEnableObject = null;
    
    private void Awake()
    {
        _userManager.UpdateSpEvent += UpdateSp;
    }

    private void OnEnable()
    {
        _userManager.CallSpEvent();
    }

    //private void OnEnable()
    //{
    //    UpdateSp(_userManager.Sp, _userManager.NeedSp);
    //}

    private void UpdateSp(int haveSp, int needSp)
    {
        _spText.text = haveSp.ToString();
        _priceText.text = string.Format(_spFormat, needSp);
        _spAnimator.Rebind();
        if (haveSp - needSp >= 0)
        {
            if(!_spButtonEnableObject.activeInHierarchy)
                _spButtonEnableObject.SetActive(true);
        }
        else
        {
            if(_spButtonEnableObject.activeInHierarchy)
                _spButtonEnableObject.SetActive(false);
        }
    }
}