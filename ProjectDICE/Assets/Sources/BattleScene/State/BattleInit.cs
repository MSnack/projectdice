﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.Events;

public class BattleInit : BattleStateBase
{
    [SerializeField]
    private BattleTimeSyncer _timeSyncer = null;

    [SerializeField]
    private BattleMonsterController _monsterController = null;

    [SerializeField]
    private BattleWaveController _waveController = null;

    [SerializeField]
    private BattleStageController _stageController = null;

    [SerializeField, ReadOnly]
    private bool _isRequestReady = false;

    [SerializeField, ReadOnly]
    private float _currDelayTime = 0.0f;

    [SerializeField]
    public float _delayTime = 1.0f;

    protected override void Initialize()
    {
        State = BattleFSMSystem.BattleFSMState.Init;
    }

    public override void StartState()
    {
        base.StartState();

        SystemMgr.MsgInterface.SocketManager.ReadyEvent += PlayReadyEvent;
        _isRequestReady = false;
        _currDelayTime = 0.0f;
        _readyTime = -1.0f;

        StartCoroutine(InitRoutine());
    }

    private bool TimeSyncCheck()
    {
        var isSync = _timeSyncer.IsSync;
        if (isSync == false)
            Debug.Log("No SyncTime");

        return isSync;
    }

    private bool UserInitCheck()
    {
        var isInit = SystemMgr.MsgInterface.IsInit;
        if (isInit == false)
            Debug.Log("No UserInit");

        return isInit;
    }

    private void PlayReadyEvent(RequestReadyData data)
    {
        if (data.IsReady == false)
            SystemMgr.MsgInterface.SocketManager.EmitRequestReady();
    }

    private bool GamePlayReadyCheck()
    {
        var isPlayReady = SystemMgr.MsgInterface.IsReadyCheck;
        if (isPlayReady == false)
        {
            if (SystemMgr.MsgInterface.IsLeaveNetworkUser == true)
                return true;

            RequestReady();
            Debug.Log("No PlayReady");
        }

        return isPlayReady;
    }

    private float _readyTime = -1.0f;
    private void RequestReady()
    {
        if (_readyTime == -1.0f)
            _readyTime = Time.time;

        var currTime = Time.time - _readyTime;
        if (currTime < 0.1f)
            return;

        SystemMgr.MsgInterface.SocketManager.EmitRequestReady();
        _readyTime = -1.0f;
    }

    private bool BattleControllerCheck()
    {
        var isLoad = true;

        if (_monsterController.IsLoadData == false)
        {
            Debug.Log("MonsterController Is Not Load");
            isLoad = false;
        }

        if (_stageController.IsLoadData == false)
        {
            Debug.Log("StageController Is Not Load");
            isLoad = false;
        }

        if (_waveController.IsLoadData == false)
        {
            Debug.Log("WaveController Is Not Load");
            isLoad = false;
        }

        return isLoad;
    }

    private bool CheckLoadBossSeed()
    {
        var isLoad = _monsterController.IsLoadBossSeed;
        if (isLoad == false)
            Debug.Log("BossSeed is Not Load");

        return isLoad;
    }

    private IEnumerator InitRoutine()
    {
        yield return new WaitForSecondsRealtime(1.0f);

        _timeSyncer.OnTimeSync();
        yield return new WaitUntil(() => TimeSyncCheck() == true);

        _monsterController.OnLoadData();
        _stageController.OnLoadData();
        _waveController.OnLoadData();
        yield return new WaitUntil(() => BattleControllerCheck());

        SystemMgr.MsgInterface.SocketManager.EmitInitUserData();
        yield return new WaitUntil(() => UserInitCheck() == true);
        yield return new WaitUntil(() => CheckLoadBossSeed() == true);

        SystemMgr.MsgInterface.SocketManager.EmitPlayReady(true);
        yield return new WaitUntil(() => GamePlayReadyCheck() == true);

        EightUtil.GetCore<EightMsgMgr>().OnEightMsg(EIGHT_MSG.ON_FADE_PAUSE, new EightMsgContent(0));
        // yield return new WaitForSecondsRealtime(1.0f);
        yield return new WaitUntil(() => ChangeChecker());

        SystemMgr.ChangeState(BattleFSMSystem.BattleFSMState.Play);
    }

    private bool ChangeChecker()
    {
        _currDelayTime += Time.deltaTime;
        if (EightUtil.GetCore<EightCameraMgr>().Effect.IsComplete == false)
            return false;

        if (_currDelayTime < _delayTime)
            return false;

        return true;
    }

    public override void EndState()
    {

        SystemMgr.MsgInterface.SocketManager.ReadyEvent -= PlayReadyEvent;

        base.EndState();
    }
}
