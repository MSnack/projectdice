﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class BattlePause : BattleStateBase
{
    protected override void Initialize()
    {
        State = BattleFSMSystem.BattleFSMState.Pause;
    }

    public override void StartState()
    {
        base.StartState();
        SystemMgr.MsgInterface.PauseGame();
    }

    public override void EndState()
    {
        base.EndState();
    }
}
