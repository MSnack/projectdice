﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class BattlePlay : BattleStateBase
{
    [SerializeField, ReadOnly]
    private bool _isPause = false;

    [SerializeField]
    private BattleResultReceiver _resultReceiver = null;

    protected override void Initialize()
    {
        State = BattleFSMSystem.BattleFSMState.Play;
    }

    public override void StartState()
    {
        base.StartState();

        SystemMgr.MsgInterface.PlayGame();

        // _isPause = true;
    }

    public override void EndState()
    {
        base.EndState();

        // _isPause = false;

        SystemMgr.MsgInterface.PauseGame();
    }


    private void OnApplicationPause(bool pause)
    {
        if (pause == true)
            return;

        _resultReceiver.OnResultEvent(false);
        SystemMgr.ChangeState(BattleFSMSystem.BattleFSMState.Result);
    }
}
