﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.Events;

public class BattleResult : BattleStateBase
{
    [SerializeField]
    private BattleResultController _resultController = null;

    [SerializeField]
    private BattleResultReceiver _battleResultReceiver = null;

    [SerializeField]
    private List<BattleManager> _battleManagers = null;

    public UnityEvent ResultEvent = null;

    [SerializeField, ReadOnly]
    private bool _isLoadedBoxInventroy = false;

    [SerializeField, ReadOnly]
    private bool _isLoadedAllGoods = false;

    [SerializeField, ReadOnly]
    private bool _isLoadedBattleInfo = false;

    private bool IsLoadedResultState
    {
        get
        {
            if (_isLoadedAllGoods == false ||
                // _isLoadedBattleInfo == false ||
                _isLoadedBoxInventroy == false)
                return false;

            return true;
        }
    }

    protected override void Initialize()
    {
        State = BattleFSMSystem.BattleFSMState.Result;
    }

    public override void StartState()
    {
        base.StartState();
        StartCoroutine(ResultSetting());
    }

    private BattleManager GetManager(SocketDataType type)
    {
        return _battleManagers.Find((x) => x.ReceiverType == type);
    }

    private IEnumerator ResultSetting()
    {
        SystemMgr.MsgInterface.PauseGame();
        EightUtil.GetCore<EightMsgMgr>().OnEightMsg(EIGHT_MSG.ON_FADE_PAUSE, new EightMsgContent(0));

        foreach (var bm in _battleManagers)
        {
            bm.TileRoadMgr.ClearRoadUnit();
        }

        yield return new WaitUntil(() => SystemMgr.IsWaitResult == false);
        yield return new WaitUntil(() => _battleResultReceiver.IsResult == true);

        SystemMgr.MsgInterface.OnLeave();

        // BattleInfoServerConnector.GetBattleInfo((info) =>
        // {
        //     info.Level = _battleResultReceiver.GetExp;
        //     _isLoadedBattleInfo = true;
        // });
        bool isLoadedQuest = false;
        QuestServerConnector.RefreshUserQuestInfos(() =>
        {
            isLoadedQuest = true;
            QuestUtil.RefreshQuests();
        });
        yield return new WaitUntil(() => isLoadedQuest == true);
        
        LobbyBoxServerConnector.GetUserBoxInventory(() => _isLoadedBoxInventroy = true);
        int prevTrophy = LobbyGoodsUtil.GetGoods(GoodsID.TROPHY);
        LobbyGoodsServerConnector.GetAllGoods(() =>
        {
            _isLoadedAllGoods = true;
            SaveBattleHistory(_battleResultReceiver.IsWin, prevTrophy);
        });
        yield return new WaitUntil(() => IsLoadedResultState == true);

        SystemMgr.MsgInterface.SocketManager.EmitBattleLeaveSync();
        _resultController.OnResult(_battleResultReceiver);
        ResultEvent?.Invoke();
    }

    private void SaveBattleHistory(bool isWin, int prevTrophy)
    {
        var localUserManager = GetManager(SocketDataType.Local).UserManager;
        var networkUserManager = GetManager(SocketDataType.Network).UserManager;

        var opponentDeckList = GetDeckList(networkUserManager.UserData);
        var myDeckList = GetDeckList(localUserManager.UserData);
        
        BattleHistoryUtil.AddBattleHistory(new BattleHistoryLocalData()
        {
            IsWin = isWin,
            MatchId = 0,
            OpponentDeck = opponentDeckList.ToArray(),
            OpponentName = networkUserManager.UserData.UserName,
            OpponentTier = networkUserManager.UserData.Rating,
            OpponentTrophy = 0,
            OpponentGuild = "",
            TrophyIncrementValue = LobbyGoodsUtil.GetGoods(GoodsID.TROPHY) - prevTrophy,
            
            MyDeck = myDeckList.ToArray(),
            MyName = localUserManager.UserData.UserName,
            MyTier = localUserManager.UserData.Rating,
            MyTrophy = LobbyGoodsUtil.GetGoods(GoodsID.TROPHY),
            MyGuild = "",
        });
    }

    private List<int> GetDeckList(BattleInitUserData userData)
    {
        var deckRawList = userData.UnitList;
        var deckList = new List<int>();

        foreach (var cardData in deckRawList)
        {
            deckList.Add(cardData.UnitID);
        }

        return deckList;
    }

    public override void EndState()
    {
        base.EndState();
    }
}
