﻿using System.Collections;
using System.Collections.Generic;
using EightWork.FSM;
using UnityEngine;
using EightWork;

public class BattleFSMSystem : EightFSMSystem<BattleFSMSystem.BattleFSMState, BattleStateBase, BattleSceneInterface>
{
    public enum BattleFSMState
    {
        Init,
        Play,
        Pause,
        Result,


        UNKOWN = -9999,
    }

    public void OnResultState()
    {
        ChangeState(BattleFSMState.Result);
    }

    public void OnGiveUpEvent(SocketDataType type)
    {
        if (type == SocketDataType.Local)
        {
            ChangeState(BattleFSMState.Result);
            return;
        }

        _isWaitResult = true;
        GamePopupMgr.OpenNormalPopup("알림", "상대방이 항복했습니다!", new[]
        {
            new NormalPopup.ButtonConfig("확인", OnCheckWaitResult),
        });

        ChangeState(BattleFSMState.Result);
    }

    public void OnCheckWaitResult()
    {
        _isWaitResult = false;
        NormalPopup.Close();
    }

    [SerializeField]
    private bool _isWaitResult = false;
    public bool IsWaitResult => _isWaitResult;

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }
}
