﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.MsgSystem;
using EightWork;
using UnityEngine.Events;
using RotaryHeart.Lib.SerializableDictionary;

public class BattleMonsterData
{
    public enum MonsterType
    {
        Spawn,
        Boss,
        Summon,
    }

    private int _index = -1;
    public int Index => _index;

    private int _unitId = -1;
    public int UnitId => _unitId;

    private int _hp = 0;
    public int Hp => _hp;

    private float _speed = 0.0f;
    public float Speed => _speed;

    private int _power = 0;
    public int Power => _power;

    private float _spawnTime = -1.0f;
    public float SpawnTime => _spawnTime;

    private int _sp = 0;
    public int Sp => _sp;

    private MonsterType _type = MonsterType.Spawn;
    public MonsterType MType => _type;

    public BattleMonsterData(int index, int unitId, int hp, float speed, int power, float spawnTime, int sp, MonsterType type)
    {
        _index = index;
        _unitId = unitId;
        _hp = hp;
        _speed = speed;
        _power = power;
        _spawnTime = spawnTime;
        _sp = sp;
        _type = type;
    }
}

public class BattleMonsterController : EightMsgReceiver<BattleSceneInterface>
{
    [SerializeField]
    private BattleSocket _socket = null;

    private Dictionary<BattleMonsterData.MonsterType, List<BattleMonsterData>> _monsterDataList = new Dictionary<BattleMonsterData.MonsterType, List<BattleMonsterData>>();

    [SerializeField, ReadOnly]
    private int _bossRandomSeed = 0;

    private System.Random BossRandom = null;

    [SerializeField, ReadOnly]
    private int _currBossId = 0;
    public int CurrBossId => _currBossId;

    [SerializeField, ReadOnly]
    private bool _isLoadData = false;
    public bool IsLoadData => _isLoadData;

    [SerializeField, ReadOnly]
    private bool _isLoadBossSeed = false;
    public bool IsLoadBossSeed => _isLoadBossSeed;

    public event UnityAction<BattleMonsterData[]> UpdateSpawnMonsterDataEvent = null;
    public event UnityAction<int> UpdateNewBossEvent = null;

    protected override void OnEnable()
    {
        base.OnEnable();

        _socket.UpdateBattleMonsterEvent += UpdateMonsterData;
        _socket.BattleInitUserDataEvent += BossRandomEvent;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        _socket.BattleInitUserDataEvent -= BossRandomEvent;
        _socket.UpdateBattleMonsterEvent -= UpdateMonsterData;
    }

    public BattleMonsterData[] GetMonsterList(BattleMonsterData.MonsterType type)
    {
        if (_monsterDataList.ContainsKey(type) == false)
            return null;

        return _monsterDataList[type].ToArray();
    }

    public BattleMonsterData GetMonsterData(BattleMonsterData.MonsterType type, int unitId)
    {
        if (_monsterDataList.ContainsKey(type) == false)
            return null;

        foreach (var monsterData in _monsterDataList[type])
        {
            if (monsterData.UnitId == unitId)
                return monsterData;
        }

        return null;
    }

    public int UpdateBossId()
    {
        if (BossRandom == null)
            return -1;

        var bossUnitList = GetMonsterList(BattleMonsterData.MonsterType.Boss);
        if (bossUnitList == null || bossUnitList.Length <= 0)
            return -1;

        var bossIndex = BossRandom.Next(0, bossUnitList.Length);
        _currBossId = bossUnitList[bossIndex].UnitId;

        UpdateNewBossEvent?.Invoke(_currBossId);

        return _currBossId;
    }

    private void UpdateMonsterData(RequestBattleMonsterData data)
    {
        if (_isLoadData == true || data == null)
            return;

        _monsterDataList.Clear();

        var dataList = data.MonsterDataList;
        foreach (var monsterData in dataList)
        {
            var type = monsterData.MType;
            if (_monsterDataList.ContainsKey(type) == false)
                _monsterDataList[type] = new List<BattleMonsterData>();
            _monsterDataList[type].Add(monsterData);
        }

        _isLoadData = true;
    }

    public void UpdateSpawnMonsterData()
    {
        var monsterList = GetMonsterList(BattleMonsterData.MonsterType.Spawn);
        if (monsterList == null)
            return;

        UpdateSpawnMonsterDataEvent?.Invoke(monsterList);
    }

    private void BossRandomEvent(RequestBattleInitUserData data)
    {
        if (_isLoadBossSeed == true || data == null)
            return;

        _bossRandomSeed = data.BossRandomSeed;
        BossRandom = new System.Random(_bossRandomSeed);

        _isLoadBossSeed = true;
    }

    public void OnLoadData()
    {
        if (_isLoadData == true)
            return;

        _socket.EmitUpdateBattleUnitData();
    }
}
