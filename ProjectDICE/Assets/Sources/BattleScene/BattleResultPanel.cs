﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EightWork;

public class BattleResultPanel : MonoBehaviour
{
    [SerializeField]
    private Text _nameText = null;

    [SerializeField]
    private Text _ratingText = null;

    [SerializeField]
    private UITweenTextNumber _ratingTextTween = null;

    public void UpdateResultData(BattleResultReceiver resultReceiver, BattleUserManager userManager, bool isLocal)
    {
        _nameText.text = userManager.UserData.UserName;

        int showRating = resultReceiver.UpdateTrophy * (isLocal == false ? -1 : 1);
        if (_ratingTextTween != null)
        {
            _ratingTextTween.To = showRating;
        }

        // for(int i = 0; i < _imageSlot.Count; ++i)
        // {
        //     if (userManager.UserData.UnitCount <= i)
        //         continue;
        //
        //     var data = userManager.UserData.UnitList[i];
        //     var unitData = EightUtil.GetCore<GameUnitDataMgr>().GetCharacaterData(data.UnitID);
        //
        //     _imageSlot[i].ChangeSprite(unitData.UnitPreviewSprite);
        // }
    }
}
