﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EightWork;

public class BattleTouchController : MonoBehaviour
{
    [SerializeField]
    private BattleSocket _socket = null;

    [SerializeField, ReadOnly]
    private GameUnit _target = null;
    public GameUnit Target => _target;

    [SerializeField]
    private Color _defaultColor = Color.white;
    public Color DefaultColor => _defaultColor;

    [SerializeField]
    private Color _onTargetColor = Color.white;
    public Color OnTargetColor => _onTargetColor;

    private Vector3 _centerPivot = Vector3.zero;
    public Vector3 CenterPivot => _centerPivot;

    [System.Serializable]
    private class BattleTouchTargetEvent : UnityEvent<GameUnit> { }

    [SerializeField]
    private BattleTouchTargetEvent _battleTouchTargetEvent = null;
    public event UnityAction<GameUnit> TouchTargetEvent
    {
        add { _battleTouchTargetEvent.AddListener(value); }
        remove { _battleTouchTargetEvent.RemoveListener(value); }
    }

    public event UnityAction TargetOffEvent = null;

    public void OnReturnTarget(GameUnit unit)
    {
        if (_target == null || unit == null)
            return;

        if (_target == unit)
        {
            if(_target.SpineUnit != null)
                _target.SpineUnit.transform.localPosition = Vector3.zero;
            _target = null;
        }

        TargetOffEvent?.Invoke();
    }

    public void OnTargetTouch(GameUnit target)
    {
        _target = target;
        if (_target != null)
        {
            _centerPivot = target.SpineUnit.CenterPivot;
            _battleTouchTargetEvent?.Invoke(_target);
        }
        else
            TargetOffEvent?.Invoke();
    }

    public void OnMergeEvent(GameUnit merge)
    {
        if (_target == null || merge == null)
            return;

        _target.OnMerge(merge);
        // _socket.OnMergeUnit(_target, merge);
    }

    //private void Update()
    //{
    //    if (EightUtil.IsLoadCompleteDevice == false)
    //        return;

    //    if (_target == null)
    //    {
    //        if (Input.GetMouseButtonDown(0) == true)
    //        {
    //            Vector2 pos = EightUtil.GetCore<EightCameraMgr>().EightCamera.ScreenToWorldPoint(Input.mousePosition);
    //            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero, 0.0f);
    //            if (hit.collider == null)
    //                return;

    //            var tile = hit.collider.gameObject.GetComponent<UserTile>();
    //            _target = tile.Owner;
    //            if (_target == null)
    //                return;

    //            if (_target.IsHaveBuff(BuffType.Lock))
    //                return;

    //            _centerPivot = _target.SpineUnit.CenterPivot;

    //            _target.OnReturnEvent += OnReturnTarget;
    //            _battleTouchTargetEvent?.Invoke(_target);
    //        }
    //    }
    //    else
    //    {
    //        if (Input.GetMouseButtonUp(0) == true)
    //        {
    //            Vector2 pos = EightUtil.GetCore<EightCameraMgr>().EightCamera.ScreenToWorldPoint(Input.mousePosition);
    //            RaycastHit2D[] hitList = Physics2D.RaycastAll(pos, Vector2.zero, 0.0f);
    //            if (hitList.Length > 0)
    //            {
    //                foreach (var hit in hitList)
    //                {
    //                    var tile = hit.collider.gameObject.GetComponent<UserTile>();
    //                    var merge = tile.Owner;
    //                    if (merge == null)
    //                        continue;

    //                    if (merge == _target)
    //                        continue;


    //                    if (merge.UnitStep != _target.UnitStep)
    //                        continue;

    //                    if (_target.UnitStep >= _target.UserManager.UserData.UnitCount)
    //                        continue;

    //                    _socket.OnMergeUnit(_target, merge);
    //                    //EightUtil.GetCore<GameUnitMgr>().ReturnUnit(_target);
    //                    //_target = null;
    //                    //Destroy(_target.gameObject);
    //                    break;
    //                }
    //            }

    //            _target.OnReturnEvent -= OnReturnTarget;

    //            if (_target != null)
    //            {
    //                if(_target.SpineUnit != null)
    //                    _target.SpineUnit.transform.localPosition = Vector3.zero;
    //                _target = null;
    //                TargetOffEvent?.Invoke();
    //            }

    //            return;
    //        }
    //        else if (Input.GetMouseButton(0) == true)
    //        {
    //            Vector2 pos = EightUtil.GetCore<EightCameraMgr>().EightCamera.ScreenToWorldPoint(Input.mousePosition);
    //            _target.SpineUnit.transform.position = (Vector3)pos - _centerPivot;
    //        }
    //    }
    //}
}
