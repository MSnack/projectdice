﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class BattleTouchDragTargetState : BattleTouchStateBase
{
    protected override void Initialize()
    {
        State = BattleTouchStateMgr.BattleTouchState.OnDrage;
    }

    public override void StartState()
    {
        SystemMgr.TouchController.TargetOffEvent += OnChangeNoTarget;
    }

    private void OnChangeNoTarget()
    {
        SystemMgr.ChangeState(BattleTouchStateMgr.BattleTouchState.NoTarget);
    }

    private void Update()
    {
        if (Input.GetMouseButton(0) == true)
        {
            Vector2 movePos = EightUtil.GetCore<EightCameraMgr>().EightCamera.ScreenToWorldPoint(Input.mousePosition);
            SystemMgr.TouchController.Target.SpineUnit.transform.position = (Vector3)movePos - SystemMgr.TouchController.CenterPivot;
        }
        else if (Input.GetMouseButtonUp(0) == true)
        {
            var raycastObject = SystemMgr.Raycaster.Raycast<BattleTouchRaycastObject>();
            if (raycastObject == null || raycastObject.HitUserTile == null)
            {
                SystemMgr.TouchController.Target.SpineUnit.transform.localPosition = Vector3.zero;
                SystemMgr.TouchController.OnTargetTouch(null);
                SystemMgr.ChangeState(BattleTouchStateMgr.BattleTouchState.NoTarget);
                return;
            }

            var tile = raycastObject.HitUserTile;
            var target = tile.Owner;
            if (target == null || SystemMgr.TouchController.Target == target || target.TileSlotManager.CheckBlock(target.CurrTileIndex) == true)
            {
                SystemMgr.TouchController.Target.SpineUnit.transform.localPosition = Vector3.zero;
                SystemMgr.TouchController.OnTargetTouch(null);
                SystemMgr.ChangeState(BattleTouchStateMgr.BattleTouchState.NoTarget);
                return;
            }

            SystemMgr.TouchController.OnMergeEvent(target);

            SystemMgr.TouchController.Target.SpineUnit.transform.localPosition = Vector3.zero;
            SystemMgr.TouchController.OnTargetTouch(null);
            SystemMgr.ChangeState(BattleTouchStateMgr.BattleTouchState.NoTarget);

        }
    }

    public override void EndState()
    {
        SystemMgr.TouchController.TargetOffEvent -= OnChangeNoTarget;
    }
}
