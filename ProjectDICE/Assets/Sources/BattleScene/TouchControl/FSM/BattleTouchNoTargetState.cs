﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.EventSystems;

public class BattleTouchNoTargetState : BattleTouchStateBase
{
    protected override void Initialize()
    {
        State = BattleTouchStateMgr.BattleTouchState.NoTarget;
    }

    public override void StartState()
    {
        SystemMgr.TouchController.TouchTargetEvent += ChangeOnTarget;
    }

    private void ChangeOnTarget(GameUnit unit)
    {
        SystemMgr.ChangeState(BattleTouchStateMgr.BattleTouchState.OnTarget);
    }

    public void Update()
    {
        if (Input.GetMouseButtonDown(0) == true)
        {
            var raycastObject = SystemMgr.Raycaster.Raycast<BattleTouchRaycastObject>();
            if (raycastObject == null || raycastObject.HitUserTile == null)
                return;

            var target = raycastObject.HitUserTile.Owner;
            if (target == null || target.TileSlotManager.CheckBlock(target.CurrTileIndex) == true)
                return;

            if (target.IsHaveBuff(BuffType.Lock))
                return;

            SystemMgr.TouchController.OnTargetTouch(target);
        }
    }

    public override void EndState()
    {
        SystemMgr.TouchController.TouchTargetEvent -= ChangeOnTarget;
    }
}
