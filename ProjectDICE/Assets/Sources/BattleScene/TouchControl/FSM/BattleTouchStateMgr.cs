﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using EightWork.FSM;
using EightWork.MsgSystem;

public abstract class BattleTouchStateBase : EightFSMStateBase<BattleTouchStateMgr.BattleTouchState, BattleTouchStateMgr>
{

}

public class BattleTouchStateMgr : EightFSMSystem<BattleTouchStateMgr.BattleTouchState, BattleTouchStateBase, BattleSceneInterface>
{
    public enum BattleTouchState
    {
        NoTarget,
        OnTarget,
        OnDrage,
    }

    [SerializeField]
    private BattleTouchController _touchController = null;
    public BattleTouchController TouchController => _touchController;

    [SerializeField]
    private BattleTouchRaycaster _raycaster = null;
    public BattleTouchRaycaster Raycaster => _raycaster;
}
