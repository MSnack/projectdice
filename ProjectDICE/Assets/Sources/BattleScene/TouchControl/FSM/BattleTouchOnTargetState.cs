﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.EventSystems;

public class BattleTouchOnTargetState : BattleTouchStateBase
{
    private Vector3 _touchPos = Vector3.zero;
    private bool _startMouseUp = false;

    protected override void Initialize()
    {
        State = BattleTouchStateMgr.BattleTouchState.OnTarget;
    }

    public override void StartState()
    {
        SystemMgr.TouchController.TargetOffEvent += OnChangeNoTarget;
    }

    private void OnChangeNoTarget()
    {
        SystemMgr.ChangeState(BattleTouchStateMgr.BattleTouchState.NoTarget);
    }

    private void Update()
    {
#if UNITY_EDITOR
        TargetCheckEditor();
#else
        TargetCheckTouch();
#endif
    }

    private void TargetCheckEditor()
    {
        if (Input.GetMouseButtonDown(0) == true)
        {
            _touchPos = Input.mousePosition;
            OnTargetDown();
        }
        else if (Input.GetMouseButton(0) == true)
        {
            if (_touchPos == Input.mousePosition)
                return;

            OnTargetDrag();
        }
        else if (Input.GetMouseButtonUp(0) == true)
        {
            OnTargetUp();
            _touchPos = Vector3.zero;
        }
    }

    private void TargetCheckTouch()
    {
        if (Input.touchCount <= 0)
            return;

        var touch = Input.GetTouch(0);
        switch (touch.phase)
        {
            case TouchPhase.Began: OnTargetDown(); break;
            case TouchPhase.Ended: OnTargetUp(); break;
            case TouchPhase.Moved: OnTargetDrag(); break;
        }
    }

    private void OnTargetDown()
    {
        if (EventSystem.current.currentSelectedGameObject != null)
            return;

        var camera = EightUtil.GetCore<EightCameraMgr>().EightCamera;
        Vector2 pos = camera.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(pos, camera.transform.forward, 0.0f);
        if (hit.collider == null)
        {
            SystemMgr.TouchController.OnTargetTouch(null);
            SystemMgr.ChangeState(BattleTouchStateMgr.BattleTouchState.NoTarget);
            return;
        }

        var tile = hit.collider.gameObject.GetComponent<UserTile>();
        var target = tile.Owner;
        if (target == null || target.TileSlotManager.CheckBlock(target) == true)
        {
            SystemMgr.TouchController.OnTargetTouch(null);
            SystemMgr.ChangeState(BattleTouchStateMgr.BattleTouchState.NoTarget);
            return;
        }

        if (SystemMgr.TouchController.Target == target)
            return;
    }

    private void OnTargetDrag()
    {
        var camera = EightUtil.GetCore<EightCameraMgr>().EightCamera;
        Vector2 pos = camera.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(pos, camera.transform.forward, 0.0f);
        if (hit.collider == null)
            return;

        var tile = hit.collider.gameObject.GetComponent<UserTile>();
        var target = tile.Owner;
        if (SystemMgr.TouchController.Target == target)
            return;

        SystemMgr.ChangeState(BattleTouchStateMgr.BattleTouchState.OnDrage);
    }

    private void OnTargetUp()
    {
        if (_startMouseUp == false)
        {
            _startMouseUp = true;
            return;
        }

        var raycastObject = SystemMgr.Raycaster.Raycast<BattleTouchRaycastObject>();
        if (raycastObject == null || raycastObject.HitUserTile == null)
        {
            SystemMgr.TouchController.Target.SpineUnit.transform.localPosition = Vector3.zero;
            SystemMgr.TouchController.OnTargetTouch(null);
            SystemMgr.ChangeState(BattleTouchStateMgr.BattleTouchState.NoTarget);
            return;
        }

        var tile = raycastObject.HitUserTile;
        var target = tile.Owner;
        if (target == null || SystemMgr.TouchController.Target == target)
        {
            SystemMgr.TouchController.Target.SpineUnit.transform.localPosition = Vector3.zero;
            SystemMgr.TouchController.OnTargetTouch(null);
            SystemMgr.ChangeState(BattleTouchStateMgr.BattleTouchState.NoTarget);
            return;
        }

        if (SystemMgr.TouchController.Target != target)
        {
            SystemMgr.TouchController.Target.SpineUnit.transform.localPosition = Vector3.zero;
            SystemMgr.TouchController.OnMergeEvent(target);
            SystemMgr.TouchController.OnTargetTouch(null);
            SystemMgr.ChangeState(BattleTouchStateMgr.BattleTouchState.NoTarget);
            return;
        }
    }

    public override void EndState()
    {
        _touchPos = Vector3.zero;
        _startMouseUp = false;
        SystemMgr.TouchController.TargetOffEvent -= OnChangeNoTarget;
    }
}
