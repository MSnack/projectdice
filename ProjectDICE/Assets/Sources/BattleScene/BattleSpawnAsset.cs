﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnData
{
    [SerializeField]
    private int _monsterId = -1;
    public int MonsterId => _monsterId;

    [SerializeField]
    private float _spawnDelay = -1.0f;
    public float SpawnDelay => _spawnDelay;

    [Header("Sp")]
    [SerializeField]
    private float _defaultSp = 10.0f;
    public float DefaultSp => _defaultSp;

    [SerializeField]
    private float _spCoeff = 10.0f;
    public float SpCoeff => _spCoeff;

    public int GetSp(int weight)
    {
        float result = _defaultSp + (_spCoeff * weight);
        return (int)result;
    }
}

[System.Serializable]
public class BossSpawnData
{
    [SerializeField]
    private int _monsterId = -1;
    public int MonsterId => _monsterId;

    [SerializeField]
    private Sprite _icon = null;
    public Sprite Icon => _icon;

    [Header("Sp")]
    [SerializeField]
    private float _defaultSp = 10.0f;
    public float DefaultSp => _defaultSp;

    [SerializeField]
    private float _spCoeff = 10.0f;
    public float SpCoeff => _spCoeff;

    public int GetSp(int weight)
    {
        float result = _defaultSp + (_spCoeff * weight);
        return (int)result;
    }
}

[CreateAssetMenu(fileName = "New BattleSpawnAsset", menuName = "AssetData/BattleSpawnAsset", order = 1)]
public class BattleSpawnAsset : ScriptableObject
{
    [SerializeField]
    private List<SpawnData> _dataList = new List<SpawnData>();
    public SpawnData[] DataList => _dataList.ToArray();

    [SerializeField]
    private List<BossSpawnData> _bossDataList = new List<BossSpawnData>();
    public BossSpawnData[] BossDataList => _bossDataList.ToArray();

    [SerializeField]
    private List<int> _loadUnitIdList = new List<int>();
    public int[] LoadUnitIdList => _loadUnitIdList.ToArray();

    [SerializeField]
    private GameEffectData _monsterSpawnEffect = null;
    public GameEffectData MonsSpawnEffect => _monsterSpawnEffect;

    [SerializeField]
    private float _spawnDelay = 0.0f;
    public float SpawnDelay => _spawnDelay;
}
