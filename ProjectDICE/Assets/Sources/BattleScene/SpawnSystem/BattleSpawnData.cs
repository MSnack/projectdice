﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BattleSpawnData
{
    public abstract GameUnit InvokSpawn(BattleManager battleManager);
}
