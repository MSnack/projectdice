﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.AddressableAssets;

public sealed class CharacterSpawnData : BattleSpawnData
{
    private int _unitId = -1;
    private int _grade = -1;
    private int _randomSeed = -1;
    private int _slotIndex = -1;

    public CharacterSpawnData(int unitId, int grade, int randomSeed, int slotIndex)
    {
        _unitId = unitId;
        _grade = grade;
        _randomSeed = randomSeed;
        _slotIndex = slotIndex;
    }

    public CharacterSpawnData(SpawnUnitData data)
    {
        _unitId = data.UnitId;
        _grade = data.Grade;
        _randomSeed = data.RandomSeed;
        _slotIndex = data.SlotIndex;
    }

    public override GameUnit InvokSpawn(BattleManager battleManager)
    {
        var unit = EightUtil.GetCore<GameUnitMgr>().CreateCharacater<CharacterController>(_unitId, _grade, _randomSeed, battleManager);
        if (unit == null)
            return null;

        unit.CurrTileIndex = _slotIndex;
        return unit;
    }
}

public class SpawnUnitData
{
    private int _unitId = -1;
    public int UnitId => _unitId;

    private int _slotIndex = -1;
    public int SlotIndex => _slotIndex;

    private int _grade = -1;
    public int Grade => _grade;

    private int _randomSeed = 0;
    public int RandomSeed => _randomSeed;

    public SpawnUnitData(int unitId, int slotIndex, int grade, int randomSeed)
    {
        _unitId = unitId;
        _slotIndex = slotIndex;
        _grade = grade;
        _randomSeed = randomSeed;
    }

    public SpawnUnitData(RequestUnitData unitData)
    {
        _unitId = unitData.UnitId;
        _slotIndex = unitData.SlotIndex;
        _grade = unitData.Grade;
        _randomSeed = unitData.RandomSeed;
    }
}

public sealed class BattleCharacterSpawner : BattleSpawnSystem
{
    [SerializeField]
    private BattleManager _battleManager = null;

    [SerializeField]
    private GameEffectDataAssetReference _spawnEffect = null;
    public bool IsLoad => _spawnEffect == null ? false : _spawnEffect.IsDone;

    [SerializeField]
    private float _spawnEffectDelay = 0.3f;

    private void Awake()
    {
        LoadEffect();
    }

    private void OnEnable()
    {
        _battleManager.OnCreateCharacterEvent += RequestEvent;
        _battleManager.OnSpawnCharacterEvent += RequestEvent;
    }

    private void OnDisable()
    {
        _battleManager.OnCreateCharacterEvent -= RequestEvent;
        _battleManager.OnSpawnCharacterEvent -= RequestEvent;
    }

    private void LoadEffect()
    {
        _spawnEffect.LoadAssetAsync();
    }

    private void RequestEvent(RequestUnitData data)
    {
        var unitData = new SpawnUnitData(data);
        OnSpawn(unitData);
    }

    public void OnSpawn(SpawnUnitData data)
    {
        if (data == null)
            return;

        var spawnData = new CharacterSpawnData(data);
        var unit = spawnData.InvokSpawn(_battleManager);
        if (unit == null)
            return;

        TileSlot slot = null;
        switch (_battleManager.ReceiverType)
        {
            case SocketDataType.Local:
                {
                    slot = _battleManager.TileSlotMgr.UseReserveSlot(data.SlotIndex, unit);
                    break;
                }

            case SocketDataType.Network:
                {
                    slot = _battleManager.TileSlotMgr.UseSlot(data.SlotIndex, unit);
                    break;
                }
        }

        if (slot == null)
            return;

        var pos = (Vector2)_battleManager.TileSlotMgr.GetTileSlot(data.SlotIndex).GetPositionForUnit();
        unit.transform.position = pos;
        unit.UnitState = GameUnitState.Spawn;

        StartCoroutine(SpawnRoutine(pos));
    }

    private IEnumerator SpawnRoutine(Vector2 pos)
    {
        yield return new WaitForSeconds(_spawnEffectDelay);

        var effectData = _spawnEffect == null ? null : _spawnEffect.Asset as GameEffectData;
        if (effectData == null)
            yield break;

        var effect = EightUtil.GetCore<GameEffectMgr>().UseEffect(effectData);

        float xDir = 1.0f;
        if (_battleManager.ReceiverType == SocketDataType.Network)
        {
            xDir = -1.0f;
        }

        int xLayer = (int)(effect.transform.position.x * xDir);
        int yLayer = (int)(effect.transform.position.y * -10.0f);
        effect.SortingOrder = 7001 + yLayer + xLayer;

        effect.transform.position = pos;
    }

    private void OnDestroy()
    {
        _spawnEffect.ReleaseAsset();
    }
}
