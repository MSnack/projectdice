﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using EightWork.MsgSystem;

public class BattlePlayTimmer : EightMsgReceiver<BattleSceneInterface>
{
    [SerializeField, ReadOnly]
    private float _battleTime = 0.0f;
    public float BattleTime => _battleTime;

    [SerializeField, ReadOnly]
    private bool _isPause = false;
    public bool IsPause { get => _isPause; set => IsPlay(!value); }

    private Coroutine _timmerCoroutine = null;

    protected override void OnEnable()
    {
        base.OnEnable();

        MsgInterface.PlayEvent += IsPlay;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        MsgInterface.PlayEvent -= IsPlay;
    }

    public void StartTimmer()
    {
        if (_timmerCoroutine != null)
            return;

        _timmerCoroutine = StartCoroutine(TimeRoutine());
    }

    private void IsPlay(bool isPlay)
    {
        _isPause = !isPlay;
    }

    private IEnumerator TimeRoutine()
    {
        while (true)
        {
            if (_isPause == true)
                yield return new WaitUntil(() => _isPause == false);

            _battleTime += Time.deltaTime;
            yield return null;
        }
    }
}
