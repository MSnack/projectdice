﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EightWork;
using EightWork.MsgSystem;

[System.Serializable]
public class BattleInitUserData
{
    [SerializeField, ReadOnly]
    private List<BattleCardData> _unitList = new List<BattleCardData>();
    public BattleCardData[] UnitList => _unitList?.ToArray();
    public int UnitCount => _unitList.Count;

    [SerializeField, ReadOnly]
    private string _userName = null;
    public string UserName => _userName;

    [SerializeField, ReadOnly]
    private int _rating = 0;
    public int Rating => _rating;

    [SerializeField, ReadOnly]
    private int _userLevel = 0;
    public int UserLevel => _userLevel;

    public BattleInitUserData(BattleCardData[] unitList, string userName, int rating, int level)
    {
        _unitList.AddRange(unitList);
        _userName = userName;
        _rating = rating;
        _userLevel = level;
    }

    public BattleCardData GetUnitData(int unitId)
    {
        foreach (var data in _unitList)
        {
            if (data.UnitID == unitId)
                return data;
        }

        return null;
    }
}

[System.Serializable]
public class BattleCardData
{
    [SerializeField, ReadOnly]
    private int _unitId = -1;
    public int UnitID => _unitId;

    [SerializeField, ReadOnly]
    private int _level = 0;
    public int Level => _level;

    [SerializeField, ReadOnly]
    private int _reinforce = 0;
    public int Reinforce => _reinforce;

    public event UnityAction<BattleCardData> UpdateEvent = null;

    public BattleCardData(int unitId, int level)
    {
        _unitId = unitId;
        _level = level;
        _reinforce = 0;
    }

    public void UpgradeForce()
    {
        _reinforce++;
        UpdateEvent?.Invoke(this);
    }
}

public class BattleUserManager : EightMsgReceiver<BattleSceneInterface>
{
    [SerializeField, ReadOnly]
    private BattleInitUserData _userData = null;
    public BattleInitUserData UserData => _userData;

    public event UnityAction<BattleInitUserData> OnInitEvent = null;

    [System.Serializable]
    private class SpSetting
    {
        [SerializeField]
        private int _spDefault = 10;
        public int SpDefault => _spDefault;

        [SerializeField]
        private int _spCoeff = 10;
        public int SpCoeff => _spCoeff;

        [SerializeField]
        private int _startSp = 100;
        public int StartSp => _startSp;

        public int GetNeedSp(int index)
        {
            int need = _spDefault + (SpCoeff * index);
            return need;
        }
    }

    [SerializeField, ReadOnly]
    private int _sp = 0;
    public int Sp => _sp;

    [SerializeField, ReadOnly]
    private int _createCount = 0;
    public int CreateCount => _createCount;

    public bool IsPossibleCreateUnit
    {
        get { return _sp >= NeedSp ? true : false; }
    }

    [SerializeField]
    private SpSetting _spSetting = null;
    public int NeedSp
    {
        get
        {
            if (_spSetting == null)
                return 0;

            return _spSetting.GetNeedSp(_createCount);
        }
    }

    public event UnityAction<int, int> UpdateSpEvent = null;

    [SerializeField]
    private int _startLife = 10;

    [SerializeField, ReadOnly]
    private int _life = 0;
    public int Life => _life;

    [SerializeField]
    private int _maxLife = 5;
    public int MaxLIfe => _maxLife;

    [SerializeField, ReadOnly]
    private bool _isReady = false;
    public bool IsReady { get => _isReady; set => _isReady = value; }

    public UnityEvent OnDieEvent = null;

    public event UnityAction<int, int> UpdateLifeEvent = null;

    [SerializeField, ReadOnly]
    private bool _leaveUser = false;
    public bool LeaveUser => _leaveUser;

    [SerializeField, ReadOnly]
    private bool _isInit = false;
    public bool IsInit => _isInit;

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        if (_isInit == true)
            return;

        _sp = _spSetting.StartSp;
        _life = _startLife;
        _isInit = true;

        UpdateSpEvent?.Invoke(Sp, NeedSp);
        UpdateLifeEvent?.Invoke(_life, 0);
    }

    public void CallSpEvent()
    {
        if (_isInit == false)
        {
            Init();
            return;
        }

        UpdateSpEvent?.Invoke(Sp, NeedSp);
    }

    public void CallLifeEvent()
    {
        if (_isInit == false)
        {
            Init();
            return;
        }

        UpdateLifeEvent?.Invoke(Life, 0);
    }

    public void InitUserInfo(PostUnitInfo[] unitList, string userName, int rating, int level)
    {
        var skinData = EightUtil.GetCore<GameSkinMgr>();
        var userDeck = new List<BattleCardData>();
        foreach (var unitInfo in unitList)
        {
            var cradData = new BattleCardData(unitInfo.unitId, unitInfo.unitLevel);
            //skinData.UploadSkin(unitInfo.unitId);

            userDeck.Add(cradData);
        }

        _userData = new BattleInitUserData(userDeck.ToArray(), userName, rating, level);
        OnInitEvent?.Invoke(_userData);
    }

    public void SetEventManager(BattleManager battleManager)
    {
        if (battleManager == null)
            return;

        battleManager.OnCreateCharacterEvent += OnCreateCharacterEvent;
        battleManager.Spawner.OnCreateMonster += OnCreateMonsterUnit;
        battleManager.Spawner.OnBossCreateEvent += OnCreateMonsterUnit;
        battleManager.UpgradeUnitEvent += UpdateEvent;
        battleManager.BattleLeaveSyncEvent += BattleLeaveSyncEvent;
        battleManager.BattleDisconnectEvent += BattleDisconnectEvent;
    }

    private void BattleDisconnectEvent()
    {
        _leaveUser = true;
    }

    private void BattleLeaveSyncEvent()
    {
        _leaveUser = true;
    }

    private void UpdateEvent(RequestUpgradeData data)
    {
        var unitData = UserData.GetUnitData(data.UnitId);
        if (data == null || _sp < data.NeedSp)
        {
            Debug.LogError("UpgradeCard Not have Sp\n" + "Have : " + _sp.ToString() + "\n" + "Need : " + data.NeedSp.ToString());
            return;
        }

        _sp -= data.NeedSp;
        unitData.UpgradeForce();
        UpdateSpEvent?.Invoke(_sp, NeedSp);
    }

    public bool PossibleCreateUnit => _sp >= NeedSp;

    private void OnCreateCharacterEvent(RequestUnitData data)
    {
        if (data.ReceiveType == SocketDataType.Local)
            return;

        OnCreateCharacterEvent();
    }

    public void OnCreateCharacterEvent()
    {
        _sp -= NeedSp;
        _createCount += 1;
        UpdateSpEvent?.Invoke(_sp, NeedSp);
    }

    private void OnCreateMonsterUnit(GameUnit unit)
    {
        unit.OnHitDie += OnHitDie;
        unit.OnRoadEndDie += OnTileRoadEndDie;
    }

    private void OnTileRoadEndDie(GameUnit unit)
    {
        unit.OnRoadEndDie -= OnTileRoadEndDie;
        OnDieMonster(unit.Power);
    }

    private void OnHitDie(GameUnit unit)
    {
        unit.OnHitDie -= OnHitDie;

        _sp += unit.Sp;
        UpdateSpEvent?.Invoke(_sp, NeedSp);
    }

    public void OnDieMonster(int life)
    {
        OnHit(life);
    }

    public void OnHit(int damage)
    {
        if (_life <= 0)
            return;

        var saveLife = _life;
        _life -= damage;
        if (_life <= 0)
        {
            _life = 0;
            OnDieEvent?.Invoke();
        }

        var lifeDir = _life - saveLife;
        UpdateLifeEvent?.Invoke(_life, lifeDir);
    }

    public void OnRecovery(int recovery)
    {
        if (_life >= _maxLife)
            return;

        var saveLife = _life;
        _life += recovery;
        var lifeDir = _life - saveLife;
        UpdateLifeEvent?.Invoke(_life, lifeDir);
    }
}
