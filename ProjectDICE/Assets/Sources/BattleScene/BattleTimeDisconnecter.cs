﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.Events;

public class BattleTimeDisconnecter : MonoBehaviour
{
    [SerializeField]
    private float _disconnectTime = 60.0f;
    public float DisconnectTime => _disconnectTime;

    [SerializeField, ReadOnly]
    private float _nowTime = 0.0f;
    public float NowTime => _nowTime;

    private Coroutine _disconnectRoutine = null;

    [SerializeField]
    public UnityEvent DisconnectEvent = null;

    private IEnumerator DisconnectCheckRoutine()
    {
        while (_disconnectTime > _nowTime)
        {
            _nowTime += Time.deltaTime;
            yield return null;
        }

        DisconnectEvent?.Invoke();
    }

    public void DisconnectCheck()
    {
        if (_disconnectRoutine != null)
            StopDisconnectRoutine();

        _nowTime = 0.0f;
        StartDisconnectRoutine();
    }

    public void ResetWaitTime()
    {
        _nowTime = 0.0f;
    }

    public void StopDisconnectTime()
    {
        if (_disconnectRoutine != null)
            StopDisconnectRoutine();

        _nowTime = 0.0f;
    }

    private void StartDisconnectRoutine()
    {
        if (_disconnectRoutine != null)
            return;

        _disconnectRoutine = StartCoroutine(DisconnectCheckRoutine());
    }

    private void StopDisconnectRoutine()
    {
        if (_disconnectRoutine == null)
            return;

        StopCoroutine(_disconnectRoutine);
        _disconnectRoutine = null;
    }
}
