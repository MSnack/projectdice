﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleLifeController : MonoBehaviour
{
    [SerializeField]
    private BattleUserManager _userManager = null;

    [SerializeField]
    private HpController _HpController = null;

    private void Awake()
    {
        if (_HpController == null)
            return;

        _userManager.UpdateLifeEvent += _HpController.UpdateHp;
    }

    private void OnEnable()
    {
        _userManager.CallLifeEvent();
    }

    private void UpdateLife(int life, int dir)
    {
        if (_HpController == null)
            return;

        _HpController.UpdateHp(life, 0);
    }
}
