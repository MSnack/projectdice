﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.MsgSystem;
using UnityEngine.Events;
using EightWork;

public class BattleRoulette : EightMsgReceiver<BattleSceneInterface>
{
    [SerializeField]
    private BossIconRouletteController _iconRoulette = null;

    [SerializeField]
    private UnityEvent _onEndRouette = null;
    public event UnityAction OnEndRouette
    {
        add => _onEndRouette.AddListener(value);
        remove => _onEndRouette.RemoveListener(value);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        _iconRoulette.OnEndAniEvent += OnEndAni;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        _iconRoulette.OnEndAniEvent -= OnEndAni;
    }

    public void OnNextStageRoulette(BattleMonsterData[] monsterList, int bossId)
    {
        var bossIndex = -1;
        var monsterDataList = new List<MonsterUnitData>();
        var unitMgr = EightUtil.GetCore<GameUnitDataMgr>();
        for (int i = 0; i < monsterList.Length; ++i)
        {
            var battleMonsterData = monsterList[i];
            var monsterData = unitMgr.GetMonsterData(battleMonsterData.UnitId);
            if (monsterData == null)
                continue;

            if (monsterData.DataID == bossId)
                bossIndex = monsterDataList.Count;

            monsterDataList.Add(monsterData);
        }

        _iconRoulette.OnRoulette(monsterDataList.ToArray(), bossIndex);
    }

    private void OnEndAni()
    {
        _onEndRouette?.Invoke();
    }
}
