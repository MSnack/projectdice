﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlePlayBossSelectMode : BattlePlayModeStateBase
{
    [SerializeField]
    private BattleMonsterController _monsterCntroller = null;

    [SerializeField]
    private BattleRoulette _roulette = null;

    protected override void Initialize()
    {
        State = BattlePlayFSM.BattlePlayMode.BossSelect;
    }

    public override void StartState()
    {
        base.StartState();

        SystemMgr.MsgInterface.UpdateAllUnitSeed();

        _roulette.OnEndRouette += OnEndRoulette;

        var bossId = _monsterCntroller.UpdateBossId();
        var bossList = _monsterCntroller.GetMonsterList(BattleMonsterData.MonsterType.Boss);
        _roulette.OnNextStageRoulette(bossList, bossId);
        //SystemMgr.MsgInterface.PauseGame();
    }

    private void OnEndRoulette()
    {
        SystemMgr.ChangeState(BattlePlayFSM.BattlePlayMode.NormalWave);
    }

    public override void EndState()
    {
        base.EndState();

        //SystemMgr.MsgInterface.PlayGame();

        _roulette.OnEndRouette -= OnEndRoulette;
    }
}
