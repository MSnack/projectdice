﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class BattlePlayBossSpawnMode : BattlePlayModeStateBase
{
    [SerializeField]
    private GameEffectDataAssetReference _phaseEffectData = null;

    [SerializeField]
    private List<BattleManager> _battleManagerList = null;

    protected override void Awake()
    {
        _phaseEffectData.LoadAssetAsync();
    }

    protected override void Initialize()
    {
        State = BattlePlayFSM.BattlePlayMode.BossSpawn;
    }

    public override void StartState()
    {
        base.StartState();

        SystemMgr.MsgInterface.UpdateAllUnitSeed();

        var phaseEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_phaseEffectData.Asset as GameEffectData);
        if (phaseEffect == null)
        {
            BossSpawn();
            return;
        }

        phaseEffect.OnReturnEvent += BossSpawn;
    }

    private void BossSpawn()
    {
        foreach (var battleManager in _battleManagerList)
        {
            var totalSp = 0;
            var totalHp = 0;
            foreach (var unit in battleManager.TileRoadMgr.TileUnitListAtLive)
            {
                totalHp += unit.Hp;
                totalSp += unit.Sp;
            }

            battleManager.TileRoadMgr.ClearRoadUnit();
            battleManager.Spawner.BossSpawnEvent(totalHp, totalSp);
        }

        SystemMgr.ChangeState(BattlePlayFSM.BattlePlayMode.BossWave);
    }

    public override void EndState()
    {
        base.EndState();
    }
}
