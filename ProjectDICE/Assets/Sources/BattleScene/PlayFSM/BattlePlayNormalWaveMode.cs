﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlePlayNormalWaveMode : BattlePlayModeStateBase
{
    [SerializeField]
    private BattleWaveController _waveController = null;

    [SerializeField]
    private BattleMonsterController _monsterController = null;

    protected override void Initialize()
    {
        State = BattlePlayFSM.BattlePlayMode.NormalWave;
    }

    public override void StartState()
    {
        base.StartState();

        _waveController.WaveEndEvent += OnWaveEnd;

        _monsterController.UpdateSpawnMonsterData();
        _waveController.StartBattleWave();
    }

    private void OnWaveEnd()
    {
        _waveController.WaveEndEvent -= OnWaveEnd;

        SystemMgr.ChangeState(BattlePlayFSM.BattlePlayMode.BossSpawn);
    }

    public override void EndState()
    {
        base.EndState();
    }
}
