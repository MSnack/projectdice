﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.FSM;

public class BattlePlayFSM : EightFSMSystem<BattlePlayFSM.BattlePlayMode, BattlePlayModeStateBase, BattleSceneInterface>
{
    public enum BattlePlayMode
    {
        Empty = -1,
        BossSelect,
        NormalWave,
        BossSpawn,
        BossWave,


        UNKOWN = -9999,
    }

    public void OnPlayStart()
    {
        if (CurrState != BattlePlayMode.Empty)
            return;

        ChangeState(BattlePlayMode.BossSelect);
    }

    public void OnPlayEmpty()
    {
        ChangeState(BattlePlayMode.Empty);
    }
}
