﻿using System.Collections;
using System.Collections.Generic;
using EightWork.FSM;
using UnityEngine;
using UnityEngine.Events;

public abstract class BattlePlayModeStateBase : EightFSMStateBase<BattlePlayFSM.BattlePlayMode, BattlePlayFSM>
{
    [SerializeField]
    private UnityEvent _startEvent = null;
    public event UnityAction StartEvent
    {
        add => _startEvent.AddListener(value);
        remove => _startEvent.RemoveListener(value);
    }

    [SerializeField]
    private UnityEvent _endEvent = null;
    public event UnityAction EndEvent
    {
        add => _endEvent.AddListener(value);
        remove => _endEvent.RemoveListener(value);
    }

    public override void StartState()
    {
        _startEvent?.Invoke();
    }

    public override void EndState()
    {
        _endEvent?.Invoke();
    }
}
