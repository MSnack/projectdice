﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class BattlePlayBossWaveMode : BattlePlayModeStateBase
{
    [SerializeField]
    private BattleStageController _stageController = null;

    [SerializeField]
    private BattleManager _localManager = null;

    [SerializeField, ReadOnly]
    private bool _localClear = false;

    [SerializeField]
    private BattleManager _defaultManager = null;

    [SerializeField, ReadOnly]
    private bool _defaultClear = false;

    protected override void Initialize()
    {
        State = BattlePlayFSM.BattlePlayMode.BossWave;
    }

    public override void StartState()
    {
        base.StartState();

        _localManager.BossControlCenter.OnBossDie += OnLocalBossDie;
        _defaultManager.BossControlCenter.OnBossDie += OnDefaultBossDie;
    }

    private void OnLocalBossDie()
    {
        _localClear = true;
        OnDieChecker();
    }

    private void OnDefaultBossDie()
    {
        _defaultClear = true;
        OnDieChecker();
    }

    private void OnDieChecker()
    {
        if (_localClear == false || _defaultClear == false)
            return;

        StartCoroutine(OnSameChecker());
    }

    private IEnumerator OnSameChecker()
    {
        SystemMgr.MsgInterface.SocketManager.EmitBossClearSync(true);
        yield return new WaitUntil(() => SyncCheck() == true);

        //SystemMgr.MsgInterface.SocketManager.EmitBossClearSync(false);
        //yield return new WaitUntil(() => SyncCheck(false) == true);

        ResetSync();
        _stageController.UpdateNextStage();
        SystemMgr.ChangeState(BattlePlayFSM.BattlePlayMode.BossSelect);
        //yield return null;
    }

    private void ResetSync()
    {
        _localManager.BossControlCenter.ResetSync();
        _defaultManager.BossControlCenter.ResetSync();
    }

    private bool SyncCheck()
    {
        return true;

        var localLeave = _localManager.UserManager.LeaveUser;
        var defaultLeave = _defaultManager.UserManager.LeaveUser;

        if (localLeave == true || defaultLeave == true)
            return true;

        var localSync = _localManager.BossControlCenter.IsSyncClear;
        var defaultSync = _defaultManager.BossControlCenter.IsSyncClear;

        var totalSync = localSync == true && defaultSync == true;
        return totalSync;
    }

    public override void EndState()
    {
        base.EndState();

        _localManager.BossControlCenter.OnBossDie -= OnLocalBossDie;
        _defaultManager.BossControlCenter.OnBossDie -= OnDefaultBossDie;

        _localClear = false;
        _defaultClear = false;
    }
}
