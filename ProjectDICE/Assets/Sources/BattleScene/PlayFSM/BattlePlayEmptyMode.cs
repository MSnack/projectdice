﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlePlayEmptyMode : BattlePlayModeStateBase
{
    protected override void Initialize()
    {
        State = BattlePlayFSM.BattlePlayMode.Empty;
    }

    public override void StartState()
    {
        base.StartState();
    }

    public override void EndState()
    {
        base.EndState();
    }
}
