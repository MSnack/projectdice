﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EightWork;
using UnityEngine.Events;

public class BattleResultController : MonoBehaviour
{
    [SerializeField]
    private GameObject _panelObject = null;

    [SerializeField]
    private BattleResultPanel _winPanel = null;

    [SerializeField]
    private BattleResultPanel _losePanel = null;

    [SerializeField]
    private BattleResultRewardContent _rewardContent = null;

    [SerializeField]
    private BattleUserManager _localUserManager = null;

    [SerializeField]
    private BattleUserManager _networkuserManager = null;

    [SerializeField]
    private UnityEvent _onResultEvent = null;

    [SerializeField]
    private Animator _animator = null;

    private int _sendSuccessCount = 0;

    private void Awake()
    {
        _panelObject.SetActive(false);
    }

    public void OnResult(BattleResultReceiver resultReceiver, UnityAction successEvent = null)
    {
        bool isWin = resultReceiver.IsWin;
        //StartCoroutine(SendResult(isWin, successEvent));
        
        _winPanel.UpdateResultData(resultReceiver, _localUserManager, true);
        _losePanel.UpdateResultData(resultReceiver, _networkuserManager, false);
        _rewardContent.InitData(resultReceiver);
        
        _panelObject.SetActive(true);
        _animator.enabled = true;
        _animator.Rebind();
        _animator.SetBool("isWin", isWin);
        _onResultEvent?.Invoke();
    }

    //private IEnumerator SendResult(bool isWin, UnityAction successEvent = null)
    //{
    //    int totalSendCount = 5;
    //    int trophy = isWin ? 14 : -7; //트로피 임시 값
    //    GameBattleInfo battleInfo = null;
    //    BattleInfoServerConnector.GetBattleInfo((received) => { battleInfo = received; });
    //    //Send 1, 2
    //    if (isWin)
    //    {
    //        ++battleInfo.Win;
    //        QuestUtil.UpdateQuestEvent(QuestCondition.WinGame, 1, true, () => ++_sendSuccessCount);
    //        QuestUtil.UpdateQuestEvent(QuestCondition.GetTrophy, trophy, true, () => ++_sendSuccessCount);
    //    }
    //    else
    //    {
    //        ++battleInfo.Lose;
    //        _sendSuccessCount += 2;
    //    }

    //    float exp = Random.Range(0.1f, 1.0f);
    //    float prevLevel = battleInfo.Level;
    //    battleInfo.Level += exp;

    //    //Send 3
    //    if ((int) battleInfo.Level > (int) prevLevel)
    //        QuestUtil.UpdateQuestEvent(QuestCondition.UserLevelUp, 1, true, () => ++_sendSuccessCount);
    //    ++_sendSuccessCount;


    //    BattleInfoServerConnector.UpdateBattleInfo(() =>
    //    {
    //        //Send 4
    //        ++_sendSuccessCount;

    //        //Send 5
    //        int currTrophy = LobbyGoodsUtil.GetGoods(GoodsID.TROPHY);
    //        if (currTrophy >= 16000)
    //        {
    //            LobbyGoodsServerConnector.IncrementGoods(GoodsID.TROPHY, currTrophy - 16000, () => ++_sendSuccessCount);
    //        }
    //        else if (currTrophy + trophy > 16000)
    //        {
    //            trophy = 16000 - currTrophy;
    //            LobbyGoodsServerConnector.IncrementGoods(GoodsID.TROPHY, trophy, () => ++_sendSuccessCount);
    //        }
    //        else
    //        {
    //            LobbyGoodsServerConnector.IncrementGoods(GoodsID.TROPHY, trophy, () => ++_sendSuccessCount);
    //        }
    //    });
        
    //    var opponentDeckRawList = _networkuserManager.UserData.UnitList;
    //    var opponentDeckList = new List<int>();

    //    foreach (var cardData in opponentDeckRawList)
    //    {
    //        opponentDeckList.Add(cardData.UnitID);
    //    }
        
    //    BattleHistoryUtil.AddBattleHistory(new BattleHistoryLocalData()
    //    {
    //        IsWin = isWin,
    //        MatchId = 0,
    //        OpponentDeck = opponentDeckList.ToArray(),
    //        OpponentName = _networkuserManager.UserData.UserName,
    //        OpponentTier = _networkuserManager.UserData.Rating,
    //        OpponentTrophy = 0,
    //        TrophyIncrementValue = trophy,
    //    });
        
    //    //Send 6
    //    //BattleHistoryServerConnector
        
    //    if(_sendSuccessCount < totalSendCount)
    //        yield return new WaitUntil(() => _sendSuccessCount >= totalSendCount);
        
    //    successEvent?.Invoke();
    //}
}
