﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleUserProfileController : MonoBehaviour
{
    [SerializeField]
    private BattleUserManager _userManager = null;

    [SerializeField]
    private Text _trophyText = null;

    [SerializeField]
    private Text _nameText = null;

    [SerializeField]
    private Text _guildText = null;

    [SerializeField]
    private Image _guildImage = null;

    [SerializeField]
    private Image _TierImage = null;

    private void OnEnable()
    {
        _userManager.OnInitEvent += UpdateUserInfo;
    }

    private void OnDisable()
    {
        _userManager.OnInitEvent -= UpdateUserInfo;
    }

    private void UpdateUserInfo(BattleInitUserData data)
    {
        _nameText.text = data.UserName;
        // _levelText.text = data.UserLevel.ToString();
        //_guildImage.transform.position;
        _TierImage.sprite = TierUtil.GetTierSprite(TierUtil.GetTierState(data.Rating));
        _trophyText.text = $"{data.Rating:##,###}";
    }
}
