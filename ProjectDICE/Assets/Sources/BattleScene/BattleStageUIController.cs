﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EightWork;

public class BattleStageUIController : MonoBehaviour
{
    [SerializeField]
    private BattleWaveController _waveController = null;

    [SerializeField]
    private BattleMonsterController _monsterController = null;

    [SerializeField]
    private Text _minuteText = null;

    [SerializeField]
    private Text _secoendText = null;

    [SerializeField]
    private string _minuteFormat = null;

    [SerializeField]
    private string _seconedFormat = null;

    [SerializeField]
    private ImageAutoScaler _stageImage = null;
    
    [SerializeField]
    private ImageAutoScaler _stagePrevImage = null;

    [SerializeField]
    private List<Sprite> _bossIconList = new List<Sprite>();

    [SerializeField]
    private Sprite _stateCommonIcon = null;

    [SerializeField]
    private Animator _iconAnimator = null;


    private void OnEnable()
    {
        //_waveController.UpdateBossIndexEvent += UpdateBoss;
        _monsterController.UpdateNewBossEvent += UpdateBoss;
        _waveController.LeftTimeEvent += UpdateTimer;

        UpdateTimer(0.0f);
    }

    private void OnDisable()
    {
        _monsterController.UpdateNewBossEvent -= UpdateBoss;
        _waveController.LeftTimeEvent -= UpdateTimer;
    }

    private void UpdateBoss(int bossId)
    {
        var bossData = EightUtil.GetCore<GameUnitDataMgr>().GetMonsterData(bossId);
        if (bossData == null)
            return;

        _stagePrevImage.sprite = _stageImage.sprite;
        _stageImage.sprite = bossData.PreviewImage;
        _stagePrevImage.SetNativeRatioSize();
        _stageImage.SetNativeRatioSize();
        _iconAnimator.Rebind();
        //Debug.Log("tag"+_waveController.CurrBossIndex);
        //_stageImage.sprite = _bossIconList[_waveController.CurrBossIndex];
    }

    private void UpdateTimer(float time)
    {
        int minute = (int)(time / 60.0f);
        int second = (int)(time % 60.0f);
        
        var minuteText = string.Format(_minuteFormat, minute);
        _minuteText.text = minuteText;

        var secondText = string.Format(_seconedFormat, second);
        _secoendText.text = secondText;
    }

    public void UpdateBossStateUI()
    {
        _stagePrevImage.sprite = _stageImage.sprite;
        _stageImage.sprite = _stateCommonIcon;
        _stagePrevImage.SetNativeRatioSize();
        _stageImage.SetNativeRatioSize();
        _iconAnimator.Rebind();
    }
}
