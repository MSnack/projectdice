﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class BattleResultReceiver : MonoBehaviour
{
    [SerializeField]
    private BattleSocket _socket = null;

    [SerializeField]
    private BattleUserManager _localUserManager = null;

    [SerializeField]
    private BattleUserManager _defaultUserManager = null;

    [SerializeField, ReadOnly]
    private bool _isWin = false;
    public bool IsWin => _isWin;

    [SerializeField, ReadOnly]
    private int _updateTrophy = 0;
    public int UpdateTrophy => _updateTrophy;

    [SerializeField, ReadOnly]
    private int _getGold = 0;
    public int GetGold => _getGold;

    [SerializeField, ReadOnly]
    private int _getBoxId = -1;
    public int GetBoxId => _getBoxId;

    [SerializeField, ReadOnly]
    private bool _isResult = false;
    public bool IsResult => _isResult;

    [SerializeField, ReadOnly]
    private bool _isPostEvent = false;
    public bool IsPostEvent => _isPostEvent;

    private void OnEnable()
    {
        _socket.BattleResultEvent += BattleResultEvent;
    }

    private void OnDisable()
    {
        _socket.BattleResultEvent -= BattleResultEvent;
    }

    private void BattleResultEvent(RequestBattleResultData data)
    {
        _isWin = data.IsWin;
        _updateTrophy = data.UpdateTrophy;
        _getGold = data.GetGold;
        _getBoxId = data.GetBoxId;

        _isResult = true;
        _isPostEvent = false;
    }

    public void OnResultEvent(bool isWin)
    {
        if (_isPostEvent == true || _isResult == true)
            return;

        var roomId = _socket.SocketMgr.RoomId;
        var userId = EightUtil.GetCore<GameUserMgr>().UserInfo.UserID;

        _socket.EmitBattleResult(_localUserManager.UserData, _defaultUserManager.UserData, isWin);
        _isPostEvent = true;
    }
}
