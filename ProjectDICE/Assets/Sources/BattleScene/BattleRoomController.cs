﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

using UnityEngine.Events;

public class BattleRoomController : EightReceiver
{
    [System.Serializable]
    private class BattleGiveUpEvent : UnityEvent<SocketDataType> { }

    [SerializeField]
    private BattleSocket _socket;

    [SerializeField, ReadOnly]
    private bool _isLeaveRoom = false;
    public bool _IsLeaveRoom => _isLeaveRoom;

    [SerializeField]
    private BattleGiveUpEvent BattleGiveUp = null;

    protected override void OnEnable()
    {
        base.OnEnable();
        _socket.OnGiveUpEvent += OnGiveUpEvent;
    }

    private void OnGiveUpEvent(RequestBattleGiveUp data)
    {
        BattleGiveUp?.Invoke(data.ReceiveType);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        _socket.OnGiveUpEvent -= OnGiveUpEvent;
    }

    protected override void RegisterMsg()
    {
        base.RegisterMsg();
        RegisterEvent(EIGHT_MSG.CHANGE_SCENE_START, ChangeSceneStart);
    }

    protected override void UnRegisterMsg()
    {
        base.UnRegisterMsg();
        RemoveEvent(EIGHT_MSG.CHANGE_SCENE_START, ChangeSceneStart);
    }

    private void ChangeSceneStart(EightMsgContent content)
    {
        LeaveBattleRoom();
    }

    public void LeaveBattleRoom()
    {
        Debug.Log("LeaveRoom!");
        _socket.EmitLeaveBattle();
    }
}
