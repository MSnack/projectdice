﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.AddressableAssets;

public class BattleUpgradeCardSlotMgr : MonoBehaviour
{
    [SerializeField]
    private BattleSocket _receiver = null;

    [SerializeField]
    private BattleManager _battleManager = null;

    [SerializeField]
    private List<BattleCharSelectSlotObject> _cardSlotList = null;
    public int SlotCount => _cardSlotList.Count;

    [SerializeField]
    private GameEffectDataAssetReference _slotEffectData = null;

    private void Awake()
    {
        _slotEffectData.LoadAssetAsync();
    }

    protected void OnEnable()
    {
        _battleManager.UserManager.OnInitEvent += InitEvent;
        _battleManager.UserManager.UpdateSpEvent += UpdateSpEvent;
    }

    protected void OnDisable()
    {
        _battleManager.UserManager.UpdateSpEvent -= UpdateSpEvent;
        _battleManager.UserManager.OnInitEvent -= InitEvent;
    }

    private void UpdateSpEvent(int sp, int needSp)
    {
        for(int i = 0; i < _cardSlotList.Count; ++i )
        {
            var slot = _cardSlotList[i];
            if (slot == null)
                continue;

            if (slot.IsMax == true)
                continue;

            var cardData = _battleManager.UserManager.UserData.UnitList[i];
            if (cardData == null)
                continue;

            var unitData = EightUtil.GetCore<GameUnitDataMgr>().GetCharacaterData(cardData.UnitID);
            if (unitData == null)
                continue;

            var reinforceData = unitData.ReinforceData?.DataList;
            if (reinforceData == null)
                continue;

            int upgradeNeedSp = reinforceData[cardData.Reinforce];
            if (upgradeNeedSp <= sp)
            {
                slot.OnSlot();
            }
            else
                slot.OffSlot();
        }
    }

    private void InitEvent(BattleInitUserData data)
    {
        var unitLIst = data.UnitList;
        for (int i = 0; i < unitLIst.Length; ++i)
        {
            _cardSlotList[i].OnInitSlot(unitLIst[i]);
        }

        UpdateSpEvent(_battleManager.UserManager.Sp, _battleManager.UserManager.NeedSp);
    }

    public void OnUpgrade(int index)
    {
        if (_battleManager.TileSlotMgr.UseSlotList.Length <= 0)
            return;

        var slot = _cardSlotList[index];
        if (slot == null)
            return;

        if (slot.ReceiveUpgrade == true ||
            slot.IsBlock == true)
            return;

        var cardData = _battleManager.UserManager.UserData.UnitList[index];
        if (cardData == null)
            return;

        var unitData = EightUtil.GetCore<GameUnitDataMgr>().GetCharacaterData(cardData.UnitID);
        if (unitData == null)
            return;

        var reinforceData = unitData.ReinforceData?.DataList;
        if (reinforceData == null)
            return;

        int needSp = reinforceData[cardData.Reinforce];
        if (needSp > _battleManager.UserManager.Sp)
            return;

        //if (unitData == null ||
        //    unitData.MaxUpgrade >= cardData.Reinforce)
        //    return;

        var effect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_slotEffectData.Asset as GameEffectData);
        if (effect != null)
        {
            effect.transform.position = slot.transform.position;
            //effect.SortingOrder = 15100;
        }

        slot.ReserveSlot();
        _receiver.OnUpgrade(cardData.UnitID, needSp);
    }
}
