﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BattleRiskEventController : MonoBehaviour
{

    [SerializeField]
    private UnityEvent _riskPlayerEvent;

    [SerializeField]
    private UnityEvent _riskOtherPlayerEvent;

    public void OnRiskEvent(bool isPlayer)
    {
        if (isPlayer)
        {
            _riskPlayerEvent?.Invoke();
        }
        else
        {
            _riskOtherPlayerEvent?.Invoke();
        }
    }
}
