﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.Events;

public class BattleTimeSyncer : MonoBehaviour
{
    [SerializeField]
    private BattleSocket _socket;

    [SerializeField, ReadOnly]
    private long _nowSync = 0;
    public long NowSync => _nowSync;

    [SerializeField, ReadOnly]
    private long _totalSync = 0;
    public long TotalSync => _totalSync;

    [SerializeField]
    private long _saveSync = 0;
    public long SaveSync => _saveSync;

    [SerializeField, ReadOnly]
    private bool _isSync = false;
    public bool IsSync => _isSync;

    [SerializeField, ReadOnly]
    private bool _isTuning = false;
    public bool IsTuning => _isTuning;

    private void OnEnable()
    {
        _socket.OnTimeSyncEvent += TimeSyncEvent;
    }

    private void OnDisable()
    {
        _socket.OnTimeSyncEvent -= TimeSyncEvent;
    }

    public void OnTimeSync()
    {
        if (_isTuning == true || _isSync == true)
            return;

        _socket.EmitBattleTimeSync();
        _isTuning = true;
    }

    private void TimeSyncEvent(RequestBattleTimeSyncData data)
    {
        if (_isSync == true)
            return;

        _nowSync = data.TimeDistance;
        _totalSync += data.TimeDistance;

        Debug.Log("NowSync : " + _nowSync.ToString() + "\n" +
                "TotalSync : " + _totalSync.ToString());

        _isSync = SyncCheck();
        if (_isSync == false)
            _socket.EmitBattleTimeSync();
        else
            _isTuning = false;
    }

    private bool SyncCheck()
    {
        if (_nowSync == 0)
            return true;

        if (_totalSync == 0)
            return false;

        var nowDir = _nowSync / Mathf.Abs(_nowSync);
        var totalDir = _totalSync / Mathf.Abs(_totalSync);

        if (nowDir != totalDir || Mathf.Abs(_nowSync) > _saveSync)
            return false;

        return true;
    }
}
