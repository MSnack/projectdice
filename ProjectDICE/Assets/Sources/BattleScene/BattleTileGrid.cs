﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.UI;

public class BattleTileGrid : MonoBehaviour
{
    [SerializeField]
    private Vector2 _cellSize = Vector2.zero;
    public Vector2 CellSize
    {
        get => _cellSize;
        set => _cellSize = value;
    }

    [SerializeField]
    private Vector2 _spacing = Vector2.zero;
    public Vector2 Spacing
    {
        get => _spacing;
        set => _spacing = value;
    }

    [SerializeField]
    private float _lineSpacing = 0.0f;
    public float LineSpacing
    {
        get => _lineSpacing;
        set => _lineSpacing = value;
    }

    [SerializeField]
    private int _count = 1;
    public int Count
    {
        get => _count;
        set => _count = value;
    }

    private void OnDrawGizmos()
    {
        OnUpdateGridTile();
    }

    private void OnUpdateGridTile()
    {
        int line = 0;
        int saveIndex = 0;
        var childList = this.transform.GetComponentsInChildren<Image>();
        for (int i = childList.Length - 1; i >= 0; --i)
        {
            int currIndex = childList.Length - i - 1;
            var childTrans = childList[i].GetComponent<RectTransform>();
            childTrans.sizeDelta = CellSize;
            Vector2 pos = Vector2.zero + ((CellSize + Spacing) * (currIndex));
            pos.y += line * LineSpacing;
            childTrans.localPosition = pos;
        }

        //while (true)
        //{
        //    int lastIndex = line * Count;
        //    for (int i = lastIndex + Count -1; i >= lastIndex; --i)
        //    {
        //        if (i >= childList.Length)
        //            return;

        //        childList[i].sizeDelta = CellSize;
        //        Vector2 pos = Vector2.zero + ((CellSize + Spacing) * (i - lastIndex));
        //        pos.y += line * LineSpacing;
        //        childList[i].localPosition = pos;
        //        //childList[i].SetSiblingIndex(0);

        //        if (i <= 0)
        //            return;
        //    }

        //    saveIndex = ++line * Count;
        //}
    }
}
