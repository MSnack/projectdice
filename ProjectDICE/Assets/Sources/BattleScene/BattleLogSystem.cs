﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class BattleLogSystem : MonoBehaviour
{
    public enum BattleLogType
    {
        SocketEvent,
        SkillEvent,
        DamageLog,
    }

    [SerializeField]
    private BattlePlayTimmer _battlePlayTimmer = null;

    [SerializeField, ReadOnly]
    private int _logIndex = -1;

    [SerializeField, ReadOnly]
    private int _damageLogIndex = -1;

    public void SendLog(BattleLogType logType, string eventName, params LogData[] logDatas)
    {
        var logList = new List<LogData>();
        logList.Add(new LogData("eventType", LogTypeString(logType)));
        logList.Add(new LogData("eventName", eventName));

        var socketMgr = EightUtil.GetCore<GameSocketMgr>();

        logList.Add(new LogData("roomId", socketMgr.RoomId));
        logList.Add(new LogData("battleTime", _battlePlayTimmer.BattleTime));
        logList.Add(new LogData("logIndex", ++_logIndex));

        logList.AddRange(logDatas);

        SendLogUtil.SendLog(SendLogUtil.TYPE.BATTLE, logList.ToArray());

    }

    public void SendDamageLog(GameUnit unit, int damage)
    {
        if (unit == null)
            return;

        var logList = new List<LogData>();
        logList.Add(new LogData("eventType", LogTypeString(BattleLogType.DamageLog)));

        var isLocal = unit.BattleManager?.ReceiverType == SocketDataType.Local ? true : false;
        logList.Add(new LogData("characterId", unit.UnitID));
        logList.Add(new LogData("characterLevel", unit.UnitLevel));
        logList.Add(new LogData("reinforce", unit.UnitReinforce));
        logList.Add(new LogData("step", unit.UnitStep));
        logList.Add(new LogData("damage", damage));
        logList.Add(new LogData("isLocal", isLocal));

        var socketMgr = EightUtil.GetCore<GameSocketMgr>();
        logList.Add(new LogData("roomId", socketMgr.RoomId));
        logList.Add(new LogData("battleTime", _battlePlayTimmer.BattleTime));
        logList.Add(new LogData("damageLogIndex", _damageLogIndex));

        SendLogUtil.SendLog(SendLogUtil.TYPE.BATTLE, logList.ToArray());
    }

    private string LogTypeString(BattleLogType type)
    {
        return System.Enum.GetName(typeof(BattleLogType), type);
    }

    //public void SendLog(params LogData[] logDatas)
    //{
    //    var logList = new List<LogData>(logDatas);

    //    var socketMgr = EightUtil.GetCore<GameSocketMgr>();

    //    logList.Add(new LogData("clientId", socketMgr.ClientID));
    //    logList.Add(new LogData("roomId", socketMgr.RoomId));
    //    logList.Add(new LogData("battleTime", _battlePlayTimmer.BattleTime));
    //    logList.Add(new LogData("logIndex", ++_logIndex));

    //    SendLogUtil.SendLog(SendLogUtil.TYPE.BATTLE, logList.ToArray());

    //}
}
