﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class BossIconRouletteController : MonoBehaviour
{
    [SerializeField]
    private Image _selectIcon = null;

    [SerializeField]
    private GameObject _content = null;

    public event UnityAction OnEndAniEvent = null;

    public void OnRoulette(MonsterUnitData[] bossList, int selectIconIndex)
    {
        gameObject.SetActive(true);
        var iconList = _content.GetComponentsInChildren<Image>(true);
        foreach (var icon in iconList)
        {
            int randomIndex = Random.Range(0, bossList.Length);
            Sprite iconSprite = null;
            if (bossList.Length > 0)
                iconSprite = bossList[randomIndex].PreviewImage;

            icon.sprite = iconSprite;
        }

        _selectIcon.sprite = bossList[selectIconIndex].PreviewImage;
    }

    private void OnEndAni()
    {
        OnEndAniEvent?.Invoke();
    }
}
