﻿using System.Collections;
using System.Collections.Generic;
using EightWork.MsgSystem;
using UnityEngine;
using UnityEngine.Events;
using EightWork;

public class BattleSceneInterface : EightMsgSystem
{
    [SerializeField]
    private SceneChanger _sceneChanger = null;

    [SerializeField]
    private BattleSocket _socketManager = null;
    public BattleSocket SocketManager => _socketManager;

    [SerializeField]
    private BattleLogSystem _logSystem = null;
    public BattleLogSystem LogSystem => _logSystem;

    public event UnityAction<bool> PlayEvent = null;
    public event UnityAction<RequestBattleInitUserData> OnInit = null;

    public bool IsLeaveNetworkUser
    {
        get
        {
            foreach (var manager in _battleManagerList)
            {
                if (manager.ReceiverType == SocketDataType.Network)
                {
                    return manager.UserManager.LeaveUser;
                }
            }

            return true;
        }
    }

    [SerializeField]
    private List<BattleManager> _battleManagerList = null;

    [SerializeField, ReadOnly]
    private bool _isInit = false;
    public bool IsInit => _isInit;

    public bool IsReadyCheck
    {
        get
        {
            foreach (var battleManager in _battleManagerList)
            {
                if (battleManager.UserManager.IsReady == false)
                    return false;
            }
            return true;
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        _socketManager.BattleInitUserDataEvent += BattleInitEvent;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        _socketManager.BattleInitUserDataEvent -= BattleInitEvent;
    }

    private void BattleInitEvent(RequestBattleInitUserData data)
    {
        OnInit?.Invoke(data);
        _isInit = true;
    }

    public void PlayGame()
    {
        PlayEvent?.Invoke(true);
    }

    public void PauseGame()
    {
        PlayEvent?.Invoke(false);
    }

    public void GiveUpBattle()
    {
        GamePopupMgr.OpenNormalPopup("항복", "항복하시겠습니까?", new[]
        {
            new NormalPopup.ButtonConfig("예", () => {
                _socketManager.EmitGiveUp();
                NormalPopup.Close();
            }),
            new NormalPopup.ButtonConfig("아니요", NormalPopup.Close),
        });
    }

    public void OnChangeLobby()
    {
        _sceneChanger?.ChangeScene();
    }

    public void UpdateAllUnitSeed()
    {
        foreach (var battleManager in _battleManagerList)
        {
            battleManager.TileSlotMgr.ResetAllUnitSeed();
        }
    }

    public void CreateCharacter(int unitId, int grade, BattleManager battleManager)
    {
        if (battleManager == null || battleManager.ReceiverType != SocketDataType.Local)
            return;

        var slot = battleManager.TileSlotMgr.ReserveRandomSlot();
        if (slot == null)
            return;

        battleManager.UserManager.OnCreateCharacterEvent();
        _socketManager.CreateUnit(unitId, grade, slot.slotIndex);
    }

    public void SpawnCharacter(int unitId, int grade, int slotIndex, BattleManager battleManager)
    {
        if (battleManager == null || battleManager.ReceiverType != SocketDataType.Local)
            return;

        _socketManager.SpawnCharacterUnit(unitId, grade, slotIndex);
    }

    public void MergeCreate(GameUnit target, GameUnit merge, int unitId, int grade, BattleManager battleManager)
    {
        if (battleManager == null || battleManager.ReceiverType != SocketDataType.Local ||
            target == null || merge == null)
            return;

        if (battleManager.TileSlotMgr.CheckBlock(target) == true ||
            battleManager.TileSlotMgr.CheckBlock(merge) == true)
            return;

        battleManager.TileSlotMgr.BlockControll(target, true);
        battleManager.TileSlotMgr.BlockControll(merge, true);

        _socketManager.OnMergeCreate(target, merge, unitId, grade);
    }

    public void MergeUpgrade(GameUnit target, GameUnit merge, int unitId, int grade, BattleManager battleManager)
    {
        if (battleManager == null || battleManager.ReceiverType != SocketDataType.Local ||
            target == null || merge == null)
            return;

        if (battleManager.TileSlotMgr.CheckBlock(target) == true ||
            battleManager.TileSlotMgr.CheckBlock(merge) == true)
            return;

        battleManager.TileSlotMgr.BlockControll(target, true);
        battleManager.TileSlotMgr.BlockControll(merge, true);

        _socketManager.OnMergeUpgrade(target, merge, unitId, grade);
    }

    public void CopyUnit(GameUnit target, GameUnit merge, int unitId, int grade, BattleManager battleManager)
    {
        if (battleManager == null || battleManager.ReceiverType != SocketDataType.Local ||
            target == null || merge == null)
            return;

        if (battleManager.TileSlotMgr.CheckBlock(target) == true)
            return;

        battleManager.TileSlotMgr.BlockControll(target, true);

        _socketManager.OnCopyUnit(target, merge, unitId, grade);
    }

    public void BattleUserLife(int life, int dir, BattleManager battleManager)
    {
        if (battleManager == null || battleManager.ReceiverType != SocketDataType.Local)
            return;

        _socketManager.EmitBattleLife(life, dir);
    }

    public void OnLeave()
    {
        _socketManager.EmitLeaveBattle();
    }
}
