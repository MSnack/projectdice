﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.MsgSystem;
using EightWork;
using UnityEngine.Events;
using RotaryHeart.Lib.SerializableDictionary;

public class BattleWaveData
{
    private int _waveIndex = -1;
    public int WaveIndex => _waveIndex;

    private float _waveTime = 0.0f;
    public float WaveTime => _waveTime;

    private float _spawnScale = 1.0f;
    public float SpawnScale => _spawnScale;

    private float _hpScale = 1.0f;
    public float HpScale => _hpScale;

    public BattleWaveData(int index, float time, float spawnScale, float hpScale)
    {
        _waveIndex = index;
        _waveTime = time;
        _spawnScale = spawnScale;
        _hpScale = hpScale;
    }
}

public class BattleWaveController : EightMsgReceiver<BattleSceneInterface>
{
    [SerializeField]
    private BattleSocket _socket = null;

    private Dictionary<int, BattleWaveData> _waveData = new Dictionary<int, BattleWaveData>();

    [SerializeField, ReadOnly]
    private int _currWave = 1;
    public int CurrWave => _currWave;

    [SerializeField, ReadOnly]
    private float _totalWaveTime = 0.0f;
    public float TotalWaveTime => _totalWaveTime;

    [SerializeField, ReadOnly]
    private float _currWaveTime = 0.0f;
    public float CurrWaveTime => _currWaveTime;

    [SerializeField]
    private float _delayTime = 3.0f;
    public float DealyTime => _delayTime;

    public bool IsDelayTime
    {
        get
        {
            var isDelayTime = _currWaveTime <= _delayTime;
            return isDelayTime;
        }
    }

    [SerializeField, ReadOnly]
    private bool _isPlay = true;
    public bool IsPlay { get => _isPlay; set => SetPlay(value); }

    private Coroutine _waveRoutine = null;

    [SerializeField, ReadOnly]
    private bool _isLoadData = false;
    public bool IsLoadData => _isLoadData;

    public event UnityAction UpdateWaveTime = null;
    public event UnityAction WaveEndEvent = null;

    public event UnityAction<float> WaveTimeEvent = null;
    public event UnityAction<float> LeftTimeEvent = null;

    protected override void OnEnable()
    {
        base.OnEnable();

        MsgInterface.PlayEvent += SetPlay;
        _socket.UpdateBattleWaveEvent += UpdateBattleWaveData;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        _socket.UpdateBattleWaveEvent -= UpdateBattleWaveData;
        MsgInterface.PlayEvent -= SetPlay;
    }

    private void UpdateBattleWaveData(RequestBattleWaveData data)
    {
        if (_isLoadData == true || data == null)
            return;

        _waveData.Clear();

        var dataList = data.WaveDataList;
        foreach (var waveData in dataList)
        {
            _waveData[waveData.WaveIndex] = waveData;
        }

        _isLoadData = true;
    }

    private void SetPlay(bool isPlay)
    {
        _isPlay = isPlay;
    }

    public BattleWaveData GetCurrWaveData()
    {
        if (_waveData.ContainsKey(_currWave) == false)
            return null;

        return _waveData[_currWave];
    }

    private void UpdateNextWave()
    {
        if (_waveData.ContainsKey(_currWave + 1) == false)
            return;

        _currWave += 1;
    }

    public void StartBattleWave()
    {
        if (_waveRoutine != null)
            return;

        InitBattleWave();
        _waveRoutine = StartCoroutine(BattleWaveRoutine());
    }

    private void InitBattleWave()
    {
        _totalWaveTime = 0.0f;
        foreach (var waveData in _waveData.Values)
        {
            _totalWaveTime += waveData.WaveTime;
        }
        _totalWaveTime += _delayTime;

        _currWaveTime = _totalWaveTime;
        _currWave = 1;
    }

    private IEnumerator BattleWaveRoutine()
    {
        float keepTime = 0.0f;
        while (_currWaveTime > 0.0f)
        {
            if (_isPlay == false)
                yield return new WaitUntil(() => _isPlay == true);

            var currWaveData = GetCurrWaveData();
            if (currWaveData == null)
                break;

            _currWaveTime -= Time.fixedDeltaTime;

            var timeDist = _totalWaveTime - _currWaveTime;
            var leftWaveTime = timeDist - keepTime;
            if (leftWaveTime >= currWaveData.WaveTime)
            {
                keepTime += currWaveData.WaveTime;
                UpdateNextWave();
            }

            UpdateWaveTime?.Invoke();
            LeftTimeEvent?.Invoke(_currWaveTime);
            WaveTimeEvent?.Invoke(timeDist);
            yield return new WaitForFixedUpdate();
        }

        //yield return new WaitForSeconds(_currWaveTime);

        WaveEndEvent?.Invoke();
        _waveRoutine = null;
    }

    public void OnLoadData()
    {
        if (_isLoadData == true)
            return;

        _socket.EmitUpdateBattleWaveData();
    }
}
