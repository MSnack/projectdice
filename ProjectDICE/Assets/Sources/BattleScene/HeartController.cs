﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class HpController : MonoBehaviour
{
    public abstract void UpdateHp(int hp, int dir);
}

public class HeartController : HpController
{
    [SerializeField]
    private GameObject _heart = null;

    [SerializeField]
    private GridLayoutGroup _grid = null;

    [SerializeField]
    private int _unit = 0; 

    [SerializeField]
    private int _poolCount = 0;

    private List<Image> _heartList = new List<Image>();

    public override void UpdateHp(int hp, int idr)
    {
        UpdatePool(hp);

        int lifeCheck = 0;
        foreach (var Image in _heartList)
        {
            if (lifeCheck >= hp)
            {
                Image.fillAmount = 0.0f;
                continue;
            }

            float fillAmount = 0.0f;

            for (int i = 0; i < _unit; ++i)
            {
                if (lifeCheck >= hp)
                {

                }
                else
                {
                    fillAmount += 1.0f / _unit;
                    Image.fillAmount = fillAmount;

                }
                ++lifeCheck;
            }
        }
    }

    private void CreateSpace()
    {
        for (int i = 0; i < _poolCount; ++i)
        {
            _heartList.Add(Instantiate(_heart, _grid.transform).GetComponent<Image>());
            _heartList[i].fillAmount = 0.0f;
        }
    }

    private void DestroySpace()
    {
        if (_heartList.Count <= 0)
            return;

        int index = _heartList.Count - 1;
        for (int i = 0; i < _poolCount; ++i, --index)
        {
            Image img = _heartList[index];
            _heartList.Remove(img);
            Destroy(img);
        }
    }

    private void UpdatePool(int hp)
    {
        if (hp / _unit > _heartList.Count)
            CreateSpace();
        else if (hp / _unit < _heartList.Count - _poolCount)
            DestroySpace();
    }
}
