﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.MsgSystem;
using EightWork;
using UnityEngine.Events;
using RotaryHeart.Lib.SerializableDictionary;

public class BattleBossControlCenter : EightMsgReceiver<BattleSceneInterface>
{
    [SerializeField]
    private BattleManager _battleManager = null;

    [SerializeField, ReadOnly]
    private GameUnit _bossUnit = null;
    public GameUnit BossUnit => _bossUnit;

    public event UnityAction OnBossDie = null;

    [SerializeField, ReadOnly]
    private bool _isSyncClear = false;
    public bool IsSyncClear => _isSyncClear;

    protected override void OnEnable()
    {
        base.OnEnable();

        _battleManager.Spawner.OnBossCreateEvent += InitBossUnit;
        _battleManager.SyncBossEvent += SyncBossEvent;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        _battleManager.SyncBossEvent -= SyncBossEvent;
        _battleManager.Spawner.OnBossCreateEvent -= InitBossUnit;
    }

    private void SyncBossEvent(bool isClear)
    {
        _isSyncClear = isClear;
    }

    private void InitBossUnit(GameUnit unit)
    {
        unit.OnDieEvent += OnClearBoss;
        _bossUnit = unit;
    }

    private void OnClearBoss(GameUnit unit)
    {
        unit.OnDieEvent -= OnClearBoss;
        _bossUnit = null;
        OnBossDie?.Invoke();
    }

    public void ResetSync()
    {
        _isSyncClear = false;
    }
}
