﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RewardPopup : MonoBehaviour
{

    [SerializeField]
    private RewardPopupSlotObject _slotPrefab = null;

    [SerializeField]
    private GameObject _slotContent = null;

    [SerializeField]
    private List<ContentSizeFitter> _contentSizeFitters = null;

    private readonly List<RewardPopupSlotObject> _slotList = new List<RewardPopupSlotObject>();
    private UIAlphaController _alphaController = null;
    private static UnityAction _closeAction;

    // Start is called before the first frame update
    void Start()
    {
        _alphaController = GetComponent<UIAlphaController>();
        _closeAction += CloseEvent;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CloseEvent();
        }
    }

    public void SetData(List<RewardItemInfo> infos)
    {
        ReleaseSlots();
        foreach (var info in infos)
        {
            var slot = Instantiate(_slotPrefab, _slotContent.transform);
            slot.InitData(info);
            _slotList.Add(slot);
        }

        gameObject?.SetActive(true);
        foreach (var csf in _contentSizeFitters)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform) csf.transform);
        }
    }

    private void ReleaseSlots()
    {
        foreach (var slotObject in _slotList)
        {
            Destroy(slotObject.gameObject);
        }

        _slotList.Clear();
    }

    private void OnEnable()
    {
        RefreshPopupData();
    }

    private void RefreshPopupData()
    {

        if (_alphaController != null)
        {
            _alphaController.ReloadObjects();
            _alphaController.Alpha = 0;
        }

    }

    public void CloseEvent()
    {
        try
        {
            foreach (var slot in _slotList)
            {
                slot.PlayEffect();
            }
        }
        catch (Exception e) {}
        
        gameObject.SetActive(false);
        if (_alphaController != null)
            _alphaController.Alpha = 1;
    }

    public static void Close()
    {
        _closeAction?.Invoke();
    }
}
