﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New CreateTrigger", menuName = "GameEvent/Trigger/Create", order = 3)]
public class CreateTrigger : GameTriggerBase
{
    [SerializeField]
    private GameActionSelectTargetDataList _actionDataList = null;

    public override void OnTriggerEventInUnit(GameUnit gameUnit)
    {
        base.OnTriggerEventInUnit(gameUnit);

        gameUnit.OnCreateEvent += OnCreateEvent;
    }

    private void OnCreateEvent(GameUnit unit)
    {
        _actionDataList.Invok(unit);
    }

    protected override void OnReleaseEvent(GameUnit unit)
    {
        base.OnReleaseEvent(unit);

        unit.OnCreateEvent -= OnCreateEvent;
    }
}
