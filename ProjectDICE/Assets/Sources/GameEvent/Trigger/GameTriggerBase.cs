﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public abstract class GameTriggerBase : GameEventBase
{
    public GameTriggerBase()
    {
        EventType = GameEventType.Trigger;
    }

    public virtual void OnTriggerEventInUnit(GameUnit gameUnit)
    {
        gameUnit.OnReleaseEvent += OnReleaseEvent;
    }

    protected virtual void OnReleaseEvent(GameUnit unit)
    {
        unit.OnReleaseEvent -= OnReleaseEvent;
    }
}