﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using RotaryHeart.Lib.SerializableDictionary;
using EightWork;

[CreateAssetMenu(fileName = "New MergeTrigger", menuName = "GameEvent/Trigger/Merge", order = 3)]
public class MergeTrigger : GameTriggerBase
{
    public enum MergeType
    {
        NoneSame,
        UnitSame,
        StepSame,
        AllSame,
    }

    [System.Serializable]
    private class MergeActionEvent : SerializableDictionaryBase<MergeType, GameActionDataList> { }

    [SerializeField]
    private MergeActionEvent _mergeActionList = null;

    public override void OnTriggerEventInUnit(GameUnit gameUnit)
    {
        base.OnTriggerEventInUnit(gameUnit);

        gameUnit.OnMergeEvent += OnMergeEvent;
    }

    private void OnMergeEvent(GameUnit caster, GameUnit target)
    {
        var mergeType = GetMergeType(caster, target);
        if (_mergeActionList.ContainsKey(mergeType) == false)
            return;

        var actionDataList = _mergeActionList[mergeType];
        if (actionDataList.Count <= 0)
            return;

        actionDataList.Invok(caster, target);
    }

    public static MergeType GetMergeType(GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return MergeType.NoneSame;

        var isSameStep = caster.UnitStep == target.UnitStep ? true : false;
        var isSameUnit = caster.UnitID == target.UnitID ? true : false;

        if (isSameStep == true && isSameUnit == true)
            return MergeType.AllSame;
        else if (isSameStep == true && isSameUnit == false)
            return MergeType.StepSame;
        else if (isSameStep == false && isSameUnit == true)
            return MergeType.UnitSame;

        return MergeType.NoneSame;
    }

    public bool IsHaveSkill(MergeType type)
    {
        if (_mergeActionList.ContainsKey(type) == false)
            return false;

        var skillList = _mergeActionList[type];
        if (skillList.Count <= 0)
            return false;

        return true;
    }

    protected override void OnReleaseEvent(GameUnit unit)
    {
        base.OnReleaseEvent(unit);

        unit.OnMergeEvent -= OnMergeEvent;
    }
}
