﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New AttackTrigger", menuName = "GameEvent/Trigger/Attack", order = 3)]
public class AttackTrigger : GameTriggerBase
{
    public override void OnTriggerEventInUnit(GameUnit gameUnit)
    {
        base.OnTriggerEventInUnit(gameUnit);
    }

    protected override void OnReleaseEvent(GameUnit unit)
    {
        base.OnReleaseEvent(unit);
    }
}
