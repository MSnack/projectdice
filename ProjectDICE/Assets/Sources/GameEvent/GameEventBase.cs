﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public abstract class GameEventBase : ScriptableObject
{
    public enum GameEventType
    {
        Trigger,
        Action,
    }

    private GameEventType _eventType;
    public GameEventType EventType { get => _eventType; set => _eventType = value; }
}
