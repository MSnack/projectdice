﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using SubjectNerd.Utilities;

[System.Serializable]
public class GameActionDataList
{
    [SerializeField, Reorderable]
    private List<SkillAsset> _actionDataList = null;
    public SkillAsset[] ActionDataList => _actionDataList.ToArray();

    public int Count => _actionDataList.Count;

    public void Invok(GameUnit caster, GameUnit target)
    {
        var skillMgr = EightUtil.GetCore<GameSkillActionMgr>();
        foreach (var actionData in _actionDataList)
        {
            skillMgr.OnProcessSkill(actionData, caster, target);
        }
    }
}

[System.Serializable]
public class GameActionSelectTargetData
{
    [SerializeField]
    private TargetType _targetType = TargetType.Null;
    public TargetType TargetType => _targetType;

    [SerializeField]
    private GameActionData _actionData = null;
    public GameActionData ActionData => _actionData;
}

[System.Serializable]
public class GameActionSelectTargetDataList
{
    [SerializeField, Reorderable]
    private List<GameActionSelectTargetData> _actionDataList = null;
    public int Count => _actionDataList.Count;

    public void Invok(GameUnit caster)
    {
        var skillMgr = EightUtil.GetCore<GameSkillActionMgr>();
        foreach (var actionData in _actionDataList)
        {
            var target = caster.GetTargetType(actionData.TargetType);
            skillMgr.OnProcessAction(actionData.ActionData, caster, target);
        }
    }
}

public class GameActionData : GameEventBase
{
    [SerializeReference]
    private List<SkillFrame> _skillFrames = null;
    public SkillFrame[] SkillFrames => _skillFrames.ToArray();

    public GameActionData()
    {
        EventType = GameEventType.Action;
    }
}