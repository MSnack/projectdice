﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using EightWork;

using UnityEngine.ResourceManagement.AsyncOperations;

using UnityEngine.UI;

public class AddSample : EightReceiver
{
    public AssetLabelReference assetLabel = null;

    public Image progress = null;
    public Text downloadText = null;

    private void Awake()
    {
        SetupComplete();

        progress.fillAmount = 0.0f;
        downloadText.text = string.Format("{0:p}", 0);
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();

        EightUtil.GetCore<EightCameraMgr>().EightCamera.orthographic = true;
    }

    public void GetDownloadSize()
    {
        if (EightUtil.IsLoadCompleteDevice == false)
            return;

        Addressables.GetDownloadSizeAsync(assetLabel.labelString).Completed += (AsyncOperationHandle<long> handle) =>
        {
            Debug.Log("size : " + handle.Result);
            Addressables.Release(handle);
        };
        //Addressables.DownloadDependenciesAsync()
        //Debug.Log(assetLabel.labelString);
        //Addressables.GetDownloadSizeAsync()
    }

    bool _isDownload = false;
    public void GetDownload()
    {
        if (EightUtil.IsLoadCompleteDevice == false)
            return;

        //Addressables.DownloadDependenciesAsync(assetLabel.labelString).Completed += (AsyncOperationHandle handle) =>{

        //    Debug.Log("Complate");
        //    Addressables.Release(handle);
        //};

        if (_isDownload == true)
            return;

        var handle = Addressables.DownloadDependenciesAsync(assetLabel.labelString);
        _isDownload = true;
        StartCoroutine(DownloadRoutine(handle));
    }

    public void ClearBundles()
    {
        Addressables.ClearDependencyCacheAsync(assetLabel.labelString);

        _useBundle = false;
    }

    private IEnumerator DownloadRoutine(AsyncOperationHandle handle)
    {
        while (handle.IsDone == false)
        {
            downloadText.text = string.Format("{0:p}", handle.PercentComplete);
            progress.fillAmount = handle.PercentComplete;
            yield return null;
        }

        downloadText.text = string.Format("{0:p}", 1.0f);
        progress.fillAmount = 1.0f;

        Debug.Log("DownloadComplate");

        Addressables.Release(handle);

        _isDownload = false;
        _useBundle = true;
    }

    private bool _useBundle = false;
    public void LoadKnight()
    {
        if (EightUtil.IsLoadCompleteDevice == false)
            return;

        if (_useBundle == false)
            return;

        Addressables.InstantiateAsync("Knight.prefab", this.transform).Completed += (AsyncOperationHandle<GameObject> handle) =>
        {
            var obj = handle.Result;
            if (obj == null)
                Debug.Log("fail");

            //Addressables.Release(handle);
        };
    }
}
