﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EightWork;

public class GameSpineObject : MonoBehaviour
{
    [SerializeField]
    private int _skinId = -1;
    public int SkinID => _skinId;

    [SerializeField]
    private Animator _animator = null;
    protected Animator SpineAnimator => _animator;

    [SerializeField]
    private Color _skinColor = Color.white;
    public Color SkinColor { get => _skinColor; set => SetSkinColor(value); }

    [SerializeField]
    private int _sortingOrder = 0;
    public int SortingOrder
    {
        get => _sortingOrder;
        set
        {
            SortingOrderChange(value);
            _sortingOrder = value;
        }
    }

    public event UnityAction OnEndAniEvent = null;
    public event UnityAction OnAttackEvent = null;
    public event UnityAction OnSkillEvent = null;
    public event UnityAction OnDieEvent = null;
    public event UnityAction OnDieTriggerEvent = null;

    [SerializeField]
    private SpriteRenderer _shadowObject = null;

    [SerializeField]
    private bool _drawShadow = true;
    public bool DrawShadow
    {
        get => _drawShadow;
        set => SetDrawShadow(value);
    }

    [SerializeField]
    private Vector3 _head = Vector3.zero;
    public Vector3 HeadPivot => _head;

    [SerializeField]
    private Vector3 _center = Vector3.zero;
    public Vector3 CenterPivot => _center;

    [SerializeField]
    private Vector3 _bottom = Vector3.zero;
    public Vector3 BottomPivot => _bottom;

    [SerializeField]
    private bool _isFlip = false;
    public bool IsFlip
    {
        get => _isFlip;
        set
        {
            Vector3 angle = transform.rotation.eulerAngles;
            if (value == true)
                angle.y = 180.0f;
            else
                angle.y = 0.0f;

            _isFlip = value;
            transform.eulerAngles = angle;
        }
    }

    private void SetSkinColor(Color color)
    {
        var childList = GetComponentsInChildren<SpriteRenderer>(true);
        foreach (var child in childList)
        {
            child.color = color;
        }
        _skinColor = color;
    }

    public void UpdateSkinData(GameSkinData data)
    {
        if (data == null)
            return;

        _skinId = data.DataId;
    }

    public void OnInitialize()
    {
        ChangeState(0);

        OnEndAniEvent = null;
        OnAttackEvent = null;
        OnSkillEvent = null;
        OnDieEvent = null;
        OnDieTriggerEvent = null;
    }

    private void SetDrawShadow(bool isDraw)
    {
        if(_shadowObject != null)
            _shadowObject.gameObject.SetActive(isDraw);
        _drawShadow = isDraw;
    }

    private void SortingOrderChange(int layer)
    {
        int index = 0;
        var childList = GetComponentsInChildren<SpriteRenderer>(true);
        foreach (var child in childList)
        {
            child.sortingOrder = layer + ++index;
        }
    }

    public void ChangeState(int state)
    {
        _animator.SetInteger("State", state);
    }

    public void Rebind()
    {
        _animator.Rebind();
    }

    public void ChangeState<TEnum>(TEnum state)
        where TEnum : System.Enum
    {
        _animator.SetInteger("State", System.Convert.ToInt32(state));
    }

    public void ChangeWeapon(int weapon)
    {
        SpineAnimator.SetInteger("Weapon", weapon);
    }

    public void ChangeWeapon<TEnum>(TEnum state)
        where TEnum : System.Enum
    {
        SpineAnimator.SetInteger("Weapon", System.Convert.ToInt32(state));
    }

    private void OnEndAni()
    {
        OnEndAniEvent?.Invoke();
    }

    private void OnAttack()
    {
        OnAttackEvent?.Invoke();
    }

    private void OnSkill()
    {
        OnSkillEvent?.Invoke();
    }

    private void OnDieTrigger()
    {
        OnDieTriggerEvent?.Invoke();
    }

    private void OnDie()
    {
        OnDieEvent?.Invoke();
    }

    private void Reset()
    {
        _animator = GetComponentInChildren<Animator>(true);
        SortingOrder = 0;
        IsFlip = false;
    }

    private void OnDrawGizmos()
    {
        SortingOrderChange(_sortingOrder);
        IsFlip = _isFlip;
        DrawShadow = _drawShadow;
        SkinColor = _skinColor;
    }

    [ContextMenu("SettingPivot")]
    private void FindPivot()
    {
        int childCount = transform.childCount;
        for (int i = 0; i < childCount; ++i)
        {
            var child = transform.GetChild(i);
            switch (child.gameObject.name)
            {
                case "HeadPivot": _head = child.transform.position; break;
                case "CenterPivot": _center = child.transform.position; break;
                case "BottomPivot": _bottom = child.transform.position; break;
            }
        }
    }

    [ContextMenu("FindShadow")]
    private void FindShadow()
    {
        var spriteList = GetComponentsInChildren<SpriteRenderer>(true);
        foreach (var spriteRenderer in spriteList)
        {
            if (spriteRenderer.gameObject.name == "Shadow")
            {
                _shadowObject = spriteRenderer;
                break;
            }
        }
    }

    public void WrapFrame(float speed)
    {
        if (speed > 0.3f)
            return;

        _animator.speed /= (speed / 0.3f);

    }

    public void UnWrapFrame() { _animator.speed = 1.0f; }
}
