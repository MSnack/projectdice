﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public abstract class PrivateFSM<State> : MonoBehaviour where State : System.Enum
{
    public enum StartType
    {
        Awake,
        None,
    }

    protected abstract class PStateBase
    {
        public abstract void OnInit(PrivateFSM<State> manager);

        public virtual void StartState() { }
        public virtual void Update() { }
        public virtual void EndState() { }

        public virtual void Release() { }
    }

    private Dictionary<State, PStateBase> _statePool = new Dictionary<State, PStateBase>();

    [SerializeField]
    private StartType _startType = StartType.Awake;
    public StartType Start => _startType;

    [SerializeField]
    private State _startState;
    public State StartState { get => _startState; set => _startState = value; }

    [SerializeField, ReadOnly]
    private State _currState;
    public State CurrState => _currState;

    // [SerializeField, ReadOnly]
    private bool _isStart = false;
    public bool IsStart => _isStart;

    // [SerializeField, ReadOnly]
    private bool _isFirstState = true;
    public bool IsFirstState => _isFirstState;

    protected virtual void Awake()
    {
        if (Start == StartType.Awake)
            StartFSM();
    }

    public void StartFSM()
    {
        if (IsStart == true)
            return;

        StateSetting();
        ChangeState(_startState);
        _isStart = true;
    }

    protected abstract void StateSetting();

    protected void AddState(State key, PStateBase state)
    {
        state.OnInit(this);
        _statePool[key] = state;
    }

    public void ChangeState(State state)
    {
        if (IsStart == true)
            return;

        if (IsFirstState == true)
            _isFirstState = false;
        else
            _statePool[CurrState].EndState();

        _currState = state;
        _statePool[state].StartState();
    }

    protected virtual void Update()
    {
        if (IsStart == false || IsFirstState == true)
            return;

        _statePool[CurrState].Update();
    }

    public virtual void Release()
    {
        foreach (var state in _statePool.Values)
        {
            state.Release();
        }
        _statePool.Clear();

        _isStart = false;
        _isFirstState = true;
    }
}
