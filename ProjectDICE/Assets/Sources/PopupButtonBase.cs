﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class PopupButtonBase : MonoBehaviour
{

    [SerializeField]
    private Text _buttonText = null;

    private Button _button = null;
    private Image _image = null;

    private SoundEffectPlayer _soundEffectPlayer = null;
    
    void Awake()
    {
        LoadComponents();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetData(NormalPopup.ButtonConfig config)
    {
        LoadComponents();

        _image.color = config.BaseColor;
        _buttonText.color = config.TextColor;

        _buttonText.text = config.ButtonName;
        _button.onClick = config.OnClicked;
        _button.onClick.AddListener(() => _soundEffectPlayer.Play());
    }

    private void LoadComponents()
    {
        if (_button == null) _button = GetComponent<Button>();
        if (_image == null) _image = GetComponent<Image>();
        if (_soundEffectPlayer == null) _soundEffectPlayer = GetComponent<SoundEffectPlayer>();
    }
}
