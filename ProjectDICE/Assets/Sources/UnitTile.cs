﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum UnitTileType
{
    Null,
    OnDraw,
    UseTile,
}

[RequireComponent(typeof(Image))]
public class UnitTile : MonoBehaviour
{
    [SerializeField]
    private Image _image = null;

    [SerializeField]
    private UnitTileType _type = UnitTileType.Null;
    public UnitTileType TileType => _type;

    private void OnDrawGizmos()
    {
        UpdateType();
    }

    private void UpdateType()
    {
        switch (_type)
        {
            case UnitTileType.Null: _image.enabled = false; break;
            case UnitTileType.OnDraw: _image.enabled = true; break;
            case UnitTileType.UseTile: _image.enabled = true; break;
        }
    }

    private void Reset()
    {
        _type = UnitTileType.Null;
        _image = GetComponent<Image>();

        UpdateType();
    }
}
