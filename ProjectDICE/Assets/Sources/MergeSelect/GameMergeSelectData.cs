﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(fileName = "New GameMergeSelectData", menuName = "Data/MergeSelectData", order = 3)]
public class GameMergeSelectData : ScriptableObject
{
    public enum MergeSelectType
    {
        Select,
        IdSameMerge,
        StepSameMerge,
        AllSameMerge,
    }

    [System.Serializable]
    private class MergeSelectDataList : SerializableDictionaryBase<MergeSelectType, Sprite> { }

    [SerializeField]
    private MergeSelectDataList _dataList = null;
    public Dictionary<MergeSelectType, Sprite> DataList => _dataList.Clone();

    public Sprite GetMergeSelectSprite(MergeSelectType type)
    {
        if (_dataList.ContainsKey(type) == false)
            return null;

        return _dataList[type];
    }
}
