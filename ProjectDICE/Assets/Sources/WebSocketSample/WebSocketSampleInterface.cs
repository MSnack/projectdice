﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EightWork;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
//using socket.io;
using BestHTTP.SocketIO;

namespace WebSsocketSample
{
    public class RequestWSSPingData
    {
        public long postMS = 0;
        public long serverMS = 0;
    }

    public class RequestWSSTimeSyncData
    {
        public long timeDistance = 0;
    }

    public class WebSocketSampleInterface : MonoBehaviour
    {
        [SerializeField]
        private string _serverPath = null;

        private SocketManager _socketManager = null;
        public Socket GameSocket => _socketManager == null ? null : _socketManager.Socket;

        public event UnityAction Connect = null;
        public event UnityAction Disconnect = null;
        public event UnityAction<RequestWSSPingData> Ping = null;
        public event UnityAction<RequestWSSTimeSyncData> TimeSync = null;

        public void OnConnectServer()
        {
            _socketManager = new SocketManager(new System.Uri(_serverPath));

            GameSocket.On(SocketIOEventTypes.Connect, OnConnectEvent);
            GameSocket.On(SocketIOEventTypes.Disconnect, OnDisConnectEvent);
            GameSocket.On("samplePing", OnPingEvent);
            GameSocket.On("sampleTimeSync", OnTimeSyncEvent);
        }

        private void OnPingEvent(Socket socket, Packet packet, object[] args)
        {
            var json = args[0] as Dictionary<string, object>;

            var pingData = new RequestWSSPingData();
            //pingData.postMS = long.Parse(json["postMS"].ToString());
            pingData.serverMS = long.Parse(json["serverMS"].ToString());

            Ping?.Invoke(pingData);
        }

        private void OnTimeSyncEvent(Socket socket, Packet packet, object[] args)
        {
            var json = args[0] as Dictionary<string, object>;

            var timeSyncData = new RequestWSSTimeSyncData();
            timeSyncData.timeDistance = long.Parse(json["timeDistance"].ToString());

            TimeSync?.Invoke(timeSyncData);
        }

        private void OnConnectEvent(Socket socket, Packet packet, object[] args)
        {
            Debug.Log("OnConnectEvent!");

            Connect?.Invoke();
        }

        private void OnDisConnectEvent(Socket socket, Packet packet, object[] args)
        {
            Debug.Log("OnDisconnectEvent!");

            Disconnect?.Invoke();
        }

        public void EmitSamplePing(long postMS)
        {
            JObject json = new JObject();
            json["postMS"] = postMS;

            GameSocket.Emit("samplePing", json.ToString());
        }

        public void EmitSampleTimeSync(long clientTime)
        {
            var json = new JObject();
            json["clientTime"] = clientTime;

            GameSocket.Emit("sampleTimeSync", json.ToString());
        }
    }
}