﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.UI;
using System;

namespace WebSsocketSample
{
    public class WSSCLPing : WSSCLStateBase
    {
        [SerializeField]
        private GameObject _pingButton = null;

        [SerializeField]
        private GameObject _cencelButton = null;

        [SerializeField]
        private Text _pingText = null;

        [SerializeField, ReadOnly]
        private bool _isPing = false;

        [SerializeField]
        private float _postTimeOut = 0.3f;

        [SerializeField, ReadOnly]
        private bool _isWait = false;

        [SerializeField, ReadOnly]
        private long _timeLatency = long.MaxValue;

        protected override void Initialize()
        {
            State = WSSCLState.Ping;
        }

        public override void StartState()
        {
            base.StartState();

            OnSetPing(false);
            _pingText.text = null;

            SystemMgr.Interface.Ping += ReceivePingEvent;

            StartCoroutine(PostPingCheckEventRoutine());
        }

        private IEnumerator PostPingCheckEventRoutine()
        {
            while (true)
            {
                if (_isPing == false)
                    yield return new WaitUntil(() => _isPing == true);

                yield return new WaitForSeconds(1.0f);

                var now = DateTimeOffset.UtcNow;
                //var result = now - DateTimeOffset.FromUnixTimeMilliseconds(0);

                //var ms = (long)result.TotalMilliseconds;

                //Debug.Log(ms);
                SystemMgr.Interface.EmitSamplePing(now.UtcTicks);
            }
        }

        private void ReceivePingEvent(RequestWSSPingData data)
        {
            _isWait = false;
            if (_isPing == false)
                return;

            var postTime = new DateTime(data.postMS, DateTimeKind.Utc);
            var serverTime = DateTimeOffset.FromUnixTimeMilliseconds(data.serverMS);

            var postResult = serverTime - postTime;
            if (_timeLatency > (long)postResult.TotalMilliseconds)
                _timeLatency = (long)postResult.TotalMilliseconds;

            //var nowTime = DateTime.UtcNow;
            //nowTime.AddMilliseconds(_timeLatency);
            var receiveResult = DateTimeOffset.UtcNow - serverTime;

            Debug.Log("receivePing : " + receiveResult.TotalMilliseconds.ToString());

            _pingText.text += "\n";
            _pingText.text += "post : " + ((long)postResult.TotalMilliseconds - _timeLatency).ToString() + "ms" + " " +
                "receive : " + ((long)receiveResult.TotalMilliseconds + _timeLatency).ToString() + "ms";
        }

        public override void EndState()
        {
            base.EndState();

            _pingText.text = null;
            SystemMgr.Interface.Ping -= ReceivePingEvent;
            OnSetPing(true);
        }

        public void OnSetPing(bool isPing)
        {
            if (isPing == _isPing)
                return;

            _pingButton.SetActive(!isPing);
            _cencelButton.SetActive(isPing);
            _isPing = isPing;
        }
    }
}