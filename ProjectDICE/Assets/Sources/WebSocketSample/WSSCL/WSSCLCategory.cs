﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.UI;
using System;

namespace WebSsocketSample
{
    public class WSSCLCategory : WSSCLStateBase
    { 
        protected override void Initialize()
        {
            State = WSSCLState.Category;
        }

        public override void StartState()
        {
            base.StartState();
        }

        public override void EndState()
        {
            base.EndState();
        }

        public void ChangePingCheck()
        {
            SystemMgr.ChangeState(WSSCLState.Ping);
        }

        public void ChangeTimeSync()
        {
            SystemMgr.ChangeState(WSSCLState.TimeSync);
        }
    }
}