﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.FSM;

namespace WebSsocketSample
{
    public enum WSSCLState
    {
        Category,
        Ping,
        TimeSync,
    }

    public abstract class WSSCLStateBase : EightFSMStateBase<WSSCLState, WSSCLobbyFSM>
    {
        [SerializeField]
        private GameObject _stateObject = null;

        public override void StartState()
        {
            _stateObject?.SetActive(true);
        }

        public override void EndState()
        {
            _stateObject?.SetActive(false);
        }

        [ContextMenu("StateInit")]
        private void StateInit()
        {
            Initialize();
        }
    }

    public class WSSCLobbyFSM : EightFSMSystem<WSSCLState, WSSCLStateBase>
    {
        [SerializeField]
        private WebSocketSampleInterface _interface = null;
        public WebSocketSampleInterface Interface => _interface;
    }
}