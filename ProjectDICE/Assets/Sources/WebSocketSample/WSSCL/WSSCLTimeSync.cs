﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.UI;
using System;

namespace WebSsocketSample
{
    public class WSSCLTimeSync : WSSCLStateBase
    {
        [SerializeField]
        private Text _logText = null;

        [SerializeField]
        private GameObject _syncButton = null;

        [SerializeField]
        private GameObject _cancelButton = null;

        private Coroutine _timeSyncRoutine = null;

        [SerializeField, ReadOnly]
        private bool _receiveTimeSync = false;

        [SerializeField]
        private long _saveInterval = 10;

        [SerializeField, ReadOnly]
        private long _stackTime = 0;

        [SerializeField, ReadOnly]
        private long _nowDitance = 0;

        protected override void Initialize()
        {
            State = WSSCLState.TimeSync;
        }

        public override void StartState()
        {
            base.StartState();
            SystemMgr.Interface.TimeSync += TimeSyncEvent;
        }

        public override void EndState()
        {
            base.EndState();
            SystemMgr.Interface.TimeSync -= TimeSyncEvent;
        }

        private void TimeSyncEvent(RequestWSSTimeSyncData data)
        {
            _nowDitance = data.timeDistance;
            _stackTime += data.timeDistance;

            _receiveTimeSync = true;
        }

        private IEnumerator TimeSyncRoutine()
        {
            _stackTime = 0;
            while (true)
            {
                var nowTime = (DateTimeOffset.UtcNow.Ticks - DateTimeOffset.FromUnixTimeMilliseconds(0).Ticks) / TimeSpan.TicksPerMillisecond;
                var resultTime = nowTime + _stackTime;
                SystemMgr.Interface.EmitSampleTimeSync(resultTime);
                _receiveTimeSync = false;

                if (_receiveTimeSync == false)
                    yield return new WaitUntil(() => _receiveTimeSync == true);

                string logText = "NowDistance : " + _nowDitance.ToString();
                PushText(logText);
                if (Mathf.Abs(_nowDitance) <= _saveInterval)
                    break;
            }

            StopTimeSync();
        }

        public void PushText(string text)
        {
            _logText.text += "\n" + text;
        }

        public void OnTimeSync(bool isUse)
        {
            if (isUse == true)
                StartTimeSync();
            else
                StopTimeSync();

            _syncButton?.SetActive(isUse);
            _cancelButton?.SetActive(!isUse);
        }

        public void StartTimeSync()
        {
            if (_timeSyncRoutine != null)
                StopTimeSync();

            _timeSyncRoutine = StartCoroutine(TimeSyncRoutine());
        }

        public void StopTimeSync()
        {
            if (_timeSyncRoutine == null)
                return;

            StopCoroutine(_timeSyncRoutine);
            _timeSyncRoutine = null;
        }
    }
}