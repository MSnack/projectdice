﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using EightWork.FSM;

namespace WebSsocketSample
{
    public enum WSSCState
    {
        Init,
        Lobby,
    }

    public abstract class WebSocketSampleCanvasStateBase : EightFSMStateBase<WSSCState, WebSocketSampleCanvasFSM> { }

    public class WebSocketSampleCanvasFSM : EightFSMSystem<WSSCState, WebSocketSampleCanvasStateBase>
    {
        [SerializeField]
        private WebSocketSampleInterface _interface = null;
        public WebSocketSampleInterface Interface => _interface;
    }
}