﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WebSsocketSample
{
    public class WSSCInit : WebSocketSampleCanvasStateBase
    {
        [SerializeField]
        private GameObject _initObject = null;

        protected override void Initialize()
        {
            State = WSSCState.Init;
        }

        public override void StartState()
        {
            _initObject?.SetActive(true);
            SystemMgr.Interface.Connect += OnConnect;
        }

        private void OnConnect()
        {
            SystemMgr.ChangeState(WSSCState.Lobby);
        }

        public override void EndState()
        {
            SystemMgr.Interface.Connect -= OnConnect;
            _initObject?.SetActive(false);
        }
    }
}