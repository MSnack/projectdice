﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WebSsocketSample
{
    public class WSSCLobby : WebSocketSampleCanvasStateBase
    {
        [SerializeField]
        private GameObject _lobbyObject = null;

        protected override void Initialize()
        {
            State = WSSCState.Lobby;
        }

        public override void StartState()
        {
            _lobbyObject?.SetActive(true);
        }

        public override void EndState()
        {
            _lobbyObject?.SetActive(false);
        }
    }
}