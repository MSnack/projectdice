﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using EightWork.UI;
using UnityEngine;

public class ThrowExceptionToPopup : MonoBehaviour
{

	private static string _recentExceptionLog = null;
	private static List<string> _exceptionLogList = new List<string>();

	public static string RecentExceptionLog => _recentExceptionLog;

	public static List<string> ExceptionLogList => _exceptionLogList;

	private void Awake()
	{
		AddExceptionHandler();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void AddExceptionHandler()
	{
		// 유니티 메인 스레드가 아닌 다른 스레드에서 발생한 익셉션은 여기로 핸들링됩니다.
		AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
   
#pragma warning disable 0618
		// 유니티 스레드에서 발생된 익셉션은 여기로 핸들링됩니다.
		UnityEngine.Application.RegisterLogCallback((condition, stackTrace, type) =>
		{
			if (type != LogType.Exception)
				return;
			
			SendLog(condition, stackTrace);
            SendLogUtil.SendLog(SendLogUtil.TYPE.ERROR, condition);
		});
#pragma warning restore 0618
	}
 
	private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
	{
		if (e.ExceptionObject is Exception == false)
			return;
		var ex = e.ExceptionObject as Exception;
 
		SendLog(ex.Message, ex.StackTrace);
	}
	private void SendLog(string msg, string stackTrace)
	{
		// 익셉션 로그를 저장하거나 / 서버로 전송한다
		string log = msg + "\n\n" + stackTrace;
		_recentExceptionLog = log;
		_exceptionLogList.Add(log);
		GamePopupMgr.OpenNormalPopup("에러 발생", msg + "\n" + stackTrace);

        var userMgr = EightUtil.GetCore<GameUserMgr>();
        SendLogUtil.SendLog(SendLogUtil.TYPE.ERROR,
            new LogData("UID", userMgr.UserInfo.UID),
            new LogData("nickName", userMgr.UserInfo.NickName),
            new LogData("errorLog", msg),
            new LogData("stackTrace", stackTrace));
	}
}
