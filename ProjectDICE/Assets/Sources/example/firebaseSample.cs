﻿using Firebase.Auth;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

public class firebaseSample : MonoBehaviour
{
    private static FirebaseAuth _firebaseAuth = null;
    private static string _firebaseToken = null;

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        _firebaseAuth = FirebaseAuth.DefaultInstance;
    }

    public void Login()
    {
        this.SignInAnonymous().ContinueWith(task =>
        {
            _firebaseAuth.CurrentUser.TokenAsync(true).ContinueWith(token =>
            {
                _firebaseToken = token.Result;
                Debug.Log("idToken : " + _firebaseToken);
            });
        });

        StartCoroutine(LoginRotuine());
    }

    IEnumerator LoginRotuine()
    {
        while (_firebaseToken == null) { yield return null; }

        UnityWebRequest www = UnityWebRequest.Post("http://localhost:3000/login/firebase/in/" + _firebaseToken, "");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            Debug.Log("Form upload complete!");
        }
    }


    public async Task SignInAnonymous()
    {
        await _firebaseAuth.SignInAnonymouslyAsync().ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInAnonymouslyAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });
    }
}
