﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameUnitReferenceFilter : MonoBehaviour
{
    private enum ReferenceFilterType
    {
        Check,
        Clear,
    }

    private Dictionary<ReferenceFilterType, List<GameUnit>> _referencePool = new Dictionary<ReferenceFilterType, List<GameUnit>>();

    public event UnityAction<GameUnit[]> OnFilteringUnitList = null;

    private void Awake()
    {
        foreach (ReferenceFilterType type in System.Enum.GetValues(typeof(ReferenceFilterType)))
        {
            if (_referencePool.ContainsKey(type) == false)
                _referencePool[type] = new List<GameUnit>();
        }
    }

    private void OnEnable()
    {
        StartCoroutine(ReferenceCheck());
    }

    public void RegisterReference(GameUnit unit)
    {
        if (unit == null)
            return;

        if (_referencePool.ContainsKey(ReferenceFilterType.Check) == false)
            _referencePool[ReferenceFilterType.Check] = new List<GameUnit>();

        unit.gameObject.SetActive(false);
        _referencePool[ReferenceFilterType.Check].Add(unit);
    }

    private void ClearUnit(GameUnit unit)
    {
        if (unit == null)
            return;

        if (_referencePool.ContainsKey(ReferenceFilterType.Clear) == false)
            _referencePool[ReferenceFilterType.Clear] = new List<GameUnit>();

        _referencePool[ReferenceFilterType.Check].Remove(unit);
        _referencePool[ReferenceFilterType.Clear].Add(unit);
    }

    private IEnumerator ReferenceCheck()
    {
        while (true)
        {
            var referenceList = _referencePool[ReferenceFilterType.Check];
            for (int i = referenceList.Count - 1; i >= 0; --i)
            {
                var unit = referenceList[i];
                if (unit.IsCleanUnit == true)
                {
                    ClearUnit(unit);
                }
            }

            if (_referencePool[ReferenceFilterType.Clear].Count > 0)
            {
                OnFilteringUnitList?.Invoke(_referencePool[ReferenceFilterType.Clear].ToArray());
                ClearReferenceList();
            }

            yield return null;
        }
    }

    private void ClearReferenceList()
    {
        if (_referencePool.ContainsKey(ReferenceFilterType.Clear) == false)
            _referencePool[ReferenceFilterType.Clear] = new List<GameUnit>();

        _referencePool[ReferenceFilterType.Clear].Clear();
    }
}
