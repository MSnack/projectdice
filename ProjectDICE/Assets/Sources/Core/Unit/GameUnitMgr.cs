﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class GameUnitMgr : GameUnitFactory
{
    public override void InitializedCore()
    {

    }

    protected override void RegisterMsg()
    {
        base.RegisterMsg();

        this.RegisterEvent(EIGHT_MSG.CHANGE_SCENE_START, OnChangeScene);
    }

    private void OnChangeScene(EightMsgContent eightMsgContent)
    {
        ReleaseAll();
    }

    protected override void UnRegisterMsg()
    {
        base.UnRegisterMsg();

        this.RemoveEvent(EIGHT_MSG.CHANGE_SCENE_START, OnChangeScene);
    }

    //public T FindUnit<T>(GameUnit unit) where T : GameUnit
    //{
    //    if (unit == null)
    //        return null;

    //    foreach (var find in UnitContainerPool)
    //    {
    //        if (find == unit)
    //        {
    //            if (find.GetType() == unit.GetType())
    //                return find as T;
    //            else
    //            {
    //                Debug.Log("Dont Same Type");
    //                return null;
    //            }
    //        }
    //    }

    //    return null;
    //}

    //public T[] FindUnitList<T>(GameUnitTeam type) where T : GameUnit
    //{
    //    if (UnitContainerPool.ContainsKey(type) == false)
    //        UnitContainerPool[type] = new List<GameUnit>();

    //    if (UnitContainerPool[type].Count <= 0)
    //        return null;

    //    T[] unitList = new T[UnitContainerPool[type].Count];
    //    for (int i = 0; i < UnitContainerPool[type].Count; ++i)
    //    {
    //        unitList[i] = (T)UnitContainerPool[type][i];
    //    }
    //    return unitList;
    //}

    public void ReturnAllUnit()
    {
        for (int i = UnitContainerPool.Count - 1; i >= 0; --i)
        {
            ReturnUnit(UnitContainerPool[i]);
        }
        UnitContainerPool.Clear();
        //foreach (var value in UnitContainerPool.Values)
        //{
        //    for (int i = value.Count - 1; i >= 0; --i)
        //    {
        //        ReturnUnit(value[i]);
        //    }
        //    value.Clear();
        //}
    }

    public void ReleaseAll()
    {
        ReturnAllUnit();
        ReservePool.ClearPool();
    }
}
