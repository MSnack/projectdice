﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.Events;

using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.AddressableAssets;

using RotaryHeart.Lib.SerializableDictionary;

[System.Serializable]
public class SerializableTriggerData : SerializableDictionaryBase<UnitTriggerType, UnitTriggerList> { }

public abstract class GameUnitData : ScriptableObject
{
    [SerializeField]
    private int _dataId = -1;
    public int DataID => _dataId;

    [SerializeField]
    private int _skinId = -1;
    public int SkinID => _skinId;

    [SerializeField]
    private SerializableTriggerData _skillTriggerData = null;
    public Dictionary<UnitTriggerType, UnitTriggerList> SkillTriggerData => _skillTriggerData.Clone();
}
