﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[System.Serializable]
public class CharacterUnitStatData
{
    [SerializeField]
    private float _value = -1;
    public float Value => _value;

    [SerializeField]
    private float _levelCoeff = -1.0f;
    public float LevelCoeff => _levelCoeff;

    [SerializeField]
    private float _classCoeff = -1.0f;
    public float ClassCoeff => _classCoeff;

    public int GetValueAtCondition(int level, int cls)
    {
        int classValue = (int)(ClassCoeff * cls);
        int levelValue = (int)(LevelCoeff * level);

        return (int)Value + levelValue + classValue;
    }
}

[Serializable]
public class CharacterGimmickInformation
{
    [SerializeField]
    private string _name = null;
    public string Name => _name;
    
    [SerializeField]
    private float _value = 0;
    public float Value => _value;
    
    [SerializeField]
    private Sprite _icon = null;
    public Sprite Icon => _icon;
    
    [SerializeField]
    private float _plusLevel = 0;
    public float PlusLevel => _plusLevel;
    
    [SerializeField]
    private float _plusReinforce = 0;
    public float PlusReinforce => _plusReinforce;

    [SerializeField]
    private string _format = "{0}";
    public string Format => _format;
}

[CreateAssetMenu(fileName = "New CharacterUnitData", menuName = "UnitData/CharacterUnitData", order = 3)]
public class CharacterUnitData : GameUnitData
{
    [SerializeField]
    private MergeTrigger _mergeTriggerData = null;
    public MergeTrigger MergeTriggerData => _mergeTriggerData;

    [SerializeField]
    private GameMergeSelectData _mergeSelectData = null;
    public GameMergeSelectData MergeSelectData => _mergeSelectData;

    [Header("UnitInfo")]
    [SerializeField]
    private Sprite _unitPreviewSprite = null;
    public Sprite UnitPreviewSprite => _unitPreviewSprite;
    
    [SerializeField]
    private string _unitName = null;
    public string UnitName => _unitName;
    
    [SerializeField]
    private string _skillDescription = null;
    public string SkillDescription => _skillDescription;

    [SerializeField, TextArea]
    private string _description = null;
    public string Description => _description;

    [SerializeField]
    private CharacterGimmickInformation _gimmick1 = null;
    public CharacterGimmickInformation Gimmick1 => _gimmick1;

    [SerializeField]
    private CharacterGimmickInformation _gimmick2 = null;
    public CharacterGimmickInformation Gimmick2 => _gimmick2;
    
    [SerializeField]
    private CharacterGimmickInformation _gimmick3 = null;
    public CharacterGimmickInformation Gimmick3 => _gimmick3;
    
    [SerializeField]
    private string _storyTitle = null;
    public string StoryTitle => _storyTitle;
    
    [SerializeField, TextArea]
    private string _storyDescription = null;
    public string StoryDescription => _storyDescription;

    [SerializeField]
    private int _grade = 1;
    public int Grade => _grade;

    [SerializeField]
    private int _proxyId = -1;
    public int ProxyId => _proxyId;

    [SerializeField]
    private CardUReinforceData _reinforceData = null;
    public CardUReinforceData ReinforceData => _reinforceData;

    [SerializeField]
    private StatData _combat = null;
    public StatData Combat => _combat;

    [SerializeField]
    private StatData _attackSpeed = null;
    public StatData AttackSpeed => _attackSpeed;

    [SerializeField]
    private StatData _criticalPercent = null;
    public StatData CriticalPercent => _criticalPercent;

    [SerializeField]
    private StatData _criticalCombat = null;
    public StatData CriticalCombat => _criticalCombat;
}
