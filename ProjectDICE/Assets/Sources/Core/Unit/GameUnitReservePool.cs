﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUnitReservePool : MonoBehaviour
{
    private Queue<GameUnit> _reservePool = new Queue<GameUnit>();

    [SerializeField]
    private int _reserveCount = 5;
    public int ReserveCount { get => _reserveCount; set => _reserveCount = value; }

    public bool ReserveUnit()
    {
        for (int i = 0; i < _reserveCount; ++i)
        {
            GameObject unitObj = new GameObject("GameUnit");
            var unit = unitObj.AddComponent<GameUnit>();
            unit.transform.SetParent(this.transform);
            unit.gameObject.SetActive(false);
            _reservePool.Enqueue(unit);
        }

        return true;
    }

    public GameUnit GetUnit()
    {
        if (_reservePool.Count <= 0)
        {
            if (ReserveUnit() == false)
                return null;
        }

        var unit = _reservePool.Dequeue();
        unit.gameObject.SetActive(true);

        return unit;
    }

    public void ReturnUnit(GameUnit unit)
    {
        if (unit == null)
            return;

        unit.transform.SetParent(this.transform);
        unit.transform.position = Vector3.zero;

        unit.gameObject.SetActive(false);

        _reservePool.Enqueue(unit);
    }

    public void ClearPool()
    {
        var list = _reservePool.ToArray();
        _reservePool.Clear();
        for (int i = list.Length - 1; i >= 0; --i)
        {
            Destroy(list[i].gameObject);
        }
    }
}
