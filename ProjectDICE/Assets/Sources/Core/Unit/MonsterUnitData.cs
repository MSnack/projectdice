﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ImmuneList
{
    [SerializeField]
    private bool _death = false;
    public bool Death => _death;
}

[CreateAssetMenu(fileName = "New MonsterUnitData", menuName = "UnitData/MonsterUnitData", order = 3)]
public class MonsterUnitData : GameUnitData
{
    [SerializeField]
    private Sprite _previewImage = null;
    public Sprite PreviewImage => _previewImage;

    [SerializeField]
    private bool _isBossMonster = false;
    public bool IsBossMonster => _isBossMonster;

    [SerializeField]
    private ImmuneList _immune = null;
    public ImmuneList Immune => _immune;
}
