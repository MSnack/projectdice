﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.Events;

public abstract class GameUnitFactory : EightWork.Core.EightCore
{
    [SerializeField]
    private GameUnitReservePool _reservePool = null;
    protected GameUnitReservePool ReservePool => _reservePool;

    [SerializeField]
    private GameUnitReferenceFilter _referenceFilter = null;
    protected GameUnitReferenceFilter ReferenceFilter => _referenceFilter;

    protected List<GameUnit> UnitContainerPool = new List<GameUnit>();

    private void Awake()
    {
        _referenceFilter.OnFilteringUnitList += ClearFilteringUnitList;
    }

    private void ClearFilteringUnitList(GameUnit[] unitList)
    {
        foreach (var unit in unitList)
        {
            unit.Release();
            _reservePool.ReturnUnit(unit);
        }
    }

    public void ReturnUnit(GameUnit unit)
    {
        if (unit == null)
        {
            UnitContainerPool.Remove(unit);
            return;
        }

        UnitContainerPool.Remove(unit);
        _referenceFilter.RegisterReference(unit);
        unit.Return();
    }

    protected void RegisterUnit(GameUnit unit)
    {
        UnitContainerPool.Add(unit);
    }

    public GameUnit CreateCharacater<Controller>(int unitId, int grade, int randomSeed, TileRoadMgr tileRoadMgr, TileSlotMgr tileSlotMgr, BattleUserManager userManager, BattleManager battleManager) where Controller : GameUnitController
    {
        var gameUnitData = EightUtil.GetCore<GameUnitDataMgr>().GetCharacaterData(unitId);
        if (gameUnitData == null)
            return null;

        GameUnit unit = _reservePool.GetUnit();

        var skin = EightUtil.GetCore<GameSkinMgr>().GetSkin(gameUnitData.SkinID);
        unit.SpineUnit = skin;

        unit.Controller = unit.gameObject.AddComponent<Controller>();

        unit.SetCharacterData(gameUnitData, grade);
        unit.TileRoadManager = tileRoadMgr;
        unit.TileSlotManager = tileSlotMgr;
        unit.UserManager = userManager;
        unit.BattleManager = battleManager;

        RegisterUnit(unit);

        unit.UnitRandomController.UpdateSeed(randomSeed);

        unit.OnInitialize();
        //unit.UnitState = GameUnitState.Spawn;
        unit.OnNormal();

        return unit;
    }

    public GameUnit CreateCharacater<Controller>(int unitId, int grade, int randomSeed, BattleManager battleManager) where Controller : GameUnitController
    {
        var unit = CreateCharacater<Controller>(unitId, grade, randomSeed, battleManager.TileRoadMgr, battleManager.TileSlotMgr, battleManager.UserManager, battleManager);

        return unit;
    }

    public GameUnit CreateMonster<Controller>(int unitId, BattleMonsterSpawner.MonsterInitData initData, BattleManager battleManager, TileRoadMgr tileRoadMgr, TileSlotMgr tileSlotMgr, BattleUserManager userManager ,BattleWaveController waveController) where Controller : GameUnitController
    {
        var gameUnitData = EightUtil.GetCore<GameUnitDataMgr>().GetMonsterData(unitId);
        if (gameUnitData == null)
            return null;

        GameUnit unit = _reservePool.GetUnit();

        var skin = EightUtil.GetCore<GameSkinMgr>().GetSkin(gameUnitData.SkinID);
        unit.SpineUnit = skin;

        unit.Controller = unit.gameObject.AddComponent<Controller>();

        unit.SetMonsterData(gameUnitData, initData);
        unit.BattleManager = battleManager;
        unit.TileRoadManager = tileRoadMgr;
        unit.TileSlotManager = tileSlotMgr;
        unit.UserManager = userManager;
        unit.WaveController = waveController;

        var hpBarMgr = EightUtil.GetCore<GameHpBarMgr>().GetHpBar(initData.HpType);
        hpBarMgr.OnInit(unit);
        //unit.HpBar = EightUtil.GetCore<GameUnitHpBarMgr>().GetHpBar();
        //unit.HpBar.OnInitHpBar(unit);

        RegisterUnit(unit);

        unit.UnitRandomController.UpdateSeed(0);

        unit.OnInitialize();
        unit.OnNormal();

        unit.transform.position = tileRoadMgr.SpawnTile.GetPositionForUnit();
        tileRoadMgr.RegisterUnit(unit);

        return unit;
    }

    public GameUnit CreateMonster<Controller>(int unitId, BattleMonsterSpawner.MonsterInitData initData, BattleManager battleManager) where Controller : GameUnitController
    {
        TileRoadMgr tileRoadMgr = null;
        TileSlotMgr tileSlotMgr = null;
        BattleUserManager userMgr = null;
        BattleWaveController waveController = null;

        if (battleManager != null)
        {
            tileRoadMgr = battleManager.TileRoadMgr;
            tileSlotMgr = battleManager.TileSlotMgr;
            userMgr = battleManager.UserManager;
            waveController = battleManager.WaveController;
        }

        var unit = CreateMonster<Controller>(unitId, initData, battleManager,tileRoadMgr, tileSlotMgr, userMgr, waveController );
        return unit;
    }
}
