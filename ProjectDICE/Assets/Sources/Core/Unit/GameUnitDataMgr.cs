﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using EightWork;

using UnityEngine.AddressableAssets;

public class GameUnitDataMgr : EightWork.Core.EightCore
{
    [System.Serializable]
    private class UnitDataPool<TData> where TData : GameUnitData
    {
        [SerializeField]
        private AssetLabelReference _lable = null;
        public AssetLabelReference Lable => _lable;

        private Dictionary<int, TData> _dataPool = new Dictionary<int, TData>();
        public int[] UserDataIDList => _dataPool.Keys.ToArray();

        [SerializeField, ReadOnly]
        private bool _isLoad = false;
        public bool IsLoad => _isLoad;

        public void LoadData()
        {
            if (_isLoad == true)
                return;

            var loadHandle = Addressables.LoadAssetsAsync<TData>(_lable.labelString, null);
            loadHandle.Completed += (result) =>
            {
                var gameUnitData = result.Result;
                if (gameUnitData == null)
                {
                    Debug.Log("Fail Load GameUnitData");
                    GameDataMgr.IsLoadFailed = true;
                    return;
                }

                foreach (var unitData in gameUnitData)
                {
                    int dataId = unitData.DataID;
                    _dataPool[dataId] = unitData;
                }

                _isLoad = true;
            };
        }

        public void ClearData()
        {
            if (_dataPool.Count <= 0)
                return;

            var unitDataList = _dataPool.Values;
            _dataPool.Clear();

            foreach (var unitData in _dataPool)
            {
                Addressables.Release(unitData);
            }

            _isLoad = false;
        }

        public TData GetData(int id)
        {
            if (_dataPool.ContainsKey(id) == false)
                return null;

            return _dataPool[id];
        }
    }

    [System.Serializable]
    private class CharacterUnitDataPool : UnitDataPool<CharacterUnitData> { }

    [System.Serializable]
    private class MonsterUnitDataPool : UnitDataPool<MonsterUnitData> { }

    [SerializeField]
    private CharacterUnitDataPool _characterData = null;

    public int[] CharactersIDList => _characterData.UserDataIDList;

    [SerializeField]
    private MonsterUnitDataPool _monsterData = null;

    public bool IsLoadCompleted => _characterData.IsLoad && _monsterData.IsLoad;
    private bool _isLoadFailed = false;
    public bool IsLoadFailed => _isLoadFailed;

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
    }

    public override void InitializedCore()
    {
        //LoadGameUnitData();
    }

    public void LoadGameUnitData()
    {
        _characterData.LoadData();
        _monsterData.LoadData();
    }

    public void ClearData()
    {
        _characterData.ClearData();
        _monsterData.ClearData();
    }

    public CharacterUnitData GetCharacaterData(int id)
    {
        return _characterData.GetData(id);
    }

    public MonsterUnitData GetMonsterData(int id)
    {
        return _monsterData.GetData(id);
    }
}
