﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.Events;

public class GameEffectObject : MonoBehaviour
{
    [SerializeField, ReadOnly]
    private int _autoId = -1;
    public int AutoID { get { return _autoId; } set { _autoId = value; } }

    [SerializeField, ReadOnly(ReadOnlyType.DISABLE_RUNTIME)]
    private int _sortingOrder = 0;

    private int _sortingOrderSrc = -1;
    public int SortingOrder
    {
        get => _sortingOrder;
        set => OrderInLayer(value);
    }

    [SerializeField]
    private float _lifeTime = 0.0f;
    public float LifeTime => _lifeTime;

    public event UnityAction OnReturnEvent = null;

    public void Clear()
    {
        var trailRenderer = GetComponentsInChildren<TrailRenderer>(true);
        foreach (var trail in trailRenderer)
        {
            trail.Clear();
        }

        var particleSystems = GetComponentsInChildren<ParticleSystem>(true);
        foreach (var particle in particleSystems)
        {
            particle.Clear();
        }
    }

    public void ReStart()
    {
        var particleSystems = GetComponentsInChildren<ParticleSystem>(true);
        foreach (var particle in particleSystems)
        {
            particle.Play();
        }
    }

    public void Release()
    {
        OnReturnEvent?.Invoke();
        OnReturnEvent = null;
    }

    public void ResetOrderInLayer()
    {
        OrderInLayer(_sortingOrderSrc);
    }

    private void OrderInLayer(int layer, bool isReverse = false)
    {
        if (_sortingOrderSrc < 0) _sortingOrderSrc = _sortingOrder;
        _sortingOrder = layer;
        List<Renderer> rendererList = new List<Renderer>();
        rendererList.AddRange(gameObject.GetComponentsInChildren<Renderer>(true));
        if (isReverse == true)
            rendererList.Reverse();

        for(int i = rendererList.Count -1; i >= 0; --i)
        {
            rendererList[i].sortingOrder = layer++;
        }
    }

    [ContextMenu("UpdateOrderInLayer")]
    public void UpdateOrderInLayer()
    {
        OrderInLayer(_sortingOrder);
    }

    [ContextMenu("UpdateOrderInLayerReverse")]
    public void UpdateOrderInLayerReverse()
    {
        OrderInLayer(_sortingOrder, true);
    }}
