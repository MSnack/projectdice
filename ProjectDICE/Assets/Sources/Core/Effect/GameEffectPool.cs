﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

using UnityEngine.Events;
using UnityEngine.AddressableAssets;

public class GameEffectPool : MonoBehaviour
{
    private class UseEffectInfo
    {
        public GameEffectObject Effect;
        public Coroutine EffectRoutine;

        //public UseEffectInfo(GameEffectObject effect, Coroutine routine)
        //{
        //    _effect = effect;
        //    _effectRoutine = routine;
        //}
    }

    [SerializeField]
    private AssetLabelReference _effectLabel = null;

    [SerializeField]
    private Transform _reserveParent = null;

    [SerializeField]
    private Transform _useParent = null;

    //[SerializeField, ReadOnly]
    //private int _autoIndex = 0;
    //public int AutoIndex => _autoIndex;

    [SerializeField]
    private int _reserveCount = 5;
    public int ReserveCount { get => _reserveCount; set => _reserveCount = value; }

    private Dictionary<int, GameEffectData> _effectAssetData = new Dictionary<int, GameEffectData>();

    private Dictionary<int, Queue<GameEffectObject>> _effectReservePool = new Dictionary<int, Queue<GameEffectObject>>();
    private Dictionary<int, List<UseEffectInfo>> _effectUsePool = new Dictionary<int, List<UseEffectInfo>>();

    public void UploadEffectAsset()
    {
        Addressables.LoadAssetsAsync<GameEffectData>(_effectLabel.labelString, (result) =>
        {
            if (result == null)
                return;

            _effectAssetData[result.DataID] = result;
        });
    }

    //public int UploadEffectAsset(GameEffectData data)
    //{
    //    if (data.AutoID != -1)
    //        return data.AutoID;

    //    data.UpdateAutoID(_autoIndex);
    //    _effectAssetData[_autoIndex++] = data;
    //    //int reserveId = _autoIndex++;
    //    //data.LoadEffect((effect) =>
    //    //{
    //    //    data.AutoID = reserveId;
    //    //    data.GameEffect.AutoID = reserveId;
    //    //    _effectAssetData[reserveId] = data;
    //    //    success?.Invoke();
    //    //});
    //    //effectAsset.LoadAssetAsync().Completed += (handle) =>
    //    //{
    //    //    var gameEffectObject = handle.Result.GetComponent<GameEffectObject>();
    //    //    if (gameEffectObject == null)
    //    //    {
    //    //        Debug.Log("Not Have GameEffectObject");
    //    //        return;
    //    //    }

    //    //    gameEffectObject.AutoID = reserveId;
    //    //    _effectAssetData[reserveId] = gameEffectObject;
    //    //    success?.Invoke();
    //    //};

    //    return data.AutoID;
    //}

    public bool CheckEffect(GameEffectData data)
    {
        if (_effectAssetData.ContainsKey(data.DataID) == false ||
            _effectAssetData[data.DataID] != data)
            return false;

        return true;
    }

    private bool ReserveEffect(int id)
    {
        if (_effectAssetData.ContainsKey(id) == false)
            return false;

        if (_effectReservePool.ContainsKey(id) == false)
            _effectReservePool[id] = new Queue<GameEffectObject>();

        var effectAsset = _effectAssetData[id].GameEffect;
        if (effectAsset == null)
            return false;

        for (int i = 0; i < _reserveCount; ++i)
        {
            var effect = Instantiate(effectAsset, _reserveParent);
            effect.AutoID = id;
            effect.gameObject.SetActive(false);

            _effectReservePool[id].Enqueue(effect);
        }

        return true;
    }

    public void RegisterEffectData(GameEffectData data)
    {
        if (data == null)
            return;

        if (_effectAssetData.ContainsKey(data.DataID) == true)
            return;

        _effectAssetData[data.DataID] = data;
    }

    public GameEffectObject UseEffect(int id)
    {
        if (_effectReservePool.ContainsKey(id) == false ||
            _effectReservePool[id].Count <= 0)
        {
            if (ReserveEffect(id) == false)
                return null;
        }

        var effect = _effectReservePool[id].Dequeue();
        effect.transform.SetParent(_useParent);
        effect.gameObject.SetActive(true);
        var info = new UseEffectInfo();
        info.Effect = effect;

        if(effect.LifeTime != -1)
            info.EffectRoutine = StartCoroutine(LifeRoutine(effect));

        if (_effectUsePool.ContainsKey(id) == false)
            _effectUsePool[id] = new List<UseEffectInfo>();

        _effectUsePool[id].Add(info);
        return effect;
    }

    public void ReturnEffect(GameEffectObject effect)
    {
        if (effect == null)
            return;

        int id = effect.AutoID;
        if (_effectUsePool.ContainsKey(id) == false ||
            _effectUsePool[id].Count <= 0)
            return;

        var effectList = _effectUsePool[id];
        for (int i = effectList.Count - 1; i >= 0; --i)
        {
            if (effectList[i].Effect == effect)
            {
                if (effectList[i].EffectRoutine != null)
                    StopCoroutine(effectList[i].EffectRoutine);

                _effectUsePool[id].RemoveAt(i);
                break;
            }
        }

        effect.gameObject.SetActive(false);
        _effectReservePool[id].Enqueue(effect);
        effect.transform.SetParent(_reserveParent);
        effect.transform.position = Vector3.zero;
        effect.Clear();
        effect.Release();
    }

    public void ReturnAll()
    {
        var effectPools = _effectUsePool.Values;
        foreach (var effectList in effectPools)
        {
            for (int i = effectList.Count - 1; i >= 0; --i)
            {
                ReturnEffect(effectList[i].Effect);
            }
        }
        _effectUsePool.Clear();
    }

    public void ReleaseAll()
    {
        ReturnAll();

        if (_effectReservePool == null)
            return;

        var effectQueues = _effectReservePool.Values;
        if (effectQueues == null) return;
        foreach (var effectQueue in effectQueues)
        {
            if (effectQueue == null)
                continue;

            var list = effectQueue.ToArray();
            if (list == null) continue;
            for (int i = list.Length - 1; i >= 0; --i)
            {
                if (list[i] == null) continue;
                Destroy(list[i].gameObject);
            }
        }
        _effectReservePool.Clear();
        _effectAssetData.Clear();

        //var assets = _effectAssetData.Values;
        //foreach (var asset in assets)
        //{
        //    asset.Release();
        //    //asset.ReleaseEffect();
        //}
        //_effectAssetData.Clear();

        //_autoIndex = 0;
    }

    private IEnumerator LifeRoutine(GameEffectObject effect)
    {
        yield return new WaitForSeconds(effect.LifeTime);

        ReturnEffect(effect);
    }
}
