﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

using UnityEngine.Events;
using UnityEngine.AddressableAssets;

public class GameEffectMgr : EightWork.Core.EightCore
{
    [SerializeField]
    private GameEffectPool _effectPool = null;

    //[SerializeField]
    //private GameEffectPool _staticEffectPool = null;

    public override void InitializedCore()
    {
        //_effectPool.UploadEffectAsset();
    }

    //public void UploadEffectAsset(GameEffectData data)
    //{
    //    //if (data.IsStatic == true)
    //    //    _staticEffectPool.UploadEffectAsset(data);
    //    //else
    //        _effectPool.UploadEffectAsset(data);
    //}

    public GameEffectObject UseEffect(GameEffectData data)
    {
        if (data == null)
            return null;

        if (_effectPool.CheckEffect(data) == false)
            _effectPool.RegisterEffectData(data);

        return _effectPool.UseEffect(data.DataID);

        //if (data.IsStatic == true)
        //{
        //    if (_staticEffectPool.CheckEffect(data) == false)
        //        _staticEffectPool.UploadEffectAsset(data);

        //    return _staticEffectPool.UseEffect(data.AutoID);
        //}
        //else
        //{
        //    if (_effectPool.CheckEffect(data) == false)
        //        _effectPool.UploadEffectAsset(data);

        //    return _effectPool.UseEffect(data.AutoID);
        //}
    }

    public void ReturnEffect(GameEffectObject effect/*, bool _isStatic = false*/)
    {
        //if (_isStatic == true)
        //    _staticEffectPool.ReturnEffect(effect);
        //else
            _effectPool.ReturnEffect(effect);
    }

    //public GameEffectObject UseEffect(int id, bool isStatic)
    //{
    //    if (isStatic == true)
    //        return _staticEffectPool.UseEffect(id);
    //    else
    //        return _effectPool.UseEffect(id);
    //}

    public void ReleaseAll(/*bool useStatic = false*/)
    {
        //if (useStatic == true)
        //    _staticEffectPool.ReleaseAll();

        _effectPool.ReleaseAll();
    }

    public void ReturnAll(/*bool useStatic = false*/)
    {
        //if (useStatic == true)
        //    _staticEffectPool.ReturnAll();

        _effectPool.ReturnAll();
    }
}
