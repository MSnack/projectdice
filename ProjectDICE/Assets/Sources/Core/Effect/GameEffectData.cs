﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "New GameEffectData", menuName = "GameEffectData", order = 3)]
public class GameEffectData : ScriptableObject
{
    [SerializeField]
    private int _dataId = -1;
    public int DataID => _dataId;

    //private int _autoId = -1;
    //public int AutoID { get => _autoId; }

    [SerializeField]
    private GameEffectObject _gameEffect = null;
    public GameEffectObject GameEffect => _gameEffect;

    [SerializeField]
    private bool _isStatic = false;
    public bool IsStatic => _isStatic;

    //public void UpdateAutoID(int id)
    //{
    //    if (_autoId == -1)
    //        _autoId = id;
    //}

    //public void Release()
    //{
    //    _autoId = -1;
    //}

    //private void OnEnable()
    //{
    //    _autoId = -1;
    //}
}
