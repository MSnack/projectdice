﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class BattleSceneState : EightSceneStateBase
{
    protected override void Initialize()
    {
        State = SCENE_TYPE.BATTLE;
    }

    public override void StartState()
    {
        EightUtil.GetCore<EightMsgMgr>().RegisterEvent(EIGHT_MSG.CHANGE_SCENE_START, ReleaseScene);
        EightUtil.GetCore<EightCameraMgr>().EightCamera.orthographic = true;
        EightUtil.GetCore<EightMsgMgr>().OnEightMsg(EIGHT_MSG.ON_FADE_PAUSE, new EightMsgContent(1));
    }

    private void ReleaseScene(EightMsgContent content)
    {
        EightUtil.GetCore<GameSkillActionMgr>().ReleaseAll();
        EightUtil.GetCore<GameUnitMgr>().ReleaseAll();
        EightUtil.GetCore<GameEffectMgr>().ReleaseAll();
        EightUtil.GetCore<GameHpBarMgr>().Clear();
        EightUtil.GetCore<GameSkinMgr>().ClearAll();
        EightUtil.GetCore<GameDamageFontMgr>().Clear();

        EightUtil.GetCore<EightMsgMgr>().RemoveEvent(EIGHT_MSG.CHANGE_SCENE_START, ReleaseScene);
    }

    public override void EndState()
    {

    }
}
