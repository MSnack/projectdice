﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EightWork;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;

using BestHTTP.SocketIO;

//using socket.io;

public class GameSocketMgr : EightWork.Core.EightCore
{
    [SerializeField, ReadOnly(ReadOnlyType.DISABLE_RUNTIME)]
    private string _serverPath = null;

    //[SerializeField, ReadOnly]
    //private Socket _socket = null;
    //public Socket GameSocket => _socket;

    private SocketManager _socketManager = null;
    public Socket GameSocket => _socketManager == null ? null : _socketManager.Socket;

    [SerializeField, ReadOnly]
    private bool _isLogin = false;
    public bool IsLogin => _isLogin;

    [SerializeField, ReadOnly]
    private string _roomId = null;
    public string RoomId { get => _roomId; set => _roomId = value; }

    public event UnityAction ComplateLogin = null;

    public event UnityAction OnDisconnectEvent = null;
    public event UnityAction OnReconnectEvent = null;

    [SerializeField, ReadOnly]
    private bool _isFirstConnect = false;
    public bool IsFirstConnect => _isFirstConnect;

    protected override void OnEnable()
    {
        base.OnEnable();

        StartCoroutine(LoginChecker());
    }

    public override void InitializedCore()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();

        //SocketManager.Instance.Reconnection = true;

        EightUtil.GetCore<GameUserMgr>().OnLoginCompleteEvent += () =>
        {
            _socketManager = new SocketManager(new System.Uri(_serverPath));

            GameSocket.On(SocketIOEventTypes.Connect, (s, p, a) =>
            {
                Debug.Log("Socket Connect!");

                EmitLogin();

                if (_isFirstConnect == false)
                    _isFirstConnect = true;
                else
                    OnReconnectEvent?.Invoke();
            });

            //GameSocket.On("join", (s, p, a) =>
            //{
            //    //var json = a[0] as Dictionary<string, object>;
            //});

            GameSocket.On("login", (s, p, a) =>
            {
                Debug.Log("Login Socket");

                //var json = a[0] as Dictionary<string, object>;

                _isLogin = true;
            });

            GameSocket.On(SocketIOEventTypes.Disconnect, (s, p, a) =>
            {
                _isLogin = false;
                OnDisconnectEvent?.Invoke();
            });

            GameSocket.On("JoinRoom", (s, p, a) =>
            {
                //var json = a[0] as Dictionary<string, object>;
                _roomId = a[0] as string;
            });
        };
    }

    private void Reconnect()
    {
        OnReconnectEvent?.Invoke();
    }

    private void EmitLogin()
    {
        JObject obj = new JObject();
        obj["token"] = EightUtil.GetCore<GameWebServerMgr>().ServerToken;

        GameSocket.Emit("login", obj.ToString());
    }

    private IEnumerator LoginChecker()
    {
        while (true)
        {
            yield return new WaitUntil(() => _isLogin == true);

            ComplateLogin?.Invoke();
            ComplateLogin = null;
        }
    }
}
