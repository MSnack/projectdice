﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using System.Threading.Tasks;
using Firebase.Auth;
using UnityEngine.Events;
using UnityEngine.Networking;

public enum LoginPlatform
{
    Unknown = -1,
    Guest = 0,
    GPGS = 1,
    
}

public class GameFirebaseMgr : EightWork.Core.EightCore
{
    [SerializeField, ReadOnly]
    private FirebaseAuth _firebaseAuth = null;

    [SerializeField, ReadOnly]
    private string _firebaseToken = null;

    public string FirebaseToken => _firebaseToken;

    private bool _hasToken = false;
    public bool HasToken => _hasToken;

    public FirebaseUser CurrentUser => _firebaseAuth?.CurrentUser;

    private GameWebRequestTranslator _translator = new GameWebRequestTranslator();

    public override void InitializedCore()
    {
        _firebaseAuth = FirebaseAuth.DefaultInstance;
        // this.SignInAnonymous().ContinueWith(GetTokenResult);
#if UNITY_EDITOR
        LoginFunction(_firebaseAuth.SignInAnonymouslyAsync(), "SignInAnonymouslyAsync")
                    .ContinueWith(GetTokenResult);
#endif
    }

    private void GetTokenResult(Task task)
    {
        _firebaseAuth.CurrentUser.TokenAsync(true).ContinueWith(token =>
        {
            _firebaseToken = token.Result;
            Debug.Log("idToken : " + _firebaseToken);
            _hasToken = true;
        });
    }

    public void SignIn(LoginPlatform platform, List<string> tokens, UnityAction failedEvent = null,
        UnityAction successEvent = null)
    {
        switch (platform)
        {
            case LoginPlatform.GPGS:
                Credential credential = PlayGamesAuthProvider.GetCredential(tokens[0]);
                LoginFunction(_firebaseAuth.SignInWithCredentialAsync(credential), "GPGSSignInCredentialAsync",
                        failedEvent, successEvent)
                    .ContinueWith(GetTokenResult);
                return;
            default:
                LoginFunction(_firebaseAuth.SignInAnonymouslyAsync(), "SignInAnonymouslyAsync",
                        failedEvent, successEvent)
                    .ContinueWith(GetTokenResult);
                return;
        }
    }

    public void LinkIn(LoginPlatform platform, List<string> tokens, UnityAction failedEvent = null,
        UnityAction successEvent = null)
    {
        switch (platform)
        {
            case LoginPlatform.GPGS:
                Credential credential = PlayGamesAuthProvider.GetCredential(tokens[0]);
                LoginFunction(_firebaseAuth.CurrentUser.LinkWithCredentialAsync(credential),
                        "GPGSLinkWithCredentialAsync", failedEvent, successEvent)
                    .ContinueWith(GetTokenResult);
                return;
            default:
                return;
        }
    }

    private async Task LoginFunction(Task<FirebaseUser> asyncConnectFunction, string logName,
        UnityAction failedEvent = null, UnityAction successEvent = null)
    {
        await asyncConnectFunction.ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError(logName + " was canceled.");
                failedEvent?.Invoke();
                return;
            }

            if (task.IsFaulted)
            {
                Debug.LogError(logName + " encountered an error: " + task.Exception);
                failedEvent?.Invoke();
                return;
            }

            FirebaseUser newUser = task.Result;
            Debug.LogFormat(logName + " in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
            Debug.Log("Provider Id : " + _firebaseAuth.CurrentUser.ProviderId);
            successEvent?.Invoke();
        });
    }

    public void ReallocateServerToken(UnityAction successEvent = null)
    {
        _translator.Request(GameWebRequestType.Realloc_Token,
            (trans) =>
            {
                var userInfo = trans.GetRequestData();
                EightUtil.GetCore<GameWebServerMgr>().ServerToken = userInfo["token"].ToString();
                successEvent?.Invoke();
            });
    }
}
