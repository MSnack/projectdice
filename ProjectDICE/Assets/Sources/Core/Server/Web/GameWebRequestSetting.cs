﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(fileName = "New GameWebRequestSetting", menuName = "Setting/GameWebRequestSetting", order = 2)]
public class GameWebRequestSetting : ScriptableObject
{
    [SerializeField]
    private string _serverPath = null;
    public string ServerPath => _serverPath;

    [System.Serializable]
    protected class ServerRequestData : SerializableDictionaryBase<GameWebRequestType, GameWebRequest> { }

    [SerializeField]
    private ServerRequestData _requestData = null;
    public Dictionary<GameWebRequestType, GameWebRequest> RequestData => _requestData.Clone();

    public bool IsEmpty => _requestData.Count <= 0;
}
