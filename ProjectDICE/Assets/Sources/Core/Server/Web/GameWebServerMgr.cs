﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork.Core;
using UnityEngine.Events;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine.Networking;
using EightWork;

public enum GameWebRequestType
{
    Basic_UserInfos,
    Basic_Auth_Test,
    Login_Firebase_Token,
    Get_UserDeckInfo,
    Get_UserBattleInfo,
    Update_UserDeckInfo,
    Update_UserBattle,
    Change_UserNickName,
    Basic_UserInfo,
    Create_UserCharacterInfo, //사용 금지
    Get_UserCharacterInfo,
    Update_UserCharacterInfo,
    Get_UserGoods,
    Update_UserGoods, //사용 금지
    Get_UserAllGoods,
    Get_UserBoxes,
    Open_UserBox,
    Get_Mails,
    Receive_Mail,
    Buy_ShopPackage,
    Get_DailyShopInfos,
    Refresh_DailyShop,
    Buy_DailyShopPackage,
    Refresh_UserQuestInfos,
    Get_UserQuestInfos,
    Update_UserQuestInfo, //사용 금지
    Receive_QuestReward,
    Get_UserEmoDeckInfo,
    Update_UserEmoDeckInfo,
    Get_UserEmoticonInfos,
    Get_Version,
    Realloc_Token,
    Check_ShopLimitState,
    Box_RemoveBoxTime,
    Complete_Tutorial,

    Debug_CreateUserBox = 10000,
    Debug_RemoveBoxTime = 10001,
}

public enum WebMsgType
{
    Get,
    Post,
}

[System.Serializable]
public class GameWebRequest
{
    [SerializeField]
    private string _function = null;
    public string Function => _function;

    [SerializeField]
    private WebMsgType _msgType = WebMsgType.Get;
    public WebMsgType MsgType => _msgType;

    [SerializeField]
    private bool _useToken = false;
    public bool UseToken => _useToken;

    public UnityAction<string> SuccessEvent = null;
}

public class GameWebMsgContent
{
    private string _function = null;
    public string Function => _function;

    private string _query = null;
    public string Query
    {
        get => _query;
        set => _query = value;
    }

    private WebMsgType _msgType = WebMsgType.Get;
    public WebMsgType MsgType => _msgType;

    private bool _useToken = false;
    public bool UseToken => _useToken;

    public string SendDataString = null;
    public UnityAction<string> CallBack = null;

    public GameWebMsgContent(GameWebRequest info, string sendData = null, UnityAction<string> successEvent = null)
    {
        _function = info.Function;
        _msgType = info.MsgType;
        _useToken = info.UseToken;
        SendDataString = sendData;
        CallBack = successEvent;
    }
}

public class GameWebRequestTranslator
{
    private JObject _sendData = new JObject();
    private string _requestData = null;

    //private event UnityAction<GameWebRequestTranslator> SuccessEvent = null;

    public void SetSendData(string name, int data)
    {
        _sendData[name] = data;
    }
    public void SetSendData(string name, float data)
    {
        _sendData[name] = data;
    }
    public void SetSendData(string name, string data)
    {
        _sendData[name] = data;
    }
    public void SetSendData<TClass>(string name, TClass data) where TClass : class
    {
        _sendData[name] = JObject.Parse(JsonConvert.SerializeObject(data));
    }
    public void SetSendData(string jsonData)
    {
        _sendData = JObject.Parse(jsonData);
    }
    public void SetSendDataAtClass<TClass>(TClass data) where TClass : class
    {
        _sendData = JObject.Parse(JsonConvert.SerializeObject(data));
    }
    public void ClearSendData()
    {
        _sendData.RemoveAll();
    }

    public void Request(GameWebRequestType type, UnityAction<GameWebRequestTranslator> successEvent = null)
    {
        //if (successEvent != null)
            //SuccessEvent += successEvent;

        EightUtil.GetCore<GameWebServerMgr>().RequestData(type, JsonConvert.SerializeObject(_sendData), (jsonData) =>
        {
            Response(jsonData);
            if (CheckServerCheckLog(jsonData) == false)
                successEvent?.Invoke(this);
        });
        _sendData.RemoveAll();
    }

    public void Request(GameWebRequestType type, string query = null, UnityAction<GameWebRequestTranslator> successEvent = null)
    {
        //if (successEvent != null)
            //SuccessEvent += successEvent;

        EightUtil.GetCore<GameWebServerMgr>().RequestData(type, JsonConvert.SerializeObject(_sendData), query,
            (jsonData) =>
            {
                Response(jsonData);
                if (CheckServerCheckLog(jsonData) == false)
                    successEvent?.Invoke(this);
            });
        _sendData.RemoveAll();
    }

    public TRequestData GetRequestData<TRequestData>()
        where TRequestData : class
    {
        return JsonConvert.DeserializeObject<TRequestData>(_requestData);
    }

    public JObject GetRequestData()
    {
        return JObject.Parse(_requestData);
    }

    private void Response(string jsonData)
    {
        _requestData = jsonData;
        //TRequestData requestData = JsonConvert.DeserializeObject<TRequestData>(jsonData);
        //SuccessEvent?.Invoke(this);

        //SuccessEvent = null;
    }

    private bool CheckServerCheckLog(string jsonData)
    {
        var check = jsonData.IndexOf("checkResult", StringComparison.Ordinal);
        if (check < 0) return false;

        var jObject = JObject.Parse(jsonData);

        string message = jObject["message"].ToString();
        string url = jObject["url"].ToString();

        int buttonSize = string.IsNullOrEmpty(url) ? 1 : 2;
        NormalPopup.ButtonConfig[] buttons = new NormalPopup.ButtonConfig[buttonSize];
        if(!string.IsNullOrEmpty(url)) 
            buttons[0] = new NormalPopup.ButtonConfig("자세히", () => Application.OpenURL(url));
        buttons[buttonSize - 1] = new NormalPopup.ButtonConfig("확인", Application.Quit);

        GamePopupMgr.CloseLoadingPopup();
        GamePopupMgr.OpenNormalPopup("서버 점검", message, buttons);
        return true;
    }
}

public class GameWebServerMgr : EightCore
{
    [SerializeField]
    private GameWebRequestSetting _requestSetting = null;

    private Dictionary<GameWebRequestType, GameWebRequest> _requestInfos = new Dictionary<GameWebRequestType, GameWebRequest>();

    private Queue<GameWebMsgContent> _reserveServerMsgQueue = new Queue<GameWebMsgContent>();
    private GameWebMsgContent _nowMsgContent = null;

    private string _serverPath = null;
    public string ServerPath => _serverPath;

    [SerializeField, ReadOnly]
    private string _serverToken = null;
    public string ServerToken
    {
        get
        {
            if (_serverToken == null)
                return "NoToken";

            return _serverToken;
        }
        set => _serverToken = value;
    }

    private int _lastErrorCode = -1;
    public int LastErrorCode => _lastErrorCode;

    public override void InitializedCore()
    {
        foreach (var pair in _requestSetting.RequestData)
        {
            AddRequestInfo(pair.Key, pair.Value);
        }

        _serverPath = _requestSetting.ServerPath;
    }

    private void SuccessLoginToken(string jsonData)
    {
        JObject jObject = JObject.Parse(jsonData);
        ServerToken = jObject["token"].ToString();
    }

    private void AddRequestInfo(GameWebRequestType type, GameWebRequest info)
    {
        _requestInfos[type] = info;
    }

    public void RequestData(GameWebRequestType type, string jsonData = null, UnityAction<string> successEvent = null)
    {
        if (successEvent != null)
            _requestInfos[type].SuccessEvent = successEvent;

        var request = _requestInfos[type];
        var content = new GameWebMsgContent(request, jsonData, successEvent);
        SendMsg(content);
    }

    public void RequestData(GameWebRequestType type, string jsonData = null, string query = null, UnityAction<string> successEvent = null)
    {
        if (successEvent != null)
            _requestInfos[type].SuccessEvent = successEvent;

        var request = _requestInfos[type];
        var content = new GameWebMsgContent(request, jsonData, successEvent);
        content.Query = query;
        SendMsg(content);
    }

    private void SendMsg(GameWebMsgContent webMsgContent)
    {
        if (_nowMsgContent != null)
        {
            ReserveMsg(webMsgContent);
            return;
        }

        switch (webMsgContent.MsgType)
        {
            case WebMsgType.Get:
                {
                    WebMemberGetSend(webMsgContent);
                    break;
                }
            case WebMsgType.Post:
                {
                    WebMemberPostSend(webMsgContent);
                    break;
                }
        }
    }

    private void ReserveMsg(GameWebMsgContent webMsgContent)
    {
        _reserveServerMsgQueue.Enqueue(webMsgContent);
    }

    private void ReserveMsg_First(GameWebMsgContent webMsgContent)
    {
        Queue<GameWebMsgContent> tempMsgList = new Queue<GameWebMsgContent>();
        while (_reserveServerMsgQueue.Count != 0)
            tempMsgList.Enqueue(_reserveServerMsgQueue.Dequeue());

        _reserveServerMsgQueue.Enqueue(webMsgContent);
        while (tempMsgList.Count != 0)
            _reserveServerMsgQueue.Enqueue(tempMsgList.Dequeue());
    }

    private void WebMemberGetSend(GameWebMsgContent webMsgContent)
    {
        if (_nowMsgContent != null)
        {
            ReserveMsg(webMsgContent);
            return;
        }

        _nowMsgContent = webMsgContent;
        StartCoroutine(WebGetSend(_nowMsgContent));
    }

    private void WebMemberPostSend(GameWebMsgContent webMsgContent)
    {
        if (_nowMsgContent != null)
        {
            ReserveMsg(webMsgContent);
            return;
        }

        _nowMsgContent = webMsgContent;
        StartCoroutine(WebPostSend(_nowMsgContent));
    }

    private void ClearMsgRoutine()
    {
        _nowMsgContent = null;
    }

    private void Update()
    {
        if (_nowMsgContent != null || _reserveServerMsgQueue.Count <= 0)
            return;

        SendMsg(_reserveServerMsgQueue.Dequeue());
    }

    private IEnumerator WebGetSend(GameWebMsgContent webMsgContent)
    {
        UnityWebRequest unityWebRequest = UnityWebRequest.Get(_serverPath + webMsgContent.Function + webMsgContent.Query);
        unityWebRequest.SetRequestHeader("Content-Type", "application/json");
        if (webMsgContent.UseToken == true)
            unityWebRequest.SetRequestHeader("authorization", ServerToken);

        yield return unityWebRequest.SendWebRequest();

        if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
        {
            Debug.LogError(unityWebRequest.error);
            ErrorReturn(unityWebRequest.downloadHandler.text);
        }
        else
        {
#if UNITY_EDITOR
            Debug.Log(unityWebRequest.downloadHandler.text);
#endif

            if (webMsgContent.CallBack != null)
            {
                webMsgContent.CallBack.Invoke(unityWebRequest.downloadHandler.text);
                webMsgContent.CallBack = null;
            }
        }

        ClearMsgRoutine();
    }

    private IEnumerator WebPostSend(GameWebMsgContent webMsgContent)
    {
        Debug.Log(string.Format("WebMsg Sended. Function-\"{0}\", Variable-\"{1}\"", webMsgContent.Function, webMsgContent.SendDataString));

        List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        UnityWebRequest unityWebRequest = null;

        if (string.IsNullOrEmpty(webMsgContent.Query) == true)
            unityWebRequest = UnityWebRequest.Post(_serverPath + webMsgContent.Function, formData);
        else
            unityWebRequest = UnityWebRequest.Post(_serverPath + webMsgContent.Function + webMsgContent.Query, formData);

        unityWebRequest.SetRequestHeader("Content-Type", "application/json");

        if (webMsgContent.UseToken == true)
            unityWebRequest.SetRequestHeader("authorization", ServerToken);

        if (string.IsNullOrEmpty(webMsgContent.SendDataString) == false)
        {
            var sendData = System.Text.Encoding.UTF8.GetBytes(webMsgContent.SendDataString);
            unityWebRequest.uploadHandler = new UploadHandlerRaw(sendData);
        }

        yield return unityWebRequest.SendWebRequest();

        if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
        {
            if (unityWebRequest.isNetworkError)
                Debug.LogError(unityWebRequest.error);

            if (unityWebRequest.isHttpError)
                Debug.LogError(unityWebRequest.downloadHandler.text);
            ErrorReturn(unityWebRequest.downloadHandler.text);
        }
        else
        {
#if UNITY_EDITOR
            Debug.Log(unityWebRequest.downloadHandler.text);
#endif

            if (webMsgContent.CallBack != null)
                webMsgContent.CallBack.Invoke(unityWebRequest.downloadHandler.text);
        }

        ClearMsgRoutine();
    }

    public bool IsError(string jsonData)
    {
        if (string.IsNullOrEmpty(jsonData))
            return true;

        _lastErrorCode = SystemIndex.InvalidInt;
        JObject jsonList;

        try
        {
            jsonList = JsonConvert.DeserializeObject<JObject>(jsonData);
        }
        catch
        {
            Debug.Log("ServerMsg ErrorCheck - Can't Translate ServerMsg To Json");
            return true;
        }

        try
        {
            var result = jsonList["result"];

            if (jsonList["result"] == null)
                return true;
        }
        catch
        {
            Debug.Log("ErrorCheck - ServerMsg Is Not Has \"Result\" Item");
            return false;
        }

        try
        {
            _lastErrorCode = int.Parse(jsonList["result"].ToString());
        }
        catch
        {
            Debug.Log("ErrorCheck - ServerMsg's \"Result\" Item Is Not a Number");
            return true;
        }

        if (_lastErrorCode == -1)
            return true;

        return false;
    }

    private void ErrorReturn(string error)
    {
        if (string.IsNullOrEmpty(error))
        {
            Debug.Log("error result null");
            return;
        }

        IsError(error);

        Debug.Log(string.Format("Server Error Code - {0}", _lastErrorCode));
        EightUtil.GetCore<EightMsgMgr>().OnEightMsg(EIGHT_MSG.SERVER_ERROR_CODE, new EightMsgContent(_lastErrorCode));
    }
}