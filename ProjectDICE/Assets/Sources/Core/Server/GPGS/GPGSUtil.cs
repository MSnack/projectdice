﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;
#if UNITY_ANDROID
using GooglePlayGames.BasicApi;
using GooglePlayGames;
#endif
public class GPGSUtil : MonoBehaviour
{
#if UNITY_ANDROID
    private static PlayGamesClientConfiguration _config;
#endif
    
    public static bool IsLoaded { get; private set; }
    
    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID
        InitGPGSConfig();
#endif
    }

    private static void InitGPGSConfig()
    {
#if UNITY_ANDROID
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration
                .Builder()
            .RequestServerAuthCode(false)
            .RequestIdToken()
            .Build();

        //커스텀된 정보로 GPGS 초기화
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        //GPGS 시작
        PlayGamesPlatform.Activate();
        IsLoaded = true;
#endif
    }

    public static void GetServerAuthCode(UnityAction<string> successEvent = null, UnityAction failedEvent = null)
    {
#if UNITY_ANDROID
        if (!IsLoaded)
        {
            InitGPGSConfig();
        }
        
        // GPGS 로그인 되어 있는 경우
        if (Social.localUser.authenticated)
        {
            string authCode = PlayGamesPlatform.Instance.GetServerAuthCode();
            successEvent?.Invoke(authCode);
            return;
        }

        // GPGS 로그인이 되어 있지 않은 경우
        Social.localUser.Authenticate((success, errorCode) =>
        {
            //로그인 시도 성공
            if (success)
            {
                string authCode = PlayGamesPlatform.Instance.GetServerAuthCode();
                successEvent?.Invoke(authCode);
            }
            else //로그인 실패
            {
                Debug.LogError("Google Play Game Service Login Failed.");
                failedEvent?.Invoke();
            }
        });
#else
    failedEvent?.Invoke();
#endif
    }
}
