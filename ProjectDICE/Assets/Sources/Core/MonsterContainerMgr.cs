﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class MonsterContainerMgr : EightReceiver
{
    //[SerializeField]
    //private GameUnitTeam team = GameUnitTeam.Null;

    [SerializeField]
    private MonsterTilePool _monsterTilePool = null;

    private bool _isCheck = false;

    private GameUnit _firstMonster = null;
    public GameUnit FirstMonster => _firstMonster;

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();

        _isCheck = true;
    }

    private void Update()
    {
        if (EightUtil.IsLoadCompleteDevice == false)
            return;

        //var monsterList = EightUtil.GetCore<GameUnitMgr>().FindUnitList<GameUnit>(team);
        //if (monsterList == null || monsterList.Length <= 0)
        //{
        //    _firstMonster = null;
        //    return;
        //}

        //GameUnit first = null;
        //foreach (var monster in monsterList)
        //{
        //    //Monster monster = unit as Monster;
        //    if (first == null)
        //    {
        //        first = monster;
        //        continue;
        //    }

        //    if (monster.CurrIndex > first.CurrIndex)
        //        first = monster;
        //    else if (monster.CurrIndex == first.CurrIndex)
        //    {
        //        var tilePos = monster.TilePool.TilePath[monster.CurrIndex].GetPositionForUnit();
        //        float monsterDist = (tilePos - monster.transform.position).magnitude;
        //        float firstDist = (tilePos - first.transform.position).magnitude;

        //        if (monsterDist > firstDist)
        //            first = monster;
        //    }
        //}

        //_firstMonster = first;
    }
}
