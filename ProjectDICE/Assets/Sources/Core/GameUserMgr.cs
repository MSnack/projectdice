﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine.Events;

[System.Serializable]
public class GameUserInfo : DataBaseSystem
{
    [SerializeField]
    private int _userId = SystemIndex.InvalidInt;
    public int UserID { get => _userId; set => _userId = value; }

    [SerializeField]
    private string UId = null;
    public string UID { get => UId; set => UId = value; }

    [SerializeField]
    private string _nickName = null;
    public string NickName { get => _nickName; set => _nickName = value; }

    [SerializeField]
    private string _token = null;
    public string Token { get => _token; set => _token = value; }
    public override void SetJson()
    {
        JsonObject["UId"] = UId;
        JsonObject["nickName"] = _nickName;
    }
}

public class GameUserMgr : EightWork.Core.EightCore
{
    [SerializeField, ReadOnly]
    private GameUserInfo _userInfo = null;

    public GameUserInfo UserInfo => _userInfo;

    private GameWebRequestTranslator _loginTranslator = new GameWebRequestTranslator();

    private bool _isCompleteLogin = false;
    public bool IsCompleteLogin => _isCompleteLogin;

    public event UnityAction OnLoginCompleteEvent = null;

    public override void InitializedCore()
    {
        StartCoroutine(LoginRoutine());

        StartCoroutine(LoginCompleteEventChecker());
    }

    public void RefreshUserInfo(UnityAction successEvent = null)
    {
        if (_isCompleteLogin == false) return;
        
        _loginTranslator.Request(GameWebRequestType.Basic_UserInfo, (trans) =>
        {
            _userInfo = trans.GetRequestData<GameUserInfo>();
            _userInfo.Token = EightUtil.GetCore<GameWebServerMgr>().ServerToken;
            successEvent?.Invoke();
        });
    }

    private IEnumerator LoginCompleteEventChecker()
    {
        while (true)
        {
            yield return new WaitUntil(()=>_isCompleteLogin == true);
            
            OnLoginCompleteEvent?.Invoke();
            OnLoginCompleteEvent = null;
        }
    }

    private IEnumerator LoginRoutine()
    {
        GameFirebaseMgr firebaseMgr = null;
        GameWebServerMgr serverMgr = null;
        yield return new WaitUntil(() => (firebaseMgr = EightUtil.GetCore<GameFirebaseMgr>()) != null);
        yield return new WaitUntil(() => firebaseMgr.HasToken == true);
        yield return new WaitUntil(() => (serverMgr = EightUtil.GetCore<GameWebServerMgr>()) != null);

        Debug.Log("Firebase Login : " + firebaseMgr.FirebaseToken);
        
        _loginTranslator.Request(GameWebRequestType.Login_Firebase_Token, firebaseMgr.FirebaseToken, LoginRequest);
    }

    private void LoginRequest(GameWebRequestTranslator translator)
    {
        Debug.Log("LoginRequest = " + translator.GetRequestData());
        _userInfo = translator.GetRequestData<GameUserInfo>();
        EightUtil.GetCore<GameWebServerMgr>().ServerToken = _userInfo.Token;

        _isCompleteLogin = true;
    }
}
