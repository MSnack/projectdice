﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.Events;

public class GamePopupMgr : EightWork.Core.EightCore
{

    [Header("Popup Prefabs")]
    [SerializeField]
    private NormalPopup _normalPopup = null;
    
    [SerializeField]
    private SimplePopup _simplePopup = null;
    
    [SerializeField]
    private ScrollPopup _scrollPopup = null;

    [SerializeField]
    private RewardPopup _rewardPopup = null;

    [SerializeField]
    private PrefabPopup _prefabPopup = null;

    [SerializeField]
    private GameObject _loadingPopup = null;

    [SerializeField]
    private GameObject _silentBlockPopup = null;


    private static UnityAction<string, string, NormalPopup.ButtonConfig[]> _callNormalPopup;
    private static UnityAction<string> _callSimplePopup;
    private static UnityAction<string, string, NormalPopup.ButtonConfig[]> _callScrollPopup;
    private static UnityAction<string, GameObject, NormalPopup.ButtonConfig[]> _callPrefabPopup;
    private static UnityAction<bool, bool> _callLoadingPopup;
    private static UnityAction<List<RewardItemInfo>> _callRewardPopup;
    private static bool _isPopupOpen = false;
    
    private static UnityAction _findOpenPopup;
    
    public override void InitializedCore()
    {
        _callNormalPopup += OnNormalPopup;
        _callSimplePopup += OnSimplePopup;
        _callScrollPopup += OnScrollPopup;
        _callPrefabPopup += OnPrefabPopup;
        _callLoadingPopup += OnLoadingPopup;
        _callRewardPopup += OnRewardPopup;
        _findOpenPopup += OnFindOpenPopup;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnNormalPopup(string title, string context, NormalPopup.ButtonConfig[] configs)
    {
        _normalPopup?.SetData(title, context, configs);
    }
    
    private void OnSimplePopup(string context)
    {
        if(_simplePopup?.gameObject.activeInHierarchy ?? false)
            _simplePopup.gameObject.SetActive(false);
        _simplePopup?.SetData(context);
    }
    
    private void OnScrollPopup(string title, string context, NormalPopup.ButtonConfig[] configs)
    {
        _scrollPopup?.SetData(title, context, configs);
    }
    
    private void OnPrefabPopup(string title, GameObject content, NormalPopup.ButtonConfig[] configs)
    {
        _prefabPopup?.SetData(title, content, configs);
    }

    private void OnLoadingPopup(bool enable, bool isSilent)
    {

        if (enable)
        {
            if (isSilent)
                _silentBlockPopup.transform.localPosition = Vector3.zero;
            else
                _loadingPopup.transform.localPosition = Vector3.zero;
        }
        else
        {
            var position = new Vector3(10000, 10000, 0);
            _silentBlockPopup.transform.localPosition = position;
            _loadingPopup.transform.localPosition = position;
        }
        
    }

    private void OnRewardPopup(List<RewardItemInfo> infos)
    {
        _rewardPopup?.SetData(infos);
    }

    private void OnFindOpenPopup()
    {
        _isPopupOpen =
            _normalPopup.gameObject.activeInHierarchy ||
            _simplePopup.gameObject.activeInHierarchy ||
            _scrollPopup.gameObject.activeInHierarchy ||
            _rewardPopup.gameObject.activeInHierarchy ||
            _prefabPopup.gameObject.activeInHierarchy;
    }

    public static void OpenNormalPopup(string title, string context, NormalPopup.ButtonConfig[] configs = null)
    {
        if (configs == null)
        {
            configs = new[] { new NormalPopup.ButtonConfig("확인", NormalPopup.Close) };
        }
        
        _callNormalPopup?.Invoke(title, context, configs);
    }
    
    public static void OpenSimplePopup(string context)
    {
        _callSimplePopup?.Invoke(context);
    }

    public static void OpenScrollPopup(string title, string context, NormalPopup.ButtonConfig[] configs = null)
    {
        if (configs == null)
        {
            configs = new[] { new NormalPopup.ButtonConfig("확인", ScrollPopup.Close) };
        }
        
        _callScrollPopup?.Invoke(title, context, configs);
    }
    
    public static void OpenPrefabPopup(string title, GameObject contentPrefab, NormalPopup.ButtonConfig[] configs = null)
    {
        if (configs == null)
        {
            configs = new[] { new NormalPopup.ButtonConfig("확인", ScrollPopup.Close) };
        }
        
        _callPrefabPopup?.Invoke(title, contentPrefab, configs);
    }

    public static void OpenLoadingPopup(bool isSilent = false)
    {
        _callLoadingPopup?.Invoke(true, isSilent);
    }

    public static void OpenRewardPopup(List<RewardItemInfo> infos)
    {
        _callRewardPopup?.Invoke(infos);
    }

    public static void CloseLoadingPopup()
    {
        _callLoadingPopup?.Invoke(false, false);
    }

    public static bool IsPopupOpen()
    {
        _findOpenPopup?.Invoke();
        return _isPopupOpen;
    }
    
}
