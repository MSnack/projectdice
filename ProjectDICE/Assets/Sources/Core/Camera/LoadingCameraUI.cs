﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class LoadingCameraUI : EightReceiver
{
    public enum LoadingState
    {
        NONE,
        BATTLE,
    }

    [Header("State")]
    [SerializeField, ReadOnly]
    private LoadingState _state;
    public LoadingState State => _state;

    [SerializeField]
    public GameObject _noneStateObject = null;
    
    [SerializeField]
    public GameObject _battleStateObject = null;

    [Header("None State")]
    [SerializeField]
    private Image _characterImage;
    
    [SerializeField]
    private Text _characterDesciption;

    [SerializeField]
    private Camera _loadingCamera = null;

    [Header("Battle State")]
    [SerializeField]
    private Image _circleBarImage = null;

    [SerializeField]
    private Text _loadingPercentText = null;

    [SerializeField]
    private Text _loadingTipText = null;

    private GameUnitDataMgr _unitDataMgr = null;
    private EightSceneMgr _sceneMgr = null;
    private Coroutine _charCoroutine = null;

    private static UnityAction<LoadingState> _changeState = null;

    // Start is called before the first frame update
    void Awake()
    {
        _changeState += SetStateFunc;
    }

    public static void SetState(LoadingState state)
    {
        _changeState?.Invoke(state);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        _loadingCamera.enabled = true;
        if(_state == LoadingState.NONE)
            _charCoroutine = StartCoroutine(InitCharacter());
        if(_state == LoadingState.BATTLE)
            _charCoroutine = StartCoroutine(InitBattleState());

    }

    protected override void OnDisable()
    {
        base.OnDisable();
        if (_charCoroutine != null)
        {
            StopCoroutine(_charCoroutine);
            _charCoroutine = null;
        }
        _loadingCamera.enabled = false;
    }

    private IEnumerator InitCharacter()
    {
        if(_unitDataMgr == null) _unitDataMgr = EightUtil.GetCore<GameUnitDataMgr>();
        if(!_unitDataMgr.IsLoadCompleted)
            yield return new WaitUntil(() => _unitDataMgr.IsLoadCompleted);

        var charIdList = _unitDataMgr.CharactersIDList;
        var charData = _unitDataMgr.GetCharacaterData(charIdList[Random.Range(0, charIdList.Length - 1)]);

        _characterImage.sprite = charData.UnitPreviewSprite;
        _characterDesciption.text = charData.Description;
    }

    private IEnumerator InitBattleState()
    {
        if(_sceneMgr == null) _sceneMgr = EightUtil.GetCore<EightSceneMgr>();
        _circleBarImage.fillAmount = 0;
        _loadingPercentText.text = "0%";
        
        if(_sceneMgr.LoadingPercentage >= 1) 
            yield return new WaitUntil(() => _sceneMgr.LoadingPercentage < 1);
        
        while (_sceneMgr.LoadingPercentage < 1)
        {
            float v = _sceneMgr.LoadingPercentage - _circleBarImage.fillAmount;
            v /= 3;
            _circleBarImage.fillAmount = v;
            _loadingPercentText.text = $"{_sceneMgr.LoadingPercentage:P0}";

            yield return null;
        }
        _circleBarImage.fillAmount = 1;
        _loadingPercentText.text = "100%";
    }

    private void SetStateFunc(LoadingState state)
    {
        _noneStateObject.SetActive(state == LoadingState.NONE);
        _battleStateObject.SetActive(state == LoadingState.BATTLE);
        _state = state;
    }
}
