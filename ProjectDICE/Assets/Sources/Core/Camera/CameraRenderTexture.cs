﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class CameraRenderTexture : MonoBehaviour
{
    [SerializeField]
    private Camera _renderCamera = null;

    [SerializeField, ReadOnly()]
    private RenderTexture _renderTexture = null;
    

    public RenderTexture GetRenderTexture()
    {
        // _renderCamera.Render();
        return _renderTexture;
    }

    private void OnEnable()
    {
        _renderTexture = new RenderTexture((int)_renderCamera.pixelWidth, (int)_renderCamera.pixelHeight, 16, RenderTextureFormat.RGB565);
        _renderCamera.targetTexture = _renderTexture;
    }

    private void OnDisable()
    {
        _renderTexture.Release();
        _renderTexture = null;
    }
}
