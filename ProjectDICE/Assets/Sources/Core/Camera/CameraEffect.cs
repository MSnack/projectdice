﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.EventSystems;

namespace EightWork
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Camera))]
    [AddComponentMenu("Image Effects/Screen Transition")]
    public class CameraEffect : EightMsgReceiver
    {
        /// Provides a shader property that is set in the inspector
        /// and a material instantiated from the shader
        public Shader shader;

        [Range(0, 1.0f)]
        public float _maskValue;
        public Color maskColor = Color.black;
        public Texture2D maskTexture;
        public Texture2D loadingTexture;
        public CameraRenderTexture loadingCamera;

        //[Range(0, 1.0f)]
        //public float loadingTexSize;
        public bool maskInvert;
        private Material _material;
        private PhysicsRaycaster _raycaster = null;
        private bool isMaterialEnabled = false;

        public bool IsComplete
        {
            get
            {
                return maskInvert == true ? _maskValue > 0.0f : _maskValue < 1.0f;
            }
        }

        [SerializeField]
        private float _speed = 2.0f;

        private Coroutine _fadeInRoutine = null;
        private Coroutine _fadeOutRoutine = null;

        private EightSceneMgr _sceneMgr = null;

        [SerializeField, ReadOnly]
        private bool _isPause = false;
        public bool IsPause { get => _isPause; set => _isPause = value; }

        protected override void RegisterMsg()
        {
            base.RegisterMsg();

            this.RegisterEvent(EIGHT_MSG.CHANGE_SCENE_START, FadeIn);
            this.RegisterEvent(EIGHT_MSG.CHANGE_SCENE_END, FadeOut);
            this.RegisterEvent(EIGHT_MSG.ON_FADE_PAUSE, OnFadePause);
        }

        protected override void UnRegisterMsg()
        {
            base.UnRegisterMsg();

            this.RemoveEvent(EIGHT_MSG.ON_FADE_PAUSE, OnFadePause);
            this.RemoveEvent(EIGHT_MSG.CHANGE_SCENE_START, FadeIn);
            this.RemoveEvent(EIGHT_MSG.CHANGE_SCENE_END, FadeOut);
        }

        public Material Material
        {
            get
            {
                if (_material == null)
                {
                    _material = new Material(shader);
                    _material.hideFlags = HideFlags.HideAndDontSave;
                }
                return _material;
            }
        }

        private void Awake()
        {
            _raycaster = this.GetComponent<PhysicsRaycaster>();
            EightUtil.LoadCompleteDevice += OnCoreInit;
            
            loadingCamera.gameObject.SetActive(false);
        }

        private void OnFadePause(EightMsgContent eightMsgContent)
        {
            var isPause = eightMsgContent.Index > 0 ? true : false;
            IsPause = isPause;
        }

        private void OnCoreInit()
        {
            _sceneMgr = EightUtil.GetCore<EightSceneMgr>();
        }

        private void OpenPhysicsRayCaster()
        {
            _raycaster.enabled = true;
        }

        private void ClosePhysicsRayCaster()
        {
            _raycaster.enabled = false;
        }

        void Start()
        {
            // Disable if we don't support image effects
            if (!SystemInfo.supportsImageEffects)
            {
                enabled = false;
                return;
            }

            shader = Shader.Find("Hidden/ScreenTransitionImageEffect");

            // Disable the image effect if the shader can't
            // run on the users graphics card
            if (shader == null || !shader.isSupported)
                enabled = false;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (_material)
            {
                DestroyImmediate(_material);
            }
        }

        void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            if (!enabled || !isMaterialEnabled)
            {
                Graphics.Blit(source, destination);
                return;
            }

            Material.SetColor("_MaskColor", maskColor);
            Material.SetFloat("_MaskValue", _maskValue);
            Material.SetTexture("_MainTex", source);
            Material.SetTexture("_MaskTex", maskTexture);
            Material.SetTexture("_LoadingTex", loadingCamera.GetRenderTexture());

            if (maskInvert)
                Material.SetFloat("_INVERT_MASK", 1);
            else
                Material.SetFloat("_INVERT_MASK", 0);

            Graphics.Blit(source, destination, Material);
        }

        private void FadeIn(EightMsgContent gameMsgContent)
        {
            isMaterialEnabled = true;
            if (_fadeInRoutine != null)
            {
                StopCoroutine(_fadeInRoutine);
                _fadeInRoutine = null;
            }
            
            _sceneMgr.LoadSceneActivation = false;

            _maskValue = 0.0f;
            maskInvert = false;
            loadingCamera.gameObject.SetActive(true);
            _fadeInRoutine = StartCoroutine(FadeInRoutine());
        }

        private void FadeOut(EightMsgContent gameMsgContent)
        {
            isMaterialEnabled = true;
            if (_fadeOutRoutine != null)
            {
                StopCoroutine(_fadeOutRoutine);
                _fadeOutRoutine = null;
            }
            
            _sceneMgr.LoadSceneActivation = false;

            _maskValue = 1.0f;
            maskInvert = false;
            _fadeOutRoutine = StartCoroutine(FadeOutRoutine());
        }

        private IEnumerator FadeInRoutine()
        {
            GamePopupMgr.OpenLoadingPopup(true);
            if (IsPause == true)
                yield return new WaitUntil(() => IsPause == false);

            while (_maskValue < 1.0f)
            {
                _maskValue += _speed * Time.deltaTime;
                yield return null;
            }

            _sceneMgr.LoadSceneActivation = true;
            GamePopupMgr.CloseLoadingPopup();
            yield return null;
        }

        private IEnumerator FadeOutRoutine()
        {
            GamePopupMgr.OpenLoadingPopup(true);
            if (IsPause == true)
                yield return new WaitUntil(() => IsPause == false);

            maskInvert = true;
            _maskValue = 0.0f;
            while (_maskValue < 1.0f)
            {
                _maskValue += _speed * Time.deltaTime;
                yield return null;
            }

            _sceneMgr.LoadSceneActivation = true;
            loadingCamera.gameObject.SetActive(false);
            GamePopupMgr.CloseLoadingPopup();
            isMaterialEnabled = false;
            yield return null;
        }
    }
}