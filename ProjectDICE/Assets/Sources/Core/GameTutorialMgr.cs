﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using EightWork.Core;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class GameTutorialMgr : EightCore
{
    [SerializeField, ReadOnly]
    private GameTutorialPanel _currentPanel = null;

    [SerializeField]
    private Transform _targetTransform = null;

    [SerializeField, ReadOnly]
    private bool _isTutorialActive = false;
    public bool IsTutorialActive => _isTutorialActive;

    private string _tutorialName = "";
    public string TutorialName => _tutorialName;

    private TutorialController _currentController = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void InitializedCore()
    {
    }

    public void LoadTutorial(TutorialController controller)
    {
        Addressables.LoadAssetAsync<GameObject>(controller.TutorialPrefab).Completed +=
            OnCompleted;
        _currentController = controller;
    }

    private void OnCompleted(AsyncOperationHandle<GameObject> result)
    {
        var dataList = result.Result;
        _tutorialName = dataList.name;
        _currentPanel = Instantiate(dataList, _targetTransform).GetComponent<GameTutorialPanel>();
        _isTutorialActive = true;
    }

    public void ChangePage(short index)
    {
        if (_currentPanel == null) return;
        GamePopupMgr.OpenLoadingPopup(true);
        bool isSave = _currentPanel.ChangePage(index);
        if (isSave)
            QuestServerConnector.CompleteTutorial(_currentController.Condition, index, false,
                GamePopupMgr.CloseLoadingPopup);
        else GamePopupMgr.CloseLoadingPopup();
        ReleasePanel();
    }

    public void NextPage()
    {
        if (_currentPanel == null) return;
        GamePopupMgr.OpenLoadingPopup(true);
        bool isSave = _currentPanel.NextPage();
        if (isSave)
            QuestServerConnector.CompleteTutorial(_currentController.Condition, _currentPanel.TutorialIndex, false,
                GamePopupMgr.CloseLoadingPopup);
        else GamePopupMgr.CloseLoadingPopup();
        ReleasePanel();
    }

    public void ForceClosePage()
    {
        if (_currentPanel == null) return;
        _currentController.InvokeFailedEvent();
        Destroy(_currentPanel.gameObject);
        _currentPanel = null;
        _isTutorialActive = false;
    }

    private void ReleasePanel()
    {
        if (_currentPanel == null) return;
        if (!_currentPanel.IsComplete) return;
        _currentController.InvokeCompleteEvent();
        Destroy(_currentPanel.gameObject);
        _currentPanel = null;
        _isTutorialActive = false;
    }
}
