﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

using UnityEngine.Events;

public abstract class GameSkillBase : EightReceiver
{
    private static GameEffectMgr _effectMgr = null;
    protected static GameEffectMgr EffectMgr
    {
        get
        {
            if (_effectMgr == null)
            {
                if (EightUtil.IsLoadCompleteDevice == false)
                    _effectMgr = null;
                else
                    _effectMgr = EightUtil.GetCore<GameEffectMgr>();
            }

            return _effectMgr;
        }
    }

    [SerializeField, ReadOnly]
    private SkillType _skillType = SkillType.None;
    public SkillType GameSkillType { get => _skillType; protected set => _skillType = value; }

    [SerializeField, ReadOnly]
    private Vector3 _startPos = Vector3.zero;
    protected Vector3 StartPos { get => _startPos; set => _startPos = value; }

    [SerializeField, ReadOnly]
    private Vector3 _targetPos = Vector3.zero;
    protected Vector3 TargetPos { get => _targetPos; set => _targetPos = value; }

    [SerializeField, ReadOnly]
    private GameUnit _targetUnit = null;
    public GameUnit TargetUnit { get => _targetUnit; protected set => _targetUnit = value; }

    [SerializeField, ReadOnly]
    private float _value = -1.0f;
    public float Value { get { return _value; } protected set => _value = value; }

    protected UnityAction<GameSkillBase> _skillAction = null;
    public event UnityAction<GameSkillBase> SkillAction { add => _skillAction += value; remove => _skillAction -= value; }

    private Coroutine _processing = null;

    private bool _isComplateProcessing = true;
    public bool IsComplateProcessing => _isComplateProcessing;

    public abstract void Init();

    public void ProcessSkill(Vector3 startPos, float value, GameUnit targetUnit, GameSkillData data, UnityAction<GameSkillBase> skillAction = null)
    {
        if (_skillType != data.SkillType)
        {
            Debug.Log("Err SkillType");
            return;
        }

        ProcessingData(startPos, value, targetUnit, data, skillAction);
        SkillProcessing();
    }

    protected virtual void ProcessingData(Vector3 startPos, float value, GameUnit targetUnit, GameSkillData data, UnityAction<GameSkillBase> skillAction = null)
    {
        _targetPos = targetUnit.transform.position;
        _startPos = startPos;
        _targetUnit = targetUnit;
        _value = value;

        SkillAction += skillAction;
    }

    private void SkillProcessing()
    {
        if (EffectMgr == null)
            return;

        if (_processing != null)
            return;

        _isComplateProcessing = false;
        _processing = StartCoroutine(SkillRoutine());
    }

    protected void RoutineRelease()
    {
        if (_processing != null)
            StopCoroutine(_processing);

        _processing = null;
        _isComplateProcessing = true;
    }

    public virtual void ReleaseSkill()
    {
        RoutineRelease();

        _startPos = Vector3.zero;
        _targetPos = Vector3.zero;
        _skillAction = null;
        _targetUnit = null;
    }

    protected abstract IEnumerator SkillRoutine();
}
