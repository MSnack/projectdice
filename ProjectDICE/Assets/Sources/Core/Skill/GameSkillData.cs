﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public enum SkillType
{
    None,
    Instant,
    Bullet,
}

[System.Serializable]
public class AssetReferenceGameEffectData : AssetReferenceT<GameEffectData>
{
    public AssetReferenceGameEffectData(string guid) : base(guid) { }
}

public abstract class GameSkillData : ScriptableObject
{
    [SerializeField, ReadOnly]
    private SkillType _skillType = SkillType.None;
    public SkillType SkillType { get => _skillType; protected set { _skillType = value; } }

   // public abstract void Upload
}
