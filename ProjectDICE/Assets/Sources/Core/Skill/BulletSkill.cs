﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using EightWork;

public class BulletSkill : GameSkillBase
{
    [SerializeField, ReadOnly]
    private GameBulletSkill _skillData = null;

    public override void Init()
    {
        GameSkillType = SkillType.Bullet;
    }

    protected override void ProcessingData(Vector3 startPos, float value, GameUnit targetUnit, GameSkillData data, UnityAction<GameSkillBase> skillAction = null)
    {
        base.ProcessingData(startPos, value, targetUnit, data, skillAction);

        _skillData = data as GameBulletSkill;
    }

    protected override IEnumerator SkillRoutine()
    {
        var characaterEffect = EffectMgr.UseEffect(_skillData.BulletEffectData);
        characaterEffect.transform.position = StartPos;
        yield return null;

        var bulletEffect = EffectMgr.UseEffect(_skillData.BulletEffectData);
        bulletEffect.transform.position = StartPos;
        Vector3 bulletPos = bulletEffect.transform.position;
        Vector3 dir = (TargetPos - bulletPos).normalized;
        while (true)
        {
            float dist = (TargetPos - bulletPos).magnitude;
            if (dist <= _skillData.BulletSpeed * Time.fixedDeltaTime)
            {
                bulletPos = TargetPos;
                bulletEffect.transform.position = bulletPos;
                break;
            }
            else
            {
                bulletPos += dir * _skillData.BulletSpeed * Time.fixedDeltaTime;
                bulletEffect.transform.position = bulletPos;
            }

            yield return new WaitForFixedUpdate();
        }
        yield return null;
        EffectMgr.ReturnEffect(bulletEffect);

        _skillAction?.Invoke(this);
        _skillAction = null;

        var hitEffect = EffectMgr.UseEffect(_skillData.HitEffectData);
        hitEffect.transform.position = TargetPos;
        yield return null;

        RoutineRelease();
    }

    public override void ReleaseSkill()
    {
        base.ReleaseSkill();

        _skillData = null;
    }

    private void Reset()
    {
        GameSkillType = SkillType.Instant;
    }
}
