﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSkillActionReferencePool
{
    private List<GameSkillAction> _pool = new List<GameSkillAction>();
    public int ReferenceCount => _pool.Count;

    public void Register(GameSkillAction skillAction)
    {
        if (skillAction == null)
        {
            Debug.LogError("SkillAction is Null");
            return;
        }

        skillAction.ReleaseEvent += ReleaseEvent;
        _pool.Add(skillAction);
    }

    private void ReleaseEvent(GameSkillAction skillAction)
    {
        _pool.Remove(skillAction);
    }
}
