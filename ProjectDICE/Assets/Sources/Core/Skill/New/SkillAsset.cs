﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

[CreateAssetMenu(fileName = "New SkillAsset", menuName = "SkillAsset", order = 3)]
public class SkillAsset : ScriptableObject
{
    [SerializeReference]
    private List<SkillFrame> _skillFrames = null;
    public SkillFrame[] SkillFrames => _skillFrames.ToArray();
}