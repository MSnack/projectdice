﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Stock", menuName = "BuffAsset/Stock", order = 1)]
public class Stock : LifeBuffAsset
{
    [SerializeField]
    private BuffAsset _asset = null;
    public BuffAsset Asset => _asset;

    [SerializeField]
    private StatData _percent = new StatData();
    public StatData Percent => _percent;

    private void OnEnable()
    {
        BuffType = BuffType.Stock;
    }
}
