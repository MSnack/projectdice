﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Death", menuName = "BuffAsset/Death", order = 1)]
public class Death : BuffAsset
{
    private void OnEnable()
    {
        BuffType = BuffType.Death;
    }
}
