﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New ElementAmp", menuName = "BuffAsset/ElementAmp", order = 1)]
public class ElementAMP : LifeBuffAsset
{
    [SerializeField]
    private StatData _burn = new StatData();

    [SerializeField]
    private StatData _frozen = new StatData();

    [SerializeField]
    private StatData _poison = new StatData();

    [SerializeField]
    private StatData _electrick = new StatData();

    public StatData Burn => _burn;
    public StatData Frozen => _frozen;
    public StatData Poison => _poison;
    public StatData Electrick => _electrick;

    private void OnEnable()
    {
        BuffType = BuffType.ElementAMP;
    }
}
