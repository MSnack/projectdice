﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New GoldCycle", menuName = "BuffAsset/GoldCycle", order = 1)]
public class GoldCycle : CycleBuffAsset
{
    [SerializeField]
    private float _percent;
    public float Percent => _percent;

    private void OnEnable()
    {
        BuffType = BuffType.GoldCycle;
    }
}
