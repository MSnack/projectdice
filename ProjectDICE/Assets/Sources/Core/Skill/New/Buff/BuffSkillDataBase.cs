﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EightWork;
using System;

public abstract class GameBuff
{
    private GameUnit _target = null;
    public GameUnit Target => _target;

    private GameUnit _caster = null;
    public GameUnit Caster => _caster;

    private BuffAsset _asset = null;
    public BuffAsset Asset => _asset;

    public GameEffectObject BuffEffect = null;

    private int _level = 0;
    public int Level { get => _level; set => _level = value; }

    private int _upgrade = 0;
    public int Upgrade { get => _upgrade; set => _upgrade = value; }

    private int _reinforce = 0;
    public int Reinforce { get => _reinforce; set => _reinforce = value; }

    public static GameBuff GetBuff(BuffType type)
    {
        switch (type)
        {
            case BuffType.Poison: return new PoisonBuff();
            case BuffType.Stun: return new StunBuff();
            case BuffType.Burn: return new BurnBuff();
            case BuffType.ElectricShock: return new ElectricShockBuff();
            case BuffType.Lock: return new LockBuff();
            case BuffType.Stone: return new StoneBuff();
            case BuffType.Frozen: return new FrozenBuff();
            case BuffType.Slow: return new SlowBuff();
            case BuffType.Stock: return new StockBuff();
            case BuffType.Death: return new DeathBuff();
            case BuffType.ElementAMP: return new ElementAMPBuff();
            case BuffType.Curse: return new CurseBuff();
            case BuffType.GoldCycle: return new GoldCycleBuff();
            case BuffType.Stat: return new StatusBuff();
            case BuffType.Faster: return new FasterBuff();
        }
        return null;
    }

    public virtual bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        _asset = asset;
        _target = target;
        _caster = caster;

        _level = caster.UnitLevel;
        _upgrade = caster.UnitStep;
        _reinforce = caster.UnitReinforce;

        if (target.IsDontBuff(asset.BuffType) == true)
            return false;

        GameBuff buff = null;
        switch (_asset.ApplyType)
        {
            case BuffAsset.BuffApplyType.Nest : return true;
            case BuffAsset.BuffApplyType.Unique: if (target.IsHaveBuff(_asset.BuffType)) { return false; } return true;
            case BuffAsset.BuffApplyType.UniqueTag: if (target.IsHaveBuff(_asset.BuffType, _asset.BuffTag)) { return false; } return true;
            case BuffAsset.BuffApplyType.better:
                buff = target.GetBuff(_asset.BuffType, _asset.BuffTag);
                if(buff != null)
                {
                    if (buff.Level > _level ||
                        buff.Upgrade > _upgrade ||
                        buff.Reinforce > _reinforce)
                    {
                        buff.Release();
                        return true;
                    }else
                    {
                        return false;
                    }
                }

                return true;
        }
        return true;
    }

    public virtual void Reset()
    {

    }

    public virtual void Release()
    {

    }
}

public abstract class RoutineBuff : GameBuff
{
    private Coroutine _buffRoutine = null;

    protected void OnRoutine()
    {
        if (_buffRoutine != null)
            return;

        if (Target == null || Target.gameObject.activeSelf == false)
            return;

        _buffRoutine = Target.StartCoroutine(BuffLogic());
    }

    protected abstract IEnumerator BuffLogic();

    public override void Release()
    {
        base.Release();

        if (_buffRoutine != null && Target != null)
            Target.StopCoroutine(_buffRoutine);
        _buffRoutine = null;
    }
}

public abstract class LifeBuff : RoutineBuff
{
    private float _LifeTime = 0.0f;

    protected override IEnumerator BuffLogic()
    {
        OnStart();

        var lifeAsset = (LifeBuffAsset)Asset;
        float lifeGage = 0.0f;
        while (true)
        {
            if (_LifeTime >= 0.0f)
            {
                lifeGage += Time.deltaTime;
                if (lifeGage >= _LifeTime)
                    break;
            }

            yield return null;
        }
        OnEnd();
    }

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        if (caster == null || target == null)
            return false;

        _LifeTime = ((LifeBuffAsset)asset).LifeTime.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);

        return true;
    }

    protected virtual void OnStart()
    {

    }

    protected virtual void OnEnd()
    {
        Target?.RemoveBuff(this);
    }

    public override void Release()
    {
        base.Release();
        OnEnd();
    }
}

public abstract class CycleBuff : RoutineBuff
{
    protected int Level = 0;
    protected int Reinforce = 0;
    protected int Step = 0;

    protected override IEnumerator BuffLogic()
    {
        OnStart();

        var cycleAsset = (CycleBuffAsset)Asset;

        float lifeTime = cycleAsset.LifeTime.GetStat(Level, Reinforce, Step);
        float CycleLife = cycleAsset.CycleTime.GetStat(Level, Reinforce, Step);

        float lifeGage = 0.0f;
        float cycleGage = 0.0f;
        while (true)
        {
            if (lifeTime >= 0.0f)
            {
                lifeGage += Time.fixedDeltaTime;
                if (lifeGage >= lifeTime)
                    break;
            }

            if (CycleLife > 0.0f)
            {
                cycleGage += Time.fixedDeltaTime;
                if (cycleGage >= CycleLife)
                {
                    OnUpdate();
                    cycleGage -= CycleLife;
                }
            }
            yield return new WaitForFixedUpdate();
        }
        OnEnd();
    }

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        Level = caster.UnitLevel;
        Reinforce = caster.UnitReinforce;
        Step = caster.UnitStep;

        return true;
    }

    protected virtual void OnUpdate()
    {

    }

    protected virtual void OnStart()
    {

    }

    protected virtual void OnEnd()
    {
        Target?.RemoveBuff(this);
    }
}

public class StunBuff : LifeBuff
{
    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        OnRoutine();
        return true;
    }

    protected override void OnStart()
    {
        base.OnStart();
        Target.IsDontMove = true;

    }

    protected override void OnEnd()
    {
        Target.IsDontMove = false;
        base.OnEnd();
    }
}

public class PoisonBuff : CycleBuff
{
    private int poisonDamage = 0;

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        poisonDamage = (int)((Poison)asset).Damage.GetStat(Level, Reinforce, caster.UnitStep);

        if (caster.IsHaveBuff(BuffType.ElementAMP) == true)
        {
            List<GameBuff> buffs = new List<GameBuff>();
            caster.GetBuff(BuffType.ElementAMP, buffs);

            foreach (var buff in buffs)
                poisonDamage = (int)(poisonDamage * ((ElementAMPBuff)buff).Poison());
        }

       
        OnRoutine();

        return true;
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();

        Target.OnSubDamage(poisonDamage, false);
    }
}

public class BurnBuff : CycleBuff
{
    private float _range = 0.0f;
    private int _damage = 0;
    private int _count = 0;

    private GameEffectData _effectData = null;

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        var burn = (Burn)asset;

        _range = burn.Range;
        _damage = (int)burn.Combat.GetStat(Level, Reinforce, Step);
        _count = burn.Count;

        if(caster.IsHaveBuff(BuffType.ElementAMP) == true)
        {
            List<GameBuff> buffs = new List<GameBuff>();
            caster.GetBuff(BuffType.ElementAMP, buffs);

            foreach (var buff in buffs)
                _damage = (int)(_damage * ((ElementAMPBuff)buff).Burn());
        }

        _effectData = burn.BoolEffect;

        OnRoutine();
        return true;
    }

    protected override void OnStart()
    {
        base.OnStart();
    }

    protected override void OnEnd()
    {
        base.OnEnd();
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();
        int hitCount = 0;
        foreach (var unit in Target.TileRoadManager.TileUnitListAtLive)
        {
            if (unit == Target)
                continue;

            float dx = unit.transform.position.x - Target.transform.position.x;
            float dy = unit.transform.position.y - Target.transform.position.y;

            float dist = (float)Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2));
            if (dist <= _range)
            {
                unit.OnSubDamage(_damage, false);
                ++hitCount;
            }

            if (hitCount >= _count)
                break;
        }

        var useEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_effectData);
        useEffect.transform.position = Target.transform.position;
        Target.OnSubDamage(_damage, false);
    }
}

public class ElectricShockBuff : GameBuff
{
    private int _count = 0;
    private int _electricShockDamage = 0;

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        var esAsset = (ElectricShock)Asset;
        _count = (int)esAsset.Count.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);
        if (_count == 0)
            return false;

        _electricShockDamage = (int)esAsset.Power.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);
        if (caster.IsHaveBuff(BuffType.ElementAMP) == true)
        {
            List<GameBuff> buffs = new List<GameBuff>();
            caster.GetBuff(BuffType.ElementAMP, buffs);

            foreach (var buff in buffs)
                _electricShockDamage = (int)(_electricShockDamage * ((ElementAMPBuff)buff).Electric());
        }

        Target.OnHitEvent += OnHitEvent;

        return true;
    }

    private void OnHitEvent()
    {
        var useEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(((ElectricShock)Asset).HitEffectData);
        if (useEffect != null)
        {
            Target.AddEffect(useEffect);

            useEffect.Clear();
            useEffect.ReStart();
        }

        Target.OnSubDamage(_electricShockDamage, false);
        if (_count > 0)
        {
            if (--_count == 0)
            {
                Target.OnHitEvent -= OnHitEvent;
            }
        }
    }

    public override void Release()
    {
        base.Release();

        Target.OnHitEvent -= OnHitEvent;
    }
}

public class FrozenBuff : LifeBuff
{
    private float _frozenPower = 0.0f;
    public float FrozenPower => _frozenPower;

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        _frozenPower = ((Frozen)asset).Power.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);

        if (caster.IsHaveBuff(BuffType.ElementAMP) == true)
        {
            List<GameBuff> buffs = new List<GameBuff>();
            caster.GetBuff(BuffType.ElementAMP, buffs);

            foreach (var buff in buffs)
                _frozenPower = (int)(_frozenPower * ((ElementAMPBuff)buff).Frozen());
        }

        OnRoutine();
        return true;
    }
}

public class SlowBuff : LifeBuff
{
    private float _slowPower = 0.0f;
    public float SlowPower => _slowPower;

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        _slowPower = ((Slow)asset).Power.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);

        OnRoutine();

        return true;
    }
}

public class LockBuff : LifeBuff
{
    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        OnRoutine();

        return true;
    }

    protected override void OnStart()
    {
        base.OnStart();
        Target.IsDontMove = true;

    }

    protected override void OnEnd()
    {
        Target.IsDontMove = false;
        base.OnEnd();
    }
}

public class StoneBuff : LifeBuff
{
    private int _stoneHp = 0;

    private bool _subHit = false;
    private int _subDamage = 0;

    private bool _multi = false;
    private int _range = 0;

    private GameEffectData _stoneHit = null;

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        if (target.Hp <= 0)
            return false;

        Stone stone = (Stone)asset;

        _stoneHp = (int)stone.Hp.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);
        Target.OnDamageEvent += OnDamageEvent;

        _subHit = stone.IsSubHit;
        _subDamage = (int)stone.Damage.GetStat(caster.UnitLevel,caster.UnitReinforce,caster.UnitStep);
        _multi = stone.IsMulti;
        _range = stone.Range;
        _stoneHit = stone.StoneHit;

        // target.IsNoDamage = true;
        Target.IsDontMove = true;
        return true;
    }

    private void OnDamageEvent(int damage)
    {
        _stoneHp -= damage;
        if (_stoneHp <= 0)
        {
            if(_subHit)
            {
                if (_multi)
                {
                    var unitList = Target.TileRoadManager.TileUnitListAtLive;
                    for (int i = 0; i < unitList.Length; ++i)
                    {
                        if (CollisionCheck(unitList[i].transform.position, Target.transform.position) == true)
                            unitList[i].OnNoEventDamage(_subDamage, false);
                    }
                }
                else
                    Target.OnNoEventDamage(_subDamage, false);
            }
            var useEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_stoneHit);
            Target.AddEffect(useEffect);
            Target.IsNoDamage = false;
            Target.IsDontMove = false;

            Target.OnDamageEvent -= OnDamageEvent;
            Target?.RemoveBuff(this);
        }
    }

    private bool CollisionCheck(Vector3 pos1, Vector3 pos2)
    {
        float dx = pos1.x - pos2.x;
        float dy = pos1.y - pos2.y;

        float dist = (float)Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2));
        if (dist <= _range)
            return true;

        return false;
    }

    public override void Release()
    {
        Target.IsNoDamage = false;
        Target.IsDontMove = false;
        base.Release();
    }
}

public class DeathBuff : GameBuff
{
    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        if (target.Immune == null || target.Immune.Death == true)
            return false;

        target.OnDamage(target.Hp, false);

        Release();
        return true;
    }
}

public class ElementAMPBuff : LifeBuff
{
    private float _poison = 0.0f;
    public float Poison() {UpdateStat(); return _poison; }

    private float _burn = 0.0f;
    public float Burn() { UpdateStat(); return _burn; }

    private float _frozen = 0.0f;
    public float Frozen() { UpdateStat(); return _frozen; }

    private float _electrick = 0.0f;
    public float Electric() { UpdateStat(); return _electrick; }

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        if (caster == null || target == null)
            return false;

        OnRoutine();

        return true;
    }

    private void UpdateStat()
    {
        ElementAMP elementAmp = (ElementAMP)Asset;
        _poison = elementAmp.Poison.GetStat(Caster.UnitLevel, Caster.UnitReinforce, Caster.UnitStep);
        _burn = elementAmp.Burn.GetStat(Caster.UnitLevel, Caster.UnitReinforce, Caster.UnitStep);
        _electrick = elementAmp.Electrick.GetStat(Caster.UnitLevel, Caster.UnitReinforce, Caster.UnitStep);
        _frozen = elementAmp.Frozen.GetStat(Caster.UnitLevel, Caster.UnitReinforce, Caster.UnitStep);
    }
}

public class StockBuff : LifeBuff
{
    private BuffAsset _asset = null;
    private float _percent = 0.0f;

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        if (caster == null || target == null)
            return false;

        _asset = ((Stock)asset).Asset;
        _percent = ((Stock)asset).Percent.GetStat(caster.UnitLevel,caster.UnitReinforce,caster.UnitStep);

        OnRoutine();

        return true;
    }

    protected override void OnStart()
    {
        base.OnStart();
        Caster.OnReleaseEvent += Release;
        Target.OnGiveDamageEvent += OnStock;
    }

    protected override void OnEnd()
    {
        base.OnEnd();
        Caster.OnReleaseEvent -= Release;
        Target.OnGiveDamageEvent -= OnStock;
    }

    private void Release(GameUnit unit)
    {
        base.Release();
    }

    private void OnStock(GameUnit caster, GameUnit target, int damage)
    {
        float rand = caster.UnitRandomController.GetNextValue(RandomValueType.StockBuff);
        //float rand = 1.0f;
        if (rand <= _percent)
        {
            GameBuff buff = GetBuff(_asset.BuffType);
            if (buff.OnInitBuff(_asset, Caster, target) == true)
                target.OnBuff(buff);
           
        }
    }
}

public class CurseBuff : LifeBuff
{
    private float _percent = 0.0f;

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        if (caster == null || target == null)
            return false;

        _percent = ((Curse)asset).Percent.GetStat(caster.UnitLevel,caster.UnitReinforce,caster.UnitStep);

        OnRoutine();

        return true;
    }

    protected override void OnStart()
    {
        base.OnStart();
        Target.OnDamageEvent += OnDamage;
        Caster.OnReleaseEvent += Release;
    }

    protected override void OnEnd()
    {
        base.OnEnd();
        Target.OnDamageEvent -= OnDamage;
        Caster.OnReleaseEvent -= Release;
    }

    private void Release(GameUnit unit)
    {
        base.Release();
    }

    private void OnDamage(int damage)
    {
        float random = Caster.UnitRandomController.GetNextValue(RandomValueType.CurseBuff);
        //float random = 1.0f;
        if (random <= _percent)
        {
            var useEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(((Curse)Asset).HitEffectData);
            if (useEffect != null)
            {
                Target.AddEffect(useEffect);

                useEffect.Clear();
                useEffect.ReStart();
            }
            Target.OnNoEventDamage(damage*5, false);
        }
    }
}

public class GoldCycleBuff : CycleBuff
{
    private float _gold = 0.0f;
    private float _range = 0.0f;

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        _gold = caster.Sp;
        _range = _gold * ((GoldCycle)asset).Percent;

        OnRoutine();

        return true;
    }

    protected override void OnStart()
    {
        base.OnStart();
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();
        _gold -= _range;

        if (_gold < 0)
            _gold = 0;

        //Target.Sp = (int)_gold;
    }

    protected override void OnEnd()
    {
        base.OnEnd();
    }
}

public class StatusBuff : LifeBuff
{
    private List<GameEffectObject> _statEffectList = new List<GameEffectObject>(); 

    private StatBuff _combatBuff = null;
    private StatBuff _speedBuff = null;
    private StatBuff _percentBuff = null;
    private StatBuff _criticalBuff = null;

    private bool _isPercent = false;

    

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        var buff = (Stat)asset;

        foreach (var effectData in buff.StatEffectDataList)
        {
            var useEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(effectData);
            useEffect.Clear();
            useEffect.ReStart();
            _statEffectList.Add(useEffect);
        }

        int level = caster.UnitLevel;
        int reinforce = caster.UnitReinforce;
        int step = caster.UnitStep;

        float combat = buff.Combet.GetStat(level, reinforce, step);
        float speed = 1 / buff.Speed.GetStat(level, reinforce, step);
        float percent= buff.Percent.GetStat(level, reinforce, step);
        float critical = buff.Critical.GetStat(level, reinforce, step);
        _isPercent = buff.IsPercent;


        if (combat != 0.0f)
            _combatBuff = new StatBuff(combat, _isPercent);

        if (speed != 0.0f)
            _speedBuff = new StatBuff(speed, _isPercent);

        if (percent != 0.0f)
            _percentBuff = new StatBuff(percent, _isPercent);

        if (critical != 0.0f)
            _criticalBuff = new StatBuff(critical, _isPercent);

        OnRoutine();
        
        return true;
    }

    protected override void OnStart()
    {
        base.OnStart();

        if (_combatBuff != null && Target.CombatStat != null)
            Target.CombatStat.AddStatBuff(_combatBuff);

        if (_speedBuff != null && Target.AttackSpeedStat != null)
            Target.AttackSpeedStat.AddStatBuff(_speedBuff);

        if (_percentBuff != null && Target.CriticalPercentStat != null)
            Target.CriticalPercentStat.AddStatBuff(_percentBuff);

        if (_criticalBuff != null && Target.CriticalCombatStat != null)
            Target.CriticalCombatStat.AddStatBuff(_criticalBuff);

        foreach (var effect in _statEffectList)
            Target.AddEffect(effect);
    }

    protected override void OnEnd()
    {
        base.OnEnd();

        if (_combatBuff != null && Target.CombatStat != null)
            Target.CombatStat.RemoveStatBuff(_combatBuff);

        if (_speedBuff != null && Target.AttackSpeedStat != null)
            Target.AttackSpeedStat.RemoveStatBuff(_speedBuff);

        if (_percentBuff != null && Target.CriticalPercentStat != null)
            Target.CriticalPercentStat.RemoveStatBuff(_percentBuff);

        if (_criticalBuff != null && Target.CriticalCombatStat != null)
            Target.CriticalCombatStat.RemoveStatBuff(_criticalBuff);

        foreach (var effect in _statEffectList)
            EightUtil.GetCore<GameEffectMgr>().ReturnEffect(effect);
    }
}

public class FasterBuff : GameBuff
{
    private StatBuff _buff;
    private float _upgrade = 0.0f;
    private float _max = 0.0f;

    public override bool OnInitBuff(BuffAsset asset, GameUnit caster, GameUnit target)
    {
        if (!base.OnInitBuff(asset, caster, target))
            return false;

        Faster faster = (Faster)asset;

        _upgrade = faster.Up.GetStat(caster.UnitLevel,caster.UnitReinforce,caster.UnitStep);
        _max = faster.Max.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);

        _buff = new StatBuff(1.0f, true);

        Caster.OnGiveDamageEvent += Upgrade;
        Caster.AttackSpeedStat.AddStatBuff(_buff);
        return true;
    }

    public override void Release()
    {
        base.Release();
        Caster.OnGiveDamageEvent -= Upgrade;
        Caster.AttackSpeedStat.RemoveStatBuff(_buff);
    }

    private void Upgrade(GameUnit caster, GameUnit target, int damage)
    {
        float result = 1.0f;

        result = 1 / _buff.Stat + _upgrade;

        if (result > _max)
            result = _max;

        _buff.Stat = 1 / result;
    }
}