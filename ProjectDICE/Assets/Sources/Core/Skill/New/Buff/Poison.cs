﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Poison", menuName = "BuffAsset/Poison", order = 1)]
public class Poison : CycleBuffAsset
{
    [SerializeField]
    private StatData _damage = new StatData();
    public StatData Damage => _damage;

    private void OnEnable()
    {
        BuffType = BuffType.Poison;
    }
}
