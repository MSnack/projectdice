﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Burn", menuName = "BuffAsset/Burn", order = 1)]
public class Burn : CycleBuffAsset
{
    [SerializeField]
    private float _range = 0.0f;
    public float Range => _range;

    [SerializeField]
    private StatData _combat = new StatData();
    public StatData Combat => _combat;

    [SerializeField]
    private int _count = 0;
    public int Count => _count;

    [SerializeField]
    private GameEffectData _boomEffect = null;
    public GameEffectData BoolEffect => _boomEffect;

    private void OnEnable()
    {
        BuffType = BuffType.Burn;
    }
}
