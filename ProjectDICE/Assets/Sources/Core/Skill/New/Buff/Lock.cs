﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Lock", menuName = "BuffAsset/Lock", order = 1)]
public class Lock : LifeBuffAsset
{
    private void OnEnable()
    {
        BuffType = BuffType.Lock;
    }
}
