﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Slow", menuName = "BuffAsset/Slow", order = 1)]
public class Slow : LifeBuffAsset
{
    [SerializeField]
    private StatData _power = new StatData();
    public StatData Power => _power;

    private void OnEnable()
    {
        BuffType = BuffType.Slow;
    }
}