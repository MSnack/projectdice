﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Stone", menuName = "BuffAsset/Stone", order = 1)]
public class Stone : LifeBuffAsset
{
    [SerializeField]
    private GameEffectData _stoneHit = null;
    public GameEffectData StoneHit => _stoneHit;

    [SerializeField]
    private StatData _hp = new StatData();
    public StatData Hp => _hp;

    [SerializeField]
    private StatData _damage = new StatData();
    public StatData Damage => _damage;

    [SerializeField]
    private int _range = 0;
    public int Range => _range;

    [SerializeField]
    private bool _isSubHit = false;
    public bool IsSubHit => _isSubHit;

    [SerializeField]
    private bool _isMulti = false;
    public bool IsMulti => _isMulti;

    private void OnEnable()
    {
        BuffType = BuffType.Stone;
    }
}
