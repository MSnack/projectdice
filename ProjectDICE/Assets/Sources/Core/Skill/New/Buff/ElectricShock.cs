﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New ElectricShock", menuName = "BuffAsset/ElectricShock", order = 1)]
public class ElectricShock : CountBuffAsset
{
    [SerializeField]
    private StatData _power = new StatData();
    public StatData Power => _power;

    [SerializeField]
    private GameEffectData _hitEffectData = null;
    public GameEffectData HitEffectData => _hitEffectData;

    private void OnEnable()
    {
        BuffType = BuffType.ElectricShock;
    }
}
