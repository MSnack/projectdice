﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Stat", menuName = "BuffAsset/Stat", order = 1)]
public class Stat : LifeBuffAsset
{
    [SerializeField]
    private List<GameEffectData> _statEffectDataList = new List<GameEffectData>();
    public List<GameEffectData> StatEffectDataList => _statEffectDataList;

    [SerializeField]
    private StatData _combat = new StatData();
    public StatData Combet => _combat;

    [SerializeField]
    private StatData _speed = new StatData();
    public StatData Speed => _speed;

    [SerializeField]
    private StatData _percent = new StatData();
    public StatData Percent => _percent;

    [SerializeField]
    private StatData _critical = new StatData();
    public StatData Critical => _critical;

    [SerializeField]
    private bool _isPercent = false;
    public bool IsPercent => _isPercent;


    private void OnEnable()
    {
        BuffType = BuffType.Stat;
    }
}
