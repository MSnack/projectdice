﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Faster", menuName = "BuffAsset/Faster", order = 1)]
public class Faster : BuffAsset
{
    [SerializeField]
    private StatData _up = new StatData();
    public StatData Up => _up;

    [SerializeField]
    private StatData _max = new StatData();
    public StatData Max => _max;

    private void OnEnable()
    {
        BuffType = BuffType.Faster;
    }
}
