﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Curse", menuName = "BuffAsset/Curse", order = 1)]
public class Curse : LifeBuffAsset
{
    [SerializeField]
    private StatData _percent = new StatData();
    public StatData Percent => _percent;

    [SerializeField]
    private GameEffectData _hitEffect = null;
    public GameEffectData HitEffectData => _hitEffect;

    private void OnEnable()
    {
        BuffType = BuffType.Curse;
    }
}
