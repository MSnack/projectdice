﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Frozen", menuName = "BuffAsset/Frozen", order = 1)]
public class Frozen : LifeBuffAsset
{
    [SerializeField]
    private StatData _power = new StatData();
    public StatData Power =>_power;

    private void OnEnable()
    {
        BuffType = BuffType.Frozen;
    }
}
