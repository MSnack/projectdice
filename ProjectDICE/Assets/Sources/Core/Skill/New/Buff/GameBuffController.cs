﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EightWork;

public class GameBuffController : MonoBehaviour
{
    private Dictionary<BuffType, List<GameBuff>> _buffPool = new Dictionary<BuffType, List<GameBuff>>();

    public event UnityAction<BuffType, int> UpdateBuffEvent = null;

    public int BuffCount(BuffType type)
    {
        if (_buffPool.ContainsKey(type) == false)
            return 0;

        return _buffPool[type].Count;
    }

    public void RegisterBuff(BuffType type, GameBuff buff)
    {
        if (_buffPool.ContainsKey(type) == false)
            _buffPool[type] = new List<GameBuff>();


    }
}
