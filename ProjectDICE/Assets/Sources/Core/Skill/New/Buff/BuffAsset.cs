﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public enum BuffType
{
    None,
    Stun,  //대상을 일정 시간 동안 기절 시킨다.
    Poison, //대상에게 일정 시간 동안 지정된 틱 마다 피해를 입힌다.
    Burn, //대상에게 일정 시간 동안 지정된 틱 마다 범위피해를 입힌다.
    ElectricShock, //대상에게 지정된 카운트 만큼 추가 피해를 입힌다.
    Frozen, //대상의 이동속도를 일정 시간 동안 감소 시킨다.
    Lock, //대상의 움직임을 일정 시간 동안 봉쇄 한다.
    Stone, //대상을 체력을 가진 돌로 만든다. 돌의 체력이 모두 깎일시 대상에게 피해를 입힌다.
    Slow, //대상을 일정 시간 동안 느려지게 만든다.
    Combat, //대상의 공격력을 일정 시간 증.감소 시킨다.
    Speed, //대상의 공격속도를 일정 시간 증.감소 시킨다.
    CriticalPercent, // 대상의 크리티컬 확률을 일정 시간 동안 증.감소 시킨다.
    CriticalCombat, //대상의 클리티컬 공격력을 일정 시간 동안 증.감소 시킨다. 
    Stock, //대상의 일반 공격에 특수한 기능을 일정 시간 동안 심는다.
    Death, //대상을 즉사 시킨다.
    ElementAMP, //대상의 속성 능력을 증폭 시킨다.
    Curse, //대상이 피해를 입을시 일정 확률로 두배의 피해를 입는다.
    GoldCycle, //시간이 지남에 따라, 대상을 처치시 얻을수 있는 골드를 증,감소 시킨다.
    Stat,
    Faster,
}

public abstract class BuffAsset : ScriptableObject
{
    [SerializeField, ReadOnly]
    private BuffType _buffType = BuffType.None;

    public enum BuffApplyType
    {
        Nest,
        Unique,
        UniqueTag,
        better,
    }

    [SerializeField]
    private BuffApplyType _applyType;
    public BuffApplyType ApplyType => _applyType;

    [SerializeField]
    private GameEffectData _effectData = null;
    public GameEffectData EffectData => _effectData;

    [SerializeField]
    private bool _isFixedOrder = false;
    public bool IsFixedOrder => _isFixedOrder;

    [SerializeField]
    private int _buffTag = -1;

    public BuffType BuffType { get => _buffType; protected set => _buffType = value; }
    public int BuffTag => _buffTag;
}

public abstract class LifeBuffAsset : BuffAsset
{
    [SerializeField]
    private StatData _lifeTime = new StatData();
    public StatData LifeTime => _lifeTime;
}

public abstract class CycleBuffAsset : LifeBuffAsset
{
    [SerializeField]
    private StatData _cycleTime = new StatData();
    public StatData CycleTime => _cycleTime;
}

public abstract class CountBuffAsset : BuffAsset
{
    [SerializeField]
    private StatData _count = new StatData();
    public StatData Count => _count;
}

public abstract class LifeCountBuffAsset : LifeBuffAsset
{
    [SerializeField]
    private StatData _count = new StatData();
    public StatData Count => _count;
}

public abstract class ComplexBuffAsset : CycleBuffAsset
{
    [SerializeField]
    private StatData _count = new StatData();
    public StatData Count => _count;
}