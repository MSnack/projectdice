﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Stun", menuName = "BuffAsset/Stun", order = 1)]
public class Stun : LifeBuffAsset
{
    private void OnEnable()
    {
        BuffType = BuffType.Stun;
    }
}
