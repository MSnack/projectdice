﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.Events;

public class GameSkillAction : EightReceiver
{
    private GameUnit _caster = null;
    private GameUnit _target = null;

    public Vector3 SavePos = Vector3.zero;

    private Coroutine _skillProcessing = null;

    [SerializeField, ReadOnly]
    private bool _isProcessing = false;
    public bool IsProcessing => _isProcessing;

    [SerializeField, ReadOnly]
    private bool _isPause = false;
    public bool IsPause { get => _isPause; set => _isPause = value; }

    [SerializeField, ReadOnly]
    private SkillAsset _skillAsset = null;
    public SkillAsset SkillData => _skillAsset;

    public event UnityAction<GameSkillAction> ReleaseEvent = null;

    public void ProcessSkill(SkillAsset skillAsset, GameUnit caster, GameUnit target)
    {
        _caster = caster;
        _caster.RegisterSkillAction(RegisterSkillType.Caster, this);
        //_caster.OnReleaseEvent += (unit) =>
        //{
        //    _caster = null;
        //};

        _target = target;
        _target?.RegisterSkillAction(RegisterSkillType.Target, this);
        //_target.OnReleaseEvent += (unit) =>
        //{
        //    _target = null;
        //};

        _skillAsset = skillAsset;
        if (_skillAsset == null)
            return;

        if (_caster.BattleManager != null &&
            _caster.BattleManager.MsgInterface != null &&
            _caster.BattleManager.MsgInterface.LogSystem != null)
        {
            _caster.BattleManager.MsgInterface.LogSystem.SendLog(BattleLogSystem.BattleLogType.SkillEvent, skillAsset.name);
        }

        StartProcessing(skillAsset);
    }

    private void StartProcessing(SkillAsset skillAsset)
    {
        if (IsProcessing == true)
            return;

        _skillProcessing = StartCoroutine(SkillRoutine(skillAsset.SkillFrames));
        _isProcessing = true;
    }

    private IEnumerator SkillRoutine(SkillFrame[] skillFrames)
    {
        foreach (var skillFrame in skillFrames)
        {
            yield return null;
            skillFrame.OnSkillAction(this, _caster, _target);
            if (skillFrame.WaitTime > 0.0f)
                yield return new WaitForSeconds(skillFrame.WaitTime);

            if (IsPause == true)
                yield return new WaitUntil(() => IsPause == false);

            //yield return new WaitUntil(skillFrame.IsClearAction);
        }

        yield return null;
        StopProcessing();
    }

    public void StopProcessing()
    {
        if (IsProcessing == false)
            return;

        //if (_skillProcessing != null)
        StopCoroutine(_skillProcessing);

        _skillProcessing = null;
        _isProcessing = false;
    }

    public void ReleaseSkill()
    {
        StopProcessing();

        _caster = null;
        _target = null;
        _skillAsset = null;
        _isPause = false;

        ReleaseEvent?.Invoke(this);
        ReleaseEvent = null;
    }
}
