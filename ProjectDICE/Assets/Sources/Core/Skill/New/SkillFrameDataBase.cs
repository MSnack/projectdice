﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public enum SkillActionType
{
    LookAtTarget, //타겟을 바라봄
    EffectAction, //이펙트를 생성함
    WaitTime, //딜레이를 준다
    OnSkill, //State를 Skill로 전이 시킨다.
    OnDamage, //일반공격을 한다 
    OnDamageOfType, //타입에 따른 피헤를 입힌다.
    OnRandomDamage, //정해진 범위안의 랜덤값으로 피해를 입힌다.
    GoldPerDamageAction, //플레이어가 보유한 골드에 비례하여 피해를 입힌다.
    BulletAction, //총알이 날아가서 피해를 입힌다.
    RandomTarget, //기본 랜덤 타겟의 확장 버전으로, 대상의 수를 지정할 수 있다.
    MultiTargetAction, //해당 위치에 지정한 반경에 들어온, 상대를 타겟으로 하위 액션을 실행 한다.
    TrapAction, //해당 위치에 함정을 설치 한다.
    FloorAction, //해당 위치에 반복적인 범위 액션을 실행한다.
    BuffAction, //대상에게 버프를 시전 한다.
    RemoveBuff, //버프를 삭제 한다.
    DontBuffAction,//대상이 면역할 버프를 지정한다.
    AdjacencyAction, //자신에 인접한 유닛들을 타겟으로 두어 하위 액션을 실행한다.
    MergeUpgradeAction, //합성을 통해, 높은 등급의 캐릭터를 소환 한다.
    CreateAction, //새로운 캐릭터를 소환 한다.
    SwapAction, //같은 등급의 캐릭터와 위치를 교환 한다.
    CopyAction, // 같은 등급의 다른 캐릭터를 복사한다.
    RoadAction, //길에 존재 하는 모든 몬스터를 타겟으로 반복 액션을 실행 한다. 
    WalkerAction, //길을 따라 이동 하며, 해당 위치에 반복 액션을 실행 한다.
    RushAction, //지정된 칸수 만큼 빠르게 이동 한다.
    BlockAction, //해당 타일 위치에, 블럭을 설치하여 적이 이동하지 못하게 한다.
    SpawnAction, //지정된 위치에, 입력한 ID의 몬스터를 소환 한다.
    CollisionAction, //타겟과 지정된 ID의 충돌 여부를 판단하여 하위 스킬을 실행 한다.
    LifeAction, //플레이어의 체력을 올리거나 내린다.
    HpAction, //Hp와 관련된 세부적인 기능을 담당
    HpCondition, //Hp의 값을 체크하여 하위 액션을 실행 한다.
    SpineAction, //타겟의 스파인을 바꾼다.
    SpineCondition, //타겟의 스파인을 체크하여, 하위 액션을 실행 한다.
    TimmerAction, //입력된 시간 동안 지정된 사이클 마다 하위 액션을 실행 한다.
    PercentageAction, //지정된 확률에 따라 하위 액션을 실행한다.
    CountingAction, //지정된 카운트가 도달했을때 하위 액션을 실행한다
    MergeCreateAction,
}

public enum MonsterValueType
{
    Hp,
    Speed,
}

[System.Serializable]
public abstract class SkillFrame
{
    [SerializeField]
    private float _waitTime = 0.0f;
    public float WaitTime => _waitTime;

    private bool _isWaitingClaer = false;
    public bool IsWatingClear => _isWaitingClaer;

    public virtual void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        _isWaitingClaer = false;
        skillAction.StartCoroutine(WaitTimeCheck());
    }

    private IEnumerator WaitTimeCheck()
    {
        if (_waitTime > 0.0f)
            yield return new WaitForSeconds(_waitTime);

        _isWaitingClaer = true;
    }

    public static SkillFrame GetFrame(SkillActionType type)
    {
        switch (type)
        {
            case SkillActionType.WaitTime: return new WaitAction();
            case SkillActionType.LookAtTarget: return new LookAtTarget();
            case SkillActionType.BulletAction: return new BulletAction();
            case SkillActionType.OnDamage: return new DamageAction();
            case SkillActionType.PercentageAction: return new PercentageAction();
            case SkillActionType.TimmerAction: return new TimmerSkillAction();
            case SkillActionType.BuffAction: return new BuffAction();
            case SkillActionType.CountingAction: return new CountingAction();
            case SkillActionType.OnDamageOfType: return new DamageOfType();
            case SkillActionType.OnSkill: return new SkillAction();
            case SkillActionType.MultiTargetAction: return new MultiTarget();
            case SkillActionType.OnRandomDamage: return new RandomDamage();
            case SkillActionType.FloorAction: return new FloorAction();
            case SkillActionType.AdjacencyAction: return new AdjacencyAction();
            case SkillActionType.GoldPerDamageAction: return new GoldPerDamageAction();
            case SkillActionType.SwapAction: return new SwapAction();
            case SkillActionType.CreateAction: return new CreateAction();
            case SkillActionType.CopyAction: return new CopyAction();
            case SkillActionType.MergeUpgradeAction: return new MergeUpgrade();
            case SkillActionType.RoadAction: return new RoadAction();
            case SkillActionType.BlockAction: return new BlockAction();
            case SkillActionType.WalkerAction: return new WalkerAction();
            case SkillActionType.LifeAction: return new LifeAction();
            case SkillActionType.RushAction: return new RushAction();
            case SkillActionType.RandomTarget: return new RandomTarget();
            case SkillActionType.SpawnAction: return new SpawnAction();
            case SkillActionType.CollisionAction: return new CollsionAction();
            case SkillActionType.HpAction: return new HpAction();
            case SkillActionType.EffectAction: return new EffectAction();
            case SkillActionType.HpCondition: return new HpCondition();
            case SkillActionType.SpineAction: return new SpineAction();
            case SkillActionType.SpineCondition: return new SpineCondition();
            case SkillActionType.DontBuffAction: return new DontBuffAction();
            case SkillActionType.RemoveBuff: return new RemoveBuffAction();
            case SkillActionType.MergeCreateAction: return new MergeCreate();
        }

        return null;
    }

    public static SkillActionType[] GetUseTargetTypeList()
    {
        return System.Enum.GetValues(typeof(SkillActionType)) as SkillActionType[];
    }
}

public enum SortType
{
    Fixed,
    LocationBased,
}

public enum ProduceType
{
    None,
    Skill,
    Caster,
    Target,
    CasterParent,
    TargetParent,
}

public class SkillUtil
{
    public bool CollisionCheck(Vector3 pos1, Vector3 pos2, float range)
    {
        float dx = pos1.x - pos2.x;
        float dy = pos1.y - pos2.y;

        float dist = (float)Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2));
        if (dist <= range)
            return true;

        return false;
    }
}

[System.Serializable]
public class SkillFrameEffect
{
    [SerializeField]
    private GameEffectData _effectData = null;
    public GameEffectData EffectData => _effectData;

    [SerializeField]
    private ProduceType _producePosition = ProduceType.Caster;
    public ProduceType ProducePosition => _producePosition;

    [SerializeField]
    private SortType _sortType;
    public SortType SortType => _sortType;

    [SerializeField]
    private int _sortingOrder = 0;

    public GameEffectObject CreateEffect(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (target == null || caster == null)
            return null;

        var useEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_effectData);
        if (useEffect == null)
            return null;

        switch (_producePosition)
        {
            case ProduceType.None: break;
            case ProduceType.Skill: useEffect.transform.position = skillAction.SavePos; break;
            case ProduceType.Caster: useEffect.transform.position = caster.transform.position; break;
            case ProduceType.Target: useEffect.transform.position = target.transform.position; break;
            case ProduceType.CasterParent: caster.AddEffect(useEffect); break;
            case ProduceType.TargetParent: target.AddEffect(useEffect); break;
        }

        switch (_sortType)
        {
            case SortType.Fixed:
                useEffect.SortingOrder = _sortingOrder;
                break;
            case SortType.LocationBased:
                float xDir = 1.0f;
                if (caster.BattleManager != null &&
                    caster.BattleManager.ReceiverType == SocketDataType.Network)
                {
                    xDir = -1.0f;
                }

                int xLayer = (int)(useEffect.transform.position.x * xDir);
                int yLayer = (int)(useEffect.transform.position.y * -10.0f);
                useEffect.SortingOrder = 9001 + yLayer + xLayer + _sortingOrder;

                break;
        }

        useEffect.Clear();
        useEffect.ReStart();
        return useEffect;
    }
}

[System.Serializable]
public class WaitAction : SkillFrame
{
    [SerializeField]
    private SkillFrameEffect _skillEffect = null;
    public SkillFrameEffect SkillEffect => _skillEffect;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        base.OnSkillAction(skillAction, caster, target);

        SkillEffect.CreateEffect(skillAction, caster, target);
    }
}

[System.Serializable]
public class EffectAction : SkillFrame
{
    private enum Type
    {
        RoadAll,
        MySelf,
        Target,
        MySelf_Fllow,
        Target_Fllow,
    };

    [SerializeField]
    private Type _type;

    [SerializeField]
    private GameEffectData _effectData = null;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        base.OnSkillAction(skillAction, caster, target);

        GameEffectObject useEffect = null;

        switch (_type)
        {
            case Type.RoadAll:
                TileObjectBase[] tileArray = caster.TileRoadManager.TileRoadList;
                for (int i = 1; i < tileArray.Length - 1; ++i)
                {
                    useEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_effectData);
                    if (useEffect == null)
                        continue;

                    useEffect.transform.position = tileArray[i].GetPositionForUnit();

                    useEffect.Clear();
                    useEffect.ReStart();
                }

                break;
            case Type.MySelf:
                useEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_effectData);
                if (useEffect == null)
                    break;

                useEffect.transform.position = caster.transform.position;

                useEffect.Clear();
                useEffect.ReStart();
                break;
            case Type.Target:
                useEffect = EightUtil.GetCore<GameEffectMgr>().UseEffect(_effectData);
                if (useEffect == null)
                    break;

                useEffect.transform.position = target.transform.position;

                useEffect.Clear();
                useEffect.ReStart();
                break;
        }

    }
}

[System.Serializable]
public class FloorAction : SkillFrame
{
    private enum Condition { Cycle, Single, Multi, once }
    private enum Floor { Target, Random }

    [SerializeField]
    private SkillAsset _SkillAsset = null;

    [SerializeField]
    private SkillFrameEffect _effect = null;

    [SerializeField]
    private Condition _condition;

    [SerializeField]
    private float _range = 0.0f;

    [SerializeField]
    private float _cycle = 0.0f;

    [SerializeField]
    private int _spread = 0;

    [SerializeField]
    private Floor _floor;

    [SerializeField]
    private StatData _lifeTime = new StatData();

    [SerializeField]
    private int _count = 0;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        base.OnSkillAction(skillAction, caster, target);

        if (target.UnitState == GameUnitState.Die || caster.UnitState == GameUnitState.Die)
            return;

        int index = -1;

        switch (_floor)
        {
            case Floor.Target:
                index = target.CurrTileIndex;
                break;

            case Floor.Random:
                index = RandomFloor(caster);
                break;
        }

        List<GameEffectObject> useEffects = new List<GameEffectObject>();

        for (int i = -_spread; i <= _spread; ++i)
        {
            if (index + i < 0 || index + i >= caster.TileRoadManager.TileRoadList.Length)
                continue;

            skillAction.SavePos = caster.TileRoadManager.TileRoadList[index + i].GetPositionForUnit();
            useEffects.Add(_effect.CreateEffect(skillAction, caster, target));
        }

        if (useEffects.Count == 0)
            return;

        skillAction.IsPause = true;

        switch (_condition)
        {
            case Condition.Single:
                foreach (var ef in useEffects)
                    skillAction.StartCoroutine(Single(skillAction, caster, target, ef));
                break;

            case Condition.Multi:
                foreach (var ef in useEffects)
                    skillAction.StartCoroutine(Multi(skillAction, caster, target, ef));
                break;

            case Condition.Cycle:
                foreach (var ef in useEffects)
                    skillAction.StartCoroutine(Cycle(skillAction, caster, target, ef));
                break;
        }
    }

    private int RandomFloor(GameUnit caster)
    {
        var rand = caster.UnitRandomController.GetNextValueRange(RandomValueType.Floor, 1, caster.TileRoadManager.TileRoadList.Length - 2);
        return rand;

        //return 2;
    }


    private IEnumerator Single(GameSkillAction skillAction, GameUnit caster, GameUnit target, GameEffectObject effect)
    {
        float currTime = 0.0f;

        float lifeTime = _lifeTime.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);

        while (true)
        {
            if (currTime >= lifeTime)
                break;

            currTime += Time.fixedDeltaTime;

            if (caster == null || caster.UnitState == GameUnitState.Die)
                break;
            bool exit = false;


            SkillUtil skillUtil = new SkillUtil();
            GameUnit[] unitList = caster.TileRoadManager.TileUnitListAtLive;

            for (int i = 0; i < unitList.Length; ++i)
            {
                if (skillUtil.CollisionCheck(unitList[i].transform.position, effect.transform.position,_range) == true)
                {
                    EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_SkillAsset, caster, unitList[i]);
                    exit = true;
                    break;
                }
            }

            if (exit == true)
                break;

            yield return new WaitForFixedUpdate();
        }
        EightUtil.GetCore<GameEffectMgr>().ReturnEffect(effect);
        skillAction.IsPause = false;
    }

    private IEnumerator Multi(GameSkillAction skillAction, GameUnit caster, GameUnit target, GameEffectObject effect)
    {
        int level = caster.UnitLevel;
        int reinforce = caster.UnitReinforce;
        int step = caster.UnitStep;

        float lifeTime = _lifeTime.GetStat(level, reinforce, step);

        int count = 0;

        SkillUtil skillUtil = new SkillUtil();
        GameUnit[] unitList = caster.TileRoadManager.TileUnitListAtLive;
        for (int i = 0; i < unitList.Length; ++i)
        {
            if (skillUtil.CollisionCheck(unitList[i].transform.position, effect.transform.position,_range) == true)
            {
                EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_SkillAsset, caster, unitList[i]);
                ++count;
            }

            if (count >= _count)
                break;
        }

        float currTime = 0.0f;

        while (true)
        {
            if (currTime >= lifeTime)
                break;

            currTime += Time.fixedDeltaTime;

            if (caster == null || caster.UnitState == GameUnitState.Die)
                break;

            yield return new WaitForFixedUpdate();
        }

        EightUtil.GetCore<GameEffectMgr>().ReturnEffect(effect);
        skillAction.IsPause = false;
    }

    private IEnumerator Cycle(GameSkillAction skillAction, GameUnit caster, GameUnit target, GameEffectObject effect)
    {
        int level = caster.UnitLevel;
        int reinforce = caster.UnitReinforce;
        int step = caster.UnitStep;

        float lifeTime = _lifeTime.GetStat(level, reinforce, step);

        float currTime = 0.0f;
        float currCycle = 0.0f;
        while (true)
        {
            if (currTime >= lifeTime)
                break;

            currTime += Time.fixedDeltaTime;

            if (caster == null || caster.UnitState == GameUnitState.Die)
                break;

            int count = 0;

            if (currCycle >= _cycle)
            {
                SkillUtil skillUtil = new SkillUtil();
                GameUnit[] unitList = caster.TileRoadManager.TileUnitListAtLive;
                for (int i = 0; i < unitList.Length; ++i)
                {
                    if (skillUtil.CollisionCheck(unitList[i].transform.position, effect.transform.position,_range) == true)
                    {
                        EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_SkillAsset, caster, unitList[i]);
                        ++count;
                    }
                    if (count >= _count)
                        break;
                }

                currCycle = 0.0f;
            }

            currCycle += Time.fixedDeltaTime;


            yield return new WaitForFixedUpdate();
        }

        EightUtil.GetCore<GameEffectMgr>().ReturnEffect(effect);
        skillAction.IsPause = false;
    }
};

[System.Serializable]
public class LookAtTarget : SkillFrame
{
    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        base.OnSkillAction(skillAction, caster, target);

        var targetPos = target.transform.position;
        var casterPos = caster.transform.position;

        if (targetPos.x > casterPos.x)
            caster.SpineUnit.IsFlip = true;
        else if (targetPos.x < casterPos.x)
            caster.SpineUnit.IsFlip = false;
    }
}

[System.Serializable]
public class BulletAction : SkillFrame
{
    [SerializeField]
    private SkillFrameEffect _skillEffect = null;

    [SerializeField]
    private SkillFrameEffect _hitEffect = null;

    [SerializeField]
    private bool _isDirMatch = false;

    [SerializeField]
    private float _moveSpeed = 0.0f;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster.UnitState == GameUnitState.Die || target.UnitState == GameUnitState.Die)
            return;

        base.OnSkillAction(skillAction, caster, target);

        var useEffect = _skillEffect.CreateEffect(skillAction, caster, target);
        if (useEffect == null)
            return;

        skillAction.StartCoroutine(BulletLogic(skillAction, useEffect, caster, target));

        skillAction.IsPause = true;
    }
    private IEnumerator BulletLogic(GameSkillAction skillAction, GameEffectObject effect, GameUnit caster, GameUnit target)
    {
        Vector3 goalPos = target.transform.position;

        while (true)
        {
            Vector3 effectPos = effect.transform.position;
            Vector3 dir = (goalPos - effectPos).normalized;

            float angle = Mathf.Atan(dir.y / dir.x) * (180.0f / 3.14159f);

            if (_isDirMatch == true)
            {
                if (dir.x > 0)
                    effect.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 360.0f - angle);
                else
                    effect.transform.rotation = Quaternion.Euler(0, 0, angle);

            }

            float dist = (goalPos - effectPos).magnitude;
            float moveSpeed = _moveSpeed * Time.fixedDeltaTime;

            if (dist > moveSpeed)
                effectPos += dir * moveSpeed;
            else
            {
                effectPos += dir * moveSpeed;

                if (_hitEffect != null)
                {
                    var useEffect = _hitEffect.CreateEffect(skillAction, caster, target);

                    if (_isDirMatch == true)
                    {
                        if (dir.x > 0)
                            useEffect.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 360.0f - angle);
                        else
                            useEffect.transform.rotation = Quaternion.Euler(0, 0, angle);
                    }
                }

                break;
            }
            effect.transform.position = effectPos;

            yield return new WaitForFixedUpdate();
        }

        effect.transform.position = goalPos;
        EightUtil.GetCore<GameEffectMgr>().ReturnEffect(effect);


        skillAction.SavePos = goalPos;
        skillAction.IsPause = false;
    }
}

[System.Serializable]
public class DamageOfType : SkillFrame
{
    private enum DamageType
    {
        Static,
        Percent,
    }

    [SerializeField]
    private DamageType _type;

    [SerializeField]
    private StatData _damage = new StatData();

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        base.OnSkillAction(skillAction, caster, target);

        float combat = 0.0f;
        switch (_type)
        {
            case DamageType.Static:
                combat = _damage.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);
                break;

            case DamageType.Percent:
                combat = caster.Combat * _damage.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);
                break;
        }

        var random = caster.UnitRandomController.GetNextValue(RandomValueType.Damage);
        float range = caster.CriticalPercent;
        var isCritical = random < range;
        var criticalPer = isCritical == true ? caster.CriticalCombat : 1.0f;
        var damage = (int)(combat * criticalPer);

        caster.OnGiveDamage(target, damage);
        target.OnDamage(damage, isCritical);
    }
}

[System.Serializable]
public class DamageAction : SkillFrame
{
    [SerializeField]
    private SkillAsset _critical = null;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        base.OnSkillAction(skillAction, caster, target);

        var random = caster.UnitRandomController.GetNextValue(RandomValueType.Damage);
        float range = caster.CriticalPercent;
        var isCritical = random < range;
        var criticalPer = isCritical == true ? caster.CriticalCombat : 1.0f;
        int damage = (int)(caster.Combat * criticalPer);

        caster.OnGiveDamage(target, damage);
        target.OnDamage(damage, isCritical);

        if (isCritical == true)
        {
            if (_critical != null)
                EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_critical, caster, target);
        }
    }
}

[System.Serializable]
public class MultiTarget : SkillFrame
{
    private enum MultiType
    {
        Target,
        All,
    }

    [SerializeField]
    private MultiType _multiType;

    [SerializeField]
    private float _fRadius = 0.0f;

    [SerializeField]
    private SkillAsset _skillAsset = null;

    [SerializeField]
    private int _count = 0;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        base.OnSkillAction(skillAction, caster, target);
        if (_skillAsset == null)
            return;

        List<GameUnit> targetList = new List<GameUnit>();

        switch (_multiType)
        {
            case MultiType.Target:
                GetTargetlist(targetList, caster.TileRoadManager, target.transform.position);
                OnSkill(targetList.ToArray(), caster);
                break;

            case MultiType.All:
                OnSkill(caster.TileRoadManager.TileUnitListAtLive, caster);
                break;
        }
    }

    private void GetTargetlist(List<GameUnit> targetList, TileRoadMgr trm, Vector3 pos)
    {
        foreach (var unit in trm.TileUnitListAtLive)
        {
            float dx = pos.x - unit.transform.position.x;
            float dy = pos.y - unit.transform.position.y;

            float dist = (float)Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2));
            if (dist <= _fRadius)
                targetList.Add(unit);

            if (targetList.Count >= _count)
                return;

        }
    }

    private void OnSkill(GameUnit[] targetList, GameUnit caster)
    {
        foreach (var unit in targetList)
            EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_skillAsset, caster, unit);
    }
};

[System.Serializable]
public class PercentageAction : SkillFrame
{
    [SerializeField]
    private StatData _percent = new StatData();

    [SerializeField]
    private SkillAsset _successSkill = null;

    [SerializeField]
    private SkillAsset _failSkill = null;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        base.OnSkillAction(skillAction, caster, target);

        float percent = _percent.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);

        if (percent == 0.0f)
            return;

        var random = caster.UnitRandomController.GetNextValue(RandomValueType.Percent);
        //var random = 1.0f;

        if (random < percent)
            EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_successSkill, caster, target);
        else
        {
            if (_failSkill != null)
                EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_failSkill, caster, target);
        }
    }
}

[System.Serializable]
public class TimmerSkillAction : SkillFrame
{
    [SerializeField]
    private StatData _lifeTime = new StatData();

    [SerializeField]
    private StatData _cycleTime = new StatData();

    [SerializeField]
    private SkillAsset _skill = null;

    [SerializeField]
    private TargetType _targetType = TargetType.Null;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        base.OnSkillAction(skillAction, caster, target);

        target.OnTimmerSkill(_skill, _targetType, _lifeTime.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep), _cycleTime.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep));
    }
}

[System.Serializable]
public class BuffAction : SkillFrame
{
    [SerializeField]
    private BuffAsset _buffAsset = null;

    [SerializeField]
    private bool _self = false;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        if (_self)
            target = caster;

        base.OnSkillAction(skillAction, caster, target);

        var buff = GameBuff.GetBuff(_buffAsset.BuffType);
        if (buff == null)
            return;

        if (buff.OnInitBuff(_buffAsset, caster, target) == true)
            target.OnBuff(buff);
    }
}

[System.Serializable]
public class CountingAction : SkillFrame
{
    [SerializeField]
    private int _countOver = 0;

    [SerializeField]
    private SkillAsset _countOverSkill = null;

    [SerializeField]
    private SkillAsset _othersSkill = null;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        base.OnSkillAction(skillAction, caster, target);
        int count = caster.Count;

        if (count >= _countOver - 1)
        {
            if (_othersSkill != null)
                EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_countOverSkill, caster, target);

            caster.Count = 0;
        }
        else
        {
            if (_othersSkill != null)
                EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_othersSkill, caster, target);

            caster.Count += 1;
        }
    }
}

[System.Serializable]
public class RandomDamage : SkillFrame
{
    [SerializeField]
    private StatData _minCombat = new StatData();

    [SerializeField]
    private StatData _maxCombat = new StatData();

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        base.OnSkillAction(skillAction, caster, target);

        var random = caster.UnitRandomController.GetNextValue(RandomValueType.Damage);
        float range = caster.CriticalPercent;
        var isCritical = random < range;
        var criticalPer = isCritical == true ? caster.CriticalCombat : 1.0f;
        //int damage = (int)(caster.Combat * criticalPer);

        float diff = (int)_maxCombat.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitReinforce) - (int)_minCombat.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);

        float rand = caster.UnitRandomController.GetNextValue(RandomValueType.RandomDamage);
        //float rand = 1.0f;
        float addDamage = (int)((float)diff * rand);
        int damage = (int)((_minCombat.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep) + addDamage) * criticalPer);

        caster.OnGiveDamage(target, damage);
        target.OnDamage(damage, isCritical);
    }
};

[System.Serializable]
public class SkillAction : SkillFrame
{
    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        base.OnSkillAction(skillAction, caster, target);
        caster.IsSkillReady = true;
    }
}

[System.Serializable]
public class AdjacencyAction : SkillFrame
{
    private enum Type
    {
        Adjacency,
    };

    [SerializeField]
    private Type _type;

    [SerializeField]
    private float _lifeTime = -1;

    //private static int[,] _trians = {
    //    {-1,1,2}, {-1,-1,3}, {-1,3,6 }, {0,4,7},
    //    {1,5,8}, {-1,-1,9 }, {-1,7,-1 }, {2,8,10 },
    //    { 3,9,-1}, {4,-1,-1 }, { 6,-1,-1}, {9,-1,-1 } };

    //private static int[,] _inverts = {
    //    {-1,-1,3}, {-1,0,4}, {0,-1,7}, {1,2,8},
    //    {-1,4,9}, {-1,4,-1}, {2,-1,10}, {3,6,-1},
    //    {4,7,-1}, {5,8,11}, {7,-1,-1}, {-1,-1,-1} };

    [SerializeField]
    private List<BuffAsset> _buffList = new List<BuffAsset>();

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        if (caster == null || target == null)
            return;

        skillAction.StartCoroutine(AdjacencyLogic(skillAction, caster));
        skillAction.IsPause = true;
    }

    private IEnumerator AdjacencyLogic(GameSkillAction skillAction, GameUnit caster)
    {
        List<GameBuff> castList = new List<GameBuff>();

        float curTime = 0.0f;

        while (true)
        {
            if (caster.isActiveAndEnabled == false)
                break;

            if (curTime >= _lifeTime)
            {
                curTime = 0.0f;

                if (_lifeTime != -1)
                    break;
            }

            curTime += Time.fixedDeltaTime;

            foreach (var cast in castList) cast.Release();
            castList.Clear();

            switch (_type)
            {
                case Type.Adjacency: AdjacencyCheck(caster, castList); break;
                //case Type.Trian: TraianAction(caster, castList); break;
                //case Type.InvertTrian: InvertAction(caster, castList); break;
            }

            yield return new WaitForFixedUpdate();
        }

        foreach (var cast in castList) cast.Release();

        skillAction.IsPause = false;
    }

    private void AdjacencyCheck(GameUnit caster, List<GameBuff> castList)
    {
        var tileMap = caster.TileSlotManager.TilePool.TileMap;
        var tilePos = caster.TileSlotManager.TilePool.GetTilePos(caster.CurrTileIndex);

        var tileList = new List<UserTile>();
        for (int y = 0; y < tileMap.Length; ++y)
        {
            if (Mathf.Abs(tilePos.Y - y) > 1)
                continue;

            int lineWeight = y % 2;
            for (int x = 0; x < tileMap[y].Length; ++x)
            {
                if (Mathf.Abs(tilePos.X - x) > 1)
                    continue;

                var tile = tileMap[y][x];
                if(tile == null || tile.Owner == null)
                    continue;

                var unit = tile.Owner;
                foreach (var buffAsset in _buffList)
                {
                    var buff = GameBuff.GetBuff(buffAsset.BuffType);
                    if (buff == null)
                        return;

                    if (buff.OnInitBuff(buffAsset, caster, unit) == true)
                    {
                        unit.OnBuff(buff);
                        castList.Add(buff);
                    }
                }
            }
        }
    }

    //private void TraianAction(GameUnit caster, List<GameBuff> castList)
    //{

    //    for (int j = 0; j < 3; ++j)
    //    {
    //        int index = _trians[caster.CurrTileIndex, j];
    //        if (index == -1)
    //            continue;


    //        GameUnit unit = caster.TileSlotManager.GetTileUnit(index);
    //        if (unit == null)
    //            continue;

    //        foreach (var buffAsset in _buffList)
    //        {
    //            var buff = GameBuff.GetBuff(buffAsset.BuffType);
    //            if (buff == null)
    //                return;

    //            if (buff.OnInitBuff(buffAsset, caster, unit) == true)
    //            {
    //                unit.OnBuff(buff);
    //                castList.Add(buff);
    //            }
    //        }
    //    }
    //}

    //private void InvertAction(GameUnit caster, List<GameBuff> castList)
    //{
    //    for (int j = 0; j < 3; ++j)
    //    {
    //        int index = _inverts[caster.CurrTileIndex, j];
    //        if (index == -1)
    //            continue;


    //        GameUnit unit = caster.TileSlotManager.GetTileUnit(index);
    //        if (unit == null)
    //            continue;

    //        foreach (var buffAsset in _buffList)
    //        {
    //            var buff = GameBuff.GetBuff(buffAsset.BuffType);
    //            if (buff == null)
    //                return;

    //            if (buff.OnInitBuff(buffAsset, caster, unit) == true)
    //            {
    //                unit.OnBuff(buff);
    //                castList.Add(buff);
    //            }
    //        }
    //    }
    //}
}

[System.Serializable]
public class GoldPerDamageAction : SkillFrame
{
    [SerializeField]
    private StatData _percent = new StatData();
    public StatData Percent => _percent;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        if (caster == null || target == null)
            return;

        var random = caster.UnitRandomController.GetNextValue(RandomValueType.Damage);
        float range = caster.CriticalPercent;
        var isCritical = random < range;
        var criticalPer = isCritical == true ? caster.CriticalCombat : 1.0f;
        float combat = _percent.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep) * (float)caster.UserManager.Sp;
        var damage = (int)(combat * criticalPer);

        target.OnDamage(damage, isCritical);
    }
};

[System.Serializable]
public class SwapAction : SkillFrame
{
    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        var tempIndex = caster.CurrTileIndex;
        caster.CurrTileIndex = target.CurrTileIndex;
        target.CurrTileIndex = tempIndex;

        var tempPos = caster.transform.position;
        caster.transform.position = target.transform.position;
        target.transform.position = tempPos;

        var tile1 = caster.TileSlotManager.GetUseSlot(caster.CurrTileIndex);
        caster.TileSlotManager.SetSlotUnit(tile1, caster);

        var tile2 = target.TileSlotManager.GetUseSlot(target.CurrTileIndex);
        target.TileSlotManager.SetSlotUnit(tile2, target);
    }
}

[System.Serializable]
public class CreateAction : SkillFrame
{
    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        var battleManager = caster.BattleManager;
        var tileSlotMgr = battleManager.TileSlotMgr;

        var deckList = battleManager.UserManager.UserData.UnitList;
        int deckIndex = Random.Range(0, deckList.Length);
        int charId = deckList[deckIndex].UnitID;

        skillAction.StartCoroutine(WaitCreate(skillAction, charId, 0, battleManager));
    }

    private IEnumerator WaitCreate(GameSkillAction skillAction, int id, int grade, BattleManager battleManager)
    {
        yield return new WaitUntil(() => battleManager.TileSlotMgr.IsHaveEmptySlot == true);

        var slot = battleManager.TileSlotMgr.ReserveRandomSlot();
        if (slot == null)
            yield break;

        battleManager.MsgInterface.SpawnCharacter(id, grade, slot.slotIndex, battleManager);
        skillAction.IsPause = false;
    }
};

[System.Serializable]
public class CopyAction : SkillFrame
{
    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        int charId = target.UnitID;
        int step = caster.UnitStep;

        caster.BattleManager.MsgInterface.CopyUnit(caster, target, charId, step, caster.BattleManager);
    }
}

[System.Serializable]
public class MergeUpgrade : SkillFrame
{
    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        var battleManager = caster.BattleManager;

        var deckList = battleManager.UserManager.UserData.UnitList;
        int deckIndex = Random.Range(0, deckList.Length);
        int charId = deckList[deckIndex].UnitID;

        int step = caster.UnitStep;
        if (step < 6)
            step += 1;

        caster.BattleManager.MsgInterface.MergeUpgrade(caster, target, charId, step, battleManager);
    }
}

[System.Serializable]
public class MergeCreate : SkillFrame
{
    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        var battleManager = caster.BattleManager;
        var deckList = battleManager.UserManager.UserData.UnitList;
        int deckIndex = Random.Range(0, deckList.Length);
        int charId = deckList[deckIndex].UnitID;

        int step = caster.UnitStep;
        if (step < 6)
            step += 1;

        caster.BattleManager.MsgInterface.MergeCreate(caster, target, charId, step, battleManager);
    }
}

[System.Serializable]
public class RoadAction : SkillFrame
{
    [SerializeField]
    private GameEffectData _effetData = null;

    [SerializeField]
    private SkillAsset _skillAsset = null;

    [SerializeField]
    private bool _isAlready = false;

    [SerializeField]
    private StatData _lifeTime = new StatData();
    public StatData LifeTime => _lifeTime;

    [SerializeField]
    private float _cycleTime = 0.0f;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        TileRoadMgr tileRoadMgr = caster.TileRoadManager;

        if (!_isAlready)
            if (tileRoadMgr.IsAlreadyRoadEffectID(_effetData.DataID))
                return;

        var effect = tileRoadMgr.CreateRoadEffect(_effetData);
        skillAction.StartCoroutine(RoadLogic(skillAction, tileRoadMgr, effect, caster));

        skillAction.IsPause = true;
    }

    private IEnumerator RoadLogic(GameSkillAction skillAction, TileRoadMgr tileRoadMgr, GameEffectObject effect, GameUnit caster)
    {
        float lifeGage = 0.0f;
        float cycleGage = 0.0f;
        float lifeTime = _lifeTime.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);

        while (true)
        {
            if (lifeGage >= lifeTime)
                break;

            lifeGage += Time.fixedDeltaTime;

            if (cycleGage >= _cycleTime)
            {
                foreach (var unit in tileRoadMgr.TileUnitListAtLive)
                    EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_skillAsset, caster, unit);

                cycleGage = 0.0f;
            }

            cycleGage += Time.fixedDeltaTime;

            yield return new WaitForFixedUpdate();
        }

        skillAction.IsPause = false;
        tileRoadMgr.ReturnRoadEffect(effect);
    }
}

[System.Serializable]
public class BlockAction : SkillFrame
{
    [SerializeField]
    private SkillFrameEffect _skillEffect = null;

    [SerializeField]
    private SkillFrameEffect _endSkillEffect = null;

    [SerializeField]
    private SkillAsset _endSkillAsset = null;

    [SerializeField]
    private StatData _lifeTime = new StatData();

    [SerializeField]
    private float _range = 0.0f;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        int tileId = RandomFloor(caster);
        skillAction.SavePos = caster.TileRoadManager.TileRoadList[tileId].GetPositionForUnit();
        var useEffect = _skillEffect.CreateEffect(skillAction, caster, target);

        caster.TileRoadManager.CreateRoadTileEffect(tileId, useEffect);

        skillAction.StartCoroutine(BlockLogic(skillAction, useEffect, caster, tileId));

        skillAction.IsPause = true;

    }

    private int RandomFloor(GameUnit caster)
    {

        List<TileObjectBase> tileList = new List<TileObjectBase>(caster.TileRoadManager.TileRoadList);
        int rand = 0;
        while (tileList.Count > 1)
        {
            rand = caster.UnitRandomController.GetNextValueRange(RandomValueType.Block, 1, tileList.Count - 1);
            if (caster.TileRoadManager.IsAlreadyRoadTileEffectID(rand, _skillEffect.EffectData.DataID) == true)
            {
                tileList.Remove(tileList[rand]);
                continue;
            }

            break;
        }

        if(tileList.Count == 1)
            rand = caster.UnitRandomController.GetNextValueRange(RandomValueType.Block, 1, caster.TileRoadManager.TileRoadList.Length - 1);

        return rand;

        //return 2;
    }

    private IEnumerator BlockLogic(GameSkillAction skillAction, GameEffectObject effect, GameUnit caster, int tileId)
    {
        float lifeGage = 0.0f;
        float lifeTime = _lifeTime.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);

        List<GameUnit> listBlockUnit = new List<GameUnit>();

        while (true)
        {
            if (lifeGage >= lifeTime)
                break;

            lifeGage += Time.fixedDeltaTime;

            if (caster.TileRoadManager == null)
            {
                listBlockUnit.Clear();
                break;
            }
            SkillUtil skillUtil = new SkillUtil();

            foreach (var unit in caster.TileRoadManager.TileUnitListAtLive)
            {
                if (skillUtil.CollisionCheck(effect.transform.position, unit.transform.position,_range))
                {
                    if (listBlockUnit.Contains(unit))
                        continue;

                    unit.IsDontMove = true;

                    listBlockUnit.Add(unit);
                    unit.OnReleaseEvent += (gameUnit) => listBlockUnit.Remove(gameUnit);
                }
            }

            yield return new WaitForFixedUpdate();
        }

        caster.TileRoadManager.ReturnRoadTileEffect(tileId, effect);

        EightUtil.GetCore<GameEffectMgr>().ReturnEffect(effect);

        foreach (var unit in listBlockUnit)
        {
            unit.IsDontMove = false;
            EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_endSkillAsset, caster, unit);
        }

        var useEffect = _endSkillEffect.CreateEffect(skillAction, caster, caster);

        skillAction.IsPause = false;
    }
}

[System.Serializable]
public class WalkerAction : SkillFrame
{
    [SerializeField]
    private SkillAsset _skilAsset = null;

    [SerializeField]
    private SkillFrameEffect _effectData = null;

    [SerializeField]
    private bool _revers = false;

    [SerializeField]
    private float _speed = 0.0f;

    [SerializeField]
    private float _range = 0.0f;

    [SerializeField]
    private float _cycle = 0.0f;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        var useEffect = _effectData.CreateEffect(skillAction, caster, target);
        skillAction.StartCoroutine(WalkerLogic(skillAction, useEffect, caster.TileRoadManager, caster));

        int index = caster.TileRoadManager.TileRoadList.Length - 1;

        skillAction.SavePos = caster.TileRoadManager.TileRoadList[index].GetPositionForUnit(); ;
        useEffect.transform.position = skillAction.SavePos;
        skillAction.IsPause = true;
    }

    private IEnumerator WalkerLogic(GameSkillAction skillAction, GameEffectObject effect, TileRoadMgr tileRoadMgr, GameUnit caster)
    {
        int currIndex = tileRoadMgr.TileRoadList.Length - 1;

        float cycleGage = 0.0f;

        while (true)
        {
            Vector3 targetpos = tileRoadMgr.TileRoadList[currIndex].GetPositionForUnit();

            Vector3 pos = skillAction.SavePos;
            float moveSpeed = _speed;

            Vector3 dir = (targetpos - pos).normalized;
            float dist = (targetpos - pos).magnitude;
            float speed = moveSpeed * Time.fixedDeltaTime;
            if (dist < speed)
            {
                float moveTo = speed - dist;
                pos += dir * moveTo;

                --currIndex;
                if (currIndex < 0)
                    break;
            }
            else
            {
                pos += dir * speed;
                skillAction.SavePos = pos;
                effect.transform.position = skillAction.SavePos;
            }

            if (cycleGage >= _cycle)
            {
                SkillUtil skillUtil = new SkillUtil();

                foreach (var unit in tileRoadMgr.TileUnitListAtLive)
                    if (skillUtil.CollisionCheck(unit.transform.position, skillAction.SavePos,_range))
                        EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_skilAsset, caster, unit);

                cycleGage = 0.0f;
            }

            cycleGage += Time.fixedDeltaTime;

            yield return new WaitForFixedUpdate();
        }

        skillAction.IsPause = false;
        EightUtil.GetCore<GameEffectMgr>().ReturnEffect(effect);
    }
}

[System.Serializable]
public class LifeAction : SkillFrame
{
    [SerializeField]
    private StatData _life = new StatData();

    [SerializeField]
    private StatData _percent = new StatData();

    [SerializeField]
    private bool _isOthersDamage = false;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);
        var rand = caster.UnitRandomController.GetNextValue(RandomValueType.Life);
        var percent = _percent.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);
        //var lifeDir = rand <= _percent.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep) ? 1 : -1;

        var life = _life.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep);
        Debug.Log("rand : " + rand.ToString() + "percent : " + percent.ToString());
        if (rand <= percent)
            caster.BattleManager.MsgInterface.BattleUserLife((int)life, 1, caster.BattleManager);
        else
        {
            if(_isOthersDamage == true)
                caster.BattleManager.MsgInterface.BattleUserLife((int)life, -1, caster.BattleManager);
        }

        //if (caster.UserManager == null)
        //    return;

        //if (rand <= _percent.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep))
        //    caster.UserManager.OnRecovery((int)_life.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep));
        //else
        //{
        //    if (_isOthersDamage)
        //        caster.UserManager.OnHit((int)_life.GetStat(caster.UnitLevel, caster.UnitReinforce, caster.UnitStep));
        //}
    }

}

[System.Serializable]
public class RushAction : SkillFrame
{
    [SerializeField]
    private float _speed = 0.0f;

    [SerializeField]
    private int _range = 0;

    [SerializeField]
    private SkillFrameEffect _effectData = null;

    [SerializeField]
    private bool _isNoDamage = false;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        var useEffect = _effectData.CreateEffect(skillAction, caster, target);
        if (useEffect != null)
        {
            target.AddEffect(useEffect);

            useEffect.Clear();
            useEffect.ReStart();
        }

        skillAction.StartCoroutine(RushLogic(skillAction, caster, target, useEffect));
        skillAction.IsPause = true;
    }

    private IEnumerator RushLogic(GameSkillAction skillAction, GameUnit caster, GameUnit target, GameEffectObject effect)
    {
        TileRoadMgr tileRoadMgr = target.TileRoadManager;
        int targetIndex = ++target.CurrTileIndex + _range;

        target.StopMove();

        //if (_isNoDamage)
        //    target.IsNoDamage = true;

        while (true)
        {
            if (target.Hp <= 0)
                break;

            if (target.IsDontMove == true)
                break;

            if (target.CurrTileIndex == -1)
                yield return new WaitUntil(() => target.CurrTileIndex != -1);

            Vector3 targetpos = tileRoadMgr.TileRoadList[target.CurrTileIndex].GetPositionForUnit();

            Vector3 pos = target.transform.position;
            float moveSpeed = _speed;

            //슬로우 버프 
            if (target.IsHaveBuff(BuffType.Slow) == true)
            {
                List<GameBuff> listBuff = new List<GameBuff>();
                target.GetBuff(BuffType.Slow, listBuff);
                foreach (var buff in listBuff)
                {
                    moveSpeed *= 1 / ((SlowBuff)buff).SlowPower;
                }
            }

            //빙결 버프 
            if (target.IsHaveBuff(BuffType.Frozen) == true)
            {
                List<GameBuff> listBuff = new List<GameBuff>();
                target.GetBuff(BuffType.Frozen, listBuff);
                foreach (var buff in listBuff)
                {
                    moveSpeed *= 1 / ((FrozenBuff)buff).FrozenPower;
                }
            }

            Vector3 dir = (targetpos - pos).normalized;
            float dist = (targetpos - pos).magnitude;
            float speed = moveSpeed * Time.fixedDeltaTime;

            if (dist < speed)
            {
                float moveTo = speed - dist;
                pos += dir * moveTo;

                int currIndex = target.CurrTileIndex + 1;

                if (currIndex >= tileRoadMgr.TileRoadList.Length ||
                    currIndex >= targetIndex)
                    break;
                else
                    target.CurrTileIndex = currIndex;

            }
            else
            {
                pos += dir * speed;
                skillAction.SavePos = pos;
                target.transform.position = skillAction.SavePos;

                if (target.SpineUnit != null)
                {
                    if (dir.x > 0.0f)
                        target.SpineUnit.IsFlip = true;
                    else
                        target.SpineUnit.IsFlip = false;
                }
            }

            yield return new WaitForFixedUpdate();
        }

        if (_isNoDamage)
            target.IsNoDamage = false;

        ////우선 이렇게 처리 해두었지만, 해당 스킬을 사용하는 녀석이 오크말고 생긴다면 수정 필요

        if (target.UnitState != GameUnitState.Die)
            target.UnitState = GameUnitState.Idle;

        EightUtil.GetCore<GameEffectMgr>().ReturnEffect(effect);

        skillAction.IsPause = false;
    }
}

[System.Serializable]
public class RandomTarget : SkillFrame
{
    private enum Type
    {
        mon,
        cha
    }

    [SerializeField]
    private Type _type;

    [SerializeField]
    private int _count = 0;

    [SerializeField]
    private SkillAsset _skillAsset = null;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        List<GameUnit> unitList = new List<GameUnit>();

        switch (_type)
        {
            case Type.mon:
                unitList.AddRange(caster.TileRoadManager.TileUnitListAtLive);
                break;
            case Type.cha:
                for (int i = 0; i < caster.TileSlotManager.TileSize; ++i)
                {
                    GameUnit unit = caster.TileSlotManager.GetTileUnit(i);
                    if (unit == null)
                        continue;

                    unitList.Add(unit);
                }
                break;
        }

        for (int i = 0; i < _count; ++i)
        {
            if (unitList.Count <= 0)
                break;

            var rand = caster.UnitRandomController.GetNextValueRange(RandomValueType.RandomTarget, 0, unitList.Count - 1);
            //var rand = 0;

            GameUnit unit = unitList[rand];
            unitList.RemoveAt(rand);

            if (unit.UnitState == GameUnitState.Die)
                continue;

            EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_skillAsset, caster, unit);
        }
    }
}

[System.Serializable]
public class SpawnAction : SkillFrame
{
    private enum Position
    {
        start,
        my,
        input,
        All,
    }

    [SerializeField]
    private int _dataId = 0;

    [SerializeField]
    private Position _pos;

    [SerializeField]
    private int _input = 0;

    [SerializeField]
    private bool _isDontMove = false;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        GameUnit unit = null;
        var monsterData = caster.BattleManager.MonsterController.GetMonsterData(BattleMonsterData.MonsterType.Summon, _dataId);
        var stageData = caster.BattleManager.StageController.GetCurrStageData();
        var waveData = caster.BattleManager.WaveController.GetCurrWaveData();

        switch (_pos)
        {
            case Position.start:
                break;

            case Position.my:
                unit = EightUtil.GetCore<GameUnitMgr>()?.CreateMonster<MonsterController>(_dataId, caster.BattleManager.Spawner.GetSpawnInit(monsterData, stageData, null), caster.BattleManager, caster.TileRoadManager, caster.TileSlotManager, caster.UserManager, caster.WaveController);
                unit.CurrTileIndex = caster.CurrTileIndex;
                unit.transform.position = caster.transform.position;
                break;

            case Position.input:
                unit = EightUtil.GetCore<GameUnitMgr>()?.CreateMonster<MonsterController>(_dataId, caster.BattleManager.Spawner.GetSpawnInit(monsterData, stageData, null), caster.BattleManager, caster.TileRoadManager, caster.TileSlotManager, caster.UserManager, caster.WaveController);
                unit.CurrTileIndex = _input;
                unit.transform.position = caster.TileRoadManager.TileRoadList[_input].GetPositionForUnit();
                break;

            case Position.All:
                TileObjectBase[] tileArray = caster.TileRoadManager.TileRoadList;
                for (int i = 1; i < tileArray.Length - 1; ++i)
                {
                    unit = EightUtil.GetCore<GameUnitMgr>()?.CreateMonster<MonsterController>(_dataId, caster.BattleManager.Spawner.GetSpawnInit(monsterData, stageData, null), caster.BattleManager, caster.TileRoadManager, caster.TileSlotManager, caster.UserManager, caster.WaveController);

                    unit.CurrTileIndex = i;
                    unit.transform.position = tileArray[i].GetPositionForUnit();
                }
                break;
        }

        if (_isDontMove)
            unit.IsDontMove = true;
    }
}

[System.Serializable]
public class CollsionAction : SkillFrame
{
    [SerializeField]
    private int _matchId = -1;

    [SerializeField]
    private float _range = 0.0f;

    [SerializeField]
    private SkillAsset _skillAsset = null;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        SkillUtil skillUtil = new SkillUtil();

        foreach (var unit in caster.TileRoadManager.TileUnitListAtLive)
        {
            if (unit.UnitID == _matchId)
            {
                if (skillUtil.CollisionCheck(target.transform.position, unit.transform.position,_range))
                {
                    EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_skillAsset, caster, unit);
                    break;
                }
            }
        }
    }
}

[System.Serializable]
public class HpAction : SkillFrame
{
    private enum Type
    {
        send,
    }

    [SerializeField]
    private Type _type;

    [SerializeField]
    private float _ratio = 0.0f;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        switch (_type)
        {
            case Type.send:
                target.Hp = target.Hp + (int)((float)caster.Hp * _ratio);
                caster.OnDamage(caster.Hp, false);
                break;
        }
    }
}

public class ConditionAction : SkillFrame
{
    protected enum Type
    {
        Same,
        NotSame,
        SameAndHigh,
        SameAndLow,
        High,
        Low
    }

    [SerializeField]
    protected Type _type;

    [SerializeField]
    protected SkillAsset _skillAsset = null;
}

[System.Serializable]
public class HpCondition : ConditionAction
{
    [SerializeField]
    private int _value = 0;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);

        switch (_type)
        {
            case Type.Same: if (target.Hp == _value) { break; } return;
            case Type.NotSame: if (target.Hp != _value) { break; } return;
            case Type.SameAndHigh: if (target.Hp >= _value) { break; } return;
            case Type.SameAndLow: if (target.Hp <= _value) { break; } return;
            case Type.High: if (target.Hp > _value) { break; } return;
            case Type.Low: if (target.Hp < _value) { break; } return;
        }

        EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(_skillAsset, caster, target);
    }
};

[System.Serializable]
public class SpineAction : SkillFrame
{
    [SerializeField]
    private int _skinId = -1;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);
        ChangeSpine(target);
    }

    private void ChangeSpine(GameUnit target)
    {
        if (_skinId == -1)
        {
            Debug.Log(target);
            Debug.Log("변경할 스파인 ID를 입력해 주세요.");
            return;
        }

        var skin = EightUtil.GetCore<GameSkinMgr>().GetSkin(_skinId);
        target.SpineUnit = skin;
        target.OnSpineInitialize();
        target.OnNormal();
        skin.transform.position = target.transform.position;
    }
}

[System.Serializable]
public class SpineCondition : ConditionAction
{
    [SerializeField]
    private SkillAsset _others = null;

    [SerializeField]
    private int _value = -1;

    [SerializeField]
    bool _self = false;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);


        SkillAsset skill = null;
        GameUnit unit = target;
        if (_self == true)
            unit = caster;

        if (Check(unit) == true)
            skill = _skillAsset;
        else
            skill = _others;

        if (skill == null)
            return;

        EightUtil.GetCore<GameSkillActionMgr>().OnProcessSkill(skill, caster, target);
    }

    private bool Check(GameUnit target)
    {
        switch (_type)
        {
            case Type.Same: if (target.SpineUnit.SkinID == _value) { break; } return false;
            case Type.NotSame: if (target.SpineUnit.SkinID != _value) { break; } return false;
        }

        return true;
    }
}

[System.Serializable]
public class DontBuffAction : SkillFrame
{
    private enum Type
    {
        Add,
        Remove
    }

    [SerializeField]
    private Type _type;

    [SerializeField]
    private List<BuffType> _buffTypes = new List<BuffType>();

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);
        switch (_type)
        {
            case Type.Add:
                foreach (var type in _buffTypes) { target.AddDontBuff(type); }
                break;
            case Type.Remove:
                foreach (var type in _buffTypes) { target.RemoveDontBuff(type); }
                break;
        }
    }
}

[System.Serializable]
public class RemoveBuffAction : SkillFrame
{
    [SerializeField]
    private BuffType _buff;

    [SerializeField]
    private int _tag = -1;

    public override void OnSkillAction(GameSkillAction skillAction, GameUnit caster, GameUnit target)
    {
        base.OnSkillAction(skillAction, caster, target);
        target.RemoveBuff(target.GetBuff(_buff, _tag));
    }
}