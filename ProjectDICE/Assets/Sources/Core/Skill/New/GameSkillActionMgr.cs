﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class GameSkillActionMgr : EightWork.Core.EightCore
{
    private Queue<GameSkillAction> _skillActionQueue = new Queue<GameSkillAction>();

    private List<GameSkillAction> _useSkillActionList = new List<GameSkillAction>();

    [SerializeField]
    private int _reserveCount = 5;
    public int ReserveCount { get => _reserveCount; set => _reserveCount = value; }

    public override void InitializedCore()
    {

    }

    protected override void OnEnable()
    {
        base.OnEnable();

        StartCoroutine(UseSkillChecker());
    }

    protected override void RegisterMsg()
    {
        base.RegisterMsg();

        this.RegisterEvent(EIGHT_MSG.CHANGE_SCENE_START, OnChangeScene);
    }

    private void OnChangeScene(EightMsgContent eightMsgContent)
    {
        ReleaseAll();
    }

    protected override void UnRegisterMsg()
    {
        base.UnRegisterMsg();

        this.RemoveEvent(EIGHT_MSG.CHANGE_SCENE_START, OnChangeScene);
    }

    private bool ReserveSkillAction()
    {
        for (int i = 0; i < _reserveCount; ++i)
        {
            GameObject skillObject = new GameObject("GameSkillAction");
            var skillAction = skillObject.AddComponent<GameSkillAction>();
            skillObject.transform.SetParent(this.transform);
            skillObject.SetActive(false);
            _skillActionQueue.Enqueue(skillAction);
        }

        return true;
    }

    public void OnProcessSkill(SkillAsset skillAsset, GameUnit caster, GameUnit target)
    {
        if (_skillActionQueue.Count <= 0)
        {
            if (ReserveSkillAction() == false)
                return;
        }

        var skillAction = _skillActionQueue.Dequeue();
        skillAction.gameObject.SetActive(true);
        _useSkillActionList.Add(skillAction);

        skillAction.ProcessSkill(skillAsset, caster, target);

        //StartCoroutine(SkillEndCheck(skillAction));
    }

    public void OnProcessAction(GameActionData action, GameUnit caster, GameUnit target)
    {

    }

    private IEnumerator UseSkillChecker()
    {
        while (true)
        {
            for (int i = _useSkillActionList.Count - 1; i >= 0; --i)
            {
                var skill = _useSkillActionList[i];
                if (skill.IsProcessing == false)
                    ReturnSkillAction(skill);
            }

            yield return null;
        }
    }

    private IEnumerator SkillEndCheck(GameSkillAction skillAction)
    {
        if (skillAction.IsProcessing == true)
            yield return new WaitUntil(() => skillAction.IsProcessing == false);

        ReturnSkillAction(skillAction);
    }

    public void ReturnSkillAction(GameSkillAction skillAction)
    {
        skillAction.ReleaseSkill();
        skillAction.gameObject.SetActive(false);
        skillAction.transform.position = Vector3.zero;

        _useSkillActionList.Remove(skillAction);
        _skillActionQueue.Enqueue(skillAction);
    }

    public void ReleaseAll()
    {
        Debug.Log("Release SkillAction");
        for (int i = _useSkillActionList.Count - 1; i >= 0; --i)
        {
            ReturnSkillAction(_useSkillActionList[i]);
        }

        var list = _skillActionQueue.ToArray();
        for (int i = list.Length - 1; i >= 0; --i)
        {
            Destroy(list[i].gameObject);
        }
        _skillActionQueue.Clear();
    }
}
