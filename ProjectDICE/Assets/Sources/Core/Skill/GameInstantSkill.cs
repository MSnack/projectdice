﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "New GameInstantSkill", menuName = "GameInstantSkill", order = 3)]
public class GameInstantSkill : GameSkillData
{
    [SerializeField]
    private GameEffectData _characterEffect = null;
    public GameEffectData CharacterEffect => _characterEffect;

    [SerializeField]
    private GameEffectData _hitEffect = null;
    public GameEffectData HitEffect => _hitEffect;

    private void OnEnable()
    {
        SkillType = SkillType.Instant;
    }
}
