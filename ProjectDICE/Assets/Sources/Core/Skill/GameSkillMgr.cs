﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.Events;

public class GameSkillMgr : EightWork.Core.EightCore
{
    private Dictionary<SkillType, Queue<GameSkillBase>> _skillQueue = new Dictionary<SkillType, Queue<GameSkillBase>>();

    private List<GameSkillBase> _useSkillList = new List<GameSkillBase>();

    public override void InitializedCore()
    {

    }

    public GameSkillBase ProcessSkill(Vector2 startPos, float value, GameUnit targetUnit, GameSkillData data, UnityAction<GameSkillBase> skillEvent = null)
    {
        var skill = GetSkill(data.SkillType);
        if (skill == null)
            return null;

        skill.ProcessSkill(startPos, value, targetUnit, data, skillEvent);
        StartCoroutine(SkillCheck(skill));
        return skill;
    }

    private GameSkillBase GetSkill(SkillType type)
    {
        GameSkillBase skill = null;
        if (_skillQueue.ContainsKey(type) == false ||
            _skillQueue[type].Count <= 0)
        {
            GameObject obj = new GameObject("GameSkill");
            obj.transform.SetParent(this.transform);
            obj.SetActive(true);
            switch (type)
            {
                case SkillType.Bullet: skill = obj.AddComponent<BulletSkill>(); break;
                case SkillType.Instant: skill = obj.AddComponent<InstantSkill>(); break;
            }

            skill.Init();
        }
        else
        {
            skill = _skillQueue[type].Dequeue();
            skill.gameObject.SetActive(true);
        }

        _useSkillList.Add(skill);
        return skill;
    }

    private void ReturnSkill(GameSkillBase skill)
    {
        _useSkillList.Remove(skill);

        if (_skillQueue.ContainsKey(skill.GameSkillType) == false)
            _skillQueue[skill.GameSkillType] = new Queue<GameSkillBase>();

        skill.ReleaseSkill();
        skill.gameObject.SetActive(false);
        _skillQueue[skill.GameSkillType].Enqueue(skill);
    }

    public void ReturnAllSkill()
    {
        for (int i = _useSkillList.Count - 1; i >= 0; --i)
        {
            ReturnSkill(_useSkillList[i]);
        }
        _useSkillList.Clear();
    }

    private IEnumerator SkillCheck(GameSkillBase skill)
    {
        yield return new WaitUntil(() => skill.IsComplateProcessing == true);

        ReturnSkill(skill);
    }
}
