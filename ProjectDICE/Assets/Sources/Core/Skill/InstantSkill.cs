﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using EightWork;

public class InstantSkill : GameSkillBase
{
    [SerializeField, ReadOnly]
    private GameInstantSkill _skillData = null;

    public override void Init()
    {
        GameSkillType = SkillType.Instant;
    }

    protected override void ProcessingData(Vector3 startPos, float value, GameUnit targetUnit, GameSkillData data, UnityAction<GameSkillBase> skillAction = null)
    {
        base.ProcessingData(startPos, value, targetUnit, data, skillAction);

        _skillData = data as GameInstantSkill;
    }

    protected override IEnumerator SkillRoutine()
    {
        var characaterEffect = EffectMgr.UseEffect(_skillData.CharacterEffect);
        characaterEffect.transform.position = StartPos;
        yield return null;

        var hitEffect = EffectMgr.UseEffect(_skillData.HitEffect);
        hitEffect.transform.position = TargetPos;
        yield return null;

        RoutineRelease();
    }

    public override void ReleaseSkill()
    {
        base.ReleaseSkill();

        _skillData = null;
    }

    private void Reset()
    {
        GameSkillType = SkillType.Instant;
    }
}
