﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "New GameBulletSkill", menuName = "GameBulletSkill", order = 3)]
public class GameBulletSkill : GameSkillData
{
    [SerializeField]
    private GameEffectData _characterEffectData = null;
    public GameEffectData CharacterEffecData => _characterEffectData;

    [SerializeField]
    private GameEffectData _bulletEffectData = null;
    public GameEffectData BulletEffectData => _bulletEffectData;

    [SerializeField]
    private GameEffectData _hitEffectData = null;
    public GameEffectData HitEffectData => _hitEffectData;

    [SerializeField]
    private float _bulletSpeed = 1.0f;
    public float BulletSpeed => _bulletSpeed;

    private void OnEnable()
    {
        SkillType = SkillType.Bullet;
    }
}
