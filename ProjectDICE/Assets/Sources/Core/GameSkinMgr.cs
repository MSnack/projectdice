﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class GameSkinMgr : EightWork.Core.EightCore
{
    private class SkinInfo
    {
        private static GameSkinMgr _skinMgr = null;
        private static GameSkinMgr SkinMgr => GetSkinMgr();

        private static GameSkinMgr GetSkinMgr()
        {
            if (_skinMgr == null)
            {
                _skinMgr = EightUtil.GetCore<GameSkinMgr>();
            }

            return _skinMgr;
        }

        private GameSkinData _skinAsset = null;

        private GameSpineObject _skinAssetPrefab = null;
        private Queue<GameSpineObject> _reserveQueue = new Queue<GameSpineObject>();
        public bool IsEmptyQueue => _reserveQueue.Count <= 0;

        private bool _isLoaded = false;
        public bool IsLoaded => _isLoaded;

        public SkinInfo(GameSkinData data)
        {
            if (data == null)
                return;

            _skinAsset = data;
        }

        private bool ReserveQueue()
        {
            if (_isLoaded == false)
                return false;

            var reserveCount = _skinAsset.ReserveCount;
            for (int i = 0; i < reserveCount; ++i)
            {
                var copy = Instantiate(_skinAssetPrefab);
                if (copy == null)
                    return false;

                copy.UpdateSkinData(_skinAsset);
                copy.gameObject.SetActive(false);
                copy.transform.SetParent(SkinMgr.transform);
                _reserveQueue.Enqueue(copy);
            }

            return true;
        }

        public GameSpineObject GetSkin()
        {
            if (_isLoaded == false)
                return null;

            if (IsEmptyQueue == true)
            {
                if (ReserveQueue() == false)
                    return null;
            }

            return _reserveQueue.Dequeue();
        }

        public void ReturnSkin(GameSpineObject skin)
        {
            if (skin == null || skin.SkinID != _skinAsset.DataId)
                return;

            _reserveQueue.Enqueue(skin);
        }

        public void UploadSkin()
        {
            if (_skinAsset == null)
                return;

            var handle = _skinAsset.SkinAsset.LoadAssetAsync();
            handle.Completed += (result) =>
            {
                var gameObject = result.Result;
                if (gameObject == null)
                {
                    GameDataMgr.IsLoadFailed = true;
                    return;
                }

                var spine = gameObject.GetComponent<GameSpineObject>();
                if (spine == null)
                    return;

                _skinAssetPrefab = spine;
                _isLoaded = true;
            };
        }

        public void Release()
        {
            if (_isLoaded == false)
                return;

            Clear();

            _skinAsset.SkinAsset.ReleaseAsset();
            _skinAssetPrefab = null;
            _isLoaded = false;
        }

        public void Clear()
        {
            var list = _reserveQueue.ToArray();
            _reserveQueue.Clear();

            for (int i = list.Length - 1; i >= 0; --i)
            {
                Destroy(list[i].gameObject);
            }
        }
    }

    [SerializeField]
    private AssetLabelReference _skinLabel = null;

    private Dictionary<int, SkinInfo> _skinInfoPool = new Dictionary<int, SkinInfo>();

    //private Dictionary<int, GameSpineObject> _prefabPool = new Dictionary<int, GameSpineObject>();
    //private Dictionary<int, Queue<GameSpineObject>> _reservePool = new Dictionary<int, Queue<GameSpineObject>>();

    //[SerializeField]
    //private int _reserveCount = 5;
    //public int ReserveCount { get => _reserveCount; set => _reserveCount = value; }

    [SerializeField, ReadOnly]
    private bool _isLoadComplete = false;
    public bool IsLoadComplete => _isLoadComplete;

    public override void InitializedCore()
    {
        //LoadData();
    }

    public void LoadData()
    {
        if (_isLoadComplete == true)
            return;

        var loadHandle = Addressables.LoadAssetsAsync<GameSkinData>(_skinLabel.labelString, null);
        loadHandle.Completed += (result) =>
            {
                var skinDataList = result.Result;
                if (skinDataList == null)
                {
                    Debug.Log("Fail Load SkinData");
                    GameDataMgr.IsLoadFailed = true;
                    return;
                }

                foreach (var data in skinDataList)
                {
                    var skinInfo = new SkinInfo(data);
                    _skinInfoPool.Add(data.DataId, skinInfo);
                }

                _isLoadComplete = true;
            };
    }

    public GameSpineObject GetSkin(int skinId)
    {
        if (_skinInfoPool.ContainsKey(skinId) == false)
            return null;

        var skin = _skinInfoPool[skinId].GetSkin();
        if (skin != null)
        {
            skin.gameObject.SetActive(true);
            skin.transform.SetParent(null);
        }

        return skin;
    }

    public void ReturnSkin(GameSpineObject skin)
    {
        if (skin == null || _skinInfoPool.ContainsKey(skin.SkinID) == false)
            return;

        skin.SkinColor = Color.white;
        skin.ChangeState(0);
        skin.Rebind();
        skin.transform.SetParent(this.transform);
        skin.gameObject.SetActive(false);
        skin.transform.position = Vector3.zero;

        var skinInfo = _skinInfoPool[skin.SkinID];
        skinInfo.ReturnSkin(skin);
    }

    public bool HasSkin(int skinId)
    {
        return _skinInfoPool.ContainsKey(skinId);
    }

    private SkinInfo GetSkinInfo(int skinId)
    {
        if (_isLoadComplete == false ||
            _skinInfoPool.ContainsKey(skinId) == false)
            return null;

        return _skinInfoPool[skinId];
    }

    public void UploadSkin(int skinId)
    {
        if (_isLoadComplete == false ||
            _skinInfoPool.ContainsKey(skinId) == false)
            return;

        _skinInfoPool[skinId].UploadSkin();
    }

    public bool IsUploadSkin(int skinId)
    {
        if (_isLoadComplete == false || _skinInfoPool.ContainsKey(skinId) == false)
            return false;

        return _skinInfoPool[skinId].IsLoaded;
    }

    public bool IsAllUpload
    {
        get
        {
            foreach (var data in _skinInfoPool.Values)
            {
                if (data.IsLoaded == false)
                    return false;
            }

            return true;
        }
    }
    public void UploadAllSkin()
    {
        if (_isLoadComplete == false)
            return;

        foreach (var data in _skinInfoPool.Values)
        {
            data.UploadSkin();
        }
    }

    public void ReleaseSkin(int skinId)
    {
        if (_isLoadComplete == false ||
            _skinInfoPool.ContainsKey(skinId) == false)
            return;

        _skinInfoPool[skinId].Release();
    }

    public void ReleaseAll()
    {
        if (_isLoadComplete == false)
            return;

        foreach (var skinInfo in _skinInfoPool.Values)
        {
            skinInfo.Release();
        }
    }

    public void ClearSkin(int skinId)
    {
        if (_isLoadComplete == false ||
            _skinInfoPool.ContainsKey(skinId) == false)
            return;

        _skinInfoPool[skinId].Clear();
    }

    public void ClearAll()
    {
        if (_isLoadComplete == false)
            return;

        foreach (var skinInfo in _skinInfoPool.Values)
        {
            skinInfo.Clear();
        }
    }
    //private void LoadData()
    //{
    //    if (_isLoadComplete == true)
    //        return;

    //    var loadHandle = Addressables.LoadAssetsAsync<GameObject>(_skinLabel.labelString, null);
    //    loadHandle.Completed += (result) =>
    //        {
    //            var gameSkinPrefab = result.Result;
    //            if (gameSkinPrefab == null)
    //            {
    //                Debug.Log("Fail Load GameSkinPrefab");
    //                return;
    //            }

    //            foreach (var prefab in gameSkinPrefab)
    //            {
    //                var spine = prefab.GetComponent<GameSpineObject>();
    //                _prefabPool[spine.SkinID] = spine;
    //            }
    //        };

    //    _isLoadComplete = true;
    //}

    //public void ReturnSkin(GameSpineObject skin)
    //{
    //    skin.ChangeState(0);
    //    skin.transform.SetParent(this.transform);
    //    skin.gameObject.SetActive(false);
    //    skin.transform.position = Vector3.zero;

    //    _reservePool[skin.SkinID].Enqueue(skin);
    //}

    //public GameSpineObject GetSkin(int skinId)
    //{
    //    if (_reservePool.ContainsKey(skinId) == false)
    //    {
    //        if (ReserveSkin(skinId) == false)
    //            return null;
    //    }

    //    if (_reservePool[skinId].Count <= 0)
    //    {
    //        if (ReserveSkin(skinId) == false)
    //            return null;
    //    }

    //    var skin = _reservePool[skinId].Dequeue();
    //    skin.gameObject.SetActive(true);
    //    skin.transform.SetParent(null);

    //    return skin;
    //}

    //public bool HasSkin(int skinId)
    //{
    //    return _prefabPool.ContainsKey(skinId);
    //}

    //public void RegisterPrefab(int skinId)
    //{
    //    string name = "UnitSkin" + skinId.ToString();
    //    Addressables.LoadAssetAsync<GameObject>(name).Completed += (AsyncOperationHandle<GameObject> handle) =>
    //    {
    //        var spineObject = handle.Result.GetComponent<GameSpineObject>();
    //        if (spineObject == null)
    //        {
    //            Debug.LogError("Not Have SpineObject at " + name);
    //            return;
    //        }

    //        ReleaseSkin(skinId);
    //        AddSkin(skinId, spineObject);
    //    };
    //}

    //public bool ReserveSkin(int skinId)
    //{
    //    if (_reservePool.ContainsKey(skinId) == false)
    //        _reservePool[skinId] = new Queue<GameSpineObject>();

    //    if (_prefabPool.ContainsKey(skinId) == false)
    //        return false;

    //    var skin = _prefabPool[skinId];
    //    if (skin == null)
    //        return false;

    //    for (int i = 0; i < _reserveCount; ++i)
    //    {
    //        var skinCopy = Instantiate(skin, this.transform);
    //        skinCopy.gameObject.SetActive(false);
    //        _reservePool[skinId].Enqueue(skinCopy);
    //    }

    //    return true;
    //}

    //private void AddSkin(int skinId, GameSpineObject skin)
    //{
    //    _prefabPool[skinId] = skin;
    //    ReserveSkin(skinId);
    //}

    //public void ReleaseSkin(int skinId)
    //{
    //    if (_reservePool.ContainsKey(skinId) == true)
    //    {
    //        int queueCount = _reservePool[skinId].Count;
    //        for (int i = 0; i <= queueCount; ++i)
    //        {
    //            Destroy(_reservePool[skinId].Dequeue());
    //        }
    //        _reservePool[skinId].Clear();
    //    }

    //    if (_prefabPool.ContainsKey(skinId) == true)
    //    {
    //        var prefab = _prefabPool[skinId];
    //        if (prefab != null)
    //        {
    //            _prefabPool.Remove(skinId);
    //            Addressables.Release(prefab);
    //        }
    //    }
    //}

    //public void ReleaseSkinAll()
    //{
    //    var keyList = _prefabPool.Keys;
    //    foreach (var skinId in keyList)
    //    {
    //        ReleaseSkin(skinId);
    //    }
    //}
}
