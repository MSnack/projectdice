﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;

public class GameSoundPool : MonoBehaviour
{

    [SerializeField]
    private int _audioSourceSize = 5;

    [SerializeField]
    private GameObject _gameSoundSlotPrefab = null;

    private readonly List<SoundSelector> _slots = new List<SoundSelector>();

    private void Awake()
    {
        GeneratePool();
    }

    private void Start()
    {
        
    }

    private void GeneratePool()
    {
        for (int i = 0; i < _audioSourceSize; ++i)
        {
            var slotObject = Instantiate(_gameSoundSlotPrefab, transform);
            slotObject.name = "Slot" + i;

            var selector = slotObject.GetComponent<SoundSelector>();
            _slots.Add(selector);
        }
    }

    public bool PlaySound(AudioClip audioClip)
    {
        if (audioClip == null) return false;
        
        foreach (var selector in _slots)
        {
            if (selector.isPlaying()) continue;
            selector.PlayOneShot(audioClip);
            return true;
        }
        return false;
    }

    public void PlaySound(AssetReferenceAudioClip assetReference, UnityAction<bool> resultEvent = null)
    {
        if (assetReference == null)
        {
            resultEvent?.Invoke(false);
            return;
        }
        
        foreach (var selector in _slots)
        {
            if (selector.isPlaying()) continue;
            selector.AudioClipReference = assetReference;
            selector.SetAudioClip(() =>
            {
                selector.Play();
                resultEvent?.Invoke(true);
            });
            return;
        }
        resultEvent?.Invoke(false);
    }

    public void SetVolume(float value)
    {
        foreach (var selector in _slots)
        {
            selector.AudioSource.volume = value;
        }
    }
}

[Serializable]
public class AssetReferenceAudioClip : AssetReferenceT<AudioClip>
{
    public AssetReferenceAudioClip(string guid) : base(guid) {}
}