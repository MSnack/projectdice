﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork.Core;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;

public class GameSoundMgr : EightCore
{
    [SerializeField]
    private GameSoundPool _soundPool = null;
    [SerializeField]
    private AssetLabelReference _effectSoundLabel = null;

    private static readonly Dictionary<int, AudioClip> _audioClips = new Dictionary<int, AudioClip>();

    private UnityAction<float> _setBGMVolumeEvent = null;
    public event UnityAction<float> SetBGMVolumeEvent
    {
        add => _setBGMVolumeEvent += value;
        remove
        {
            if (value != null) _setBGMVolumeEvent -= value;
        }
    }
    
    [SerializeField]
    private float _bgmVolume = 0.7f;
    public float BGMVolume
    {
        get => _bgmVolume;
        set
        {
            _bgmVolume = value;
            _setBGMVolumeEvent?.Invoke(value);
        }
    }

    public bool IsLoaded { get; private set; }

    [SerializeField]
    private float _effectVolume = 1.0f;
    public float EffectVolume
    {
        get => _effectVolume;
        set
        {
            _effectVolume = value;
            _soundPool.SetVolume(value);
        }
    }

    private void Awake()
    {
        if(_audioClips.Count <= 0) Init();
    }
    
    private void Init()
    {
        var loadHandle = Addressables.LoadAssetsAsync<SoundEffectData>(_effectSoundLabel.labelString, null);
        loadHandle.Completed += (result) =>
        {
            var soundData = result.Result;
            if (soundData == null)
            {
                Debug.Log("Load Goods Sprite Failed");
                return;
            }

            foreach (var sound in soundData)
            {
                _audioClips.Add(sound.DataId, sound.AudioClip);
            }

            IsLoaded = true;
        };
    }

    public override void InitializedCore()
    {
        LoadVolumeSettings();
    }

    private void LoadVolumeSettings()
    {
        if(PlayerPrefs.HasKey("bgm_volume")) 
            _bgmVolume = PlayerPrefs.GetFloat("bgm_volume");
        else 
            SaveVolumeSettings();
        
        if(PlayerPrefs.HasKey("effect_volume"))
            _effectVolume = PlayerPrefs.GetFloat("effect_volume");
        else 
            SaveVolumeSettings();
    }

    public void SaveVolumeSettings()
    {
        PlayerPrefs.SetFloat("bgm_volume", _bgmVolume);
        PlayerPrefs.SetFloat("effect_volume", _effectVolume);
    }

    public bool PlaySoundEffect(int id)
    {
        // if (_audioClips.ContainsKey(id) == false) return false;
        var sound = _audioClips[id];
        return _soundPool.PlaySound(sound);
    }

    public bool PlaySoundEffect(AudioClip clip)
    {
        return _soundPool.PlaySound(clip);
    }

    public AudioClip GetAudioClip(int id)
    {
        return _audioClips.ContainsKey(id) == false ? null : _audioClips[id];
    }
}
