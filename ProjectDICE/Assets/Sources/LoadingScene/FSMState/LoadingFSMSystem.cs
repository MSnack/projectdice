﻿using System.Collections;
using System.Collections.Generic;
using EightWork.FSM;
using UnityEngine;
using UnityEngine.UI;

public class LoadingFSMSystem : EightFSMSystem<LoadingFSMSystem.LoadingFSMState, LoadingStateBase, LoadingSceneInterface>
{
    
    public enum LoadingFSMState
    {
        DOWNLOAD,
        LOGIN,
        POLICY,
        AGREEMENT,
        COMPLETE,
        NICKNAME,
        CHECK_VERSION,
        
        NONE = -9999,
    }

    [SerializeField]
    private Text _versionText = null;
    
    protected override void Awake()
    {
        base.Awake();
        Input.multiTouchEnabled = false; 
        _versionText.text = "Ver " + Application.version;
        SetupComplete();
    }

    // Start is called before the first frame update
    void Start()
    {
        LoadingSceneInterface.CallChangingState += ChangeStateFunction;
    }
    
    private void ChangeStateFunction(LoadingFSMState state)
    {
        if (CurrState == state) return;
        ChangeState(state);
    }
}
