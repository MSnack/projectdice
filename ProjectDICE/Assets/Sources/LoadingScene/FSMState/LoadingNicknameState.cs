﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LoadingNicknameState : LoadingStateBase
{

    [SerializeField]
    private Text _nicknameText = null;

    private GameUserMgr _userMgr = null;
    private readonly GameWebRequestTranslator _translator = new GameWebRequestTranslator();

    protected override void Start()
    {
        base.Start();
    }

    protected override void Initialize()
    {
        State = LoadingFSMSystem.LoadingFSMState.NICKNAME;
    }

    public void ChangeNickname()
    {
        if (_userMgr == null) _userMgr = EightUtil.GetCore<GameUserMgr>();
        
        string nickname = _nicknameText.text;
        string idChecker = Regex.Replace(nickname, @"[^a-zA-Z0-9ㄱ-ㅎㅏ-ㅣ가-힣㉼-㉾]", "", RegexOptions.Singleline);
        if (nickname != idChecker)
        {
            GamePopupMgr.OpenNormalPopup("경고", "사용할 수 없는 닉네임 입니다.\n(특수문자 및 공백 사용불가)");
            _nicknameText.text = "";
            return;
        }
        
        if (nickname.Length < 2 || nickname.Length > 10)
        {
            GamePopupMgr.OpenNormalPopup("경고", "닉네임은 2~10자 입니다.");
            _nicknameText.text = "";
            return;
        }

        var userInfo = _userMgr.UserInfo;
        userInfo.NickName = nickname;
        
        _translator.SetSendData(userInfo.ToJson());
        _translator.Request(GameWebRequestType.Change_UserNickName, (trans) =>
        {
            var result = trans.GetRequestData();
            if (result["result"].ToString() == "-1")
            {
                GamePopupMgr.OpenNormalPopup("경고", "이미 존재하는 닉네임 입니다.");
                _nicknameText.text = "";
                return;
            }
            _userMgr.RefreshUserInfo(() => SystemMgr.ChangeState(LoadingFSMSystem.LoadingFSMState.COMPLETE));
        });
    }
}
