﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class LoadingDownloadState : LoadingStateBase
{
    private EightAssetMgr _assetMgr = null;

    [SerializeField]
    private Text _progressText = null;

    [SerializeField]
    private Transform _progressBarTransform = null;

    [SerializeField]
    private GameObject _webViewCanvas = null;
    [SerializeField]
    private GameObject _webViewGameObject = null;
    [SerializeField]
    private WebViewObject _webViewObject = null;

    private bool _noticeClosed = false;

    public void SetNoticeClose()
    {
        _noticeClosed = true;
        _webViewObject.SetVisibility(false);
        _webViewCanvas.SetActive(false);
        SystemMgr.ChangeState(LoadingFSMSystem.LoadingFSMState.LOGIN);
    }


    private bool _isInit = false;

    public override void StartState()
    {
        InitWebView("https://www.notion.so/eightstudiocorp/979d30f004614adb93fc3c6d4da0c028");
        _progressBarTransform.localScale = new Vector3(0, 1, 1);
        _progressText.text = $"리소스 확인 중";
        base.StartState();
        if (!_isInit)
        {
            InitDownloader();
            _isInit = true;
        }
    }
    

    public override void EndState()
    {
        base.EndState();
    }

    private void InitDownloader()
    {
        _assetMgr = EightUtil.GetCore<EightAssetMgr>();
        _assetMgr.AskDownload((downloadSize) =>
        {
            if (downloadSize <= 0)
            {
                OpenWebView();
                return;
            }

            float downloadSizeMB = (downloadSize / 1024f) / 1024f;
            GamePopupMgr.OpenNormalPopup("업데이트",
                $"{downloadSizeMB:F2}MB 만큼의 데이터를 다운로드하여\n업데이트를 진행합니다.",
                new[] {
                    new NormalPopup.ButtonConfig("취소", NormalPopup.Close),
                    new NormalPopup.ButtonConfig("확인", OnDownload)
                });
        });
    }

    private void OnDownload()
    {
        NormalPopup.Close();
        _assetMgr.DownloadAssets(DownloadProgress, () =>
        {
            DownloadProgress(1.0f);
            OpenWebView();
        });
    }

    private void DownloadProgress(float percent)
    {
        _progressBarTransform.localScale = new Vector3(percent, 1, 1);
        _progressText.text = $"리소스 {percent:P2} 다운로드";
    }

    protected override void Initialize()
    {
        State = LoadingFSMSystem.LoadingFSMState.DOWNLOAD;
    }
    
    private void InitWebView(string url)
    {
        var strUrl = url;
        if (_webViewObject == null)
        {
            _webViewObject = _webViewGameObject.AddComponent<WebViewObject>();
            _webViewObject.Init(msg =>
            {
                //load
                Debug.Log(string.Format("{0}", msg));
            });
        }

        _webViewObject.LoadURL(strUrl);
    }

    private void OpenWebView()
    {
        _webViewCanvas.SetActive(true);
        _webViewGameObject.SetActive(true);
        _webViewObject.SetVisibility(true);
        Rect newSafeArea = Screen.safeArea;
        Vector2 _anchorMin = newSafeArea.position;
        Vector2 _anchorMax = newSafeArea.position + newSafeArea.size;

        _anchorMin.x /= Screen.width;
        _anchorMax.x /= Screen.width;
        _anchorMin.y /= Screen.height;
        _anchorMax.y /= Screen.height;

        _webViewObject.SetMargins(
            (int) (_anchorMin.x * Screen.width) + 30,
            (int) ((1 - _anchorMax.y) * Screen.height) + 150,
            (int) ((1 - _anchorMax.x) * Screen.width) + 30,
            (int) (_anchorMin.y * Screen.height) + 30);
    }
}
