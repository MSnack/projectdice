﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingCheckVerState : LoadingStateBase
{
    [SerializeField]
    private Text _loadingText = null;

    [SerializeField]
    private string _loadingString = "버전 확인 중";
    
    private readonly GameWebRequestTranslator _translator = new GameWebRequestTranslator();

    public override void StartState()
    {
        base.StartState();

        _loadingText.text = _loadingString;
        string url = "";

        if (Application.platform == RuntimePlatform.Android)
        {
            url = $"market://details?id={Application.identifier}";
        }
        else if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            url = "https://youtu.be/tRycu1zJBMg";
            SystemMgr.ChangeState(LoadingFSMSystem.LoadingFSMState.DOWNLOAD);
            return;
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            url = "https://youtu.be/tRycu1zJBMg";
        }
        
        string query = "/0";
        _translator.Request(GameWebRequestType.Get_Version, query, (trans) =>
        {
            var json = trans.GetRequestData();
            if (IsNeedUpdate(json["version"].ToString()))
            {
                GamePopupMgr.OpenNormalPopup("업데이트", "최신버전의 게임으로 업데이트를 해야합니다.",
                    new []
                    {
                        new NormalPopup.ButtonConfig("업데이트", () =>
                        {
                            Application.OpenURL(url);
                            Application.Quit();
                        }), 
                    }
                );
                return;
            }
            SystemMgr.ChangeState(LoadingFSMSystem.LoadingFSMState.DOWNLOAD);
        });
    }

    private bool IsNeedUpdate(string serverVer)
    {
        string[] current = Application.version.Split('.');
        string[] server = serverVer.Split('.');
        
        int[] majorVersion = { int.Parse(current[0]), int.Parse(server[0]) };
        int[] minorVersion = { int.Parse(current[1]), int.Parse(server[1]) };
        int[] build = { int.Parse(current[2]), int.Parse(server[2]) };

        if (majorVersion[0] > majorVersion[1]) return false;
        if (majorVersion[0] < majorVersion[1]) return true;
        
        if (minorVersion[0] > minorVersion[1]) return false;
        if (minorVersion[0] < minorVersion[1]) return true;
        
        if (build[0] > build[1]) return false;
        if (build[0] < build[1]) return true;

        return false;
    }

    public override void EndState()
    {
        base.EndState();
    }

    protected override void Initialize()
    {
        State = LoadingFSMSystem.LoadingFSMState.CHECK_VERSION;
    }
}
