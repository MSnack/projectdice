﻿using System.Collections;
using System.Collections.Generic;
using EightWork.MsgSystem;
using UnityEngine;
using UnityEngine.Events;

public class LoadingSceneInterface : EightMsgSystem
{
    private static UnityAction<LoadingFSMSystem.LoadingFSMState> _changingState = null;
    public static event UnityAction<LoadingFSMSystem.LoadingFSMState> OnChangingState
    {
        add => _changingState += value;
        remove
        {
            if (value != null) _changingState -= value;
        }
    }

    private static UnityAction<LoadingFSMSystem.LoadingFSMState> _callChangingState = null;
    public static event UnityAction<LoadingFSMSystem.LoadingFSMState> CallChangingState
    {
        add
        {
            _callChangingState += value;
        }
        remove {}
    }

    /**
     * State 변경시 호출하는 함수
     */
    public static void InvokeChangingState(LoadingFSMSystem.LoadingFSMState state)
    {
        _callChangingState.Invoke(state);
    }
    
    /**
     * State 클래스의 StartState 함수 이외 사용 금지
     */
    public static void InvokeChangedState(LoadingFSMSystem.LoadingFSMState state)
    {
        _changingState?.Invoke(state);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        ReleaseActions();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
    }

    private void ReleaseActions()
    {
        _changingState = null;
        _callChangingState = null;
    }
}
