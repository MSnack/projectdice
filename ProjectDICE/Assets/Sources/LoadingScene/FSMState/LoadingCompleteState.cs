﻿using System;
using EightWork;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LoadingCompleteState : LoadingStateBase
{
    
    [SerializeField]
    private Text _progressText = null;

    [SerializeField]
    private string _progressString = "";

    [SerializeField]
    private Transform _progressBarTransform = null;

    public override void StartState()
    {
        _progressText.text = _progressString;
        SetPercent(0);
        base.StartState(); 
        StartCoroutine(WaitingDataLoaded());

    }

    public override void EndState()
    {
        base.EndState();
    }

    protected override void Initialize()
    {
        State = LoadingFSMSystem.LoadingFSMState.COMPLETE;
    }

    private IEnumerator WaitingDataLoaded()
    {
        int currentPercentIndex = 0;
        int loadSize = 4;
        bool isFailed = false;
        
        var gameDataMgr = EightUtil.GetCore<GameDataMgr>();
        if (gameDataMgr.IsLoaded == false)
        {
            gameDataMgr.OnLoadData();
            SetPercent((1f / loadSize) * (currentPercentIndex++));
            yield return new WaitUntil(() =>
            {
                if (!GameDataMgr.IsLoadFailed) return gameDataMgr.IsLoaded;
                isFailed = true;
                return true;
            });
        }
        if (isFailed)
        {
            OpenRestartPopup();
            yield break;
        }

        var gameUnitDataMgr = EightUtil.GetCore<GameUnitDataMgr>();
        gameUnitDataMgr.LoadGameUnitData();
        SetPercent((1f / loadSize) * (currentPercentIndex++));
        yield return new WaitUntil(() =>
        {
            if (!GameDataMgr.IsLoadFailed) return gameUnitDataMgr.IsLoadCompleted;
            isFailed = true;
            return true;
        });
        if (isFailed)
        {
            OpenRestartPopup();
            yield break;
        }

        var emoticonManager = EightUtil.GetCore<EmoticonManager>();
        emoticonManager.LoadData();
        SetPercent((1f / loadSize) * (currentPercentIndex++));
        yield return new WaitUntil(() =>
        {
            if (!GameDataMgr.IsLoadFailed) return emoticonManager.IsLoad;
            isFailed = true;
            return true;
        });
        if (isFailed)
        {
            OpenRestartPopup();
            yield break;
        }

        var gameSkinMgr = EightUtil.GetCore<GameSkinMgr>();
        gameSkinMgr.LoadData();
        yield return new WaitUntil(() =>
        {
            if (!GameDataMgr.IsLoadFailed) return gameSkinMgr.IsLoadComplete;
            isFailed = true;
            return true;
        });
        if (isFailed)
        {
            OpenRestartPopup();
            yield break;
        }
        gameSkinMgr.UploadAllSkin();
        SetPercent((1f / loadSize) * (currentPercentIndex++));
        yield return new WaitUntil(() =>
        {
            if (!GameDataMgr.IsLoadFailed) return gameSkinMgr.IsAllUpload;
            isFailed = true;
            return true;
        });
        if (isFailed)
        {
            OpenRestartPopup();
            yield break;
        }

        var battleThemeMgr = EightUtil.GetCore<BattleThemeMgr>();
        battleThemeMgr.LoadData();
        SetPercent((1f / loadSize) * (currentPercentIndex++));
        yield return new WaitUntil(() =>
        {
            if (!GameDataMgr.IsLoadFailed) return battleThemeMgr.IsSuccessLoad;
            isFailed = true;
            return true;
        });
        if (isFailed)
        {
            OpenRestartPopup();
            yield break;
        }

        var damageFontMgr = EightUtil.GetCore<GameDamageFontMgr>();
        damageFontMgr.LoadData();
        yield return new WaitUntil(() =>
        {
            if (!GameDataMgr.IsLoadFailed) return damageFontMgr.IsLoadComplete;
            isFailed = true;
            return true;
        });
        if (isFailed)
        {
            OpenRestartPopup();
            yield break;
        }

        EightUtil.GetCore<EightSceneMgr>().ChangeScene(SCENE_TYPE.LOBBY);
        EightUtil.GetCore<EightMsgMgr>().OnEightMsg(EIGHT_MSG.ON_FADE_PAUSE, new EightMsgContent(1));

    }

    private void OpenRestartPopup()
    {
        GamePopupMgr.OpenNormalPopup("경고", "게임의 업데이트를 위해 재시작을 해주세요.",
            new[]
            {
                new NormalPopup.ButtonConfig("재시작", () =>
                {
#if UNITY_ANDROID
                    AndroidJavaObject unity_activity =
                        new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>(
                            "currentActivity");
                    AndroidJavaClass pending_intnet_class = new AndroidJavaClass("android.app.PendingIntent");
                    AndroidJavaObject base_context = unity_activity.Call<AndroidJavaObject>("getBaseContext");
                    AndroidJavaObject intentObj = base_context.Call<AndroidJavaObject>("getPackageManager")
                        .Call<AndroidJavaObject>("getLaunchIntentForPackage",
                            base_context.Call<string>("getPackageName"));
                    AndroidJavaObject context = unity_activity.Call<AndroidJavaObject>("getApplicationContext");
                    AndroidJavaObject pending_intent_obj =
                        pending_intnet_class.CallStatic<AndroidJavaObject>("getActivity", context, 123456, intentObj,
                            pending_intnet_class.GetStatic<int>("FLAG_CANCEL_CURRENT"));
                    AndroidJavaClass alarm_manager = new AndroidJavaClass("android.app.AlarmManager");
                    AndroidJavaClass JavaSystemClass = new AndroidJavaClass("java.lang.System");
                    AndroidJavaObject alarm = unity_activity.Call<AndroidJavaObject>("getSystemService", "alarm");
                    long restart_mill = JavaSystemClass.CallStatic<long>("currentTimeMillis") + 100;
                    alarm.Call("set", alarm_manager.GetStatic<int>("RTC"), restart_mill, pending_intent_obj);
                    JavaSystemClass.CallStatic("exit", 0); 
#else
                    Application.Quit();
#endif
                }),
            }
        );
    }

    private void SetPercent(float percent)
    {
        _progressBarTransform.localScale = new Vector3(percent, 1, 1);
    }
}
