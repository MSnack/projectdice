﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class LoadingPolicyState : LoadingStateBase
{

    private GameDataMgr _dataMgr = null;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = LoadingFSMSystem.LoadingFSMState.POLICY;
    }

    public override void StartState()
    {
        base.StartState();
        StartCoroutine(InitPolicyPopup());
    }

    public override void EndState()
    {
        base.EndState();
    }

    private IEnumerator InitPolicyPopup()
    {
        _dataMgr = EightUtil.GetCore<GameDataMgr>();
        _dataMgr.OnLoadData();
        if (!_dataMgr.IsLoaded)
            yield return new WaitUntil(() => _dataMgr.IsLoaded);
        
        var policyData = _dataMgr.GetPolicyData("policy");
        GamePopupMgr.OpenScrollPopup("이용 약관", policyData.Data, new []
        {
            new NormalPopup.ButtonConfig("거절", () =>
            {
                ScrollPopup.Close();
                SystemMgr.ChangeState(LoadingFSMSystem.LoadingFSMState.LOGIN);
            }, new Color(0.93f, 0.30f, 0.37f)), 
            new NormalPopup.ButtonConfig("동의", () =>
            {
                ScrollPopup.Close();
                SystemMgr.ChangeState(LoadingFSMSystem.LoadingFSMState.NICKNAME);
            }), 
        });
    }
}


