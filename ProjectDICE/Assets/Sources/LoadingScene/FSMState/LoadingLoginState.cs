﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.Events;

public class LoadingLoginState : LoadingStateBase
{

    [SerializeField, ReadOnly]
    private List<string> _tokens = new List<string>();

    [SerializeField, ReadOnly]
    private LoginPlatform _loginPlatform = LoginPlatform.Guest;

    private GameFirebaseMgr _firebaseMgr = null;

    [SerializeField]
    private SoundSelector _bgmSoundSelector = null;

    private bool _isFirstLogin = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    protected override void Initialize()
    {
        State = LoadingFSMSystem.LoadingFSMState.LOGIN;
    }

    public override void StartState()
    {
        _bgmSoundSelector.SetAudioClip();
        _firebaseMgr = EightUtil.GetCore<GameFirebaseMgr>();
        bool isSignUp = InitLoginData();
        if(!isSignUp)
            base.StartState();
    }

    public override void EndState()
    {
        base.EndState();
        GamePopupMgr.CloseLoadingPopup();
    }

    public void ClickGPGS()
    {
        _loginPlatform = LoginPlatform.GPGS;
        GamePopupMgr.OpenLoadingPopup();
        StartCoroutine(LoadGPGS(SuccessLoginFunction));
    }

    public void ClickGuest()
    {
        _loginPlatform = LoginPlatform.Guest;
        GamePopupMgr.OpenNormalPopup("경고", "게스트로 로그인 하는 경우 유저 데이터가 소실 될 수 있습니다.\n계속하시겠습니까?", 
            new [] {
                new NormalPopup.ButtonConfig("취소", NormalPopup.Close),
                new NormalPopup.ButtonConfig("확인", () =>
                {
                    NormalPopup.Close();
                    GamePopupMgr.OpenLoadingPopup();
                    SuccessLoginFunction();
                })
            }
        );
    }

    public void ClickGuestAuto()
    {
        _loginPlatform = LoginPlatform.Guest;
        GamePopupMgr.OpenLoadingPopup();
        SuccessLoginFunction();
    }

    private bool InitLoginData()
    {
        if (PlayerPrefs.HasKey("LoginPlatform"))
        {
            LoginPlatform login = (LoginPlatform) PlayerPrefs.GetInt("LoginPlatform");

            if (login == LoginPlatform.Guest) ClickGuestAuto();
            else if (login == LoginPlatform.GPGS) ClickGPGS();

            else { return false; }

            return true;
        }

        _isFirstLogin = true;
        return false;
    }

    private void SuccessLoginFunction()
    {
        _firebaseMgr.SignIn(_loginPlatform, _tokens);
        StartCoroutine(GetNickname());
        PlayerPrefs.SetInt("LoginPlatform", (int) _loginPlatform);
    }

    private IEnumerator GetNickname()
    {
        var userMgr = EightUtil.GetCore<GameUserMgr>();
        if(!userMgr.IsCompleteLogin)
            yield return new WaitUntil(() => userMgr.IsCompleteLogin);

        if (string.IsNullOrEmpty(userMgr.UserInfo.NickName))
        {
            SystemMgr.ChangeState(LoadingFSMSystem.LoadingFSMState.POLICY);
            yield break;
        }
        SystemMgr.ChangeState(LoadingFSMSystem.LoadingFSMState.COMPLETE);
    }

    private IEnumerator LoadGPGS(UnityAction successEvent = null)
    {
        if(!GPGSUtil.IsLoaded)
            yield return new WaitUntil(() => GPGSUtil.IsLoaded);
        
        GPGSUtil.GetServerAuthCode((token) =>
        {
            _tokens.Add(token);
            successEvent?.Invoke();
        });
    }
}
