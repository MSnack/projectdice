﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New SoundEffectData", menuName = "Data/SoundEffectData", order = 3)]
public class SoundEffectData : ScriptableObject
{
   [SerializeField]
   private int _dataId = -1;
   public int DataId => _dataId;

   [SerializeField]
   private AudioClip _audioClip = null;
   public AudioClip AudioClip => _audioClip;
}
