﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using EightWork;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;

public struct LogData
{
    public string Name;
    public string Data;

    public LogData(string name, object data)
    {
        Name = name;
        Data = data.ToString();
    }
}

public class SendLogUtil : EightWork.Core.EightCore
{
    private DateTime _runningTime;

    private static byte[] _certBytes = null;

    private static string _logID = null;

    public static string LogId
    {
        get
        {
            if (string.IsNullOrEmpty(_logID))
            {
                var time = DateTime.Now;
                _logID = $"T{time:yy.MM.dd.HH.mm.ss.fff}";
            }
            return _logID;
        }
    }

    private static ConcurrentQueue<string> _sendLogQueue = new ConcurrentQueue<string>();

    private static Task _sendlogTask = null;

    public enum TYPE
    {
        NONE,
        DEBUG,
        ERROR,
        ERROR_LIST,
        RUNNING_TIME,
        BATTLE,
    }

    void Start()
    {
        _runningTime = DateTime.Now;
    }

    public override void InitializedCore()
    {
        _sendlogTask = Task.Run(SendLogThead);
        GetCertTextBytes();
        //_certBytes = _certText.bytes;
    }

    private static void GetCertTextBytes()
    {
        var certText = Resources.Load<TextAsset>("SSL/Key");
        if (certText == null)
            return;

        _certBytes = certText.bytes;
    }

    private void OnApplicationQuit()
    {
        SendRunningTimeLog();
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        SendRunningTimeLog(pauseStatus);
    }

    private void SendRunningTimeLog(bool isPaused = false)
    {
        var time = DateTime.Now - _runningTime;
        string str = $"Running Time : {(int)(time.TotalHours)}h {(int)(time.Minutes)}m {time.Seconds}s" +
            (isPaused ? " (OnPaused)" : "");
        SendLog(TYPE.RUNNING_TIME, str);
        if (string.IsNullOrEmpty(ThrowExceptionToPopup.RecentExceptionLog)) return;

        var errorList = ThrowExceptionToPopup.ExceptionLogList;
        SendLog(TYPE.ERROR, ThrowExceptionToPopup.RecentExceptionLog + " ( +" + (errorList.Count - 1) + " errors)");

        string errorListStr = "Error Lists (total " + errorList.Count + " errors)\n\n";

        for (int i = 0; i < errorList.Count; ++i)
            errorListStr += "[" + i + "] " + errorList[i] + "\n";

        SendLog(TYPE.ERROR_LIST, errorListStr);
    }

    private static string GetTypeString(TYPE type)
    {
        return type.ToString();
    }

    public static void SendLog(TYPE type, params LogData[] logDatas)
    {
        var typeStr = GetTypeString(type);

        JObject json = new JObject();
        foreach (var data in logDatas)
        {
            json[data.Name] = data.Data;
        }

        SendLogJson(typeStr, json);
    }

    public static void SendLog(TYPE type, string log)
    {
        var typeStr = GetTypeString(type);

        SendLogVoid(typeStr, log);
    }

    private static async void SendLogThead()
    {
        while (true)
        {
            string log = null;
            if (_sendLogQueue.TryDequeue(out log) == false)
                continue;

            await SendLogData(log);
        }
    }

    private static void SendLogJson(string type, JObject logJson)
    {
        var userId = EightUtil.GetCore<GameUserMgr>().UserInfo.UserID;
        var platform = Application.platform;
        var version = Application.version;
        bool isEditor = platform == RuntimePlatform.WindowsEditor;

        logJson["projectName"] = "P7baaf12568776c9b262_projectRD";
        logJson["projectVersion"] = version;
        logJson["logLevel"] = (isEditor ? "DEBUG" : type);
        logJson["userId"] = userId;
        logJson["logSource"] = LogId;

        var jsonData = JsonConvert.SerializeObject(logJson);
        //var task = Task.Run(() => SendLogData(jsonData));
        _sendLogQueue.Enqueue(jsonData);
    }

    private static async Task SendLogData(string log)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://elsa-col.ncloud.com/_store");
        request.Method = "POST";

        // SSL(TLS) 인증서 세팅하는 부분
        X509Certificate2 certFile = new X509Certificate2(_certBytes);
        request.ClientCertificates.Add(certFile);

        // 콜백함수 등록하는 부분
        System.Net.ServicePointManager.ServerCertificateValidationCallback =
            (sender, certificate, chain, sslPolicyErrors) => { return true; };

        // write param list.
        byte[] byteArray = Encoding.UTF8.GetBytes(log);

        request.ContentType = "application/json";
        request.ContentLength = byteArray.Length;

        Stream dataStream = await request.GetRequestStreamAsync();
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();

        // 응답 받기
        WebResponse res = await request.GetResponseAsync();
        if (res != null)
        {
            Stream s = res.GetResponseStream();
            using (StreamReader sr = new StreamReader(s))
            {
                await s.FlushAsync();
                string responseString = sr.ReadToEnd();
                //Debug.Log("LogRequest : " + responseString + "\n" + log);
            }
            s.Close();
        }
    }

    private static void SendLogVoid(string type, string log)
    {
        var logData = log;
        logData = logData.Replace("\n", "\\n");
        logData = logData.Replace("\"", "\'");
        logData = logData.Replace("/", "\\/");
        logData = logData.Replace("\\", "\\\\");

        var platform = Application.platform;
        var version = Application.version;
        var userId = EightUtil.GetCore<GameUserMgr>().UserInfo.UserID;
        bool isEditor = platform == RuntimePlatform.WindowsEditor;
        string headerData = (isEditor ? "[" + type + "] " : "")
                            + "<id=\\\"" + userId
                            + "\\\", platform=\\\"" + version + "\\\" hasError="
                            + (string.IsNullOrEmpty(ThrowExceptionToPopup.RecentExceptionLog) ? "FALSE" : "TRUE") + "> ";

        string str = "{  \"projectName\": \"P7baaf12568776c9b262_projectRD\",\n" +
                     "\"projectVersion\": \"" + version + "\",\n" +
                     "\"body\": \"" + headerData + logData + "\",\n" +
                     "\"logLevel\": \"" + (isEditor ? "DEBUG" : type) + "\",\n" +
                     "\"logType\": \"" + userId + "\",\n" +
                     "\"logSource\": \"" + LogId + "\" }";

        //var task = Task.Run(() => SendLogData(str));
        _sendLogQueue.Enqueue(str);
    }
}
