﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TimeUtil
{
    public static long TimeInterval = 0;

    public static readonly long TickForUnix = 621355968000000000;

    public static DateTime GetNowTimeUtc(double timeInterval)
    {
        var utcNow = DateTime.UtcNow.ToUniversalTime();
        utcNow.AddSeconds(timeInterval);
        return utcNow;
    }

    public static DateTime GetDateAtUnix(long tick, DateTimeKind kind)
    {
        long ticks = TickForUnix + (tick * TimeSpan.TicksPerMillisecond);
        return new DateTime(ticks, kind);
    }

    public static long GetMillisecondDate(DateTime date)
    {
        return date.Ticks / TimeSpan.TicksPerMillisecond;
    }
}
