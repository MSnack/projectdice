﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class CanvasCameraSetter : EightReceiver
{
    [SerializeField]
    private Canvas _canvas = null;

    [SerializeField]
    private int _planDistance = 5;

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        if (_canvas == null)
            return;

        _canvas.worldCamera = EightUtil.GetCore<EightCameraMgr>().EightCamera;
        _canvas.planeDistance = _planDistance;
    }
}
