﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;
using Object = UnityEngine.Object;

public class SoundSelector : EightReceiver
{
    [SerializeField]
    private AssetReferenceAudioClip _audioClipReference;
    public AssetReferenceAudioClip AudioClipReference
    {
        set => _audioClipReference = value;
    }
    [SerializeField]
    private AudioSource _audioSource = null;
    public AudioSource AudioSource
    {
        get => _audioSource;
        set => _audioSource = value;
    }

    [SerializeField]
    private bool _setAudioSourceAwake;

    [SerializeField]
    private bool _playOnLoaded;

    [SerializeField]
    private float _fadeOutTime = 0.7f;
    public float FadeOutTime
    {
        get => _fadeOutTime;
        set => _fadeOutTime = value;
    }

    private AsyncOperationHandle<AudioClip> _handle;
    private AudioClip _audioClip = null;

    private Coroutine _loadCoroutine = null;

    [SerializeField]
    private bool _dontDestroyOnLoad;
    
    // Start is called before the first frame update
    void Start()
    {
        if(_setAudioSourceAwake) _loadCoroutine = StartCoroutine(SetAudioClipCoroutine());
    }

    protected override void RegisterMsg()
    {
        base.RegisterMsg();
        RegisterEvent(EIGHT_MSG.CHANGE_SCENE_START, DestroySoundSelector);
    }

    protected override void UnRegisterMsg()
    {
        base.UnRegisterMsg();
        RemoveEvent(EIGHT_MSG.CHANGE_SCENE_START, DestroySoundSelector);
    }

    public void SetAudioClip(UnityAction successEvent)
    {
        if (_loadCoroutine != null)
        {
            Debug.LogWarning("Audio Clip has already been created.");
            return;
        }
        
        _loadCoroutine = StartCoroutine(SetAudioClipCoroutine(successEvent));
    }

    public void SetAudioClip(AudioClip audioClip, UnityAction successEvent)
    {
        if (_loadCoroutine != null)
        {
            Debug.LogWarning("Audio Clip has already been created.");
            return;
        }
        _audioClip = audioClip;
        _audioSource.clip = _audioClip;
        successEvent?.Invoke();
    }
    
    public void SetAudioClip()
    {
        SetAudioClip(null);
    }

    public void DestroySoundSelector(EightMsgContent eightMsgContent = new EightMsgContent())
    {
        DontDestroyOnLoad(gameObject);
        if (_dontDestroyOnLoad) return;
        StartCoroutine(DestroyMusic());
    }

    public bool isPlaying()
    {
        return _audioSource?.isPlaying ?? false;
    }

    public void Play()
    {
        _audioSource?.Play();
    }

    public void PlayOneShot(AudioClip clip)
    {
        _audioSource.PlayOneShot(clip);
    }

    private void OnDestroy()
    {
        if(_loadCoroutine != null) StopCoroutine(_loadCoroutine);
        if(_handle.DebugName != "InvalidHandle") Addressables.Release(_handle);
    }

    private IEnumerator SetAudioClipCoroutine(UnityAction successEvent = null)
    {
        _audioSource?.Stop();
        if (_handle.DebugName != "InvalidHandle")
        {
            Addressables.Release(_handle);
        }
        _handle = _audioClipReference.LoadAssetAsync<AudioClip>();
        yield return _handle;
        
        if (_handle.Status != AsyncOperationStatus.Succeeded) {
            yield return new WaitUntil(() => _handle.Status == AsyncOperationStatus.Succeeded);
        }
        
        _audioClip = _handle.Result;
        if(_audioSource != null) _audioSource.clip = _audioClip;
        if(_playOnLoaded) _audioSource?.Play();
        successEvent?.Invoke();
        _loadCoroutine = null;
    }

    private IEnumerator DestroyMusic()
    {
        if(_loadCoroutine != null) StopCoroutine(_loadCoroutine);
        if (_audioSource.isPlaying == false)
        {
            Destroy(gameObject);
            yield break;
        }
        
        float startVol = _audioSource.volume;

        for (float t = 0; t <= _fadeOutTime; t += Time.deltaTime)
        {
            float rt = t / _fadeOutTime;
            _audioSource.volume = Mathf.Lerp(startVol, 0, rt);
            yield return null;
        }
        
        Destroy(gameObject);
    }
}
