﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RouletteController : MonoBehaviour
{
    [SerializeField]
    private BattleWaveController _waveController = null;

    [SerializeField]
    private GridSlideQueue _slideQueue = null;

    [SerializeField]
    private GameObject _baseObject = null;

    private void OnEnable()
    {
        _slideQueue.ClearList();
        for (int i = 0; i < 10; ++i)
        {
            _slideQueue.AddData(1);
        }
        _slideQueue.OnRoulette(2);
    }


}
