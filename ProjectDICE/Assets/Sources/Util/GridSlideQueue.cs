﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridSlideQueue : MonoBehaviour
{
    private struct RouletteData
    {
        public RandomIcon _randomIcon;
        public int _weight;

        public RouletteData(RandomIcon randomIcon, int weight)
        {
            _randomIcon = randomIcon;
            _weight = weight;
        }
    }

    [SerializeField]
    private RandomIcon _copyInstance = null;

    [SerializeField]
    private float _time = 1.2f;
    public float Time => _time;

    [SerializeField]
    private float _power = 10.0f;
    public float Power => _power;

    public float TotalDistance => _time * _power;

    private Queue<RandomIcon> _childQueue = new Queue<RandomIcon>();
    private List<RouletteData> _slideList = new List<RouletteData>();
    private int TotalWeight
    {
        get
        {
            int weight = 0;
            foreach (var data in _slideList)
            {
                weight += data._weight;
            }
            return weight;
        }
    }

    //private void Awake()
    //{
    //    InitData();
    //}

    //public void InitData()
    //{
    //    ClearList();

    //    _childQueue.Clear();
    //    for (int index = 0; index < transform.childCount; ++index)
    //    {
    //        var child = transform.GetChild(index).GetComponent<RandomIcon>();
    //        child.gameObject.SetActive(false);
    //        _childQueue.Enqueue(child);
    //    }
    //}

    public void ClearList()
    {
        foreach (RouletteData data in _slideList)
        {
            data._randomIcon.gameObject.SetActive(false);
            _childQueue.Enqueue(data._randomIcon);
        }
        _slideList.Clear();
    }

    public void AddData(int weight)
    {
        RandomIcon copy = null;
        if (_childQueue.Count <= 0)
            copy = Instantiate(_copyInstance, transform);
        else
            copy = _childQueue.Dequeue();

        RouletteData data = new RouletteData(copy, weight);
        copy.gameObject.SetActive(true);
        copy.UpdateImage(copy.RandomImage());
        _slideList.Add(data);
    }

    public void OnRoulette(int iconIndex)
    {
        int random = Random.Range(0, TotalWeight);
        int currWeight = 0;
        for (int currIndex = 0; currIndex < _slideList.Count; ++currIndex)
        {
            currWeight += _slideList[currIndex]._weight;
            if (currWeight > random)
            {
                RouletteData temp = _slideList[currIndex];
                _slideList[currIndex] = _slideList[0];
                _slideList[0] = temp;

                temp._randomIcon.UpdateImage(iconIndex);
                break;
            }
        }

        Vector2 pos = transform.localPosition;
        Vector2 size = RTransform.sizeDelta;

        float dir = -1.0f;
        int listIndex = 0;
        for (int currIndex = 0; currIndex < _slideList.Count; ++currIndex)
        {
            RouletteData data;
            if (currIndex == 0)
                data = _slideList[currIndex];
            else
            {
                random = Random.Range(currIndex, _slideList.Count);
                data = _slideList[random];

                RouletteData temp = _slideList[currIndex];
                _slideList[currIndex] = _slideList[random];
                _slideList[random] = temp;
            }

            Vector2 currSize = ((RectTransform)(data._randomIcon.transform)).sizeDelta;
            float range = TotalDistance % (currSize.x * _slideList.Count);

            Vector2 currPos = data._randomIcon.transform.localPosition;
            currPos.x = dir * (listIndex * currSize.x) + range;

            if (dir == -1.0f)
            {
                if (currPos.x - currSize.x * 0.5f <= pos.x - size.x * 0.5f)
                {
                    dir = 1.0f;
                    listIndex = 0;
                }
            }

            listIndex++;

            data._randomIcon.transform.localPosition = currPos;
        }

        StartCoroutine(SlideQueue());
    }

    private IEnumerator SlideQueue()
    {
        Vector2 pos = transform.localPosition;
        Vector2 size = gameObject.GetComponent<RectTransform>().sizeDelta;

        float currTime = 0.0f;
        bool isRunTime = true;
        float timeRate = 0.0f;
        while (isRunTime)
        {
            currTime += UnityEngine.Time.deltaTime;
            if (currTime >= _time)
            {
                isRunTime = false;
                timeRate = _time - currTime;
            }

            foreach (RouletteData data in _slideList)
            {
                Vector2 textPos = ((RectTransform)data._randomIcon.transform).localPosition;
                Vector2 textSize = ((RectTransform)data._randomIcon.transform).sizeDelta;
                if (textPos.x + textSize.x * 0.5f <= pos.x - size.x * 0.5f)
                {
                    textPos.x += _slideList.Count * textSize.x;
                }

                textPos.x -= _power * (UnityEngine.Time.deltaTime + timeRate);

                float dist = Mathf.Abs(pos.x - textPos.x);
                float length = size.x * 0.5f + textSize.x * 0.5f;
                if (dist >= length)
                    dist = length;

                float alpha = 1.0f - (dist / length);


                data._randomIcon.transform.localPosition = textPos;
            }

            yield return null;
        }
    }

    private Rect GetRect(RectTransform rectTransform)
    {
        var pos = transform.localPosition;
        var scale = transform.localScale;
        var size = RTransform.sizeDelta;

        var minAnchor = RTransform.anchorMin;
        var maxAnchor = RTransform.anchorMax;

        var minPos = (Vector2)pos - (minAnchor * (size * scale));
        var maxPos = (Vector2)pos + (minAnchor * (size * scale));

        return Rect.MinMaxRect(minPos.x, minPos.y, maxPos.x, maxPos.y);
    }

    private RectTransform RTransform => (RectTransform)transform;
}
