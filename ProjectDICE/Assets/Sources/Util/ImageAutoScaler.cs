﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ImageAutoScaler : MonoBehaviour
{
    [SerializeField]
    private Image _image;

    [SerializeField]
    private Vector2 _maxScale = new Vector2(100, 100);

    public Sprite sprite
    {
        get => _image.sprite;
        set => _image.sprite = value;
    }

    private void Awake()
    {
        if (_image == null) _image = GetComponent<Image>();
    }

    // Start is called before the first frame update
    private void Start()
    {
    }

    private void Reset()
    {
        _image = GetComponent<Image>();
    }

    public void SetNativeRatioSize()
    {
        _image.SetNativeSize();
        RectTransform rectTransform = (RectTransform) transform;
        Vector2 rect = rectTransform.sizeDelta;

        if (rect.x <= _maxScale.x && rect.y <= _maxScale.y)
            return;

        Vector2 ratio = Vector2.zero;
        ratio = rect.x > rect.y ? new Vector2(1, rect.y / rect.x) : new Vector2(rect.x / rect.y, 1);
        float maxScale = rect.x > rect.y ? _maxScale.x : _maxScale.y;

        rectTransform.sizeDelta = ratio * maxScale;

    }
}