﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

public class SceneLoadUtil : MonoBehaviour
{
    [SerializeField, SerializeReference]
    private GameObject[] _loadInterfaces;

    [SerializeField]
    private UnityEvent _loadedEvent = null;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadObjects());
    }

    private IEnumerator LoadObjects()
    {
        for (int i = 0; i < _loadInterfaces.Length; ++i)
        {
            var interfaceObject = _loadInterfaces[i].GetComponent(typeof(LoadInterface)) as LoadInterface;
            if (interfaceObject == null) continue;
            if (interfaceObject.IsLoaded() == false)
            {
                yield return new WaitUntil(() => interfaceObject.IsLoaded());
            }
        }
        EightUtil.GetCore<EightMsgMgr>().OnEightMsg(EIGHT_MSG.ON_FADE_PAUSE, new EightMsgContent(0));
        _loadedEvent?.Invoke();
    }
}
