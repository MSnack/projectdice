﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class SoundEffectPlayer : MonoBehaviour
{
    [SerializeField]
    private int _dataId = -1;

    private GameSoundMgr _gameSoundMgr = null;
    private EightAssetMgr _assetMgr = null;

    private AudioClip _clip = null;

    private void Awake()
    {
        _gameSoundMgr = EightUtil.GetCore<GameSoundMgr>();
        _clip = _gameSoundMgr.GetAudioClip(_dataId);
    }

    public void Play()
    {
        if (_gameSoundMgr == null) _gameSoundMgr = EightUtil.GetCore<GameSoundMgr>();
        if (_assetMgr == null) _assetMgr = EightUtil.GetCore<EightAssetMgr>();

        if (!_assetMgr.IsDownloadComplete)
        {
            StartCoroutine(AsyncPlay());
            return;
        }
        _gameSoundMgr.PlaySoundEffect(_clip);
    }

    private IEnumerator AsyncPlay()
    {
        if(!_assetMgr.IsDownloadComplete) 
            yield return new WaitUntil(() => _assetMgr.IsDownloadComplete);
        
        _gameSoundMgr.PlaySoundEffect(_dataId);
    }
}
