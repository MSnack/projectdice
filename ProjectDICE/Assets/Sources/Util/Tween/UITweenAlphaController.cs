﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightWork.UI
{
	[RequireComponent(typeof(UIAlphaController))]
	public class UITweenAlphaController : TweenGenericFloat
	{

		[SerializeField, ReadOnly]
		private UIAlphaController _controller = null;

		// Use this for initialization
		void Start ()
		{
			
		}

		protected override void SetValue(float value)
		{
			if (_controller == null) GetController();
			_controller.Alpha = value;
		}

		protected override float GetValue()
		{
			if (_controller == null) GetController();
			return _controller.Alpha;
		}

		private void GetController()
		{
			_controller = GetComponent<UIAlphaController>();
		}

		public void InitAlpha()
		{
			Init();
		}
	}

}
