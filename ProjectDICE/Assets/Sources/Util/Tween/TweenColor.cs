﻿using UnityEngine;
using UnityEngine.UI;

namespace EightWork.UI
{
    public class TweenColor : TweenGenericColor
    {
        protected override void SetValue(Color value)
        {
            _target.GetComponent<Image>().color = value;
        }

        protected override Color GetValue()
        {
            return _target.GetComponent<Image>().color;

        }
    }
}