﻿using UnityEngine;
using UnityEngine.UI;

namespace EightWork.UI
{
    public class TweenSpriteRenderer : TweenGenericColor
    {
        protected override void SetValue(Color value)
        {
            _target.GetComponent<SpriteRenderer>().color = value;
        }

        protected override Color GetValue()
        {
            return _target.GetComponent<SpriteRenderer>().color;
        }
    }
}