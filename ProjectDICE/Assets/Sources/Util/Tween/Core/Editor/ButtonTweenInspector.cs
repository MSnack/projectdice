﻿using UnityEditor;
using UnityEditor.Events;
using UnityEngine;

namespace EightWork.UI
{
    [CustomEditor(typeof(ButtonTween))]
    public class ButtonTweenInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var buttonEvent = Selection.activeGameObject.GetComponent<ButtonTween>();

            GUILayout.BeginHorizontal();

            TweenScale scale = buttonEvent.GetComponent<TweenScale>();
            if (scale == null)
            {
                if (GUILayout.Button("Scale"))
                {
                    var tween = buttonEvent.gameObject.AddComponent<TweenScale>();
                    tween.isPlayOnStart = false;
                    tween.To = new Vector3(0.1f, 0.1f, 0);
                    tween.Duration = 0.2f;
                    tween.Curve = AnimationCurve.EaseInOut(0, 0, 1, 1);

                    UnityEventTools.AddPersistentListener(buttonEvent.EventOnPointerDown, tween.PlayForward);
                    UnityEventTools.AddPersistentListener(buttonEvent.EventOnPointerUp, tween.PlayReverse);
                }
            }
            else
            {
                if (GUILayout.Button("ScaleDestroy"))
                {
                    DestroyImmediate(scale);
                }
            }

            TweenRotation rotation = buttonEvent.GetComponent<TweenRotation>();
            if (rotation == null)
            {
                if (GUILayout.Button("Rotation"))
                {
                    var tween = buttonEvent.gameObject.AddComponent<TweenRotation>();
                    tween.isPlayOnStart = false;
                    tween.To = new Vector3(1, 1, 1);

                    UnityEventTools.AddPersistentListener(buttonEvent.EventOnPointerDown, tween.PlayForward);
                    UnityEventTools.AddPersistentListener(buttonEvent.EventOnPointerUp, tween.PlayReverse);
                }
            }
            else
            {
                if (GUILayout.Button("RotationDestroy"))
                    DestroyImmediate(rotation);
            }

            TweenPosition position = buttonEvent.GetComponent<TweenPosition>();
            if (position == null)
            {
                if (GUILayout.Button("Position"))
                {
                    var tween = buttonEvent.gameObject.AddComponent<TweenPosition>();
                    tween.isPlayOnStart = false;
                    tween.To = new Vector3(1, 1, 1);

                    UnityEventTools.AddPersistentListener(buttonEvent.EventOnPointerDown, tween.PlayForward);
                    UnityEventTools.AddPersistentListener(buttonEvent.EventOnPointerUp, tween.PlayReverse);
                }
            }
            else
            {
                if (GUILayout.Button("PositionDestroy"))
                    DestroyImmediate(position);
            }

            UITweenColorAlpha colorAlpha = buttonEvent.GetComponent<UITweenColorAlpha>();
            if (colorAlpha == null)
            {
                if (GUILayout.Button("ColorAlpha"))
                {
                    var tween = buttonEvent.gameObject.AddComponent<UITweenColorAlpha>();
                    tween.isPlayOnStart = false;

                    UnityEventTools.AddPersistentListener(buttonEvent.EventOnPointerDown, tween.PlayForward);
                    UnityEventTools.AddPersistentListener(buttonEvent.EventOnPointerUp, tween.PlayReverse);
                }
            }
            else
            {
                if (GUILayout.Button("ColorAlphaDestroy"))
                    DestroyImmediate(colorAlpha);
            }

            TweenColor color = buttonEvent.GetComponent<TweenColor>();
            if (color == null)
            {
                if (GUILayout.Button("Color"))
                {
                    var tween = buttonEvent.gameObject.AddComponent<TweenColor>();
                    tween.isPlayOnStart = false;
                    tween.From = tween.To = Color.white;

                    UnityEventTools.AddPersistentListener(buttonEvent.EventOnPointerDown, tween.PlayForward);
                    UnityEventTools.AddPersistentListener(buttonEvent.EventOnPointerUp, tween.PlayReverse);
                }
            }
            else
            {
                if (GUILayout.Button("ColorDestroy"))
                    DestroyImmediate(color);
            }

            GUILayout.EndHorizontal();
        }
    }
}