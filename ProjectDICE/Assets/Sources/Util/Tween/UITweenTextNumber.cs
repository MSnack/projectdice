﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using EightWork.UI;
using UnityEngine;
using UnityEngine.UI;

public class UITweenTextNumber : TweenGenericFloat
{
    [SerializeField, ReadOnly]
    private float _value;

    [SerializeField]
    private Text _text = null;

    [SerializeField]
    private string _format = "{0}";

    [SerializeField]
    private bool _enablePlusCharacter = false;

    public string text
    {
        get => _text.text;
        set => _text.text = value;
    }

    protected override void SetValue(float value)
    {
        _value = value;
        if (_text != null)
        {
            _text.text = string.Format(_format, value);
            if (_enablePlusCharacter) _text.text = (value > 0 ? "+" : "") + _text.text;
        }
    }

    protected override float GetValue()
    {
        return _value;
    }
}
