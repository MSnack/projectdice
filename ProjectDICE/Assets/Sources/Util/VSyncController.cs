﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VSyncType
{
    Off = 0,
    On = 1,
}

public class VSyncController : MonoBehaviour
{
    [SerializeField]
    private VSyncType _syncType = VSyncType.Off;
    public VSyncType SyncType { get => _syncType; set => SetSync(value); }

    [SerializeField]
    private int _frameRate = 60;
    public int FrameRate { get => _frameRate; set => SetFrame(value); }

    private void Awake()
    {
        SetSync(SyncType);
        SetFrame(FrameRate);
    }

    public void SetSync(VSyncType type)
    {
        _syncType = type;
        QualitySettings.vSyncCount = (int)type;
    }

    public void SetFrame(int frame)
    {
        _frameRate = frame;
        Application.targetFrameRate = frame;
    }
}
