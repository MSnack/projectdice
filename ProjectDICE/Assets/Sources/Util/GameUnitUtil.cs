﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUnitUtil
{
    private static string _settingPath = "Core/CoreSetting/UnitSetting";

    private static UnitSetting _unitSettingAsset = null;
    public static UnitSetting UnitSettingAsset => _unitSettingAsset;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void OnInitializedUtil()
    {
        var setting = Resources.Load<UnitSetting>(_settingPath);
        if (setting == null)
            Debug.Log("Dont Have UnitSetting At " + _settingPath);
        else
            Debug.Log("Success UnitSetting");

        _unitSettingAsset = setting;
    }
}
