﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterCardUtil : MonoBehaviour
{

    private static readonly Dictionary<int, UserCardDetail> _cards = new Dictionary<int, UserCardDetail>();
    public static UserCardDetail[] CardList => _cards.Values.ToArray();
    public static int UserCardLength => _cards.Count;

    public static void RegisterUserCardDetail(int characterId, int level, int cardCount)
    {
        var userCard = new UserCardDetail() 
            {CharacterId = characterId, Level = level, CardCount = cardCount};
        
        RegisterUserCardDetail(userCard);
    }

    public static void RegisterUserCardDetail(CharacterCardInfo info)
    {
        RegisterUserCardDetail(info.CharacterId, info.Level, info.CardCount);
    }

    public static void ClearUserCardDetails()
    {
        _cards.Clear();
    }

    public static void RegisterUserCardDetail(UserCardDetail userCardDetail)
    {
        if (_cards.ContainsKey(userCardDetail.CharacterId))
        {
            _cards[userCardDetail.CharacterId] = userCardDetail;
            return;
        }
        
        _cards.Add(userCardDetail.CharacterId, userCardDetail);
    }

    public static UserCardDetail GetCardDetail(int characterId)
    {
        if(_cards.ContainsKey(characterId))
            return _cards[characterId];
        
        // Debug.LogAssertion("CharacterCardUtil : Currently the user does not have a [" + characterId + "] key.");
        return null;
    }

    public static List<KeyValuePair<int, UserCardDetail>> GetAllCardDetails()
    {
        return _cards.ToList();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public class UserCardDetail
    {
        public int CharacterId = 0;
        public int Level = 0;
        public int CardCount = 0;

        public CharacterCardInfo ToCharacterCardInfo()
        {
            var info = new CharacterCardInfo()
            {
                CardCount = this.CardCount,
                CharacterId = this.CharacterId,
                Level = this.Level,
            };

            return info;
        }
    }
}
