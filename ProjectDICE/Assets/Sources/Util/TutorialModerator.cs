﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class TutorialModerator : MonoBehaviour
{
    [SerializeField]
    private Button _activeButton;

    private GameTutorialMgr _tutorialMgr = null;

    [SerializeField]
    private bool _moveNextPage = true;

    [SerializeField]
    private short _changePageIndex = 0;

    [SerializeField]
    private bool _forceClose = false;

    [SerializeField]
    private string _tutorialName = "";
    
    // Start is called before the first frame update
    void Start()
    {
        _activeButton?.onClick.AddListener(InvokeEvent);
    }

    public void InvokeEvent()
    {
        if (_tutorialMgr == null)
            _tutorialMgr = EightUtil.GetCore<GameTutorialMgr>();
        if (_tutorialMgr.IsTutorialActive == false) return;
        if (string.IsNullOrEmpty(_tutorialName) == false &&
            _tutorialMgr.TutorialName.Equals(_tutorialName) == false)
            return;
        if (_forceClose)
        {
            _tutorialMgr.ForceClosePage();
            return;
        }
        
        if(_moveNextPage) _tutorialMgr.NextPage();
        else _tutorialMgr.ChangePage(_changePageIndex);
    }
}
