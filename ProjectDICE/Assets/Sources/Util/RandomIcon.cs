﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomIcon : MonoBehaviour
{
    [SerializeField]
    private Image _image = null;

    [SerializeField]
    private List<Sprite> _iconList = new List<Sprite>();
    public int IconCount => _iconList.Count;

    public int RandomImage()
    {
        var count = _iconList.Count;
        if (count <= 0)
            return -1;

        var index = Random.Range(0, count);
        return index;
    }

    public void UpdateImage(int index)
    {
        if (_iconList.Count <= 0 || _iconList.Count <= index || index < 0)
            return;

        _image.sprite = _iconList[index];
    }
}
