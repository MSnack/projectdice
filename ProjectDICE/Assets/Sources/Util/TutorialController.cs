﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;

public class TutorialController : MonoBehaviour
{
    [SerializeField]
    private AssetReferenceGameObject _tutorialPrefab = null;
    public AssetReferenceGameObject TutorialPrefab => _tutorialPrefab;

    public string TutorialName => _tutorialPrefab.SubObjectName;

    [SerializeField]
    private int _targetQuestId = 5001;

    [SerializeField]
    private QuestCondition _condition = QuestCondition.CompleteKnightsTutorial;
    public QuestCondition Condition => _condition;

    [SerializeField]
    private bool _generatingWhenStart = false;

    [SerializeField]
    private UnityEvent _startTutorialEvent = null;

    [SerializeField]
    private UnityEvent _completeEvent = null;
    
    [SerializeField]
    private UnityEvent _failEvent = null;

    private QuestInfo _questInfo = null;
    private GameTutorialMgr _tutorialMgr = null;

    // Start is called before the first frame update
    void Start()
    {
        if (_generatingWhenStart)
        {
            Generate();
        }
    }

    public void Generate()
    {
#if !Bot_System
        if (_questInfo != null) return;
        var questList = QuestUtil.GetQuestData(QuestCategory.TutorialQuest);
        var questInfo = questList.Find((x) => x.QuestId == _targetQuestId);
        var questLocalData = QuestUtil.GetQuestLocalData(_targetQuestId);
        if (questInfo == null || questInfo.Value >= questLocalData.TargetValue) return;

        if (_tutorialMgr == null)
            _tutorialMgr = EightUtil.GetCore<GameTutorialMgr>();
        if (_tutorialMgr.IsTutorialActive) return;
        
        _tutorialMgr.LoadTutorial(this);
        _questInfo = questInfo;
        StartCoroutine(WaitTutorialPanel((short) questInfo.Value));
        _startTutorialEvent?.Invoke();
#endif
    }

    public void InvokeCompleteEvent()
    {
        QuestUtil.UpdateQuestEvent(_condition, 99);
        QuestServerConnector.CompleteTutorial(_condition, 99, true, () => _completeEvent?.Invoke());
    }

    public void InvokeFailedEvent()
    {
        QuestUtil.UpdateQuestEvent(_condition, 1);
        QuestServerConnector.CompleteTutorial(_condition, 1, true, () => _failEvent?.Invoke());
    }

    public void ReceiveQuestRewardEvent()
    {
        QuestServerConnector.ReceiveQuestReward(QuestCategory.TutorialQuest, _targetQuestId - 5000, (mailId) =>
        {
            var questLocalData = QuestUtil.GetQuestLocalData(_targetQuestId);
            LobbyMailServerConnector.ReceiveMailItem(mailId, 
                (trans) =>
                {
                    MailSlotObject.ReceivedMailEvent(trans, questLocalData.ItemCategory);
                    
                    LobbyMailServerConnector.GetMails(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Mails));
                    _questInfo.IsReceived = true;
                    QuestUtil.OnNotice(QuestCategory.TutorialQuest, false);
                    LobbyRefreshUtil.OnRefresh(RefreshState.QuestValue);
                    
                    var info = new RewardItemInfo
                        {ItemCategory = (int) questLocalData.ItemCategory, Id = questLocalData.ItemId, Count = questLocalData.ItemCount};

                    GamePopupMgr.CloseLoadingPopup();
                    if(questLocalData.ItemCategory != ItemCategory.BOX)
                        GamePopupMgr.OpenRewardPopup(new List<RewardItemInfo> {info});
                });
        }, () =>
        {
            GamePopupMgr.OpenNormalPopup("경고", "튜토리얼 진행 중 문제가 생겼습니다.\n게임을 재시작 해주세요.");
            GamePopupMgr.CloseLoadingPopup();
        });
    }

    private void Reset()
    {
        _completeEvent = new UnityEvent();
        _completeEvent.AddListener(ReceiveQuestRewardEvent);
    }

    private IEnumerator WaitTutorialPanel(short page)
    {
        if (_tutorialMgr.IsTutorialActive == false)
        {
            yield return new WaitUntil(() => _tutorialMgr.IsTutorialActive);
        }
        _tutorialMgr.ChangePage(page);
    }
}
