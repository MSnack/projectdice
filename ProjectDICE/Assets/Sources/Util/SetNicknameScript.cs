﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class SetNicknameScript : MonoBehaviour
{
    [SerializeField]
    private Text _targetText = null;
    // Start is called before the first frame update
    void Start()
    {
        var userMgr = EightUtil.GetCore<GameUserMgr>();

        _targetText.text = string.Format(_targetText.text, userMgr.UserInfo.NickName);
    }
    
    
}
