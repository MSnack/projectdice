﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BackButtonUtil : MonoBehaviour
{

    [SerializeField]
    private UnityEvent _onBackButton = null;
    
#if UNITY_ANDROID
    void Update()
    {
        if (GamePopupMgr.IsPopupOpen()) return;
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _onBackButton?.Invoke();
        }
    }
#endif
}
