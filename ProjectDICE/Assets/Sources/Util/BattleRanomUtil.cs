﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleRandomUtil : MonoBehaviour
{
    private static List<float> _values = new List<float>();
    private static int _index = -1;
    public static int CurrIndex => _index;

    public static void InitRandom(float[] values)
    {
        ClearValues();
        AddValues(values);
    }

    public static void AddValues(float[] values)
    {
        _values.AddRange(values);
    }

    public static void ClearValues()
    {
        _values.Clear();
        _index = -1;
    }

    public static float GetNextValue()
    {
        if (_values.Count <= 0)
        {
            Debug.Log("Not Hvae Values Return Random.Range");
            return Random.Range(0.0f, 1.0f);
        }

        if (_index >= _values.Count - 1)
            _index = -1;

        _index += 1;
        //Debug.Log("RandomIndex : " + _index.ToString());

        var value = _values[_index];
        Debug.Log("RandomUtil Index : " + _index.ToString() + " " + "value : " + value.ToString());

        return value;
        //return Random.Range(0.0f, 1.0f);
    }
}
