﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class URLLoader : MonoBehaviour
{
    [SerializeField]
    private string _url = "";

    public string URL
    {
        get => _url;
        set => _url = value;
    }

    public void OpenURL()
    {
        Application.OpenURL(_url);
    }
}
