﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseApplicationUtil : MonoBehaviour
{
    private bool _isQuitPopup = false;
    public void Quit()
    {
        _isQuitPopup = true;
        GamePopupMgr.OpenNormalPopup("게임 종료", "게임을 종료하시겠습니까?", new []
        {
            new NormalPopup.ButtonConfig("취소", () =>
            {
                NormalPopup.Close();
                _isQuitPopup = false;
            }),
            new NormalPopup.ButtonConfig("종료", Application.Quit, new Color(1, 0.1f, 0.1f))
        });
    }

    private void Update()
    {
        if (_isQuitPopup)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
    }
}
