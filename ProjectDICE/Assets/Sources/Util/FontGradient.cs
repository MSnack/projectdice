﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
 
[AddComponentMenu("UI/Effects/Gradient")]
public class FontGradient : BaseMeshEffect
{
    public Gradient GradientColor;
    public override void ModifyMesh(VertexHelper vh)
    {
        if (!IsActive()) return;

        var count = vh.currentVertCount;
        if (count == 0) return;

        var vertices = new List<UIVertex>();
        for (var i = 0; i < count; ++i)
        {
            var vertex = new UIVertex();
            vh.PopulateUIVertex(ref vertex, i);
            vertices.Add(vertex);
        }

        var topY = vertices[0].position.y;
        var bottomY = vertices[0].position.y;


        for (var i = 1; i < count; ++i)
        {
            var y = vertices[i].position.y;
            if (y > topY) topY = y;
            else if (y < bottomY) bottomY = y;
        }

        var height = topY - bottomY;
        for (var i = 0; i < count; ++i)
        {
            var vertex = vertices[i];
            var color = GradientColor.Evaluate((vertex.position.y - bottomY) / height);

            vertex.color = color;
            vh.SetUIVertex(vertex, i);
        }
    }
    
    public void SetDirty()
    {
        graphic.SetVerticesDirty();
    }
    
    public static Gradient Lerp(Gradient a, Gradient b, float t, bool noAlpha, bool noColor) {
        //list of all the unique key times
        var keysTimes = new List<float>();
 
        if (!noColor) {
            for (int i = 0; i < a.colorKeys.Length; ++i) {
                float k = a.colorKeys[i].time;
                if (!keysTimes.Contains(k))
                    keysTimes.Add(k);
            }
 
            for (int i = 0; i < b.colorKeys.Length; ++i) {
                float k = b.colorKeys[i].time;
                if (!keysTimes.Contains(k))
                    keysTimes.Add(k);
            }
        }
 
        if (!noAlpha) {
            for (int i = 0; i < a.alphaKeys.Length; ++i) {
                float k = a.alphaKeys[i].time;
                if (!keysTimes.Contains(k))
                    keysTimes.Add(k);
            }
 
            for (int i = 0; i < b.alphaKeys.Length; ++i) {
                float k = b.alphaKeys[i].time;
                if (!keysTimes.Contains(k))
                    keysTimes.Add(k);
            }
        }
 
        var clrs = new GradientColorKey[keysTimes.Count];
        var alphas = new GradientAlphaKey[keysTimes.Count];
 
        //Pick colors of both gradients at key times and lerp them
        for (int i = 0; i < keysTimes.Count; ++i) {
            float key = keysTimes[i];
            var clr = Color.Lerp(a.Evaluate(key), b.Evaluate(key), t);
            clrs[i] = new GradientColorKey(clr, key);
            alphas[i] = new GradientAlphaKey(clr.a, key);
        }
 
        var g = new Gradient();
        g.SetKeys(clrs, alphas);
 
        return g;
    }
}
