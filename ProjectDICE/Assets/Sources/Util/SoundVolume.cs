﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public enum SoundCategory
{
    NONE,
    BGM,
    EFFECTS,
}

public class SoundVolume : EightReceiver
{
    [SerializeField]
    private SoundCategory _soundCategory = SoundCategory.BGM;
    public SoundCategory SoundCategory
    {
        get => _soundCategory;
        set => _soundCategory = value;
    }

    [SerializeField]
    private AudioSource _audioSource = null;
    public AudioSource AudioSource
    {
        set => _audioSource = value;
    }

    private GameSoundMgr _soundMgr = null;

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        _soundMgr = EightUtil.GetCore<GameSoundMgr>();
        SetSoundVolume();
    }

    private void OnDestroy()
    {
        if (_soundMgr == null) return;
        _soundMgr.SetBGMVolumeEvent -= ChangeBGMVolumeEvent;
    }

    public void SetSoundVolume()
    {
        if(_soundMgr == null) _soundMgr = EightUtil.GetCore<GameSoundMgr>();
        if (_soundCategory == SoundCategory.BGM)
        {
            _audioSource.volume = _soundMgr.BGMVolume;
            _soundMgr.SetBGMVolumeEvent += ChangeBGMVolumeEvent;
        }

        else if (_soundCategory == SoundCategory.EFFECTS)
        {
            _audioSource.volume = _soundMgr.EffectVolume;
        }
    }

    private void ChangeBGMVolumeEvent(float value)
    {
        _audioSource.volume = value;
    }
}
