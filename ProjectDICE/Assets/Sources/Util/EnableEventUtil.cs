﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnableEventUtil : MonoBehaviour
{

    [SerializeField]
    private UnityEvent _onEnableEvent = null;

    private void OnEnable()
    {
        _onEnableEvent?.Invoke();
    }
}
