﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(fileName = "New IllustData", menuName = "Data/IllustData", order = 3)]
public class IllustData : ScriptableObject
{
    [SerializeField]
    private int _dataId;
    public int DataId => _dataId;

    [SerializeField]
    private int _reserveCount = 1;
    public int ReserveCount => _reserveCount;

    [SerializeField]
    private AssetReferenceGameObject _illustAsset = null;
    public AssetReferenceGameObject IllustAsset => _illustAsset;

    [SerializeField]
    private AssetReferenceSprite _previewIllustSpriteAsset = null;
    public AssetReferenceSprite PreviewIlustSpriteAsset => _previewIllustSpriteAsset;
}
