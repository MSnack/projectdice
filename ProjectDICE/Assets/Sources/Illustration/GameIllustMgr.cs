﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;

public class GameIllustMgr : EightWork.Core.EightCore
{
    [System.Serializable]
    private class IllustInfo
    {
        private static GameIllustMgr _illustMgr = null;
        private static GameIllustMgr IllustMgr => GetIllustMgr();

        private static GameIllustMgr GetIllustMgr()
        {
            if (_illustMgr == null)
            {
                _illustMgr = EightUtil.GetCore<GameIllustMgr>();
            }

            return _illustMgr;
        }

        private IllustData _illustAsset = null;

        private Sprite _previewSprite = null;
        public Sprite PreviewSprite => _previewSprite;

        private GameIllustObject _illustPrefab = null;
        private Queue<GameIllustObject> _reserveQueue = new Queue<GameIllustObject>();
        public bool IsEmptyQueue => _reserveQueue.Count <= 0;

        private bool _isLoaded = false;
        public bool IsLoaded => _isLoaded;

        public IllustInfo(IllustData data)
        {
            _illustAsset = data;
        }

        private bool ReserveQueue()
        {
            if (_isLoaded == false)
                return false;

            var reserveCount = _illustAsset.ReserveCount;
            for (int i = 0; i < reserveCount; ++i)
            {
                var copy = Instantiate(_illustPrefab);
                if (copy == null)
                    return false;

                copy.UpdateIllustData(_illustAsset);
                copy.gameObject.SetActive(false);
                copy.transform.SetParent(IllustMgr.transform);
                _reserveQueue.Enqueue(copy);
            }

            return true;
        }

        public void UploadIllust(UnityAction successEvent = null)
        {
            if (_isLoaded == true)
                return;

            _illustAsset.PreviewIlustSpriteAsset.LoadAssetAsync().Completed += (result) =>
            {
                _previewSprite = result.Result;
                successEvent?.Invoke();
            };
            
            _isLoaded = true;
            return;

            _illustAsset.IllustAsset.LoadAssetAsync<GameObject>().Completed += (result) =>
            {
                var illustObj = result.Result;
                if (illustObj == null)
                {
                    return;
                }

                var component = illustObj.GetComponent<GameIllustObject>();
                if (component == null)
                    return;

                _illustPrefab = component;
                _isLoaded = true;
            };
        }

        public GameIllustObject GetIllustObject()
        {
            if (_isLoaded == false)
                return null;

            if (IsEmptyQueue == true)
            {
                if (ReserveQueue() == false)
                    return null;
            }

            return _reserveQueue.Dequeue();
        }

        public void ReturnIllust(GameIllustObject illust)
        {
            if (illust == null || illust.DataId != _illustAsset.DataId)
                return;

            _reserveQueue.Enqueue(illust);
        }

        public void Release()
        {
            if (_isLoaded == false)
                return;

            //Clear();

            //Addressables.Release(_illustPrefab);
            // Addressables.Release(_previewSprite);
            _illustAsset.PreviewIlustSpriteAsset.ReleaseAsset();

            _isLoaded = false;
        }

        public void Clear()
        {
            var list = _reserveQueue.ToArray();
            _reserveQueue.Clear();

            for (int i = list.Length - 1; i >= list.Length; --i)
            {
                Destroy(list[i]);
            }
        }
    }

    [SerializeField]
    private AssetLabelReference _lable = null;

    private Dictionary<int, IllustInfo> _dataPool = new Dictionary<int, IllustInfo>();

    [SerializeField, ReadOnly]
    private bool _isLoaded = false;
    public bool IsLoaded => _isLoaded;

    public override void InitializedCore()
    {
        LoadData();
    }

    public void LoadData()
    {
        if (_isLoaded == true)
            return;

        var loadHandle = Addressables.LoadAssetsAsync<IllustData>(_lable.labelString, null);
        loadHandle.Completed += (result) =>
        {
            var illustDatas = result.Result;
            if (illustDatas == null)
            {
                Debug.Log("Fail Load IllustDatas");
                return;
            }

            foreach (var data in illustDatas)
            {
                _dataPool.Add(data.DataId, new IllustInfo(data));
            }

            _isLoaded = true;
        };
    }

    private IllustInfo GetIllustInfo(int dataId)
    {
        if (_dataPool.ContainsKey(dataId) == false)
            return null;

        return _dataPool[dataId];
    }

    public Sprite GetIllustSprite(int dataId, UnityAction<Sprite> successEvent)
    {
        var info = GetIllustInfo(dataId);
        if (info == null)
            return null;

        if (info.IsLoaded == false)
        {
            info.UploadIllust(() => successEvent?.Invoke(info.PreviewSprite));

            return null;
        }

        successEvent?.Invoke(info.PreviewSprite);
        return info.PreviewSprite;
    }

    public void ReleaseIllustSprite(int dataId)
    {
        var info = GetIllustInfo(dataId);
        if (info == null)
            return;

        if (info.IsLoaded == false) return;
        
        info.Release();
    }

    public GameIllustObject GetIllust(int dataId)
    {
        var info = GetIllustInfo(dataId);
        if (info == null)
            return null;

        var illust = info.GetIllustObject();
        if (illust != null)
        {
            illust.gameObject.SetActive(true);
        }

        return illust;
    }

    public void ReturnIllust(GameIllustObject illust)
    {
        if (illust == null)
            return;

        var info = GetIllustInfo(illust.DataId);
        if (info == null)
            return;

        illust.transform.SetParent(this.transform);
        illust.gameObject.SetActive(false);
        illust.transform.position = Vector3.zero;

        info.ReturnIllust(illust);
    }
}
