﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class GameIllustObject : MonoBehaviour
{
    [SerializeField, ReadOnly]
    private int _dataId = -1;
    public int DataId => _dataId;

    public void UpdateIllustData(IllustData data)
    {
        if (data == null)
            return;

        _dataId = data.DataId;
    }
}
