﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(fileName = "New UnitSetting", menuName = "Setting/UnitSetting", order = 2)]
public class UnitSetting : ScriptableObject
{
    [System.Serializable]
    private class UnitStateData : SerializableDictionaryBase<BuffType, GameEffectData> { }

    [SerializeField]
    private UnitStateData _stateData = null;

    public GameEffectData GetEffectData(BuffType type)
    {
        if (_stateData.ContainsKey(type) == false)
            return null;

        return _stateData[type];
    }
}
