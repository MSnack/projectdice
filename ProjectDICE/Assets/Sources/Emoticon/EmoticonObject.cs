﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EmoticonObject : MonoBehaviour
{
    private int _dataId = -1;
    public int DataID => _dataId;

    public event UnityAction OnEndEmoticonEvent = null;

    [SerializeField]
    private int _sortingOrder = 0;
    public int SortingOrder { get => _sortingOrder; set => UpdateOrder(value); }

    public void UpdateEmoticonInfo(EmoticonData data)
    {
        if (data == null)
            return;

        _dataId = data.DataID;
    }

    private void OnEndEmoticon()
    {
        OnEndEmoticonEvent?.Invoke();
        OnEndEmoticonEvent = null;
    }

    [ContextMenu("UpdateOrderLayer")]
    private void UpdateOrderLayer()
    {
        UpdateOrder(_sortingOrder);
    }

    private void UpdateOrder(int order)
    {
        _sortingOrder = order;

        var spriteRenderers = GetComponentsInChildren<SpriteRenderer>(true);
        foreach (var spriteRenderer in spriteRenderers)
        {
            spriteRenderer.sortingOrder = order++;
        }
    }
}
