﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class NetworkEmoticon : MonoBehaviour
{
    [SerializeField]
    private BattleSocket _socket = null;

    [SerializeField]
    private Animator _animator = null;

    [SerializeField]
    private GameObject _emoticonSlot = null;

    [SerializeField]
    private GameObject _banButton = null;

    public bool IsBan { get => _isBan; set => _isBan = value; }
    private bool _isBan = false;

    private void Awake()
    {
        OnIdle();
    }

    private void OnEnable()
    {
        _socket.EmoticonEvent += OnEmoticonEvent;
    }

    private void OnDisable()
    {
        _socket.EmoticonEvent -= OnEmoticonEvent;
    }

    public void ChangeState(EmoticonButtonType type)
    {
        _animator.SetInteger("State", (int)type);
    }

    private void OnEmoticonEvent(RequestEmoticonData data)
    {
        if (data.ReceiveType == SocketDataType.Local)
            return;

        if (_isBan == true)
            return;

        var manager = EightUtil.GetCore<EmoticonManager>();
        var emoticon = manager.GetEmoticon(data.EmoticonId);
        if (emoticon == null)
            return;

        emoticon.transform.position = _emoticonSlot.transform.position;
        emoticon.SortingOrder = 30000;
        emoticon.OnEndEmoticonEvent += () =>
        {
            _banButton.SetActive(true);
            manager.ReturnEmoticon(emoticon);
            BackOff();
        };
        _banButton.SetActive(false);
        BackOn();
    }

    private void OnIdle()
    {
        ChangeState(EmoticonButtonType.Idle);
    }

    private void BackOn()
    {
        ChangeState(EmoticonButtonType.BackOn);
    }

    private void BackOff()
    {
        ChangeState(EmoticonButtonType.BackOff);
    }
}
