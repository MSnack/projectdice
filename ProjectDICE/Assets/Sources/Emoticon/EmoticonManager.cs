﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.AddressableAssets;

public class EmoticonManager : EightWork.Core.EightCore
{
    [SerializeField]
    private AssetLabelReference _lable = null;

    private Dictionary<int, EmoticonData> _dataPool = new Dictionary<int, EmoticonData>();

    private Dictionary<int, Queue<EmoticonObject>> _reservePool = new Dictionary<int, Queue<EmoticonObject>>();
    private List<EmoticonObject> _usePool = new List<EmoticonObject>();

    [SerializeField]
    private int _reserveCount = 5;
    public int ReserveCount { get => _reserveCount; set => _reserveCount = value; }

    [SerializeField, ReadOnly]
    private bool _isLoad = false;
    public bool IsLoad => _isLoad;

    public override void InitializedCore()
    {
        //LoadData();
    }

    public void LoadData()
    {
        if (_isLoad == true)
            return;

        var loadHandle = Addressables.LoadAssetsAsync<EmoticonData>(_lable.labelString, null);
        loadHandle.Completed += (result) =>
        {
            var emoticonDatas = result.Result;
            if (emoticonDatas == null)
            {
                Debug.Log("Fail Load EmoticonData");
                GameDataMgr.IsLoadFailed = true;
                return;
            }

            foreach (var emotionData in emoticonDatas)
            {
                int dataId = emotionData.DataID;
                _dataPool[dataId] = emotionData;
            }

            _isLoad = true;
        };
    }

    private bool ReserveEmoticon(int id)
    {
        if (_dataPool.ContainsKey(id) == false)
            return false;

        if (_reservePool.ContainsKey(id) == false)
            _reservePool[id] = new Queue<EmoticonObject>();

        for (int i = 0; i < _reserveCount; ++i)
        {
            var data = _dataPool[id];
            if (data.ObjectData == null)
                return false;

            var emoticonObject = data.ObjectData.GetComponent<EmoticonObject>();
            if (emoticonObject == null)
                return false;

            var copy = Instantiate(emoticonObject, transform);

            copy.UpdateEmoticonInfo(data);
            copy.transform.position = Vector3.zero;
            copy.gameObject.SetActive(false);

            _reservePool[id].Enqueue(copy);
        }

        return true;
    }

    public Sprite GetEmoticonIcon(int id)
    {
        if (_dataPool.ContainsKey(id) == false)
            return null;

        return _dataPool[id].EmoticonIcon;
    }

    public EmoticonObject GetEmoticon(int id)
    {
        if (_reservePool.ContainsKey(id) == false ||
            _reservePool[id].Count <= 0)
        {
            if (ReserveEmoticon(id) == false)
                return null;
        }

        var emoticon = _reservePool[id].Dequeue();
        emoticon.gameObject.SetActive(true);
        _usePool.Add(emoticon);

        return emoticon;
    }

    public EmoticonData GetEmoticonData(int id)
    {
        if (_dataPool.ContainsKey(id) == false)
            return null;

        return _dataPool[id];
    }

    public int GetEmoticonShopID(int id)
    {
        if (_dataPool.ContainsKey(id) == false)
            return -1;

        return _dataPool[id].ShopIdData;
    }

    public void ReturnEmoticon(EmoticonObject emoticon)
    {
        if (emoticon == null)
            return;

        emoticon.transform.SetParent(this.transform);
        emoticon.transform.position = Vector3.zero;
        emoticon.gameObject.SetActive(false);

        _usePool.Remove(emoticon);
        _reservePool[emoticon.DataID].Enqueue(emoticon);
    }

    public void ReturnAllEmoticon()
    {
        for (int i = _usePool.Count - 1; i >= 0; --i)
        {
            ReturnEmoticon(_usePool[i]);
        }
        _usePool.Clear();
    }

    public void Clear()
    {
        ReturnAllEmoticon();

        foreach (var queue in _reservePool.Values)
        {
            var list = queue.ToArray();
            queue.Clear();

            for (int i = list.Length - 1; i >= 0; --i)
            {
                Destroy(list[i]);
            }
        }
    }

    public void Release()
    {
        Clear();

        _dataPool.Clear();
        _isLoad = false;
    }

    public List<int> GetEmoticonIDs()
    {
        return new List<int>(_dataPool.Keys);
    }
}
