﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public enum EmoticonButtonType
{
    Idle,
    PickOn,
    PickOff,
    BackOn,
    BackOff,
}

public class EmoticonButtonController : MonoBehaviour
{
    [SerializeField]
    private Animator _animator = null;

    [SerializeField, ReadOnly]
    private EmoticonButtonType _stateType = EmoticonButtonType.Idle;
    public EmoticonButtonType StateType => _stateType;

    [SerializeField]
    private BattleSocket _socket = null;

    [SerializeField]
    private GameObject _blockPanel = null;

    [SerializeField]
    private GameObject _emoticonSlot = null;

    [SerializeField]
    private GameObject _emotionGrid = null;

    [SerializeField]
    private EmoticonIconSlot _emotionSlot = null;

    private List<EmoticonIconSlot> _slotList = new List<EmoticonIconSlot>();
    private List<int> _emoticonIdList = new List<int>();

    private void Awake()
    {
        OnIdle();
        _blockPanel.SetActive(false);
    }

    private void OnEnable()
    {
        _socket.EmoticonEvent += OnEmoticonEvent;

        EmoticonServerConnector.GetEmoticonDeck(() =>
        {
            OnInitEmotion();
        });

    }

    private void OnInitEmotion()
    {
        if (_slotList.Count > 0) // 임시
            return;

        string jstr = EmoticonUtil.GetEmoticonDeckData().ToJson();
        JObject jobj = JObject.Parse(jstr);
        for (int i = 0; i < 8; ++i)
        {
            string label = "deck" + (i + 1);
            int emoticonId = int.Parse(jobj[label].ToString());
            if (emoticonId > 0)
            {
                EmoticonIconSlot slot = Instantiate(_emotionSlot, _emotionGrid.transform);
                slot.GetComponent<Button>().onClick.AddListener(() => ClickSlot(slot));
                _slotList.Add(slot);
                _emoticonIdList.Add(emoticonId);
            }
        }

        FlexRect();
    }

    private void FlexRect()
    {
        GridLayoutGroup grid = _emotionGrid.GetComponent<GridLayoutGroup>();
        if (grid == null)
            return;

        int countY = grid.constraintCount;
        float unitX = grid.cellSize.x;
        float UnitY = grid.cellSize.y;
        float calculate = (float)_slotList.Count / (float)countY;
        float width = Mathf.Ceil(calculate) * unitX + grid.padding.left + grid.padding.right;
        float height = UnitY * countY + grid.padding.top + grid.padding.bottom;

        if (calculate < 1.0f)
            height = (float)_slotList.Count * UnitY + grid.padding.top + grid.padding.bottom;

        if(height <= grid.padding.top + grid.padding.bottom)
        {
            width = unitX;
            height = UnitY;
        }

        grid.GetComponent<RectTransform>().sizeDelta = new Vector2(width,height);
    }

    private void OnDisable()
    {
        _socket.EmoticonEvent -= OnEmoticonEvent;
        _slotList.Clear();
        _emoticonIdList.Clear();
    }

    private void OnEmoticonEvent(RequestEmoticonData data)
    {
        if (data.ReceiveType != SocketDataType.Local)
            return;

        var manager = EightUtil.GetCore<EmoticonManager>();
        var emoticon = manager.GetEmoticon(data.EmoticonId);
        if (emoticon == null)
            return;

        emoticon.transform.position = _emoticonSlot.transform.position;
        emoticon.SortingOrder = 30000;
        emoticon.OnEndEmoticonEvent += () =>
        {
            manager.ReturnEmoticon(emoticon);
            BackOff();
        };

        BackOn();
    }

    private bool _useSlot = false;
    public void ClickSlot(EmoticonIconSlot slot)
    {
        if (_useSlot == false)
            return;

        _socket.EmitEmoticon(slot.EmoticonId);
        _useSlot = false;
    }

    public void ChangeState(EmoticonButtonType type)
    {
        _animator.SetInteger("State", (int)type);
        _stateType = type;
    }

    private void OnResetIdle()
    {
        ChangeState(EmoticonButtonType.Idle);
    }

    public void OnIdle()
    {
        ChangeState(EmoticonButtonType.Idle);

        _emoticonSlot.SetActive(false);
        SlotActive(false);
    }

    public void PickOn()
    {
        UpdateSlotList();
        SlotActive(true);
        _emoticonSlot.SetActive(false);
        _blockPanel?.gameObject.SetActive(true);
        ChangeState(EmoticonButtonType.PickOn);
        _useSlot = true;
        FlexRect();

    }

    public void PickOff()
    {
        ChangeState(EmoticonButtonType.PickOff);
        _blockPanel?.gameObject.SetActive(false);
        _emoticonSlot.SetActive(false);
        SlotActive(false);
    }

    public void BackOn()
    {
        ChangeState(EmoticonButtonType.BackOn);
        _emoticonSlot.SetActive(true);
        SlotActive(false);
        _blockPanel?.gameObject.SetActive(false);
    }

    public void BackOff()
    {
        ChangeState(EmoticonButtonType.BackOff);
    }

    private void SlotActive(bool active)
    {
        foreach (var slot in _slotList)
        {
            slot.gameObject.SetActive(active);
        }
    }

    private void UpdateSlotList()
    {
        // var manager = EightUtil.GetCore<EmoticonManager>();

        int index = 0;
        foreach(var Id in _emoticonIdList)
        {
            _slotList[index].UpdateIcon(Id);
            ++index;
        }
    }
}
