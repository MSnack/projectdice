﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.UI;

public class EmoticonIconSlot : MonoBehaviour
{
    [SerializeField, ReadOnly]
    private int _emoticonId = -1;
    public int EmoticonId => _emoticonId;

    [SerializeField]
    private Image _image = null;

    public void UpdateIcon(int id)
    {
        var icon = EightUtil.GetCore<EmoticonManager>().GetEmoticonIcon(id);
        _image.sprite = icon;
        _emoticonId = id;
    }
}
