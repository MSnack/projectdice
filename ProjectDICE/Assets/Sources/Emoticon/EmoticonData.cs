﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(fileName = "New EmoticonData", menuName = "Data/EmoticonData", order = 3)]
public class EmoticonData : ScriptableObject
{
    [SerializeField]
    private int _dataId = -1;
    public int DataID => _dataId;

    [SerializeField]
    private Sprite _emoticonIcon = null;
    public Sprite EmoticonIcon => _emoticonIcon;

    [SerializeField]
    private GameObject _objectData = null;
    public GameObject ObjectData => _objectData;

    [SerializeField]
    private int _shopIdData = -1;
    public int ShopIdData => _shopIdData;
}
