﻿using System.Collections;
using System.Collections.Generic;
using EightWork.FSM;
using UnityEngine;

public class LobbyFSMSystem : EightFSMSystem<LobbyFSMSystem.LobbyFSMState, LobbyStateBase, LobbySceneInterface>
{
    public enum LobbyFSMState
    {
        SHOP,
        ORGANIZATION,
        BATTLE,
        QUEST,
        GUILD,
        ORGANIZATION_EDIT,
        EMOTICON,
        EMOTICON_EDIT,
        TIER = 100,
        MEMBERSHIP = 101,
        
        PREV_STATE = -1000,
        NONE = -9999,
    }

    private LobbyFSMState _prevState = (LobbyFSMState) (-1001);
    public LobbyFSMState CurrSubState = LobbyFSMState.BATTLE;

    protected override void Awake()
    {
        base.Awake();
        
        SetupComplete();
    }

    // Start is called before the first frame update
    void Start()
    {
        LobbySceneInterface.CallChangingState += ChangeStateFunction;
    }


    private void ChangeStateFunction(LobbyFSMState state)
    {
        if (state == LobbyFSMState.PREV_STATE)
        {
            ChangeState(_prevState);
            SetCurrSubState();
            return;
        }
        if (CurrState == state) return;
        
        _prevState = CurrState;
        ChangeState(state);
        SetCurrSubState();
    }

    private void SetCurrSubState()
    {
        CurrSubState = StatePool[CurrState].SubState;
    }

    public bool IsBottomMenuState()
    {
        return StatePool[CurrState].IsBottomMenuState;
    }

    public void RefreshStateObjects()
    {
        StatePool[CurrState].RefreshObjects();
    }
    
}
