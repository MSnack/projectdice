﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EightWork;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LobbyEmoticonState : LobbyStateBase
{
    [Header("Emoticon State")]
    [SerializeField]
    private ContentSizeFitter[] _contentSizeFitter;

    [SerializeField]
    private EmoticonSlotObject _slotPrefab = null;
    
    [SerializeField]
    private List<EmoticonSlotObject> _deckObjects = new List<EmoticonSlotObject>();

    [SerializeField]
    private GameObject _unlockContentObject = null;
    [SerializeField]
    private GameObject _lockContentObject = null;
    private List<EmoticonSlotObject> _unlockSlotObjects = new List<EmoticonSlotObject>();
    private List<EmoticonSlotObject> _lockSlotObjects = new List<EmoticonSlotObject>();

    private Coroutine _deckUICoroutine;
    private Coroutine _slotUICoroutine;

    [SerializeField]
    private Canvas[] _inactiveCanvases;
    
    [SerializeField]
    private Transform[] _inactiveTransform;
    private Vector3[] _startTransformPositions;

    private bool _isNeedRefresh = false;

    protected override void Initialize()
    {
        State = LobbyFSMSystem.LobbyFSMState.EMOTICON;
    }

    protected override void Start()
    {
        base.Start();
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.Emoticon, RefreshEmoticons);
    }

    private void RefreshEmoticons()
    {
        _isNeedRefresh = true;
        if (enabled == false) return;
        StartState();
    }

    public override void StartState()
    {
        GamePopupMgr.OpenLoadingPopup();
        Init(_isNeedRefresh);
        _isNeedRefresh = false;
        StartCoroutine(WaitForInit(() =>
        {
            base.StartState();

            bool isNeedToSetStartTransform = false;
            if (_startTransformPositions == null)
            {
                _startTransformPositions = new Vector3[_inactiveTransform.Length];
                isNeedToSetStartTransform = true;
            }

            for (int i = 0; i < _inactiveTransform.Length; ++i)
            {
                var trans = _inactiveTransform[i];
                if(isNeedToSetStartTransform)
                    _startTransformPositions[i] = trans.localPosition;
                trans.localPosition = new Vector3(9999, 9999, 9999);
            }
            foreach (var canvas in _inactiveCanvases)
            {
                canvas.enabled = false;
            }
            GamePopupMgr.CloseLoadingPopup();
        }));
    }

    public override void EndState()
    {
        base.EndState();
        for (int i = 0; i < _inactiveTransform.Length; ++i)
        {
            var trans = _inactiveTransform[i];
            trans.localPosition =  _startTransformPositions[i];
        }
        foreach (var canvas in _inactiveCanvases)
        {
            canvas.enabled = true;
        }

        _startTransformPositions = null;
        if(_deckUICoroutine != null) StopCoroutine(_deckUICoroutine);
        if(_slotUICoroutine != null) StopCoroutine(_slotUICoroutine);
    }

    private void Init(bool forceUpdate = false)
    {
        if (!forceUpdate && _isLoaded) return;

        _isLoaded = false;
        ReleaseAllSlots();
        _deckUICoroutine = StartCoroutine(InitDeck());
        _slotUICoroutine = StartCoroutine(InitUnlockSlot());
        StartCoroutine(RefreshFitter());
    }

    private IEnumerator WaitForInit(UnityAction successEvent)
    {
        if(!_isLoaded)
            yield return new WaitUntil(() => _isLoaded);
        successEvent?.Invoke();
    }

    private IEnumerator InitDeck()
    {
        if (!EmoticonServerConnector.IsLoaded)
        {
            yield return new WaitUntil(() => EmoticonServerConnector.IsLoaded);
        }
        
        EmoticonServerConnector.GetEmoticonDeck(() =>
        {
            var info = EmoticonUtil.GetEmoticonDeckData();
            int[] deckId =
            {
                info.Deck1, info.Deck2, info.Deck3, info.Deck4,
                info.Deck5, info.Deck6, info.Deck7, info.Deck8,
            };
            for (int i = 0; i < _deckObjects.Count; ++i)
            {
                var deckObject = _deckObjects[i];
                deckObject.InitData(deckId[i]);
            }

            _deckUICoroutine = null;
        });
    }

    private IEnumerator InitUnlockSlot()
    {
        if (!EmoticonUtil.IsLoaded)
        {
            yield return new WaitUntil(() => EmoticonUtil.IsLoaded);
        }
        EmoticonManager mgr = EightUtil.GetCore<EmoticonManager>();
        var emoList = EmoticonUtil.GetEmoticonData();
        var emoListToInt = new List<int>();

        foreach (var id in emoList)
        {
            var slotObj = Instantiate(_slotPrefab, _unlockContentObject.transform);
            slotObj.InitData(id._emoticonId);
            emoListToInt.Add(id._emoticonId);
            _unlockSlotObjects.Add(slotObj);
        }

        var lockedEmoList = mgr.GetEmoticonIDs().Except(emoListToInt);
        foreach (var id in lockedEmoList)
        {
            var slotObj = Instantiate(_slotPrefab, _lockContentObject.transform);
            slotObj.InitData(id);
            slotObj.ChangeState(EmoticonSlotObject.State.Locked);
            _lockSlotObjects.Add(slotObj);
        }

        _slotUICoroutine = null;
    }

    private IEnumerator RefreshFitter()
    {
        if (_slotUICoroutine != null)
        {
            yield return new WaitUntil(() => _slotUICoroutine == null);
        }
        
        foreach (var fitter in _contentSizeFitter)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform) fitter.transform);
        }

        _isLoaded = true;
    }

    public static void UpdateDeckInfo(EmoticonDeckInfo info)
    {
        EmoticonServerConnector.UpdateEmoticonDeck(info, () =>
        {
            EmoticonUtil.RegisterEmoticonDeckData(info);
            LobbyRefreshUtil.OnRefresh(RefreshState.Emoticon);
        });
    }

    private void ReleaseAllSlots()
    {
        foreach (var slotObject in _lockSlotObjects)
        {
            Destroy(slotObject.gameObject);
        }
        _lockSlotObjects.Clear();
        
        foreach (var slotObject in _unlockSlotObjects)
        {
            Destroy(slotObject.gameObject);
        }
        _unlockSlotObjects.Clear();
    }
}
