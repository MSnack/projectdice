﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyStaticShopState : LobbyStateBase
{
    [SerializeField]
    private List<LobbyShopPackageObject> _shopPackages = new List<LobbyShopPackageObject>();

    [SerializeField]
    private GameObject _contentObject = null;
    
    [SerializeField]
    private ScrollRect _scrollView = null;
    private Coroutine _scrollCoroutine = null;
    
    [SerializeField]
    private float _scrollTotalTime = 0.5f;
    [SerializeField]
    private AnimationCurve _scrollCurve = AnimationCurve.Linear(0, 0, 1, 1);

    protected override void Start()
    {
        base.Start();
        for (int i = 0; i < _contentObject.transform.childCount; ++i)
        {
            var child = _contentObject.transform.GetChild(i).GetComponent<LobbyShopPackageObject>();
            if (child == null) continue;
            _shopPackages.Add(child);
        }
    }

    protected override void Initialize()
    {
        State = LobbyFSMSystem.LobbyFSMState.SHOP;
    }

    public override void StartState()
    {
        base.StartState();
    }

    public override void EndState()
    {
        base.EndState();
    }

    public void ViewShopPackage(int id)
    {
        if (id >= _shopPackages.Count) return;
        float targetY = ((RectTransform)_shopPackages[id].transform).rect.height 
                        / ((RectTransform)_contentObject.transform).rect.height;
        ForceMoving(targetY);
    }
    
    private void ForceMoving(float targetY)
    {
        if(_scrollCoroutine != null) StopCoroutine(_scrollCoroutine);
        _scrollCoroutine = StartCoroutine(OnScrollAnimation(targetY));
    }

    private IEnumerator OnScrollAnimation(float targetPosition)
    {
        float currPosition = _scrollView.verticalNormalizedPosition;
        float elapsedTime = 0;
        targetPosition = Math.Min(1, targetPosition);

        for (; elapsedTime < _scrollTotalTime; elapsedTime += Time.deltaTime)
        {
            if(Input.GetMouseButtonDown(0)) yield break;
            float t = _scrollCurve.Evaluate(elapsedTime / _scrollTotalTime);
            float v = Mathf.Lerp(currPosition, targetPosition, t);
            _scrollView.verticalNormalizedPosition = v;
            yield return null;
        }

        _scrollView.verticalNormalizedPosition = targetPosition;
        _scrollCoroutine = null;
    }
}
