﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class LobbyMembershipState : LobbyStateBase
{

    [SerializeField]
    private GameObject _contentObject = null;

    [SerializeField]
    private MembershipSlotObject _slotPrefab = null;
    
    private readonly List<MembershipSlotObject> _slotObjects = new List<MembershipSlotObject>();
    
    private bool _isUpgraded = false;
    public bool IsUpgraded => _isUpgraded;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = LobbyFSMSystem.LobbyFSMState.MEMBERSHIP;
    }

    public override void StartState()
    {
        if (!_isLoaded)
        {
            GamePopupMgr.OpenLoadingPopup();
            StartCoroutine(Init(() =>
            {
                base.StartState();
                GamePopupMgr.CloseLoadingPopup();
            }));
        }
        else
        {
            base.StartState();
        }
    }

    public void Reload()
    {
        Reload(null);
    }

    public void Reload(UnityAction successEvent)
    {
        _isLoaded = false;
        GamePopupMgr.OpenLoadingPopup();
        StartCoroutine(Init(() =>
        {
            base.StartState();
            successEvent?.Invoke();
            GamePopupMgr.CloseLoadingPopup();
        }));
    }

    public void ReceiveAll()
    {
        foreach (var slotObject in _slotObjects)
        {
            slotObject.CurrentStateObject.InvokeEvent();
        }
    }

    private IEnumerator Init(UnityAction successEvent)
    {
        if(!QuestServerConnector.IsLoaded || !BattleInfoServerConnector.IsLoaded)
            yield return new WaitUntil(() => QuestServerConnector.IsLoaded && BattleInfoServerConnector.IsLoaded);

        ReleaseSlots();
        
        var list = QuestUtil.GetQuestData(QuestCategory.MembershipQuest);
        var infos = QuestUtil.GetQuestData(QuestCategory.MembershipQuest);
        int level = infos[infos.Count / 2 - 1].Value;

        {
            var firstMembership = infos[infos.Count / 2];
            var localData = QuestUtil.GetQuestLocalData(firstMembership.QuestId);
            _isUpgraded = firstMembership.Value >= localData.TargetValue;
        }

        //퀘스트 양쪽 두개씩 있기 때문에 실제 얻어오는 횟수는 절반으로 줄임
        int size = list.Count / 2;
        for (int i = 0; i < size; ++i)
        {
            var questInfos = new[] { list[i], list[i + size] };
            var nextQuestLevel = (i + 1 < size) ? QuestUtil.GetQuestLocalData(list[i + 1].QuestId).TargetValue : -1;
            var obj = Instantiate(_slotPrefab, _contentObject.transform);
            var questLocalData = new[]
            {
                QuestUtil.GetQuestLocalData(questInfos[0].QuestId),
                QuestUtil.GetQuestLocalData(questInfos[1].QuestId)
            };
            var targetLevel = questLocalData[0].TargetValue;
            MembershipSlotObject.State state = MembershipSlotObject.State.NONE;
            if (level >= targetLevel)
            {
                state = level >= nextQuestLevel ?
                    MembershipSlotObject.State.OVER_ACTIVE : MembershipSlotObject.State.ACTIVE;
            }
            
            MembershipInfo info = new MembershipInfo
            {
                Level = targetLevel,
                IsUpgraded = _isUpgraded,
                LeftInfo = questInfos[0],
                LeftData = questLocalData[0],
                RightInfo = questInfos[1],
                RightData = questLocalData[1],
                State = state,
            };

            obj.InitData(info);
            _slotObjects.Add(obj);
        }
        
        _isLoaded = true;
        successEvent?.Invoke();
    }

    private void ReleaseSlots()
    {
        foreach (var slotObject in _slotObjects)
        {
            Destroy(slotObject.gameObject);
        }
        _slotObjects.Clear();
    }
}
