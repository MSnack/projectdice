﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

public class LobbyEmoticonEditState : LobbyStateBase
{
    
    private static UnityAction<int> _getEmoticonData;
    public static void SetEmoticonData(int id)
    {
        _getEmoticonData.Invoke(id);
    }
    
        
    [SerializeField, ReadOnly()]
    private int _emoticonId = -1;

    [SerializeField]
    private LobbyEmoticonEditUI _editUi = null;
    
    [SerializeField]
    private Canvas[] _inactiveCanvases;
    
    [SerializeField]
    private Transform[] _inactiveTransform;
    private Vector3[] _startTransformPositions;
    
    protected override void Awake()
    {
        base.Awake();
        _getEmoticonData = null;
        _getEmoticonData += GetEmoticonInfo;
    }

    public override void StartState()
    {
        base.StartState();
        _startTransformPositions = new Vector3[_inactiveTransform.Length];
        
        for (int i = 0; i < _inactiveTransform.Length; ++i)
        {
            var trans = _inactiveTransform[i];
            _startTransformPositions[i] = trans.localPosition;
            trans.localPosition = new Vector3(9999, 9999, 9999);
        }
        foreach (var canvas in _inactiveCanvases)
        {
            canvas.enabled = false;
        }
    }

    public override void EndState()
    {
        base.EndState();
        for (int i = 0; i < _inactiveTransform.Length; ++i)
        {
            var trans = _inactiveTransform[i];
            trans.localPosition =  _startTransformPositions[i];
        }
        foreach (var canvas in _inactiveCanvases)
        {
            canvas.enabled = true;
        }
    }

    protected override void Initialize()
    {
        State = LobbyFSMSystem.LobbyFSMState.EMOTICON_EDIT;
    }
    
    private void GetEmoticonInfo(int id)
    {
        _emoticonId = id;
        _editUi.SetData(id);
    }
    
    public void ChangeEmoticon(int deckId)
    {
        var deck = EmoticonUtil.GetEmoticonDeckData();

        if (deckId == 1) deck.Deck1 = _emoticonId;
        if (deckId == 2) deck.Deck2 = _emoticonId;
        if (deckId == 3) deck.Deck3 = _emoticonId;
        if (deckId == 4) deck.Deck4 = _emoticonId;
        if (deckId == 5) deck.Deck5 = _emoticonId;
        if (deckId == 6) deck.Deck6 = _emoticonId;
        if (deckId == 7) deck.Deck7 = _emoticonId;
        if (deckId == 8) deck.Deck8 = _emoticonId;
        
        LobbyEmoticonState.UpdateDeckInfo(deck);
    }
}
