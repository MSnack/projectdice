﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

public class LobbyCardInfoState : LobbyPopupStateBase
{

    private static UnityAction<CharacterUnitData> _getCharacterUnitData;

    public static void SetCharacterUnitData(CharacterUnitData data)
    {
        _getCharacterUnitData.Invoke(data);
    }

    [SerializeField, ReadOnly()]
    private CharacterUnitData _unitData = null;

    [SerializeField]
    private LobbyCardInfoUI _cardInfoUi = null;

    [SerializeField]
    private Transform[] _inactiveTransform;
    private Vector3[] _startTransformPositions;

    [SerializeField]
    private Canvas[] _inactiveCanvases;

    // Start is called before the first frame update
    void Awake()
    {
        _getCharacterUnitData = null;
        _getCharacterUnitData += GetCardInfo;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        _getCharacterUnitData = null;
    }

    protected override void Initialize()
    {
        State = LobbyPopupFSMSystem.LobbyPopupFSMState.CARD_INFO;
    }

    private void GetCardInfo(CharacterUnitData data)
    {
        _unitData = data;
        _cardInfoUi.SetData(data);
        _cardInfoUi.SetDetailData();
    }

    public CharacterUnitData GetCardInfo()
    {
        return _unitData;
    }
    
    public override void StartState()
    {
        base.StartState();
        
        _startTransformPositions = new Vector3[_inactiveTransform.Length];

        for (int i = 0; i < _inactiveTransform.Length; ++i)
        {
            var trans = _inactiveTransform[i];
            _startTransformPositions[i] = trans.localPosition;
            
            trans.localPosition = new Vector3(9999, 9999, 9999);
        }
        
        foreach (var canvas in _inactiveCanvases)
        {
            canvas.enabled = false;
        }
    }

    public override void EndState()
    {
        base.EndState();
        for (int i = 0; i < _inactiveTransform.Length; ++i)
        {
            var trans = _inactiveTransform[i];
            
            trans.localPosition =  _startTransformPositions[i];
        }
        
        foreach (var canvas in _inactiveCanvases)
        {
            canvas.enabled = true;
        }
    }
}
