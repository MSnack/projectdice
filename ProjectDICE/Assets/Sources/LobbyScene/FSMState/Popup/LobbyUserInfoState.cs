﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyUserInfoState : LobbyPopupStateBase
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void Initialize()
    {
        State = LobbyPopupFSMSystem.LobbyPopupFSMState.USER_INFO;
    }
}
