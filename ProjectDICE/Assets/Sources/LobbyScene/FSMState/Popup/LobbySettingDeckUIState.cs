﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

public class LobbySettingDeckUIState : LobbyPopupStateBase
{
    
    private static UnityAction<CharacterUnitData> _getCharacterUnitData;
    public static void SetCharacterUnitData(CharacterUnitData data)
    {
        _getCharacterUnitData.Invoke(data);
    }

    [SerializeField, ReadOnly()]
    private CharacterUnitData _unitData = null;

    [SerializeField]
    private LobbySettingDeckUI _settingDeckUi = null;


    protected override void Awake()
    {
        base.Awake();
        _getCharacterUnitData = null;
        _getCharacterUnitData += GetCardInfo;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void Initialize()
    {
        State = LobbyPopupFSMSystem.LobbyPopupFSMState.SETTING_DECK;
    }
    
    private void GetCardInfo(CharacterUnitData data)
    {
        _unitData = data;
        _settingDeckUi.SetData(data);
    }
}
