﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

public class LobbySettingState : LobbyPopupStateBase
{

    [SerializeField]
    private GameObject _providePlayGames = null;

    [SerializeField]
    private GameObject _provideFacebook = null;
    
    private GameFirebaseMgr _firebaseMgr = null;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = LobbyPopupFSMSystem.LobbyPopupFSMState.SETTINGS;
    }

    public override void StartState()
    {
        base.StartState();
        _firebaseMgr = EightUtil.GetCore<GameFirebaseMgr>();
        RefreshProvide();
    }

    public override void EndState()
    {
        base.EndState();
    }

    private void RefreshProvide()
    {
        foreach (var info in _firebaseMgr.CurrentUser.ProviderData)
        {
            var provide = info.ProviderId;
            if (provide == "playgames.google.com")
            {
                _providePlayGames.SetActive(true);
            }

            if (provide == "facebook.com")
            {
                _provideFacebook.SetActive(true);
            }
        }
    }

    public void LinkGPGS()
    {
        GamePopupMgr.OpenLoadingPopup();
        StartCoroutine(LoadGPGS((token) =>
        {
            List<string> tokens = new List<string> {token};
            _firebaseMgr.LinkIn(LoginPlatform.GPGS, tokens,
                () =>
                {
                    GamePopupMgr.OpenNormalPopup("경고", "이미 가입된 계정입니다.");
                    GamePopupMgr.CloseLoadingPopup();
                }, () =>
                {
                    RefreshProvide();
                    PlayerPrefs.SetInt("LoginPlatform", (int) LoginPlatform.GPGS);
                    GamePopupMgr.CloseLoadingPopup();
                });
        }));
    }

    private IEnumerator LoadGPGS(UnityAction<string> successEvent)
    {
        if(!GPGSUtil.IsLoaded)
            yield return new WaitUntil(() => GPGSUtil.IsLoaded);
        
        GPGSUtil.GetServerAuthCode((token) =>
        {
            successEvent?.Invoke(token);
        });
    }
}
