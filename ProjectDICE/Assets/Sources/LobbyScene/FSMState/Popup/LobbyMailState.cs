﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyMailState : LobbyPopupStateBase
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = LobbyPopupFSMSystem.LobbyPopupFSMState.MAILBOX;
    }

    public override void StartState()
    {
        base.StartState();
    }

    public override void EndState()
    {
        base.EndState();
    }
}
