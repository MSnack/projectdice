﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyPopupNoneState : LobbyPopupStateBase
{
    [SerializeField]
    private LobbyFSMSystem _lobbyFsmSystem = null;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void StartState()
    {
        base.StartState();
        _lobbyFsmSystem.RefreshStateObjects();
    }

    protected override void Initialize()
    {
        State = LobbyPopupFSMSystem.LobbyPopupFSMState.NONE;
    }
}
