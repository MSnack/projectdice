﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyBattleHistoryState : LobbyPopupStateBase
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = LobbyPopupFSMSystem.LobbyPopupFSMState.BATTLE_HISTORY;
    }

    public override void StartState()
    {
        base.StartState();
    }

    public override void EndState()
    {
        base.EndState();
    }
}
