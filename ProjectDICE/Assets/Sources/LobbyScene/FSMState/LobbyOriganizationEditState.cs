﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

public class LobbyOriganizationEditState : LobbyStateBase
{
    
    private static UnityAction<CharacterUnitData> _getCharacterUnitData;
    public static void SetCharacterUnitData(CharacterUnitData data)
    {
        _getCharacterUnitData.Invoke(data);
    }
    
    
    [SerializeField, ReadOnly()]
    private CharacterUnitData _unitData = null;

    [SerializeField]
    private LobbyOrganizationEditUI _editUi = null;
    
    protected override void Awake()
    {
        base.Awake();
        _getCharacterUnitData = null;
        _getCharacterUnitData += GetCardInfo;
    }

    protected override void Initialize()
    {
        State = LobbyFSMSystem.LobbyFSMState.ORGANIZATION_EDIT;
    }
    
    private void GetCardInfo(CharacterUnitData data)
    {
        _unitData = data;
        _editUi.SetData(data);
    }
}
