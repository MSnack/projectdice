﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbyBattleState : LobbyStateBase, LoadInterface
{

    [SerializeField]
    private Text _nicknameText = null;

    [SerializeField]
    private UITweenTextNumber _trophyText = null;

    [SerializeField]
    private Button _battleStartButton = null;

    private int _trophy
    {
        set
        {
            if (!_trophyText.isPlaying)
            {
                _trophyText.From = _trophyText.To;
                _trophyText.To = Math.Max(0, value);
                _trophyText.enabled = false;
                _trophyText.enabled = true;
            }
            else
            {
                _trophyText.To = Math.Max(0, value);
            }
            // _goldText.text = (value <= 0 ? "0" : $"{value:#,###}");
        }
    }

    private GameUserMgr _userMgr = null;
    private Coroutine _updateUserInfoCoroutine = null;
    private bool _isReloadNickname = false;

    protected override void Awake()
    {
        if(_trophyText != null)
            LobbyRefreshUtil.SetRefreshEvent(RefreshState.Goods, RefreshGoods);
    }

    // Start is called before the first frame update
    void Start()
    {
        _updateUserInfoCoroutine = StartCoroutine(UpdateUserInfo());
        StartCoroutine(Init());
    }

    private IEnumerator Init()
    {
        if(!LobbyGoodsServerConnector.IsLoaded || !BattleInfoServerConnector.IsLoaded)
            yield return new WaitUntil(() => LobbyGoodsServerConnector.IsLoaded && BattleInfoServerConnector.IsLoaded);

        LobbyGoodsServerConnector.GetAllGoods( () =>
        {
            bool isLoaded = false;
            // LobbyRefreshUtil.OnRefresh(RefreshState.Goods);
            var prev_trophy = LobbyGoodsUtil.GetPrevGoods(GoodsID.TROPHY);
            if (prev_trophy > 0 && prev_trophy < LobbyGoodsUtil.GetGoods(GoodsID.TROPHY))
            {
                _trophyText.text = $"{LobbyGoodsUtil.GetPrevGoods(GoodsID.TROPHY)}";
                LobbyRewardEffectUtil.PlayEffect(GoodsID.TROPHY, _battleStartButton.transform);
                _trophyText.To = Math.Max(0, prev_trophy);
                isLoaded = true;
            }

            if (isLoaded) return;
            _trophyText.text = $"{LobbyGoodsUtil.GetGoods(GoodsID.TROPHY)}";
            _trophyText.To = Math.Max(0, LobbyGoodsUtil.GetGoods(GoodsID.TROPHY));
        });
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isReloadNickname && LobbyPopupFSMSystem.IsPopupActivated)
            _isReloadNickname = true;

        if (_isReloadNickname && !LobbyPopupFSMSystem.IsPopupActivated)
        {
            if(_updateUserInfoCoroutine == null)
                _updateUserInfoCoroutine = StartCoroutine(UpdateUserInfo());
            
            _isReloadNickname = false;
        }
    }

    private void OnDestroy()
    {
        if(_trophyText != null)
            LobbyRefreshUtil.ReleaseRefreshEvent(RefreshState.Goods, RefreshGoods);
    }

    protected override void Initialize()
    {
        State = LobbyFSMSystem.LobbyFSMState.BATTLE;
    }

    public override void StartState()
    {
        base.StartState();
        if(_updateUserInfoCoroutine == null)
            _updateUserInfoCoroutine = StartCoroutine(UpdateUserInfo());
    }

    public override void EndState()
    {
        base.EndState();
        if(_updateUserInfoCoroutine != null)
            StopCoroutine(_updateUserInfoCoroutine);
    }

    private IEnumerator UpdateUserInfo()
    {
        yield return new WaitUntil(() =>
        {
            _userMgr = EightUtil.GetCore<GameUserMgr>();
            return _userMgr != null && _userMgr.IsCompleteLogin;
        });
        
        _nicknameText.text = _userMgr.UserInfo.NickName;
        if (string.IsNullOrEmpty(_nicknameText.text)) _nicknameText.text = "이름 없음";
        _updateUserInfoCoroutine = null;
        _isLoaded = true;
    }

    private void RefreshGoods()
    {
        _trophy = LobbyGoodsUtil.GetGoods(GoodsID.TROPHY);
    }

    public bool IsLoaded()
    {
        return _isLoaded;
    }
}
