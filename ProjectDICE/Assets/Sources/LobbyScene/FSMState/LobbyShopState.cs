﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

public class LobbyShopState : LobbyStateBase
{

    [SerializeField]
    private AssetLabelReference _shopPackageAssetLabel = null;
    //private static List<GameObject> _shopPackageObjects = new List<GameObject>();
    private readonly List<LobbyShopPackageObject> _shopPackages = new List<LobbyShopPackageObject>();

    [SerializeField]
    private GameObject _loadingObject = null;

    [SerializeField]
    private GameObject _contentObject = null;

    protected override void Start()
    {
        base.Start();

        _contentObject.SetActive(false);
    }

    protected override void Initialize()
    {
        State = LobbyFSMSystem.LobbyFSMState.SHOP;
    }

    public override void StartState()
    {
        base.StartState();
        //_isLoaded = _shopPackageObjects.Count > 0;
        InitPackage();
    }

    private void InitPackage()
    {
        //if (_shopPackageObjects.Count > 0) return;

        if (_shopPackages.Count > 0)
            return;

        var loadHandle = Addressables.LoadResourceLocationsAsync(_shopPackageAssetLabel.labelString, null);
        loadHandle.Completed += (result) =>
        {
            var objData = result.Result;
            if (objData == null)
            {
                Debug.Log("Load Shop Package Failed");
                return;
            }

            //var list = new List<GameObject>();
            //list.AddRange(result.Result);

            //list = list.OrderBy((x) => x.GetComponent<LobbyShopPackageObject>().PackageIndex).ToList();
            //LoadPackage(list);
            StartCoroutine(LoadPackages(result.Result));


            //Addressables.Release(result.Result);
            //_isLoaded = true;
        };

    }

    private IEnumerator LoadPackages(IList<IResourceLocation> result)
    {
        var list = new List<LobbyShopPackageObject>();
        int completeCount = 0;
        foreach (var packageObject in result)
        {
            var instantiatehandle = Addressables.InstantiateAsync(packageObject);
            instantiatehandle.Completed += (handle) =>
            {
                var data = handle.Result;
                list.Add(handle.Result.GetComponent<LobbyShopPackageObject>());
                completeCount++;
            };
        }

        yield return new WaitUntil(() => completeCount >= result.Count);

        Addressables.Release(result);
        list = list.OrderBy((x) => x.PackageIndex).ToList();
        foreach (var data in list)
        {
            var localScale = data.transform.localScale;
            data.transform.SetParent(_contentObject.transform);
            data.transform.localPosition = Vector3.zero;
            data.transform.localScale = localScale;
            _shopPackages.Add(data);
        }
        //_shopPackages.AddRange(list);

        _loadingObject?.SetActive(false);
        _contentObject.SetActive(true);
        var animators = _contentObject.GetComponentsInChildren<Animator>();

        foreach (var animator in animators)
        {
            animator.enabled = true;
            animator.Rebind();
        }
    }

    private void LoadPackage(List<GameObject> list)
    {
        foreach (var packageObject in list)
        {
            var package = Instantiate(packageObject, _contentObject.transform)?.GetComponent<LobbyShopPackageObject>();
            if (package == null) continue;

            _shopPackages.Add(package);
        }

        _loadingObject?.SetActive(false);
        _contentObject.SetActive(true);
        var animators = _contentObject.GetComponentsInChildren<Animator>();
        //foreach (var animator in animators)
        //{
        //    animator.enabled = false;
        //}

        foreach (var animator in animators)
        {
            animator.enabled = true;
            animator.Rebind();
        }
    }

    private void ReleasePackages()
    {
        var packageList = _shopPackages.ToArray();
        _shopPackages.Clear();
        for (int i = packageList.Length - 1; i >= 0; --i)
        {
            Addressables.ReleaseInstance(packageList[i].gameObject);
        }
    }

    public override void EndState()
    {
        base.EndState();

        ReleasePackages();
    }
}
