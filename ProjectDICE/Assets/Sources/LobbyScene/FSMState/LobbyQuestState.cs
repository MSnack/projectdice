﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyQuestState : LobbyStateBase
{
    [SerializeField]
    private LobbyQuestUI _questUI = null;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = LobbyFSMSystem.LobbyFSMState.QUEST;
    }
    
    public override void StartState()
    {
        base.StartState();
        _questUI.GetQuestInfos();
    }

    public override void EndState()
    {
        base.EndState();
    }
}
