﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyOrganizationState : LobbyStateBase
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void Initialize()
    {
        State = LobbyFSMSystem.LobbyFSMState.ORGANIZATION;
    }

    public override void StartState()
    {
        base.StartState();
        LobbyNoticeUtil.OnNotice(LobbyNoticeUtil.NoticeState.Cards, false);
    }

    public override void EndState()
    {
        base.EndState();
    }
}
