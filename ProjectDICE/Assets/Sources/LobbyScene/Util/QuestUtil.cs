﻿using System;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

public enum QuestCategory
{
    None = 0,
    DailyReward = 1,
    DailyQuest = 2,
    TierQuest = 3,
    MembershipQuest = 4,
    TutorialQuest = 5,
    
    InnerTestReward = 10,
}

public enum QuestCondition
{
    WinGame = 1,
    Login = 2,
    GetTrophy = 3,
    GetGold = 4,
    GetStar = 5,
    CompletedDailyQuest = 6,
    OpenBoxAtLobby = 7,
    PlayGame = 8,
    BuyDailyShop = 9,
    UserLevelUp = 10,
    BuyMembership = 11,
    CompleteKnightsTutorial = 12,
}

public class QuestUtil : MonoBehaviour
{
    private static readonly Dictionary<QuestCategory, List<QuestInfo>> _questData =
        new Dictionary<QuestCategory, List<QuestInfo>>();

    private static Dictionary<int, List<GameIndexIDData>> _questLocalData = null;
    
    private static readonly Dictionary<QuestCategory, UnityAction<bool>> _questNotice =
        new Dictionary<QuestCategory, UnityAction<bool>>();
    private static readonly Dictionary<QuestCategory, bool> _questNoticeState = 
        new Dictionary<QuestCategory, bool>();

    // Start is called before the first frame update
    private void Start()
    {
        RefreshQuests();
    }


    public static void RegisterQuestData(QuestCategory category, List<QuestInfo> data)
    {
        if (!_questNotice.ContainsKey(category))
        {
            _questNotice.Add(category, null);
            _questNoticeState.Add(category, false);
        }
        if (!_questData.ContainsKey(category))
        {
            _questData.Add(category, data);
            return;
        }

        _questData[category] = data;
    }

    public static void RegisterQuestData(QuestCategory category, QuestInfo data)
    {
        if (!_questNotice.ContainsKey(category))
        {
            _questNotice.Add(category, null);
            _questNoticeState.Add(category, false);
        }
        
        if (!_questData.ContainsKey(category))
            _questData.Add(category, new List<QuestInfo>());

        _questData[category].Add(data);
    }

    public static List<QuestInfo> GetQuestData(QuestCategory category)
    {
        if (!_questData.ContainsKey(category)) return null;
        return _questData[category];
    }

    public static void ReleaseQuestData(QuestCategory category)
    {
        if (!_questData.ContainsKey(category)) return;
        _questData[category].Clear();
    }

    public static void ReleaseQuestData()
    {
        _questData.Clear();
    }

    public static void UpdateQuestEvent(QuestCondition condition, int value, bool isIncrement = true,
        UnityAction successEvent = null)
    {
        foreach (var pair in _questData)
        {
            var list = pair.Value;
            foreach (var questInfo in list)
            {
                if(questInfo.IsReceived) continue;
                var localData = GetQuestLocalData(questInfo.QuestId);
                if (localData.IsIncrement != isIncrement) continue;

                var resultStr = Array.Find(localData.Condition, (x) =>
                {
                    try { return condition == (QuestCondition) int.Parse(x); }
                    catch (FormatException) { return false; }
                });

                if (resultStr == null) continue;
                
                questInfo.Value = isIncrement ? questInfo.Value + value : value;
                bool isComplete = questInfo.Value >= localData.TargetValue;

                if (isComplete)
                {
                    QuestCategory targetCategory = (QuestCategory) localData.Category;
                    if (!_questNotice.ContainsKey(targetCategory))
                    {
                        _questNotice.Add(targetCategory, null);
                        _questNoticeState.Add(targetCategory, false);
                    }
                    OnNotice(targetCategory, true);
                }
            }
        }
        // QuestServerConnector.UpdateUserQuestInfo(condition, value, isIncrement, successEvent);
        LobbyRefreshUtil.OnRefresh(RefreshState.QuestValue);
    }

    public static void RefreshQuests()
    {
        foreach (var pair in _questData)
        {
            var list = pair.Value;
            foreach (var questInfo in list)
            {
                if(questInfo.IsReceived) continue;
                var localData = GetQuestLocalData(questInfo.QuestId);
                bool isComplete = questInfo.Value >= localData.TargetValue;

                if (!isComplete) continue;
                
                QuestCategory targetCategory = (QuestCategory) localData.Category;
                if (!_questNotice.ContainsKey(targetCategory))
                {
                    _questNotice.Add(targetCategory, null);
                    _questNoticeState.Add(targetCategory, false);
                }
                OnNotice(targetCategory, true);
            }
        }
        LobbyRefreshUtil.OnRefresh(RefreshState.QuestValue);
    }

    public static QuestLocalData GetQuestLocalData(int questId)
    {
        if(_questLocalData == null)
            _questLocalData = EightUtil.GetCore<GameDataMgr>()
                .GetCsvData(GameDataMgr.DataId.QuestLocalData);
        
        return _questLocalData[questId]?[0].ToClass<QuestLocalData>();
    }
    
    public static void SetNoticeEvent(QuestCategory category, UnityAction<bool> noticeEvent)
    {
        if (!_questNotice.ContainsKey(category))
        {
            _questNotice.Add(category, null);
            _questNoticeState.Add(category, false);
        }

        _questNotice[category] += noticeEvent;
    }

    public static void ReleaseNoticeEvent(QuestCategory category, UnityAction<bool> noticeEvent)
    {
        if (!_questNotice.ContainsKey(category)) return;

        if (noticeEvent != null) 
            _questNotice[category] -= noticeEvent;
    }

    public static void OnNotice(QuestCategory category, bool isActive)
    {
        _questNoticeState[category] = isActive;
        _questNotice[category]?.Invoke(isActive);
    }
    
    public static bool IsNotice(QuestCategory category)
    {
        return _questNoticeState.ContainsKey(category) && _questNoticeState[category];
    }
}