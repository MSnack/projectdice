﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MailUtil : MonoBehaviour
{

    private static List<MailInfo> _mailData = new List<MailInfo>();

    // Start is called before the first frame update
    void Start()
    {

    }

    public static void RegisterMailData(MailInfo data)
    {
        _mailData.Add(data);
        LobbyNoticeUtil.OnNotice(LobbyNoticeUtil.NoticeState.MailBox);
    }

    public static void RegisterMailList(List<MailInfo> data)
    {
        _mailData = data;
        LobbyNoticeUtil.OnNotice(LobbyNoticeUtil.NoticeState.MailBox);
    }

    public static List<MailInfo> GetMailData()
    {
        return _mailData;
    }

    public static void ReleaseMailData()
    {
        _mailData.Clear();
        LobbyNoticeUtil.OnNotice(LobbyNoticeUtil.NoticeState.MailBox, false);
    }

}
