﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class EightIAPButton : MonoBehaviour
{

    [SerializeField]
    private IAPButton _iapButton = null;
    
    private void OnEnable()
    {
        var button = GetComponent<Button>();
        button.onClick.AddListener(OnNoticePopup);
    }

    private void OnDisable()
    {
        var button = GetComponent<Button>();
        button.onClick.RemoveListener(OnNoticePopup);
    }

    private void OnNoticePopup()
    {
        if (_iapButton.enabled == true) return;
        
        GamePopupMgr.OpenNormalPopup("경고", "정식 서비스 시 데이터가 초기화 될 수 있으므로, 과금 선택 여부를 신중하게 결정해주세요.", 
            new []
            {
                new NormalPopup.ButtonConfig("취소", NormalPopup.Close, new Color(0.6f, 0.4f, 0.4f)),
                new NormalPopup.ButtonConfig("구매 진행", () =>
                {
                    NormalPopup.Close();
                    GamePopupMgr.OpenLoadingPopup();
                    _iapButton.enabled = true;
                    _iapButton.PurchaseProduct();
                })
            });
    }
}
