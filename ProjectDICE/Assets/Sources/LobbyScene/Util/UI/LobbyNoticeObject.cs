﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LobbyNoticeObject : MonoBehaviour
{

    [SerializeField]
    private bool _registerNoticeSystem = true;

    [SerializeField]
    private List<LobbyNoticeUtil.NoticeState> _conditions;

    [SerializeField]
    private List<GameObject> _activeObjects;
    
    
    
    private void Awake()
    {
        if (_registerNoticeSystem)
        {
            foreach (var state in _conditions)
            {
                LobbyNoticeUtil.SetNoticeEvent(state, OnNotice);
            }
        }

        OnNotice(_conditions.Any(LobbyNoticeUtil.IsNotice));
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    private void OnNotice(bool isActive)
    {
        foreach (var state in _conditions)
        {
            if (LobbyNoticeUtil.IsNotice(state) != true) continue;
            
            isActive = true;
            break;
        }
        
        foreach (var activeObject in _activeObjects)
        {
            activeObject.SetActive(isActive);
        }
    }

    private void OnDestroy()
    {
        foreach (var state in _conditions)
        {
            LobbyNoticeUtil.ReleaseNoticeEvent(state, OnNotice);
        }
    }
}
