﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LobbySlideableButton : EightReceiver
{
    public enum ButtonState
    {
        NONE, DOWN, MOVE, BLOCKED
    }

    [SerializeField, ReadOnly()]
    private ButtonState _state = ButtonState.NONE;

    [SerializeField]
    private UnityEvent _onClicked = null;
    
    private Camera _mainCamera = null;

    [SerializeField]
    private Canvas _currentCanvas = null;
    private EventSystem _eventSystem = null;
    private readonly PointerEventData _pointerData = new PointerEventData(null);

    private void Awake()
    {
        SetupComplete();
        _eventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Reset()
    {
        _currentCanvas = GetComponentInParent<Canvas>();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        _mainCamera = EightUtil.GetCore<EightCameraMgr>().EightCamera;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)
            && RectTransformUtility.RectangleContainsScreenPoint(
            (RectTransform)transform, 
            Input.mousePosition, _mainCamera))
        {
            
            _pointerData.position = Input.mousePosition;
            var results = new List<RaycastResult>();
            _eventSystem.RaycastAll(_pointerData, results);

            foreach (var result in results)
            {
                if (result.sortingOrder <= _currentCanvas.sortingOrder) continue;
                _state = ButtonState.NONE;
                return;
            }
            
            _state = ButtonState.DOWN;
        }

        if (_state == ButtonState.DOWN && Input.GetMouseButton(0))
        {
            _state = ButtonState.MOVE;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if ((_state == ButtonState.DOWN || _state == ButtonState.MOVE)
                && RectTransformUtility.RectangleContainsScreenPoint(
                    (RectTransform) transform,
                    Input.mousePosition, _mainCamera))
            {
                _onClicked?.Invoke();
            }
            _state = ButtonState.NONE;
        }
    }

    public void SetBlockButton()
    {
        _state = ButtonState.BLOCKED;
    }
}
