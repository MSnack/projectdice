﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LobbyQuestNoticeObject : MonoBehaviour
{
    [SerializeField]
    private bool _registerNoticeSystem = true;

    [SerializeField]
    private List<QuestCategory> _conditions;

    [SerializeField]
    private List<GameObject> _activeObjects;
    
    
    
    private void Awake()
    {
        if (_registerNoticeSystem)
        {
            foreach (var category in _conditions)
            {
                QuestUtil.SetNoticeEvent(category, OnNotice);
            }
        }

        OnNotice(_conditions.Any(QuestUtil.IsNotice));
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnNotice(bool isActive)
    {
        foreach (var category in _conditions)
        {
            if (QuestUtil.IsNotice(category) != true) continue;
            
            isActive = true;
            break;
        }
        
        foreach (var activeObject in _activeObjects)
        {
            activeObject.SetActive(isActive);
        }
    }

    private void OnDestroy()
    {
        foreach (var category in _conditions)
        {
            QuestUtil.ReleaseNoticeEvent(category, OnNotice);
        }
    }
}
