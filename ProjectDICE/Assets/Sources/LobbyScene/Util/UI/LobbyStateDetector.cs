﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LobbyStateDetector : MonoBehaviour
{

    [SerializeField]
    private LobbyFSMSystem.LobbyFSMState _lobbySceneState = LobbyFSMSystem.LobbyFSMState.BATTLE;

    [SerializeField]
    private LobbyPopupFSMSystem.LobbyPopupFSMState _lobbyPopupState = LobbyPopupFSMSystem.LobbyPopupFSMState.NONE;

    private LobbyFSMSystem.LobbyFSMState _currLobbySceneState = LobbyFSMSystem.LobbyFSMState.NONE;
    private LobbyPopupFSMSystem.LobbyPopupFSMState _currLobbyPopupState = LobbyPopupFSMSystem.LobbyPopupFSMState.NONE;

    [SerializeField]
    private UnityEvent _onStateCorrect = null;

    [SerializeField]
    private UnityEvent _onStateIncorrect = null;

    // Start is called before the first frame update
    void Start()
    {
        LobbySceneInterface.CallChangingState += ChangeLobbyState;
        LobbyPopupInterface.CallChangingState += ChangeLobbyPopupState;
    }

    private void OnDestroy()
    {
        LobbySceneInterface.CallChangingState -= ChangeLobbyState;
        LobbyPopupInterface.CallChangingState -= ChangeLobbyPopupState;
    }

    private void ChangeLobbyState(LobbyFSMSystem.LobbyFSMState state)
    {
        _currLobbySceneState = state;
        if (_lobbySceneState == _currLobbySceneState
            && _lobbyPopupState == _currLobbyPopupState)
        {
            _onStateCorrect?.Invoke();
        }
        else
        {
            _onStateIncorrect?.Invoke();
        }
    }

    private void ChangeLobbyPopupState(LobbyPopupFSMSystem.LobbyPopupFSMState state)
    {
        _currLobbyPopupState = state;
        if (_lobbySceneState == _currLobbySceneState
            && _lobbyPopupState == _currLobbyPopupState)
        {
            _onStateCorrect?.Invoke();
        }
        else
        {
            
            _onStateIncorrect?.Invoke();
        }
    }
}
