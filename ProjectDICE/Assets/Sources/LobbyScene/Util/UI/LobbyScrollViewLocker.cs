﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

[RequireComponent(typeof(ScrollRect))]
public class LobbyScrollViewLocker : MonoBehaviour
{
    [SerializeField]
    private LobbyUITouchSlider _slider = null;

    private ScrollRect _scrollRect = null;
    
    // Start is called before the first frame update
    void Start()
    {
        if (_slider == null)
        {
            Debug.LogError("Must Set LobbyUITouchSlider");
            return;
        }
        _slider.DraggingEvent += Lock;
        _scrollRect = GetComponent<ScrollRect>();
    }

    private void Lock(bool isLocked)
    {
        _scrollRect.enabled = !isLocked;
    }
}
