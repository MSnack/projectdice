﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class LobbyEffectGenerator : MonoBehaviour
{
    [SerializeField]
    private Transform _targetTransform = null;

    [SerializeField]
    private GameEffectData _data = null;

    [SerializeField, ReadOnly()]
    private GameEffectObject _effect = null;
    public GameEffectObject Effect => _effect;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(InitEffect());
    }

    private IEnumerator InitEffect()
    {
        yield return new WaitUntil(() => EightUtil.IsLoadCompleteDevice);
        var mgr = EightUtil.GetCore<GameEffectMgr>();
        _effect = mgr.UseEffect(_data);
        Vector3 pos = _effect.transform.localPosition;
        Vector3 scale = _effect.transform.localScale;
        Quaternion rot = _effect.transform.rotation;
        
        _effect.transform.SetParent(_targetTransform);
        _effect.transform.localPosition = pos;
        _effect.transform.localScale = scale;
        _effect.transform.rotation = rot;
    }

    private void OnDestroy()
    {
        var mgr = EightUtil.GetCore<GameEffectMgr>();
        mgr?.ReturnEffect(_effect);
    }

    private void Reset()
    {
        _targetTransform = transform;
    }
}
