﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class EmoticonUtil : MonoBehaviour
{
    private static EmoticonDeckInfo _currentEmoticonDeck = null;
    private static readonly List<EmoticonInfo> _emoticonList = new List<EmoticonInfo>();

    [SerializeField]
    private AssetLabelReference _emoticonLabel = null;
    public static bool IsLoaded { get; private set; }

    private void Awake()
    {
        if(_emoticonList.Count <= 0) Init();
    }

    private void Init()
    {
        IsLoaded = false;
    }

    public static void RegisterEmoticonDeckData(EmoticonDeckInfo emoticonDeckInfo)
    {
        _currentEmoticonDeck = emoticonDeckInfo;
        IsLoaded = true;
    }

    public static EmoticonDeckInfo GetEmoticonDeckData()
    {
        return _currentEmoticonDeck;
    }

    public static void RegisterEmoticonData(List<EmoticonInfo> infos)
    {
        ReleaseEmoticonData();
        _emoticonList.AddRange(infos);
    }

    public static List<EmoticonInfo> GetEmoticonData()
    {
        return _emoticonList;
    }

    public static void ReleaseEmoticonData()
    {
        _emoticonList.Clear();
    }
    
}
