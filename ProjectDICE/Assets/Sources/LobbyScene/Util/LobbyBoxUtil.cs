﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class LobbyBoxUtil : MonoBehaviour
{
    [SerializeField]
    private AssetLabelReference _boxSpriteLabel = null;

    [SerializeField]
    private AssetLabelReference _boxPrefabLabel = null;
    private static readonly List<Sprite> _boxSprites = new List<Sprite>();
    private static readonly List<GameObject> _boxPrefabs = new List<GameObject>();

    private static List<UserBoxInvenInfo> _userBoxInventory = null;
    private static Dictionary<int, List<GameIndexIDData>> _boxLocalData = null;
    private static Dictionary<int, List<GameIndexIDData>> _boxOpenTimeData = null;

    private bool isSpriteLoaded = false;
    private bool isPrefabLoaded = false;
    
    public static bool IsLoaded { get; private set; }

    private void Awake()
    {
        if (_boxSprites.Count <= 0)
        {
            IsLoaded = false;
            Init();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Init()
    {
        var loadSpriteHandle = Addressables.LoadAssetsAsync<Sprite>(_boxSpriteLabel.labelString, null);
        loadSpriteHandle.Completed += (result) =>
        {
            var spritesData = result.Result;
            if (spritesData == null)
            {
                Debug.Log("Load Box Sprite Failed");
                return;
            }

            foreach (var sprite in spritesData)
            {
                _boxSprites.Add(sprite);
            }

            isSpriteLoaded = true;
            IsLoaded = isSpriteLoaded && isPrefabLoaded;
        };
        
        var loadPrefabHandle = Addressables.LoadAssetsAsync<GameObject>(_boxPrefabLabel.labelString, null);
        loadPrefabHandle.Completed += (result) =>
        {
            var prefabData = result.Result;
            if (prefabData == null)
            {
                Debug.Log("Load Box Prefab Failed");
                return;
            }

            foreach (var prefab in prefabData)
            {
                _boxPrefabs.Add(prefab);
            }

            isPrefabLoaded = true;
            IsLoaded = isSpriteLoaded && isPrefabLoaded;
        };
    }

    public static Sprite GetBoxSprite(int boxId, bool isMiniSprite = false)
    {
        if (boxId <= 0 || _boxSprites.Count < boxId) return null;

        string spriteName = (isMiniSprite ? "MiniBox" : "Box") + (boxId - 1).ToString();

        return _boxSprites.Find((x) => x.name.Contains(spriteName));
    }

    public static GameObject GetBoxPrefab(int boxId, bool hasOpenAnimation = false)
    {
        if (boxId <= 0 || _boxPrefabs.Count < boxId) return null;
        
        string prefabName = "Box" + (boxId - 1) + (hasOpenAnimation ? "Open" : "");

        return _boxPrefabs.Find((x) => x.name.Contains(prefabName));
    }
    
    public static string GetBoxName(int id)
    {
        switch (id)
        {
            case 1: return "일반 상자";
            case 2: return "고급 상자";
            case 3: return "희귀 상자";
            case 4: return "영웅 상자";
            case 5: return "전설 상자";
            case 6: return "보석 상자";
            case 7: return "UNKNOWN";
            case 8: return "UNKNOWN";
        }

        return "Unknown";
    }

    public static void SetUserBoxInventory(List<UserBoxInvenInfo> infos)
    {
        _userBoxInventory?.Clear();
        _userBoxInventory = new List<UserBoxInvenInfo>() {null, null, null, null};
        
        foreach (var info in infos)
        {
            _userBoxInventory[info.BoxIndex - 1] = info;
        }
    }

    public static void SetUserBoxInventory(UserBoxInvenInfo info)
    {
        if(_userBoxInventory == null)
            _userBoxInventory = new List<UserBoxInvenInfo>() {null, null, null, null};
        
        _userBoxInventory[info.BoxIndex - 1] = info;
    }

    public static UserBoxInvenInfo GetUserBoxInventory(int index)
    {
        if (index > _userBoxInventory.Count || index <= 0)
        {
            Debug.LogAssertion("Out of Index Assert Error (LobbyBoxUtil)");
            return null;
        }
        
        return _userBoxInventory[index - 1];
    }

    public static List<UserBoxInvenInfo> GetUserBoxInventories()
    {
        return _userBoxInventory;
    }

    public static BoxLocalData GetBoxLocalData(int boxId)
    {
        if (_boxLocalData == null)
        {
            var dataMgr = EightUtil.GetCore<GameDataMgr>();
            _boxLocalData = dataMgr.GetCsvData(GameDataMgr.DataId.BoxLocalData);
        }

        return _boxLocalData?[boxId][0].ToClass<BoxLocalData>();

    }

    public static BoxOpenTimeData GetBoxOpenTimeData(int time)
    {
        if (_boxOpenTimeData == null)
        {
            var dataMgr = EightUtil.GetCore<GameDataMgr>();
            _boxOpenTimeData = dataMgr.GetCsvData(GameDataMgr.DataId.BoxOpenTimeData);
        }

        BoxOpenTimeData result = null;
        foreach (var pair in _boxOpenTimeData)
        {
            if (time <= pair.Key) return pair.Value[0].ToClass<BoxOpenTimeData>();
        }
        
        return null;
    }
}
