﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public enum ItemCategory
{
    NONE = 0,
    GOODS = 1,
    BOX = 2,
    CHARACTER = 3,
    EMOTICON = 4,
    QUEST_CONDITION = 5,
}

[Serializable]
public class RewardItemInfo
{
    private ItemCategory _itemCategory;
    public int ItemCategory
    {
        get => (int) _itemCategory;
        set => _itemCategory = (ItemCategory) value;
    }

    private int _userId;
    public int UserId
    {
        get => _userId;
        set => _userId = value;
    }

    private int _id;
    public int Id
    {
        get => _id;
        set => _id = value;
    }

    private int _count;
    public int Count
    {
        get => _count;
        set => _count = value;
    }
    
}

public class ItemCategoryUtil : MonoBehaviour
{
    public class ItemInfo
    {
        public string Name = null;
        public Sprite SpriteImage = null;
        public Sprite FrameImage = null;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public static ItemInfo GetItemInfo(ItemCategory category, int itemId)
    {
        ItemInfo result = new ItemInfo();
        switch (category)
        {
            case ItemCategory.GOODS:
                result.SpriteImage = LobbyGoodsUtil.GetGoodsSprite((GoodsID) itemId);
                result.Name = LobbyGoodsUtil.GetGoodsName((GoodsID) itemId);
                break;
            case ItemCategory.BOX:
                result.SpriteImage = LobbyBoxUtil.GetBoxSprite(itemId);
                result.Name = LobbyBoxUtil.GetBoxName(itemId);
                break;
            case ItemCategory.CHARACTER:
                var gameUnitDataMgr = EightUtil.GetCore<GameUnitDataMgr>();
                var cardInfo = gameUnitDataMgr.GetCharacaterData(itemId);
                if (cardInfo == null) break;
                result.SpriteImage = cardInfo.UnitPreviewSprite;
                result.Name = cardInfo.UnitName;
                result.FrameImage = LobbyCardUtil.GetFrameSprite(cardInfo.Grade - 1);
                break;
            case ItemCategory.EMOTICON:
                var emoticonMgr = EightUtil.GetCore<EmoticonManager>();
                var data = emoticonMgr.GetEmoticonData(itemId);
                result.SpriteImage = data.EmoticonIcon;
                result.Name = "";
                break;
        }

        return result;
    }
    
}
