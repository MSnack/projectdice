﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.AddressableAssets;

public enum GoodsID
{
    CASH = -1,
    UNKOWN = 0,
    GOLD = 1,
    STAR = 2,
    TROPHY = 3,
}

public class LobbyGoodsUtil : MonoBehaviour
{
    private static Dictionary<GoodsID, int> _goods = new Dictionary<GoodsID, int>();
    private static Dictionary<GoodsID, int> _prevGoods = new Dictionary<GoodsID, int>();

    [SerializeField]
    private AssetLabelReference _goodsSpriteLabel = null;
    private static readonly Dictionary<GoodsID, Sprite> _goodsSprites = new Dictionary<GoodsID, Sprite>();
    public static bool IsLoaded { get; private set; }
    
    private void Awake()
    {
        if(_goodsSprites.Count <= 0) Init();
    }
    
    private void Init()
    {
        var loadHandle = Addressables.LoadAssetsAsync<Sprite>(_goodsSpriteLabel.labelString, null);
        loadHandle.Completed += (result) =>
        {
            var spritesData = result.Result;
            if (spritesData == null)
            {
                Debug.Log("Load Goods Sprite Failed");
                return;
            }

            foreach (var sprite in spritesData)
            {
                _goodsSprites.Add((GoodsID) int.Parse(sprite.name),sprite);
            }

            IsLoaded = true;
        };
    }

    /**
     * 서버에 영향을 주지 않음 (서버 관련은 LobbyGoodsServerConnector::IncrementGoods 참조)
     */
    public static void SetGoods(GoodsID id, int value)
    {
        if (!_goods.ContainsKey(id))
        {
            _goods.Add(id, value);
            return;
        }

        _goods[id] = value;
    }
    
    public static void SavePrevGoods()
    {
        _prevGoods = new Dictionary<GoodsID, int>(_goods);
    }

    /**
     * 서버에 영향을 주지 않음 (서버 관련은 LobbyGoodsServerConnector::GetAllGoods 참조)
     */
    public static int GetGoods(GoodsID id)
    {
        return !_goods.ContainsKey(id) ? SystemIndex.InvalidInt : _goods[id];
    }
    
    public static int GetPrevGoods(GoodsID id)
    {
        return !_prevGoods.ContainsKey(id) ? SystemIndex.InvalidInt : _prevGoods[id];
    }

    public static string GetGoodsName(GoodsID id)
    {
        switch (id)
        {
            case GoodsID.GOLD: return "골드";
            case GoodsID.STAR: return "스타";
            case GoodsID.TROPHY: return "트로피";
            case GoodsID.CASH: return "\\";
        }

        return "Unknown";
    }

    public static Sprite GetGoodsSprite(GoodsID id)
    {
        if (!_goodsSprites.ContainsKey(id)) return null;
        return _goodsSprites[id];
    }
}
