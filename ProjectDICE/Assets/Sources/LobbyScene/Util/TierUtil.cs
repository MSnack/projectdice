﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EightWork;
using UnityEngine;
using UnityEngine.AddressableAssets;

public enum TierState
{
    Unknown = 0,
    Bronze0 = 1001,
    Bronze1 = 1002,
    Bronze2 = 1003,
    Silver0 = 2001,
    Silver1 = 2002,
    Silver2 = 2003,
    Gold0 = 3001,
    Gold1 = 3002,
    Gold2 = 3003,
    Diamond0 = 4001,
    Diamond1 = 4002,
    Diamond2 = 4003,
    Diamond3 = 4004,
    Diamond4 = 4005,
    Master0 = 5001,
    Master1 = 5002,
    Master2 = 5003,
    Master3 = 5004,
    Master4 = 5005,
    Challenge0 = 6000,
    
}

public class TierUtil : MonoBehaviour
{
    [SerializeField]
    private AssetLabelReference _spriteLabel = null;
    
    private static readonly List<Sprite> _tierSprites = new List<Sprite>();
    private static Dictionary<int, List<GameIndexIDData>> _tierLocalData = null;

    private static List<TierState> _tierEnumList;

    [SerializeField]
    private GameEffectData[] _tierEffects = new GameEffectData[6];
    private static GameEffectData[] _tierEffectsStatic = null;

    public static bool IsLoaded { get; private set; }


    private void Awake()
    {
        IsLoaded = false;
        if (_tierSprites.Count <= 0) Init();
        else
            IsLoaded = true;
        _tierEffectsStatic = _tierEffects;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Init()
    {
        _tierEnumList = Enum.GetValues(typeof(TierState)).Cast<TierState>().ToList();
        
        var loadSpriteHandle = Addressables.LoadAssetsAsync<Sprite>(_spriteLabel.labelString, null);
        loadSpriteHandle.Completed += (result) =>
        {
            var spritesData = result.Result;
            if (spritesData == null)
            {
                Debug.Log("Load Box Sprite Failed");
                return;
            }

            foreach (var sprite in spritesData)
            {
                _tierSprites.Add(sprite);
            }

            IsLoaded = true;
        };
        
    }

    public static int TierSize()
    {
        return _tierEnumList?.Count() ?? 0;
    }
    
    public static Sprite GetTierSprite(TierState tierId, bool isMiniSprite = false)
    {
        if (tierId == TierState.Unknown
            || !Enum.IsDefined(typeof(TierState), tierId)) return null;

        string spriteName = (isMiniSprite ? "Mini" : "") + tierId.ToString();

        return _tierSprites.Find((x) => x.name.Equals(spriteName));
    }

    public static GameEffectData GetTierEffect(TierState tierId)
    {
        int convert = ((int) tierId) / 1000 - 1;
        if (convert < 0 || convert > _tierEffectsStatic.Length) return null;
        return _tierEffectsStatic[convert];
    }
    
    public static TierLocalData GetTierLocalData(TierState tierId)
    {
        if (_tierLocalData == null)
        {
            var dataMgr = EightUtil.GetCore<GameDataMgr>();
            _tierLocalData = dataMgr.GetCsvData(GameDataMgr.DataId.TierLocalData);
        }

        return _tierLocalData?[(int) tierId][0].ToClass<TierLocalData>();
    }
    
    public static TierState GetTierState(int trophy)
    {
        if (_tierLocalData == null)
        {
            var dataMgr = EightUtil.GetCore<GameDataMgr>();
            _tierLocalData = dataMgr.GetCsvData(GameDataMgr.DataId.TierLocalData);
        }

        TierState result = TierState.Unknown;
        foreach (var indexIDData in _tierLocalData)
        {
            var data = indexIDData.Value[0].ToClass<TierLocalData>();
            if (data.NeedTrophy > trophy) return result;
            result = (TierState) data.DataID;
        }

        return TierState.Unknown;
    }

    public static TierState GetTierStateByIndex(int index)
    {
        if (index < 0 || index >= _tierEnumList.Count) return TierState.Unknown;
        return _tierEnumList[index];
    }

    public static TierState GetNextTierState(int trophy)
    {
        TierState result = GetTierState(trophy);
        if (result == _tierEnumList.Last()) return result;
        
        int index = _tierEnumList.FindIndex((x) => x == result);
        return _tierEnumList[index + 1];
    }
    
    public static TierState GetPrevTierState(int trophy)
    {
        TierState result = GetTierState(trophy);
        if (result == _tierEnumList[0]) return result;
        
        int index = _tierEnumList.FindIndex((x) => x == result);
        return _tierEnumList[index - 1];
    }
}
