﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BattleHistoryUtil : MonoBehaviour
{
    private static Queue<BattleHistoryLocalData> _historyLocalData = null;

    private void Awake()
    {
        Load();
    }

    private static void Load()
    {
        if(_historyLocalData != null) return;
        var array = BattleHistoryLocalData.Load("history.dat");
        _historyLocalData = (array != null) ? new Queue<BattleHistoryLocalData>(array) : new Queue<BattleHistoryLocalData>();
    }

    public static void AddBattleHistory(BattleHistoryLocalData data)
    {
        Load();
        _historyLocalData.Enqueue(data);
        if (_historyLocalData.Count > 15)
        {
            for (int i = 0; i < _historyLocalData.Count - 15; ++i)
            {
                _historyLocalData.Dequeue();
            }
        }
        BattleHistoryLocalData.Save("history.dat", _historyLocalData.ToArray());
    }

    public static List<BattleHistoryLocalData> GetBattleHistory()
    {
        Load();
        return _historyLocalData.ToList();
    }
}
