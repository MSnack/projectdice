﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyGoodsRefresher : MonoBehaviour
{

    public void RefreshAllGoods()
    {
        LobbyGoodsServerConnector.GetAllGoods(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Goods));
    }
}
