﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyCardUtil : MonoBehaviour
{
    private static Sprite[] _cardSprites;
    public static Sprite GetFrameSprite(int index) => _cardSprites[index];

    [SerializeField]
    private Sprite[] _cardFrameSprites;

    private static Color[] _cardColors;
    public static Color GetCardColor(int index) => _cardColors[index];

    [SerializeField]
    private Color[] _cardFrameColors;

    private static string[] _cardGradeTexts;
    public static string GetCardGradeText(int index) => _cardGradeTexts[index];
    
    [SerializeField]
    private Sprite[] _cardStars;
    private static Sprite[] _cardStaticStars;
    public static Sprite GetStarSprite(int index) => _cardStaticStars[index];

    [SerializeField]
    private string[] _cardFrameGradeTexts;
    
    private void Awake()
    {
        _cardSprites = _cardFrameSprites;
        _cardColors = _cardFrameColors;
        _cardGradeTexts = _cardFrameGradeTexts;
        _cardStaticStars = _cardStars;
    }
}
