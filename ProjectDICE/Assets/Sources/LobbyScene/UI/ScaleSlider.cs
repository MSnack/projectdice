﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleSlider : MonoBehaviour
{
    [SerializeField]
    private Slider _slider = null;

    [SerializeField]
    private Vector2 _scaleRange = new Vector2(0.5f, 1.5f);
    
    public void ChangeScale()
    {
        float value = Mathf.Lerp(_scaleRange.x, _scaleRange.y, _slider.value);
        transform.localScale = new Vector3(value, value, value);
    }
}
