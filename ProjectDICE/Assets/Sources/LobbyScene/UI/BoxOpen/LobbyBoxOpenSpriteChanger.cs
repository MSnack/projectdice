﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyBoxOpenSpriteChanger : MonoBehaviour
{

    [SerializeField]
    private SpriteRenderer _cardSpriteRenderer = null;

    [SerializeField]
    private SpriteRenderer _cardFrameRenderer = null;

    public void ChangeCardSprite(Sprite sprite)
    {
        _cardSpriteRenderer.sprite = sprite;
    }
    
    public void ChangeCardFrame(Sprite sprite)
    {
        _cardFrameRenderer.sprite = sprite;
    }
}
