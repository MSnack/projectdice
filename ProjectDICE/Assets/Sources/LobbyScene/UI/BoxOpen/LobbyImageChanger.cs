﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyImageChanger : MonoBehaviour
{
    [SerializeField]
    private Image _image = null;

    public void ChangeSprite(Sprite sprite)
    {
        _image.sprite = sprite;
    }

    private void Reset()
    {
        _image = GetComponent<Image>();
    }
}
