﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using Unity.Collections;
using UnityEngine;

public class LobbySettingsMenu : MonoBehaviour
{
    [SerializeField]
    private Animator _animator = null;

    private bool _isActive = false;

    public bool IsActive => _isActive;

    private void Awake()
    {
        if(_animator == null) _animator = GetComponent<Animator>();
    }

    public void OpenMenu()
    {
        gameObject.SetActive(true);
        _isActive = true;
        _animator.SetBool("Active", true);
    }

    public void CloseMenu()
    {
        _isActive = false;
        _animator.SetBool("Active", false);
    }

    public void ChangeLobbyScene()
    {
        GamePopupMgr.OpenNormalPopup("알림", "로비를 선택하세요.", new []
        {
            new NormalPopup.ButtonConfig("개편안", () =>
            {
                EightUtil.GetCore<EightSceneMgr>().ChangeScene(SCENE_TYPE.NEW_LOBBY);
                NormalPopup.Close();
            }), 
            new NormalPopup.ButtonConfig("기존안", () =>
            {
                EightUtil.GetCore<EightSceneMgr>().ChangeScene(SCENE_TYPE.LOBBY);
                NormalPopup.Close();
            }), 
        });
    }
}
