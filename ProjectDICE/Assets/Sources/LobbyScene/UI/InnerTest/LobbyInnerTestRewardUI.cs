﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LobbyInnerTestRewardUI : MonoBehaviour
{
    [SerializeField]
    private LobbyDailyRewardSlotObject _prefabSlotObject = null;

    [SerializeField]
    private Transform _contentTransform = null;

    [SerializeField]
    private Text _currentDateText = null;
    
    [SerializeField]
    private Text _remainTimeText = null;
    
    private readonly List<LobbyDailyRewardSlotObject> _slotObjects = new List<LobbyDailyRewardSlotObject>();
    private DateTime _endDate = DateTime.Today.AddMonths(1).AddDays(-DateTime.Today.Day + 2);

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }
    
    void Update()
    {
        TimeSpan t = _endDate - DateTime.Now;
        
        if (t.TotalSeconds < 0)
        {
            _endDate =  DateTime.Today.AddMonths(1).AddDays(-DateTime.Today.Day + 2);
            return;
        }
        
        _remainTimeText.text = (t.Days > 0 ? $"{t.Days}일 " : "")
                               + (t.Hours > 0 ? $"{t.Hours}시간 " : "")
                               + (t.Minutes > 0 ? $"{t.Minutes}분 " : "")
                               + (t.Hours <= 0 ? $"{t.Seconds}초 " : "");
    }

    private void Init()
    {
        if (_slotObjects.Count > 0) return;

        var infos = QuestUtil.GetQuestData(QuestCategory.InnerTestReward);
        _currentDateText.text = $"{infos.Last().Value}일차";

        foreach (var info in infos)
        {
            var slot = Instantiate(_prefabSlotObject, _contentTransform);
            var data = QuestUtil.GetQuestLocalData(info.QuestId);
            slot.InitSlot(info, data);
            _slotObjects.Add(slot);
        }
    }
}
