﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbyTopUI : MonoBehaviour, LoadInterface
{
    [Header("Goods UI")]
    [SerializeField]
    private Text _levelText = null;

    [SerializeField]
    private Text _expText = null;

    [SerializeField]
    private Transform _expBarTransform = null;

    [SerializeField]
    private UITweenTextNumber _goldText = null;

    [SerializeField]
    private Button _battleStartButton = null;
    private int _gold
    {
        set
        {
            if (!_goldText.isPlaying)
            {
                _goldText.From = _goldText.To;
                _goldText.To = Math.Max(0, value);
                _goldText.enabled = false;
                _goldText.enabled = true;
            }
            else
            {
                _goldText.To = Math.Max(0, value);
            }
            // _goldText.text = (value <= 0 ? "0" : $"{value:#,###}");
        }
    }

    [SerializeField]
    private UITweenTextNumber _starText = null;

    private int _star
    {
        set
        {
            if (!_starText.isPlaying)
            {
                _starText.From = _starText.To;
                _starText.To = Math.Max(0, value);
                _starText.enabled = false;
                _starText.enabled = true;
            }
            else
            {
                _starText.To = Math.Max(0, value);
            }
            // _goldText.text = (value <= 0 ? "0" : $"{value:#,###}");
        }
    }

    [SerializeField]
    private Text _trophyText = null;
    private int _trophy { set => _trophyText.text = (value <= 0 ? "0" : $"{value:#,###}"); }

    [SerializeField]
    private Image _tierImage = null;
    
    private Dictionary<int, List<GameIndexIDData>> _levelLocalData = null;

    private bool _isLoaded = false;


    // Start is called before the first frame update
    void Start()
    {
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.Goods, OnGoodsInfoRefreshed);
        StartCoroutine(OnConnectorLoaded());
    }

    private void OnDestroy()
    {
        LobbyRefreshUtil.ReleaseRefreshEvent(RefreshState.Goods, OnGoodsInfoRefreshed);
        LobbyGoodsUtil.SavePrevGoods();
    }

    private IEnumerator OnConnectorLoaded()
    {
        if(!LobbyGoodsServerConnector.IsLoaded || !BattleInfoServerConnector.IsLoaded)
            yield return new WaitUntil(() => LobbyGoodsServerConnector.IsLoaded && BattleInfoServerConnector.IsLoaded);
        
        LobbyGoodsServerConnector.GetAllGoods( () =>
        {
            bool isLoaded = false;
            // LobbyRefreshUtil.OnRefresh(RefreshState.Goods);
            var prev_gold = LobbyGoodsUtil.GetPrevGoods(GoodsID.GOLD);
            var prev_star = LobbyGoodsUtil.GetPrevGoods(GoodsID.STAR);
            OnGoodsInfoRefreshed(false);
            if (prev_gold > 0 && prev_gold != LobbyGoodsUtil.GetGoods(GoodsID.GOLD))
            {
                _goldText.text = $"{LobbyGoodsUtil.GetPrevGoods(GoodsID.GOLD):#,###}";
                LobbyRewardEffectUtil.PlayEffect(GoodsID.GOLD, _battleStartButton.transform);
                _goldText.To = Math.Max(0, prev_gold);
                isLoaded = true;
            }
            if (prev_star > 0 && prev_star != LobbyGoodsUtil.GetGoods(GoodsID.STAR))
            {
                _starText.text = $"{LobbyGoodsUtil.GetPrevGoods(GoodsID.STAR):#,###}";
                LobbyRewardEffectUtil.PlayEffect(GoodsID.STAR, _battleStartButton.transform);
                _starText.To = Math.Max(0, prev_star);
                isLoaded = true;
            }

            if (isLoaded) return;
            _goldText.text = $"{LobbyGoodsUtil.GetGoods(GoodsID.GOLD):#,###}";
            _starText.text = $"{LobbyGoodsUtil.GetGoods(GoodsID.STAR):#,###}";
            _goldText.To = Math.Max(0, LobbyGoodsUtil.GetGoods(GoodsID.GOLD));
            _starText.To = Math.Max(0, LobbyGoodsUtil.GetGoods(GoodsID.STAR));
        });

        BattleInfoServerConnector.GetBattleInfo((received) =>
        {
            if (_levelLocalData == null)
            {
                var dataMgr = EightUtil.GetCore<GameDataMgr>();
                _levelLocalData = dataMgr.GetCsvData(GameDataMgr.DataId.LevelLocalData);
            }
            int level = (int) received.Level;
            _levelText.text = level.ToString();
            float exp = received.Exp;
            float expTransform = 0;
            if (_levelLocalData.ContainsKey(level + 1) == false)
            {
                _expText.text = $"0/0";
                expTransform = 1;
            }
            else
            {
                float targetExp = _levelLocalData[level + 1][0].ToClass<LevelLocalData>().NeedExp;
                expTransform = exp / targetExp;
                _expText.text = $"{exp:G}/{targetExp:G}";
            }
        
            _expBarTransform.localScale = new Vector3(expTransform, 1, 1);
            _isLoaded = true;
        }, true);
    }

    private void OnGoodsInfoRefreshed()
    {
        OnGoodsInfoRefreshed(true);
    }

    private void OnGoodsInfoRefreshed(bool isAnimated)
    {
        _isLoaded = false;
        if (isAnimated)
        {
            _gold = LobbyGoodsUtil.GetGoods(GoodsID.GOLD);
            _star = LobbyGoodsUtil.GetGoods(GoodsID.STAR);
        }
        else
        {
            _goldText.text = $"{LobbyGoodsUtil.GetGoods(GoodsID.GOLD):#,###}";
            _starText.text = $"{LobbyGoodsUtil.GetGoods(GoodsID.STAR):#,###}";
        }

        int trophy = LobbyGoodsUtil.GetGoods(GoodsID.TROPHY);
        if(_trophyText != null) _trophy = trophy;
        if (_tierImage != null) StartCoroutine(LoadTierSprites(trophy));

        GameBattleInfo info = null;
        BattleInfoServerConnector.GetBattleInfo((received) => info = received);
        if (_levelLocalData == null)
        {
            var dataMgr = EightUtil.GetCore<GameDataMgr>();
            _levelLocalData = dataMgr.GetCsvData(GameDataMgr.DataId.LevelLocalData);
        }
        int level = (int) info.Level;
        _levelText.text = level.ToString();
        float exp = info.Exp;
        float expTransform = 0;
        if (_levelLocalData.ContainsKey(level + 1) == false)
        {
            _expText.text = $"0/0";
            expTransform = 1;
        }
        else
        {
            float targetExp = _levelLocalData[level + 1][0].ToClass<LevelLocalData>().NeedExp;
            expTransform = exp / targetExp;
            _expText.text = $"{exp:G}/{targetExp:G}";
        }
        
        _expBarTransform.localScale = new Vector3(expTransform, 1, 1);
        _isLoaded = true;
    }

    private IEnumerator LoadTierSprites(int trophy)
    {
        if(TierUtil.IsLoaded == false)
            yield return new WaitUntil(() => TierUtil.IsLoaded);
        _tierImage.sprite = TierUtil.GetTierSprite(TierUtil.GetTierState(trophy));
    }

    public bool IsLoaded()
    {
        return _isLoaded;
    }
}
