﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbyDailySpecialUI : EightReceiver
{
    private static List<DailyShopInfo> _shopInfos = null;
    private Dictionary<int, List<GameIndexIDData>> _shopLocalData = null;

    private readonly List<ShopItemSlotObject> _slotObjects = new List<ShopItemSlotObject>();

    [SerializeField]
    private GameObject _slotObject = null;

    [SerializeField]
    private GameObject _contentObject = null;

    [SerializeField]
    private Text _waitTimeText;

    [SerializeField]
    private int _maxRefreshCount = 1;

    private int _refreshCount = 0;

    [SerializeField]
    private LobbyShopAds _adsMgr = null;

    private DateTime _tomorrow = DateTime.Today.AddDays(1);

    public static bool IsLoaded { get; private set; }

    private void Awake()
    {
        SetupComplete();
    }

    private void Start()
    {
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.DailyShop, InitSlots);
    }

    private void Update()
    {
        TimeSpan t = _tomorrow - DateTime.Now;
        
        if (t.TotalSeconds < 0)
        {
            _refreshCount = 0;
            StartCoroutine(Init(true));
            _tomorrow = DateTime.Today.AddDays(1);
            return;
        }
        
        _waitTimeText.text = "남은 시간 : "
                             + (t.Hours > 0 ? $"{t.Hours}시간 " : "")
                             + (t.Minutes > 0 ? $"{t.Minutes}분 " : "")
                             + (t.Hours <= 0 ? $"{t.Seconds}초 " : "");
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        var dataMgr = EightUtil.GetCore<GameDataMgr>();
        _shopLocalData = dataMgr.GetCsvData(GameDataMgr.DataId.DailyShopData);
        
        StartCoroutine(Init(true));
    }

    private IEnumerator Init(bool forceInit = false)
    {
        if (_shopInfos != null && !forceInit)
        {
            InitSlots();
            yield break;
        }
        if(!LobbyShopServerConnector.IsLoaded)
            yield return new WaitUntil(() => LobbyShopServerConnector.IsLoaded);

        _shopInfos = null;
        LobbyShopServerConnector.GetDailyShop((infos, count) =>
        {
            _shopInfos = infos;
            _refreshCount = count - 1;
            LobbyRefreshUtil.OnRefresh(RefreshState.DailyShop);
            IsLoaded = true;
        });
    }

    private void InitSlots()
    {
        ReleaseSlotObjects();

        for (int i = 0; i < 6; ++i)
        {
            var info = _shopInfos[i];
            var slot = Instantiate(_slotObject, _contentObject.transform).GetComponent<ShopItemSlotObject>();
            var shopData = _shopLocalData[info.DailyPackageId][0].ToClass<DailyShopData>();

            slot.transform.SetAsLastSibling();
            slot.InitSlot(info.SlotId, shopData.ItemCategory, shopData.ItemId, info.Count, shopData.Goods,
                info.Count * shopData.Price, true);
            if(info.IsSoldOut) slot.ChangeState(ShopItemSlotObject.State.SOLD_OUT);
            else slot.ChangeState(ShopItemSlotObject.State.NONE, true);

            _slotObjects.Add(slot);
        }
    }

    private void ReleaseSlotObjects()
    {
        foreach (var slotObject in _slotObjects)
        {
            Destroy(slotObject.gameObject);
        }
        
        _slotObjects.Clear();
    }

    public void OnAdsClick()
    {
        if (_maxRefreshCount - _refreshCount <= 0)
        {
            OnRefreshFailedPopup();
            return;
        }
        
        _adsMgr.ShowRewardedAd();
    }

    public void RefreshDailyShop()
    {
        if (_maxRefreshCount - _refreshCount <= 0)
        {
            OnRefreshFailedPopup();
            return;
        }

        IsLoaded = false;
        GamePopupMgr.OpenLoadingPopup(true);
        LobbyShopServerConnector.RefreshDailyShop((infos) =>
        {
            _shopInfos = infos;
            ++_refreshCount;
            LobbyRefreshUtil.OnRefresh(RefreshState.DailyShop);
            IsLoaded = true;
            GamePopupMgr.CloseLoadingPopup();
        }, () =>
        {
            OnRefreshFailedPopup();
            IsLoaded = true;
            GamePopupMgr.CloseLoadingPopup();
        });
    }

    public static DailyShopInfo GetDailyShopInfo(int index)
    {
        return _shopInfos[index];
    }

    private void OnRefreshFailedPopup()
    {
        GamePopupMgr.OpenNormalPopup("알림", "상점 초기화는 하루에 한번씩 가능합니다.");
    }

}
