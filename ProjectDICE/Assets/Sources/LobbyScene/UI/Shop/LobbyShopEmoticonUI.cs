﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using Random = UnityEngine.Random;

public class LobbyShopEmoticonUI : MonoBehaviour
{
    [SerializeField]
    private ShopItemSlotObject _slotPrefab = null;

    [SerializeField]
    private GameObject _contents = null;

    private readonly List<ShopItemSlotObject> _slotObjects = new List<ShopItemSlotObject>();

    private EmoticonManager _emoticonManager = null;

    // Start is called before the first frame update
    void Start()
    {
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.Emoticon, RefreshSlots);
        RefreshSlots();
    }

    private void OnDestroy()
    {
        LobbyRefreshUtil.ReleaseRefreshEvent(RefreshState.Emoticon, RefreshSlots);
    }


    private void RefreshSlots()
    {
        bool needRefresh = false;
        if (PlayerPrefs.HasKey("EmoticonShopDate") == false)
        {
            needRefresh = true;
        }
        else
        {
            var str = PlayerPrefs.GetString("EmoticonShopDate");
            DateTime str_date = DateTime.Parse(str);
            DateTime cur_date = DateTime.Today;

            needRefresh = cur_date.CompareTo(str_date) > 0;
        }

        var idList = new List<int>();

        if (needRefresh == false)
        {
            if(_slotObjects.Count > 0) return;
            idList.Add(PlayerPrefs.GetInt("EmoticonShopSlot1"));
            idList.Add(PlayerPrefs.GetInt("EmoticonShopSlot2"));
            idList.Add(PlayerPrefs.GetInt("EmoticonShopSlot3"));
        }
        
        
        PlayerPrefs.SetString("EmoticonShopDate", DateTime.Today.ToString("yyyy-MM-dd"));
        ReleaseSlots();

        var emoticonList = EmoticonUtil.GetEmoticonData();
        if(_emoticonManager == null) _emoticonManager = EightUtil.GetCore<EmoticonManager>();
        var emoticonLocalList = _emoticonManager.GetEmoticonIDs();

        if (needRefresh == false)
        {
            var i = 0;
            foreach (var id in idList)
            {
                GenerateSlot(i, id);
                ++i;
            }

            return;
        }
        
        var slotList = new List<int>();

        foreach (var localId in emoticonLocalList) 
        {
            var emoticonInfo = emoticonList.Find((x) => x._emoticonId == localId);
            if (emoticonInfo != null) continue;
            
            slotList.Add(localId);
        }

        var currentSlot = new List<int>();
        for (int i = 0; i < Mathf.Min(3, slotList.Count); ++i)
        {
            var randIndex = Random.Range(0, slotList.Count);
            if (i != 0 && currentSlot.FindAll((x) => x == randIndex).Count > 0)
            {
                --i;
                continue;
            }
            currentSlot.Add(randIndex);
            GenerateSlot(i, slotList[randIndex]);

        }
        
    }

    private void GenerateSlot(int index, int id)
    {
        var slot = Instantiate(_slotPrefab, _contents.transform);
        PlayerPrefs.SetInt("EmoticonShopSlot" + (index + 1), id);
        var emoticonInfo = _emoticonManager.GetEmoticonData(id);
        slot.InitSlot(_emoticonManager.GetEmoticonShopID(emoticonInfo.DataID), ItemCategory.EMOTICON,
            emoticonInfo.DataID, 1, GoodsID.STAR, 150, true);
        _slotObjects.Add(slot);
    }

    private void ReleaseSlots()
    {
        foreach (var slotObject in _slotObjects)
        {
            Destroy(slotObject.gameObject);
        }
        _slotObjects.Clear();
    }
}
