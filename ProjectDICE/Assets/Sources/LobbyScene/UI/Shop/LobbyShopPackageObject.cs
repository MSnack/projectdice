﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class LobbyShopPackageObject : MonoBehaviour
{

    [SerializeField]
    private int _packageIndex = 0;
    public int PackageIndex => _packageIndex;

    public float Height => ((RectTransform) transform).sizeDelta.y;

    private void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }
}
