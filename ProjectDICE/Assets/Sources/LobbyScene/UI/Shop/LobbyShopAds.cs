﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;

public class LobbyShopAds : MonoBehaviour
{
    private const string _androidGameID = "3892151";
    private const string _iosGameID = "3892150";
 
    [SerializeField]
    private string _rewardedVideoId = "resetDailyShop";

    [SerializeField]
    private UnityEvent _adsFinished;
    
    [SerializeField]
    private UnityEvent _adsSkipped;
    
    [SerializeField]
    private UnityEvent _adsFailed;
 
    void Start()
    {
        Initialize();
    }
 
    private void Initialize()
    {
#if UNITY_ANDROID
        Advertisement.Initialize(_androidGameID);
#elif UNITY_IOS
        Advertisement.Initialize(_iosGameID);
#endif
    }
 
    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady(_rewardedVideoId))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
 
            Advertisement.Show(_rewardedVideoId, options);
        }
    }
 
    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
            {
                _adsFinished?.Invoke();
                break;
            }
            case ShowResult.Skipped:
            {
                _adsSkipped?.Invoke();
                break;
            }
            case ShowResult.Failed:
            {
                _adsFailed?.Invoke();
                break;
            }
        }
    }
}
