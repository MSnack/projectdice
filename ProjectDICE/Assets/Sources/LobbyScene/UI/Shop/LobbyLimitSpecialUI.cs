﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.UI;

public class LobbyLimitSpecialUI : MonoBehaviour
{

    [SerializeField]
    private Transform _content = null;
    
    [SerializeField]
    private AssetLabelReference _shopPackageAssetLabel = null;
    private IList<IResourceLocation> _assetResult = null;

    [SerializeField, ReadOnly]
    private ShopItemSlotObject _currentPackageObject = null;
    
    [SerializeField]
    private Text _waitTimeText;
    
    public static bool IsLoaded { get; private set; }
    private DateTime _tomorrow = DateTime.Today.AddDays(1);

    // Start is called before the first frame update
    void Start()
    {
        InitPackage();
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.DailyShop, () => StartCoroutine(LoadPackageCoroutine()));
    }

    private void Update()
    {
        TimeSpan t = _tomorrow - DateTime.Now;

        if (t.TotalSeconds < 0)
        {
            _tomorrow = DateTime.Today.AddDays(1);
            return;
        }
        
        _waitTimeText.text = "남은 시간 : "
                             + (t.Hours > 0 ? $"{t.Hours}시간 " : "")
                             + (t.Minutes > 0 ? $"{t.Minutes}분 " : "")
                             + (t.Hours <= 0 ? $"{t.Seconds}초 " : "");
    }

    private void OnDestroy()
    {
        if (_currentPackageObject != null)
        {
            Addressables.ReleaseInstance(_currentPackageObject.gameObject);
        }
        if (_assetResult != null)
        {
            Addressables.Release(_assetResult);
            _assetResult = null;
        }
    }

    private void InitPackage()
    {
        if (_assetResult != null)
            return;
        IsLoaded = false;

        var loadHandle = Addressables.LoadResourceLocationsAsync(_shopPackageAssetLabel.labelString, null);
        loadHandle.Completed += (result) =>
        {
            _assetResult = result.Result;
            if (_assetResult == null)
            {
                Debug.Log("Load Shop Package Failed");
                Addressables.Release(loadHandle);
                return;
            }

            IsLoaded = true;
        };
    }

    private void LoadPackage(int id, UnityAction successEvent = null)
    {
        var idStr = $"limit_{id}";
        foreach (var location in _assetResult)
        {
            if (!location.InternalId.Contains(idStr)) continue;
            
            var instantiateHandle = Addressables.InstantiateAsync(location, _content);
            instantiateHandle.Completed += (handle) =>
            {
                if(_currentPackageObject != null) Addressables.ReleaseInstance(_currentPackageObject.gameObject);
                var data = handle.Result;
                _currentPackageObject = data.GetComponent<ShopItemSlotObject>();
                successEvent?.Invoke();
            };
            break;
        }
    }

    private IEnumerator LoadPackageCoroutine()
    {
        if(IsLoaded == false) yield return new WaitUntil(() => IsLoaded);

        var package = LobbyDailySpecialUI.GetDailyShopInfo(6);
        LoadPackage(package.DailyPackageId, () =>
        {
            _currentPackageObject.InitSlot(package.SlotId);
            if(package.IsSoldOut)
                _currentPackageObject?.ChangeState(ShopItemSlotObject.State.SOLD_OUT, true);
        });
    }
}
