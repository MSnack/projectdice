﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbyUserInfoUI : MonoBehaviour
{

    [Header("NickName")]
    [SerializeField]
    private UIAlphaController _alphaController = null;

    [SerializeField]
    private Text _nicknameText = null;

    [Header("Tier")]
    [SerializeField]
    private LobbyTierObject _currentTierObject = null;

    [SerializeField]
    private LobbyTierObject _bestTierObject = null;

    [SerializeField]
    private Text _currentTrophyText = null;

    [SerializeField]
    private Text _bestTrophyText = null;

    [Header("Etc")]
    [SerializeField]
    private Text _victoryText = null;
    [SerializeField]
    private Text _defeatText = null;
    [SerializeField]
    private Text _revangeVictoryText = null;
    [SerializeField]
    private Text _revengeDefeatText = null;
    [SerializeField]
    private Text _maxWaveText = null;
    [SerializeField]
    private Text _charCountText = null;

    private GameUserMgr _userMgr = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        if(_alphaController != null)
            _alphaController.Alpha = 0;
        
        UpdateNickName();
        UpdateUserInfo();
    }

    public void UpdateNickName()
    {
        if (_userMgr == null) _userMgr = EightUtil.GetCore<GameUserMgr>();
        string nickname = _userMgr.UserInfo.NickName;
        _nicknameText.text = string.IsNullOrEmpty(nickname) ? "이름 없음" : nickname;
    }

    private void UpdateUserInfo()
    {
        GameBattleInfo battleInfo = null;
        BattleInfoServerConnector.GetBattleInfo((received) => { battleInfo = received; });

        int currentTrophy = LobbyGoodsUtil.GetGoods(GoodsID.TROPHY);
        int bestTrophy = battleInfo.BestRating;
        
        _currentTierObject.InitTier(TierUtil.GetTierState(currentTrophy));
        _bestTierObject.InitTier(TierUtil.GetTierState(bestTrophy));

        _currentTrophyText.text = currentTrophy.ToString();
        _bestTrophyText.text = bestTrophy.ToString();

        _victoryText.text = battleInfo.Win.ToString();
        _defeatText.text = battleInfo.Lose.ToString();
        _revangeVictoryText.text = battleInfo.RevWin.ToString();
        _revengeDefeatText.text = battleInfo.RevLose.ToString();
        _maxWaveText.text = battleInfo.MaxWave.ToString();
        _charCountText.text = CharacterCardUtil.UserCardLength.ToString();
    }
}
