﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbyChangeNicknamePopup : MonoBehaviour
{

    [SerializeField]
    private Text _previewText = null;

    [SerializeField]
    private Text _nickNameText = null;

    [SerializeField]
    private LobbyUserInfoUI _userInfoUi = null;

    [SerializeField]
    private Canvas[] _inactiveCanvas = null;

    private readonly GameWebRequestTranslator _translator = new GameWebRequestTranslator();
    private GameUserMgr _userMgr = null;

    private void OnEnable()
    {
        if (_userMgr == null) _userMgr = EightUtil.GetCore<GameUserMgr>();
        
        string nickname = _userMgr.UserInfo.NickName;
        if(!String.IsNullOrEmpty(nickname))
            _previewText.text = _userMgr.UserInfo.NickName;

        _nickNameText.text = "";

        foreach (var canvas in _inactiveCanvas)
        {
            canvas.gameObject.SetActive(false);
        }
    }

    private void OnDisable()
    {
        foreach (var canvas in _inactiveCanvas)
        {
            canvas.gameObject.SetActive(true);
        }
    }

    public void UpdateNickName()
    {
        string nickname = _nickNameText.text;
        string idChecker = Regex.Replace(nickname, @"[^a-zA-Z0-9ㄱ-ㅎㅏ-ㅣ가-힣]", "", RegexOptions.Singleline);
        if (nickname != idChecker)
        {
            GamePopupMgr.OpenNormalPopup("경고", "사용할 수 없는 닉네임 입니다.\n(특수문자 및 공백 사용불가)");
            _nickNameText.text = "";
            return;
        }

        if (nickname.Length < 2 || nickname.Length > 10)
        {
            GamePopupMgr.OpenNormalPopup("경고", "닉네임은 2~10자 입니다.");
            _nickNameText.text = "";
            return;
        }

        var userInfo = _userMgr.UserInfo;
        var prev_nickName = _userMgr.UserInfo.NickName;
        userInfo.NickName = nickname;
        
        _translator.SetSendData(userInfo.ToJson());
        _translator.Request(GameWebRequestType.Change_UserNickName, (trans) =>
        {
            var result = trans.GetRequestData();
            if (result["result"].ToString() == "-1")
            {
                GamePopupMgr.OpenNormalPopup("경고", "이미 존재하는 닉네임 입니다.");
                _nickNameText.text = "";
                userInfo.NickName = prev_nickName;
                return;
            }
            
            if (result["result"].ToString() == "-2")
            {
                GamePopupMgr.OpenNormalPopup("경고", "스타가 부족합니다.");
                _nickNameText.text = "";
                userInfo.NickName = prev_nickName;
                return;
            }
            
            LobbyGoodsServerConnector.GetAllGoods(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Goods));
            _userMgr.RefreshUserInfo(() => _userInfoUi.UpdateNickName());
            _previewText.text = _userMgr.UserInfo.NickName;
            gameObject.SetActive(false);
        });
    }
}
