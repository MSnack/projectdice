﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class LobbyStartBattleButton : MonoBehaviour
{
    private EightSceneMgr _sceneMgr;
    // Start is called before the first frame update
    void Start()
    {
        _sceneMgr = EightUtil.GetCore<EightSceneMgr>();
        
    }

    public void OnChangeBattleScene()
    {
        if(_sceneMgr == null) _sceneMgr = EightUtil.GetCore<EightSceneMgr>();
        _sceneMgr.ChangeScene(SCENE_TYPE.BATTLE);
    }
}
