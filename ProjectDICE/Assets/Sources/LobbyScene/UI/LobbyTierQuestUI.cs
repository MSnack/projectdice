﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyTierQuestUI : MonoBehaviour
{

    [SerializeField]
    private LobbyDailyRewardSlotObject[] _slotObjects;
    
    private DateTime _tomorrow = DateTime.Today.AddDays(1);

    private bool isNeedRefresh = false;

    private void Awake()
    {
        StartCoroutine(InitQuestInfos());
    }

    // Start is called before the first frame update
    void Start()
    {
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.Quest, RefreshQuestInfos);
    }

    private void OnEnable()
    {
        if(isNeedRefresh) RefreshQuestInfos();
    }

    private void RefreshQuestInfos()
    {
        if (enabled == false)
        {
            isNeedRefresh = true;
            return;
        }

        isNeedRefresh = false;
        StartCoroutine(InitQuestInfos());
    }

    private IEnumerator InitQuestInfos()
    {
        yield return new WaitUntil(() => QuestServerConnector.IsLoaded);

        SetQuestSlots();
        
    }
    
    private void SetQuestSlots()
    {
        var list = QuestUtil.GetQuestData(QuestCategory.TierQuest);

        for(int i = 0; i < _slotObjects.Length; ++i)
        {
            var slot = _slotObjects[i];
            var info = list[i];

            var data = QuestUtil.GetQuestLocalData(info.QuestId);
            slot.InitSlot(info, data);
        }
    }
}
