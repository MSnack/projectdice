﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyOtherPopupUI : MonoBehaviour
{
    private void Awake()
    {
        var objects = GetComponentsInChildren<LobbyOtherPopupBase>(true);
        foreach (var popupBase in objects)
        {
            popupBase.Init();
        }
    }
}
