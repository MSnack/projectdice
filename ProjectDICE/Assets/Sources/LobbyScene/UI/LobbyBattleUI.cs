﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbyBattleUI : EightReceiver, LoadInterface
{
    [SerializeField]
    private Image _illustrationImage = null;

    [Header("Membership"), SerializeField]
    private Image _ticketIcon = null;
    
    [SerializeField]
    private Sprite _crownEnable = null;

    [SerializeField]
    private Sprite _crownDisable = null;

    [SerializeField]
    private Text _membershipBarText = null;

    [SerializeField]
    private Transform _membershipBarTransform = null;
    
    

    private GameIllustMgr _gameIllustMgr = null;
    private int _illustDataId = -1;
    private bool _isLoaded = false;


    private void Awake()
    {
        SetupComplete();
    }

    private void Start()
    {
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.Quest, InitMemberShipValue);
        InitMemberShipValue();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        _gameIllustMgr = EightUtil.GetCore<GameIllustMgr>();
        StartCoroutine(InitIllustration());
    }

    private void OnDestroy()
    {
        _gameIllustMgr.ReleaseIllustSprite(_illustDataId);
    }

    private void InitMemberShipValue()
    {
        
        var list = QuestUtil.GetQuestData(QuestCategory.MembershipQuest);
        var infos = QuestUtil.GetQuestData(QuestCategory.MembershipQuest);
        QuestLocalData targetLocalData = null;
        int level = infos[infos.Count / 2 - 1].Value;
        bool isUpgraded = false;
        {
            var firstMembership = infos[infos.Count / 2];
            var localData = QuestUtil.GetQuestLocalData(firstMembership.QuestId);
            isUpgraded = firstMembership.Value >= localData.TargetValue;
        }

        _ticketIcon.sprite = isUpgraded ? _crownEnable : _crownDisable;

        int size = list.Count / 2;
        for (int i = 0; i < size; ++i)
        {
            var questInfos = new[] { list[i], list[i + size] };
            var nextQuestLevel = (i + 1 < size) ? QuestUtil.GetQuestLocalData(list[i + 1].QuestId).TargetValue : -1;
            var targetLevel = QuestUtil.GetQuestLocalData(questInfos[0].QuestId).TargetValue;
            if (level >= targetLevel)
            {
                if (level >= nextQuestLevel) continue;
                targetLocalData = null;
                continue;
            }
            targetLocalData = QuestUtil.GetQuestLocalData(questInfos[0].QuestId);
            break;
        }
        
        float exp = ((float)level / targetLocalData?.TargetValue) ?? -1;
        _membershipBarText.text = exp < 0 ? "패스 완료" : $"{level:D}/{(targetLocalData?.TargetValue ?? -1):D}";
        _membershipBarTransform.localScale = new Vector3(exp < 0 ? 1 : exp, 1, 1);
    }

    private IEnumerator InitIllustration()
    {
        if(LobbyUserDeckServerConnector.IsLoaded == false)
            yield return new WaitUntil(() => LobbyUserDeckServerConnector.IsLoaded);

        bool isReceived = false;
        bool isOverlap = false;
        BattleInfoServerConnector.GetBattleInfo((info) =>
        {
            if (_illustDataId == info.FavorCharId && _illustrationImage.sprite != null)
            {
                isOverlap = true;
                return;
            }

            _illustDataId = info.FavorCharId;
            isReceived = true;
        });
        if(isOverlap) yield break;
        _illustrationImage.gameObject.SetActive(false);
        if(isReceived == false)
            yield return new WaitUntil(() => isReceived);
        

        if(_gameIllustMgr == null)
            _gameIllustMgr = EightUtil.GetCore<GameIllustMgr>();
        if(_gameIllustMgr.IsLoaded == false)
            yield return new WaitUntil(() => _gameIllustMgr.IsLoaded);
        
        if(_illustDataId >= 0) _gameIllustMgr.ReleaseIllustSprite(_illustDataId);
        _gameIllustMgr.GetIllustSprite(_illustDataId, (sprite) =>
        {
            _illustrationImage.gameObject.SetActive(true);
            _illustrationImage.sprite = sprite;
            _isLoaded = true;
        });
    }

    public void RefreshIllustration()
    {
        _isLoaded = false;
        StartCoroutine(InitIllustration());
    }

    public bool IsLoaded()
    {
        return _isLoaded;
    }
}
