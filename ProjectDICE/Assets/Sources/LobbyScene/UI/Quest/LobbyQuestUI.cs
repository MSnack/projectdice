﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbyQuestUI : MonoBehaviour
{
    private readonly Dictionary<QuestCategory, List<LobbyQuestSlotObject>> _questSlotLists =
        new Dictionary<QuestCategory, List<LobbyQuestSlotObject>>();

    [SerializeField]
    private GameObject _dailyQuestContent;

    [SerializeField]
    private GameObject _dailyRewardContent;

    [SerializeField]
    private GameObject _dailyShopSlotObject;

    [SerializeField]
    private GameObject _questSlotObject;

    [SerializeField]
    private ContentSizeFitter csf;
    
    [SerializeField]
    private Text _waitTimeText;

    
    private DateTime _tomorrow = DateTime.Today.AddDays(1);
    
    private void Awake()
    {
        StartCoroutine(InitQuestInfos());
    }

    // Start is called before the first frame update
    private void Start()
    {
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.Quest, RefreshQuestInfos);
    }

    private void Update()
    {
        if (_waitTimeText == null) return;
        TimeSpan t = _tomorrow - DateTime.Now;
        
        if (t.TotalSeconds + 1 < 0)
        {
            QuestServerConnector.RefreshUserQuestInfos(() => LobbyRefreshUtil.OnRefresh(RefreshState.Quest));
            return;
        }
        
        _waitTimeText.text = "남은 시간 : "
                             + (t.Hours > 0 ? $"{t.Hours}시간 " : "")
                             + (t.Minutes > 0 ? $"{t.Minutes}분 " : "")
                             + (t.Hours <= 0 ? $"{t.Seconds}초 " : "");
    }

    public void GetQuestInfos()
    {
        LobbyRefreshUtil.OnRefresh(RefreshState.QuestValue);
    }

    private void RefreshQuestInfos()
    {
        StartCoroutine(InitQuestInfos());
    }

    private IEnumerator InitQuestInfos()
    {
        yield return new WaitUntil(() => QuestServerConnector.IsLoaded);

        ReleaseQuestObjects();


        GenerateQuestSlot(QuestCategory.DailyQuest, _questSlotObject, _dailyQuestContent);
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform) csf.transform);
        GenerateQuestSlot(QuestCategory.DailyReward, _dailyShopSlotObject, _dailyRewardContent);
    }

    private void GenerateQuestSlot(QuestCategory category, GameObject slotPrefab, GameObject parent)
    {
        var list = QuestUtil.GetQuestData(category);
        _questSlotLists.Add(category, new List<LobbyQuestSlotObject>());

        foreach (var questInfo in list)
        {
            var obj = Instantiate(slotPrefab, parent.transform)
                .GetComponent<LobbyQuestSlotObject>();
            var data = QuestUtil.GetQuestLocalData(questInfo.QuestId);
            obj.InitSlot(questInfo, data);
            _questSlotLists[category].Add(obj);
        }
    }

    private void OnDestroy()
    {
        ReleaseQuestObjects();
    }

    private void ReleaseQuestObjects()
    {
        foreach (var listPair in _questSlotLists)
        foreach (var slotObject in listPair.Value)
            Destroy(slotObject.gameObject);
        _questSlotLists.Clear();
    }
}