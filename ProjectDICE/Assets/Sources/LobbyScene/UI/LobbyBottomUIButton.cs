﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LobbyBottomUIButton : MonoBehaviour
{
    [SerializeField]
    private LobbyFSMSystem.LobbyFSMState _state = LobbyFSMSystem.LobbyFSMState.BATTLE;
    public LobbyFSMSystem.LobbyFSMState State => _state;

    [SerializeField]
    private Button _button = null;

    [SerializeField]
    private FontGradient _imageGradient = null;
    
    [SerializeField]
    private FontGradient _textGradient = null;

    [Header("Active-Inactive Animation")]
    [SerializeField]
    private AnimationCurve _animationCurve = AnimationCurve.Linear(0, 0, 1, 1);

    [SerializeField]
    private float _totalTime = 0.2f;

    [SerializeField]
    private Vector3 _activeSize = Vector3.one;
    [SerializeField]
    private Gradient _activeGradient = new Gradient();
    
    [Space(8)]
    [SerializeField]
    private Vector3 _inactiveSize = Vector3.one;
    [SerializeField]
    private Gradient _inactiveGradient = new Gradient();

    private Vector2 _startRect;
    private Vector2 _targetRect;

    private Coroutine _currCoroutine = null;
    private bool _isActive = false;

    [SerializeField]
    private List<LobbyFSMSystem.LobbyFSMState> _moreStates = null;

    // Start is called before the first frame update
    void Start()
    {
        LobbySceneInterface.OnChangingState += OnStateChanging;
        Vector2 sizeDelta = ((RectTransform) transform).sizeDelta;
        _startRect = sizeDelta;
        _targetRect = new Vector2(sizeDelta.x * _activeSize.x, sizeDelta.y * _activeSize.y);
    }


    public void OnClicked()
    {
        LobbySceneInterface.InvokeChangingState(_state);
    }

    private void OnStateChanging(LobbyFSMSystem.LobbyFSMState state)
    {
        if(_currCoroutine != null) StopCoroutine(_currCoroutine);

        bool moreStateResult = _moreStates != null && _moreStates.Exists((fsmState) => fsmState == state);
        
        if (state != _state)
        {
            if (moreStateResult) return;
            
            _button.enabled = true;
            
            if (!_isActive) return;
            _isActive = false;
            _currCoroutine = StartCoroutine(OnAnimation(false));
            return;
        }

        _isActive = true;
        _button.enabled = false;
        _currCoroutine = StartCoroutine(OnAnimation(true));
    }

    private IEnumerator OnAnimation(bool isActive)
    {
        float elapsedTime = 0;
        Vector2 fv = isActive ? _startRect : _targetRect;
        Vector2 tv = isActive ? _targetRect : _startRect;
        // Color fc = isActive ? _inactiveColor : _activeColor;
        // Color tc = isActive ? _activeColor : _inactiveColor;

        Gradient fg = isActive ? _inactiveGradient : _activeGradient;
        Gradient tg = isActive ? _activeGradient : _inactiveGradient;

        for (elapsedTime = 0; elapsedTime < _totalTime; elapsedTime += Time.deltaTime)
        {
            float v = _animationCurve.Evaluate(elapsedTime / _totalTime);

            ((RectTransform)transform).sizeDelta = Vector2.Lerp(fv, tv, v);
            // Color c = Color.Lerp(fc, tc, v);
            Gradient gradient = FontGradient.Lerp(fg, tg, v, true, false);

            _imageGradient.GradientColor = gradient;
            _imageGradient.SetDirty();
            _textGradient.GradientColor = gradient;
            _textGradient.SetDirty();
            yield return null;
        }

        ((RectTransform)transform).sizeDelta = tv;
        _imageGradient.GradientColor = tg;
        _textGradient.GradientColor = tg;
        _imageGradient.SetDirty();
        _textGradient.SetDirty();

        _currCoroutine = null;
        yield break;
    }

    private void Reset()
    {
        _activeGradient.colorKeys = new[]
        {
            new GradientColorKey(new Color(1, 0.78f, 0.43f), 0),
            new GradientColorKey(new Color(1, 0.78f, 0.43f), 1)
        };
        
        _inactiveGradient.colorKeys = new[]
        {
            new GradientColorKey(new Color(0.56f, 0.62f, 0.75f), 0),
            new GradientColorKey(new Color(0.56f, 0.62f, 0.75f), 1)
        };
    }
}
