﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LobbyNoticeUtil : MonoBehaviour
{

    // Quest 관련 상태는 QuestUtil에서 작동합니다.
    public enum NoticeState
    {
        Cards,
        MailBox,
    }

    private static readonly Dictionary<NoticeState, UnityAction<bool>> _noticeEvents
        = new Dictionary<NoticeState, UnityAction<bool>>();

    private static readonly Dictionary<NoticeState, bool> _noticeStates
        = new Dictionary<NoticeState, bool>();

    public static void SetNoticeEvent(NoticeState state, UnityAction<bool> noticeEvent)
    {
        if (!_noticeEvents.ContainsKey(state))
        {
            _noticeEvents.Add(state, null);
            _noticeStates.Add(state, false);
        }

        _noticeEvents[state] += noticeEvent;
    }

    public static void ReleaseNoticeEvent(NoticeState state, UnityAction<bool> noticeEvent)
    {
        if (!_noticeEvents.ContainsKey(state)) return;

        if (noticeEvent != null) 
            _noticeEvents[state] -= noticeEvent;
    }
    
    public static void OnNotice(NoticeState state, bool isActive = true)
    {
        if (!_noticeEvents.ContainsKey(state))
        {
            _noticeEvents.Add(state, null);
            _noticeStates.Add(state, false);
        }
        
        _noticeStates[state] = isActive;
        _noticeEvents[state]?.Invoke(isActive);
    }

    public static bool IsNotice(NoticeState state)
    {
        return _noticeStates.ContainsKey(state) && _noticeStates[state];
    }
    
    private void ReleaseAllActions()
    {
        _noticeEvents.Clear();
        _noticeStates.Clear();
    }

    private void Awake()
    {
        ReleaseAllActions();
    }

    private void OnDestroy()
    {
        ReleaseAllActions();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }
}
