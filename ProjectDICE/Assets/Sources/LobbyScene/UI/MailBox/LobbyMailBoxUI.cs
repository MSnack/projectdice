﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class LobbyMailBoxUI : EightReceiver
{

    [SerializeField]
    private GameObject _content = null;

    [SerializeField]
    private GameObject _slotGameObject = null;

    [SerializeField]
    private UIAlphaController _alphaController = null;

    [SerializeField]
    private GameObject _emptyText = null;
    
    
    private readonly List<MailSlotObject> _slotObjects = new List<MailSlotObject>();
    

    private void Awake()
    {
        SetupComplete();
    }

    // Start is called before the first frame update
    void Start()
    {
        _emptyText.SetActive(true);
    }


    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.Mails, RefreshSlots);
        
        StartCoroutine(InitSlots());
    }

    private IEnumerator InitSlots()
    {
        yield return new WaitUntil(() => LobbyMailServerConnector.IsLoaded);
        RefreshSlots();
    }

    private void RefreshSlots()
    {
        ReleaseAllSlot();
        var slotList = MailUtil.GetMailData();
        foreach (var info in slotList)
        {
            AddSlot(info);
        }
        if(slotList.Count > 0) LobbyNoticeUtil.OnNotice(LobbyNoticeUtil.NoticeState.MailBox);
    }

    private void AddSlot(MailInfo info)
    {
        if (info.IsReceived) return;
        var obj = Instantiate(_slotGameObject, _content.transform);
        var mailSlot = obj.GetComponent<MailSlotObject>();
        
        mailSlot.InitData(info);
        _slotObjects.Add(mailSlot);
        _emptyText.SetActive(false);
    }

    private void ReleaseAllSlot()
    {
        foreach (var obj in _slotObjects)
        {
            Destroy(obj.gameObject);
        }
        
        _slotObjects.Clear();
        _emptyText.SetActive(true);
        LobbyNoticeUtil.OnNotice(LobbyNoticeUtil.NoticeState.MailBox, false);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        _alphaController.Alpha = 0;
    }

    public void OpenAllMails()
    {
        StartCoroutine(OpenAllMailsCoroutine());
    }

    private IEnumerator OpenAllMailsCoroutine()
    {
        if (_slotObjects.Count <= 0) yield break;
        bool isCharacterExist = false;
        bool isAllBoxes = true;
        List<RewardItemInfo> infos = new List<RewardItemInfo>();
        
        foreach (var mail in _slotObjects)
        {
            if (mail.ItemCategory == ItemCategory.BOX) continue;
            isAllBoxes = false;
            bool isReceived = false;
            mail.OnReceiveMail((trans) =>
            {
                if (mail.ItemCategory == ItemCategory.CHARACTER) isCharacterExist = true;
                isReceived = true;
                var info = new RewardItemInfo
                    {ItemCategory = (int) mail.ItemCategory, Id = mail.Info.ItemId, Count = mail.Info.ItemCount};
                infos.Add(info);
            });
            
            yield return new WaitUntil(() => isReceived);
        }
        
        if(isAllBoxes) yield break;

        if (isCharacterExist)
        {
            LobbyCharacterCardServerConnector.GetCurrentCharList(() =>
            {
                LobbyRefreshUtil.OnRefresh(RefreshState.Organization);
                LobbyRefreshUtil.OnRefresh(RefreshState.Cards);
            });
            LobbyNoticeUtil.OnNotice(LobbyNoticeUtil.NoticeState.Cards);
        }
        
        // LobbyGoodsServerConnector.GetAllGoods(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Goods));
        LobbyMailServerConnector.GetMails(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Mails));

        GamePopupMgr.CloseLoadingPopup();
        GamePopupMgr.OpenRewardPopup(infos);
    }
}
