﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;

public class LobbyUITouchSlider : MonoBehaviour
{
    [SerializeField]
    private RectTransform[] _transforms;

    [SerializeField]
    private LobbyFSMSystem _system = null;
    
    private Vector2 _currPos;
    private Vector2 _deltaPos;
    private Vector2 _startPos;

    private Vector3 _startTransformPos;

    [SerializeField]
    private float _dragAccuracy;

    [SerializeField]
    private float _dragSpeed;
    private bool _isDragging = false;
    private bool _isDraggingBlocked = false;
    public bool IsDragging => _isDragging;

    private float _maxDragWidth = 0;

    [SerializeField]
    private GameObject _blockTouchObject = null;

    private Vector2 _screenRatio;

    [SerializeField]
    private float _lerpTime = 1;

    private float _lerpDeltaTime = 0;

    [SerializeField]
    private int _stateCount = 5;

    private UnityAction<bool> _draggingEvent = null;
    public event UnityAction<bool> DraggingEvent
    {
        add
        {
            if (value != null) _draggingEvent += value;
        }

        remove
        {
            if (value != null) _draggingEvent -= value;
        }
    }

    private bool _blockEvent = false;

    public void SetBlockEvent(bool active)
    {
        _blockEvent = active;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        var rect = ((RectTransform) transform).rect;
        _screenRatio = new Vector2(rect.width / Screen.width, rect.height / Screen.height);
        int count = (_stateCount - 1) / 2;
        _maxDragWidth = (count * rect.width);
    }

    // Update is called once per frame
    void Update()
    {
        if (_blockEvent || !_system.IsBottomMenuState() || LobbyPopupFSMSystem.IsPopupActivated) return;
        
        if (Input.GetMouseButton(0))
        {
            _currPos = (Input.touchCount == 0) ? (Vector2)Input.mousePosition : Input.GetTouch(0).position;

            if (Input.GetMouseButtonDown(0))
            {
                _startPos = _currPos;
                _startTransformPos = transform.localPosition;
                _lerpDeltaTime = 0;
            }

            _deltaPos = (_startPos - _currPos) * _screenRatio;

            if (!_isDragging && Math.Abs(_deltaPos.y) > _dragAccuracy * 0.5f)
            {
                _isDraggingBlocked = true;
            }
            else if(!_isDraggingBlocked && (_isDragging || Math.Abs(_deltaPos.x) > _dragAccuracy))
            {
                var speed = _deltaPos.x * _dragSpeed;
                var pos = transform.localPosition;
                _lerpDeltaTime += Time.deltaTime;
                if (Math.Abs(_startTransformPos.x - speed) > _maxDragWidth)
                {
                    float x = transform.localPosition.x;
                    // transform.localPosition = new Vector3(_startTransformPos.x, pos.y, pos.z);
                    var resultPosition = new Vector3(x < 0 ? -_maxDragWidth : _maxDragWidth, pos.y, pos.z);
                    foreach (var t in _transforms)
                    {
                        t.localPosition = resultPosition;
                    }
                }
                else
                {
                    float lerpSpeed = Mathf.Lerp(0, speed, Math.Min(_lerpDeltaTime / _lerpTime, 1));
                    var resultPosition = new Vector3(_startTransformPos.x - lerpSpeed, pos.y, pos.z);
                    foreach (var t in _transforms)
                    {
                        t.localPosition = resultPosition;
                    }
                    
                    _isDragging = true;
                    _draggingEvent?.Invoke(true);
                    _blockTouchObject.SetActive(true);
                }
            }
            else
            {
                _lerpDeltaTime = 0;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (_isDragging && Math.Abs(_deltaPos.x) > _dragAccuracy)
            {
                var currState = _system.CurrSubState == LobbyFSMSystem.LobbyFSMState.NONE
                    ? _system.CurrState
                    : _system.CurrSubState;
                LobbyFSMSystem.LobbyFSMState state = currState + ((_deltaPos.x < 0) ? -1 : 1);
                if(Enum.IsDefined(typeof(LobbyFSMSystem.LobbyFSMState), state)
                    && _stateCount > (int) state)
                    LobbySceneInterface.InvokeChangingState(state);
            }
            else if (_isDragging)
            {
                LobbyUIMovement.OnMove(_startTransformPos);
            }
            else
            {
                _blockTouchObject.SetActive(false);
            }

            _isDragging = false;
            _isDraggingBlocked = false;
            _draggingEvent?.Invoke(false);
        }
    }
}
