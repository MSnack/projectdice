﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class LobbyRewardEffectUtil : MonoBehaviour
{
    private static UnityAction<GoodsID, Transform, Transform> _playEffectEvent = null;

    [SerializeField]
    private GameObject _goldEffectContent = null;
    
    [SerializeField]
    private GameObject _starEffectContent = null;
    
    [SerializeField]
    private GameObject _trophyEffectContent = null;
    
    // Start is called before the first frame update
    void Start()
    {
        _playEffectEvent += PlayEffectFunc;
    }

    private void OnDestroy()
    {
        _playEffectEvent = null;
    }

    public static void PlayEffect(GoodsID id, Transform from, Transform to = null)
    {
        _playEffectEvent?.Invoke(id, from, to);
    }

    private void PlayEffectFunc(GoodsID id, Transform src, Transform dst)
    {
        GameObject content = null;
        switch (id)
        {
            case GoodsID.GOLD:
                content = _goldEffectContent;
                break;
            case GoodsID.STAR:
                content = _starEffectContent;
                break;
            case GoodsID.TROPHY:
                content = _trophyEffectContent;
                break;
        }

        if (content == null) return;
        var effects = content.GetComponents<LobbyRewardEffectObject>();
        StartCoroutine(PlayEffectCoroutine(effects, src, dst));
    }

    private IEnumerator PlayEffectCoroutine(LobbyRewardEffectObject[] effects, Transform src, Transform dst)
    {
        foreach (var effect in effects)
        {
            effect.transform.position = Vector3.zero;
            effect.StartPosition = src;
            if(dst != null) effect.TargetPosition = dst;
            effect.PlayEffect();

            var delayTime = Random.Range(0.05f, 0.3f);
            yield return new WaitForSeconds(delayTime);
        }
    }
}
