﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbySetAnimatorParam : MonoBehaviour
{
    [SerializeField]
    private Animator _animator = null;

    public void SetTrue(string param)
    {
        _animator?.SetBool(param, true);
    }
    
    public void SetFalse(string param)
    {
        _animator?.SetBool(param, false);
    }
}
