﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UniRx.Triggers;
using UnityEngine;

public class LobbyCharListContent : EightReceiver
{

    [SerializeField]
    private CardSlotObject _slotObject = null;

    [SerializeField]
    private LobbyCharListGridLayoutGroup _grid = null;

    [SerializeField]
    private LobbyOrganizationScrollSelectMgr _selectMgr = null;

    private readonly List<CardSlotObject> _slotObjects = new List<CardSlotObject>();
    private readonly List<CardSlotObject> _slotUnlockObjects = new List<CardSlotObject>();

    private GameUnitDataMgr _gameUnitDataMgr = null;

    [SerializeField, ReadOnly]
    private int _filterIndex = 0;
    public int FilterIndex
    {
        get => _filterIndex;
        set => _filterIndex = value;
    }

    [SerializeField]
    private Material _grayScaleMaterial = null;

    [SerializeField]
    private RectTransform _viewportTransform = null;
    private Canvas _canvas = null;

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        _gameUnitDataMgr = EightUtil.GetCore<GameUnitDataMgr>();
    }

    // Start is called before the first frame update
    void Start()
    {
        var rect = (RectTransform) transform;
        rect.offsetMin = new Vector2(0, rect.offsetMin.y);
        rect.offsetMax = new Vector2(0, rect.offsetMax.y);
        _canvas = GetComponentInParent<Canvas>();
    }

    public void UpdateSlotRender()
    {
        if (_canvas.worldCamera == null) return;
        var panelPosition = WorldToUISpace(_canvas, _viewportTransform.position);
        var rect = new Rect(0, panelPosition.y, 100, _viewportTransform.rect.height);
        
        foreach (var slotObject in _slotObjects)
        {
            if (slotObject.gameObject.activeInHierarchy == false) continue;
            var rectTransform = (RectTransform) slotObject.transform;
            var slotPosition = WorldToUISpace(_canvas, rectTransform.position);
            var slotRect = new Rect(0, slotPosition.y, 100, rectTransform.rect.height);

            
            if (rect.Overlaps(slotRect))
            {
                if (slotObject.IsEnableRender) continue;
                slotObject.SetCardRender(true);
            }
            else
            {
                if (!slotObject.IsEnableRender) continue;
                slotObject.SetCardRender(false);
            }
        }
    }

    public CardSlotObject CreateSlot(CharacterUnitData data)
    {
        var orig = _slotObjects.Find((x) => x.Id == data.DataID);
        if (orig != null)
        {
            return orig;
        }
        
        var obj = Instantiate(_slotObject, transform);
        obj.InitData(data);
        obj.transform.SetAsFirstSibling();
        _selectMgr.SetSelector(obj.GetComponent<LobbyOrganizationScrollSelector>());
        _slotObjects.Add(obj);
        if(_filterIndex != 0 && _filterIndex != data.Grade) obj.gameObject.SetActive(false);
        return obj;
    }

    public CardSlotObject CreateUnlockSlot(CharacterUnitData data)
    {
        var obj = Instantiate(_slotObject, transform);
        obj.InitData(data);
        obj.SetUnlock(_grayScaleMaterial);
        if(_slotUnlockObjects == null) ResetUnlockSlot();
        _slotUnlockObjects.Add(obj);
        obj.gameObject.SetActive(false);

        return obj;
    }

    public void ResetUnlockSlot()
    {
        foreach (var slotObject in _slotUnlockObjects)
        {
            Destroy(slotObject.gameObject);
        }
        _slotUnlockObjects.Clear();
    }

    public void ReleaseAllSlot()
    {
        foreach (var slotObject in _slotObjects)
        {
            Destroy(slotObject.gameObject);
        }
        
        _slotObjects.Clear();
    }

    public void RefreshFilter(int filterIndex)
    {
        if (_filterIndex == filterIndex) return;
        if (_filterIndex < 0)
        {
            foreach (var slotObject in _slotUnlockObjects)
            {
                slotObject.gameObject.SetActive(false);
            }
        }
        _filterIndex = filterIndex;

        if (filterIndex >= 0)
        {
            foreach (var slotObject in _slotObjects)
            {
                var data = slotObject.UnitData;
                if (_filterIndex != 0 && _filterIndex != data.Grade)
                {
                    slotObject.gameObject.SetActive(false);
                }
                else
                {
                    slotObject.gameObject.SetActive(true);
                }
            }
        }
        else
        {
            foreach (var slotObject in _slotObjects)
            {
                slotObject.gameObject.SetActive(false);
            }
            foreach (var slotObject in _slotUnlockObjects)
            {
                slotObject.gameObject.SetActive(true);
            }
        }

        
    }
    
    private Vector3 WorldToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
        Vector3 screenPos = parentCanvas.worldCamera.WorldToScreenPoint(worldPos);
        Vector2 movePos;

        //Convert the screenpoint to ui rectangle local point
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        return movePos;
    }
}
