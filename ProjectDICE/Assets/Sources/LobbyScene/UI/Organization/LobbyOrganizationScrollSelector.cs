﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LobbyOrganizationScrollSelector : MonoBehaviour
{

    private LobbyOrganizationScrollSelectMgr _selectMgr = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void InitSelector(LobbyOrganizationScrollSelectMgr selectMgr)
    {
        _selectMgr = selectMgr;
    }

    public void OnSelect()
    {
        _selectMgr?.OnSelect(this);
    }
    
}
