﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class LobbyDeckLoader : EightReceiver
{
    [SerializeField]
    private CardSlotObject[] _cardList;
    
    private GameUnitDataMgr _gameUnitDataMgr;
    
    
    private void Awake()
    {
        SetupComplete();
    }

    
    // Start is called before the first frame update
    void Start()
    {
        if(_cardList == null) GetCardObjects();
    }
    
    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        _gameUnitDataMgr = EightUtil.GetCore<GameUnitDataMgr>();
        StartCoroutine(OnLoaded());
    }

    public void LoadDecks(int index)
    {
        if (!LobbyUserDeckServerConnector.IsLoaded) return;
        LobbyUserDeckServerConnector.GetDeckData(index, LoadDecks);
    }

    private IEnumerator OnLoaded()
    {
        yield return new WaitUntil(() => LobbyUserDeckServerConnector.IsLoaded);
        yield return new WaitUntil(() => EightUtil.GetCore<GameUnitDataMgr>().IsLoadCompleted);
        LoadDecks(LobbyUserDeckServerConnector.GetStartDeckIndex());
    }

    private void LoadDecks(DeckInfo deckInfo)
    {
        int[] charId = { deckInfo.deck1, deckInfo.deck2, deckInfo.deck3, deckInfo.deck4, deckInfo.deck5 };
        int index = 0;
        
        if(_cardList == null) GetCardObjects();
        foreach (var card in _cardList)
        {
            var data = _gameUnitDataMgr.GetCharacaterData(charId[index]);
            card.InitData(data);
            ++index;
        }
    }

    private void GetCardObjects()
    {
        _cardList = GetComponentsInChildren<CardSlotObject>(true);
        _gameUnitDataMgr = EightUtil.GetCore<GameUnitDataMgr>();
    }
}
