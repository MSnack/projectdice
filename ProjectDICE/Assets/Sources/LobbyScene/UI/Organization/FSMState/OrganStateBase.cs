﻿using System.Collections;
using System.Collections.Generic;
using EightWork.FSM;
using UnityEngine;

public abstract class OrganStateBase : EightFSMStateBase<OrganFSMSystem.OrganFSMState, OrganFSMSystem>
{
    [SerializeField]
    private GameObject[] _activeObjects;

    [SerializeField]
    private GameObject[] _inactiveObjects;


    protected bool _isLoaded = false;
    public bool IsLoaded => _isLoaded;


    public override void StartState()
    {
        OrganFSMInterface.InvokeChangedState(State);
        foreach (var obj in _activeObjects)
        {
            obj.SetActive(true);
        }
        
        foreach (var obj in _inactiveObjects)
        {
            obj.SetActive(false);
        }
    }

    public override void EndState()
    {
        foreach (var obj in _activeObjects)
        {
            obj.SetActive(false);
        }

        foreach (var obj in _inactiveObjects)
        {
            obj.SetActive(true);
        }
    }
}
