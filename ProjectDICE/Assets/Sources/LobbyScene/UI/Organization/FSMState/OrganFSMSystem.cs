﻿using System.Collections;
using System.Collections.Generic;
using EightWork.FSM;
using UnityEngine;

public class OrganFSMSystem : EightFSMSystem<OrganFSMSystem.OrganFSMState, OrganStateBase, OrganFSMInterface>
{
    public enum OrganFSMState
    {
        COLLECTION,
        DECK,
        EMOTICON,
        THEME,
    }
    
    protected override void Awake()
    {
        base.Awake();
        
        SetupComplete();
    }

    // Start is called before the first frame update
    void Start()
    {
        OrganFSMInterface.CallChangingState += ChangeStateFunction;
    }
    
    private void ChangeStateFunction(OrganFSMState state)
    {
        if (CurrState == state) return;
        ChangeState(state);
    }
}
