﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrganEmoticonState : OrganStateBase
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = OrganFSMSystem.OrganFSMState.EMOTICON;
    }
}
