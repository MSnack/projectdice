﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyOrganizationEditCardObject : CardSlotObject
{

    [SerializeField]
    private RectTransform _canvasRect;

    [Tooltip("수치가 커지면 러프속도가 느려집니다.")]
    [SerializeField]
    private float _lerpSpeed = 4;
    
    private Vector3 _startPosition;
    private Vector3 _targetPosition;
    private Vector2 _screenRatio;

    private Vector2 _prevPos;
    private bool _isDrag = false;

    // Start is called before the first frame update
    void Start()
    {
        var rect = _canvasRect.rect;
        _screenRatio = new Vector2(rect.width / Screen.width, rect.height / Screen.height);

    }

    protected override void OnDisable()
    {
        base.OnDisable();
        _targetPosition = Vector3.zero;
        transform.localPosition = new Vector3(0, -100, 0);
    }

    protected override void Update()
    {
        var prevPosition = transform.localPosition;
        
        if (Input.GetMouseButton(0))
        {

            var currPos = (Input.touchCount == 0) ? (Vector2)Input.mousePosition : Input.GetTouch(0).position;

            if (Input.GetMouseButtonDown(0) && RectTransformUtility.RectangleContainsScreenPoint(
                GetComponent<RectTransform>(), Input.mousePosition, _mainCamera))
            {
                _prevPos = currPos;
                _startPosition = transform.localPosition;
                _isDrag = true;
            }

            if (_isDrag)
            {
                var deltaCurPos = (currPos - _prevPos) * _screenRatio;
                _targetPosition = _startPosition + new Vector3(deltaCurPos.x, deltaCurPos.y, 0);
            }

        }

        if (Input.GetMouseButtonUp(0))
        {
            _targetPosition = Vector3.zero;
            _isDrag = false;
        }
        
        var deltaPos = prevPosition - _targetPosition;
        deltaPos /= _lerpSpeed;

        var localPosition = transform.localPosition;
        
        localPosition -= deltaPos;
        transform.localPosition = localPosition;
        
        var scale = 1.5f - (localPosition.y / _canvasRect.rect.height);
        transform.localScale = new Vector3(scale, scale, 1);
    }

    public override void OnClicked()
    {
        
    }
}
