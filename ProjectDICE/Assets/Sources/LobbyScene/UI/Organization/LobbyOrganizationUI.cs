﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbyOrganizationUI : EightReceiver
{
    
    private GameUnitDataMgr _gameUnitDataMgr;

    [SerializeField]
    private List<int> _characterList = null;

    [SerializeField]
    private LobbyCharListContent _content = null;

    [SerializeField]
    private Text _currentCharCount = null;

    [SerializeField]
    private LobbyOrganizationFilter _filter = null;

    private Coroutine _updateListCoroutine = null;
    
    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        _gameUnitDataMgr = EightUtil.GetCore<GameUnitDataMgr>();
        StartCoroutine(UpdateCharacterListRoutine());
    }

    private IEnumerator UpdateCharacterListRoutine()
    {
        if (_gameUnitDataMgr.IsLoadCompleted == false || !LobbyCharacterCardServerConnector.IsLoaded)
            yield return new WaitUntil(() => _gameUnitDataMgr.IsLoadCompleted == true
                                             && LobbyCharacterCardServerConnector.IsLoaded);
        UpdateCharacterList();
        StartCoroutine(UpdateCharScrollView());
    }

    // Start is called before the first frame update
    void Start()
    {
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.Organization, RefreshScrollViewList);
    }
    

    private void UpdateCharacterList()
    {
        _characterList.Clear();

        foreach (var pair in CharacterCardUtil.GetAllCardDetails())
        {
            _characterList.Add(pair.Key);
        }

    }

    private IEnumerator UpdateCharScrollView()
    {
        yield return new WaitUntil(() => _gameUnitDataMgr.IsLoadCompleted 
                                         && CharacterCardUtil.UserCardLength > 0
                                         && LobbyCharacterCardServerConnector.IsLoaded
                                         && EightUtil.GetCore<GameDataMgr>().IsLoaded);

        _characterList = _characterList.OrderByDescending((x) => x).ToList();
        var allCharList = _gameUnitDataMgr.CharactersIDList.ToList();
        foreach (int i in _characterList)
        {
            var data = _gameUnitDataMgr.GetCharacaterData(i);
            if (data == null) continue;
            var slot = _content.CreateSlot(data);
            allCharList.Remove(i);
        }

        foreach (int i in allCharList)
        {
            var data = _gameUnitDataMgr.GetCharacaterData(i);
            if (data == null) continue;
            var slot = _content.CreateUnlockSlot(data);
        }

        if(_currentCharCount != null)
            _currentCharCount.text = $"{CharacterCardUtil.UserCardLength}/{_gameUnitDataMgr.CharactersIDList.Length}";
        
        _content.RefreshFilter(0);
        _filter.RefreshFilter();
    }

    private void RefreshScrollViewList()
    {
        UpdateCharacterList();
        _content.ReleaseAllSlot();
        _content.ResetUnlockSlot();
        StartCoroutine(UpdateCharScrollView());
    }
}
