﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class LobbyUserDeckMgr : EightReceiver
{

    [SerializeField]
    private LobbyDeckLoader _deckLoader = null;
    
    [SerializeField, ReadOnly]
    private int _currentDeckIndex = 1;

    [SerializeField, ReadOnly()]
    private int _currentSelectedCharIndex = SystemIndex.InvalidInt;

    [SerializeField]
    private GameObject[] _activeDeckButtons;

    public int CurrentDeckIndex
    {
        get => _currentDeckIndex;
        set
        {
            _currentDeckIndex = value;
            _deckLoader.LoadDecks(value);
            for (int i = 0; i < _activeDeckButtons.Length; i++)
            {
                var bt = _activeDeckButtons[i];
                bt.SetActive(i == value - 1);
            }

            LobbyUserDeckServerConnector.SetCurrentDeckIndex(value);
        }
    }

    public int CurrentSelectedCharIndex
    {
        get => _currentSelectedCharIndex;
        set => _currentSelectedCharIndex = value;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Init());
    }


    private IEnumerator Init()
    {
        yield return new WaitUntil(() => LobbyUserDeckServerConnector.IsLoaded);

        _currentDeckIndex = LobbyUserDeckServerConnector.GetStartDeckIndex();
        _activeDeckButtons[_currentDeckIndex - 1].SetActive(true);
        LoadDecks(_currentDeckIndex);
    }
    
    private void UpdateDeck(int index, DeckInfo info)
    {
        --index;
        
        //중복 체크
        int[] charId = { info.deck1, info.deck2, info.deck3, info.deck4, info.deck5 };
        var findIndex = Array.IndexOf(charId, _currentSelectedCharIndex);
        if (findIndex >= 0)
        {
            //swap
            charId[findIndex] = charId[index];
        }
        
        charId[index] = _currentSelectedCharIndex;

        info.deck1 = charId[0];
        info.deck2 = charId[1];
        info.deck3 = charId[2];
        info.deck4 = charId[3];
        info.deck5 = charId[4];
        
        LobbyUserDeckServerConnector.SetDeckData(info);
        LoadDecks();
    }

    public void LoadDecks(int index = -1)
    {
        _currentDeckIndex = index < 0 ? _currentDeckIndex : index;
        _deckLoader.LoadDecks(_currentDeckIndex);
        LobbyUserDeckServerConnector.SetCurrentDeckIndex(_currentDeckIndex);
    }
    
    public void ChangeCharacter(int index)
    {
        LobbyUserDeckServerConnector.GetDeckData(_currentDeckIndex, (info) => UpdateDeck(index, info));
    }
    
}
