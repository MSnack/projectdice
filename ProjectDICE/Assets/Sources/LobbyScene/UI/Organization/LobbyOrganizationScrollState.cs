﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LobbyOrganizationScrollState : MonoBehaviour
{
    public enum State
    {
        NON_SCROLL = 0,
        SCROLL = 1,
    }
    
    [SerializeField]
    private GridLayoutGroup _layoutGroup = null;

    [SerializeField]
    private LobbyOrganizationScrollSelectMgr _selectMgr = null;

    [SerializeField]
    private Transform _contentTransform = null;

    [SerializeField]
    private float _targetActiveScrollPositionY = 30;
    
    [SerializeField]
    private float _targetInactiveScrollPositionY = -100;

    [SerializeField]
    private UnityEvent _changeToScrollEvent = null;
    
    [SerializeField]
    private UnityEvent _changeToNonScrollEvent = null;

    [SerializeField, ReadOnly]
    private State _currState = State.NON_SCROLL;
    
    [Header("For Padding Animation")]
    [SerializeField]
    private float _totalTime = 0.5f;
    
    [SerializeField]
    private AnimationCurve _curve = AnimationCurve.Linear(0, 0, 1, 1);
    
    private Coroutine _scrollCoroutine = null;

    // Update is called once per frame
    void Update()
    {
        if (_currState == State.NON_SCROLL)
        {
            if(_contentTransform.localPosition.y >= _targetActiveScrollPositionY) ChangeState(State.SCROLL);
        }
        else if (_currState == State.SCROLL)
        {
            if (_contentTransform.localPosition.y < _targetInactiveScrollPositionY) ChangeState(State.NON_SCROLL);
        }
    }

    private void Reset()
    {
        _contentTransform = transform;
    }

    public void ChangeState(State state)
    {
        if (_currState == state) return;
        _currState = state;
        if(state == State.SCROLL) _changeToScrollEvent?.Invoke();
        else if(state == State.NON_SCROLL) _changeToNonScrollEvent?.Invoke();
        ChangePadding();
    }

    public void ResetScroll()
    {
        ChangeState(State.NON_SCROLL);
    }

    private void ChangePadding()
    {
        float targetPos = _currState == State.SCROLL ? 50 : 450;
        if(_scrollCoroutine != null) StopCoroutine(_scrollCoroutine);
        _scrollCoroutine = StartCoroutine(ChangePaddingAnimation(targetPos));
        
        if(_currState == State.NON_SCROLL) _selectMgr.ForceMoving(1);
    }

    private IEnumerator ChangePaddingAnimation(float targetSize)
    {
        float currPosition = _layoutGroup.padding.top;
        float elapsedTime = 0;

        for (; elapsedTime < _totalTime; elapsedTime += Time.deltaTime)
        {
            if(Input.GetMouseButtonDown(0)) yield break;
            float t = _curve.Evaluate(elapsedTime / _totalTime);
            float v = Mathf.Lerp(currPosition, targetSize, t);
            _layoutGroup.padding.top = (int) v;
            LayoutRebuilder.MarkLayoutForRebuild((RectTransform) _layoutGroup.transform);

            yield return null;
        }

        _layoutGroup.padding.top = (int) targetSize;
        LayoutRebuilder.MarkLayoutForRebuild((RectTransform) _layoutGroup.transform);
        _scrollCoroutine = null;
    }
}
