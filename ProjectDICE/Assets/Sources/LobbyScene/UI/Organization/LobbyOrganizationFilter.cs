﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyOrganizationFilter : MonoBehaviour
{

    [SerializeField]
    private LobbyCharListContent _content = null;

    [SerializeField]
    private Text[] _filterTexts = null;

    [SerializeField]
    private Color _activeColor;
    
    [SerializeField]
    private Color _inactiveColor;
    
    // Start is called before the first frame update
    void Start()
    {
        RefreshFilter();
    }

    public void RefreshFilter()
    {
        int index = _content.FilterIndex;
        for (int i = 0; i < _filterTexts.Length; ++i)
        {
            var filterText = _filterTexts[i];
            filterText.color = index == i - 1 ? _activeColor : _inactiveColor;
        }
    }

}
