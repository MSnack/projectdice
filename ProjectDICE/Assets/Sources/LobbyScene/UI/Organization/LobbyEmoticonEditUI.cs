﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyEmoticonEditUI : MonoBehaviour
{
    [SerializeField]
    private EmoticonSlotObject _emoticon = null;


    private bool _startOrganizationScrollState = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetData(int id)
    {
        _emoticon.InitData(id);
    }
}
