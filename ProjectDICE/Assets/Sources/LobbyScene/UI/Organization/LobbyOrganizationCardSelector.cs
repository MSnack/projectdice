﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

public class LobbyOrganizationCardSelector : EightReceiver
{
    private Camera _mainCamera;

    [SerializeField]
    private Animator _animator = null;

    private bool _isActive = false;

    [SerializeField]
    private UnityEvent _onSelected;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        _mainCamera = EightUtil.GetCore<EightCameraMgr>().EightCamera;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            _isActive = RectTransformUtility.RectangleContainsScreenPoint(
                GetComponent<RectTransform>(), Input.mousePosition, _mainCamera);
            _animator.SetBool("isActive", _isActive);
        }

        if (_isActive && Input.GetMouseButtonUp(0))
        {
            _onSelected.Invoke();
            _isActive = false;
            _animator.SetBool("isActive", _isActive);
        }
    }
}
