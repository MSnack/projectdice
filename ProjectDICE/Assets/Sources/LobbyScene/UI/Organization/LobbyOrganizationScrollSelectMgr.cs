﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LobbyOrganizationScrollSelectMgr : MonoBehaviour
{

    [SerializeField]
    private ScrollRect _scrollView = null;

    [SerializeField]
    private GridLayoutGroup _layoutGroup = null;

    [SerializeField]
    private VerticalLayoutGroup _verticalLayoutGroup = null;

    [SerializeField]
    private AnimationCurve _curve = AnimationCurve.Linear(0, 0, 1, 1);

    [SerializeField]
    private float _totalTime = 0.5f;

    private Coroutine _scrollCoroutine = null;

    [SerializeField]
    private float test = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        test = _scrollView.verticalNormalizedPosition;
    }

    public void SetSelector(LobbyOrganizationScrollSelector selector)
    {
        if (selector == null)
        {
            Debug.LogAssertion("selector is null. (LobbyOrganizationScrollSelectMgr)");
            return;
        }
        selector.InitSelector(this);
    }

    public void OnSelect(LobbyOrganizationScrollSelector selector)
    {
        if (enabled == false) return;
        float spacingY = _layoutGroup != null ? _layoutGroup.spacing.y : _verticalLayoutGroup.spacing; 
        float elementHeight = ((RectTransform) selector.transform).rect.height + spacingY;
        float elementPos = -selector.transform.localPosition.y - elementHeight;
        float targetPos = 1 - (elementPos / (((RectTransform)transform).rect.height - ((RectTransform)_scrollView.transform).rect.height));

        targetPos = Math.Max(Math.Min(targetPos, 1), 0);
        if(_scrollCoroutine != null) StopCoroutine(_scrollCoroutine);
        _scrollCoroutine = StartCoroutine(OnScrollAnimation(targetPos));

    }

    public void ForceMoving(float targetY)
    {
        if(_scrollCoroutine != null) StopCoroutine(_scrollCoroutine);
        _scrollCoroutine = StartCoroutine(OnScrollAnimation(targetY));
    }

    private IEnumerator OnScrollAnimation(float targetPosition)
    {
        float currPosition = _scrollView.verticalNormalizedPosition;
        float elapsedTime = 0;
        targetPosition = Math.Min(1, targetPosition);

        for (; elapsedTime < _totalTime; elapsedTime += Time.deltaTime)
        {
            if(Input.GetMouseButtonDown(0)) yield break;
            float t = _curve.Evaluate(elapsedTime / _totalTime);
            float v = Mathf.Lerp(currPosition, targetPosition, t);
            _scrollView.verticalNormalizedPosition = v;
            yield return null;
        }

        _scrollView.verticalNormalizedPosition = targetPosition;
        _scrollCoroutine = null;
    }
    
}
