﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyOrganizationEditUI : MonoBehaviour
{
    [SerializeField]
    private LobbyUserDeckMgr _deckMgr = null;

    [SerializeField]
    private CardSlotObject _card = null;

    [SerializeField]
    private Animator _organizationTopAnimator = null;

    private bool _startOrganizationScrollState = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetData(CharacterUnitData data)
    {
        _card.InitData(data);
        _deckMgr.CurrentSelectedCharIndex = data.DataID;
    }

    private void OnEnable()
    {
        _startOrganizationScrollState = _organizationTopAnimator.GetBool("isActive");

        if (_startOrganizationScrollState == false)
        {
            _organizationTopAnimator.enabled = true;
            _organizationTopAnimator.Rebind();
            _organizationTopAnimator.SetBool("isActive", true);
        }
    }

    private void OnDisable()
    {
        if (_startOrganizationScrollState == false)
        {
            _organizationTopAnimator.SetBool("isActive", false);
        }
    }
}
