﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class LobbyBattleHistoryUI : EightReceiver
{
    [SerializeField]
    private GameObject _content = null;

    [SerializeField]
    private GameObject _slotGameObject = null;

    [SerializeField]
    private UIAlphaController _alphaController = null;

    [SerializeField]
    private GameObject _emptyText = null;
    
    
    private readonly List<BattleHistorySlotObject> _slotObjects = new List<BattleHistorySlotObject>();
    
    private void Awake()
    {
        SetupComplete();
    }

    // Start is called before the first frame update
    void Start()
    {
        _emptyText.SetActive(true);
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        InitSlots();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        _alphaController.Alpha = 0;
    }

    private void InitSlots()
    {
        ReleaseAllSlot();
        var list = BattleHistoryUtil.GetBattleHistory();
        foreach (var localData in list)
        {
            AddSlot(localData);
        }
    }
    
    private void AddSlot(BattleHistoryLocalData info)
    {
        if (info == null) return;
        var obj = Instantiate(_slotGameObject, _content.transform);
        var historySlot = obj.GetComponent<BattleHistorySlotObject>();
        
        historySlot.InitData(info);
        obj.transform.SetAsFirstSibling();
        _slotObjects.Add(historySlot);
        _emptyText.SetActive(false);
    }
    
    private void ReleaseAllSlot()
    {
        foreach (var obj in _slotObjects)
        {
            Destroy(obj.gameObject);
        }
        
        _slotObjects.Clear();
        _emptyText.SetActive(true);
    }
    
}
