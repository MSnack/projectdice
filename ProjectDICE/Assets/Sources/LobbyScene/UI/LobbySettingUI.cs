﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbySettingUI : MonoBehaviour
{
    [SerializeField]
    private Slider _bgmSlider = null;

    [SerializeField]
    private Slider _effectSlider = null;

    [SerializeField]
    private Text _bgmText = null;

    [SerializeField]
    private Text _effectText = null;

    private GameSoundMgr _soundMgr = null;
    
    // Start is called before the first frame update
    void Start()
    {
        _soundMgr = EightUtil.GetCore<GameSoundMgr>();
        GetVolumes();
    }

    public void SetBGMVolume()
    {
        _soundMgr.BGMVolume = _bgmSlider.value;
        GetVolumes();
    }
    
    public void SetEffectVolume()
    {
        _soundMgr.EffectVolume = _effectSlider.value;
        GetVolumes();
    }

    public void GetVolumes()
    {
        float bgm = _soundMgr.BGMVolume;
        _bgmSlider.value = bgm;
        _bgmText.text = $"{(int)(bgm * 100f)}%";
        
        float effect = _soundMgr.EffectVolume;
        _effectSlider.value = effect;
        _effectText.text = $"{(int)(effect * 100f)}%";
    }

    public void SaveSettings()
    {
        _soundMgr.SaveVolumeSettings();
    }
}