﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum RefreshState
{
    None = 0,
    Goods,
    Cards,
    Organization,
    Boxes,
    Mails,
    QuestValue,
    Quest,
    Emoticon,
    DailyShop,
}

public class LobbyRefreshUtil : MonoBehaviour
{
    

    private static readonly Dictionary<RefreshState, UnityAction> _refreshEvents = new Dictionary<RefreshState, UnityAction>();

    public static void SetRefreshEvent(RefreshState state, UnityAction refreshEvent)
    {
        if (!_refreshEvents.ContainsKey(state))
            _refreshEvents.Add(state, null);
        
        _refreshEvents[state] += refreshEvent;
    }

    public static void ReleaseRefreshEvent(RefreshState state, UnityAction refreshEvent)
    {
        if (!_refreshEvents.ContainsKey(state))
            return;

        if (refreshEvent != null) _refreshEvents[state] -= refreshEvent;
    }

    public static void OnRefresh(RefreshState state)
    {
        if(_refreshEvents.ContainsKey(state))
            _refreshEvents[state]?.Invoke();
    }

    public static UnityAction OnRefreshEvent(RefreshState state)
    {
        if(_refreshEvents.ContainsKey(state))
            return () => _refreshEvents[state]?.Invoke();

        return null;
    }

    private void ReleaseAllActions()
    {
        _refreshEvents.Clear();
    }

    private void Awake()
    {
        // ReleaseAllActions();
    }

    private void OnDestroy()
    {
        ReleaseAllActions();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }
    
}
