﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyStateChanger : MonoBehaviour
{
    [Header("Lobby FSM State Change")]
    [SerializeField]
    private bool isLobbyFSMStateChanging = true;
    [SerializeField]
    private LobbyFSMSystem.LobbyFSMState _state = LobbyFSMSystem.LobbyFSMState.BATTLE;

    [Header("Lobby Popup FSM State Change")]
    [SerializeField]
    private bool isLobbyPopupFSMStateChanging = false;

    [SerializeField]
    private LobbyPopupFSMSystem.LobbyPopupFSMState _popupState = LobbyPopupFSMSystem.LobbyPopupFSMState.NONE;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ChangeState()
    {
        if (isLobbyFSMStateChanging) LobbySceneInterface.InvokeChangingState(_state);
        if (isLobbyPopupFSMStateChanging) LobbyPopupInterface.InvokeChangingState(_popupState);
    }
    
}
