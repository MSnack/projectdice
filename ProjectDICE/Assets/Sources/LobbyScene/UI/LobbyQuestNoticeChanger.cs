﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyQuestNoticeChanger : MonoBehaviour
{
    [SerializeField]
    private QuestCategory category;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ChangeNoticeState(bool isActive)
    {
        QuestUtil.OnNotice(category, isActive);
    }
}
