﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class ShopSlotSoldOutState : SlotStateBase<ShopItemSlotObject.State>
{
    [SerializeField]
    private Button _button = null;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = ShopItemSlotObject.State.SOLD_OUT;
    }

    public override void StartState()
    {
        var slotMgr = (ShopItemSlotObject) SlotStateMgr;

        if (!slotMgr.IsLimitSale)
        {
            ChangeState(ShopItemSlotObject.State.NONE);
            return;
        }

        if (_button != null)
        {
            _button.enabled = false;
        }
        base.StartState();
    }

    public override void EndState()
    {
        base.EndState();
    }

    public override void InvokeEvent()
    {
        
    }
}
