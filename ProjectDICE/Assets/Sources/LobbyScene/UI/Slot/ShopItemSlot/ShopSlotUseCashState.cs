﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Purchasing;

public class ShopSlotUseCashState : SlotStateBase<ShopItemSlotObject.State>
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = ShopItemSlotObject.State.USE_CASH;
    }

    public override void StartState()
    {
        base.StartState();
        // GamePopupMgr.OpenLoadingPopup();  //EightIAPButton 임시 팝업으로 로딩 팝업 뜨도록 설정
    }

    public override void EndState()
    {
        base.EndState();
        GamePopupMgr.CloseLoadingPopup();
    }

    public override void InvokeEvent()
    {
        
        // ShopSlotBuyItemState.InvokePopup((ShopItemSlotObject) SlotStateMgr);
    }

    public void OnPurchaseComplete(Product product)
    {
        //나중에 ios도 적용하도록 객체화 필요
        GamePopupMgr.OpenLoadingPopup();
        NormalPopup.Close();
        var slotMgr = (ShopItemSlotObject) SlotStateMgr;
        Debug.Log(product.receipt);
        var receiptJsonStr = JObject.Parse(product.receipt)["Payload"]?.ToString() ?? "";
        receiptJsonStr = JObject.Parse(receiptJsonStr).SelectToken("json")?.ToString() ?? "";
        var receiptJson = string.IsNullOrEmpty(receiptJsonStr) 
            ? new JObject() : JObject.Parse(receiptJsonStr);

        string purchaseToken = receiptJson.SelectToken("purchaseToken")?.ToString() ?? "";
        string packageName = receiptJson.SelectToken("packageName")?.ToString() ?? Application.identifier;

        PurchaseInfo purchaseInfo = new PurchaseInfo
        {
            PurchaseId = product.definition.id,
            PurchaseToken = purchaseToken,
            PackageName = packageName,
            Date = DateTime.Now,
        };

        BuyShopProcess(slotMgr.ShopId, purchaseInfo);
    }

    protected virtual void BuyShopProcess(int shopId, PurchaseInfo purchaseInfo)
    {
        LobbyShopServerConnector.BuyShopPackage(shopId, purchaseInfo, (mailId) =>
            {
                ChangeState(ShopItemSlotObject.State.SOLD_OUT);
                LobbyMailServerConnector.GetMails(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Mails));
                LobbyGoodsServerConnector.GetAllGoods(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Goods));
                GamePopupMgr.CloseLoadingPopup();
            },
            (error) =>
            {
                string errorStr = "";

                switch (error)
                {
                    case LobbyShopServerConnector.ErrorCode.NOT_ENOUGH_GOODS:
                        errorStr = "재화가 부족합니다.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.UNKNOWN_PACKAGE_ID:
                        errorStr = "앱 업데이트를 해야 정상적인 구매가 가능합니다.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.UNKNOWN_RECEIPT:
                        errorStr = "결제 진행에 문제가 생겼습니다.\n1:1 문의를 해주세요.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.VERIFY_RECEIPT_FAILED:
                        errorStr = "정상적인 결제방식이 아닙니다.\n1:1 문의를 해주세요.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.UNKOWN_PURCHASE_STATE:
                        errorStr = "정상적으로 결제되지 않았습니다.\n1:1 문의를 해주세요.";
                        break;
                }

                GamePopupMgr.OpenNormalPopup("구매 실패", errorStr);
                GamePopupMgr.CloseLoadingPopup();
                ChangeState(ShopItemSlotObject.State.NONE);
            });
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        switch (failureReason)
        {
            default:
                ChangeState(ShopItemSlotObject.State.NONE);
                break;
        }
    }
}
