﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemSlotObject : SlotStateMgr<ShopItemSlotObject.State>
{
    public enum State
    {
        NONE,
        BUY_ITEM,
        BUY_BOX,
        BUY_EMOJI,
        USE_CASH,
        DAILY_BUY,


        SOLD_OUT = 100,
    }

    [Header("Shop Item Info"), SerializeField]
    private int _shopId = SystemIndex.InvalidInt;
    public int ShopId => _shopId;

    [SerializeField]
    private ItemCategory _itemCategory;
    public ItemCategory ItemCategory => _itemCategory;

    [SerializeField]
    private int _itemId = SystemIndex.InvalidInt;
    public int ItemId => _itemId;

    [SerializeField]
    private int _itemCount = 1;

    [SerializeField]
    private GoodsID _goods = GoodsID.STAR;
    public GoodsID Goods => _goods;

    [SerializeField]
    private int _price = 1000;
    public int Price => _price;

    [SerializeField]
    private bool _isLimitSale = false;
    public bool IsLimitSale => _isLimitSale;

    private int _limitCount = SystemIndex.InvalidInt;
    public int LimitCount => _limitCount;

    [Header("Item Objects"), SerializeField]
    private ImageAutoScaler _itemImage = null;

    [SerializeField]
    private Text _itemCountText = null;

    [SerializeField]
    private Text _itemCategoryText = null;

    [SerializeField]
    private Image _cardFrameImage = null;

    [SerializeField]
    private Image _rewardCardImage = null;

    [SerializeField]
    private GameObject _cardMode = null;

    [SerializeField]
    private Image _rewardGoodsImage = null;

    [SerializeField]
    private bool _ignoreItemObjects = false;

    // Start is called before the first frame update
    void Start()
    {
        if (_ignoreItemObjects) return;
        StartCoroutine(InitItemInfo());
    }

    private IEnumerator InitItemInfo()
    {
        if (LobbyGoodsUtil.IsLoaded == false)
            yield return new WaitUntil(() => LobbyGoodsUtil.IsLoaded);
        if(LobbyBoxUtil.IsLoaded == false)
            yield return new WaitUntil(() => LobbyBoxUtil.IsLoaded);

        var itemInfo = ItemCategoryUtil.GetItemInfo(_itemCategory, _itemId);

        if(_itemImage != null) _itemImage.sprite = itemInfo.SpriteImage;
        _itemCategoryText.text = itemInfo.Name;
        if (itemInfo.FrameImage != null)
        {
            _cardFrameImage.sprite = itemInfo.FrameImage;
            _cardFrameImage.gameObject.SetActive(true);
            if(_cardMode != null) _cardMode.gameObject.SetActive(true);
            if (_rewardCardImage != null)
            {
                _rewardCardImage.sprite = itemInfo.SpriteImage;
                _itemImage.gameObject.SetActive(false);
            }
            else
            {
                _itemImage.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            }
        }
        if(_itemImage != null) _itemImage.SetNativeRatioSize();
        _itemCountText.text = (_itemCount <= 1 ? "" : $"x{_itemCount:#,###}");
        var goodsSprite = LobbyGoodsUtil.GetGoodsSprite(_goods);
        if (_rewardGoodsImage != null)
        {
            _rewardGoodsImage.enabled = goodsSprite != null;
            if (goodsSprite != null)
                _rewardGoodsImage.sprite = goodsSprite;
        }
    }

    private void OnDestroy()
    {
        
    }

    public void InitSlot(int shopId, ItemCategory itemCategory, int itemId, int itemCount, GoodsID goods, int price,
        bool isLimitSale = false)
    {
        _shopId = shopId;
        _itemCategory = itemCategory;
        _itemId = itemId;
        _itemCount = itemCount;
        _goods = goods;
        _price = price;
        _isLimitSale = isLimitSale;
        CurrentStateObject.StartState();
        
        Start();
    }

    public void InitSlot(int shopId)
    {
        _shopId = shopId;
        Start();
    }

    public void ClickSlot()
    {
        CurrentStateObject.InvokeEvent();
    }
}
