﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopSlotDailyNoneState : SlotStateBase<ShopItemSlotObject.State>
{
    
    
    [SerializeField]
    private Text _priceText = null;

    [SerializeField]
    private EightIAPButton _iapButton = null;

    private void Start()
    {
        
    }

    public override void InvokeEvent()
    {
        var slotMgr = (ShopItemSlotObject) SlotStateMgr;
        ShopItemSlotObject.State state;

        if (slotMgr.Goods == GoodsID.CASH)
            state = ShopItemSlotObject.State.USE_CASH;
        else
            state = ShopItemSlotObject.State.DAILY_BUY;
        
        ChangeState(state);
    }
    
    public override void StartState()
    {
        base.StartState();
        var slotMgr = (ShopItemSlotObject) SlotStateMgr;
        if (slotMgr.Goods == GoodsID.CASH)
        {
            _iapButton.enabled = true;
        }
        if (slotMgr.Price <= 0)
        {
            _priceText.text = "무료";
        }
        else
        {
            _priceText.text = LobbyGoodsUtil.GetGoodsName(slotMgr.Goods) + " " + $"{slotMgr.Price:#,###}";
        }
    }

    protected override void Initialize()
    {
        State = ShopItemSlotObject.State.NONE;
    }
}
