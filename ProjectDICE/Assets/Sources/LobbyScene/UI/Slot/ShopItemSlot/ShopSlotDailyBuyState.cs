﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopSlotDailyBuyState : SlotStateBase<ShopItemSlotObject.State>
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = ShopItemSlotObject.State.DAILY_BUY;
    }
    
    public override void StartState()
    {
        base.StartState();
        var slotMgr = (ShopItemSlotObject) SlotStateMgr;

        if (slotMgr.Price <= 0)
        {
            InvokeEvent();
            return;
        }

        GamePopupMgr.OpenNormalPopup("일일 한정 구매",
            LobbyGoodsUtil.GetGoodsName(slotMgr.Goods) + " " + slotMgr.Price + " 소모합니다.",
            new []
            {
                new NormalPopup.ButtonConfig("취소", () =>
                {
                    NormalPopup.Close();
                    ChangeState(ShopItemSlotObject.State.NONE);
                }, new Color(0.32f, 0.35f, 0.4f)), 
                new NormalPopup.ButtonConfig("구매", InvokeEvent)
            });
    }

    public override void EndState()
    {
        base.EndState();
    }

    public override void InvokeEvent()
    {
        GamePopupMgr.OpenLoadingPopup();
        NormalPopup.Close();
        var slotMgr = (ShopItemSlotObject)SlotStateMgr;

        LobbyShopServerConnector.BuyDailyShopPackage(slotMgr.ShopId, () =>
            {
                ChangeState(ShopItemSlotObject.State.SOLD_OUT);
                LobbyMailServerConnector.GetMails(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Mails));
                LobbyGoodsServerConnector.GetAllGoods(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Goods));
                QuestUtil.UpdateQuestEvent(QuestCondition.BuyDailyShop, 1);
                GamePopupMgr.CloseLoadingPopup();
                GamePopupMgr.OpenSimplePopup("상점에서 우편이 도착했습니다!");
            }, 
            (error) =>
            {
                string errorStr = "";

                switch (error)
                {
                    case LobbyShopServerConnector.ErrorCode.NOT_ENOUGH_GOODS:
                        errorStr = "재화가 부족합니다.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.UNKNOWN_PACKAGE_ID:
                        errorStr = "앱 업데이트를 해야 정상적인 구매가 가능합니다.";
                        break;
                }
            
                GamePopupMgr.OpenNormalPopup("구매 실패", errorStr);
                GamePopupMgr.CloseLoadingPopup();
                ChangeState(ShopItemSlotObject.State.NONE);
            });
    }
}
