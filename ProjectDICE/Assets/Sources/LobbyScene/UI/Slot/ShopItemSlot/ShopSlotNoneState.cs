﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class ShopSlotNoneState : SlotStateBase<ShopItemSlotObject.State>
{

    [SerializeField]
    private Text _priceText = null;

    [SerializeField]
    private EightIAPButton _iapButton = null;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = ShopItemSlotObject.State.NONE;
    }

    public override void StartState()
    {
        base.StartState();
        var slotMgr = (ShopItemSlotObject) SlotStateMgr;
        if (slotMgr.Goods == GoodsID.CASH)
        {
            _iapButton.enabled = true;
        }
        if (slotMgr.Price <= 0)
        {
            _priceText.text = "무료";
        }
        else
        {
            _priceText.text = LobbyGoodsUtil.GetGoodsName(slotMgr.Goods) + " " + $"{slotMgr.Price:#,###}";
        }

        if (slotMgr.IsLimitSale)
        {
            LobbyShopServerConnector.CheckShopLimitState(slotMgr.ShopId, (isPassed) =>
            {
                if(isPassed == false) ChangeState(ShopItemSlotObject.State.SOLD_OUT);
            });
        }
    }

    public override void InvokeEvent()
    {
        var slotMgr = (ShopItemSlotObject) SlotStateMgr;
        ShopItemSlotObject.State state;

        if (slotMgr.Goods == GoodsID.CASH)
            state = ShopItemSlotObject.State.USE_CASH;
        
        else if (slotMgr.ItemCategory == ItemCategory.BOX)
            state = ShopItemSlotObject.State.BUY_BOX;
        
        else
            state = ShopItemSlotObject.State.BUY_ITEM;
        
        ChangeState(state);

    }
}
