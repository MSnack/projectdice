﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopSlotDailyUseCashState : ShopSlotUseCashState
{
    protected override void BuyShopProcess(int shopId, PurchaseInfo purchaseInfo)
    {
        LobbyShopServerConnector.BuyDailyShopPackage(shopId, purchaseInfo, () =>
            {
                ChangeState(ShopItemSlotObject.State.SOLD_OUT);
                LobbyMailServerConnector.GetMails(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Mails));
                LobbyGoodsServerConnector.GetAllGoods(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Goods));
                GamePopupMgr.CloseLoadingPopup();
            },
            (error) =>
            {
                string errorStr = "";

                switch (error)
                {
                    case LobbyShopServerConnector.ErrorCode.NOT_ENOUGH_GOODS:
                        errorStr = "재화가 부족합니다.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.UNKNOWN_PACKAGE_ID:
                        errorStr = "앱 업데이트를 해야 정상적인 구매가 가능합니다.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.UNKNOWN_RECEIPT:
                        errorStr = "결제 진행에 문제가 생겼습니다.\n1:1 문의를 해주세요.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.VERIFY_RECEIPT_FAILED:
                        errorStr = "정상적인 결제방식이 아닙니다.\n1:1 문의를 해주세요.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.UNKOWN_PURCHASE_STATE:
                        errorStr = "정상적으로 결제되지 않았습니다.\n1:1 문의를 해주세요.";
                        break;
                }

                GamePopupMgr.OpenNormalPopup("구매 실패", errorStr);
                GamePopupMgr.CloseLoadingPopup();
                ChangeState(ShopItemSlotObject.State.NONE);
            });
    }
}
