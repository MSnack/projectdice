﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopSlotBuyBoxState : SlotStateBase<ShopItemSlotObject.State>
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = ShopItemSlotObject.State.BUY_BOX;
    }

    public override void StartState()
    {
        base.StartState();
        var slotMgr = (ShopItemSlotObject) SlotStateMgr;
        LobbyBoxDetailPopupUI.Open(slotMgr.ItemId, slotMgr.Price, InvokeEvent,
            () =>
            {
                ChangeState(ShopItemSlotObject.State.NONE);
            });
        // GamePopupMgr.OpenNormalPopup("상자 구매 화면", "상자 상세 정보 알려주는 임시 팝업",
        //     new []
        //     {
        //         new NormalPopup.ButtonConfig("취소", () =>
        //         {
        //             NormalPopup.Close();
        //             ChangeState(ShopItemSlotObject.State.NONE);
        //         }), 
        //         new NormalPopup.ButtonConfig("구매", InvokeEvent)
        //     });
    }

    public override void EndState()
    {
        base.EndState();
    }

    public override void InvokeEvent()
    {
        ShopSlotBuyItemState.InvokePopup((ShopItemSlotObject) SlotStateMgr);
    }
}
