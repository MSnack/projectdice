﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShopSlotBuyItemState : SlotStateBase<ShopItemSlotObject.State>
{
    [SerializeField]
    private bool _getItemImmediately = false;

    [SerializeField]
    private string _immediatelyEventText = "";

    private static UnityAction<int[], UnityAction> _immediatelyMailOpenEvent = null;
    
    // Start is called before the first frame update
    void Start()
    {
        if (_immediatelyMailOpenEvent != null)
        {
            return;
        }

        _immediatelyMailOpenEvent += (mailId, immediatelyEvent) =>
        {
            StartCoroutine(OpenMailsCoroutine(mailId, immediatelyEvent));
        };
    }

    protected override void Initialize()
    {
        State = ShopItemSlotObject.State.BUY_ITEM;
    }

    public override void StartState()
    {
        base.StartState();
        var slotMgr = (ShopItemSlotObject) SlotStateMgr;

        GamePopupMgr.OpenNormalPopup("아이템 구매",
            LobbyGoodsUtil.GetGoodsName(slotMgr.Goods) + " " + slotMgr.Price + " 소모합니다.",
            new []
            {
                new NormalPopup.ButtonConfig("취소", () =>
                {
                    NormalPopup.Close();
                    ChangeState(ShopItemSlotObject.State.NONE);
                }, new Color(0.32f, 0.35f, 0.4f)), 
                new NormalPopup.ButtonConfig("구매", InvokeEvent)
            });
    }

    public override void EndState()
    {
        base.EndState();
    }

    private void OnDestroy()
    {
        _immediatelyMailOpenEvent = null;
    }

    public override void InvokeEvent()
    {
        InvokePopup((ShopItemSlotObject) SlotStateMgr, _getItemImmediately,
            () => GamePopupMgr.OpenNormalPopup("획득 완료", _immediatelyEventText));
    }

    public static void InvokePopup(ShopItemSlotObject mgr, bool getItemImmediately = false, UnityAction immediatelyEvent = null)
    {
        GamePopupMgr.OpenLoadingPopup();
        NormalPopup.Close();
        var slotMgr = mgr;

        LobbyShopServerConnector.BuyShopPackage(slotMgr.ShopId, null, (mailId) =>
            {
                if (getItemImmediately)
                {
                    _immediatelyMailOpenEvent?.Invoke(mailId, immediatelyEvent);
                    mgr.ChangeState(ShopItemSlotObject.State.SOLD_OUT);
                }
                else
                {
                    mgr.ChangeState(ShopItemSlotObject.State.SOLD_OUT);
                    LobbyMailServerConnector.GetMails(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Mails));
                    LobbyGoodsServerConnector.GetAllGoods(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Goods));
                    GamePopupMgr.CloseLoadingPopup();
                    GamePopupMgr.OpenSimplePopup("상점에서 우편이 도착했습니다!");
                }
            }, 
            (error) =>
            {
                string errorStr = "";

                switch (error)
                {
                    case LobbyShopServerConnector.ErrorCode.NOT_ENOUGH_GOODS:
                        errorStr = "재화가 부족합니다.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.UNKNOWN_PACKAGE_ID:
                        errorStr = "앱 업데이트를 해야 정상적인 구매가 가능합니다.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.HAS_ALREADY_BEEN_LIMITED_TO_PURCHASE:
                        errorStr = "이미 구매한 상품입니다. 인앱 결제 시 환불을 진행해주세요.";
                        break;
                }
            
                GamePopupMgr.OpenNormalPopup("구매 실패", errorStr);
                GamePopupMgr.CloseLoadingPopup();
                mgr.ChangeState(ShopItemSlotObject.State.NONE);
            });
    }

    private IEnumerator OpenMailsCoroutine(int[] mailId, UnityAction immediatelyEvent)
    {
        for (int i = 0; i < mailId.Length; ++i)
        {
            bool isReceived = false;
            LobbyMailServerConnector.ReceiveMailItem(mailId[i],
                (trans) =>
                {
                    isReceived = true;
                });
            yield return new WaitUntil(() => isReceived);
        }
        
        LobbyMailServerConnector.GetMails(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Mails));
        LobbyGoodsServerConnector.GetAllGoods(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Goods));
        EmoticonServerConnector.GetEmoticonData(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Emoticon));
        immediatelyEvent?.Invoke();
        GamePopupMgr.CloseLoadingPopup();
    }
}
