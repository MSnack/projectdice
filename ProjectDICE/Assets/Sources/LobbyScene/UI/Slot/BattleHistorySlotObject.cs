﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class BattleHistorySlotObject : EightReceiver
{

    [SerializeField]
    private Text _titleText = null;

    [SerializeField]
    private GameObject[] _winStateActiveObjects;

    [SerializeField]
    private GameObject[] _loseStateActiveObjects;
    
    [SerializeField]
    private Text _getTrophyText = null;

    [Header("Opponent User Info")]
    [SerializeField]
    private Text _opponentUserNameText = null;
    
    [SerializeField]
    private Text _opponentGuildText = null;

    [SerializeField]
    private Text _opponentTrophyText = null;

    [SerializeField]
    private LobbyTierObject _opponentTierObject = null;

    [Header("Opponent User Deck Slots")]
    [SerializeField]
    private LobbyImageChanger[] _opponentSlots = null;
    
    [Header("My User Info")]
    [SerializeField]
    private Text _myUserNameText = null;
    
    [SerializeField]
    private Text _myGuildText = null;

    [SerializeField]
    private Text _myTrophyText = null;

    [SerializeField]
    private LobbyTierObject _myTierObject = null;

    [Header("My User Deck Slots")]
    [SerializeField]
    private LobbyImageChanger[] _mySlots = null;

    private GameUnitDataMgr _unitDataMgr = null;
    private int _matchId = SystemIndex.InvalidInt;
    private BattleHistoryLocalData _localData = null;

    private void Awake()
    {
        SetupComplete();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        if(_unitDataMgr == null) _unitDataMgr = EightUtil.GetCore<GameUnitDataMgr>();
    }

    public void InitData(BattleHistoryLocalData data)
    {
        if(_unitDataMgr == null) _unitDataMgr = EightUtil.GetCore<GameUnitDataMgr>();

        _localData = data;
        _matchId = data.MatchId;
        foreach (var activeObject in _winStateActiveObjects)
        {
            activeObject.SetActive(data.IsWin);
        }
        foreach (var activeObject in _loseStateActiveObjects)
        {
            activeObject.SetActive(!data.IsWin);
        }
        _getTrophyText.text = (data.TrophyIncrementValue > 0)
            ? "+" + data.TrophyIncrementValue
            : data.TrophyIncrementValue.ToString();
        
        _opponentUserNameText.text = data.OpponentName;
        _opponentGuildText.text = (string.IsNullOrEmpty(data.OpponentGuild)) ? "길드 없음" : data.OpponentGuild;
        _opponentTrophyText.text = data.OpponentTier.ToString();
        TierState opponentTier = TierUtil.GetTierState(data.OpponentTier);
        _opponentTierObject.InitTier(opponentTier);

        if (!string.IsNullOrEmpty(data.MyName))
        {
            _myUserNameText.text = data.MyName;
            _myGuildText.text = (string.IsNullOrEmpty(data.MyGuild)) ? "길드 없음" : data.MyGuild;
            _myTrophyText.text = data.MyTier.ToString();
            TierState myTier = TierUtil.GetTierState(data.MyTier);
            _myTierObject.InitTier(myTier);
        }
        
        for (int i = 0; i < _opponentSlots.Length; ++i)
        {
            var opponentSlot = _opponentSlots[i];
            var opponentCharId = data.OpponentDeck[i];

            opponentSlot.ChangeSprite(_unitDataMgr.GetCharacaterData(opponentCharId).UnitPreviewSprite);
            if (string.IsNullOrEmpty(data.MyName)) continue;
            
            var mySlot = _mySlots[i];
            var myCharId = data.MyDeck[i];
                
            mySlot.ChangeSprite(_unitDataMgr.GetCharacaterData(myCharId).UnitPreviewSprite);
        }
    }

}
