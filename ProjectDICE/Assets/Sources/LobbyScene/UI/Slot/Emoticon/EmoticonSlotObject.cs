﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class EmoticonSlotObject : SlotStateMgr<EmoticonSlotObject.State>
{

    private int _id = SystemIndex.InvalidInt;
    public int ID => _id;

    private EmoticonManager _emoticonMgr = null;

    [SerializeField]
    private Image _enableImage = null;

    public enum State
    {
        None,
        Focus,
        Locked,
        Unknown,
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void InitData(int id)
    {
        _id = id;
        if (_emoticonMgr == null) _emoticonMgr = EightUtil.GetCore<EmoticonManager>();

        if (id <= 0)
        {
            ChangeState(State.Unknown);
            return;
        }
        ChangeState(State.None);

        _enableImage.sprite = GetPreviewSprite();
        
    }

    private Sprite GetPreviewSprite()
    {
        if (_emoticonMgr == null) _emoticonMgr = EightUtil.GetCore<EmoticonManager>();
        return _emoticonMgr?.GetEmoticonIcon(_id);
    }


    public virtual void OnClick()
    {
        CurrentStateObject.InvokeEvent();
    }
}
