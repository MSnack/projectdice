﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmoticonSlotUnkownState : SlotStateBase<EmoticonSlotObject.State>
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = EmoticonSlotObject.State.Unknown;
    }

    public override void InvokeEvent()
    {
        
    }
}
