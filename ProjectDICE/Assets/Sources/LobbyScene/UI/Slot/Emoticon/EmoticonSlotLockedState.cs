﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmoticonSlotLockedState : SlotStateBase<EmoticonSlotObject.State>
{

    [SerializeField]
    private GameObject _grayScaleObject = null;

    [SerializeField]
    private Material _grayMaterial = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = EmoticonSlotObject.State.Locked;
    }

    public override void InvokeEvent()
    {
        
    }

    public override void StartState()
    {
        base.StartState();
        var images = _grayScaleObject.GetComponentsInChildren<Image>(true);
        foreach (var image in images)
        {
            image.material = _grayMaterial;
        }
    }

    public override void EndState()
    {
        base.EndState();
        var images = _grayScaleObject.GetComponentsInChildren<Image>(true);
        foreach (var image in images)
        {
            image.material = null;
        }
    }
}
