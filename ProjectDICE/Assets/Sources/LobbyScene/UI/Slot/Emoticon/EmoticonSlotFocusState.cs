﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class EmoticonSlotFocusState : SlotStateBase<EmoticonSlotObject.State>
{

    private Camera _mainCamera;
    
    private EmoticonManager _emoticonMgr = null;

    [SerializeField]
    private RectTransform _rectTransform;
    
    [SerializeField]
    private Transform _previewTransform = null;

    [SerializeField]
    private int _orderLayer = 0;
    private EmoticonObject _previewEmoticonObject = null;

    private int _prevEmoticonOrderLayer = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        _mainCamera = EightUtil.GetCore<EightCameraMgr>().EightCamera;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && 
            !RectTransformUtility.RectangleContainsScreenPoint(
                _rectTransform, 
                Input.mousePosition, _mainCamera)) {
            ChangeState(EmoticonSlotObject.State.None);
        }
    }

    protected override void Initialize()
    {
        State = EmoticonSlotObject.State.Focus;
    }

    public override void InvokeEvent()
    {
        
    }

    public void OnSettingDeck()
    {
        LobbyEmoticonEditState.SetEmoticonData(((EmoticonSlotObject)SlotStateMgr).ID);
        LobbySceneInterface.InvokeChangingState(LobbyFSMSystem.LobbyFSMState.EMOTICON_EDIT);
    }

    public override void StartState()
    {
        base.StartState();
        if (_emoticonMgr == null) _emoticonMgr = EightUtil.GetCore<EmoticonManager>();
        if (_previewEmoticonObject != null)
        {
            ReleaseEmoticonObject(_previewEmoticonObject);
            _previewEmoticonObject = null;
        }
        _previewEmoticonObject = GetEmoticonObject();
        Vector3 pos = _previewEmoticonObject.transform.localPosition;
        Vector3 scale = _previewEmoticonObject.transform.localScale;
        Quaternion rot = _previewEmoticonObject.transform.rotation;
        _prevEmoticonOrderLayer = _previewEmoticonObject.SortingOrder;
        
        _previewEmoticonObject.transform.SetParent(_previewTransform);
        _previewEmoticonObject.transform.localPosition = pos;
        _previewEmoticonObject.transform.localScale = scale;
        _previewEmoticonObject.transform.rotation = rot;
        _previewEmoticonObject.SortingOrder = _orderLayer;
    }

    public override void EndState()
    {
        base.EndState();
        ReleaseEmoticonObject(_previewEmoticonObject);
    }
    
    private void ReleaseEmoticonObject(EmoticonObject emoticonObject)
    {
        if (_previewEmoticonObject == null) return;
        if (_emoticonMgr == null) _emoticonMgr = EightUtil.GetCore<EmoticonManager>();

        Vector3 pos = _previewEmoticonObject.transform.localPosition;
        Vector3 scale = _previewEmoticonObject.transform.localScale;
        Quaternion rot = _previewEmoticonObject.transform.rotation;
        
        _emoticonMgr?.ReturnEmoticon(emoticonObject);

        _previewEmoticonObject.SortingOrder = _prevEmoticonOrderLayer;
        _previewEmoticonObject.transform.localPosition = pos;
        _previewEmoticonObject.transform.localScale = scale;
        _previewEmoticonObject.transform.rotation = rot;
    }
    
    private EmoticonObject GetEmoticonObject()
    {
        if (_emoticonMgr == null) _emoticonMgr = EightUtil.GetCore<EmoticonManager>();
        return _emoticonMgr?.GetEmoticon(((EmoticonSlotObject)SlotStateMgr).ID);
    }
}
