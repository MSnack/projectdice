﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmoticonSlotNoneState : SlotStateBase<EmoticonSlotObject.State>
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = EmoticonSlotObject.State.None;
    }

    public override void InvokeEvent()
    {
        ChangeState(EmoticonSlotObject.State.Focus);
    }
}
