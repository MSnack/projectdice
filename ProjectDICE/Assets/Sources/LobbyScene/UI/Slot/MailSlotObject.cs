﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MailSlotObject : MonoBehaviour
{

    [Header("Item"), SerializeField]
    private ImageAutoScaler _itemImage;

    [SerializeField]
    private Text _itemCount;

    [Header("Card Item"), SerializeField]
    private GameObject _cardPreviewObject = null;

    [SerializeField]
    private Image _cardGradeImage = null;

    [SerializeField]
    private Image _cardCharacterImage = null;

    [Header("Inner Base"), SerializeField]
    private Text _titleText;

    [SerializeField]
    private Text _detailText;

    [SerializeField]
    private Text _remainTimeText;

    [SerializeField]
    private ContentSizeFitter _contentSizeFitter;


    private int _mailId = SystemIndex.InvalidInt;
    private ItemCategory _itemCategory = ItemCategory.NONE;
    public ItemCategory ItemCategory => _itemCategory;

    private DateTime _endTime;

    private GameUnitDataMgr _unitDataMgr = null;

    private MailInfo _info;
    public MailInfo Info => _info;
    
    // Start is called before the first frame update
    void Start()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform) _contentSizeFitter.transform);
    }

    private void Update()
    {
        TimeSpan t = _endTime - DateTime.Now;
        _remainTimeText.text = (t.Days > 0 ? $"{t.Days}일 " : "")
                                + (t.Hours > 0 ? $"{t.Hours}시간 " : "")
                                + (t.Days <= 0 ? $"{t.Minutes}분 " : "")
                                + "남음";
        
        if (t.TotalSeconds <= 0)
        {
            gameObject.SetActive(false);
        }
    }

    public void InitData(MailInfo info)
    {
        if (info.IsReceived) return;
        _info = info;
        if (_unitDataMgr == null) _unitDataMgr = EightUtil.GetCore<GameUnitDataMgr>();
        _mailId = info.Id;
        _itemCategory = (ItemCategory) info.ItemCategory;

        string itemName = "UNKNOWN";

        if (_itemCategory == ItemCategory.BOX)
        {
            _itemImage.sprite = LobbyBoxUtil.GetBoxSprite(info.ItemId, true);
            itemName = LobbyBoxUtil.GetBoxName(info.ItemId);
        }

        if (_itemCategory == ItemCategory.GOODS)
        {
            _itemImage.sprite = LobbyGoodsUtil.GetGoodsSprite((GoodsID) info.ItemId);
            itemName = LobbyGoodsUtil.GetGoodsName((GoodsID) info.ItemId);
        }

        if (_itemCategory == ItemCategory.CHARACTER)
        {
            var charData = _unitDataMgr.GetCharacaterData(info.ItemId);
            _cardPreviewObject.SetActive(true);
            _cardGradeImage.sprite = LobbyCardUtil.GetFrameSprite(charData.Grade - 1);
            _cardCharacterImage.sprite = charData?.UnitPreviewSprite;
            itemName = charData?.UnitName;
        }

        _itemImage.SetNativeRatioSize();
        _itemCount.text = "x" + info.ItemCount;

        _endTime = info.GetStartTime;
        _endTime = _endTime.AddDays(7);

        if (info.Title.Equals("Purchase"))
        {
            if (info.ItemCategory == (int) ItemCategory.QUEST_CONDITION &&
                info.ItemId == (int) QuestCondition.BuyMembership)
            {
                _titleText.text = "멤버십 적용 상품";
                _detailText.text = "이번달 멤버십 해택 적용";
                _itemCount.text = "";
            }
            else
            {
                _titleText.text = "구매 상품";
                _detailText.text = itemName + " " + info.ItemCount + "개";
            }
        }
        else if (info.Title.Equals("Quest"))
        {
            _titleText.text = "퀘스트 완료 보상";
            _detailText.text = itemName + " " + info.ItemCount + "개";
        }
        else
        {
            _titleText.text = info.Title;
            _detailText.text = info.Description; 
        }
        
    }

    public void OnReceiveMail()
    {
        GamePopupMgr.OpenLoadingPopup();
        LobbyMailServerConnector.ReceiveMailItem(_mailId, (trans) =>
        {
            bool isCharacterExist = false;
            ReceivedMailEvent(trans, _itemCategory);
            
            if (ItemCategory == ItemCategory.BOX) return;
            var info = new RewardItemInfo
                {ItemCategory = (int) ItemCategory, Id = Info.ItemId, Count = Info.ItemCount};
            
            GamePopupMgr.CloseLoadingPopup();
            GamePopupMgr.OpenRewardPopup(new List<RewardItemInfo> {info});
        });
    }

    public void OnReceiveMail(UnityAction<GameWebRequestTranslator> receivedEvent)
    {
        GamePopupMgr.OpenLoadingPopup();
        LobbyMailServerConnector.ReceiveMailItem(_mailId, (trans) =>
        {
            if (receivedEvent != null) receivedEvent.Invoke(trans);
            else ReceivedMailEvent(trans, _itemCategory);
        });
    }

    public static void ReceivedMailEvent(GameWebRequestTranslator translator, ItemCategory itemCategory)
    {
        switch (itemCategory)
        {
            case ItemCategory.BOX:
            {
                var result = translator.GetRequestData<BoxResultInfo>();
                LobbyOpenBoxUI.OpenBoxUI(result);
                break;
            }
            case ItemCategory.GOODS:
                LobbyGoodsServerConnector.GetAllGoods();
                break;
            case ItemCategory.CHARACTER:
                LobbyCharacterCardServerConnector.GetCurrentCharList(() =>
                {
                    LobbyRefreshUtil.OnRefresh(RefreshState.Organization);
                    LobbyRefreshUtil.OnRefresh(RefreshState.Cards);
                });
                LobbyNoticeUtil.OnNotice(LobbyNoticeUtil.NoticeState.Cards);
                break;
            
            case ItemCategory.QUEST_CONDITION:
                QuestServerConnector.RefreshUserQuestInfos(() => LobbyRefreshUtil.OnRefresh(RefreshState.Quest));
                break;
        }

        LobbyMailServerConnector.GetMails(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Mails));
        GamePopupMgr.CloseLoadingPopup();
    }
    
}
