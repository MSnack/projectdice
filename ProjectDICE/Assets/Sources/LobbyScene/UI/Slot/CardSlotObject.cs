﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CardSlotObject : EightReceiver
{
    [SerializeField, ReadOnly()]
    private short _rate = 1;

    [SerializeField, ReadOnly()]
    private int _id = SystemIndex.InvalidInt;
    public int Id => _id;

    private string _cardName = "";
    private string _cardDescription = "";

    [SerializeField]
    private Image _cardImage = null;
    
    [SerializeField]
    private Image _gradeFrame = null;

    [SerializeField]
    private Text _cardLevel = null;

    [SerializeField]
    private Image _expBarImage = null;
    
    [SerializeField]
    private Text _expText = null;

    [SerializeField]
    private GameObject _expFullGaugeObject = null;

    [SerializeField]
    private Text _cardNameText = null;

    [SerializeField, ReadOnly()]
    private CharacterUnitData _unitData = null;
    public CharacterUnitData UnitData => _unitData;
    
    [SerializeField]
    private Animator _animator = null;

    [SerializeField]
    private CardSlotObjectEffect _effect = null;

    [SerializeField]
    private bool _isAutoRefreshActived = true;

    private bool _isActive = false;

    protected Camera _mainCamera = null;
    protected GameDataMgr _gameDataMgr = null;

    private MaskableGraphic[] _maskableGraphics = null;
    private bool _isUnlock = false;

    private bool _isEnableRender = true;
    public bool IsEnableRender => _isEnableRender;

    [SerializeField]
    private Button _SetPartyButton = null;

    // Start is called before the first frame update
    void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        _mainCamera = EightUtil.GetCore<EightCameraMgr>().EightCamera;
        _gameDataMgr = EightUtil.GetCore<GameDataMgr>();

        if (_isAutoRefreshActived)
            LobbyRefreshUtil.SetRefreshEvent(RefreshState.Cards, RefreshData);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (_isActive && Input.GetMouseButton(0) && 
            !RectTransformUtility.RectangleContainsScreenPoint(
                _gradeFrame.GetComponent<RectTransform>(), 
                Input.mousePosition, _mainCamera)) {
            SetCardActive(false);
        }
    }
    
    protected override void OnDisable()
    {
        base.OnDisable();
        if (_animator != null)
        {
            _animator.Rebind();
            _animator.enabled = false;
        }
        _isActive = false;
    }

    private void OnDestroy()
    {
        if (_isAutoRefreshActived)
            LobbyRefreshUtil.ReleaseRefreshEvent(RefreshState.Cards, RefreshData);
    }

    public void InitData(CharacterUnitData data)
    {
        _cardImage.enabled = false;

        if (data == null)
        {
            
            return;
        }
        
        _unitData = data;
        _id = data.DataID;
        if (data.DataID > SystemIndex.InvalidInt)
        {
            _cardImage.enabled = true;
        }
        _rate = (short) data.Grade;
        _gradeFrame.sprite = LobbyCardUtil.GetFrameSprite(data.Grade - 1);
        if (data.UnitPreviewSprite != null)
        {
            _cardImage.enabled = true;
            _cardImage.sprite = data.UnitPreviewSprite;
        }

        _cardName = data.UnitName;
        if (_cardNameText != null) _cardNameText.text = _cardName;
        _cardDescription = data.Description;

        _effect.SetUnitEffect(data);
        
        //유저 전용 캐릭터 값 불러오는 구간
        InitUserCharacterDetail(CharacterCardUtil.GetCardDetail(_id));

    }

    public void RefreshData()
    {
        InitData(_unitData);
    }

    public void SetUnlock(Material grayScale)
    {
        _isUnlock = true;
        if (_maskableGraphics == null)
        {
            _maskableGraphics = GetComponentsInChildren<MaskableGraphic>();
        }

        int size = _maskableGraphics.Length;
        for (int i = 0; i < size; ++i)
        {
            var graphic = _maskableGraphics[i];
            if (graphic.GetComponent<Mask>() != null) continue;
            graphic.material = grayScale;
        }
        
        if(_SetPartyButton != null) _SetPartyButton.gameObject.SetActive(false);
    }

    private IEnumerator WaitDetailData()
    {
        yield return new WaitUntil(() => LobbyCharacterCardServerConnector.IsLoaded);
        yield return new WaitUntil(() => EightUtil.GetCore<GameDataMgr>().IsLoaded);
        
        InitUserCharacterDetail(CharacterCardUtil.GetCardDetail(_id));
    }

    private void InitUserCharacterDetail(CharacterCardUtil.UserCardDetail cardDetail)
    {
        if (cardDetail == null) return;
        if(_gameDataMgr == null) _gameDataMgr = EightUtil.GetCore<GameDataMgr>();
        if (_gameDataMgr.IsLoaded == false) return;

        var localCsvData = _gameDataMgr.GetCsvData(GameDataMgr.DataId.CardUpgradeData);

        int key = _rate * 1000 + cardDetail.Level + 1;
        CardUpgradeData localData = null;
        if (localCsvData.ContainsKey(key))
        {
            localData = localCsvData[key][0].ToClass<CardUpgradeData>();
        }

        _cardLevel.text = cardDetail.Level.ToString();
        _expText.text = (localData != null) ? 
            $"{cardDetail.CardCount}/{localData.NeedCard}" : "MAX";
        
        float gaugePercent = (localData != null) ? Mathf.Min(1, (float) cardDetail.CardCount / localData.NeedCard) : 1;
        _expBarImage.transform.localScale = new Vector3(gaugePercent, 1, 1);
        if (localData == null) // MAX
        {
            _expFullGaugeObject.SetActive(false);
            return;
        }
        if(_expFullGaugeObject != null) _expFullGaugeObject.SetActive(gaugePercent >= 1);
    }

    public virtual void OnClicked()
    {
        // if (_isUnlock) return;
        if (_isActive == false)
        {
            _isActive = !_isActive;
            SetCardActive(_isActive);
        }
    }

    private void SetCardActive(bool active)
    {
        if (active)
        {
            _animator.enabled = true;
            _animator.Rebind();
        }

        _isActive = active;
        _animator.SetBool("isActive", active);
    }
    
    public void OnPopupCardInfo()
    {
        LobbyCardInfoState.SetCharacterUnitData(_unitData);
        LobbyPopupInterface.InvokeChangingState(LobbyPopupFSMSystem.LobbyPopupFSMState.CARD_INFO);
        if(_animator != null) SetCardActive(false);
    }

    public void OnPopupSettingDeck()
    {
        LobbyOriganizationEditState.SetCharacterUnitData(_unitData);
        LobbySceneInterface.InvokeChangingState(LobbyFSMSystem.LobbyFSMState.ORGANIZATION_EDIT);
        if(_animator != null) SetCardActive(false);
    }

    public void SetCardRender(bool enable)
    {
        if (_expFullGaugeObject != null)
        {
            var expAnimator = _expFullGaugeObject.GetComponent<Animator>();
            expAnimator.enabled = enable;
            if(enable) expAnimator.Rebind();
        }

        _isEnableRender = enable;
    }
}
