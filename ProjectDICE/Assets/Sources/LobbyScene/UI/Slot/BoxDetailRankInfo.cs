﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxDetailRankInfo : MonoBehaviour
{

    [SerializeField]
    private Image _gradeImage = null;

    [SerializeField]
    private Text _rankName = null;

    [SerializeField]
    private Text _countText = null;

    public void InitData(int grade, int kind, int count, float per = 1)
    {
        _gradeImage.sprite = LobbyCardUtil.GetFrameSprite(grade - 1);
        _rankName.text = LobbyCardUtil.GetCardGradeText(grade - 1) + (per >= 1 ? "" : $" {per*100f:F2}%");
        _rankName.color = LobbyCardUtil.GetCardColor(grade - 1);
        _countText.text = (per < 1 ? "0 ~ " : "") + $"{count}  <color=#FFDC59FF>{(kind > 1 ? "x" + kind : "")}</color>";
        gameObject.SetActive(true);
    }
}
