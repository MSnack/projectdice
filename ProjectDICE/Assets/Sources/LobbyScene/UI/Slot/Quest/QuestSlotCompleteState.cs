﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestSlotCompleteState : SlotStateBase<LobbyQuestSlotObject.State>
{
    
    private LobbyQuestSlotObject _slotObject = null;

    [SerializeField]
    private QuestCategory _completeQuestCategory = QuestCategory.DailyQuest;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = LobbyQuestSlotObject.State.COMPLETE;
    }
    
    public override void StartState()
    {
        base.StartState();
        if (_slotObject == null) _slotObject = GetComponent<LobbyQuestSlotObject>();
        QuestUtil.OnNotice(_completeQuestCategory, true);
    }

    public override void InvokeEvent()
    {
        GamePopupMgr.OpenLoadingPopup(true);
        QuestServerConnector.ReceiveQuestReward(_slotObject.QuestCategory, _slotObject.SlotId, (mailId) =>
        {
            var questLocalData = _slotObject.QuestLocalData;
            LobbyMailServerConnector.ReceiveMailItem(mailId, 
                (trans) =>
                {
                    MailSlotObject.ReceivedMailEvent(trans, questLocalData.ItemCategory);
                    
                    LobbyMailServerConnector.GetMails(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Mails));
                    _slotObject.QuestInfo.IsReceived = true;
                    QuestUtil.OnNotice(_completeQuestCategory, false);
                    LobbyRefreshUtil.OnRefresh(RefreshState.QuestValue);
                    
                    var info = new RewardItemInfo
                        {ItemCategory = (int) questLocalData.ItemCategory, Id = questLocalData.ItemId, Count = questLocalData.ItemCount};

                    GamePopupMgr.CloseLoadingPopup();
                    if(questLocalData.ItemCategory != ItemCategory.BOX)
                        GamePopupMgr.OpenRewardPopup(new List<RewardItemInfo> {info});
                });
        }, () =>
        {
            GamePopupMgr.OpenNormalPopup("경고", "좀 더 열심히하면 얻을 수 있을거에요!\n(임시팝업)");
            GamePopupMgr.CloseLoadingPopup();
            ChangeState(LobbyQuestSlotObject.State.NONE);
        });
    }
}
