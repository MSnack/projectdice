﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestSlotNoneState : SlotStateBase<LobbyQuestSlotObject.State>
{

    private LobbyQuestSlotObject _slotObject = null;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = LobbyQuestSlotObject.State.NONE;
    }

    public override void StartState()
    {
        base.StartState();
        if (_slotObject == null) _slotObject = (LobbyQuestSlotObject) SlotStateMgr;
    }

    public override void InvokeEvent()
    {
    }
}
