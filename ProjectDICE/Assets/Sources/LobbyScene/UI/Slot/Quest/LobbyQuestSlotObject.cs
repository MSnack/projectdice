﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbyQuestSlotObject : SlotStateMgr<LobbyQuestSlotObject.State>
{
    public enum State
    {
        NONE,
        RECEIVED,
        COMPLETE,
    }
    
    [SerializeField]
    protected Text _titleText = null;

    [SerializeField]
    protected Text _descriptionText = null;

    [SerializeField]
    protected Text _valueText = null;

    [SerializeField]
    protected GameObject _valueBarObject = null;

    [SerializeField]
    protected ImageAutoScaler _itemImage = null;

    [SerializeField]
    protected Text _itemCountText = null;

    [SerializeField]
    protected Text _itemCategoryText = null;
    
    protected ItemCategory _itemCategory = ItemCategory.NONE;
    protected int _itemId = SystemIndex.InvalidInt;
    protected int _itemCount = 1;
    protected string _title = null;
    protected string _description = null;
    protected int _targetValue = 0;
    protected int _currentValue = -1;
    protected QuestCategory _questCategory = QuestCategory.None;
    protected bool _isReceived = false;
    protected int _slotId = 0;

    private QuestInfo _questInfo = null;
    public QuestInfo QuestInfo => _questInfo;

    private QuestLocalData _questLocalData = null;
    public QuestLocalData QuestLocalData => _questLocalData;
    
    public QuestCategory QuestCategory => _questCategory;
    public int SlotId => _slotId;


    // Start is called before the first frame update
    protected virtual void Start()
    {
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.QuestValue, RefreshSlot);
        // Init();
    }

    private void OnDestroy()
    {
        ReleaseSlot();
    }

    protected virtual void Init()
    {
        _titleText.text = _title;
        _descriptionText.text = _description;
        _valueText.text = $"{_currentValue}/{_targetValue}";
        float scale = (float) _currentValue / _targetValue;
        _valueBarObject.transform.localScale = new Vector3(scale, 1, 1);
        
        
        var itemInfo = ItemCategoryUtil.GetItemInfo(_itemCategory, _itemId);
        _itemImage.sprite = itemInfo.SpriteImage;
        _itemImage.SetNativeRatioSize();
        _itemCountText.text = (_itemCount <= 0 ? "0" : $"{_itemCount:#,###}");
        if(_itemCategoryText != null) _itemCategoryText.text = itemInfo.Name;

        if (_isReceived)
        {
            ChangeState(State.RECEIVED);
        }
        else if (scale >= 1)
        {
            QuestUtil.OnNotice(_questCategory, true);
            ChangeState(State.COMPLETE);
        }
    }

    protected virtual void RefreshSlot()
    {
        if (_questInfo == null || _questLocalData == null)
        {
            Debug.LogAssertion("Quest data is null (LobbyQuestSlotObject)");
            return;
        }
        InitSlot(_questInfo, _questLocalData);
    }

    public virtual void InitSlot(QuestInfo info, QuestLocalData localData)
    {
        _questInfo = info;
        _questLocalData = localData;
        
        _title = localData?.Title;
        _description = localData?.Description;
        _targetValue = localData?.TargetValue ?? 0;
        _itemCategory = localData?.ItemCategory ?? ItemCategory.NONE;
        _itemId = localData?.ItemId ?? SystemIndex.InvalidInt;
        _itemCount = localData?.ItemCount ?? 0;

        _questCategory = (QuestCategory) info.Category;
        _currentValue = Math.Min(info.Value, _targetValue);
        _isReceived = info.IsReceived;
        _slotId = info.SlotId;
        
        Init();
    }

    public virtual void ReleaseSlot()
    {
        LobbyRefreshUtil.ReleaseRefreshEvent(RefreshState.QuestValue, RefreshSlot);
    }
    
    public void ClickSlot()
    {
        CurrentStateObject.InvokeEvent();
    }
    
}
