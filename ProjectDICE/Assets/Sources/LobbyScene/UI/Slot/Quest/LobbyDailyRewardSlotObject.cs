﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbyDailyRewardSlotObject : LobbyQuestSlotObject
{
    [SerializeField]
    private Image _cardFrameImage = null;

    [SerializeField]
    private GameObject _cardPreviewObject = null;

    [SerializeField]
    private Image _cardPreviewImage = null;
    
    protected override void Init()
    {
        if(_titleText != null) _titleText.text = _title;
        if(_descriptionText != null) _descriptionText.text = _description;
        var itemInfo = ItemCategoryUtil.GetItemInfo(_itemCategory, _itemId);
        _itemImage.sprite = itemInfo.SpriteImage;
        _itemImage.SetNativeRatioSize();
        _itemCountText.text = (_itemCount <= 0 ? "x0" : $"x{_itemCount:#,###}");
        if(_itemCategoryText != null) _itemCategoryText.text = itemInfo.Name;
        if (_valueText != null) _valueText.text = $"{_targetValue:#,###}";
        float scale = _targetValue > 0 ? (float) _currentValue / _targetValue : 1;

        bool isCardFrame = itemInfo.FrameImage != null;
        if (isCardFrame)
        {
            _itemImage.gameObject.SetActive(false);
            if(_cardPreviewImage != null) _cardPreviewImage.sprite = itemInfo.SpriteImage;
            _cardFrameImage.sprite = itemInfo.FrameImage;
            _cardFrameImage.gameObject.SetActive(true);
            if (_cardPreviewObject != null) _cardPreviewObject.SetActive(true);
        }

        _itemImage.transform.localScale = isCardFrame ? new Vector3(1.1f, 1.1f, 1.1f) : Vector3.one;
        
        if (_isReceived)
        {
            ChangeState(State.RECEIVED);
        }
        else if (_currentValue >= _targetValue)
        {
            QuestUtil.OnNotice(_questCategory, true);
            ChangeState(State.COMPLETE);
        }

    }

    public override void ReleaseSlot()
    {
        base.ReleaseSlot();
    }
}
