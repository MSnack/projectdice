﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestSlotReceivedState : SlotStateBase<LobbyQuestSlotObject.State>
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = LobbyQuestSlotObject.State.RECEIVED;
    }

    public override void InvokeEvent()
    {
        
    }
}
