﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardPopupSlotObject : MonoBehaviour
{

    [SerializeField]
    private ImageAutoScaler _image = null;

    [SerializeField]
    private GameObject _cardPreviewObject = null;

    [SerializeField]
    private Image _cardPreviewImage = null;

    [SerializeField]
    private Image _frameImage = null;

    [SerializeField]
    private Text _countText = null;

    [SerializeField]
    private Text _itemNameText = null;

    [SerializeField]
    private Transform _pivot = null;

    private GoodsID _currentGoodsId = GoodsID.UNKOWN;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    public void InitData(RewardItemInfo info)
    {
        var item = ItemCategoryUtil.GetItemInfo((ItemCategory) info.ItemCategory, info.Id);
        _currentGoodsId = (GoodsID) info.Id;
        _frameImage.gameObject.SetActive(item.FrameImage != null);
        if (item.FrameImage != null)
        {
            _cardPreviewObject.SetActive(true);
            _frameImage.sprite = item.FrameImage;
            _cardPreviewImage.sprite = item.SpriteImage;
            _image.gameObject.SetActive(false);
        }
        else
        {
            _image.sprite = item.SpriteImage;
        }
        _countText.text = $"x{info.Count}";
        _itemNameText.text = item.Name;
    }

    public void PlayEffect()
    {
        if (_pivot == null) return;
        if (_currentGoodsId == GoodsID.GOLD)
        {
            LobbyRewardEffectUtil.PlayEffect(GoodsID.GOLD, _pivot);
        }
        if (_currentGoodsId == GoodsID.STAR)
        {
            LobbyRewardEffectUtil.PlayEffect(GoodsID.STAR, _pivot);
        }
    }
}
