﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class BoxSlotObject : SlotStateMgr<BoxSlotObject.State>
{
    public enum State
    {
        NULL,
        NONE,
        WAIT,
        OPEN,
    }

    [SerializeField]
    protected short _boxIndex = 1;

    [SerializeField]
    private GameObject _boxObject = null;

    private UserBoxInvenInfo _userBox = null;

    protected override void Awake()
    {
        base.Awake();
        StartCoroutine(InitUserBoxSlot());
    }

    // Start is called before the first frame update
    void Start()
    {
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.Boxes, RefreshBoxSlot);
    }
    
    private void OnEnable()
    {
        RefreshBoxSlot();
    }

    private void OnDestroy()
    {
        LobbyRefreshUtil.ReleaseRefreshEvent(RefreshState.Boxes, RefreshBoxSlot);
    }

    protected void RefreshBoxSlot()
    {
        if (gameObject.activeSelf == false)
            return;

        StartCoroutine(InitUserBoxSlot());
    }

    protected virtual IEnumerator InitUserBoxSlot()
    {
        yield return new WaitUntil(() => LobbyBoxServerConnector.IsLoaded && LobbyBoxUtil.IsLoaded);

        var slotInfo = LobbyBoxUtil.GetUserBoxInventory(_boxIndex);
        _userBox = slotInfo;
        

        if (slotInfo == null || slotInfo.BoxId <= 0)
        {
            ChangeState(State.NULL);
            yield break;
        }

        var objects = _boxObject.GetComponentsInChildren<Transform>();
        foreach (var trans in objects)
        {
            if (trans == _boxObject.transform) continue;
            Destroy(trans.gameObject);
        }
        Instantiate(LobbyBoxUtil.GetBoxPrefab(slotInfo.BoxId), _boxObject.transform);

        if (slotInfo.OpenStartTime.Equals(DateTime.MinValue))
        {
            ChangeState(State.NONE);
            yield break;
        }

        var elapsedTime = DateTime.Now - slotInfo.OpenStartTime;
        if (elapsedTime.TotalSeconds < LobbyBoxUtil.GetBoxLocalData(slotInfo.BoxId).Time)
        {
            ChangeState(State.WAIT);
            yield break;
        }
        
        ChangeState(State.OPEN);

    }

    public UserBoxInvenInfo GetUserBoxInfo()
    {
        return _userBox;
    }

    public void ClickBox()
    {
        CurrentStateObject.InvokeEvent();
    }
}
