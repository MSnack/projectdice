﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxPreviewSlotObject : BoxSlotObject
{

    protected override IEnumerator InitUserBoxSlot()
    {
        yield return new WaitUntil(() => LobbyBoxServerConnector.IsLoaded && LobbyBoxUtil.IsLoaded);

        var slotInfos = LobbyBoxUtil.GetUserBoxInventories();
        var currentBox = slotInfos[0];
        int currentLevel = 0;

        foreach (var info in slotInfos)
        {
            int level = 0;
            if (info == null) continue;
            level = info.BoxId;
            var elapsedTime = DateTime.Now - info.OpenStartTime;
            if (info.BoxId <= 0) level = 0;
            else if (info.OpenStartTime.Equals(DateTime.MinValue)) { }
            else if (elapsedTime.TotalSeconds < LobbyBoxUtil.GetBoxLocalData(info.BoxId).Time) level += 1000;
            else level += 2000;

            if (currentLevel < level)
            {
                currentBox = info;
                currentLevel = level;
            }
        }

        if (currentBox != null)
        {
            _boxIndex = (short) currentBox.BoxIndex;
        }
        
        yield return base.InitUserBoxSlot();
    }
}
