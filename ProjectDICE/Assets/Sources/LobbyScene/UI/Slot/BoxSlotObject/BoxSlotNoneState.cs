﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class BoxSlotNoneState : SlotStateBase<BoxSlotObject.State>
{
    [SerializeField]
    private Text _remainTimeText;


    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    protected override void Initialize()
    {
        State = BoxSlotObject.State.NONE;
    }

    public override void StartState()
    {
        base.StartState();
        var t = TimeSpan.FromSeconds(
            LobbyBoxUtil.GetBoxLocalData(((BoxSlotObject) SlotStateMgr).GetUserBoxInfo().BoxId).Time);

        _remainTimeText.text = " "
                               + (t.Hours > 0 ? $"{t.Hours}시간 " : "")
                               + (t.Minutes > 0 ? $"{t.Minutes}분 " : "")
                               + (t.Seconds > 0 ? $"{t.Seconds}초 " : "");
    }

    public override void EndState()
    {
        base.EndState();
    }

    public override void InvokeEvent()
    {
        GamePopupMgr.OpenLoadingPopup(true);
        
        var userBox = ((BoxSlotObject) SlotStateMgr).GetUserBoxInfo();
        LobbyBoxServerConnector.OpenBox(userBox.BoxId, userBox.BoxIndex,
            LobbyBoxServerConnector.BoxReference.INVENTORY_OPEN,
            (result) =>
            {
                if (result.Result == 1)
                {
                    LobbyBoxServerConnector.GetUserBoxInventory(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Boxes));
                    GamePopupMgr.CloseLoadingPopup();
                }
                else
                {
                    GamePopupMgr.OpenNormalPopup("경고", "상자는 하나씩 열 수 있습니다.");
                    GamePopupMgr.CloseLoadingPopup();
                }
            });
    }
}