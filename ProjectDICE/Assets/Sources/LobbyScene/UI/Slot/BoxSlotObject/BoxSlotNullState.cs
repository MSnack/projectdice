﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSlotNullState : SlotStateBase<BoxSlotObject.State>
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void Initialize()
    {
        State = BoxSlotObject.State.NULL;
    }


    public override void InvokeEvent()
    {
        
    }
}
