﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class BoxSlotOpenState : SlotStateBase<BoxSlotObject.State>
{

    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void Initialize()
    {
        State = BoxSlotObject.State.OPEN;
    }

    public override void InvokeEvent()
    {
        GamePopupMgr.OpenLoadingPopup();
        
        var userBox = ((BoxSlotObject) SlotStateMgr).GetUserBoxInfo();
        
        LobbyBoxServerConnector.OpenBox(userBox.BoxId, userBox.BoxIndex,
        LobbyBoxServerConnector.BoxReference.INVENTORY,
        (result) =>
        {
            if (result.Result != 1)
            {
                GamePopupMgr.OpenNormalPopup("경고", "상자를 여는데 문제가 발생했습니다.\n에러코드 : " + result.Result);
                GamePopupMgr.CloseLoadingPopup();
                return;
            }
            GamePopupMgr.CloseLoadingPopup();
            LobbyOpenBoxUI.OpenBoxUI(result);
            QuestUtil.UpdateQuestEvent(QuestCondition.OpenBoxAtLobby, 1);
        });
    }
}
