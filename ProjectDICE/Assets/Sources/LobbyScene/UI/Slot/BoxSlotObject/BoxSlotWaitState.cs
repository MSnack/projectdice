﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxSlotWaitState : SlotStateBase<BoxSlotObject.State>
{

    [SerializeField]
    private Text _waitTimeText = null;

    private DateTime _endTime;

    private int _boxIndex = -1;
    private int _boxId = -1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void StartState()
    {
        base.StartState();
        var info = ((BoxSlotObject) SlotStateMgr).GetUserBoxInfo();
        _endTime = info.OpenStartTime;
        _endTime = _endTime.AddSeconds(LobbyBoxUtil.GetBoxLocalData(info.BoxId).Time);
        _boxIndex = info.BoxIndex;
        _boxId = info.BoxId;
    }

    public override void InvokeEvent()
    {
        TimeSpan t = _endTime - DateTime.Now;
        int price = LobbyBoxUtil.GetBoxOpenTimeData((int) t.TotalSeconds).PriceStar;
        LobbyBoxDetailPopupUI.RemoveTimeUIOpen(_boxId, price, () =>
        {
            GamePopupMgr.OpenLoadingPopup();
            LobbyBoxServerConnector.RemoveBoxTime(_boxIndex, () =>
            {
                ChangeState(BoxSlotObject.State.OPEN);
                LobbyGoodsServerConnector.GetAllGoods(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Goods));
                GamePopupMgr.CloseLoadingPopup();
            }, (error) =>
            {
                GamePopupMgr.OpenNormalPopup("경고", "스타가 부족합니다.");
                GamePopupMgr.CloseLoadingPopup();
            });
        }, _endTime);
    }

    // Update is called once per frame
    void Update()
    {
        TimeSpan t = _endTime - DateTime.Now;
        _waitTimeText.text = " "
                             + (t.Hours > 0 ? $"{t.Hours}시간 " : "")
                             + (t.Minutes > 0 ? $"{t.Minutes}분 " : "")
                             + (t.Hours <= 0 ? $"{t.Seconds}초 " : "");

        if (t.TotalSeconds <= 0)
        {
            ChangeState(BoxSlotObject.State.OPEN);
        }
    }

    protected override void Initialize()
    {
        State = BoxSlotObject.State.WAIT;
    }
}
