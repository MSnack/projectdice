﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MembershipSlotOverActiveState : SlotStateBase<MembershipSlotObject.State>
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = MembershipSlotObject.State.OVER_ACTIVE;
    }

    public override void InvokeEvent()
    {
        
    }
}
