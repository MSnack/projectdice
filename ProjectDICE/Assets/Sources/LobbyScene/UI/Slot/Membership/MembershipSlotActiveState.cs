﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MembershipSlotActiveState : SlotStateBase<MembershipSlotObject.State>
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Initialize()
    {
        State = MembershipSlotObject.State.ACTIVE;
    }

    public override void InvokeEvent()
    {
        
    }
}
