﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MembershipInfo
{
    public int Level;
    public MembershipSlotObject.State State;
    
    public QuestInfo LeftInfo;
    public QuestLocalData LeftData;

    public QuestInfo RightInfo;
    public QuestLocalData RightData;

    public bool IsUpgraded;
}

public class MembershipSlotObject : SlotStateMgr<MembershipSlotObject.State>
{
    public enum State
    {
        NONE,
        ACTIVE,
        OVER_ACTIVE,
    }

    [SerializeField]
    private bool _isUpgraded = false;
    public bool IsUpgraded => _isUpgraded;

    [SerializeField]
    private LobbyDailyRewardSlotObject _leftSlot = null;
    [SerializeField]
    private LobbyDailyRewardSlotObject _rightSlot = null;
    [SerializeField]
    private GameObject _rightDisableImage = null;

    [SerializeField]
    private Text _levelText = null;

    private MembershipInfo _info = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public LobbyDailyRewardSlotObject GetSlot(bool isLeft)
    {
        return isLeft ? _leftSlot : _rightSlot;
    }

    public void InitData(MembershipInfo info)
    {
        _info = info;
        _levelText.text = info.LeftData.Title;
        _isUpgraded = info.IsUpgraded;

        {
            var button = _rightSlot.GetComponent<Button>();

            button.enabled = _isUpgraded;
            _rightDisableImage.SetActive(!_isUpgraded);
        }

        _leftSlot?.InitSlot(info.LeftInfo, info.LeftData);
        _rightSlot?.InitSlot(info.RightInfo, info.RightData);
        
        ChangeState(info.State);
    }

}
