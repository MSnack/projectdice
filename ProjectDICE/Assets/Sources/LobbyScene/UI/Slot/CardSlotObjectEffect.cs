﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSlotObjectEffect : MonoBehaviour
{
    [Header("Effects")]
    [SerializeField]
    private GameObject _hero;

    [SerializeField]
    private GameObject _legend;
    
    public void SetUnitEffect(CharacterUnitData data)
    {
        ResetEffects();
        int grade = data.Grade;

        switch (grade)
        {
            case 4:
                _hero.SetActive(true);
                break;
            
            case 5:
                _legend.SetActive(true);
                break;
        }
    }

    private void ResetEffects()
    {
        int count = transform.childCount;
        
        for (int i = 0; i < count; ++i)
        {
            var effect = transform.GetChild(i);
            effect.gameObject.SetActive(false);
        }
    }
}
