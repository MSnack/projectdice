﻿using System;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public abstract class SlotStateMgr<TStateEnum> : MonoBehaviour
    where TStateEnum : Enum
{
    [SerializeField, ReadOnly()]
    private TStateEnum _currentState;
    private SlotStateBase<TStateEnum> _currentStateObject;

    private Dictionary<TStateEnum, SlotStateBase<TStateEnum>> _states =
        new Dictionary<TStateEnum, SlotStateBase<TStateEnum>>();

    public TStateEnum CurrentState => _currentState;
    public SlotStateBase<TStateEnum> CurrentStateObject => _currentStateObject;

    private bool _isLoaded = false;

    protected virtual void Awake()
    {
        if (!_isLoaded) InitStateMgr();
    }

    private void InitStateMgr()
    {
        var states = GetComponents<SlotStateBase<TStateEnum>>();

        foreach (var state in states)
        {
            state.enabled = false;
            _states.Add(state.State, state);
        }

        _currentStateObject = _states[_currentState];
        _currentStateObject.enabled = true;
        _currentStateObject.StartState();
        _isLoaded = true;
    }

    public virtual void ChangeState(TStateEnum state, bool forceChange = false)
    {
        if (!_isLoaded) InitStateMgr();
        if (Equals(_currentState, state) && !forceChange) return;
        if (!_states.ContainsKey(state)) return;
        
        if (_currentStateObject != null)
        {
            _currentStateObject.EndState();
            _currentStateObject.enabled = false;
        }

        _currentStateObject = _states[state];
        _currentState = _currentStateObject.State;
        _currentStateObject.enabled = true;
        _currentStateObject.StartState();

    }
}