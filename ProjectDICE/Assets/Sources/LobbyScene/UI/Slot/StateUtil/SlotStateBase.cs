﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork.FSM;
using UnityEngine;
[RequireComponent(typeof(SlotStateMgr<>))]
public abstract class SlotStateBase<TStateEnum> : EightStateBase<TStateEnum>
    where TStateEnum : System.Enum
{
    [SerializeField]
    private SlotStateMgr<TStateEnum> _slotStateMgr = null;
    public SlotStateMgr<TStateEnum> SlotStateMgr => _slotStateMgr;

    [SerializeField]
    protected GameObject[] _activeObjects;

    [SerializeField]
    protected GameObject[] _inactiveObjects;

    private void Awake()
    {
        if(_slotStateMgr == null) _slotStateMgr = GetComponent<SlotStateMgr<TStateEnum>>();
        Initialize();
    }

    protected void Reset()
    {
        _slotStateMgr = GetComponent<SlotStateMgr<TStateEnum>>();
        Initialize();
    }

    public override void StartState()
    {
        foreach (var obj in _activeObjects)
        {
            obj.SetActive(true);
        }
        
        foreach (var obj in _inactiveObjects)
        {
            obj.SetActive(false);
        }
    }

    public override void EndState()
    {
        foreach (var obj in _activeObjects)
        {
            obj.SetActive(false);
        }
        
        foreach (var obj in _inactiveObjects)
        {
            obj.SetActive(true);
        }
    }

    public virtual void ChangeState(TStateEnum state)
    {
        SlotStateMgr.ChangeState(state);
    }

    public abstract void InvokeEvent();
}
