﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class LobbyDebugUI : ServerConnectorBase
{
    protected override void Awake()
    {
        base.Awake();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void Init()
    {
        base.Init();
        _isLoaded = true;
    }

    public void GetBox(int boxId)
    {
        var infos = LobbyBoxUtil.GetUserBoxInventories();

        UserBoxInvenInfo targetInfo = null;
        for (int i = 0; i < infos.Count; ++i)
        {
            var info = infos[i];
            if (info == null)
            {
                targetInfo = new UserBoxInvenInfo
                {
                    BoxId = boxId, BoxIndex = i + 1
                };
                break;
            }
            if(info.BoxId >= 0) continue;

            targetInfo = info;
            break;
        }

        if (targetInfo == null)
        {
            GamePopupMgr.OpenNormalPopup("경고", "상자 슬롯이 모두 찼습니다.\n비운 뒤 다시 시도해주세요!");
            return;
        }
        
        GamePopupMgr.OpenLoadingPopup();
        Translator.SetSendData("boxIndex", targetInfo.BoxIndex);
        Translator.SetSendData("boxId", boxId);
        Translator.Request(GameWebRequestType.Debug_CreateUserBox, 
        (trans) =>
        {
            var result = trans.GetRequestData();
            GamePopupMgr.CloseLoadingPopup();
            LobbyBoxServerConnector.GetUserBoxInventory(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Boxes));
            
            if (int.Parse(result["result"].ToString()) != 1)
            {
                GamePopupMgr.OpenNormalPopup("경고", "DB 내 상자 슬롯이 모두 찼습니다.");
                return;
            }
            GamePopupMgr.OpenNormalPopup("알림", "해당 상자를 슬롯에 추가하였습니다.");
        });

    }

    public void RemoveBoxTime()
    {
        // GamePopupMgr.OpenLoadingPopup();
        // Translator.Request(GameWebRequestType.Debug_RemoveBoxTime, (trans) =>
        // {
        //     GamePopupMgr.CloseLoadingPopup();
        //     LobbyBoxServerConnector.GetUserBoxInventory(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Boxes));
        //     GamePopupMgr.OpenNormalPopup("알림", "상자의 시간을 모두 앞당겼습니다.\n상자를 확인해주세요!");
        // });
    }

    public void UpdateTrophy(int count)
    {
        // GamePopupMgr.OpenLoadingPopup();
        // LobbyGoodsServerConnector.IncrementGoods(GoodsID.TROPHY, count, () =>
        // {
        //     if (count > 0) UpdateTrophyQuest(count);
        //     else
        //     {
        //         GamePopupMgr.CloseLoadingPopup();
        //         GamePopupMgr.OpenNormalPopup("알림", "트로피를 " + count + "개 획득 했습니다.");
        //         LobbyRefreshUtil.OnRefresh(RefreshState.Goods);
        //     }
        // });
    }

    private void UpdateTrophyQuest(int count)
    {
        // QuestUtil.UpdateQuestEvent(QuestCondition.GetTrophy, count, true, () =>
        // {
        //     GamePopupMgr.CloseLoadingPopup();
        //     GamePopupMgr.OpenNormalPopup("알림", "트로피를 " + count + "개 획득 했습니다.");
        //     LobbyRefreshUtil.OnRefresh(RefreshState.Goods);
        // });
    }
    
}
