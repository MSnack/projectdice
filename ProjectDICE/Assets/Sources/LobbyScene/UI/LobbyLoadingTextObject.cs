﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyLoadingTextObject : MonoBehaviour
{

    [SerializeField]
    private Text _text = null;

    [SerializeField]
    private GameObject _targetObject = null;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        int childCount = _targetObject.transform.childCount;
        if (childCount <= 0) return;
        if (childCount == 1 && _targetObject.transform.GetChild(0) == transform)
            return;

        var fitters = GetComponentsInParent<ContentSizeFitter>(true);
        for(int i = 0; i < fitters.Length; ++i)
        {
            var fitter = fitters[i];
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform) fitter.transform);
        }
        gameObject.SetActive(false);
    }

    private void Reset()
    {
        _text = GetComponent<Text>();
    }
}
