﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class LobbyUIScaler : MonoBehaviour
{
    [SerializeField, ReadOnly]
    private List<RectTransform> _uiTransforms;

    [SerializeField, TooltipAttribute("0부터 시작")]
    private int _centerIndex = 2;
    
    // Start is called before the first frame update
    void Start()
    {
        _uiTransforms = new List<RectTransform>();
        for (int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i).gameObject;
            var comp = child.GetComponent<RectTransform>();
            if(comp == null || !comp.gameObject.activeInHierarchy) continue;
            _uiTransforms.Add(comp);
        }

        for (int i = 0; i < _uiTransforms.Count; i++)
        {
            var rect = _uiTransforms[i];
            int indexScale = i - _centerIndex;
            float width = rect.rect.width * indexScale;
            rect.offsetMin = new Vector2(width, 0);
            rect.offsetMax = new Vector2(width, 0);
        }
        
    }
}
