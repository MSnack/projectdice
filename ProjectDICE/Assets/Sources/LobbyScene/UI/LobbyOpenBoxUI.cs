﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LobbyOpenBoxUI : LobbyOtherPopupBase
{
    private class RewardItemNode
    {
        public Sprite FrameSprite;
        public Sprite ItemSprite;
        public CharacterUnitData UnitData;
        public RewardItemInfo Info;
    }

    private static UnityAction<BoxResultInfo, UnityAction> _onOpenBoxUI;
    private static UnityAction _onCloseBoxUI;
    
    private GameUnitDataMgr _gameUnitDataMgr = null;

    [SerializeField]
    private GameObject _backgroundObject = null;
    
    [SerializeField, ReadOnly()]
    private Animator _animator = null;

    [Header("UI")]
    [SerializeField]
    private GameObject _panel = null;

    [SerializeField]
    private Text _itemText = null;

    [SerializeField]
    private string _itemString = "{0} x{1}";

    private readonly Dictionary<int, GameObject> _openEffectObjects = new Dictionary<int, GameObject>();

    // Start is called before the first frame update
    void Start()
    {
    }

    public override void Init()
    {
        base.Init();
        SetupComplete();
        _backgroundObject.SetActive(true);
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        _gameUnitDataMgr = EightUtil.GetCore<GameUnitDataMgr>();
        
        _onOpenBoxUI = null;
        _onCloseBoxUI = null;
        _onOpenBoxUI += OnOpenBoxUI;
        _onCloseBoxUI += OnCloseBoxUI;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        _panel.SetActive(false);
    }

    private void OnDestroy()
    {
        foreach (var effectObject in _openEffectObjects)
        {
            Destroy(effectObject.Value);
        }

        _openEffectObjects.Clear();
    }

    public static void OpenBoxUI(BoxResultInfo info, UnityAction endEvent = null)
    {
        _onOpenBoxUI?.Invoke(info, endEvent);
    }

    public static void CloseBoxUI()
    {
        _onCloseBoxUI?.Invoke();
    }

    private void OnOpenBoxUI(BoxResultInfo info, UnityAction endEvent = null)
    {
        gameObject.SetActive(true);
        var itemList = new List<RewardItemNode>();
        
        GameObject effectObject = null;
        if (_openEffectObjects.ContainsKey(info.BoxId)) 
            effectObject = _openEffectObjects[info.BoxId];
        else
        {
            effectObject = Instantiate(LobbyBoxUtil.GetBoxPrefab(info.BoxId, true), transform);
            effectObject.SetActive(false);
            effectObject.transform.localScale = new Vector3(111, 111, 111);
            _openEffectObjects.Add(info.BoxId, effectObject);
        }

        _animator = effectObject.GetComponent<Animator>();

        if (info.Result == 1)
        {
            // 상자 결과값 적용
            foreach (var item in info.ResultValue)
            {
                var result = new RewardItemNode();
                if (item.ItemCategory != (int) ItemCategory.CHARACTER)
                {
                    result.ItemSprite = LobbyGoodsUtil.GetGoodsSprite((GoodsID) item.Id);
                    itemList.Add(result);
                    continue;
                }
        
                result.UnitData = _gameUnitDataMgr.GetCharacaterData(item.Id);
                result.ItemSprite = result.UnitData?.UnitPreviewSprite;
                result.FrameSprite = LobbyCardUtil.GetFrameSprite(result.UnitData?.Grade - 1 ?? 1);
                result.Info = item;
                
                itemList.Add(result);
            }
            effectObject.SetActive(true);
            // GetComponent<GameEffectObject>().UpdateOrderInLayer();
            endEvent += () => GamePopupMgr.OpenRewardPopup(info.ResultValue);
            StartCoroutine(OnOpenEvent(itemList, endEvent));
                
            //캐릭터 및 상자 인벤 재갱신
            LobbyBoxServerConnector.GetUserBoxInventory(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Boxes));
            LobbyCharacterCardServerConnector.GetCurrentCharList( () =>
            {
                LobbyRefreshUtil.OnRefresh(RefreshState.Organization);
                LobbyRefreshUtil.OnRefresh(RefreshState.Cards);
            });
            LobbyNoticeUtil.OnNotice(LobbyNoticeUtil.NoticeState.Cards);
            return;
        }
        
        GamePopupMgr.OpenNormalPopup("알림", "게임을 재시작 해주세요.");
        
        OnCloseBoxUI();
        endEvent?.Invoke();
        
        // gameObject.SetActive(true);
        //
        // if (info.Result == 1)
        // {
        //     // 상자 결과값 적용
        //     // 임시 적용
        //     string resultStr = "";
        //
        //     foreach (var card in info.ResultValue)
        //     {
        //         int grade = card.Id / 1000;
        //         string name = _gameUnitDataMgr.GetCharacaterData(card.Id)?.UnitName ?? "Unknown";
        //
        //         resultStr += "[" + grade + "성 카드] " + name + " (총 " + card.Count + "장)\n";
        //     }
        //         
        //     GamePopupMgr.OpenNormalPopup("상자 결과 임시 팝업", resultStr);
        //         
        //     //캐릭터 및 상자 인벤 재갱신
        //     LobbyBoxServerConnector.GetUserBoxInventory(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Boxes));
        //     LobbyCharacterCardServerConnector.GetCurrentCharList(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Organization));
        //     LobbyNoticeUtil.OnNotice(LobbyNoticeUtil.NoticeState.Cards);
        //     endEvent?.Invoke();
        //     OnCloseBoxUI();
        //     return;
        // }
        //
        // GamePopupMgr.OpenNormalPopup("알림", "게임을 재시작 해주세요.");
        //
        // OnCloseBoxUI();
        // endEvent?.Invoke();

    }

    private void OnCloseBoxUI()
    {
        foreach (var effectObject in _openEffectObjects)
        {
            effectObject.Value.SetActive(false);
        }
        gameObject.SetActive(false);
    }

    private IEnumerator OnOpenEvent(List<RewardItemNode> itemList, UnityAction endEvent)
    {
        InitDetails(itemList[0]);
        yield return new WaitUntil(() => !_animator.GetCurrentAnimatorStateInfo(0).IsName("BoxOpen"));
        _panel.SetActive(true);
        
        for(int i = 1; i < itemList.Count; ++i)
        {
            var node = itemList[i];
            yield return new WaitUntil(() => _animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1);
            yield return new WaitUntil(() => Input.GetMouseButtonUp(0));
            _animator.Play("BoxOpenCard", -1, 0f);
            InitDetails(node);
            _panel.SetActive(false);
            yield return null;
            _panel.SetActive(true);
            
        }
        
        yield return new WaitUntil(() => _animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1);
        yield return new WaitUntil(() => Input.GetMouseButtonUp(0));
        endEvent?.Invoke();
        OnCloseBoxUI();
        _panel.SetActive(false);
    }
    
    private void InitDetails(RewardItemNode node)
    {
        var spriteChanger = _animator.GetComponentInChildren<LobbyBoxOpenSpriteChanger>(true);
        var effectChanger = spriteChanger?.GetComponentInChildren<CardSlotObjectEffect>();

        var itemInfo = ItemCategoryUtil.GetItemInfo((ItemCategory) node.Info.ItemCategory, node.Info.Id);
        _itemText.text = string.Format(_itemString, itemInfo.Name, node.Info.Count);
        
        spriteChanger?.ChangeCardSprite(node?.ItemSprite);
        spriteChanger?.ChangeCardFrame(node?.FrameSprite);
        if (node?.UnitData != null)
        {
            effectChanger?.SetUnitEffect(node.UnitData);
        }
    }
}
