﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbySettingDeckUI : EightReceiver
{
    [SerializeField]
    private LobbyDeckLoader _deckLoader = null;

    [SerializeField]
    private LobbyUserDeckMgr _userDeckMgr = null;

    [SerializeField]
    private UIAlphaController _alphaController = null;

    private int _selectedCharId = SystemIndex.InvalidInt;
    private int _currentDeckId = 1;

    [SerializeField]
    private GameObject[] _inactiveDeckButtons;

    [SerializeField]
    private CardSlotObject _previewCharacterCardObject = null;

    [SerializeField]
    private Text _characterName = null;

    [SerializeField]
    private Text _characterGrade = null;

    [SerializeField]
    private Text _characterDescription = null;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        _alphaController.Alpha = 0;
        _currentDeckId = _userDeckMgr.CurrentDeckIndex;
        for (int i = 0; i < _inactiveDeckButtons.Length; i++)
        {
            var bt = _inactiveDeckButtons[i];
            bt.SetActive(i != _currentDeckId - 1);
        }
        LoadDecks(_currentDeckId);
    }

    public void SetData(CharacterUnitData data)
    {
        _selectedCharId = data.DataID;
        _previewCharacterCardObject.InitData(data);
        _characterName.text = data.UnitName;
        _characterGrade.color = LobbyCardUtil.GetCardColor(data.Grade - 1);
        _characterGrade.text = LobbyCardUtil.GetCardGradeText(data.Grade - 1);
        _characterDescription.text = data.Description;
    }
    
    public void LoadDecks(int index)
    {
        _currentDeckId = index;
        _userDeckMgr.CurrentDeckIndex = _currentDeckId;
        _deckLoader.LoadDecks(index);
    }

    public void ChangeCharacter(int index)
    {
        LobbyUserDeckServerConnector.GetDeckData(_currentDeckId, (info) => UpdateDeck(index, info));
    }

    private void UpdateDeck(int index, DeckInfo info)
    {
        --index;
        
        //중복 체크
        int[] charId = { info.deck1, info.deck2, info.deck3, info.deck4, info.deck5 };
        var findIndex = Array.IndexOf(charId, _selectedCharId);
        if (findIndex >= 0)
        {
            //swap
            charId[findIndex] = charId[index];
        }
        
        charId[index] = _selectedCharId;

        info.deck1 = charId[0];
        info.deck2 = charId[1];
        info.deck3 = charId[2];
        info.deck4 = charId[3];
        info.deck5 = charId[4];
            
        LobbyUserDeckServerConnector.SetDeckData(info);
        _userDeckMgr.LoadDecks();
    }
}
