﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyUIHider : MonoBehaviour
{

    [SerializeField]
    private Canvas _targetCanvas = null;

    [SerializeField]
    private LobbyUIMovement _lobbyUIMovement = null;

    [SerializeField]
    private LobbyFSMSystem.LobbyFSMState _state;

    [SerializeField]
    private LobbyFSMSystem _lobbyFsmSystem = null;

    [SerializeField]
    private LobbyFSMSystem.LobbyFSMState[] _moreStates = null;

    private bool _prevIsMoving = false;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (_lobbyUIMovement.IsMoving)
        {
            _targetCanvas.enabled = true;
            _prevIsMoving = true;
            return;
        }
        
        if (_prevIsMoving && _lobbyFsmSystem.CurrState != _state)
        {
            bool isState = false;
            foreach (var state in _moreStates)
            {
                if (_lobbyFsmSystem.CurrState != state) continue;
                isState = true;
                break;
            }

            _targetCanvas.enabled = isState;
        }
        _prevIsMoving = false;
    }
}
