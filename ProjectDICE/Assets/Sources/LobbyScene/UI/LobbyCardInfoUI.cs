﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;
using WebSocketSharp;

public class LobbyCardInfoUI : EightReceiver
{

    [Header("Info Top UI")]
    [SerializeField]
    private Image _illustrationImage = null;

    [SerializeField]
    private Transform _contentTransform = null;

    [SerializeField]
    private Text _cardLevel = null;
    private int _level;

    [SerializeField]
    private Animator _cardLevelAnimator = null;

    [SerializeField]
    private Image _cardRankImage = null;
    
    [SerializeField]
    private Text _cardName = null;

    [SerializeField]
    private Text _cardGrade = null;

    [SerializeField]
    private Text _cardDescription = null;

    [SerializeField]
    private Text _skillDescription = null;

    [SerializeField]
    private GameObject _effectObject = null;

    [SerializeField]
    private Button _favorCharButton = null;

    [SerializeField]
    private GameObject _favorEnableObject = null;

    [Header("Info Middle UI")]
    [SerializeField]
    private Text _damageText = null;

    [SerializeField]
    private Text _attSpeedText = null;

    [SerializeField]
    private Text _criticalPerText = null;

    [SerializeField]
    private EtcValueClass[] _etcTexts;

    [Header("Info Bottom UI")]

    [SerializeField]
    private Text _currentGold = null;
    
    [SerializeField]
    private Image _expBarImage = null;
    
    [SerializeField]
    private GameObject _expFullGaugeObject = null;
    
    [SerializeField]
    private Text _expText = null;
    
    [SerializeField]
    private Button _upgradeButton = null;

    [SerializeField]
    private GameObject _upgradeBlockImage = null;

    [SerializeField]
    private Slider _illustSlider = null;

    [SerializeField]
    private Text _upgradeGoldText = null;

    [SerializeField]
    private Button _setPartyButton = null;

    [Header("Story UI"), SerializeField]
    private Text _storyTitle = null;

    [SerializeField]
    private Text _storyDescription = null;

    [SerializeField]
    private ContentSizeFitter[] _fitters = null;

    private CharacterUnitData _unitData = null;
    
    private GameDataMgr _gameDataMgr = null;

    private GameIllustMgr _gameIllustMgr = null;

    private GameBattleInfo _battleInfo = null;

    private bool _isLocked = false;

    [Header("Locked")]
    [SerializeField]
    private Material _grayMaterial = null;

    [SerializeField]
    private Image _backgroundImage = null;

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        _gameDataMgr = EightUtil.GetCore<GameDataMgr>();
        _gameIllustMgr = EightUtil.GetCore<GameIllustMgr>();

        LobbyRefreshUtil.SetRefreshEvent(RefreshState.Cards, RefreshData);
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.Goods, SetDetailData);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        _illustSlider.value = 0.5f;
        ((RectTransform)_contentTransform).anchoredPosition = Vector2.zero;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        // _effectObject.SetActive(false);
        // _illustrationImage.sprite = null;
        _illustrationImage.gameObject.SetActive(false);
        _gameIllustMgr.ReleaseIllustSprite(_unitData.DataID);
        _illustrationImage.sprite = null;
        _cardLevelAnimator.enabled = false;
    }

    public void SetDetailData()
    {
        _upgradeButton.gameObject.SetActive(!_isLocked);
        _setPartyButton.gameObject.SetActive(!_isLocked);
        _favorCharButton.gameObject.SetActive(!_isLocked);
        if (_isLocked)
        {
            _expText.text = "미획득";
            _expBarImage.transform.localScale = Vector3.zero;
            _expFullGaugeObject.SetActive(false);
            return;
        }
        //Get Local Card Data
        if(_gameDataMgr == null) _gameDataMgr = EightUtil.GetCore<GameDataMgr>();
        if (_gameDataMgr.IsLoaded == false) return;
        var detail = CharacterCardUtil.GetCardDetail(_unitData.DataID);
        var localCsvData = _gameDataMgr.GetCsvData(GameDataMgr.DataId.CardUpgradeData);
        int key = _unitData.Grade * 1000 + detail.Level + 1;
        CardUpgradeData localData = null;
        if (localCsvData.ContainsKey(key))
        {
            localData = localCsvData[key][0].ToClass<CardUpgradeData>();
        }

        //Gold
        int currentGold = LobbyGoodsUtil.GetGoods(GoodsID.GOLD);

        _upgradeGoldText.text = localData != null ? $"{localData.NeedGold:#,###}" : "MAX";
        if (localData == null || detail.CardCount - localData.NeedCard < 0)
        {
            _upgradeGoldText.color = Color.gray;
            _upgradeButton.enabled = false;
            _upgradeBlockImage.SetActive(true);
        }
        else if (currentGold - localData.NeedGold < 0)
        {
            _upgradeGoldText.color = Color.red;
            _upgradeButton.enabled = false;
            _upgradeBlockImage.SetActive(true);
        }
        else
        {
            _upgradeGoldText.color = Color.white;
            _upgradeButton.enabled = true;
            _upgradeBlockImage.SetActive(false);
        }

        _currentGold.text = $"{currentGold:#,###}";
        
        //GaugeBar
        _expText.text = (localData != null) ? 
            $"{detail.CardCount}/{localData.NeedCard}" : "MAX";
        
        float gaugePercent = (localData != null) ? Mathf.Min(1, (float) detail.CardCount / localData.NeedCard) : 1;
        _expBarImage.transform.localScale = new Vector3(gaugePercent, 1, 1);
        if(_expFullGaugeObject != null) _expFullGaugeObject.SetActive(gaugePercent >= 1);

    }

    public void SetData(CharacterUnitData data)
    {
        //Get Card Detail Data
        if(_gameIllustMgr == null) _gameIllustMgr = EightUtil.GetCore<GameIllustMgr>();
        if (_unitData == null || _unitData.DataID != data.DataID || _illustrationImage.sprite == null)
        {
            _gameIllustMgr.GetIllustSprite(data.DataID, (sprite) =>
            {
                _illustrationImage.sprite = sprite;
                _illustrationImage.color = new Color(1, 1, 1, 0);
                _illustrationImage.gameObject.SetActive(true);
            });
        }

        //Is Favor Card Data?
        if (_battleInfo == null)
        {
            BattleInfoServerConnector.GetBattleInfo((info) => { _battleInfo = info; });
        }
        _favorEnableObject.SetActive(data.DataID == _battleInfo.FavorCharId);

        _unitData = data;
        int grade = data.Grade;
        var cardDetailData = CharacterCardUtil.GetCardDetail(data.DataID);
        _isLocked = cardDetailData == null;
        int level = _level = cardDetailData?.Level ?? 1;
        _cardLevel.text = level.ToString();
        _cardRankImage.sprite = LobbyCardUtil.GetFrameSprite(data.Grade - 1);
        _cardName.text = data.UnitName;
        _cardGrade.text = LobbyCardUtil.GetCardGradeText(grade - 1);
        _cardGrade.color = LobbyCardUtil.GetCardColor(grade - 1);
        _cardDescription.text = data.Description;
        _skillDescription.text = "[ " + data.SkillDescription + " ]";
        
        float attSpeed = (float) (Math.Truncate(_unitData.AttackSpeed.GetStat(level, 0, 0) * 100) / 100);

        _damageText.text = ((int)_unitData.Combat.GetStat(level, 0, 0)).ToString();
        _attSpeedText.text = $"{attSpeed:G}s";
        {
            TargetType type = TargetType.Null;
            if (_unitData.SkillTriggerData.ContainsKey(UnitTriggerType.Attack))
            {
                type = _unitData.SkillTriggerData[UnitTriggerType.Attack].SkillTargetType;
            }
            _criticalPerText.text = GetTypeStr(type);
        }
        //_rangeText.text = data.
        //_maxUpgradeText.text = data.

        var _gimmick = new CharacterGimmickInformation[3];
        _gimmick[0] = _unitData.Gimmick1;
        _gimmick[1] = _unitData.Gimmick2;
        _gimmick[2] = _unitData.Gimmick3;

        for (int i = 0; i < _etcTexts.Length; ++i)
        {
            var g_data = _gimmick[i];
            var etcObject = _etcTexts[i];

            if (g_data == null || string.IsNullOrEmpty(g_data.Name))
            {
                etcObject.SlotObject.SetActive(false);
                continue;
            }

            etcObject.SlotObject.SetActive(true);
            etcObject.Title.text = g_data.Name;
            etcObject.Value.text = string.Format(g_data.Format, g_data.Value + level * g_data.PlusLevel);
            etcObject.Icon.sprite = g_data.Icon;
        }

        _storyTitle.text ="\"" + _unitData.StoryTitle + "\"";
        _storyDescription.text = _unitData.StoryDescription;

        var mat = _isLocked ? _grayMaterial : null;
        _illustrationImage.material = mat;
        _backgroundImage.material = mat;
        
    }

    public void LevelPreview(bool isActive)
    {
        if (!isActive)
        {
            float attSpeed = (float) (Math.Truncate(_unitData.AttackSpeed.GetStat(_level, 0, 0) * 100) / 100);

            _damageText.text = ((int)_unitData.Combat.GetStat(_level, 0, 0)).ToString();
            _attSpeedText.text = $"{attSpeed:G}s";
            return;
        }
        
        float plusDamage = _unitData.Combat.GetStat(_level + 1, 0, 0) - _unitData.Combat.GetStat(_level, 0, 0);
        float plusSpeed = _unitData.AttackSpeed.GetStat(_level + 1, 0, 0) - _unitData.AttackSpeed.GetStat(_level, 0, 0);
        
        plusDamage = (float) (Math.Truncate(plusDamage * 100) / 100);
        plusSpeed = (float) (Math.Truncate(plusSpeed * 100) / 100);
        
        if(plusDamage != 0) _damageText.text = $"<color=#9d5dfeff>{plusDamage}{(plusDamage > 0 ? "+":"-")}</color> {_unitData.Combat.GetStat(_level, 0, 0):G}";
        if(plusSpeed != 0) _attSpeedText.text = $"<color=#9d5dfeff>{Math.Abs(plusSpeed):G}{(plusSpeed > 0 ? "+":"-")}</color> {_unitData.AttackSpeed.GetStat(_level, 0, 0):G}s";
        
        var _gimmick = new CharacterGimmickInformation[3];
        _gimmick[0] = _unitData.Gimmick1;
        _gimmick[1] = _unitData.Gimmick2;
        _gimmick[2] = _unitData.Gimmick3;

        for (int i = 0; i < _etcTexts.Length; ++i)
        {
            var g_data = _gimmick[i];
            var etcObject = _etcTexts[i];

            if (g_data == null || string.IsNullOrEmpty(g_data.Name))
            {
                continue;
            }
            
            string plusValue = string.Format(g_data.Format, Math.Abs(g_data.PlusLevel));
            string valueText = string.Format(g_data.Format, g_data.Value + _level * g_data.PlusLevel);

            if (g_data.PlusLevel == 0)
            {
                etcObject.Value.text = $"{valueText}";
                continue;
            }
            etcObject.Value.text = $"<color=#9d5dfeff>{plusValue}{(g_data.PlusLevel >= 0 ? "+" : "-")}</color> {valueText}";
        }
    }
    
    public void ReinforcePreview(bool isActive)
    {
        if (!isActive)
        {
            float attSpeed = (float) (Math.Truncate(_unitData.AttackSpeed.GetStat(_level, 0, 0) * 100) / 100);
           
            _damageText.text = ((int)_unitData.Combat.GetStat(_level, 0, 0)).ToString();
            _attSpeedText.text = $"{attSpeed:G}s";
            return;
        }
        
        float plusDamage = _unitData.Combat.GetStat(_level, 1, 0) - _unitData.Combat.GetStat(_level, 0, 0);
        float plusSpeed = _unitData.AttackSpeed.GetStat(_level, 1, 0) - _unitData.AttackSpeed.GetStat(_level, 0, 0);
        
        plusDamage = (float) (Math.Truncate(plusDamage * 100) / 100);
        plusSpeed = (float) (Math.Truncate(plusSpeed * 100) / 100);
        
        if(plusDamage != 0) _damageText.text = $"<color=#ffcd00ff>{plusDamage:G}{(plusDamage > 0 ? "+":"-")}</color> {_unitData.Combat.GetStat(_level, 0, 0):G}";
        if(plusSpeed != 0) _attSpeedText.text = $"<color=#ffcd00ff>{Math.Abs(plusSpeed):G}{(plusSpeed > 0 ? "+":"-")}</color> {_unitData.AttackSpeed.GetStat(_level, 0, 0):G}s";
        
        var _gimmick = new CharacterGimmickInformation[3];
        _gimmick[0] = _unitData.Gimmick1;
        _gimmick[1] = _unitData.Gimmick2;
        _gimmick[2] = _unitData.Gimmick3;

        for (int i = 0; i < _etcTexts.Length; ++i)
        {
            var g_data = _gimmick[i];
            var etcObject = _etcTexts[i];

            if (g_data == null || string.IsNullOrEmpty(g_data.Name))
            {
                continue;
            }
            string plusValue = string.Format(g_data.Format, Math.Abs(g_data.PlusReinforce));
            string valueText = string.Format(g_data.Format, g_data.Value + _level * g_data.PlusLevel);
            
            if (g_data.PlusReinforce == 0)
            {
                etcObject.Value.text = $"{valueText}";
                continue;
            }
            
            etcObject.Value.text = $"<color=#ffcd00ff>{plusValue}{(g_data.PlusReinforce >= 0 ? "+" : "-")}</color> {valueText}";
        }
    }
    
    public void RefreshData()
    {
        SetData(_unitData);
        SetDetailData();
    }

    public void OnPopupSettingDeck()
    {
        LobbyOriganizationEditState.SetCharacterUnitData(_unitData);
        LobbySceneInterface.InvokeChangingState(LobbyFSMSystem.LobbyFSMState.ORGANIZATION_EDIT);
    }

    public void OnUpgradeCard()
    {
        if (!LobbyCharacterCardServerConnector.IsLoaded) return;
        LobbyCharacterCardServerConnector.UpgradeCharacter(_unitData.DataID, 
            () =>
            {
                LobbyRefreshUtil.OnRefresh(RefreshState.Cards);
                LobbyGoodsServerConnector.GetAllGoods(() =>
                {
                    BattleInfoServerConnector.GetBattleInfo((battleInfo) =>
                    {
                        LobbyRefreshUtil.OnRefresh(RefreshState.Goods);
                        _cardLevelAnimator.enabled = true;
                        _cardLevelAnimator.Rebind();
                    }, true);
                    // _effectObject.SetActive(true);
                    // var particleSystem = _effectObject.GetComponent<ParticleSystem>();
                    // particleSystem.Clear();
                    // particleSystem.Play();
                    
                });
            }, 
            (error) =>
            {
                string errorContext = "";
                switch (error)
                {
                    case LobbyCharacterCardServerConnector.ErrorCode.ERROR_IN_CLIENT:
                        errorContext = "존재하지 않는 카드입니다.";
                        break;
                    
                    case LobbyCharacterCardServerConnector.ErrorCode.NOT_ENOUGH_GOLD:
                        errorContext = "골드가 부족합니다.";
                        break;
                    case LobbyCharacterCardServerConnector.ErrorCode.NOT_ENOUGH_CONDITION:
                        errorContext = "카드가 부족합니다.";
                        break;
                }
                
                GamePopupMgr.OpenNormalPopup("승급 실패", errorContext);
            });
    }

    public void SetFavorCard()
    {
        GamePopupMgr.OpenLoadingPopup(true);
        BattleInfoServerConnector.GetBattleInfo((info) =>
        {
            if (info.FavorCharId == _unitData.DataID)
            {
                GamePopupMgr.CloseLoadingPopup();
                return;
            }

            info.FavorCharId = _battleInfo.FavorCharId = _unitData.DataID;
            BattleInfoServerConnector.UpdateBattleInfo(() =>
            {
                GamePopupMgr.CloseLoadingPopup();
                _favorEnableObject.SetActive(_unitData.DataID == _battleInfo.FavorCharId);
            });
        }, true);
    }

    private string GetTypeStr(TargetType type)
    {
        switch (type)
        {
            case TargetType.Myself:
                return "-";
            case TargetType.RoadFirstUnit:
                return "앞의 적";
            case TargetType.RoadRandomUnit:
                return "무작위 적";
            case TargetType.RoadStrongUnit:
                return "강한 적";
            case TargetType.RoadWeakUnit:
                return "약한 적";
            default:
                return "-";
        }
    }

    public void RefreshContentFitter()
    {
        for(int i = 0; i < _fitters.Length; ++i)
        {
            var fitter = _fitters[i];
            fitter.enabled = false;
            fitter.enabled = true;
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform) fitter.transform);
        }
    }
    
    [Serializable]
    private class EtcValueClass
    {
        public Text Title;
        public Text Value;
        public Image Icon;
        public GameObject SlotObject;
    }
}
