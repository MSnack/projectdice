﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class LobbyMembershipUI : MonoBehaviour
{

    [Header("Top UI")]
    [SerializeField]
    private Image _crownImage = null;

    [SerializeField]
    private Image _currentTicketImage = null;

    [SerializeField]
    private Text _remainTimeText = null;

    [SerializeField]
    private Text _currentLevelText = null;

    [SerializeField]
    private Text _currentExpBarText = null;

    [SerializeField]
    private Transform _currentExpBarTransform = null;

    [SerializeField]
    private Button _upgradeButton = null;

    [SerializeField]
    private Sprite _goldUpgradedButtonSprite = null;

    [Header("Bottom UI")]
    [SerializeField]
    private GameObject _contentObject = null;

    [SerializeField]
    private Button _receiveAllButton = null;

    [Header("Etc")]
    [SerializeField]
    private LobbyMembershipState _membershipState = null;
    [SerializeField]
    private Sprite _crownEnable = null;

    [SerializeField]
    private Sprite _crownDisable = null;

    [SerializeField]
    private Sprite _silverTicket = null;

    [SerializeField]
    private Sprite _goldTicket = null;

    private DateTime _endDate = DateTime.Today.AddMonths(1).AddDays(-DateTime.Today.Day + 2);

    // Start is called before the first frame update
    void Start()
    {
        LobbyRefreshUtil.SetRefreshEvent(RefreshState.Quest, ReloadUI);
        Init();
    }

    private void Init()
    {
        GameBattleInfo battleInfo = null;
        BattleInfoServerConnector.GetBattleInfo((received) => { battleInfo = received; });
        
        var infos = QuestUtil.GetQuestData(QuestCategory.MembershipQuest);
        bool isUpgraded = _membershipState.IsUpgraded;

        _crownImage.sprite = isUpgraded ? _crownEnable : _crownDisable;
        _currentTicketImage.sprite = isUpgraded ? _goldTicket : _silverTicket;
        float levelRaw = battleInfo.Level;
        
        QuestLocalData targetLocalData = null;
        int level = infos[infos.Count / 2 - 1].Value;

        int size = infos.Count / 2;
        for (int i = 0; i < size; ++i)
        {
            var questInfos = new[] { infos[i], infos[i + size] };
            var nextQuestLevel = (i + 1 < size) ? QuestUtil.GetQuestLocalData(infos[i + 1].QuestId).TargetValue : -1;
            var targetLevel = QuestUtil.GetQuestLocalData(questInfos[0].QuestId).TargetValue;
            if (level >= targetLevel)
            {
                if (level >= nextQuestLevel) continue;
                targetLocalData = null;
                continue;
            }
            targetLocalData = QuestUtil.GetQuestLocalData(questInfos[0].QuestId);
            break;
        }

        _currentLevelText.text = targetLocalData?.Title ?? "--";
        float exp = (float)level / targetLocalData?.TargetValue ?? -1;
        _currentExpBarText.text = exp < 0 ? "패스 완료" : $"{level:D}/{(targetLocalData?.TargetValue ?? -1):D}";
        _currentExpBarTransform.localScale = new Vector3(exp < 0 ? 1 : exp, 1, 1);

        if (isUpgraded)
        {
            _upgradeButton.enabled = false;
            _upgradeButton.GetComponent<Image>().sprite = _goldUpgradedButtonSprite;
            _upgradeButton.GetComponentInChildren<Text>().text = "다이아 패스";
        }
        
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform) _contentObject.transform);

    }

    // Update is called once per frame
    void Update()
    {
        TimeSpan t = _endDate - DateTime.Now;
        
        if (t.TotalSeconds < 0)
        {
            _endDate =  DateTime.Today.AddMonths(1).AddDays(-DateTime.Today.Day + 2);
            return;
        }
        
        _remainTimeText.text = (t.Days > 0 ? $"{t.Days}일 " : "")
                             + (t.Hours > 0 ? $"{t.Hours}시간 " : "")
                             + (t.Minutes > 0 ? $"{t.Minutes}분 " : "")
                             + (t.Hours <= 0 ? $"{t.Seconds}초 " : "");
    }

    public void ReloadUI()
    {
        _membershipState.Reload();
        Init();
    }

    public void OnPurchaseComplete(Product product)
    {
        //나중에 ios도 적용하도록 객체화 필요
        GamePopupMgr.OpenLoadingPopup();
        NormalPopup.Close();
        Debug.Log(product.receipt);
        var receiptJsonStr = JObject.Parse(product.receipt)["Payload"]?.ToString() ?? "";
        receiptJsonStr = JObject.Parse(receiptJsonStr).SelectToken("json")?.ToString() ?? "";
        var receiptJson = string.IsNullOrEmpty(receiptJsonStr) 
            ? new JObject() : JObject.Parse(receiptJsonStr);

        string purchaseToken = receiptJson.SelectToken("purchaseToken")?.ToString() ?? "";
        string packageName = receiptJson.SelectToken("packageName")?.ToString() ?? Application.identifier;

        PurchaseInfo purchaseInfo = new PurchaseInfo
        {
            PurchaseId = product.definition.id,
            PurchaseToken = purchaseToken,
            PackageName = packageName,
            Date = DateTime.Now,
        };

        LobbyShopServerConnector.BuyShopPackage(100001, purchaseInfo, (mailId) =>
            {
                LobbyMailServerConnector.ReceiveMailItem(mailId[0],
                    (trans) =>
                    {
                        MailSlotObject.ReceivedMailEvent(trans, ItemCategory.QUEST_CONDITION);
                        
                        LobbyMailServerConnector.GetMails(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Mails));
                        // LobbyGoodsServerConnector.GetAllGoods(LobbyRefreshUtil.OnRefreshEvent(RefreshState.Goods));
                        GamePopupMgr.OpenNormalPopup("멤버십 구매 성공", "이번달 멤버십 해택이 적용되었습니다!");
                        GamePopupMgr.CloseLoadingPopup();
                    });
            },
            (error) =>
            {
                string errorStr = "";

                switch (error)
                {
                    case LobbyShopServerConnector.ErrorCode.UNKNOWN_PACKAGE_ID:
                        errorStr = "앱 업데이트를 해야 정상적인 구매가 가능합니다.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.UNKNOWN_RECEIPT:
                        errorStr = "결제 진행에 문제가 생겼습니다.\n1:1 문의를 해주세요.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.VERIFY_RECEIPT_FAILED:
                        errorStr = "정상적인 결제방식이 아닙니다.\n1:1 문의를 해주세요.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.UNKOWN_PURCHASE_STATE:
                        errorStr = "정상적으로 결제되지 않았습니다.\n1:1 문의를 해주세요.";
                        break;
                    case LobbyShopServerConnector.ErrorCode.HAS_ALREADY_BEEN_LIMITED_TO_PURCHASE:
                        errorStr = "이미 구매를 했기 때문에 결제가 취소되었습니다.\n인앱 결제 시 환불처리를 해주세요.";
                        break;
                }

                GamePopupMgr.OpenNormalPopup("구매 실패", errorStr);
                GamePopupMgr.CloseLoadingPopup();
            });
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
    {
        GamePopupMgr.CloseLoadingPopup();
    }
}
