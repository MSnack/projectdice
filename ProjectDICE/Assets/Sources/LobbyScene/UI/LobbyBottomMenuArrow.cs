﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyBottomMenuArrow : MonoBehaviour
{
    [SerializeField]
    private GameObject menuUi = null;

    private LobbyBottomUIButton[] _buttons;
    
    [SerializeField]
    private AnimationCurve _animationCurve = AnimationCurve.Linear(0, 0, 1, 1);

    [SerializeField]
    private float _totalTime = 0.2f;
    
    private Coroutine _currCoroutine = null;
    
    // Start is called before the first frame update
    void Start()
    {
        LobbySceneInterface.OnChangingState += OnChangingState;
        if (menuUi != null) _buttons = menuUi.GetComponentsInChildren<LobbyBottomUIButton>(true);
        
        foreach (var button in _buttons)
        {
            if (button.State == LobbyFSMSystem.LobbyFSMState.BATTLE)
            {
                transform.position = button.transform.position;
                break;
            }
        }
    }

    private void OnChangingState(LobbyFSMSystem.LobbyFSMState state)
    {
        foreach (var button in _buttons)
        {
            if (button.State == state)
            {
                if(_currCoroutine != null) StopCoroutine(_currCoroutine);
                _currCoroutine = StartCoroutine(OnAnimation(button.transform));
                break;
            }
        }
    }
    
    private IEnumerator OnAnimation(Transform targetTransform)
    {
        float elapsedTime = 0;
        Vector3 startPos = transform.position;

        for (elapsedTime = 0; elapsedTime < _totalTime; elapsedTime += Time.deltaTime)
        {
            float v = _animationCurve.Evaluate(elapsedTime / _totalTime);
            transform.position = Vector3.Lerp(startPos, targetTransform.position, v);
            yield return null;
        }

        transform.position = targetTransform.position;
        _currCoroutine = null;
        yield break;
    }
    
}
