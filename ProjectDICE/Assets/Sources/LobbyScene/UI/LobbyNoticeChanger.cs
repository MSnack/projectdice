﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyNoticeChanger : MonoBehaviour
{
    [SerializeField]
    private LobbyNoticeUtil.NoticeState state;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ChangeNoticeState(bool isActive)
    {
        LobbyNoticeUtil.OnNotice(state, isActive);
    }
}
