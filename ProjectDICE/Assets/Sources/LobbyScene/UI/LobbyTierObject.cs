﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbyTierObject : MonoBehaviour
{
    [SerializeField]
    private Image _image = null;

    [SerializeField]
    private TierState _tierState = TierState.Unknown;

    [SerializeField]
    private Transform _effectParent = null;

    [SerializeField]
    private int effectOrderLayer = -1;

    private GameEffectObject _effect = null;
    private GameEffectMgr _effectMgr = null;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Reset()
    {
        _image = GetComponent<Image>();
    }

    public void InitTier(TierState state)
    {
        if (_effect != null)
        {
            ReleaseEffect();
        }
        
        _tierState = state;
        _image.sprite = TierUtil.GetTierSprite(state);

        if (_effectParent == null) return;
        if(_effectMgr == null) _effectMgr = EightUtil.GetCore<GameEffectMgr>();
        var effectData = TierUtil.GetTierEffect(state);
        if (effectData == null) return;
        _effect = _effectMgr.UseEffect(effectData);
        var order = _effect.GetComponent<GameEffectObject>();
        if (effectOrderLayer < 0) effectOrderLayer = order.SortingOrder;
        order.SortingOrder = effectOrderLayer;
        order.UpdateOrderInLayer();
        Vector3 pos = _effect.transform.localPosition;
        Vector3 scale = _effect.transform.localScale;
        Quaternion rot = _effect.transform.rotation;
        
        _effect.transform.SetParent(_effectParent);
        _effect.transform.localPosition = pos;
        _effect.transform.localScale = scale;
        _effect.transform.rotation = rot;
    }

    private void ReleaseEffect()
    {
        if (_effectParent == null) return;
        if(_effectMgr == null) _effectMgr = EightUtil.GetCore<GameEffectMgr>();
        Vector3 pos = _effect.transform.localPosition;
        Vector3 scale = _effect.transform.localScale;
        Quaternion rot = _effect.transform.rotation;
        _effectMgr?.ReturnEffect(_effect);
        _effect.transform.localPosition = pos;
        _effect.transform.localScale = scale;
        _effect.transform.rotation = rot;
        _effect.ResetOrderInLayer();
        _effect = null;
    }
    private void OnDestroy()
    {
        if (_effect != null)
        {
            ReleaseEffect();
        }
    }
}
