﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovementScale : MonoBehaviour
{

    private Vector3 _startPosition;
    private Vector3 _targetStartPosition;

    [SerializeField]
    private Transform _targetObject;
    
    [SerializeField]
    private Vector3 _movementScale = Vector3.one;
    
    // Start is called before the first frame update
    void Start()
    {
        _startPosition = transform.localPosition;
        _targetStartPosition = _targetObject.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetMovement = _targetObject.localPosition - _targetStartPosition;
        transform.localPosition = _startPosition + Vector3.Scale(targetMovement, _movementScale);
    }
}
