﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyWebViewObject : MonoBehaviour
{
    [SerializeField]
    private GameObject _webViewGameObject = null;
    [SerializeField]
    private WebViewObject _webViewObject = null;
    
    private bool _noticeClosed = false;

    public void Close()
    {
        _noticeClosed = true;
        _webViewObject.SetVisibility(false);
        _webViewGameObject.SetActive(false);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void StartWebView(string url)
    {
        var strUrl = url;
        if (_webViewObject == null)
        {
            _webViewObject = _webViewGameObject.AddComponent<WebViewObject>();
            _webViewObject.Init(msg =>
            {
                //load
                Debug.Log(string.Format("{0}", msg));
            });
        }
        
        _webViewGameObject.SetActive(true);
        RectTransform rt = (RectTransform) _webViewGameObject.transform;
        _webViewObject.LoadURL(strUrl);
        _webViewObject.SetVisibility(true);
        Rect newSafeArea = Screen.safeArea;
        Vector2 _anchorMin = newSafeArea.position;
        Vector2 _anchorMax = newSafeArea.position + newSafeArea.size;

        _anchorMin.x /= Screen.width;
        _anchorMax.x /= Screen.width;
        _anchorMin.y /= Screen.height;
        _anchorMax.y /= Screen.height;

        _webViewObject.SetMargins(
            (int) (_anchorMin.x * Screen.width) + 30,
            (int) ((1 - _anchorMax.y) * Screen.height) + 150,
            (int) ((1 - _anchorMax.x) * Screen.width) + 30,
            (int) (_anchorMin.y * Screen.height) + 30);
    }
}
