﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class TipDataLoader : MonoBehaviour
{
    [SerializeField]
    private Text _descriptionText = null;

    [SerializeField]
    private Text _prevText = null;

    [SerializeField]
    private GameDataMgr.DataId _dataId = GameDataMgr.DataId.MatchingTipData;

    private Dictionary<int, List<GameIndexIDData>> _tipData = null;

    [SerializeField]
    private string _format = "{0}";

    private void OnEnable()
    {
        GenerateTip();
    }

    private void GenerateTip()
    {
        if(_prevText != null) _prevText.text = _descriptionText.text;
        if(_tipData == null)
            _tipData = EightUtil.GetCore<GameDataMgr>()
                .GetCsvData(_dataId);
        int randIndex = Random.Range(0, _tipData.Count);
        try
        {
            string tipStr = "";
            if(_dataId == GameDataMgr.DataId.MatchingTipData)
                tipStr = _tipData[randIndex][0].ToClass<MatchingTipData>().Description;
            if(_dataId == GameDataMgr.DataId.LoadingTipData)
                tipStr = _tipData[randIndex][0].ToClass<LoadingTipData>().Description;

            _descriptionText.text = string.Format(_format, tipStr);
        }
        catch (Exception e)
        {
            GenerateTip();
        }

    }
}
