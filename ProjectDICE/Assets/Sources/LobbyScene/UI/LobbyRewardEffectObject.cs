﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using EightWork.UI;
using UnityEngine;
using Random = UnityEngine.Random;

public class LobbyRewardEffectObject : MonoBehaviour
{

    [SerializeField]
    private GameObject _rewardEffectPrefab = null;

    [SerializeField]
    private Transform _startPosition = null;
    public Transform StartPosition
    {
        get => _startPosition;
        set => _startPosition = value;
    }

    [SerializeField]
    private Transform _targetPosition = null;
    public Transform TargetPosition
    {
        get => _targetPosition;
        set => _targetPosition = value;
    }

    [SerializeField]
    private Vector2 _range = new Vector2(-30, 30);

    private TweenPosition _tweenPositions = null;
    private GameObject _effectCloneObject = null;
    private Canvas _canvas = null;


    // Start is called before the first frame update
    void Start()
    {
        if (_effectCloneObject == null) _effectCloneObject = Instantiate(_rewardEffectPrefab, transform);
        _tweenPositions = _effectCloneObject.GetComponent<TweenPosition>();

        _canvas = GetComponentInParent<Canvas>();
    }

    //LobbyFSMSystem -> FSMInterface에서 참조
    public void PlayEffect()
    {
        _effectCloneObject.SetActive(false);
        try
        {
            var startPosition = WorldToUISpace(_canvas, _startPosition.position);
            var realPosition = startPosition + new Vector3(Random.Range(_range.x, _range.y), Random.Range(_range.x, _range.y));
            _tweenPositions.From = realPosition;
            _tweenPositions.To = WorldToUISpace(_canvas, _targetPosition.position);
            _effectCloneObject.SetActive(true);
        }
        catch (Exception e) {}

    }
    
    private Vector3 WorldToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
        Vector3 screenPos = parentCanvas.worldCamera.WorldToScreenPoint(worldPos);
        Vector2 movePos;

        //Convert the screenpoint to ui rectangle local point
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        return movePos;
    }
}
