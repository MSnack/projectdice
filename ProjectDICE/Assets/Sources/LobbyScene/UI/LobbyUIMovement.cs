﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

public class LobbyUIMovement : MonoBehaviour
{
    private static UnityAction<Vector3> _onMove = null;

    [SerializeField]
    private Transform[] _lobbyTransform;

    [SerializeField]
    private GameObject _blockTouchObject = null;
    
    [Header("Move Animation Settings")]
    [SerializeField]
    private AnimationCurve _animationCurve = AnimationCurve.Linear(0, 0, 1, 1);
    [SerializeField]
    private float _totalTime = 0.2f;

    private Coroutine _currCoroutine = null;

    private LobbyUITouchSlider _touchSlider = null;
    private bool _isMoving = false;

    public bool IsMoving => (_touchSlider?.IsDragging ?? true) || _isMoving;

    private void Awake()
    {
        _touchSlider = GetComponent<LobbyUITouchSlider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _onMove = null;
        _onMove += SetMove;
    }

    public static void OnMove(Vector3 targetPosition)
    {
        _onMove.Invoke(targetPosition);
    }

    private void SetMove(Vector3 target)
    {
        if(_currCoroutine != null) StopCoroutine(_currCoroutine);
        _currCoroutine = StartCoroutine(OnAnimation(target));
    }

    private IEnumerator OnAnimation(Vector3 target)
    {
        _isMoving = true;
        float elapsedTime = 0;
        Vector3[] current = new Vector3[_lobbyTransform.Length];
        for (int i = 0; i < current.Length; ++i)
        {
            current[i] = _lobbyTransform[i].localPosition;
        }
        if(_blockTouchObject != null) _blockTouchObject.SetActive(true);
        
        for (elapsedTime = 0; elapsedTime < _totalTime; elapsedTime += Time.deltaTime)
        {
            float v = _animationCurve.Evaluate(elapsedTime / _totalTime);
            for (int i = 0; i < current.Length; ++i)
            {
                _lobbyTransform[i].localPosition = Vector3.Lerp(current[i], target, v);
            }
            yield return null;
        }

        _isMoving = false;
        for (int i = 0; i < current.Length; ++i)
        {
            _lobbyTransform[i].localPosition = target;
        }
        if(_blockTouchObject != null) _blockTouchObject.SetActive(false);
        _currCoroutine = null;
    }
    
}
