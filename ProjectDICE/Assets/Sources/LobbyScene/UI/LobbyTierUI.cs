﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class LobbyTierUI : MonoBehaviour
{
    [SerializeField]
    private Text _bubbleText = null;

    [SerializeField]
    private Text _currentTierTrophyText = null;
    
    [SerializeField]
    private AnimationCurve _curve;

    [SerializeField]
    private float _totalTime = 1.0f;

    [Header("Tier")]
    [SerializeField]
    private Text[] _tierText = null;

    [SerializeField]
    private LobbyTierObject _currTier = null;
    private Coroutine _animCoroutine = null;
    private int _trophyValue;
    
    private TierState _currState = TierState.Unknown;
    private TierState _prevState = TierState.Unknown;
    private TierState _nextState = TierState.Unknown;

    [SerializeField]
    private Transform _tierBarBorderListTransform = null;

    [SerializeField]
    private GameObject _tierBarBorderObject = null;

    [SerializeField]
    private float _tierBarBorderSpace = 1;

    private List<GameObject> _tierBarBorderList = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
    }

    private void OnEnable()
    {
        if (_animCoroutine != null)
        {
            StopCoroutine(_animCoroutine);
            _animCoroutine = null;
        }
        _animCoroutine = StartCoroutine(TierBarAnimation());
    }

    private void OnDisable()
    {
    }

    private IEnumerator TierBarAnimation()
    {
        yield return new WaitUntil(() => LobbyGoodsServerConnector.IsLoaded);

        float elapsedTime = 0;
        _trophyValue = LobbyGoodsUtil.GetGoods(GoodsID.TROPHY);
        
        _prevState = TierUtil.GetPrevTierState(_trophyValue);
        _currState = TierUtil.GetTierState(_trophyValue);
        _nextState = TierUtil.GetNextTierState(_trophyValue);

        _currTier.InitTier(_currState);

        var currTierData = TierUtil.GetTierLocalData(_currState);
        foreach (var text in _tierText)
        {
            text.text = currTierData.Name;
        }
        float minTrophyValue = currTierData.NeedTrophy;
        float maxTrophyValue = TierUtil.GetTierLocalData(_nextState).NeedTrophy;
        
        _currentTierTrophyText.text = ((int)minTrophyValue).ToString();
        InitTierBarBorderList(_tierBarBorderSpace);
        _tierBarBorderListTransform.localPosition = new Vector3(-(_trophyValue * _tierBarBorderSpace), 0, 0);

        // float rawValue = ((_trophyValue - minTrophyValue) / (maxTrophyValue - minTrophyValue));
        // for (; elapsedTime <= _totalTime; elapsedTime += Time.deltaTime)
        // {
        //     float t = elapsedTime / _totalTime;
        //     float v = _curve.Evaluate(t) * (rawValue * 0.5f + 0.5f);
        //     
        //     _bubbleText.text = ((int)(_curve.Evaluate(t) * _trophyValue)).ToString();
        //     
        //     yield return null;
        // }
        //
        // float resultValue = (rawValue * 0.5f) + 0.5f;
        _bubbleText.text = _trophyValue.ToString();

    }

    private void InitTierBarBorderList(float space = 1)
    {
        if (_tierBarBorderList.Count > 0) return;

        _tierBarBorderListTransform.localPosition = Vector3.zero;
        int size = TierUtil.TierSize();
        TierState endState = TierUtil.GetTierStateByIndex(size - 1);
        int length = TierUtil.GetTierLocalData(endState)?.NeedTrophy ?? 0;
        for (int i = 0; i < length; i += 200)
        {
            var borderObject = Instantiate(_tierBarBorderObject, _tierBarBorderListTransform);
            borderObject.transform.localPosition = new Vector3(i * space, 0, 0);
            borderObject.GetComponentInChildren<Text>().text = i.ToString();
            _tierBarBorderList.Add(borderObject);
        }
    }
}
