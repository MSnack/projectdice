﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LobbyBoxDetailPopupUI : LobbyOtherPopupBase
{
    private static UnityAction<int, int, UnityAction, UnityAction> _onOpenUI = null;
    private static UnityAction<int, int, UnityAction, DateTime> _onRemoveTimeOpenUI = null;
    private static UnityAction _onCloseUI = null;
    
    [SerializeField]
    private BoxDetailRankInfo _rankInfoPrefab = null;

    [SerializeField]
    private GameObject _boxTransformObject = null;
    private GameObject _boxPrefab = null;

    [SerializeField]
    private int _boxSortingOrder = 6610;

    [SerializeField]
    private GameObject _infoBaseObject = null;

    private List<BoxDetailRankInfo> _rankInfoList = new List<BoxDetailRankInfo>();

    [SerializeField]
    private Text _boxPriceText = null;

    [SerializeField]
    private UIAlphaController _alphaController = null;

    private BoxLocalData _currentBoxLocalData = null;

    private UnityAction _currentSuccessEvent = null;
    private UnityAction _currentFailedEvent = null;

    private bool _isRemainTimeUI = false;
    private DateTime _endTime = DateTime.MinValue;

    [SerializeField]
    private Text _buttonCaption = null;

    [SerializeField]
    private Text _remainTimeText = null;

    [SerializeField]
    private string _buttonCaptionNoneState = "상자 구매";
    [SerializeField]
    private string _buttonCaptionRemainTimeState = "즉시 열기";

    public static void Open(int boxId, int price, UnityAction successEvent, UnityAction failedEvent)
    {
        _onOpenUI?.Invoke(boxId, price, successEvent, failedEvent);
    }

    public static void RemoveTimeUIOpen(int boxId, int price, UnityAction successEvent, DateTime endTime)
    {
        _onRemoveTimeOpenUI?.Invoke(boxId, price, successEvent, endTime);
    }

    public static void Close()
    {
        _onCloseUI?.Invoke();
    }

    private void InitRemoveTimeData(int boxId, int price, UnityAction successEvent, DateTime endTime)
    {
        _isRemainTimeUI = true;
        _endTime = endTime;
        _buttonCaption.text = _buttonCaptionRemainTimeState;
        InitData(boxId, price, successEvent, null);
    }
    
    private void InitData(int boxId, int price, UnityAction successEvent, UnityAction failedEvent)
    {
        _remainTimeText.text = "";
        ReleaseInfoList();
        var boxData = _currentBoxLocalData;
        if(boxData == null || boxData.DataID != boxId)
            boxData = _currentBoxLocalData = LobbyBoxUtil.GetBoxLocalData(boxId);

        _currentSuccessEvent = successEvent;
        _currentFailedEvent = failedEvent;

        if (_boxPrefab != null)
        {
            Destroy(_boxPrefab);
            _boxPrefab = null;
        }
        _boxPrefab = Instantiate(LobbyBoxUtil.GetBoxPrefab(boxId), _boxTransformObject.transform);
        var effectObj = _boxPrefab.GetComponent<GameEffectObject>();
        effectObj.SortingOrder = _boxSortingOrder;
        effectObj.UpdateOrderInLayerReverse();

        //Normal
        if (boxData.NKind > 0)
        {
            var rankInfo = Instantiate(_rankInfoPrefab, _infoBaseObject.transform);
            rankInfo.InitData(1, boxData.NKind, boxData.NCount);
            _rankInfoList.Add(rankInfo);
        }
        //Rare
        if (boxData.RKind > 0)
        {
            var rankInfo = Instantiate(_rankInfoPrefab, _infoBaseObject.transform);
            rankInfo.InitData(2, boxData.RKind, boxData.RCount);
            _rankInfoList.Add(rankInfo);
        }
        //S
        if (boxData.SKind > 0)
        {
            var rankInfo = Instantiate(_rankInfoPrefab, _infoBaseObject.transform);
            rankInfo.InitData(3, boxData.SKind, boxData.SCount);
            _rankInfoList.Add(rankInfo);
        }
        //Hero
        if (boxData.HKind > 0)
        {
            var rankInfo = Instantiate(_rankInfoPrefab, _infoBaseObject.transform);
            rankInfo.InitData(4, boxData.HKind, boxData.HCount);
            _rankInfoList.Add(rankInfo);
        }
        //Legend
        if (boxData.LKind > 0)
        {
            var rankInfo = Instantiate(_rankInfoPrefab, _infoBaseObject.transform);
            rankInfo.InitData(5, boxData.LKind, boxData.LCount, boxData.LPer);
            _rankInfoList.Add(rankInfo);
        }

        _boxPriceText.text = price.ToString();
        gameObject.SetActive(true);
    }

    public void CloseUI()
    {
        gameObject.SetActive(false);
        _currentFailedEvent?.Invoke();
    }

    public void BuyBox()
    {
        gameObject.SetActive(false);
        _currentSuccessEvent?.Invoke();
    }

    // Start is called before the first frame update
    public override void Init()
    {
        base.Init();

        _onOpenUI = null;
        _onCloseUI = null;
        _onRemoveTimeOpenUI = null;
        
        _onOpenUI += InitData;
        _onCloseUI += CloseUI;
        _onRemoveTimeOpenUI += InitRemoveTimeData;
    }

    private void ReleaseInfoList()
    {
        foreach (var rankInfo in _rankInfoList)
        {
            Destroy(rankInfo.gameObject);
        }
        _rankInfoList.Clear();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        _alphaController.Alpha = 0;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        _isRemainTimeUI = false;
        _buttonCaption.text = _buttonCaptionNoneState;
    }

    private void Update()
    {
        if (!_isRemainTimeUI) return;
        
        TimeSpan t = _endTime - DateTime.Now;
        _remainTimeText.text = ""
                              + (t.Hours > 0 ? $"{t.Hours}시간 " : "")
                              + (t.Minutes > 0 ? $"{t.Minutes}분 " : "")
                              + (t.Hours <= 0 ? $"{t.Seconds}초 " : "")
                              + "남음";
        
        int price = LobbyBoxUtil.GetBoxOpenTimeData((int) t.TotalSeconds).PriceStar;
        _boxPriceText.text = price.ToString();
        
        if (t.TotalSeconds <= 0)
        {
            CloseUI();
        }
    }
}
