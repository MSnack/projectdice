﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LobbyCharListGridLayoutGroup : GridLayoutGroup
{
    

    public override void CalculateLayoutInputHorizontal()
    {
        rectChildren.Reverse();
        base.CalculateLayoutInputHorizontal();
        rectChildren.Reverse();
    }

    public override void CalculateLayoutInputVertical()
    {
        rectChildren.Reverse();
        base.CalculateLayoutInputVertical();
        rectChildren.Reverse();
    }

}
