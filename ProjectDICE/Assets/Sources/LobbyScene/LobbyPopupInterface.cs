﻿using System.Collections;
using System.Collections.Generic;
using EightWork.MsgSystem;
using UnityEngine;
using UnityEngine.Events;

public class LobbyPopupInterface : EightMsgSystem
{

    private static UnityAction<LobbyPopupFSMSystem.LobbyPopupFSMState> _callChangingState = null;
    public static event UnityAction<LobbyPopupFSMSystem.LobbyPopupFSMState> CallChangingState
    {
        add
        {
            _callChangingState += value;
        }
        remove
        {
            _callChangingState -= value;
        }
    }
    
    /**
     * State 변경시 호출하는 함수
     */
    public static void InvokeChangingState(LobbyPopupFSMSystem.LobbyPopupFSMState state)
    {
        _callChangingState.Invoke(state);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        ReleaseActions();
    }

    private void ReleaseActions()
    {
        _callChangingState = null;
    }
}
