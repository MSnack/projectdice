﻿using System.Collections;
using System.Collections.Generic;
using EightWork.FSM;
using UnityEngine;

public abstract class LobbyStateBase : EightFSMStateBase<LobbyFSMSystem.LobbyFSMState, LobbyFSMSystem>
{
    [SerializeField]
    private GameObject[] _activeObjects;

    [SerializeField]
    private GameObject[] _inactiveObjects;

    [SerializeField]
    private GameObject _focusObject = null;

    [SerializeField]
    private bool _isBottomMenuState = false;

    public bool IsBottomMenuState => _isBottomMenuState;
    
    [SerializeField, Tooltip("메뉴의 서브 스테이트에 사용 (예시 : 이모티콘은 편성란에 있으므로 편성으로 설정)")]
    private LobbyFSMSystem.LobbyFSMState _subState = LobbyFSMSystem.LobbyFSMState.NONE;
    public LobbyFSMSystem.LobbyFSMState SubState => _subState;

    protected bool _isLoaded = false;
    public bool IsLoaded => _isLoaded;


    public override void StartState()
    {
        LobbySceneInterface.InvokeChangedState(State);
        RefreshObjects();
        
        if(_isBottomMenuState)
            LobbyUIMovement.OnMove(-_focusObject.transform.localPosition);
    }

    public override void EndState()
    {
        foreach (var obj in _activeObjects)
        {
            obj.SetActive(false);
        }

        foreach (var obj in _inactiveObjects)
        {
            obj.SetActive(true);
        }
    }

    public void RefreshObjects()
    {
        foreach (var obj in _activeObjects)
        {
            if(obj.activeInHierarchy == false) obj.SetActive(true);
        }
        
        foreach (var obj in _inactiveObjects)
        {
            if(obj.activeInHierarchy) obj.SetActive(false);
        }
    }
}
