﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

public class BattleInfoServerConnector : ServerConnectorBase
{

    private static UnityAction<UnityAction<GameBattleInfo>, bool> _onGetBattleInfo = null;
    private static UnityAction<UnityAction> _onUpdateBattleInfo = null;
    
    private static bool _isLoadedStatic = false;
    public static bool IsLoaded => _isLoadedStatic;

    private GameBattleInfo _currentBattleInfo = null;
    private GameUserMgr _userMgr = null;

    protected override void Awake()
    {
        _isLoadedStatic = false;
        base.Awake();
        
        _onGetBattleInfo = null;
        _onGetBattleInfo += GetBattleInfoFunc;

        _onUpdateBattleInfo = null;
        _onUpdateBattleInfo += UpdateBattleInfoFunc;
    }

    protected override void Init()
    {
        base.Init();

        if(_userMgr == null) _userMgr = EightUtil.GetCore<GameUserMgr>();
        GetBattleInfoFunc();
    }

    /**
     * forceUpdate 사용 시 resultFunction 내에서 GameBattleInfo를 모두 처리해야 함
     */
    public static void GetBattleInfo(UnityAction<GameBattleInfo> resultFunction = null, bool forceUpdate = false)
    {
        _onGetBattleInfo?.Invoke(resultFunction, forceUpdate);
    }

    public static void UpdateBattleInfo(UnityAction successEvent = null)
    {
        _onUpdateBattleInfo?.Invoke(successEvent);
    }

    private void GetBattleInfoFunc(UnityAction<GameBattleInfo> resultFunction = null, bool forceUpdate = false)
    {
        if (!forceUpdate && _currentBattleInfo != null)
        {
            resultFunction?.Invoke(_currentBattleInfo);
            return;
        }
        
        if(_userMgr == null) _userMgr = EightUtil.GetCore<GameUserMgr>();
        Translator.Request(GameWebRequestType.Get_UserBattleInfo,
            (trans) =>
            {
                _currentBattleInfo = trans.GetRequestData<GameBattleInfo>();
                _isLoaded = true;
                _isLoadedStatic = true;
                resultFunction?.Invoke(_currentBattleInfo);
            });
    }

    private void UpdateBattleInfoFunc(UnityAction successEvent = null)
    {
        if (_currentBattleInfo == null) return;
        Translator.SetSendData(_currentBattleInfo.ToJson());
        Translator.Request(GameWebRequestType.Update_UserBattle, (trans) =>
        {
            successEvent?.Invoke();
        });
    }
}
