﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class MailInfo
{
    private int _id;
    public int Id
    {
        get => _id;
        set => _id = value;
    }

    private int _type;
    public int Type
    {
        get => _type;
        set => _type = value;
    }

    private string _title;
    public string Title
    {
        get => _title;
        set => _title = value;
    }

    private string _description;
    public string Description
    {
        get => _description;
        set => _description = value;
    }

    private int _itemCategory;
    public int ItemCategory
    {
        get => _itemCategory;
        set => _itemCategory = value;
    }

    private int _itemId;
    public int ItemId
    {
        get => _itemId;
        set => _itemId = value;
    }

    private int _itemCount;
    public int ItemCount
    {
        get => _itemCount;
        set => _itemCount = value;
    }

    private DateTime _startTime;
    public string StartTime
    {
        set
        {
            if (value == null || value.Contains("0000-00-00 00:00:00"))
            {
                _startTime = DateTime.MinValue;
                return;
            }
            
            _startTime = Convert.ToDateTime(value);
        }
    }

    public DateTime GetStartTime => _startTime;

    private bool _isReceived;
    public bool IsReceived
    {
        get => _isReceived;
        set => _isReceived = value;
    }
}

public class LobbyMailServerConnector : ServerConnectorBase
{
    
    private GameUserInfo _userInfo = null;
    
    private static bool _isLoadedStatic = false;
    public static bool IsLoaded => _isLoadedStatic;

    private static UnityAction<UnityAction> _getMailInfos = null;
    private static UnityAction<int, UnityAction<GameWebRequestTranslator>> _receiveMailInfo = null;
    
    protected override void Awake()
    {
        _isLoadedStatic = false;
        base.Awake();
    }

    protected override void Init()
    {
        base.Init();
        _userInfo = EightUtil.GetCore<GameUserMgr>().UserInfo;

        _getMailInfos = null;
        _receiveMailInfo = null;
        _getMailInfos += GetMailInfos;
        _receiveMailInfo += ReceiveMailInfo;
        
        GetMailInfos();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public static void GetMails(UnityAction successEvent = null)
    {
        _getMailInfos?.Invoke(successEvent);
    }

    public static void ReceiveMailItem(int mailId, UnityAction<GameWebRequestTranslator> successEvent = null)
    {
        _receiveMailInfo?.Invoke(mailId, successEvent);
    }

    private void GetMailInfos(UnityAction successEvent = null)
    {
        Translator.Request(GameWebRequestType.Get_Mails, (trans) =>
        {
            var infos = trans.GetRequestData<List<MailInfo>>();
            MailUtil.ReleaseMailData();
            foreach (var info in infos)
            {
                if(info.IsReceived) continue;
                
                MailUtil.RegisterMailData(info);
            }
            
            successEvent?.Invoke();

            _isLoadedStatic = _isLoaded = true;
        });
    }

    private void ReceiveMailInfo(int mailId, UnityAction<GameWebRequestTranslator> successEvent = null)
    {
        Translator.SetSendData("mailId", mailId);
        Translator.Request(GameWebRequestType.Receive_Mail, (trans) =>
        {
            successEvent?.Invoke(trans);
        });
    }
}
