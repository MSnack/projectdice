﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class EmoticonDeckInfo : DataBaseSystem
{
    private int _deck1;
    public int Deck1
    {
        get => _deck1;
        set => _deck1 = value;
    }

    private int _deck2;
    public int Deck2
    {
        get => _deck2;
        set => _deck2 = value;
    }

    private int _deck3;
    public int Deck3
    {
        get => _deck3;
        set => _deck3 = value;
    }

    private int _deck4;
    public int Deck4
    {
        get => _deck4;
        set => _deck4 = value;
    }
    
    private int _deck5;
    public int Deck5
    {
        get => _deck5;
        set => _deck5 = value;
    }
    
    private int _deck6;
    public int Deck6
    {
        get => _deck6;
        set => _deck6 = value;
    }
    
    private int _deck7;
    public int Deck7
    {
        get => _deck7;
        set => _deck7 = value;
    }
    
    private int _deck8;
    public int Deck8
    {
        get => _deck8;
        set => _deck8 = value;
    }

    public override void SetJson()
    {
        JsonObject["deck1"] = _deck1;
        JsonObject["deck2"] = _deck2;
        JsonObject["deck3"] = _deck3;
        JsonObject["deck4"] = _deck4;
        JsonObject["deck5"] = _deck5;
        JsonObject["deck6"] = _deck6;
        JsonObject["deck7"] = _deck7;
        JsonObject["deck8"] = _deck8;
    }
}

[Serializable]
public class EmoticonInfo : DataBaseSystem
{
    private int _userId;
    public int UserId
    {
        get => _userId;
        set => _userId = value;
    }

    public int _emoticonId;
    public int EmoticonId
    {
        get => _emoticonId;
        set => _emoticonId = value;
    }

    public override void SetJson()
    {
        JsonObject["userId"] = _userId;
        JsonObject["emoticonId"] = _emoticonId;
    }
}

public class EmoticonServerConnector : ServerConnectorBase
{
    private static bool _isLoadedStatic = false;
    public static bool IsLoaded => _isLoadedStatic;
    
    private GameUserInfo _userInfo = null;

    private static UnityAction<EmoticonDeckInfo, UnityAction> _onUpdateEmoDeck = null;
    private static UnityAction<UnityAction> _onGetEmoDeck = null;
    private static UnityAction<UnityAction> _onGetEmo = null;

    protected override void Awake()
    {
        _isLoadedStatic = false;
        base.Awake();
    }

    protected override void Init()
    {
        if (_userInfo == null) _userInfo = EightUtil.GetCore<GameUserMgr>()?.UserInfo;
        _onUpdateEmoDeck += UpdateEmoDeckFunction;
        _onGetEmoDeck += GetEmoDeckFunction;
        _onGetEmo += GetEmoFunction;
        base.Init();
        GetEmoticonData(() => _isLoaded = _isLoadedStatic = true);
    }

    public static void GetEmoticonDeck(UnityAction successEvent = null)
    {
        _onGetEmoDeck?.Invoke(successEvent);
    }

    public static void UpdateEmoticonDeck(EmoticonDeckInfo emoticonDeckInfo, UnityAction successEvent = null)
    {
        _onUpdateEmoDeck?.Invoke(emoticonDeckInfo, successEvent);
    }

    public static void GetEmoticonData(UnityAction successEvent = null)
    {
        _onGetEmo?.Invoke(successEvent);
    }

    private void UpdateEmoDeckFunction(EmoticonDeckInfo emoticonDeckInfo, UnityAction successEvent = null)
    {
        if (_userInfo == null) _userInfo = EightUtil.GetCore<GameUserMgr>()?.UserInfo;
        
        Translator.SetSendDataAtClass(emoticonDeckInfo);
        Translator.Request(GameWebRequestType.Update_UserEmoDeckInfo,
            (trans) =>
            {
                EmoticonUtil.RegisterEmoticonDeckData(emoticonDeckInfo);
                successEvent?.Invoke();
            });
    }

    private void GetEmoDeckFunction(UnityAction successEvent = null)
    {
        if (_userInfo == null) _userInfo = EightUtil.GetCore<GameUserMgr>()?.UserInfo;
        Translator.Request(GameWebRequestType.Get_UserEmoDeckInfo, 
            trans => {
                var infoData = trans.GetRequestData<EmoticonDeckInfo>();
                EmoticonUtil.RegisterEmoticonDeckData(infoData);
                successEvent?.Invoke();
            });
    }

    private void GetEmoFunction(UnityAction successEvent = null)
    {
        if (_userInfo == null) _userInfo = EightUtil.GetCore<GameUserMgr>()?.UserInfo;
        Translator.Request(GameWebRequestType.Get_UserEmoticonInfos, 
            trans => {
                var infoData = trans.GetRequestData<List<EmoticonInfo>>();
                EmoticonUtil.RegisterEmoticonData(infoData);
                successEvent?.Invoke();
            });
    }
}
