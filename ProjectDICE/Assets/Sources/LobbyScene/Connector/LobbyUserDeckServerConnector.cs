﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class DeckInfo
{
    public int deckIndex = -1;
    public int deck1 = -1;
    public int deck2 = -1;
    public int deck3 = -1;
    public int deck4 = -1;
    public int deck5 = -1;

    public int[] ToDeckArray()
    {
        return new int[] { deck1, deck2, deck3, deck4, deck5 };
    }

    public void SetDeckArray(int[] deckList)
    {
        deck1 = deckList[0];
        deck2 = deckList[1];
        deck3 = deckList[2];
        deck4 = deckList[3];
        deck5 = deckList[4];
    }
}

public class LobbyUserDeckServerConnector : ServerConnectorBase
{

    private static UnityAction<int, UnityAction<DeckInfo>, bool> _onGetDeckData;
    private static UnityAction<DeckInfo, UnityAction<DeckInfo>> _onSetDeckData;
    private static UnityAction<int> _onSetCurrentDeckIndex;
    
    private Dictionary<int, DeckInfo> _decks = null;
    private static int _currentDeckIndex = 1;
    
    private static bool _isLoadedStatic = false;
    public static bool IsLoaded => _isLoadedStatic;

    protected override void Awake()
    {
        _isLoadedStatic = false;
        base.Awake();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        _decks = new Dictionary<int, DeckInfo>();
    }

    protected override void Init()
    {

        _onGetDeckData = null;
        _onSetDeckData = null;
        _onSetCurrentDeckIndex = null;
        
        _onGetDeckData += GetDeckDataFunc;
        _onSetDeckData += SetDeckDataFunc;
        _onSetCurrentDeckIndex += SetCurrentDeckIndexFunc;
        
        //Get Current Deck Index
        BattleInfoServerConnector.GetBattleInfo((info) =>
        {
            _currentDeckIndex = info.DeckId;
            GetDeckDataFunc(_currentDeckIndex);
            _isLoaded = true;
            _isLoadedStatic = true;
        });
    }

    public static void GetDeckData(int deckId, UnityAction<DeckInfo> resultFunction = null, bool forceUpdate = false)
    {
        _onGetDeckData?.Invoke(deckId, resultFunction, forceUpdate);
    }

    public static void GetCurrDeckData(UnityAction<DeckInfo> resultFunction = null, bool forceUpdate = false)
    {
        GetDeckData(_currentDeckIndex, resultFunction, forceUpdate);
    }

    public static void SetDeckData(DeckInfo info, UnityAction<DeckInfo> successEvent = null)
    {
        _onSetDeckData?.Invoke(info, successEvent);
    }

    public static void SetCurrentDeckIndex(int index)
    {
        _onSetCurrentDeckIndex?.Invoke(index);
    }

    private void GetDeckDataFunc(int deckId, UnityAction<DeckInfo> resultFunction = null, bool forceUpdate = false)
    {
        if (_decks.ContainsKey(deckId) && !forceUpdate)
        {
            var deck = _decks[deckId];
            resultFunction?.Invoke(deck);
            return;
        }
        
        RequestDeckData(deckId, resultFunction);
        return;
    }

    private void SetDeckDataFunc(DeckInfo info, UnityAction<DeckInfo> successEvent = null)
    {
        int deckId = info.deckIndex;
        //서버에 post 전송
        Translator.SetSendDataAtClass(info);
        Translator.Request(GameWebRequestType.Update_UserDeckInfo,
            (trans) =>
            {
                GetDeckDataFunc(deckId, successEvent, true);
            });

    }

    private void SetCurrentDeckIndexFunc(int index)
    {
        _currentDeckIndex = index;
        BattleInfoServerConnector.GetBattleInfo((info) =>
        {
            info.DeckId = index;
            BattleInfoServerConnector.UpdateBattleInfo();
        });
    }

    public static int GetStartDeckIndex()
    {
        return _currentDeckIndex;
    }

    private void RequestDeckData(int deckId, UnityAction<DeckInfo> resultFunction = null)
    {
        var userInfo = EightUtil.GetCore<GameUserMgr>().UserInfo;
        string query = $"/{deckId}";
        Translator.Request(GameWebRequestType.Get_UserDeckInfo, query, 
            trans => {
                var infoData = trans.GetRequestData<DeckInfo>();
                Request(infoData);
                resultFunction?.Invoke(infoData);
            });
    }

    private void Request(DeckInfo infoData)
    {
        int index = infoData.deckIndex;

        _decks[index] = infoData;
    }
}
