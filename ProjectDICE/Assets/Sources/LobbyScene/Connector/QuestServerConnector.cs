﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EightWork;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class QuestInfo : DataBaseSystem
{
    private int _questId;
    public int QuestId
    {
        get => _questId;
        set => _questId = value;
    }

    private int _category;
    public int Category
    {
        get => _category;
        set => _category = value;
    }

    private int _slotId;
    public int SlotId
    {
        get => _slotId;
        set => _slotId = value;
    }

    private int _value;
    public int Value
    {
        get => _value;
        set => _value = value;
    }

    private bool _isReceived;
    public bool IsReceived
    {
        get => _isReceived;
        set => _isReceived = value;
    }

    public override void SetJson()
    {
        
    }
}

public class QuestServerConnector : ServerConnectorBase
{
    private Dictionary<int, List<GameIndexIDData>> _questSlotCountData;

    private static UnityAction<UnityAction> _onRefreshUserQuestInfos;
    private static UnityAction<QuestCondition, int, bool, UnityAction> _onCompleteTutorial;
    private static UnityAction<QuestCategory, int, UnityAction<int>, UnityAction> _onReceiveQuestReward;

    public static bool IsLoaded { get; private set; }
    
    public static void RefreshUserQuestInfos(UnityAction successEvent = null)
    {
        _onRefreshUserQuestInfos?.Invoke(successEvent);
    }

    public static void CompleteTutorial(QuestCondition condition, int value, bool isIncrement,
        UnityAction successEvent = null)
    {
        _onCompleteTutorial?.Invoke(condition, value, isIncrement, successEvent);
    }

    public static void ReceiveQuestReward(QuestCategory category, int slotId, UnityAction<int> successEvent = null,
        UnityAction failedEvent = null)
    {
        _onReceiveQuestReward?.Invoke(category, slotId, successEvent, failedEvent);
    }


    // Start is called before the first frame update
    private void Start()
    {
    }

    private DateTime _tomorrow = DateTime.Today.AddDays(1);
    private void Update()
    {
        TimeSpan t = _tomorrow - DateTime.Now;
        
        if (t.TotalSeconds <= 0)
        {
            IsLoaded = false;
            StartCoroutine(InitWhenDataLoaded());
            _tomorrow = DateTime.Today.AddDays(1);
            return;
        }
    }

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
    }

    protected override void Init()
    {
        base.Init();
        _onRefreshUserQuestInfos += InitAllQuestInfosFunction;
        _onCompleteTutorial += CompleteTutorialFunction;
        _onReceiveQuestReward += ReceiveQuestRewardFunction;
        StartCoroutine(InitWhenDataLoaded());
    }

    private void InitAllQuestInfosFunction(UnityAction successEvent = null)
    {
        IsLoaded = false;
        StartCoroutine(InitWhenDataLoaded(successEvent));
    }

    private IEnumerator InitWhenDataLoaded(UnityAction successEvent = null)
    {
        yield return new WaitUntil(() => EightUtil.GetCore<GameDataMgr>().IsLoaded);

        _questSlotCountData = EightUtil.GetCore<GameDataMgr>()
            .GetCsvData(GameDataMgr.DataId.QuestSlotCountData);

        OnRefreshQuestInfos(() => StartCoroutine(InitAllQuestInfos(successEvent)));
    }

    private void OnRefreshQuestInfos(UnityAction successEvent = null)
    {
        Translator.Request(GameWebRequestType.Refresh_UserQuestInfos,
            trans =>
            {
                successEvent?.Invoke();
            });
    }

    private IEnumerator InitAllQuestInfos(UnityAction successEvent = null)
    {
        var values = Enum.GetValues(typeof(QuestCategory)).Cast<QuestCategory>();
        foreach (var category in values)
        {
            if (category == QuestCategory.None) continue;
            bool isReceived = false;
            GetUserQuestInfosFunction(category, () => isReceived = true);
            if (!isReceived) yield return new WaitUntil(() => isReceived);
        }
        _isLoaded = IsLoaded = true;
        successEvent?.Invoke();
    }

    private void GetUserQuestInfosFunction(QuestCategory category, UnityAction successEvent = null)
    {
        int maxIndex;
        try
        {
            maxIndex = _questSlotCountData[(int) category][0].ToClass<QuestSlotCountData>().SlotCount;
        }
        catch (KeyNotFoundException e)
        {
            throw new KeyNotFoundException("The quest cannot be loaded from the server. Please check the server data.");
        }

        JObject sendObj = new JObject
        {
            ["category"] = (int) category,
            ["minIndex"] = 1,
            ["maxIndex"] = maxIndex
        };

        Translator.SetSendData(sendObj.ToString());
        Translator.Request(GameWebRequestType.Get_UserQuestInfos, (trans) =>
        {
            var received = trans.GetRequestData<List<QuestInfo>>();
            if (received == null || received.Count <= 0) return;
            QuestUtil.ReleaseQuestData(category);
            QuestUtil.RegisterQuestData(category, received);

            successEvent?.Invoke();
        });
    }

    private void CompleteTutorialFunction(QuestCondition condition, int value, bool isIncrement, UnityAction successEvent = null)
    {
        JObject sendObj = new JObject
        {
            ["condition"] = (int) condition,
            ["isIncrement"] = isIncrement,
            ["value"] = value
        };
        
        Translator.SetSendData(sendObj.ToString());
        Translator.Request(GameWebRequestType.Complete_Tutorial, (trans) =>
        {
            successEvent?.Invoke();
        });
    }

    private void ReceiveQuestRewardFunction(QuestCategory category, int slotId, UnityAction<int> successEvent = null,
        UnityAction failedEvent = null)
    {
        JObject sendObj = new JObject
        {
            ["category"] = (int) category,
            ["slotId"] = slotId,
        };

        Translator.SetSendData(sendObj.ToString());
        Translator.Request(GameWebRequestType.Receive_QuestReward, (trans) =>
        {
            var received = trans.GetRequestData();
            int result = int.Parse(received["result"].ToString());
            if (result > 0)
            {
                int mailId = int.Parse(received["mailId"].ToString());
                successEvent?.Invoke(mailId);
                return;
            }

            failedEvent?.Invoke();
        });
    }
}