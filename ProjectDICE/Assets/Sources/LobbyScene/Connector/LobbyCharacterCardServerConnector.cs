﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class CharacterCardInfo : DataBaseSystem
{

    private int _characterId;
    public int CharacterId
    {
        get => _characterId;
        set => _characterId = value;
    }

    private int _level;
    public int Level
    {
        get => _level;
        set => _level = value;
    }

    private int _cardCount;
    public int CardCount
    {
        get => _cardCount;
        set => _cardCount = value;
    }


    public override void SetJson()
    {
        JsonObject["characterId"] = _characterId;
        JsonObject["level"] = _level;
        JsonObject["cardCount"] = _cardCount;
    }
}


public class LobbyCharacterCardServerConnector : ServerConnectorBase
{
    public enum ErrorCode
    {
        ERROR_IN_CLIENT = -1,
        NONE = 0,
        NOT_ENOUGH_GOLD = 1,
        NOT_ENOUGH_CONDITION = 2,
        UNKOWN_DATA = 3
    }

    private static UnityAction<int, int, UnityAction, UnityAction<ErrorCode>> _onUpdateUserCard;
    private static UnityAction<UnityAction> _onGetUserCards;
    
    private static bool _isLoadedStatic = false;
    public static bool IsLoaded => _isLoadedStatic;
    
    private GameUserInfo _userInfo = null;
    private GameDataMgr _gameDataMgr = null;

    protected override void Awake()
    {
        _isLoadedStatic = false;
        base.Awake();

        _onUpdateUserCard = null;
        _onGetUserCards = null;
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
    }

    protected override void Init()
    {
        _userInfo = EightUtil.GetCore<GameUserMgr>().UserInfo;
        _gameDataMgr = EightUtil.GetCore<GameDataMgr>();
        GetCurrentCharacterList();

        _onUpdateUserCard += UpdateCharacterDetail;
        _onGetUserCards += GetCurrentCharacterList;
    }

    public static void GetCurrentCharList(UnityAction successEvent = null)
    {
        _isLoadedStatic = false;
        _onGetUserCards?.Invoke(successEvent);
    }

    public static void UpgradeCharacter(int charId, UnityAction successEvent = null,
        UnityAction<ErrorCode> failedEvent = null)
    {
        _onUpdateUserCard?.Invoke(charId, 1, successEvent, failedEvent);
    }

    private void UpdateCharacterDetail(int charId, int upgradeCount, UnityAction successEvent,
        UnityAction<ErrorCode> failedEvent)
    {
        var detail = CharacterCardUtil.GetCardDetail(charId);
        if (detail == null)
        {
            failedEvent?.Invoke(ErrorCode.ERROR_IN_CLIENT);
            return;
        }
        
        //화면 잠금
        GamePopupMgr.OpenLoadingPopup();
        
        var info = detail.ToCharacterCardInfo();
        info.Level++;
        //1차 검증
        var localCsvData = _gameDataMgr.GetCsvData(GameDataMgr.DataId.CardUpgradeData);
        int key = (int)(info.CharacterId / 1000) * 1000 + info.Level;
        if (localCsvData.ContainsKey(key) == false)
        {
            GamePopupMgr.CloseLoadingPopup();
            return;
        }
        var localData = localCsvData[key][0].ToClass<CardUpgradeData>();
        int currentGold = LobbyGoodsUtil.GetGoods(GoodsID.GOLD);

        if (currentGold - localData.NeedGold < 0)
        {
            failedEvent?.Invoke(ErrorCode.NOT_ENOUGH_GOLD);
            GamePopupMgr.CloseLoadingPopup();
            return;
        }

        if (info.CardCount - localData.NeedCard < 0)
        {
            failedEvent?.Invoke(ErrorCode.NOT_ENOUGH_CONDITION);
            GamePopupMgr.CloseLoadingPopup();
            return;
        }
        
        
        //1차 검증 끝
        
        Translator.SetSendData(info.ToJson());
        Translator.Request(GameWebRequestType.Update_UserCharacterInfo,
            (trans) =>
            {
                var received = trans.GetRequestData();
                int result = int.Parse(received["result"].ToString());

                if (result > 0)
                {
                    info.CardCount = Mathf.Max(0, info.CardCount - localData.NeedCard);
                    CharacterCardUtil.RegisterUserCardDetail(info);
                    successEvent?.Invoke();
                    GamePopupMgr.CloseLoadingPopup();
                    return;
                }
                
                ErrorCode code = (ErrorCode) int.Parse(received["errorCode"].ToString());
                failedEvent?.Invoke(code);
                GamePopupMgr.CloseLoadingPopup();
            });

    }

    private void GetCurrentCharacterList(UnityAction successEvent = null)
    {
        Translator.Request(GameWebRequestType.Get_UserCharacterInfo, 
            (trans) =>
            {
                var infos = trans.GetRequestData<List<CharacterCardInfo>>();
                CharacterCardUtil.ClearUserCardDetails();
                
                foreach (var info in infos)
                {
                    if (info == null) continue;
                    CharacterCardUtil.RegisterUserCardDetail(info.CharacterId, info.Level, info.CardCount);
                }

                _isLoadedStatic = _isLoaded = true;
                successEvent?.Invoke();

            });
    }
}
