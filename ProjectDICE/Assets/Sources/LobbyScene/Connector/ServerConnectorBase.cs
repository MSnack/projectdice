﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public abstract class ServerConnectorBase : EightReceiver
{
    [SerializeField, ReadOnly()]
    protected bool _isLoaded = false;

    public bool IsLoaded => _isLoaded;

    protected readonly GameWebRequestTranslator Translator = new GameWebRequestTranslator();

    protected virtual void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        EightUtil.GetCore<GameUserMgr>().OnLoginCompleteEvent += Init;
    }

    /**
     * Connector가 온전히 로드되었을 때 한번 호출되는 함수(OnLoadCompleteDevice 이후에 호출됨)
     */
    protected virtual void Init()
    {
        _isLoaded = true;
    }
}
