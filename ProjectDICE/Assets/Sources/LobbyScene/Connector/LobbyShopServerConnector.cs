﻿using System;
using System.Collections.Generic;
using System.Linq;
using EightWork;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine.Events;

[Serializable]
public class DailyShopInfo
{
    private int _count;

    private int _dailyPackageId;

    private bool _isSoldOut;

    private int _slotId;

    public int SlotId
    {
        get => _slotId;
        set => _slotId = value;
    }

    public int DailyPackageId
    {
        get => _dailyPackageId;
        set => _dailyPackageId = value;
    }

    public bool IsSoldOut
    {
        get => _isSoldOut;
        set => _isSoldOut = value;
    }

    public int Count
    {
        get => _count;
        set => _count = value;
    }
}

[Serializable]
public class PurchaseInfo
{
    private DateTime _date;
    private string _purchaseId;

    private string _purchaseToken;
    private string _packageName;

    public string PurchaseId
    {
        get => _purchaseId;
        set => _purchaseId = value;
    }

    public string PurchaseToken
    {
        get => _purchaseToken;
        set => _purchaseToken = value;
    }

    public string PackageName
    {
        get => _packageName;
        set => _packageName = value;
    }

    public DateTime Date
    {
        get => _date;
        set => _date = value;
    }
}

public class LobbyShopServerConnector : ServerConnectorBase
{
    public enum ErrorCode
    {
        NONE = 0,
        UNKNOWN_PACKAGE_ID = 1,
        NOT_ENOUGH_GOODS = 2,
        UNKNOWN_RECEIPT = 3,
        UNKOWN_PURCHASE_STATE = 4,
        VERIFY_RECEIPT_FAILED = 5,
        REFUND_RECEIPT = 6,
        HAS_ALREADY_BEEN_LIMITED_TO_PURCHASE = 7,
    }

    private static UnityAction<int, UnityAction, UnityAction<ErrorCode>> _buyShopPackage;
    private static UnityAction<int, PurchaseInfo, UnityAction<int[]>, UnityAction<ErrorCode>> _buyShopPackageByCash;
    private static UnityAction<int, PurchaseInfo, UnityAction, UnityAction<ErrorCode>> _buyDailyShopPackageByCash;
    private static UnityAction<int, UnityAction<bool>> _checkShopLimitState;
    private static UnityAction<int, UnityAction, UnityAction<ErrorCode>> _buyDailyShopPackage;
    private static UnityAction<UnityAction<List<DailyShopInfo>, int>> _getDailyShopInfo;
    private static UnityAction<UnityAction<List<DailyShopInfo>>, UnityAction> _refreshDailyShop;

    public static bool IsLoaded { get; private set; }

    // Start is called before the first frame update
    private void Start()
    {
    }

    protected override void Init()
    {
        base.Init();
        _isLoaded = IsLoaded = true;
        _buyShopPackage += BuyShopPackageFunction;
        _buyShopPackageByCash += BuyShopPackageByCashFunction;
        _buyDailyShopPackageByCash += BuyDailyShopPackageByCashFunction;
        _checkShopLimitState += CheckShopLimitFunction;
        _buyDailyShopPackage += BuyDailyShopPackageFunction;
        _getDailyShopInfo += GetDailyShopInfos;
        _refreshDailyShop += RefreshDailyShopInfos;
    }

    public static void BuyShopPackage(int packageId, UnityAction successEvent = null,
        UnityAction<ErrorCode> failedEvent = null)
    {
        _buyShopPackage?.Invoke(packageId, successEvent, failedEvent);
    }

    public static void BuyShopPackage(int packageId, PurchaseInfo purchaseInfo, UnityAction<int[]> successEvent = null,
        UnityAction<ErrorCode> failedEvent = null)
    {
        _buyShopPackageByCash?.Invoke(packageId, purchaseInfo, successEvent, failedEvent);
    }
    
    public static void BuyDailyShopPackage(int packageId, PurchaseInfo purchaseInfo, UnityAction successEvent = null,
        UnityAction<ErrorCode> failedEvent = null)
    {
        _buyDailyShopPackageByCash?.Invoke(packageId, purchaseInfo, successEvent, failedEvent);
    }

    public static void CheckShopLimitState(int packageId, UnityAction<bool> resultEvent = null)
    {
        _checkShopLimitState?.Invoke(packageId, resultEvent);
    }

    public static void BuyDailyShopPackage(int packageId, UnityAction successEvent = null,
        UnityAction<ErrorCode> failedEvent = null)
    {
        _buyDailyShopPackage?.Invoke(packageId, successEvent, failedEvent);
    }

    private void BuyShopPackageFunction(int packageId, UnityAction successEvent = null,
        UnityAction<ErrorCode> failedEvent = null)
    {
        var jsonObject = new JObject
        {
            ["packageId"] = packageId
        };
        BuyShopTranslator(jsonObject, GameWebRequestType.Buy_ShopPackage, successEvent, failedEvent);
    }

    private void BuyShopPackageByCashFunction(int packageId, PurchaseInfo purchase, UnityAction<int[]> successEvent = null,
        UnityAction<ErrorCode> failedEvent = null)
    {
        JObject jsonObject = null;
        if (purchase == null)
        {
            jsonObject = new JObject
            {
                ["packageId"] = packageId
            };
        }
        else
        {
            jsonObject = new JObject
            {
                ["packageId"] = packageId,
                ["purchaseId"] = purchase.PurchaseId,
                ["purchaseToken"] = purchase.PurchaseToken,
                ["packageName"] = purchase.PackageName,
                ["date"] = purchase.Date
            };
        }

        BuyShopTranslator(jsonObject, GameWebRequestType.Buy_ShopPackage, successEvent, failedEvent);
    }
    
    private void BuyDailyShopPackageByCashFunction(int packageId, PurchaseInfo purchase, UnityAction successEvent = null,
        UnityAction<ErrorCode> failedEvent = null)
    {
        var jsonObject = new JObject
        {
            ["packageId"] = packageId,
            ["purchaseId"] = purchase.PurchaseId,
            ["purchaseToken"] = purchase.PurchaseToken,
            ["packageName"] = purchase.PackageName,
            ["date"] = purchase.Date
        };
        BuyShopTranslator(jsonObject, GameWebRequestType.Buy_DailyShopPackage, successEvent, failedEvent);
    }

    private void BuyDailyShopPackageFunction(int packageId, UnityAction successEvent = null,
        UnityAction<ErrorCode> failedEvent = null)
    {
        var jsonObject = new JObject
        {
            ["packageId"] = packageId
        }; 
        BuyShopTranslator(jsonObject, GameWebRequestType.Buy_DailyShopPackage, successEvent, failedEvent);
    }

    private void BuyShopTranslator(JObject jsonObject, GameWebRequestType type, UnityAction successEvent = null,
        UnityAction<ErrorCode> failedEvent = null)
    {
        Translator.SetSendData(jsonObject.ToString());
        Translator.Request(type, trans =>
        {
            var received = trans.GetRequestData();
            var result = int.Parse(received["result"].ToString());
            if (result > 0)
            {
                successEvent?.Invoke();
                return;
            }

            var code = (ErrorCode) int.Parse(received["errorCode"].ToString());
            failedEvent?.Invoke(code);
        });
    }
    
    private void BuyShopTranslator(JObject jsonObject, GameWebRequestType type, UnityAction<int[]> successEvent = null,
        UnityAction<ErrorCode> failedEvent = null)
    {
        Translator.SetSendData(jsonObject.ToString());
        Translator.Request(type, trans =>
        {
            var received = trans.GetRequestData();
            var result = int.Parse(received["result"].ToString());
            if (result > 0)
            {
                var mailIdRaw = received["mailId"].ToList();
                var mailIdList = new List<int>();
                for (int i = 0; i < mailIdRaw.Count; ++i)
                {
                    mailIdList.Add(int.Parse(mailIdRaw[i].ToString()));
                }
                successEvent?.Invoke(mailIdList.ToArray());
                return;
            }

            var code = (ErrorCode) int.Parse(received["errorCode"].ToString());
            failedEvent?.Invoke(code);
        });
    }

    private void CheckShopLimitFunction(int packageId, UnityAction<bool> successEvent = null)
    {
        Translator.SetSendData("packageId", packageId);
        Translator.Request(GameWebRequestType.Check_ShopLimitState, (trans) =>
        {
            var received = trans.GetRequestData();
            int result = int.Parse(received["result"].ToString());
            
            successEvent?.Invoke(result == 1);
        });
    }

    public static void GetDailyShop(UnityAction<List<DailyShopInfo>, int> successEvent = null)
    {
        _getDailyShopInfo?.Invoke(successEvent);
    }

    private void GetDailyShopInfos(UnityAction<List<DailyShopInfo>, int> successEvent = null)
    {
        Translator.Request(GameWebRequestType.Get_DailyShopInfos, trans =>
        {
            var received = trans.GetRequestData();
            var result = JsonConvert.DeserializeObject<List<DailyShopInfo>>(received["result"].ToString());
            successEvent?.Invoke(result.ToList(), int.Parse(received["refreshCount"].ToString()));
        });
    }

    public static void RefreshDailyShop(UnityAction<List<DailyShopInfo>> successEvent = null,
        UnityAction failEvent = null)
    {
        _refreshDailyShop?.Invoke(successEvent, failEvent);
    }

    private void RefreshDailyShopInfos(UnityAction<List<DailyShopInfo>> successEvent = null,
        UnityAction failEvent = null)
    {
        Translator.Request(GameWebRequestType.Refresh_DailyShop, trans =>
        {
            var received = trans.GetRequestData<List<DailyShopInfo>>();
            if (received.Count <= 0)
            {
                failEvent?.Invoke();
                return;
            }

            successEvent?.Invoke(received);
        });
    }
}