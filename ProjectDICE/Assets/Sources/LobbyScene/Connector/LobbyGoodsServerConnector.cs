﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class GoodsDataInfo : DataBaseSystem
{
    private int _id;
    public int Id
    {
        get => _id;
        set => _id = value;
    }

    private int _value;
    public int Value
    {
        get => _value;
        set => _value = value;
    }

    public override void SetJson()
    {
        JsonObject["id"] = _id;
        JsonObject["value"] = _value;
    }
}



public class LobbyGoodsServerConnector : ServerConnectorBase
{

    private static UnityAction<GoodsID, int, UnityAction> _onIncrementGoods;
    private static UnityAction<UnityAction> _onGetAllGoods;
    private static UnityAction _successEvent;

    private static bool _isLoadedStatic = false;
    public static bool IsLoaded => _isLoadedStatic;
    
    
    private const int GoodsSize = 2;
    private GameUserInfo _userInfo = null;
    private Coroutine _requestCoroutine = null;

    protected override void Awake()
    {
        _isLoadedStatic = false;
        base.Awake();
        _onIncrementGoods = null;
        _onGetAllGoods = null;
        _successEvent = null;
    }

    protected override void Init()
    {
        _userInfo = EightUtil.GetCore<GameUserMgr>().UserInfo;

        _onIncrementGoods += UpdateGoodsInfo;
        _onGetAllGoods += RefreshGoodsInfos;
        
        base.Init();
        _isLoadedStatic = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public static void IncrementGoods(GoodsID goodsId, int incrementValue, UnityAction successEvent = null)
    {
        // _onIncrementGoods?.Invoke(goodsId, incrementValue, successEvent);
    }

    public static void GetAllGoods(UnityAction successEvent = null)
    {
        _onGetAllGoods?.Invoke(successEvent);
    }
    
    private void UpdateGoodsInfo(GoodsID goodsId, int incrementValue, UnityAction successEvent = null)
    {
        var info = new GoodsDataInfo() {Id = (int) goodsId, Value = incrementValue};
        IncrementGoods(info, successEvent);
    }

    private void IncrementGoods(GoodsDataInfo goodsDataInfo, UnityAction successEvent = null)
    {
        // Translator.SetSendData(goodsDataInfo.ToJson());
        // Translator.Request(GameWebRequestType.Update_UserGoods, (trans) =>
        // {
        //     LobbyGoodsUtil.SetGoods((GoodsID) goodsDataInfo.Id,
        //         LobbyGoodsUtil.GetGoods((GoodsID) goodsDataInfo.Id) + goodsDataInfo.Value);
        //     // UpdateBestTier(goodsDataInfo);
        //     successEvent?.Invoke();
        // });
    }

    private void UpdateBestTier(GoodsDataInfo info)
    {
        if ((GoodsID) info.Id != GoodsID.TROPHY) return;

        
        int currentTrophy = LobbyGoodsUtil.GetGoods(GoodsID.TROPHY);
        GameBattleInfo battleInfo = null;

        BattleInfoServerConnector.GetBattleInfo((received) => { battleInfo = received; });
        if (currentTrophy > battleInfo.BestRating)
        {
            battleInfo.BestRating = currentTrophy;
            BattleInfoServerConnector.UpdateBattleInfo();
        }
    }

    public void RefreshGoodsInfos(UnityAction successEvent = null)
    {
        if (_requestCoroutine != null)
        {
            Debug.LogWarning("RequestCoroutine is still running.");
            if (successEvent != null) _successEvent += successEvent;
            return;
        }

        _successEvent = null;
        _requestCoroutine = StartCoroutine(PushGoodsRequest(GoodsSize, successEvent));
    }

    private IEnumerator PushGoodsRequest(int max, UnityAction successEvent = null)
    {
        _successEvent += successEvent;
        bool isReceived = false;
        Translator.Request(GameWebRequestType.Get_UserAllGoods,
            (trans) =>
            {
                var infos = trans.GetRequestData<List<GoodsDataInfo>>();
                foreach (var info in infos)
                {
                    if(info == null) continue;
                    LobbyGoodsUtil.SetGoods((GoodsID) info.Id, info.Value);
                }
                _successEvent?.Invoke();
                _successEvent = null;
                isReceived = true;
            });

        yield return new WaitUntil(() => isReceived);
        _requestCoroutine = null;
    }
}
