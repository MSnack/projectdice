﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class UserBoxInvenInfo
{

    private int _boxIndex;
    public int BoxIndex
    {
        get => _boxIndex;
        set => _boxIndex = value;
    }

    private int _boxId;
    public int BoxId
    {
        get => _boxId;
        set => _boxId = value;
    }

    private DateTime _openStart;

    public string OpenStart
    {
        set
        {
            if (value == null || value.Contains("0000-00-00 00:00:00"))
            {
                _openStart = DateTime.MinValue;
                return;
            }
            
            _openStart = Convert.ToDateTime(value);
        }
    }

    public DateTime OpenStartTime => _openStart;
}

[Serializable]
public class BoxResultInfo
{
    private int _result;
    public int Result
    {
        get => _result;
        set => _result = value;
    }

    private int _boxId;
    public int BoxId
    {
        get => _boxId;
        set => _boxId = value;
    }

    private LobbyBoxServerConnector.ErrorCode _errorCode;
    public LobbyBoxServerConnector.ErrorCode ErrorCode
    {
        get => _errorCode;
        set => _errorCode = value;
    }

    private List<RewardItemInfo> _value;
    public string Value
    {
        set => _value = JsonConvert.DeserializeObject<List<RewardItemInfo>>(value);
    }
    public List<RewardItemInfo> ResultValue => _value;
}

[Serializable]
public class UserBoxInfo : DataBaseSystem
{

    private int _boxIndex;
    public int BoxIndex
    {
        get => _boxIndex;
        set => _boxIndex = value;
    }

    private int _boxId;
    public int BoxId
    {
        get => _boxId;
        set => _boxId = value;
    }
    
    private LobbyBoxServerConnector.BoxReference _reference;
    public LobbyBoxServerConnector.BoxReference Reference
    {
        get => _reference;
        set => _reference = value;
    }

    public override void SetJson()
    {
        JsonObject["boxIndex"] = _boxIndex;
        JsonObject["boxId"] = _boxId;
        JsonObject["reference"] = (int)_reference;
    }
}

public class LobbyBoxServerConnector : ServerConnectorBase
{
    public enum BoxReference
    {
        NONE = 0,
        INVENTORY = 1,
        SHOP = 2,
        INVENTORY_OPEN = 3,
    }
    
    public enum ErrorCode
    {
        NONE = 0,
        CANNOT_FIND_BOX = 1,
        UNKOWN_REFERENCE = 2,
    }
    
    private static bool _isLoadedStatic = false;
    public static bool IsLoaded => _isLoadedStatic;
    
    private GameUserInfo _userInfo = null;

    private static UnityAction<int, int, BoxReference, UnityAction<BoxResultInfo>> _onOpenBox = null;
    private static UnityAction<UnityAction> _onGetUserBox = null;
    private static UnityAction<int, UnityAction, UnityAction<int>> _removeBoxTime = null;


    protected override void Awake()
    {
        _isLoadedStatic = false;
        base.Awake();
    }
    
    protected override void Init()
    {
        _onOpenBox = null;
        _onOpenBox += OpenBoxFunction;

        _onGetUserBox = null;
        _onGetUserBox += GetUserBoxes;
        
        _removeBoxTime = null;
        _removeBoxTime += RemoveBoxTimeFunc;
        
        base.Init();
        
        _userInfo = EightUtil.GetCore<GameUserMgr>().UserInfo;

        GetUserBoxes();

    }

    public static void OpenBox(int boxId, int boxIndex, BoxReference reference,
        UnityAction<BoxResultInfo> resultEvent = null)
    {
        _onOpenBox?.Invoke(boxId, boxIndex, reference, resultEvent);
    }

    public static void GetUserBoxInventory(UnityAction successEvent = null)
    {
        _onGetUserBox?.Invoke(successEvent);
    }

    public static void RemoveBoxTime(int boxIndex, UnityAction successEvent = null, UnityAction<int> failedEvent = null)
    {
        _removeBoxTime?.Invoke(boxIndex, successEvent, failedEvent);
    }

    private void OpenBoxFunction(int boxId, int boxIndex, BoxReference reference, UnityAction<BoxResultInfo> resultEvent)
    {
        var info = new UserBoxInfo()
        {
            BoxId = boxId,
            BoxIndex = boxIndex,
            Reference = reference,
        };
        
        Translator.SetSendData(info.ToJson());
        Translator.Request(GameWebRequestType.Open_UserBox, 
        (trans) =>
        {
            var result = trans.GetRequestData<BoxResultInfo>();
            resultEvent?.Invoke(result);
        });
    }

    private void GetUserBoxes(UnityAction successEvent = null)
    {
        Translator.Request(GameWebRequestType.Get_UserBoxes, 
        (trans) =>
        {
            var infos = trans.GetRequestData<List<UserBoxInvenInfo>>();
            LobbyBoxUtil.SetUserBoxInventory(infos);
            
            _isLoaded = true;
            _isLoadedStatic = true;
            successEvent?.Invoke();
        });
    }

    private void RemoveBoxTimeFunc(int boxIndex, UnityAction successEvent = null, UnityAction<int> failedEvent = null)
    {
        Translator.SetSendData("boxIndex", boxIndex);
        
        Translator.Request(GameWebRequestType.Box_RemoveBoxTime, 
            (trans) =>
            {
                var info = trans.GetRequestData();
                if (int.Parse(info["result"].ToString()) == 1)
                {
                    successEvent?.Invoke();
                }
                else
                {
                    failedEvent?.Invoke(int.Parse(info["errorCode"].ToString()));
                }
            });
    }
}
