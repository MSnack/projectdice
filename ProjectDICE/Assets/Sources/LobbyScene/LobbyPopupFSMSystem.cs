﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using EightWork.FSM;
using UnityEngine;

public class LobbyPopupFSMSystem : EightFSMSystem<LobbyPopupFSMSystem.LobbyPopupFSMState, LobbyPopupStateBase, LobbyPopupInterface>
{

    public enum LobbyPopupFSMState
    {
        NONE,
        USER_INFO,
        INTRO_POPUP,
        CARD_INFO,
        SETTING_DECK,
        MAILBOX,
        BATTLE_HISTORY,
        SETTINGS,
        
        PREV_STATE = -1000,
    }

    private Stack<LobbyPopupFSMState> _prevState = new Stack<LobbyPopupFSMState>();

    [SerializeField, ReadOnly()]
    public static bool IsPopupActivated = false;

    protected override void Awake()
    {
        base.Awake();
        
        SetupComplete();
    }

    // Start is called before the first frame update
    void Start()
    {
        LobbyPopupInterface.CallChangingState += ChangeStateFunction;
    }
    
    private void ChangeStateFunction(LobbyPopupFSMState state)
    {
        if (CurrState == state) return;
        
        IsPopupActivated = true;
        
        if (state == LobbyPopupFSMState.PREV_STATE)
        {
            if (_prevState.Count <= 0) { ChangeState(LobbyPopupFSMState.NONE); return; }

            var changingState = _prevState.Pop();
            if (changingState == LobbyPopupFSMState.NONE) SetPopupInactive();
            ChangeState(changingState);
            return;
        }

        if (state == LobbyPopupFSMState.NONE) SetPopupInactive();
        else _prevState.Push(CurrState);
        ChangeState(state);
    }

    private void SetPopupInactive()
    {
        _prevState.Clear();
        IsPopupActivated = false;
    }

}
