﻿using System.Collections;
using System.Collections.Generic;
using EightWork.FSM;
using UnityEngine;

public abstract class LobbyPopupStateBase : EightFSMStateBase<LobbyPopupFSMSystem.LobbyPopupFSMState, LobbyPopupFSMSystem>
{
    [SerializeField]
    private GameObject[] _activeObjects;
    [SerializeField]
    private GameObject[] _inactiveObjects;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void StartState()
    {
        foreach (var obj in _activeObjects)
        {
            obj.SetActive(true);
        }
        
        foreach (var obj in _inactiveObjects)
        {
            obj.SetActive(false);
        }

    }

    public override void EndState()
    {
        foreach (var obj in _activeObjects)
        {
            obj.SetActive(false);
        }

        foreach (var obj in _inactiveObjects)
        {
            obj.SetActive(true);
        }
    }
}
