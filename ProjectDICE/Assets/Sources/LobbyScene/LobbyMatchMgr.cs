﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class LobbyMatchMgr : EightReceiver
{

    [SerializeField]
    private MatchSocket _matchSocket = null;

    [SerializeField]
    private GameObject _matchingPopupPrefab = null;

    private GameFirebaseMgr _firebaseMgr = null;

    private void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();
        _firebaseMgr = EightUtil.GetCore<GameFirebaseMgr>();
    }

    public void OnMatch()
    {
        if (_matchSocket.IsComplateLogin == false)
            return;
        
        _matchSocket.Matching();
    }

    public void OnMatchPopup()
    {
        GamePopupMgr.OpenPrefabPopup("매칭 중", _matchingPopupPrefab, new []
        {
            new NormalPopup.ButtonConfig("취소", OnCancel), 
        });
    }

    private void OnCancel()
    {
        _matchSocket.MatchOut();
    }

    public void OnCanceled()
    {
        PrefabPopup.Close();
    }

    public void UpdateMatchingQuest()
    {
        QuestUtil.UpdateQuestEvent(QuestCondition.PlayGame, 1);
    }

    public void ReallocateServerToken()
    {
        if (_firebaseMgr == null)
        {
            Debug.LogAssertion("FirebaseMgr is null.");
            return;
        }
        
        _firebaseMgr.ReallocateServerToken();
    }
}
