﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

[System.Serializable]
public class GameBattleInfo : DataBaseSystem
{

    [SerializeField, ReadOnly]
    private int _win = -1;
    public int Win { get => _win; set => _win = value; }

    [SerializeField, ReadOnly]
    private int _lose = -1;
    public int Lose { get => _lose; set => _lose = value; }
    
    [SerializeField, ReadOnly]
    private int _deckId = -1;
    public int DeckId { get => _deckId; set => _deckId = value; }
    
    [SerializeField, ReadOnly]
    private int _revWin = -1;
    public int RevWin { get => _revWin; set => _revWin = value; }
    
    [SerializeField, ReadOnly]
    private int _revLose = -1;
    public int RevLose { get => _revLose; set => _revLose = value; }
    
    [SerializeField, ReadOnly]
    private int _maxWave = -1;
    public int MaxWave { get => _maxWave; set => _maxWave = value; }
    
    [SerializeField, ReadOnly]
    private int _bestRating = -1;
    public int BestRating { get => _bestRating; set => _bestRating = value; }

    [SerializeField, ReadOnly]
    private float _level = 1;

    public float Level { get => _level; set => _level = value; }

    [SerializeField, ReadOnly]
    private int _favorCharId = 1001;
    public int FavorCharId { get => _favorCharId; set => _favorCharId = value; }

    [SerializeField, ReadOnly]
    private int _exp = 0;
    public int Exp { get => _exp; set => _exp = value; }

    public override void SetJson()
    {
        JsonObject["win"] = Win;
        JsonObject["lose"] = Lose;
        JsonObject["deckId"] = DeckId;
        JsonObject["revWin"] = RevWin;
        JsonObject["revLose"] = RevLose;
        JsonObject["maxWave"] = MaxWave;
        JsonObject["bestRating"] = BestRating;
        JsonObject["level"] = Level;
        JsonObject["favorCharId"] = FavorCharId;
        JsonObject["exp"] = Exp;
    }
}
