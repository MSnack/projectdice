﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using EightWork;
using UnityEngine;

public class QuestSlotCountData : GameIndexIDData
{
    public int SlotCount { get; private set; }

    public override void LoadCSVLine(string csvLine)
    {
        if (string.IsNullOrEmpty(csvLine))
            return;

        string[] csvParts = Regex.Split(csvLine, CSVUtil.SplitRe);
        int csvPartIndex = 0;

        DataID = int.Parse(csvParts[csvPartIndex++]);
        SlotCount = int.Parse(csvParts[csvPartIndex++]);
    }
}
