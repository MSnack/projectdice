﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using EightWork;
using UnityEngine;

public class BoxLocalData : GameIndexIDData
{
    
    public int Time { get; private set; }
    public int NKind { get; private set; }
    public int NCount { get; private set; }
    public int RKind { get; private set; }
    public int RCount { get; private set; }
    public int SKind { get; private set; }
    public int SCount { get; private set; }
    public int HKind { get; private set; }
    public int HCount { get; private set; }
    public int LKind { get; private set; }
    public int LCount { get; private set; }
    public float LPer { get; private set; }

    public override void LoadCSVLine(string csvLine)
    {
        if (string.IsNullOrEmpty(csvLine))
            return;

        string[] csvParts = Regex.Split(csvLine, CSVUtil.SplitRe);
        int csvPartIndex = 0;

        DataID = int.Parse(csvParts[csvPartIndex++]);
        Time = int.Parse(csvParts[csvPartIndex++]);
        NKind = int.Parse(csvParts[csvPartIndex++]);
        NCount = int.Parse(csvParts[csvPartIndex++]);
        RKind = int.Parse(csvParts[csvPartIndex++]);
        RCount = int.Parse(csvParts[csvPartIndex++]);
        SKind = int.Parse(csvParts[csvPartIndex++]);
        SCount = int.Parse(csvParts[csvPartIndex++]);
        HKind = int.Parse(csvParts[csvPartIndex++]);
        HCount = int.Parse(csvParts[csvPartIndex++]);
        LKind = int.Parse(csvParts[csvPartIndex++]);
        LCount = int.Parse(csvParts[csvPartIndex++]);
        LPer = float.Parse(csvParts[csvPartIndex++]);
    }
}
