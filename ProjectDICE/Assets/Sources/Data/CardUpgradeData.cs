﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using EightWork;
using UnityEngine;

[System.Serializable]
public class CardUpgradeData : GameIndexIDData
{
    public int Level { get; private set; }
    public int Tier { get; private set; }
    public int NeedCard { get; private set; }
    public int NeedGold { get; private set; }
    public int Exp { get; private set; }
    
    public override void LoadCSVLine(string csvLine)
    {
        if (string.IsNullOrEmpty(csvLine))
            return;

        string[] csvParts = Regex.Split(csvLine, CSVUtil.SplitRe);
        int csvPartIndex = 0;

        Level = int.Parse(csvParts[csvPartIndex++]);
        Tier = int.Parse(csvParts[csvPartIndex++]);
        NeedCard = int.Parse(csvParts[csvPartIndex++]);
        NeedGold = int.Parse(csvParts[csvPartIndex++]);
        Exp = int.Parse(csvParts[csvPartIndex++]);
        
        DataID = int.Parse(csvParts[csvPartIndex++]);
    }
}
