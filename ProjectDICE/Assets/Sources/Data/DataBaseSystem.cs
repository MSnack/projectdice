﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public abstract class DataBaseSystem : JsonConvertInterface
{
    private JObject _jsonObject = new JObject();
    protected JObject JsonObject => _jsonObject;

    public abstract void SetJson();

    public string ToJson()
    {
        SetJson();
        return JsonObject.ToString();
    }
}
