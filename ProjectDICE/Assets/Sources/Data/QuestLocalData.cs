﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using EightWork;
using UnityEngine;
using WebSocketSharp;

public class QuestLocalData : GameIndexIDData
{
    public int Category { get; private set; }
    public int DetailCategory { get; private set; }
    public string[] Condition { get; private set; }
    public int TargetValue { get; private set; }
    public ItemCategory ItemCategory { get; private set; }
    public int ItemId { get; private set; }
    public int ItemCount { get; private set; }
    public int ResetDays { get; private set; }
    public int ResetMonths { get; private set; }
    public int StartDays { get; private set; }
    public string Title { get; private set; }
    public string Description { get; private set; }
    public bool IsIncrement { get; private set; }

    public override void LoadCSVLine(string csvLine)
    {
        if (string.IsNullOrEmpty(csvLine))
            return;

        string[] csvParts = Regex.Split(csvLine, CSVUtil.SplitRe);
        int csvPartIndex = 0;

        DataID = int.Parse(csvParts[csvPartIndex++]);
        Category = int.Parse(csvParts[csvPartIndex++]);
        string detailCategory = csvParts[csvPartIndex++];
        DetailCategory = detailCategory.IsNullOrEmpty() ? 0 : int.Parse(detailCategory);
        Condition = csvParts[csvPartIndex++].Split('|');
        TargetValue = int.Parse(csvParts[csvPartIndex++]);
        ItemCategory = (ItemCategory) int.Parse(csvParts[csvPartIndex++]);
        ItemId = int.Parse(csvParts[csvPartIndex++]);
        ItemCount = int.Parse(csvParts[csvPartIndex++]);
        ResetDays = int.Parse(csvParts[csvPartIndex++]);
        ResetMonths = int.Parse(csvParts[csvPartIndex++]);
        string startDays = csvParts[csvPartIndex++];
        StartDays = startDays.IsNullOrEmpty() ? 0 : int.Parse(startDays);
        Title = csvParts[csvPartIndex++];
        Description = csvParts[csvPartIndex++];
        IsIncrement = Array.Find(Condition, (x) => x.Equals("+")) != null;
    }
}
