﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Text;
using System.Text.RegularExpressions;

namespace EightWork
{
    public static class CSVUtil
    {
        public static string SplitRe = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
        public static string LineSplitRe = @"\r\n|\n\r|\n|\r";
        public static char[] TrimChars = { '\"' };


        public static void LoadCSVData(Dictionary<int, List<GameIndexIDData>> dataMap, string path, Type type)
        {
            ParseCSVFile(dataMap, path, type);
        }


        private static void ParseCSVFile(Dictionary<int, List<GameIndexIDData>> dataMap, string csvString, Type type)
        {
            string[] csvStringArray = Regex.Split(csvString, LineSplitRe);
            if (csvStringArray.Length <= 1)
                return;

            // 해더 건너뛰기 위해서 1부터 
            for (int csvStringIndex = 1; csvStringIndex < csvStringArray.Length; ++csvStringIndex)
            {
                if (csvStringArray[csvStringIndex].Length <= 0)
                    continue;
                
                GameIndexIDData data = Activator.CreateInstance(type) as GameIndexIDData;

                data?.LoadCSVLine(csvStringArray[csvStringIndex]);
                if (data == null || data.DataID == -1)
                    continue;

                if (dataMap.ContainsKey(data.DataID) == false)
                {
                    dataMap.Add(data.DataID, new List<GameIndexIDData>());
                }
                dataMap[data.DataID].Add(data);
            }
        }
    }
}
