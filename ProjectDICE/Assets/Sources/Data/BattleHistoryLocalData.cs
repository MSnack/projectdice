﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class BattleHistoryLocalData
{
    public int MatchId;
    
    public bool IsWin;

    //상대방 정보
    public string OpponentName;

    public int OpponentTier;

    public int TrophyIncrementValue;

    public int OpponentTrophy;

    public int[] OpponentDeck;

    public string OpponentGuild;
    
    //내 정보
    public string MyName;

    public int MyTier;

    public int MyTrophy;

    public int[] MyDeck;

    public string MyGuild;

    public static void Save(string fileName, BattleHistoryLocalData[] data)
    {
        try
        {
            string path = Application.persistentDataPath + "/" + fileName;
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Create);

            formatter.Serialize(stream, data);
            stream.Close();
        }
        catch (IOException e)
        {
            Debug.LogError("BattleHistoryLocalData has IOException.");
            return;
        }
        
    }

    public static BattleHistoryLocalData[] Load(string fileName)
    {
        string path = Application.persistentDataPath + "/" + fileName;
        if (File.Exists(path))
        {
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);

                var data = formatter.Deserialize(stream) as BattleHistoryLocalData[];

                stream.Close();
                return data;
            }
            catch (IOException e)
            {
                Debug.LogError("BattleHistoryLocalData has IOException.");
                return null;
            }
            
        }
        
        return null;
    }
}
