﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using EightWork;
using UnityEngine;

public class BoxOpenTimeData : GameIndexIDData
{
    public int Time { get; private set; }
    public int PriceStar { get; private set; }

    public override void LoadCSVLine(string csvLine)
    {
        if (string.IsNullOrEmpty(csvLine))
            return;

        string[] csvParts = Regex.Split(csvLine, CSVUtil.SplitRe);
        int csvPartIndex = 0;

        csvPartIndex++;
        DataID = int.Parse(csvParts[csvPartIndex]);
        Time = int.Parse(csvParts[csvPartIndex++]);
        PriceStar = int.Parse(csvParts[csvPartIndex++]);
    }
}
