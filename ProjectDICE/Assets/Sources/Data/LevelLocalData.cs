﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using EightWork;
using UnityEngine;

public class LevelLocalData : GameIndexIDData
{
    public int Level { get; private set; }
    public int NeedExp { get; private set; }
    public int WinGold { get; private set; }
    
    public override void LoadCSVLine(string csvLine)
    {
        if (string.IsNullOrEmpty(csvLine))
            return;

        string[] csvParts = Regex.Split(csvLine, CSVUtil.SplitRe);
        int csvPartIndex = 0;

        DataID = int.Parse(csvParts[csvPartIndex++]);
        Level = int.Parse(csvParts[csvPartIndex++]);
        NeedExp = int.Parse(csvParts[csvPartIndex++]);
        WinGold = int.Parse(csvParts[csvPartIndex++]);
    }
}
