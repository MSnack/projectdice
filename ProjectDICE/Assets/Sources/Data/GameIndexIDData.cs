﻿using EightWork;
using UnityEngine;

[System.Serializable]
public abstract class GameIndexIDData
{
    [SerializeField]
    private int _dataID = SystemIndex.InvalidInt;
    public int DataID { get { return _dataID; } protected set { _dataID = value; } }

    public GameIndexIDData() { DataID = SystemIndex.InvalidInt; }

    public virtual void LoadJsonData(string jsonData) { }
    public abstract void LoadCSVLine(string csvLine);

    public virtual T ToClass<T>()
        where T : GameIndexIDData
    {
        return (T) this;
    }
}