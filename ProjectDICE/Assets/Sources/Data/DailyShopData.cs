﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using EightWork;
using UnityEngine;

public class DailyShopData : GameIndexIDData
{
    public ItemCategory ItemCategory { get; private set; }
    public int ItemId { get; private set; }
    public int Grade { get; private set; }
    public GoodsID Goods { get; private set; }
    public int Price { get; private set; }
    public int MinCount { get; private set; }
    public int MaxCount { get; private set; }

    public override void LoadCSVLine(string csvLine)
    {
        if (string.IsNullOrEmpty(csvLine))
            return;

        string[] csvParts = Regex.Split(csvLine, CSVUtil.SplitRe);
        int csvPartIndex = 0;

        DataID = int.Parse(csvParts[csvPartIndex++]);
        ItemCategory = (ItemCategory) int.Parse(csvParts[csvPartIndex++]);
        ItemId = int.Parse(csvParts[csvPartIndex++]);
        Grade = int.Parse(csvParts[csvPartIndex++]);
        Goods = (GoodsID) int.Parse(csvParts[csvPartIndex++]);
        Price = int.Parse(csvParts[csvPartIndex++]);
        MinCount = int.Parse(csvParts[csvPartIndex++]);
        MaxCount = int.Parse(csvParts[csvPartIndex++]);
    }
}
