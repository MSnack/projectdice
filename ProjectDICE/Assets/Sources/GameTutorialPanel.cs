﻿using System;
using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;

public class GameTutorialPanel : MonoBehaviour
{
    [SerializeField, ReadOnly]
    private List<GameObject> _tutorialList = null;

    private short _tutorialIndex = 0;
    public short TutorialIndex => _tutorialIndex;
    
    [SerializeField, ReadOnly]
    private bool _isComplete = false;
    public bool IsComplete => _isComplete;

    [SerializeField]
    private short[] _dontSavePageList = null;

    private void Awake()
    {
        for (int i = 0; i < transform.childCount; ++i)
        {
            var child = transform.GetChild(i);
            _tutorialList.Add(child.gameObject);
            child.gameObject.SetActive(false);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _tutorialList[0].SetActive(true);
    }

    public bool NextPage()
    {
        if (_isComplete) return false;
        
        _tutorialList[_tutorialIndex].SetActive(false);
        ++_tutorialIndex;
        
        if (_tutorialList.Count <= _tutorialIndex)
        {
            _isComplete = true;
            return true;
        }
        
        _tutorialList[_tutorialIndex].SetActive(true);

        for (short i = 0; i < _dontSavePageList.Length; ++i)
        {
            if (_tutorialIndex == _dontSavePageList[i])
                return false;
        }

        return true;
    }

    public bool ChangePage(short index)
    {
        if (_isComplete) return false;
        
        _tutorialList[_tutorialIndex].SetActive(false);
        _tutorialIndex = index;
        
        if (_tutorialList.Count <= _tutorialIndex)
        {
            _isComplete = true;
            Destroy(gameObject);
            return true;
        }
        
        _tutorialList[_tutorialIndex].SetActive(true);
        
        for (short i = 0; i < _dontSavePageList.Length; ++i)
        {
            if (_tutorialIndex == _dontSavePageList[i])
                return false;
        }

        return true;
    }
}
