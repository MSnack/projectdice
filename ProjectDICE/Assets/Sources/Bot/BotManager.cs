﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightWork.Bot
{
    public class BotManager : MonoBehaviour
    {
        private static BotManager _instance = null;
        public static BotManager Instance => GetInstance();

        private static BotManager GetInstance()
        {
            if (_instance == null)
            {
                GameObject botManager = new GameObject("BotManager");
                _instance = botManager.AddComponent<BotManager>();
                DontDestroyOnLoad(_instance);
            }

            return _instance;
        }
    }
}