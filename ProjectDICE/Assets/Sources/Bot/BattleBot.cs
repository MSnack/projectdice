﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine.Events;

namespace EightWork.Bot
{
    public class BattleBot : BotBehaviour
    {
        public enum BotEventType
        {
            CreateUnit,
            Merge,
            Upgrade,
            GiveUp,
            Continue,
        }

        [System.Serializable]
        private class BotEventWeight : SerializableDictionaryBase<BotEventType, int> { }

        [SerializeField]
        private BattleSceneInterface _interface = null;

        [SerializeField]
        private BattleSocket _socket = null;

        [SerializeField]
        private BattleManager _localManager = null;

        [SerializeField]
        private BattleCharacterCreator _characterCreatetor = null;

        [SerializeField, Range(0.0f, 10.0f)]
        private float _maxWaitDelay = 1.0f;

        [SerializeField]
        private BotEventWeight _weightData = null;

        [SerializeField, ReadOnly]
        private bool _isPlay = false;
        public bool IsPlay => _isPlay;

        public UnityEvent GiveUpBot = null;

        private void OnEnable()
        {
            _interface.PlayEvent += PlayEvent;

#if Bot_System
            StartCoroutine(BotRoutine());
#endif
        }

        public void ReceiveGiveUpEvent(SocketDataType type)
        {
#if Bot_System
            GiveUpBot?.Invoke();
#endif
        }

        private void OnDisable()
        {
            _interface.PlayEvent -= PlayEvent;
        }

        private void PlayEvent(bool isPlay)
        {
            _isPlay = isPlay;
        }

        public void ResultEvent()
        {
#if Bot_System
            _interface.OnChangeLobby();
#endif
        }

        private IEnumerator BotRoutine()
        {
            while (true)
            {
                if (_isPlay == false)
                    yield return new WaitUntil(() => _isPlay == true);

                var eventType = GetEventWeight();

                bool successEvent = false;
                switch (eventType)
                {
                    case BotEventType.CreateUnit: successEvent = CreateUnit(); break;
                    case BotEventType.GiveUp: successEvent = GiveUp(); break;
                    case BotEventType.Merge: successEvent = MergeEvent(); break;
                    case BotEventType.Upgrade: successEvent = UpgradeEvent(); break;
                    case BotEventType.Continue: break;
                }

                if (successEvent == false)
                {
                    yield return null;
                    continue;
                }

                var waitTime = Random.Range(0.0f, _maxWaitDelay);
                yield return new WaitForSeconds(waitTime);
            }
        }

        private BotEventType GetEventWeight()
        {
            int _totalWeight = 0;
            foreach (var weight in _weightData.Values)
            {
                _totalWeight += weight;
            }

            int currWeight = 0;
            int random = Random.Range(0, _totalWeight) + 1;
            foreach (var pair in _weightData)
            {
                currWeight += pair.Value;
                if (random <= currWeight)
                    return pair.Key;
            }

            Debug.Log("Not Select Event Bot");

            return BotEventType.Continue;
        }

        private bool CreateUnit()
        {
            _characterCreatetor.OnCreateUnit();
            return true;
        }

        private bool GiveUp()
        {
            _socket.EmitGiveUp();
            return true;
        }

        private bool MergeEvent()
        {
            var slotMgr = _localManager.TileSlotMgr;
            var unitSlotList = slotMgr.UseSlotList;
            bool isSuccess = false;
            bool isEnd = false;
            for (int i = 0; i < unitSlotList.Length; ++i)
            {
                var unit = unitSlotList[i].unit;
                if (unit == null)
                    continue;

                for (int j = unitSlotList.Length - 1; j >= i + 1; --j)
                {
                    var target = unitSlotList[j].unit;
                    if (target == null)
                        continue;

                    //bool idCheck = unit.UnitID == target.UnitID;
                    bool stepCheck = unit.UnitStep == target.UnitStep;
                    if (stepCheck == true)
                    {
                        target.OnMerge(unit);
                        // _socket.OnMergeUnit(target, unit);
                        isSuccess = true;

                        var intBool = Random.Range(0, 2);
                        isEnd = System.Convert.ToBoolean(intBool);
                        Debug.Log("IsEnd : " + intBool.ToString());
                        if (isEnd == true)
                            break;
                    }
                }

                if (isEnd == true)
                    break;
            }

            return isSuccess;
        }

        private bool UpgradeEvent()
        {
            return false;
        }
    }
}