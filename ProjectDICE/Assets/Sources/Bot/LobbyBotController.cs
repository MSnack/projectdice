﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class LobbyBotController : EightWork.Bot.BotBehaviour
{
    private enum BotType
    {
        UserBot,
        MatchBot,
        BattleSampleBot,
    }

    [SerializeField]
    private MatchSocket _matchSocket = null;

    [SerializeField]
    private bool _isPause = false;

    [SerializeField]
    private BotType _botType = BotType.MatchBot;

    [SerializeField, ReadOnly]
    private bool _isUpdateToken = true;

    private void OnEnable()
    {
        _matchSocket.MatchingEvent.AddListener(OnMatchingEvent);
        _matchSocket.MatchOutEvent.AddListener(OnMatchOutEvent);
        _matchSocket.JoinBattleRoomEvent.AddListener(JoinBattleRoom);

#if Bot_System
        StartCoroutine(MatchingRoutine());
        StartCoroutine(TokenChecker());
#endif
    }

    private IEnumerator TokenChecker()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(1800.0f);
            _isUpdateToken = false;
            EightUtil.GetCore<GameFirebaseMgr>().ReallocateServerToken(() =>
            {
                _isUpdateToken = true;
            });
        }
    }

    private void OnMatchingEvent()
    {
        _isPause = true;
    }

    private void OnMatchOutEvent()
    {
        _isPause = false;
    }

    private void OnDisable()
    {
        _matchSocket.JoinBattleRoomEvent.RemoveListener(JoinBattleRoom);
        _matchSocket.MatchOutEvent.RemoveListener(OnMatchOutEvent);
        _matchSocket.MatchingEvent.RemoveListener(OnMatchingEvent);
    }

    private void JoinBattleRoom()
    {
        StopCoroutine(MatchingRoutine());
    }

    private IEnumerator MatchingRoutine()
    {
        while (true)
        {
            if (_isUpdateToken == false)
                yield return new WaitUntil(() => _isUpdateToken == true);

            if (_matchSocket.SocketMgr.IsLogin == false)
                yield return new WaitUntil(() => _matchSocket.SocketMgr.IsLogin == true);

            if (_isPause == true)
                yield return new WaitUntil(() => _isPause == false);

            if (_matchSocket.IsMatching == true)
                yield return new WaitUntil(() => _matchSocket.IsMatching == false);

            if (_matchSocket.EventWait == true)
                yield return new WaitUntil(() => _matchSocket.EventWait == false);

            if (LobbyUserDeckServerConnector.IsLoaded == false)
                yield return new WaitUntil(() => LobbyUserDeckServerConnector.IsLoaded == true);

            //List<int> deckList = new List<int>();
            List<CharacterCardUtil.UserCardDetail> cardList = new List<CharacterCardUtil.UserCardDetail>();
            cardList.AddRange(CharacterCardUtil.CardList);
            DeckInfo deckInfo = null;
            LobbyUserDeckServerConnector.GetCurrDeckData((info) =>
            {
                deckInfo = info;
                //deckList.AddRange(deckInfo.ToDeckArray());
            });

            if (deckInfo == null)
                yield return new WaitUntil(() => deckInfo != null);

            List<int> updateDeck = new List<int>();
            for (int i = 0; i < 5; ++i)
            {
                int index = Random.Range(0, cardList.Count);
                updateDeck.Add(cardList[index].CharacterId);
                cardList.RemoveAt(index);
            }

            bool isUpdateDeck = false;
            deckInfo.SetDeckArray(updateDeck.ToArray());
            LobbyUserDeckServerConnector.SetDeckData(deckInfo, (info) =>
            {
                isUpdateDeck = true;
            });

            if (isUpdateDeck == false)
                yield return new WaitUntil(() => isUpdateDeck == true);

#if Bot_System
            if (_botType == BotType.UserBot)
                _matchSocket.Matching();
            else
                _matchSocket.BotMatching(System.Enum.GetName(typeof(BotType), _botType));
#endif
            //_matchSocket.Matching();
            yield return null;
        }
    }
}
