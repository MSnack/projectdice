﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

[System.Serializable]
public class AssetReferenceComponent<TComponent> : AssetReferenceT<TComponent> where TComponent : MonoBehaviour
{
    public AssetReferenceComponent(string guid) : base(guid) { }

    public override bool ValidateAsset(Object obj)
    {

        var type = obj.GetType();

        if (typeof(TComponent).IsAssignableFrom(type))
        {

            return true;
        }

        GameObject gameObject = (obj as GameObject);

        return gameObject ? gameObject.GetComponent<TComponent>() != null : false;
    }

    public override bool ValidateAsset(string path)
    {

#if UNITY_EDITOR
        var type = UnityEditor.AssetDatabase.GetMainAssetTypeAtPath(path);

        if (typeof(GameEffectObject).IsAssignableFrom(type))
        {

            return true;
        }

        GameObject gameObject = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(path);

        return gameObject ? gameObject.GetComponent<TComponent>() != null : false;
#else
        return false;
#endif
    }
}
