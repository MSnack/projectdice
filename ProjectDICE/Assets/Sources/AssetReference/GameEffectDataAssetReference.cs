﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

[System.Serializable]
public class GameEffectDataAssetReference : AssetReferenceT<GameEffectData>
{
    public GameEffectDataAssetReference(string guid) : base(guid) { }
}
