﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine.UI;

public class JsonTestCode : MonoBehaviour
{
    private class JsonTestClass
    {
        public int a = 10;
        public float b = 20;
        public string c = "2313dfasdf";
    }

    [SerializeField]
    private int testCount = 100000;

    [SerializeField]
    private Text _text = null;

    private void Start()
    {
        StartCoroutine(TestJson());
    }

    private IEnumerator TestJson()
    {
        for (int i = 0; i < testCount; ++i)
        {
            string data = JsonConvert.SerializeObject(new JsonTestClass());
            _text.text = "Serialized : " + data + " : " + i.ToString();
            Debug.Log("Serialized : " + data + " : " + i.ToString());

            yield return null;
        }

        string deummy = JsonConvert.SerializeObject(new JsonTestClass());
        for (int i = 0; i < testCount; ++i)
        {
            JsonTestClass data = JsonConvert.DeserializeObject<JsonTestClass>(deummy);
            _text.text = "DeSerialized : " + data + " : " + i.ToString();
            Debug.Log("DeSerialized : " + data + " : " + i.ToString());

            yield return null;
        }
    }
}
