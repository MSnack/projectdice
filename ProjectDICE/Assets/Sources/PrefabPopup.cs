﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PrefabPopup : MonoBehaviour
{
    [Header("Popup Texts"), SerializeField]
    private string _titleText = "";

    [SerializeField]
    private GameObject _contentPrefab = null;

    [Header("Button Settings"), SerializeField]
    private NormalPopup.ButtonConfig[] _buttonConfigs;

    [SerializeField]
    private GameObject _buttonPrefab = null;

    [Header("References"), SerializeField]
    private GameObject _buttonLists = null;

    [SerializeField]
    private Text _titleTextObject = null;

    [SerializeField]
    private Transform _contentTransform = null;

    private GameObject _currentContent = null;

    private UIAlphaController _alphaController = null;

    private static UnityAction _closeAction;

    [SerializeField]
    private ContentSizeFitter[] _fitters;

    // Start is called before the first frame update
    void Start()
    {
        _alphaController = GetComponent<UIAlphaController>();
        _closeAction += CloseEvent;
    }

    private void OnEnable()
    {
        RefreshPopupData();
                
        for(int i = 0; i < _fitters.Length; ++i)
        {
            var fitter = _fitters[i];
            fitter.enabled = false;
            fitter.enabled = true;
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform) fitter.transform);
        }
    }

    private void RefreshPopupData()
    {
        _titleTextObject.text = _titleText;
        if(_currentContent != null) Destroy(_currentContent.gameObject);
        _currentContent = Instantiate(_contentPrefab, _contentTransform);

        foreach (Transform child in _buttonLists.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (var config in _buttonConfigs)
        {
            var bt = Instantiate(_buttonPrefab, _buttonLists.transform);
            var comp = bt.GetComponent<PopupButtonBase>();
            if (comp == null) continue;
            
            comp.SetData(config);
        }

        if (_alphaController != null)
        {
            _alphaController.ReloadObjects();
            _alphaController.Alpha = 0;
        }
    }

    public void SetData(string title, GameObject contentPrefab, NormalPopup.ButtonConfig[] configs)
    {
        
        _titleText = title;
        _contentPrefab = contentPrefab;
        _buttonConfigs = configs;
        
        gameObject?.SetActive(true);
    }

    public void CloseEvent()
    {
        gameObject.SetActive(false);
        if(_alphaController != null)
            _alphaController.Alpha = 1;
    }

    public static void Close()
    {
        _closeAction?.Invoke();
    }
}
