﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ScrollPopup : MonoBehaviour
{
    [Header("Popup Texts"), SerializeField]
    private string _titleText = "";

    [TextArea, SerializeField]
    private string _contextText = "";

    [Header("Button Settings"), SerializeField]
    private NormalPopup.ButtonConfig[] _buttonConfigs;

    [SerializeField]
    private GameObject _buttonPrefab = null;

    [Header("References"), SerializeField]
    private GameObject _buttonLists = null;

    [SerializeField]
    private Text _titleTextObject = null;
    [SerializeField]
    private Text _contextTextObject = null;

    private UIAlphaController _alphaController = null;

    private static UnityAction _closeAction;

    // Start is called before the first frame update
    void Start()
    {
        _alphaController = GetComponent<UIAlphaController>();
        _closeAction += CloseEvent;
    }

    private void OnEnable()
    {
        RefreshPopupData();
    }

    private void RefreshPopupData()
    {
        _titleTextObject.text = _titleText;
        _contextTextObject.text = _contextText;
        
        foreach (Transform child in _buttonLists.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (var config in _buttonConfigs)
        {
            var bt = Instantiate(_buttonPrefab, _buttonLists.transform);
            var comp = bt.GetComponent<PopupButtonBase>();
            if (comp == null) continue;
            
            comp.SetData(config);
        }

        if (_alphaController != null)
        {
            _alphaController.ReloadObjects();
            _alphaController.Alpha = 0;
        }
        
    }

    public void SetData(string title, string context, NormalPopup.ButtonConfig[] configs)
    {
        
        _titleText = title;
        _contextText = context;
        _buttonConfigs = configs;
        
        gameObject?.SetActive(true);
    }

    public void CloseEvent()
    {
        gameObject.SetActive(false);
        if(_alphaController != null)
            _alphaController.Alpha = 1;
    }

    public static void Close()
    {
        _closeAction?.Invoke();
    }
}
