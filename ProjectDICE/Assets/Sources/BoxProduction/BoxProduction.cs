﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class BoxProduction : MonoBehaviour
{
    public enum BoxProductionType
    {
        None = -1,
        Idle,
        Intro,
        OpenUp,
    }

    [SerializeField]
    private BoxProductionController _controller = null;

    public void OnProduction(BoxResultInfo info)
    {
        _controller.ChangeState(BoxProductionType.Intro);
    }
}
