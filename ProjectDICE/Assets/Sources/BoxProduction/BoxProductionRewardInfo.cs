﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxProductionRewardInfo : MonoBehaviour
{
    [SerializeField]
    private Text _ribbonText = null;

    [SerializeField]
    private string _ribbontTextFormat = "{0} x{1}";

    [SerializeField]
    private Text _cardRankText = null;

    [SerializeField]
    private Text _cardUpgradeText = null;

    [SerializeField]
    private string _cardUpgradeTextFormat = "{0}/{1}";

    [SerializeField]
    private Text _leftCardText = null;
}
