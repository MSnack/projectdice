﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxProductionRewardCardSlot : MonoBehaviour
{
    [SerializeField]
    private Animator _animator = null;

    public void ChangeState(int state)
    {
        _animator.SetInteger("State", state);
        _animator.Rebind();
    }
}
