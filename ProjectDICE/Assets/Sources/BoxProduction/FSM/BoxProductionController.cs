﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxProductionController : PrivateFSM<BoxProduction.BoxProductionType>
{
    private class BoxProductionState : PStateBase
    {
        private BoxProductionController _manager = null;
        public BoxProductionController Manager => _manager;

        public override void OnInit(PrivateFSM<BoxProduction.BoxProductionType> manager)
        {
            _manager = manager as BoxProductionController;
        }

        public override void Release()
        {
            base.Release();
            _manager = null;
        }
    }

    private class NoneState : BoxProductionState
    {
        public override void StartState()
        {
            base.StartState();

            Manager._boxStage.gameObject.SetActive(false);
            Manager._rewardCardSlot.gameObject.SetActive(false);
            Manager._rewardInfo.gameObject.SetActive(false);
        }
    }

    private class IdleState : BoxProductionState
    {
        public override void StartState()
        {
            base.StartState();

            Manager._boxStage.gameObject.SetActive(true);
            Manager._rewardCardSlot.gameObject.SetActive(false);
            Manager._rewardInfo.gameObject.SetActive(false);

            Manager._boxStage.ChangeState((int)Manager.CurrState);
        }
    }

    private class IntroState : BoxProductionState
    {
        public override void StartState()
        {
            base.StartState();

            Manager._boxStage.gameObject.SetActive(true);
            Manager._rewardCardSlot.gameObject.SetActive(false);
            Manager._rewardInfo.gameObject.SetActive(false);

            Manager._boxStage.ChangeState((int)Manager.CurrState);
        }
    }

    private class OpenUpState : BoxProductionState
    {
        public override void StartState()
        {
            base.StartState();

            Manager._boxStage.gameObject.SetActive(true);
            Manager._rewardCardSlot.gameObject.SetActive(true);
            Manager._rewardInfo.gameObject.SetActive(true);

            Manager._boxStage.ChangeState((int)Manager.CurrState);
        }
    }

    protected override void StateSetting()
    {
        AddState(BoxProduction.BoxProductionType.None, new NoneState());
        AddState(BoxProduction.BoxProductionType.Idle, new IdleState());
        AddState(BoxProduction.BoxProductionType.Intro, new IntroState());
        AddState(BoxProduction.BoxProductionType.OpenUp, new OpenUpState());
    }

    [SerializeField]
    private BoxProductionStage _boxStage = null;

    [SerializeField]
    private BoxProductionRewardCardSlot _rewardCardSlot = null;

    [SerializeField]
    private BoxProductionRewardInfo _rewardInfo = null;
}
