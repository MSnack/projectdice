﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[ExecuteInEditMode]
public class UIAlphaController : MonoBehaviour
{

	[SerializeField, Range(0.0f, 1.0f)]
	private float _alpha = 1;
	public float Alpha
	{
		get { return _alpha; }
		set
		{
			if (_alpha == value) return;
			_alpha = value;
			UpdateAlpha();
		}
	}

	[SerializeField]
	private bool isActiveOnlyPlayMode = true;

	private MaskableGraphic[] _graphics = null;
	private Shadow[] _shadows = null;

	private Color[] _srcColorsGraphics = null;
	private Color[] _srcColorsShadows = null;


	private void Awake()
	{
		UpdateAlpha();
	}

	// Use this for initialization
	void Start ()
	{
		isActiveOnlyPlayMode = false;
		UpdateAlpha();
	}
	
	// Update is called once per frame
	void Update () {
	}

	private void OnGUI()
	{
		if (isActiveOnlyPlayMode) return;
		
		UpdateAlpha();
	}

	private void UpdateAlpha()
	{
		if (_graphics == null || _shadows == null)
		{
			ReloadObjects();
		}

		int index = 0;
		foreach (var graphic in _graphics)
		{
			if (graphic == null || graphic.enabled == false)
			{
				++index;
				continue;
			}
			
			var color = graphic.color;
			color = new Color(color.r, color.g, color.b, Mathf.Lerp(0, _srcColorsGraphics[index].a, _alpha));
			graphic.color = color;
			++index;
		}

		index = 0;
		foreach (var shadow in _shadows)
		{
			if (shadow == null || shadow.enabled == false)
			{
				++index;
				continue;
			}

			var color = shadow.effectColor;
			color = new Color(color.r, color.g, color.b, Mathf.Lerp(0, _srcColorsShadows[index].a, _alpha));
			shadow.effectColor = color;
			++index;
		}
	}

	public void ReloadObjects()
	{
		_graphics = null; _shadows = null;
		
		_graphics = gameObject.GetComponentsInChildren<MaskableGraphic>(true);
		_srcColorsGraphics = new Color[_graphics.Length];
		for (int i = 0; i < _graphics.Length; ++i)
		{
			_srcColorsGraphics[i] = _graphics[i].color;
		}
		
		_shadows = gameObject.GetComponentsInChildren<Shadow>(true);
		_srcColorsShadows = new Color[_shadows.Length];
		for (int i = 0; i < _shadows.Length; ++i)
		{
			_srcColorsShadows[i] = _shadows[i].effectColor;
		}
	}
}
