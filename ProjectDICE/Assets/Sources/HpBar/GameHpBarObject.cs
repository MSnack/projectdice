﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.UI;

public class GameHpBarObject : MonoBehaviour
{
    [SerializeField, ReadOnly]
    private GameUnit _targetUnit = null;
    public GameUnit TargetUnit => _targetUnit;

    [SerializeField]
    private GameObject _imageParent = null;

    [SerializeField]
    private Image _hpBar = null;

    [SerializeField]
    private float _smoothWeight = 1.0f;
    public float SmoothWeight => _smoothWeight;

    private float CurrAmount
    {
        get { return _hpBar.fillAmount; }
        set { _hpBar.fillAmount = value; }
    }
    private float _targetAmount = 0.0f;
    private float _maxValue = 0.0f;

    public void OnInit(GameUnit unit)
    {
        if (unit == null)
            return;

        _imageParent.SetActive(false);

        CurrAmount = 1.0f;
        _targetAmount = 1.0f;
        _maxValue = unit.Hp;
        unit.UpdateHpEvent += UpdateGauge;
        unit.OnReturnEvent += UnitReleaseEvent;
        _targetUnit = unit;
    }

    private void OnEnable()
    {
        StartCoroutine(UpdatePosRoutine());
        StartCoroutine(UpdateGauge());
    }

    public void UpdateGauge(int currVal)
    {
        if (_imageParent.activeSelf == false)
            _imageParent.SetActive(true);

        if (currVal > _maxValue)
            _maxValue = currVal;

        var percent = (float)currVal / (float)_maxValue;
        if (percent > 1.0f)
            percent = 1.0f;
        else if (percent < 0.0f)
            percent = 0.0f;
        _targetAmount = percent;
        //_hpBar.fillAmount = percent;
    }

    private IEnumerator UpdateGauge()
    {
        while (true)
        {
            var distance = Mathf.Abs(CurrAmount - _targetAmount);
            if (distance <= 0.001f)
            {
                CurrAmount = _targetAmount;
                yield return null;
                continue;
            }

            var lerp = Mathf.Lerp(CurrAmount, _targetAmount, Time.deltaTime * _smoothWeight);
            CurrAmount = lerp;
            yield return null;
        }
    }

    private IEnumerator UpdatePosRoutine()
    {
        while (true)
        {
            if (_targetUnit == null || _targetUnit.SpineUnit == null)
            {
                yield return null;
                continue;
            }

            var pos = _targetUnit.transform.position;
            var headPoint = Vector3.Scale(_targetUnit.SpineUnit.HeadPivot, _targetUnit.SpineUnit.transform.localScale);
            transform.position = pos + _targetUnit.SpineUnit.HeadPivot;

            yield return null;
        }
    }

    private void UnitReleaseEvent(GameUnit unit)
    {
        EightUtil.GetCore<GameHpBarMgr>().ReturnHpBar(this);
    }

    public void Release()
    {
        CurrAmount = 0.0f;
        _targetAmount = 0.0f;
        _maxValue = 0.0f;

        if (_targetUnit != null)
        {
            _targetUnit.UpdateHpEvent -= UpdateGauge;
            _targetUnit.OnReturnEvent -= UnitReleaseEvent;

            _targetUnit = null;
        }
    }
}
