﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(fileName = "New GameHpBarData", menuName = "Data/GameHpBarData", order = 3)]
public class GameHpBarData : ScriptableObject
{
    public enum HpBarType
    {
        Monster,
        Boss,
    }

    [System.Serializable]
    public class HpBarPoolData
    {
        [SerializeField]
        private GameHpBarObject _hpBarObject = null;
        public GameHpBarObject HpBarObject => _hpBarObject;

        [SerializeField]
        private int _reserveCount = 5;
        public int ReserveCount => _reserveCount;
    }

    [System.Serializable]
    private class HpBarData : SerializableDictionaryBase<HpBarType, HpBarPoolData> { }

    [SerializeField]
    private HpBarData _hpBarDataList = null;
    public Dictionary<HpBarType, HpBarPoolData> HpBarDataList => _hpBarDataList.Clone();

    public HpBarPoolData GetHpBarPoolData(HpBarType type)
    {
        if (_hpBarDataList.ContainsKey(type) == false)
            return null;

        return _hpBarDataList[type];
    }
}
