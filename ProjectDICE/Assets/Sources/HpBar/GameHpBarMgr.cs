﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using EightWork.Core;

public class GameHpBarMgr : EightCore
{
    [SerializeField]
    private GameHpBarPool _hpBarPool = null;

    public bool IsLoad => _hpBarPool.IsLoad;

    public override void InitializedCore()
    {
        _hpBarPool.LoadData();
    }

    public GameHpBarObject GetHpBar(GameHpBarData.HpBarType type)
    {
        var hpBar = _hpBarPool.GetHpBarObject(type);
        if (hpBar == null)
            return null;

        return hpBar;
    }

    public void ReturnHpBar(GameHpBarObject hpBar)
    {
        if (hpBar == null)
            return;

        _hpBarPool.ReturnHpBar(hpBar);
    }

    public void ReturnAll()
    {
        _hpBarPool?.ReturnAll();
    }

    public void Clear()
    {
        _hpBarPool?.Clear();
    }
}
