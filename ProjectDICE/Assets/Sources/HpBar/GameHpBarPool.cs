﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class GameHpBarPool : MonoBehaviour
{
    private struct UseHpBarData
    {
        private GameHpBarData.HpBarType _barType;
        public GameHpBarData.HpBarType BarType => _barType;

        private GameHpBarObject _hpBarObject;
        public GameHpBarObject HpBarObject => _hpBarObject;

        public UseHpBarData(GameHpBarData.HpBarType type, GameHpBarObject hpBarObject)
        {
            _barType = type;
            _hpBarObject = hpBarObject;
        }
    }

    [System.Serializable]
    private class GameHpBarDataAddresableAsset : AssetReferenceT<GameHpBarData>
    {
        public GameHpBarDataAddresableAsset(string guid) : base(guid) { }
    }

    [SerializeField]
    private GameHpBarDataAddresableAsset _hpBarAsset = null;

    private Dictionary<GameHpBarData.HpBarType, Queue<GameHpBarObject>> _hpBarQueueList = new Dictionary<GameHpBarData.HpBarType, Queue<GameHpBarObject>>();
    private List<UseHpBarData> _useHpBarList = new List<UseHpBarData>();

    [SerializeField, ReadOnly]
    private bool _isLoad = false;
    public bool IsLoad => _isLoad;

    public void LoadData()
    {
        _hpBarAsset.LoadAssetAsync().Completed += (result) =>
        {
            if (result.Result == null)
                return;

            _isLoad = true;
        };
    }

    private bool ReserveQueue(GameHpBarData.HpBarType type)
    {
        if (_hpBarAsset.IsDone == false)
            return false;

        var poolData = ((GameHpBarData)(_hpBarAsset.Asset)).GetHpBarPoolData(type);
        if (poolData == null)
            return false;

        for (int i = 0; i < poolData.ReserveCount; ++i)
        {
            if (poolData.HpBarObject == null)
                return false;

            var clone = Instantiate(poolData.HpBarObject, transform);
            clone.gameObject.SetActive(false);
            EnqueueHpBarObject(type, clone);
        }

        return true;
    }

    private void EnqueueHpBarObject(GameHpBarData.HpBarType type, GameHpBarObject data)
    {
        if (data == null)
            return;

        if (_hpBarQueueList.ContainsKey(type) == false)
            _hpBarQueueList[type] = new Queue<GameHpBarObject>();

        data.Release();
        data.gameObject.SetActive(false);

        _hpBarQueueList[type].Enqueue(data);
    }

    private GameHpBarObject DequeueHpBarObject(GameHpBarData.HpBarType type)
    {
        if (_hpBarQueueList.ContainsKey(type) == false)
            _hpBarQueueList[type] = new Queue<GameHpBarObject>();

        if (_hpBarQueueList[type].Count <= 0)
        {
            if (ReserveQueue(type) == false)
                return null;
        }

        return _hpBarQueueList[type].Dequeue();
    }

    public GameHpBarObject GetHpBarObject(GameHpBarData.HpBarType type)
    {
        var gameHpBarObject = DequeueHpBarObject(type);
        if (gameHpBarObject == null)
            return null;

        gameHpBarObject.gameObject.SetActive(true);

        var useData = new UseHpBarData(type, gameHpBarObject);
        _useHpBarList.Add(useData);

        return gameHpBarObject;
    }

    public void ReturnHpBar(GameHpBarObject hpBar)
    {
        if (hpBar == null)
            return;

        for (int i = _useHpBarList.Count - 1; i >= 0; --i)
        {
            var useData = _useHpBarList[i];
            if (useData.HpBarObject == hpBar)
            {
                //useData.HpBarObject.gameObject.SetActive(false);
                EnqueueHpBarObject(useData.BarType, useData.HpBarObject);
                _useHpBarList.Remove(useData);
                return;
            }
        }
    }

    public void ReturnAll()
    {
        for (int i = _useHpBarList.Count - 1; i >= 0; --i)
        {
            var useData = _useHpBarList[i];
            //useData.HpBarObject.gameObject.SetActive(false);
            EnqueueHpBarObject(useData.BarType, useData.HpBarObject);
        }
        _useHpBarList.Clear();
    }

    public void Clear()
    {
        ReturnAll();

        foreach (var values in _hpBarQueueList.Values)
        {
            foreach (var value in values)
            {
                Destroy(value.gameObject);
            }
            values.Clear();
        }
    }
}
