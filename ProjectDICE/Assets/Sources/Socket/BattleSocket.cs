﻿using EightWork;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using BestHTTP.SocketIO;

public class PostUnitInfo
{
    public int unitId = -1;
    public int unitLevel = -1;
}

public class BattleSocketRequestData
{
    private SocketDataType _receiveType = SocketDataType.None;
    public SocketDataType ReceiveType => _receiveType;

    public BattleSocketRequestData(Socket s, Packet p, params object[] a)
    {
        ParseData(s, p, a);
    }

    protected virtual void ParseData(Socket s, Packet p, params object[] a)
    {
        var json = p.DecodedArgs[0] as Dictionary<string, object>;
        if (json.ContainsKey("socketId") == false)
            return;

        var socketId = json["socketId"] as string;
        if (socketId == EightUtil.GetCore<GameSocketMgr>().GameSocket.Id)
            _receiveType = SocketDataType.Local;
        else
            _receiveType = SocketDataType.Network;
    }
}

public class RequestUnitData : BattleSocketRequestData
{
    private int _unitId = -1;
    public int UnitId => _unitId;

    private int _slotIndex = -1;
    public int SlotIndex => _slotIndex;

    private int _grade = -1;
    public int Grade => _grade;

    //private List<float> _randomValues = new List<float>();
    //public float[] RandomValues => _randomValues.ToArray();

    private int _randomSeed = 0;
    public int RandomSeed => _randomSeed;

    public RequestUnitData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        _unitId = int.Parse(json["unitId"].ToString());
        _slotIndex = int.Parse(json["slotIndex"].ToString());
        _grade = int.Parse(json["grade"].ToString());
        //_randomValues.AddRange(JsonConvert.DeserializeObject<float[]>(json["randomValues"].ToString()));
        _randomSeed = int.Parse(json["randomSeed"].ToString());
    }
}

public class RequestReadyData : BattleSocketRequestData
{
    private bool _isReady = true;
    public bool IsReady => _isReady;

    public RequestReadyData(Socket s, Packet p, params object[] a) : base(s, p, a) { }
}

public class RequestInitData : BattleSocketRequestData
{
    [SerializeField]
    private bool _isInit = false;
    public bool IsInit => _isInit;

    public RequestInitData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        _isInit = bool.Parse(json["isInit"].ToString());
    }
}

public class RequestEmoticonData : BattleSocketRequestData
{
    private int _emoticonId = -1;
    public int EmoticonId => _emoticonId;

    public RequestEmoticonData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        _emoticonId = int.Parse(json["emoticonId"].ToString());
    }
}

//public class RequestMergeData : BattleSocketRequestData
//{
//    private int _targetSlotId = -1;
//    public int TargetSlotId => _targetSlotId;

//    private int _mergeSlotId = -1;
//    public int MergeSlotId => _mergeSlotId;

//    public RequestMergeData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

//    protected override void ParseData(Socket s, Packet p, params object[] a)
//    {
//        base.ParseData(s, p, a);
//        var json = a[0] as Dictionary<string, object>;

//        _targetSlotId = int.Parse(json["targetSlotId"].ToString());
//        _mergeSlotId = int.Parse(json["mergeSlotId"].ToString());
//    }
//}

public class RequestMergeUpgradeData : BattleSocketRequestData
{
    private int _targetSlotId = -1;
    public int TargetSlotId => _targetSlotId;

    private int _mergeSlotId = -1;
    public int MergeSlotId => _mergeSlotId;

    private int _unitId = -1;
    public int UnitId => _unitId;

    private int _grade = -1;
    public int Grade => _grade;

    private int _randomSeed = 0;
    public int RandomSeed => _randomSeed;

    public RequestMergeUpgradeData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        _targetSlotId = int.Parse(json["targetSlotId"].ToString());
        _mergeSlotId = int.Parse(json["mergeSlotId"].ToString());
        _unitId = int.Parse(json["unitId"].ToString());
        _grade = int.Parse(json["grade"].ToString());
        _randomSeed = int.Parse(json["randomSeed"].ToString());
    }
}

public class RequestMergeCreateData : BattleSocketRequestData
{
    private int _targetSlotId = -1;
    public int TargetSlotId => _targetSlotId;

    private int _mergeSlotId = -1;
    public int MergeSlotId => _mergeSlotId;

    private int _unitId = -1;
    public int UnitId => _unitId;

    private int _grade = -1;
    public int Grade => _grade;

    private int _randomSeed = 0;
    public int RandomSeed => _randomSeed;

    public RequestMergeCreateData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        _targetSlotId = int.Parse(json["targetSlotId"].ToString());
        _mergeSlotId = int.Parse(json["mergeSlotId"].ToString());
        _unitId = int.Parse(json["unitId"].ToString());
        _grade = int.Parse(json["grade"].ToString());
        _randomSeed = int.Parse(json["randomSeed"].ToString());
    }
}

public class RequestCopyUnitData : BattleSocketRequestData
{
    private int _targetSlotId = -1;
    public int TargetSlotId => _targetSlotId;

    private int _mergeSlotId = -1;
    public int MergeSlotId => _mergeSlotId;

    private int _unitId = -1;
    public int UnitId => _unitId;

    private int _grade = -1;
    public int Grade => _grade;

    private int _randomSeed = 0;
    public int RandomSeed => _randomSeed;

    public RequestCopyUnitData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        _targetSlotId = int.Parse(json["targetSlotId"].ToString());
        _mergeSlotId = int.Parse(json["mergeSlotId"].ToString());
        _unitId = int.Parse(json["unitId"].ToString());
        _grade = int.Parse(json["grade"].ToString());
        _randomSeed = int.Parse(json["randomSeed"].ToString());
    }
}

public class RequestBattleInitUserData : BattleSocketRequestData
{
    public class BattleUserInfo
    {
        private string _nickName = null;
        public string NickName => _nickName;

        private int _userLevel = 1;
        public int UserLevel => _userLevel;

        private int _trophy = 0;
        public int Trophy => _trophy;

        private List<PostUnitInfo> _unitList = new List<PostUnitInfo>();
        public PostUnitInfo[] UnitList => _unitList.ToArray();

        private SocketDataType _receiveType = SocketDataType.None;
        public SocketDataType ReceiveType => _receiveType;

        public BattleUserInfo(object json)
        {
            var data = json as Dictionary<string, object>;
            var userInfo = data["userInfo"] as Dictionary<string, object>;
            _nickName = userInfo["nickName"] as string;
            _userLevel = (int)float.Parse(userInfo["userLevel"].ToString());
            _trophy = int.Parse(userInfo["trophy"].ToString());

            var deckList = userInfo["deckList"] as List<object>;
            foreach (Dictionary<string, object> unitJson in deckList)
            {
                var postUnitInfo = new PostUnitInfo();
                postUnitInfo.unitId = int.Parse(unitJson["characterId"].ToString());
                postUnitInfo.unitLevel = int.Parse(unitJson["charLevel"].ToString());

                _unitList.Add(postUnitInfo);
            }

            var socketId = data["socketId"] as string;
            if (socketId == EightUtil.GetCore<GameSocketMgr>().GameSocket.Id)
                _receiveType = SocketDataType.Local;
            else
                _receiveType = SocketDataType.Network;
        }
    }

    private Dictionary<SocketDataType, BattleUserInfo> _battleUserInfo = new Dictionary<SocketDataType, BattleUserInfo>();

    private int _bossRandomSeed = 0;
    public int BossRandomSeed => _bossRandomSeed;

    public RequestBattleInitUserData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        var roomData = json["roomData"] as Dictionary<string, object>;
        var userInfoList = roomData["userInfoList"] as List<object>;
        foreach (var userInfo in userInfoList)
        {
            var battleUserInfo = new BattleUserInfo(userInfo);
            _battleUserInfo.Add(battleUserInfo.ReceiveType, battleUserInfo);
        }

        _bossRandomSeed = int.Parse(roomData["bossRandom"].ToString());
    }

    public BattleUserInfo GetUserInfo(SocketDataType receiveType)
    {
        if (_battleUserInfo.ContainsKey(receiveType) == false)
            return null;

        return _battleUserInfo[receiveType];
    }
}

public class RequestBattlePingData : BattleSocketRequestData
{
    private long _postPing = -1;
    public long PostPing => _postPing;

    private long _receivePing = -1;
    public long ReceivePing => _receivePing;

    private bool _successPing = false;
    public bool SuccessPing => _successPing;

    public RequestBattlePingData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        if (json["postTime"] == null || json["serverTime"] == null)
        {
            _successPing = false;
            return;
        }

        var postTime = new DateTime(long.Parse(json["postTime"].ToString()), DateTimeKind.Utc);
        var serverTime = DateTimeOffset.FromUnixTimeMilliseconds(long.Parse(json["serverTime"].ToString()));

        _postPing = (long)(serverTime - postTime).TotalMilliseconds;
        _receivePing = (long)(DateTimeOffset.UtcNow - serverTime).TotalMilliseconds;

        _successPing = true;
    }
}

public class RequestUpgradeData : BattleSocketRequestData
{
    private int _unitId = -1;
    public int UnitId => _unitId;

    private int _needSp = 0;
    public int NeedSp => _needSp;

    public RequestUpgradeData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        _unitId = int.Parse(json["unitId"].ToString());
        _needSp = int.Parse(json["needSp"].ToString());
    }
}

public class RequestBattleGiveUp : BattleSocketRequestData
{
    public RequestBattleGiveUp(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;
    }
}

public class RequestBattleTimeSyncData : BattleSocketRequestData
{
    private long _timeDistance = 0;
    public long TimeDistance => _timeDistance;

    public RequestBattleTimeSyncData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        _timeDistance = long.Parse(json["timeDistance"].ToString());
    }
}

public class RequestBattleResultData : BattleSocketRequestData
{
    private bool _isWin = false;
    public bool IsWin => _isWin;

    private int _updateTrophy = 0;
    public int UpdateTrophy => _updateTrophy;

    private int _getGold = 0;
    public int GetGold => _getGold;

    private int _getBoxId = -1;
    public int GetBoxId => _getBoxId;

    public RequestBattleResultData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        _isWin = bool.Parse(json["isWin"].ToString());
        _updateTrophy = int.Parse(json["updateTrophy"].ToString());
        _getGold = int.Parse(json["getGold"].ToString());
        _getBoxId = int.Parse(json["getBoxId"].ToString());
        //_getExp = float.Parse(json["getExp"].ToString());
    }
}

public class RequestBattleLIfeData : BattleSocketRequestData
{
    private int _life = 0;
    public int LIfe => _life;

    private int _dir = 0;
    public int Dir => _dir;

    public RequestBattleLIfeData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;
        _life = int.Parse(json["life"].ToString());
        _dir = int.Parse(json["dir"].ToString());
    }
}

public class RequestBattleMonsterData : BattleSocketRequestData
{
    private List<BattleMonsterData> _monsterDataList = new List<BattleMonsterData>();
    public BattleMonsterData[] MonsterDataList => _monsterDataList.ToArray();

    public RequestBattleMonsterData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        var dataList = json["battleUnitData"] as List<object>;
        foreach (Dictionary<string, object> jToken in dataList)
        {
            var index = int.Parse(jToken["index"].ToString());
            var unitId = int.Parse(jToken["unitId"].ToString());
            var hp = int.Parse(jToken["hp"].ToString());
            var speed = float.Parse(jToken["speed"].ToString());
            var power = int.Parse(jToken["power"].ToString());
            var spawnTime = float.Parse(jToken["spawnTime"].ToString());
            var sp = int.Parse(jToken["sp"].ToString());
            var monsterType = (BattleMonsterData.MonsterType)int.Parse(jToken["type"].ToString());

            var monsterData = new BattleMonsterData(index, unitId, hp, speed, power, spawnTime, sp, monsterType);
            _monsterDataList.Add(monsterData);
        }
    }
}

public class RequestBattleStageData : BattleSocketRequestData
{
    private List<BattleStageData> _stageDataList = new List<BattleStageData>();
    public BattleStageData[] StageDataList => _stageDataList.ToArray();

    public RequestBattleStageData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        var dataLIst = json["battleStageData"] as List<object>;
        foreach (Dictionary<string, object> jToken in dataLIst)
        {
            var index = int.Parse(jToken["index"].ToString());
            var hpScale = float.Parse(jToken["hpScale"].ToString());

            var stageData = new BattleStageData(index, hpScale);
            _stageDataList.Add(stageData);
        }
    }
}

public class RequestBattleWaveData : BattleSocketRequestData
{
    private List<BattleWaveData> _waveDataList = new List<BattleWaveData>();
    public BattleWaveData[] WaveDataList => _waveDataList.ToArray();

    public RequestBattleWaveData(Socket s, Packet p, params object[] a) : base(s, p ,a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        var dataList = json["battleWaveData"] as List<object>;
        foreach (Dictionary<string, object> jToken in dataList)
        {
            var index = int.Parse(jToken["index"].ToString());
            var time = float.Parse(jToken["time"].ToString());
            var spawnScale = float.Parse(jToken["spawnScale"].ToString());
            var hpScale = float.Parse(jToken["hpScale"].ToString());

            var waveData = new BattleWaveData(index, time, spawnScale, hpScale);
            _waveDataList.Add(waveData);
        }
    }
}

public class RequestBattleBossSyncData : BattleSocketRequestData
{
    private bool _syncClear = false;
    public bool SyncClear => _syncClear;

    public RequestBattleBossSyncData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;

        _syncClear = bool.Parse(json["syncClear"].ToString());
    }
}

public class RequestBattleLeaveSyncData : BattleSocketRequestData
{
    public RequestBattleLeaveSyncData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;
    }
}

public class RequestBattleDisconnectData : BattleSocketRequestData
{
    public RequestBattleDisconnectData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;
    }
}

public class RequestBattleRequestReadyData : BattleSocketRequestData
{
    public RequestBattleRequestReadyData(Socket s, Packet p, params object[] a) : base(s, p, a) { }

    protected override void ParseData(Socket s, Packet p, params object[] a)
    {
        base.ParseData(s, p, a);
        var json = a[0] as Dictionary<string, object>;
    }
}

public class BattleSocket : GameSocketReceiver
{
    [SerializeField]
    private BattleLogSystem _logSystem = null;

    [SerializeField]
    private BattleTimeSyncer _timeSyncer = null;

    public event UnityAction<RequestUnitData> CharacterEvent = null;
    public event UnityAction<RequestUnitData> SpawnCharacterEvent = null;
    public event UnityAction<RequestEmoticonData> EmoticonEvent = null;
    //public event UnityAction<RequestMergeData> MergeEvent = null;
    public event UnityAction<RequestReadyData> ReadyEvent = null;
    public event UnityAction<RequestBattleInitUserData> BattleInitUserDataEvent = null;
    public event UnityAction<RequestUpgradeData> UpgradeEvent = null;
    public event UnityAction<long> UpdatePingEvent = null;
    public event UnityAction<RequestBattleGiveUp> OnGiveUpEvent = null;
    public event UnityAction<RequestBattleTimeSyncData> OnTimeSyncEvent = null;
    public event UnityAction<RequestMergeUpgradeData> MergeUpgradeEvent = null;
    public event UnityAction<RequestCopyUnitData> CopyUnitEvent = null;
    public event UnityAction InitEvent = null;
    public event UnityAction<RequestBattleResultData> BattleResultEvent = null;
    public event UnityAction<RequestBattleWaveData> UpdateBattleWaveEvent = null;
    public event UnityAction<RequestBattleStageData> UpdateBattleStageEvent = null;
    public event UnityAction<RequestBattleMonsterData> UpdateBattleMonsterEvent = null;
    public event UnityAction<RequestBattleBossSyncData> BattleBossSyncEvent = null;
    public event UnityAction<RequestBattleLeaveSyncData> BattleLeaveSyncEvent = null;
    public event UnityAction<RequestBattleDisconnectData> BattleDisconnectEvent = null;
    public event UnityAction<RequestMergeCreateData> MergeCreateEvent = null;
    public event UnityAction<RequestBattleLIfeData> BattleLifeEvent = null;
    public event UnityAction<RequestBattleRequestReadyData> RequestReadyEvent = null;

    [SerializeField]
    private long _mDelay = 500;
    public long MDelay => _mDelay;

    protected override void OnLoginComplate()
    {
        base.OnLoginComplate();

        On("createCharacter", (s, p, a) =>
        {
            RequestUnitData unitData = new RequestUnitData(s, p, a);

            CharacterEvent?.Invoke(unitData);
        });

        On("spawnCharacter", (s, p, a) =>
        {
            RequestUnitData unitData = new RequestUnitData(s, p, a);

            SpawnCharacterEvent?.Invoke(unitData);
        });

        On("onEmoticon", (s, p, a) =>
        {
            RequestEmoticonData emoticonData = new RequestEmoticonData(s, p, a);

            EmoticonEvent?.Invoke(emoticonData);
        });

        On("battleTimeSync", (s, p, a) =>
        {
            var timeSyncData = new RequestBattleTimeSyncData(s, p, a);
            OnTimeSyncEvent?.Invoke(timeSyncData);
        });

        //On("onMerge", (s, p, a) =>
        //{
        //    RequestMergeData mergeData = new RequestMergeData(s, p, a);

        //    MergeEvent?.Invoke(mergeData);
        //});

        On("onMergeUpgrade", (s, p, a) =>
        {
            var mergeUpgrade = new RequestMergeUpgradeData(s, p, a);

            MergeUpgradeEvent?.Invoke(mergeUpgrade);
        });

        On("onMergeCreate", (s, p, a) =>
        {
            var mergeCreate = new RequestMergeCreateData(s, p, a);

            MergeCreateEvent?.Invoke(mergeCreate);
        });

        On("battleLife", (s, p, a) =>
        {
            var battleLife = new RequestBattleLIfeData(s, p, a);

            BattleLifeEvent?.Invoke(battleLife);
        });

        On("onCopyUnit", (s, p, a) =>
        {
            var copyUnitData = new RequestCopyUnitData(s, p, a);

            CopyUnitEvent?.Invoke(copyUnitData);
        });

        On("playReady", (s, p, a) =>
        {
            var readyData = new RequestReadyData(s, p, a);
            ReadyEvent?.Invoke(readyData);
            //var json = JObject.Parse(data);
            //_isReady = bool.Parse(json["isReady"].ToString());
        });

        On("onInitUserData", (s, p, a) =>
        {
            Debug.Log("onInitUserData : " + p.Payload);

            RequestBattleInitUserData battleInitData = new RequestBattleInitUserData(s, p, a);
            if (battleInitData == null)
            {
                Debug.Log("BattleInitData is Null!");
                return;
            }

            BattleInitUserDataEvent?.Invoke(battleInitData);
        });

        On("requestReady", (s, p, a) =>
        {
            var requestReadyData = new RequestBattleRequestReadyData(s, p, a);
            RequestReadyEvent?.Invoke(requestReadyData);
        });

        On("updateBattleWaveData", (s, p, a) =>
        {
            var requestWaveData = new RequestBattleWaveData(s, p, a);
            UpdateBattleWaveEvent?.Invoke(requestWaveData);
        });

        On("updateBattleStageData", (s, p, a) =>
        {
            var requestStageData = new RequestBattleStageData(s, p, a);
            UpdateBattleStageEvent?.Invoke(requestStageData);
        });

        On("updateBattleUnitData", (s, p, a) =>
        {
            var requestMonsterData = new RequestBattleMonsterData(s, p, a);
            UpdateBattleMonsterEvent?.Invoke(requestMonsterData);
        });

        On("onUpgrade", (s, p, a) =>
        {
            RequestUpgradeData upgradeData = new RequestUpgradeData(s, p, a);

            UpgradeEvent?.Invoke(upgradeData);
        });

        On("giveUp", (s, p, a) =>
        {
            var giveUp = new RequestBattleGiveUp(s, p, a);

            OnGiveUpEvent?.Invoke(giveUp);
        });

        On("battleResult", (s, p, a) =>
        {
            RequestBattleResultData battleResultData = new RequestBattleResultData(s, p, a);

            BattleResultEvent?.Invoke(battleResultData);
        });

        On("bossClearSync", (s, p, a) =>
        {
            var syncData = new RequestBattleBossSyncData(s, p, a);

            BattleBossSyncEvent?.Invoke(syncData);
        });

        On("battleLeaveSync", (s, p, a) =>
        {
            var battleLeaveSyncData = new RequestBattleLeaveSyncData(s, p, a);

            BattleLeaveSyncEvent?.Invoke(battleLeaveSyncData);
        });

        On("disconnectBattleRoom", (s, p, a) =>
        {
            var battleDisconnectData = new RequestBattleDisconnectData(s, p, a);
            BattleDisconnectEvent?.Invoke(battleDisconnectData);
        });
    }

    public void EmitPlayReady(bool isReady)
    {
        JObject jsonObject = new JObject();
        jsonObject["roomId"] = SocketMgr.RoomId;
        jsonObject["playReady"] = isReady;
        Emit("playReady", jsonObject);
    }

    public void EmitInit()
    {
        var json = new JObject();
        json["roomId"] = SocketMgr.RoomId;
        json["isInit"] = true;

        Emit("initBattle", json);
    }

    public void EmitBattleResult(BattleInitUserData localUserInfo, BattleInitUserData defaultUserInfo, bool isWin)
    {
        var json = new JObject();
        json["isWin"] = isWin;
        json["localUserInfo"] = JsonConvert.SerializeObject(localUserInfo);
        json["defaultUserInfo"] = JsonConvert.SerializeObject(defaultUserInfo);

        Emit("battleResult", json);
    }

    public void EmitInitUserData()
    {
        JObject jsonObject = new JObject();
        jsonObject["roomId"] = SocketMgr.RoomId;

        Emit("onInitUserData", jsonObject);
    }

    public void EmitEmoticon(int emoticonId)
    {
        JObject json = new JObject();
        json["roomId"] = SocketMgr.RoomId;
        json["emoticonId"] = emoticonId;

        Emit("onEmoticon", json);
    }

    public void CreateUnit(int unitId, int grade, int slotIndex)
    {
        JObject json = new JObject();
        json["roomId"] = SocketMgr.RoomId;
        json["unitId"] = unitId;
        json["grade"] = grade;
        json["slotIndex"] = slotIndex;

        Emit("createCharacter", json);
    }

    public void SpawnCharacterUnit(int unitId, int grade, int slotIndex)
    {
        JObject json = new JObject();
        json["roomId"] = SocketMgr.RoomId;
        json["unitId"] = unitId;
        json["grade"] = grade;
        json["slotIndex"] = slotIndex;

        Emit("spawnCharacter", json);
    }

    public void OnUpgrade(int unitId, int needSp)
    {
        JObject json = new JObject();
        json["unitId"] = unitId;
        json["roomId"] = SocketMgr.RoomId;
        json["needSp"] = needSp;

        Emit("onUpgrade", json);
    }

    //public void OnMergeUnit(GameUnit target, GameUnit merge)
    //{
    //    JObject json = new JObject();
    //    json["roomId"] = SocketMgr.RoomId;
    //    json["targetSlotId"] = target.CurrTileIndex;
    //    json["mergeSlotId"] = merge.CurrTileIndex;

    //    Emit("onMerge", json);
    //}

    public void OnMergeUpgrade(GameUnit target, GameUnit merge, int unitId, int grade)
    {
        var json = new JObject();
        json["roomId"] = SocketMgr.RoomId;
        json["targetSlotId"] = target.CurrTileIndex;
        json["mergeSlotId"] = merge.CurrTileIndex;
        json["unitId"] = unitId;
        json["grade"] = grade;

        Emit("onMergeUpgrade", json);
    }

    public void OnMergeCreate(GameUnit target, GameUnit merge, int unitId, int grade)
    {
        var json = new JObject();
        json["roomId"] = SocketMgr.RoomId;
        json["targetSlotId"] = target.CurrTileIndex;
        json["mergeSlotId"] = merge.CurrTileIndex;
        json["unitId"] = unitId;
        json["grade"] = grade;

        Emit("onMergeCreate", json);
    }

    public void OnCopyUnit(GameUnit target, GameUnit merge, int unitId, int grade)
    {
        var json = new JObject();
        json["roomId"] = SocketMgr.RoomId;
        json["targetSlotId"] = target.CurrTileIndex;
        json["mergeSlotId"] = merge.CurrTileIndex;
        json["unitId"] = unitId;
        json["grade"] = grade;

        Emit("onCopyUnit", json);
    }

    public void EmitUpdateBattleWaveData()
    {
        var json = new JObject();
        Emit("updateBattleWaveData", json);
    }

    public void EmitUpdateBattleStageData()
    {
        var json = new JObject();
        Emit("updateBattleStageData", json);
    }

    public void EmitUpdateBattleUnitData()
    {
        var json = new JObject();
        Emit("updateBattleUnitData", json);
    }

    public void EmitRequestReady()
    {
        var json = new JObject();
        json["roomId"] = SocketMgr.RoomId;

        Emit("requestReady", json);
    }

    public void EmitLeaveBattle()
    {
        Emit("leaveBattle");
    }

    public void EmitBossClearSync(bool isClear)
    {
        var json = new JObject();
        json["isClear"] = isClear;
        json["roomId"] = SocketMgr.RoomId;

        Emit("bossClearSync", json);
    }

    public void EmitBattleLeaveSync()
    {
        var json = new JObject();
        json["roomId"] = SocketMgr.RoomId;

        Emit("battleLeaveSync", json);
    }

    public void EmitGiveUp()
    {
        JObject json = new JObject();
        json["roomId"] = SocketMgr.RoomId;
        Emit("giveUp", json);
    }

    public void EmitBattlePing()
    {
        Emit("battlePing", new JObject());
    }

    public void EmitBattleLife(int life, int dir)
    {
        var json = new JObject();
        json["life"] = life;
        json["dir"] = dir;
        json["roomId"] = SocketMgr.RoomId;
        Emit("battleLife", json);
    }

    public void EmitBattleTimeSync()
    {
        var json = new JObject();
        var nowTime = (long)(DateTimeOffset.UtcNow - DateTimeOffset.FromUnixTimeMilliseconds(0)).TotalMilliseconds;
        var clientTime = nowTime + _timeSyncer.TotalSync;
        json["clientTime"] = clientTime;

        Emit("battleTimeSync", json);
    }

    public override void On(string eventName, BestHTTP.SocketIO.Events.SocketIOCallback callback)
    {
        base.On(eventName, (s, p, a) =>
        {
            StartCoroutine(OnSocketBattle(callback, s, p, a));
        });
    }

    public override void Emit(string eventName, JObject json)
    {
        if (json == null)
            json = new JObject();

        long postTime = DateTimeOffset.UtcNow.UtcTicks;
        json["postTime"] = postTime;

        base.Emit(eventName, json);
    }

    private IEnumerator OnSocketBattle(BestHTTP.SocketIO.Events.SocketIOCallback callback, BestHTTP.SocketIO.Socket socket, BestHTTP.SocketIO.Packet packet, params object[] args)
    {
        Debug.Log(packet.EventName);

        var logList = new List<LogData>();
        logList.Add(new LogData("eventData", packet.Payload));

        bool useDelay = false;

        var json = args[0] as Dictionary<string, object>;
        useDelay = json != null && json.ContainsKey("serverTime") == true ? true : false;
        logList.Add(new LogData("useDelay", useDelay));

        if (useDelay == true)
        {
            var serverTime = long.Parse(json["serverTime"].ToString());
            var nowTime = (long)(DateTimeOffset.UtcNow - DateTimeOffset.FromUnixTimeMilliseconds(0)).TotalMilliseconds;

            var timeDistance = nowTime - serverTime;
            var timeDir = 1.0f;
            if (timeDistance > 0)
                timeDir = timeDistance / (long)Mathf.Abs(timeDistance);

            var startTime = serverTime + (_mDelay + (_timeSyncer.TotalSync * timeDir));
            var result = startTime - nowTime;

            logList.Add(new LogData("serverTime", serverTime));
            logList.Add(new LogData("nowTime", nowTime));
            logList.Add(new LogData("startTime", startTime));
            logList.Add(new LogData("TimeDistance", timeDistance));
            logList.Add(new LogData("DefaultDelay", _mDelay));
            logList.Add(new LogData("SyncTime", _timeSyncer.TotalSync));

            float delay = (float)(result * TimeSpan.TicksPerMillisecond) / (float)TimeSpan.TicksPerSecond;
            Debug.Log(packet.EventName + " : " + delay.ToString());
            logList.Add(new LogData("delay", delay));

            //if (delay > 0.0f)
            //{
            //    System.Timers.Timer timer = new System.Timers.Timer();
            //    timer.Interval = 1;
            //    timer.Elapsed += new System.Timers.ElapsedEventHandler((sender, e) =>
            //    {
            //        _logSystem.SendLog(BattleLogSystem.BattleLogType.SocketEvent, packet.EventName, logList.ToArray());
            //        callback?.Invoke(socket, packet, args);
            //    });
            //    yield break;
            //}
            while (delay > 0.0f)
            {
                delay -= Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }
        }

        _logSystem.SendLog(BattleLogSystem.BattleLogType.SocketEvent, packet.EventName, logList.ToArray());
        callback?.Invoke(socket, packet, args);
    }
}
