﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.Events;
using System;
using Newtonsoft.Json.Linq;

public class MatchSocket : GameSocketReceiver
{
    [SerializeField, ReadOnly]
    private bool _isMatching = false;
    public bool IsMatching => _isMatching;

    [SerializeField, ReadOnly]
    private string _battleRoomId = null;
    public string BattleRoomId => _battleRoomId;

    public UnityEvent JoinBattleRoomEvent = null;

    public UnityEvent MatchingEvent = null;
    public UnityEvent MatchOutEvent = null;
    public UnityEvent NeedUpdateEvent = null;

    private EightAssetMgr _assetMgr = null;

    private bool _eventWait = false;
    public bool EventWait => _eventWait;

    private void OnApplicationQuit()
    {
        Emit("matchOut");
    }

    protected override void OnDisable()
    {
        base.OnDisable();

    }

    protected override void OnLoginComplate()
    {
        base.OnLoginComplate();

        if (_assetMgr == null) _assetMgr = EightUtil.GetCore<EightAssetMgr>();
        _assetMgr.AskDownload((downloadSize) =>
        {
            if (downloadSize <= 0)
            {
                SetSockets();
                return;
            }
            NeedUpdateEvent?.Invoke();
        });
    }

    private void SetSockets()
    {
        On("JoinBattleRoom", (s, p, a) =>
        {
            JoinBattleRoomEvent?.Invoke();
            //Emit("matchOut");
        });

        On("matching", (s, p, a) =>
        {
            if (_isMatching == true)
                return;

            Debug.Log(p.Payload);

            _isMatching = true;
            _eventWait = false;
            MatchingEvent?.Invoke();
        });

        On("joinMatching", (s, p, a) =>
        {
            var json = a[0] as Dictionary<string, object>;
            _battleRoomId = json["roomToken"] as string;
            SocketMgr.RoomId = _battleRoomId;
            Emit("joinBattleRoom", _battleRoomId);
            Debug.Log("joinBattleRoom : " + p.Payload);
        });

        //GameSocket.On("matching", (string data) =>
        //{
        //    if (_isMatching == true)
        //        return;

        //    Debug.Log(data);

        //    _isMatching = true;

        //    MatchingEvent?.Invoke();
        //});

        On("matchOut", (s, p, a) =>
        {
            if (_isMatching == false)
                return;

            Debug.Log(p.Payload);

            _isMatching = false;
            _eventWait = false;

            MatchOutEvent?.Invoke();
        });
    }

    public void Matching()
    {
        if (IsLogin == false || IsMatching == true || _eventWait == true)
        {
            Debug.Log("Not Start Matching!" +
                "Now Playing Matcing");

            return;
        }


        var json = new JObject();
        json["token"] = EightUtil.GetCore<GameUserMgr>().UserInfo.Token;
        json["matchType"] = "battle";
        Emit("matching", json);
        //GameSocket.Emit("matching");
    }

#if Bot_System
    public void BotMatching(string poolName)
    {
        if (IsLogin == false || IsMatching == true || _eventWait == true)
        {
            Debug.Log("Not Start Matching!" +
                "Now Playing Matcing");

            return;
        }


        var json = new JObject();
        json["token"] = EightUtil.GetCore<GameUserMgr>().UserInfo.Token;
        json["matchType"] = poolName;
        Emit("botMatching", json);
        //GameSocket.Emit("matching");
    }
#endif

    public override void Emit(string eventName, string jsonData = null)
    {
        _eventWait = true;
        base.Emit(eventName, jsonData);
    }

    protected override void OnReconnect()
    {
        base.OnReconnect();

        _eventWait = false;
    }

    //public override void On(string eventName, Action<string> callback)
    //{
    //    base.On(eventName, callback);
    //    _eventWait = false;
    //}

    //public override void On(string eventName, Action callback)
    //{
    //    base.On(eventName, callback);
    //    _eventWait = false;
    //}

    public void MatchOut()
    {
        if (IsLogin == false || IsMatching == false || _eventWait == true)
            return;

        Emit("matchOut");
    }
}
