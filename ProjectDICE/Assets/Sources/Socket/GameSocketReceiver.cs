﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using BestHTTP.SocketIO;

public class GameSocketReceiver : EightReceiver
{
    private static GameSocketMgr _socketMgr = null;
    public GameSocketMgr SocketMgr => GetGameSocketMgr();

    private List<string> _eventNameList = new List<string>();

    protected static Socket GameSocket
    {
        get
        {
            if (_socketMgr == null)
                return null;

            return _socketMgr.GameSocket;
        }
    }

    public static bool IsLogin
    {
        get
        {
            if (_socketMgr == null)
                return false;

            return _socketMgr.IsLogin;
        }
    }

    [SerializeField, ReadOnly]
    private bool _isComplateLogin = false;
    public bool IsComplateLogin => _isComplateLogin;

    private static GameSocketMgr GetGameSocketMgr()
    {
        if (EightUtil.IsLoadCompleteDevice == false)
            return null;

        if (_socketMgr == null)
            _socketMgr = EightUtil.GetCore<GameSocketMgr>();

        return _socketMgr;
    }

    protected virtual void Awake()
    {
        SetupComplete();
    }

    protected override void OnLoadCompleteDevice()
    {
        base.OnLoadCompleteDevice();

        SetupLoginCheck();
    }

    protected void SetupLoginCheck()
    {
        if (SocketMgr == null)
            return;

        SocketMgr.ComplateLogin += OnLoginComplate;
    }

    public virtual void On(string eventName, Action callback)
    {
        GameSocket.On(eventName, (s, p, a) => {
            Debug.Log("On : " + eventName);
            callback?.Invoke();
        });
        _eventNameList.Add(eventName);
    }

    public virtual void On(string eventName, BestHTTP.SocketIO.Events.SocketIOCallback callback)
    {
        GameSocket.On(eventName, (s, p, a) =>
        {
#if UNITY_EDITOR
            Debug.Log("On : " + eventName + "\n" +
                "Data : " + p.Payload);
#endif
            callback?.Invoke(s, p, a);
        });
        _eventNameList.Add(eventName);
    }

    public virtual void Off(string eventName)
    {
        for(int i = _eventNameList.Count - 1; i >= 0; --i)
        {
            if (_eventNameList[i] == eventName)
                _eventNameList.RemoveAt(i);
        }

        GameSocket.Off(eventName);
    }

    public virtual void Emit(string eventName, string jsonData = null)
    {
        Debug.Log("Emit : " + eventName);
        GameSocket?.Emit(eventName, jsonData);
    }

    public virtual void Emit(string eventName, JObject json)
    {
        Debug.Log("Emit : " + eventName);
        Emit(eventName, JsonConvert.SerializeObject(json));
    }

    protected virtual void OnLoginComplate()
    {
        _isComplateLogin = true;
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        SocketMgr.OnReconnectEvent += OnReconnect;
        SocketMgr.OnDisconnectEvent += OnDisconnect;
    }

    protected virtual void OnReconnect()
    {

    }

    protected virtual void OnDisconnect()
    {

    }

    protected override void OnDisable()
    {
        base.OnDisable();

        ReleaseSocket();
    }

    public virtual bool ReleaseSocket()
    {
        if (SocketMgr == null || SocketMgr.IsLogin == false)
            return false;

        for (int i = _eventNameList.Count - 1; i >= 0; --i)
        {
            Off(_eventNameList[i]);
        }

        SocketMgr.OnReconnectEvent -= OnReconnect;
        SocketMgr.OnDisconnectEvent -= OnDisconnect;

        return true;
    }
}
