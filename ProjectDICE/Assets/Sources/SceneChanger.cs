﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class SceneChanger : MonoBehaviour
{
    [SerializeField]
    private SCENE_TYPE _changeType = SCENE_TYPE.NULL;
    public SCENE_TYPE ChangeType { get { return _changeType; } set { _changeType = value;} }

    [SerializeField]
    private bool _isWait = false;

    [SerializeField]
    private LoadingCameraUI.LoadingState _loadingState = LoadingCameraUI.LoadingState.NONE;
    
    public void ChangeScene()
    {
        if (EightUtil.IsLoadCompleteDevice == false)
            return;

        LoadingCameraUI.SetState(_loadingState);
        EightUtil.GetCore<EightSceneMgr>().ChangeScene(_changeType);
        if(_isWait) EightUtil.GetCore<EightMsgMgr>().OnEightMsg(EIGHT_MSG.ON_FADE_PAUSE, new EightMsgContent(1));
    }
}
