﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using EightWork;
using UnityEngine.UI;

public class BattleThemeSystem : MonoBehaviour
{
    [SerializeField, ReadOnly]
    private BattleThemeData _themeData = null;
    public BattleThemeData ThemeData => _themeData;

    [System.Serializable]
    private class BattleMapData
    {
        [SerializeField]
        private UserTilePool _slotTileList = null;
        public UserTilePool SlotTileList => _slotTileList;

        [SerializeField]
        private MonsterTilePool _roadTileList = null;
        public MonsterTilePool RoadTileList => _roadTileList;
    }

    private enum BattleTeam
    {
        MyTeam,
        Ather,
    }

    [System.Serializable]
    private class BattleTeamMapList : SerializableDictionaryBase<BattleTeam, BattleMapData> { }

    [SerializeField]
    private Image _background = null;

    [SerializeField]
    private BattleTeamMapList _teamMapList = null;

    private void Awake()
    {
        OnLoadBattleTheme();
    }

    public void OnLoadBattleTheme()
    {
        _themeData = EightUtil.GetCore<BattleThemeMgr>()?.GetData(-1);
        UpdateTheme();
    }

    public void UpdateTheme()
    {
        if (_themeData == null)
            return;

        _background.sprite = _themeData.GetTileSprite(BattleThemeData.BattleResourceType.Background);
        foreach (var mapData in _teamMapList.Values)
        {
            mapData.RoadTileList.SpawnTile.UpdateSprite(_themeData.GetTileSprite(BattleThemeData.BattleResourceType.Spawn));
            mapData.RoadTileList.GoalTile.UpdateSprite(_themeData.GetTileSprite(BattleThemeData.BattleResourceType.Goal));
            foreach (var tile in mapData.RoadTileList.TileList)
            {
                tile.UpdateSprite(_themeData.GetTileSprite(BattleThemeData.BattleResourceType.Road));
            }

            foreach (var tile in mapData.SlotTileList.TileList)
            {
                tile.UpdateSprite(_themeData.GetTileSprite(BattleThemeData.BattleResourceType.CharacaterSlot));
            }
        }
    }
}
