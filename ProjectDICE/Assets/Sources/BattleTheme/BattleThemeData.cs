﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(fileName = "New BattleThemeData", menuName = "Data/BattleThemeData", order = 10)]
public class BattleThemeData : ScriptableObject
{
    public enum BattleResourceType
    {
        CharacaterSlot,
        Road,
        Spawn,
        Goal,
        Background,
    }

    [SerializeField]
    private int _themeId = -1;
    public int ThemeId => _themeId;

    [SerializeField]
    private Sprite _selectTileEffect = null;
    public Sprite SelectTileEffect => _selectTileEffect;

    [SerializeField]
    private Sprite _themePreview = null;
    public Sprite ThemePreview => _themePreview;

    [System.Serializable]
    private class BattleTileSpriteList : SerializableDictionaryBase<BattleResourceType, Sprite> { }

    [SerializeField]
    private BattleTileSpriteList _tileList = null;
    public Dictionary<BattleResourceType, Sprite> TileList => _tileList.Clone();

    public Sprite GetTileSprite(BattleResourceType type)
    {
        if (_tileList.ContainsKey(type) == false)
            return null;

        return _tileList[type];
    }
}
