﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.AddressableAssets;

public class BattleThemeMgr : EightWork.Core.EightCore
{
    [SerializeField]
    private AssetLabelReference _lable = null;

    private Dictionary<int, BattleThemeData> _themeDataList = new Dictionary<int, BattleThemeData>();

    [SerializeField, ReadOnly]
    private bool _isSuccessLoad = false;
    public bool IsSuccessLoad => _isSuccessLoad;

    public override void InitializedCore()
    {
    }

    public void LoadData()
    {
        if (_isSuccessLoad == true)
            return;

        var loadHandle = Addressables.LoadAssetsAsync<BattleThemeData>(_lable.labelString, null);
        loadHandle.Completed += (result) =>
        {
            var dataList = result.Result;
            if (dataList == null)
            {
                Debug.Log("Fail Load BattleThemeData");
                GameDataMgr.IsLoadFailed = true;
                return;
            }

            foreach (var data in dataList)
            {
                int dataId = data.ThemeId;
                _themeDataList[dataId] = data;
            }

            _isSuccessLoad = true;
        };
    }

    public BattleThemeData GetData(int id)
    {
        if (_themeDataList.ContainsKey(id) == false)
            return null;

        return _themeDataList[id];
    }
}
