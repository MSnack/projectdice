﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Raycast2DReceiver
{
    private UnityAction<Raycast2DObject> _raycastEvent = null;
    public event UnityAction<Raycast2DObject> RaycastEvent
    {
        add => _raycastEvent += value;
        remove => _raycastEvent -= value;
    }

    ~Raycast2DReceiver()
    {
        _raycastEvent = null;
    }

    protected abstract Raycast2DObject OnSimulation();

    public RaycastObject Raycast<RaycastObject>(bool isCallback = false) where RaycastObject : Raycast2DObject
    {
        var raycastHit = OnSimulation() as RaycastObject;
        if(isCallback == true)
            _raycastEvent?.Invoke(raycastHit);

        return raycastHit;
    }
}
