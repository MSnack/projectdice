﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Raycast2DObject
{
    public Raycast2DObject(RaycastHit2D raycast)
    {
        OnRaycast(ref raycast);
    }

    protected abstract void OnRaycast(ref RaycastHit2D raycast);
}
