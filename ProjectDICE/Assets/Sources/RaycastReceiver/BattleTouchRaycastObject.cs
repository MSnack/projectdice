﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleTouchRaycastObject : Raycast2DObject
{
    private UserTile _hitUserTile = null;
    public UserTile HitUserTile => _hitUserTile;

    public BattleTouchRaycastObject(RaycastHit2D raycast) : base(raycast) { }

    ~BattleTouchRaycastObject()
    {
        _hitUserTile = null;
    }

    protected override void OnRaycast(ref RaycastHit2D raycast)
    {
        if (raycast.collider == null)
            return;

        var tile = raycast.collider.gameObject.GetComponent<UserTile>();
        if (tile == null)
            return;

        _hitUserTile = tile;
    }
}
