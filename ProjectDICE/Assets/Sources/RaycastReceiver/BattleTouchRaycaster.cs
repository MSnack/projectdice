﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using EightWork;

[System.Serializable]
public class BattleTouchRaycaster : Raycast2DReceiver
{
    [SerializeField]
    private float _distance = 0.0f;
    public float Distance { get => _distance; set => _distance = value; }

    protected override Raycast2DObject OnSimulation()
    {
        if (IsPointerOverGameObject() == true)
            return null;

        var camera = EightUtil.GetCore<EightCameraMgr>().EightCamera;
        Vector2 pos = camera.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(pos, camera.transform.forward, Distance);
        var raycastObject = new BattleTouchRaycastObject(hit);
        return raycastObject;
    }

    private bool IsPointerOverGameObject()
    {
        switch (Application.platform)
        {
            case RuntimePlatform.Android: return TouchUIBlockCheck();
            case RuntimePlatform.IPhonePlayer: return TouchUIBlockCheck();
            default: return ClickBlockCheck();
        }
    }

    private bool TouchUIBlockCheck()
    {
        if (Input.touchCount <= 0)
            return false;

        return EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
    }

    public bool ClickBlockCheck()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }
}
