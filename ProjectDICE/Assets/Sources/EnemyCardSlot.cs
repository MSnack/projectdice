﻿using System.Collections;
using System.Collections.Generic;
using EightWork;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCardSlot : MonoBehaviour
{
    [SerializeField]
    private Image _frameImage = null;

    [SerializeField]
    private Image _charactorImage = null;

    [SerializeField]
    private Text _levelText;

    [SerializeField]
    private string _levelFormat;

    public void ChangeFrame(CharacterUnitData data)
    {
        if (_frameImage == null)
            return;

        var sprite = LobbyCardUtil.GetStarSprite(data.Grade - 1);
        _frameImage.sprite = sprite;
    }

    public void ChangeCharactor(CharacterUnitData data)
    {
        if (_charactorImage == null)
            return;

        _charactorImage.sprite = data.UnitPreviewSprite;
    }

    public void ChangeLevel(int level)
    {
        if (_levelText == null)
            return;

        _levelText.text = string.Format(_levelFormat, level);
    }

    public void Init(BattleCardData data)
    {
        var unitData = EightUtil.GetCore<GameUnitDataMgr>().GetCharacaterData(data.UnitID);
        if (unitData == null)
            return;

        ChangeFrame(unitData);
        ChangeCharactor(unitData);
        ChangeLevel(0);

        data.UpdateEvent += UpdateInfo;
    }

    public void UpdateInfo(BattleCardData data)
    {
        int level = int.Parse(data.Reinforce.ToString());
        ChangeLevel(level);
    }
}
