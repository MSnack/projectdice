﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EightWork;
using UnityEngine.Events;

public class DamageFontObject : MonoBehaviour
{
    [SerializeField]
    private Image _frontIcon = null;
    public Image FrontIcon => _frontIcon;

    [SerializeField]
    private RectTransform _damageFontPool = null;
    public RectTransform DamageFontPool => _damageFontPool;

    [SerializeField, ReadOnly]
    private float _lifeTime = 0.0f;
    public float LifeTime => _lifeTime;

    [SerializeField, ReadOnly]
    private float _upSpeed = 0.0f;
    public float UpSpeed => _upSpeed;

    [SerializeField, ReadOnly]
    private bool _isDraw = false;
    public bool IsDraw => _isDraw;

    private Coroutine _routine = null;

    public event UnityAction<DamageFontObject> DrawEndEvent = null;

    private float DamageFontWidth
    {
        get
        {
            var width = 0.0f;
            var childCount = _damageFontPool.childCount;
            for (int i = 0; i < childCount; ++i)
            {
                var childRectTransform = _damageFontPool.GetChild(i).GetComponent<RectTransform>();
                if (childRectTransform == null || childRectTransform.gameObject.activeSelf == false)
                    continue;

                width += childRectTransform.sizeDelta.x;
            }

            return width;
        }
    }

    public Image[] DetachFont()
    {
        var fontList = new List<Image>();
        int childCount = _damageFontPool.childCount;
        for (int i = 0; i < childCount; ++i)
        {
            var childImage = _damageFontPool.GetChild(i).GetComponent<Image>();
            fontList.Add(childImage);
        }
        _damageFontPool.DetachChildren();

        return fontList.ToArray();
    }

    public void OnInitDamageFont(Image[] fontList, DamageFontMaterialData.FrontIcon fontIconData)
    {
        foreach (var font in fontList)
        {
            font.transform.SetParent(_damageFontPool);
        }

        if (fontIconData.Icon == null)
        {
            _frontIcon.gameObject.SetActive(false);
            return;
        }

        var layoutGroup = _damageFontPool.GetComponent<HorizontalLayoutGroup>();
        _frontIcon.gameObject.SetActive(true);
        _frontIcon.sprite = fontIconData.Icon;
        _frontIcon.SetNativeSize();
        var width = DamageFontWidth;
        var posX = -width * 0.5f - (layoutGroup.spacing * 0.5f * fontList.Length);
        _frontIcon.transform.localPosition = new Vector3(posX + fontIconData.Offset.x, fontIconData.Offset.y, 0.0f);
    }

    public void OnDraw(Vector2 startPos, float upSpeed, float lifeTime)
    {
        if (_isDraw == true)
            return;

        _upSpeed = upSpeed;
        _lifeTime = lifeTime;

        transform.position = startPos;
        StartDrawRoutine();
        _isDraw = true;
    }

    private void StartDrawRoutine()
    {
        if (_routine != null)
            return;

        _routine = StartCoroutine(DrawRoutine());
    }

    private IEnumerator DrawRoutine()
    {
        while (_lifeTime > 0.0f)
        {
            _lifeTime -= Time.deltaTime;
            var pos = transform.position;
            pos.y += _upSpeed * Time.deltaTime;
            transform.position = pos;
            yield return null;
        }
        DrawEndEvent?.Invoke(this);
    }

    private void StopDrawRoutine()
    {
        if (_routine == null)
            return;

        StopCoroutine(_routine);
        _routine = null;
    }

    public void Release()
    {
        StopDrawRoutine();

        _upSpeed = 0.0f;
        _lifeTime = 0.0f;
        _isDraw = false;

        _frontIcon.sprite = null;
        _frontIcon.gameObject.SetActive(false);

        DrawEndEvent = null;
    }
}
