﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;

public class GameDamageFontMgr : EightWork.Core.EightCore
{
    [System.Serializable]
    private class DamageFontDrawReference : AssetReferenceT<DamageFontDrawData>
    {
        public DamageFontDrawReference(string guid) : base(guid) { }
    }

    [SerializeField]
    private DamageFontDrawReference _drawDataRefernece = null;

    [SerializeField, ReadOnly]
    private DamageFontDrawData _dataAsset = null;
    public DamageFontDrawData DataAsset => _dataAsset;

    [SerializeField]
    private DamageFontObjectPool _damageFontObjectPool = null;

    [SerializeField]
    private DamageFontSlotPool _damageFontSlotPool = null;

    [SerializeField, ReadOnly]
    private bool _isLoadDrawData = false;

    public bool IsLoadComplete => _damageFontObjectPool.IsLoad == true && _isLoadDrawData == true;

    private List<DamageFontObject> _useFontList = new List<DamageFontObject>();

    public override void InitializedCore()
    {
    }

    public void LoadData()
    {
        LoadDrawData();
        _damageFontObjectPool.LoadData();
    }

    private void LoadDrawData()
    {
        if (_isLoadDrawData == true)
            return;

        _drawDataRefernece.LoadAssetAsync<DamageFontDrawData>().Completed += (result) =>
        {
            var copy = result.Result;
            if (copy == null)
            {
                Debug.Log("Not Have Result");
                GameDataMgr.IsLoadFailed = true;
                return;
            }

            _dataAsset = copy;
            _isLoadDrawData = true;
        };
    }

    public DamageFontObject DrawDamageFont(int damage, bool isCritical, Vector2 startPos)
    {
        var materialType = isCritical == true ? DamageFontDrawData.DamageFontType.Critical : DamageFontDrawData.DamageFontType.Normal;
        var materialData = _dataAsset?.GetMaterialData(materialType);
        if (materialData == null)
            return null;

        var fontObject = _damageFontObjectPool.GetDamageFontObject();
        if (fontObject == null)
            return null;

        var damageString = damage.ToString();
        var fontCount = damageString.Length;

        var damageSlotList = new List<Image>();
        foreach (var str in damageString)
        {
            var fontSlot = _damageFontSlotPool.GetFontSlot();
            if (fontSlot == null)
                continue;

            int number = 0;
            var successParse = int.TryParse(str.ToString(), out number);
            if (successParse == false)
            {
                number = 0;
                Debug.Log("Not Damage Parse \n" +
                    "damageString : " + damageString + "\n" +
                    "str : " + str.ToString());
            }
            var sprite = materialData.GetDamageNumber(number);
            fontSlot.sprite = sprite;
            fontSlot.SetNativeSize();

            damageSlotList.Add(fontSlot);
        }

        fontObject.DrawEndEvent += ReturnFontObject;
        fontObject.OnInitDamageFont(damageSlotList.ToArray(), materialData.FrontIconData);
        fontObject.OnDraw(startPos, _dataAsset.UpSpeed, _dataAsset.LifeTime);

        _useFontList.Add(fontObject);

        return fontObject;
    }

    private void ReturnFontObject(DamageFontObject fontObject)
    {
        var fontSlotList = fontObject.DetachFont();
        _damageFontSlotPool.ReturnFontSlot(fontSlotList);

        _damageFontObjectPool.ReturnFontObject(fontObject);
    }

    public void ReturnAll()
    {
        for (int i = _useFontList.Count - 1; i >= 0; --i)
        {
            ReturnFontObject(_useFontList[i]);
        }
        _useFontList.Clear();
    }

    public void Clear()
    {
        ReturnAll();

        _damageFontObjectPool.Clear();
        _damageFontSlotPool.Clear();
    }
}
