﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(fileName = "New DamageFontMaterialData", menuName = "Data/DamageFontMaterialData", order = 3)]
public class DamageFontMaterialData : ScriptableObject
{
    [System.Serializable]
    private class DamageNumberDataList : SerializableDictionaryBase<int, Sprite> { }

    [SerializeField]
    private DamageNumberDataList _damageNumberDataList = null;
    public Dictionary<int, Sprite> DamageNumberList => _damageNumberDataList.Clone();

    [System.Serializable]
    public class FrontIcon
    {
        [SerializeField]
        private Sprite _icon = null;
        public Sprite Icon => _icon;

        [SerializeField]
        private Vector2 _offset = Vector2.zero;
        public Vector2 Offset => _offset;
    }

    [SerializeField]
    private FrontIcon _frontIconData = null;
    public FrontIcon FrontIconData => _frontIconData;

    public bool HaveNumber(int number)
    {
        return _damageNumberDataList.ContainsKey(number);
    }

    public Sprite GetDamageNumber(int number)
    {
        if (_damageNumberDataList.ContainsKey(number) == false)
            return null;

        return _damageNumberDataList[number];
    }
}
