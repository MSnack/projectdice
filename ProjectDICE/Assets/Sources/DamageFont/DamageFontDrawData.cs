﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[CreateAssetMenu(fileName = "New DamageFontDrawData", menuName = "Data/DamageFontDrawData", order = 3)]
public class DamageFontDrawData : ScriptableObject
{
    public enum DamageFontType
    {
        Normal,
        Critical,
    }

    [System.Serializable]
    private class DamageFontTypeList : SerializableDictionaryBase<DamageFontType, DamageFontMaterialData> { }

    [SerializeField]
    private DamageFontTypeList _damageFontTypeList = null;
    public Dictionary<DamageFontType, DamageFontMaterialData> DamageFontMaterialLIst => _damageFontTypeList.Clone();

    [SerializeField]
    private float _lifeTime = 0.0f;
    public float LifeTime => _lifeTime;

    [SerializeField]
    private float _upSpeed = 0.0f;
    public float UpSpeed => _upSpeed;

    public DamageFontMaterialData GetMaterialData(DamageFontType type)
    {
        if (_damageFontTypeList.ContainsKey(type) == false)
            return null;

        return _damageFontTypeList[type];
    }

    public bool HasMaterialData(DamageFontType type)
    {
        return _damageFontTypeList.ContainsKey(type);
    }
}
