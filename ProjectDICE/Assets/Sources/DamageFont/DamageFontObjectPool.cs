﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EightWork;

public class DamageFontObjectPool : MonoBehaviour
{
    [System.Serializable]
    private class DamageFontObjectReference : AssetReferenceComponent<DamageFontObject>
    {
        public DamageFontObjectReference(string guid) : base(guid) { }
    }

    [SerializeField]
    private DamageFontObjectReference _objectReference = null;

    [SerializeField, ReadOnly]
    private DamageFontObject _baseAset = null;

    [SerializeField]
    private int _reserveCount = 5;

    [SerializeField, ReadOnly]
    private bool _isLoad = false;
    public bool IsLoad => _isLoad;

    private Queue<DamageFontObject> _fontQueue = new Queue<DamageFontObject>();

    public void LoadData()
    {
        if (_isLoad == true)
            return;

        _objectReference.LoadAssetAsync<GameObject>().Completed += (result) =>
        {
            var copy = result.Result;
            if (copy == null)
            {
                Debug.Log("Not Hvae Result");
                return;
            }

            _baseAset = copy.GetComponent<DamageFontObject>();
            _isLoad = true;
        };
    }

    public DamageFontObject GetDamageFontObject()
    {
        if (_fontQueue.Count <= 0)
        {
            if (ReserveQueue(_reserveCount) == false)
                return null;
        }

        var fontObject = _fontQueue.Dequeue();
        fontObject.gameObject.SetActive(true);

        return fontObject;
    }

    private bool ReserveQueue(int reserveCount)
    {
        for (int i = 0; i < reserveCount; ++i)
        {
            var copy = Instantiate(_baseAset, transform);
            if (copy == null)
                return false;

            copy.transform.position = Vector3.zero;
            copy.gameObject.SetActive(false);
            _fontQueue.Enqueue(copy);
        }

        return true;
    }

    public void ReturnFontObject(DamageFontObject fontObject)
    {
        fontObject.Release();
        fontObject.gameObject.SetActive(false);

        _fontQueue.Enqueue(fontObject);
    }

    public void Clear()
    {
        var list = _fontQueue.ToArray();
        for (int i = list.Length - 1; i >= 0; --i)
        {
            Destroy(list[i].gameObject);
        }
        _fontQueue.Clear();
    }
}
