﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageFontSlotPool : MonoBehaviour
{
    [SerializeField]
    private int _reserveCount = 5;

    private Queue<Image> _fontSlotQueue = new Queue<Image>();

    private bool ReserveFontSlotQueue()
    {
        for (int i = 0; i < _reserveCount; ++i)
        {
            var numberSlot = new GameObject("NumberSlot");
            var numberImage = numberSlot.AddComponent<Image>();

            numberSlot.transform.SetParent(transform);
            numberSlot.SetActive(false);
            _fontSlotQueue.Enqueue(numberImage);
        }

        return true;
    }

    public Image GetFontSlot()
    {
        if (_fontSlotQueue.Count <= 0)
        {
            if (ReserveFontSlotQueue() == false)
                return null;
        }

        var fontSlot = _fontSlotQueue.Dequeue();
        fontSlot.gameObject.SetActive(true);
        return fontSlot;
    }

    public void ReturnFontSlot(Image fontSlot)
    {
        fontSlot.sprite = null;
        fontSlot.transform.SetParent(transform);
        _fontSlotQueue.Enqueue(fontSlot);
        fontSlot.gameObject.SetActive(false);
    }

    public void ReturnFontSlot(Image[] fontSlotList)
    {
        foreach (var fontSlot in fontSlotList)
        {
            ReturnFontSlot(fontSlot);
        }
    }

    public void Clear()
    {
        var list = _fontSlotQueue.ToArray();
        for (int i = list.Length - 1; i >= 0; --i)
        {
            Destroy(list[i].gameObject);
        }
        _fontSlotQueue.Clear();
    }
}
