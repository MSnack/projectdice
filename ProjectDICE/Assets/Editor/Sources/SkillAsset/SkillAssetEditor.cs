﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditorInternal;
using UnityEditor;

[CustomEditor(typeof(SkillAsset), true)]
public class SkillAssetEditor : Editor
{
    private ReorderableList list;

    private SerializedProperty _targetTypeElement = null;

    private void OnEnable()
    {
        _targetTypeElement = serializedObject.FindProperty("_skillTargetType");

        list = new ReorderableList(serializedObject, serializedObject.FindProperty("_skillFrames"),
            true, true, true, true);

        list.elementHeightCallback = GetConversationHeight;

        list.drawElementCallback = DrawElementCallback;
        list.onAddDropdownCallback = OnAddDropdownCallback;
    }

    private void OnDisable()
    {
        _targetTypeElement = null;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        //EditorGUILayout.PropertyField(_targetTypeElement);

        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }

    private void DrawElementCallback(Rect rect, int index, bool active, bool focused)
    {
        var element = list.serializedProperty.GetArrayElementAtIndex(index);
        //element.isExpanded = focused;
        //rect.y += 2;

        var split = element.managedReferenceFullTypename.Split(' ');
        EditorGUI.PropertyField(
            new Rect(rect.x + 16, rect.y, rect.width - 16, EditorGUIUtility.singleLineHeight),
            element, new GUIContent(split[split.Length - 1]), true);
    }

    private void OnAddDropdownCallback(Rect buttonRect, ReorderableList list)
    {
        var menu = new GenericMenu();

        var typeList = SkillFrame.GetUseTargetTypeList();
        foreach (var type in typeList)
        {
            menu.AddItem(new GUIContent(type.ToString()), false, AddSkillFrame, type);
        }
        menu.DropDown(buttonRect);

        menu.ShowAsContext();
    }

    private void AddSkillFrame(object target)
    {
        int index = list.serializedProperty.arraySize;

        SkillActionType type = (SkillActionType)target;
        var frame = SkillFrame.GetFrame(type);
        if (frame == null)
            return;

        list.serializedProperty.arraySize++;
        list.index = index;
        var element = list.serializedProperty.GetArrayElementAtIndex(index);
        element.managedReferenceValue = frame;
        serializedObject.ApplyModifiedProperties();
    }

    private float GetConversationHeight(int index)
    {
        var element = list.serializedProperty.GetArrayElementAtIndex(index);
        var elementHeight = EditorGUI.GetPropertyHeight(element);

        var margin = EditorGUIUtility.standardVerticalSpacing;
        return elementHeight + margin;
    }
}
