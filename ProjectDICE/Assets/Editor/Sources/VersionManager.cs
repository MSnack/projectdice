﻿using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

[InitializeOnLoad]
public class VersionManager
{
    private static bool _autoIncrease = true;
    private const string _autoIncreaseMenuName = "BuildUtil/Auto Increase Build Version";
    
    static VersionManager()
    {
        _autoIncrease = EditorPrefs.GetBool(_autoIncreaseMenuName, true);
    }

    /////////////////////// Settings ///////////////////////

    [MenuItem(_autoIncreaseMenuName, false, 1)]
    private static void SetAutoIncrease()
    {
        _autoIncrease = !_autoIncrease;
        EditorPrefs.SetBool(_autoIncreaseMenuName, _autoIncrease);
        Debug.Log("Auto Increase : " + _autoIncrease);
    }

    [MenuItem(_autoIncreaseMenuName, true)]
    private static bool SetAutoIncreaseValidate()
    {
        Menu.SetChecked(_autoIncreaseMenuName, _autoIncrease);
        return true;
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    [MenuItem("BuildUtil/Check Current Version", false, 2)]
    private static void CheckCurrentVersion()
    {
        Debug.Log("Build Ver " + PlayerSettings.bundleVersion + 
            " (" + PlayerSettings.Android.bundleVersionCode + ")");
    }

    [PostProcessBuild(1)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
        if (_autoIncrease) IncreaseBuild();
    }

    /////////////////////// Increase ///////////////////////

    [MenuItem("BuildUtil/Increase Major Version", false, 51)]
    public static void IncreaseMajor()
    {
        string[] lines = PlayerSettings.bundleVersion.Split('.');
        EditVersion(1, -int.Parse(lines[1]), -int.Parse(lines[2]));
    }

    [MenuItem("BuildUtil/Increase Minor Version", false, 52)]
    public static void IncreaseMinor()
    {
        string[] lines = PlayerSettings.bundleVersion.Split('.');
        EditVersion(0, 1, -int.Parse(lines[2]));
    }
  
    public static void IncreaseBuild()
    {
        EditVersion(0, 0, 1);
    }
    
    static void EditVersion(int majorIncr, int minorIncr, int buildIncr)
    {
        string[] lines = PlayerSettings.bundleVersion.Split('.');

        int majorVersion = int.Parse(lines[0]) + majorIncr;
        int minorVersion = int.Parse(lines[1]) + minorIncr;
        int build = int.Parse(lines[2]) + buildIncr;

        PlayerSettings.bundleVersion = majorVersion.ToString("0") + "." +
                                       minorVersion.ToString("0") + "." +
                                       build.ToString("0");
        CheckCurrentVersion();
    }
}