﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class SkillEffectParser : EditorWindow
{
    private static SkillEffectParser _instance = null;
    public static SkillEffectParser Instance
    {
        get
        {
            if (_instance == null)
                _instance = CreateInstance<SkillEffectParser>();

            return _instance;
        }
    }

    private static string EffectPath = null;
    private static string EffectPathAtAssets
    {
        get
        {
            if (EffectPath == null)
                return null;

            var split = EffectPath.Split('/');
            int startIndex = -1;
            for (int i = 0; i < split.Length; ++i)
            {
                if (split[i] == "Assets")
                {
                    startIndex = i;
                    break;
                }
            }

            if (startIndex == -1)
                return null;

            string path = null;
            for (int i = startIndex; i < split.Length; ++i)
            {
                path += split[i] += '/';
            }

            return path;
        }
    }
    private static string EffectPathAtResources
    {
        get
        {
            if (EffectPath == null)
                return null;

            var split = EffectPath.Split('/');
            int startIndex = -1;
            for (int i = 0; i < split.Length; ++i)
            {
                if (split[i] == "Resources")
                {
                    startIndex = i+1;
                    break;
                }
            }

            if (startIndex == -1)
                return null;

            string path = null;
            for (int i = startIndex; i < split.Length; ++i)
            {
                path += split[i] += '/';
            }

            return path;
        }
    }

    private static string SaveDataPath = null;
    private static string SaveDataPathAtAssets
    {
        get
        {
            if (SaveDataPath == null)
                return null;

            var split = SaveDataPath.Split('/');
            int startIndex = -1;
            for (int i = 0; i < split.Length; ++i)
            {
                if (split[i] == "Assets")
                {
                    startIndex = i;
                    break;
                }
            }

            if (startIndex == -1)
                return null;

            string path = null;
            for (int i = startIndex; i < split.Length; ++i)
            {
                path += split[i] += '/';
            }

            return path;
        }
    }
    private static string SaveDataPathAtResources
    {
        get
        {
            if (SaveDataPath == null)
                return null;

            var split = SaveDataPath.Split('/');
            int startIndex = -1;
            for (int i = 0; i < split.Length; ++i)
            {
                if (split[i] == "Resources")
                {
                    startIndex = i+1;
                    break;
                }
            }

            if (startIndex == -1)
                return null;

            string path = null;
            for (int i = startIndex; i < split.Length; ++i)
            {
                path += split[i] += '/';
            }

            return path;
        }
    }

    [MenuItem("Tools/SkillEffectParser")]
    private static void OpenWindow()
    {
        Instance?.ShowUtility();
    }

    private void OnGUI()
    {
        EditorGUILayout.LabelField("EffectFolder : ", EffectPath);
        EditorGUILayout.LabelField("SaveDataFolder : ", SaveDataPath);

        if (GUILayout.Button("Set Effect Folder") == true)
        {
            EffectPath = EditorUtility.OpenFolderPanel("EffectFolder", EffectPath, "");
        }

        if(GUILayout.Button("Set SaveData Folder") == true)
        {
            SaveDataPath = EditorUtility.OpenFolderPanel("SaveDataFolder", SaveDataPath, "");
        }

        if (GUILayout.Button("Parse") == true)
        {
            if (EffectPath == null ||
                SaveDataPath == null)
                return;

            OnParse();
        }

        Repaint();
    }

    private void OnParse()
    {
        string effectPath = EffectPathAtAssets;
        effectPath = effectPath.Remove(effectPath.Length - 1, 1);
        string effectGUID = AssetDatabase.AssetPathToGUID(effectPath);
        string[] effectsGUID = AssetDatabase.FindAssets("t:GameObject", new[] { effectPath });

        int index = 0;
        foreach (var assetGUID in effectsGUID)
        {
            string effectAssetPath = AssetDatabase.GUIDToAssetPath(assetGUID);
            GameEffectObject asset = AssetDatabase.LoadAssetAtPath<GameEffectObject>(effectAssetPath);
            string path = SaveDataPathAtAssets + asset.name + ".asset";
            if (File.Exists(path) == true)
                continue;

            var effectData = CreateInstance<GameEffectData>();
            SerializedObject so = new SerializedObject(effectData);
            var effect = so.FindProperty("_gameEffect");
            effect.objectReferenceValue = asset;

            var dataId = so.FindProperty("_dataId");
            dataId.intValue = index + 1;

            so.ApplyModifiedProperties();

            AssetDatabase.CreateAsset(effectData, path);
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();

            if (EditorUtility.DisplayCancelableProgressBar("Create StageData",
                "Create For " + asset.name, (float)++index / (float)(effectsGUID.Length)) == true)
                break;
        }
        EditorUtility.ClearProgressBar();
    }
}
