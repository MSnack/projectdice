﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public class MapDataGenerator : EditorWindow
{

    private TextAsset _src;

    private GameObject _userTile = null;
    private GameObject _monsterTile = null;
    private GameObject _spawnTile = null;
    private GameObject _goalTile = null;
    private DefaultAsset _folder = null;

    private List<TileClass> _tileMapClasses = null;

    private Vector2 _mapSizeDelta;
    private Vector2 _spaceSize = Vector2.zero;

    private Sprite _previewTile;
    
    [MenuItem("Tools/Map Data Generator")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(MapDataGenerator));
    }

    private void LoadMapFile()
    {
        if (_src == null) return;

        _tileMapClasses = new List<TileClass>();
        string str = _src.text;

        var strLine = str.Split('\n');

        int index = 0;
        int height = 0;
        for (int y = 0; y < strLine.Length; y++)
        {
            string line = strLine[y];
            line = line.Trim();
            if (String.IsNullOrEmpty(line) || String.IsNullOrWhiteSpace(line)) continue;

            height++;
            for (int x = 0; x < line.Length; x++, index++)
            {
                string c = line[x].ToString();
                if (String.IsNullOrEmpty(c) || String.IsNullOrWhiteSpace(c)) continue;
                
                _tileMapClasses.Add(
                    new TileClass() {
                    PosID = new Vector2(x, y), ID = index, Type = Convert.ToInt32(c)
                });
            }
        }

        _mapSizeDelta = new Vector2(_tileMapClasses.Count / height, height);
    }

    private void Generate()
    {
        _tileMapClasses = _tileMapClasses.OrderBy((x) => x.SortValueID).ToList();
        var spawnTile = _tileMapClasses.Find((x) => x.Type == 3);

        GameObject root = new GameObject(_src.name);
        List<int> monsterPath = new List<int>();

        SearchMonsterPath(monsterPath, spawnTile);

        List<UserTile> userTiles = new List<UserTile>();
        List<MonsterTile> monsterTiles = new List<MonsterTile>();

        GameObject userTilesObject = new GameObject("UserTiles");
        userTilesObject.transform.SetParent(root.transform);
        GameObject monsterTilesObject = new GameObject("MonsterTiles");
        monsterTilesObject.transform.SetParent(root.transform);
        
        foreach (var tile in _tileMapClasses)
        {
            GameObject tilePrefab = null;
            Transform parent = userTilesObject.transform;
            switch (tile.Type)
            {
                case 1: tilePrefab = _userTile; break;
                case 2: tilePrefab = _monsterTile; parent = monsterTilesObject.transform; break;
                case 3: tilePrefab = _spawnTile; parent = monsterTilesObject.transform; break;
                case 4: tilePrefab = _goalTile; parent = monsterTilesObject.transform; break;
                default: break;
            }
            
            if (tilePrefab == null)
                continue;
            
            GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(tilePrefab);
            obj.transform.SetParent(parent);
            obj.name = "Tile" + tile.ID;
            
            RectTransform trans = (RectTransform) obj.transform;
            var size = trans.sizeDelta;
            float hw = size.x / 2;
            float hh = -size.y / 2;
            
            float x = size.x * tile.PosID.x + _spaceSize.x * tile.PosID.x + hw * (tile.PosID.y % 2);
            float y = hh * tile.PosID.y + _spaceSize.y * tile.PosID.y;
            obj.transform.localPosition = new Vector3(x, y, y/100);
            var tileObjectBase = obj.GetComponent<TileObjectBase>();
            tileObjectBase?.SetData(tile.PosID, tile.ID);
            
            if(tile.Type == 1) userTiles.Add((UserTile) tileObjectBase);
            if(tile.Type >= 2) monsterTiles.Add((MonsterTile) tileObjectBase);
        }
        
        var monsterTilePool = monsterTilesObject.AddComponent<MonsterTilePool>();
        monsterTiles = monsterTiles.OrderBy((x) => x.ID).ToList();
        monsterTilePool.SetPool(monsterTiles);
        List<MonsterTile> monsterTilePath = new List<MonsterTile>();
        // monsterPathMgr.SetTilePath();
        foreach (var index in monsterPath)
        {
            var tile = monsterTiles.Find((x) => x.ID == index);
            if (tile == null)
            {
                Debug.Log("Generating Tile Failed.");
                continue;
            }
            
            monsterTilePath.Add(tile);
        }

        monsterTilePath.Reverse();
        monsterTilePool.SetTilePath(monsterTilePath);

        var userTilePool = userTilesObject.AddComponent<UserTilePool>();
        userTiles = userTiles.OrderBy((x) => x.ID).ToList();
        userTilePool.SetPool(userTiles);

        string savePath = AssetDatabase.GetAssetPath(_folder) + "/" + _src.name + ".prefab";
        savePath = AssetDatabase.GenerateUniqueAssetPath(savePath);

        PrefabUtility.SaveAsPrefabAssetAndConnect(root, savePath, InteractionMode.UserAction);
    }

    private bool SearchMonsterPath(List<int> path, TileClass tile, TileClass prevTile = null)
    {
        if (tile == null) return false;
        if (path.FindAll(tileId => tileId == tile.ID).Count > 0) return false;

        if (tile.Type < 2) return false;
        if (tile.Type == 4)
        {
            if (prevTile != null && prevTile.Type == 3) return false;
            
            path.Clear();
            return AddPathID(path, tile.ID);
        }

        int x = (int) tile.PosID.x;
        int y = (int) tile.PosID.y;
        
        AddPathID(path, tile.ID);

        int renewX = x + ((y % 2 == 0) ? -1 : 0);

        if (SearchMonsterPath(path, GetTile(x + 1, y), tile)) return AddPathID(path, tile.ID);
        if (SearchMonsterPath(path, GetTile(x - 1, y), tile)) return AddPathID(path, tile.ID);
        if (SearchMonsterPath(path, GetTile(renewX, y + 1), tile)) return AddPathID(path, tile.ID);
        if (SearchMonsterPath(path, GetTile(renewX + 1, y + 1), tile)) return AddPathID(path, tile.ID);
        if (SearchMonsterPath(path, GetTile(renewX, y - 1), tile)) return AddPathID(path, tile.ID);
        if (SearchMonsterPath(path, GetTile(renewX + 1, y - 1), tile)) return AddPathID(path, tile.ID);

        return false;
    }

    private bool AddPathID(List<int> path, int id)
    {
        path.Add(id);
        return true;
    }

    private TileClass GetTile(int x, int y)
    {
        foreach (var tile in _tileMapClasses)
        {
            int sx = (int) tile.PosID.x;
            int sy = (int) tile.PosID.y;
            if (sx == x && sy == y) return tile;
        }

        return null;
    }

    private void DrawPreview(int y)
    {
        if (_tileMapClasses == null) return;
        
        float scale = 0.3f;
        Vector2 renewSpace = _spaceSize * scale;
        
        List<Rect> rects = new List<Rect>();
            
        Rect previewRect = _previewTile.rect;
        previewRect.x = 30;
        previewRect.y = y;
        previewRect.width *= scale;
        previewRect.height *= scale;

        int tileCount = 3;
        int drawWidth = 2;
        int drawHeight = 2;

        // for (int indexY = 0, count = 0; indexY < drawHeight && count < tileCount; indexY++)
        // {
        //     int tileY = drawHeight - indexY - 1;
        //     for (int indexX = 0; indexX < drawWidth && count < tileCount; indexX++, count++)
        //     {
        //         int tileX = drawWidth - indexX - 1;
        //         float hw = previewRect.width / 2;
        //         float hh = previewRect.height / 2;
        //         float hhh = -previewRect.height / 4;
        //         
        //         Rect r = new Rect(previewRect);
        //         r.x += hw * indexX + (hw * indexY) + renewSpace.x * indexX;
        //         r.y += hhh * indexX + (-hhh * indexY) + renewSpace.y * indexY;
        //         
        //         rects.Add(r);
        //     }
        // }

        foreach (var tile in _tileMapClasses)
        {
            if(tile.Type == 0) continue;
            
            Rect r = new Rect(previewRect);
            float hw = previewRect.width / 2;
            float hh = previewRect.height / 2;
            r.x += previewRect.width * tile.PosID.x + renewSpace.x * tile.PosID.x + hw * (tile.PosID.y % 2);
            r.y += hh * tile.PosID.y + renewSpace.y * tile.PosID.y;
            rects.Add(r);

            tile.SortValueID = r.y;
        }

        rects = rects.OrderBy((x) => x.y).ToList();
        
        Color guiColor = GUI.color; // Save the current GUI color
        GUI.color = Color.clear; // This does the magic
        
        foreach (var rect in rects)
        {
            EditorGUI.DrawTextureTransparent(rect, _previewTile.texture);
        }
            
        GUI.color = guiColor; // Get back to previous GUI color
    }

    private void OnGUI()
    {
        GUILayout.Label("Map Data File", EditorStyles.boldLabel);
        _src = (TextAsset) EditorGUILayout.ObjectField(_src, typeof(TextAsset), true);
        
        GUILayout.Space(20);
        GUILayout.Label("Options", EditorStyles.boldLabel);

        _spaceSize = EditorGUILayout.Vector2Field("Space", _spaceSize);
        _folder = (DefaultAsset) EditorGUILayout.ObjectField("Save Folder Path", _folder, typeof(DefaultAsset), true);
        
        GUILayout.Space(20);
        GUILayout.Label("Tile Prefabs", EditorStyles.boldLabel);

        _userTile = (GameObject) EditorGUILayout.ObjectField("User Tile", _userTile, typeof(GameObject), true);
        _monsterTile = (GameObject) EditorGUILayout.ObjectField("Monster Tile", _monsterTile, typeof(GameObject), true);
        _spawnTile = (GameObject) EditorGUILayout.ObjectField("Spawn Tile", _spawnTile, typeof(GameObject), true);
        _goalTile = (GameObject) EditorGUILayout.ObjectField("Goal Tile", _goalTile, typeof(GameObject), true);
        
        GUILayout.Space(20);
        GUILayout.Label("Preview", EditorStyles.boldLabel);

        _previewTile = (Sprite) EditorGUILayout.ObjectField(
            "preview tile texture", _previewTile, typeof(Sprite), true);

        GUILayout.Space(500);

        if(_previewTile != null)
            DrawPreview(400);

        if (_src != null && _previewTile != null)
        {
            if (GUILayout.Button("Load Map Text File"))
                LoadMapFile();
        }
        else
        {
            GUILayout.Button("[Error] Need to Setting map data and preview texture!");
        }
        

        if (_src != null && _tileMapClasses != null && _folder != null && _tileMapClasses.Count > 0)
        {
            if (GUILayout.Button("Generate Prefab"))
                Generate();
        }
        else if(_tileMapClasses != null)
        {
            GUILayout.Button("[Error] Need to Setting Options");
        }
        
        
    }

    public class TileClass
    {
        public Vector2 PosID;
        public int ID;
        public int Type;

        public float SortValueID;
    }
}
