﻿using System.Collections;
using System.Collections.Generic;
using EightWork.Core;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace EightWork.Editor
{
    [CustomEditor(typeof(EightSetting))]
    public class EightSettingEditor : UnityEditor.Editor
    {
        private ReorderableList list;
	
        private void OnEnable() {
            list = new ReorderableList(serializedObject, 
                serializedObject.FindProperty("_coreList"), 
                true, true, true, true);
        }
	
        public override void OnInspectorGUI() {
            serializedObject.Update();
            list.DoLayoutList();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_corePath"), new GUIContent("Path"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_useSetting"), new GUIContent("UseSetting"));
            list.drawElementCallback = DrawElementCallback;
            serializedObject.ApplyModifiedProperties();
        }

        private void DrawElementCallback(Rect rect, int index, bool active, bool focused)
        {
            var element = list.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
                element, new GUIContent("Property " + index));
        }
    }
}

