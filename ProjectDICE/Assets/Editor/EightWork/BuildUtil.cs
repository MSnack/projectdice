﻿using EightWork;
using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.Build.Reporting;

namespace EightWork.Editor
{
    [Serializable]
    public class BuildSetting
    {
        public string CompanyName = null;
        public string ProductName = null;
        public string ApplicationIdentifier = null;

        public string KeyStoreName = null;
        public string KeyStorePassword = null;

        public string KeyAliasName = null;
        public string KyeAliasPassword = null;

        public string AndroidSDKFolderPath = null;
        public string ApkExportPath = null;

        public string[] Scenes = null;

        public BuildOptions BuildOption = (int)BuildOptions.None;
        public AndroidBuildSystem BuildSystemOption = AndroidBuildSystem.Gradle;
    }

    public static class AndroidSDKFolder
    {
        public static string Path
        {
            get { return EditorPrefs.GetString("AndroidSdkRoot"); }
            set { EditorPrefs.SetString("AndroidSdkRoot", value); }
        }
    }

    public class BuildUtil
    {
        private static BuildSetting _currentBuildSetting = null;

        private static string _currentApkExportPath = "";
        /// <summary>
        /// 기본 빌드 설정 파일 출력
        /// </summary>
        [MenuItem("BuildUtil/ExportBuildSetting")]
        static void ExportNewBuildSetting()
        {
            BuildSetting newBuildSettings = new BuildSetting();

            ExportBuildSetting(newBuildSettings);
        }

        [MenuItem("BuildUtil/ExprotCurrentBuildSetting")]
        static void ExportCurrentBuildSetting()
        {
            BuildSetting buildSetting = new BuildSetting();

            buildSetting.CompanyName = PlayerSettings.companyName;
            buildSetting.ProductName = PlayerSettings.productName;

            buildSetting.ApplicationIdentifier = PlayerSettings.applicationIdentifier;

            buildSetting.KeyStoreName = PlayerSettings.Android.keystoreName.Replace(EightPath.UnityProjetPath, "");
            buildSetting.KeyStorePassword = PlayerSettings.Android.keystorePass;

            buildSetting.KeyAliasName = PlayerSettings.Android.keyaliasName;
            buildSetting.KyeAliasPassword = PlayerSettings.Android.keyaliasPass;

            buildSetting.AndroidSDKFolderPath = AndroidSDKFolder.Path;

            buildSetting.BuildSystemOption = EditorUserBuildSettings.androidBuildSystem;

            _currentBuildSetting = buildSetting;
            ExportBuildSetting(buildSetting);
        }

        static void ExportBuildSetting(BuildSetting buildSetting)
        {
            var exportFile = JsonUtility.ToJson(buildSetting);

            File.WriteAllText(EightPath.EditorBuildSettingFile, exportFile);
        }

        class BuildVersion
        {
            public int version = 0;
            public string gameVersion = "0.0.0";
        }

        static int GetBundleVersionCode()
        {
            FileInfo file = new System.IO.FileInfo(EightPath.VersionDataFilePath);
            if (file.Exists == false)
            {
                if (Directory.Exists(EightPath.VersionDataFolderPath) == false)
                    Directory.CreateDirectory(EightPath.VersionDataFolderPath);

                BuildVersion data = new BuildVersion();
                File.WriteAllText(EightPath.VersionDataFilePath, JsonUtility.ToJson(data).ToString());
            }

            var jsonData = File.ReadAllText(EightPath.VersionDataFilePath);
            var buildVersionData = JsonUtility.FromJson<BuildVersion>(jsonData);

            int version = buildVersionData.version;
            PlayerSettings.bundleVersion = buildVersionData.gameVersion;
            buildVersionData.version += 1;
            VersionManager.IncreaseBuild();
            buildVersionData.gameVersion = PlayerSettings.bundleVersion;

            File.WriteAllText(EightPath.VersionDataFilePath, JsonUtility.ToJson(buildVersionData).ToString());

            return version;
        }

        [MenuItem("BuildUtil/ImportBuildSetting")]
        static void ImportBuildSetting()
        {
            string buildJson = File.ReadAllText(EightPath.EditorBuildSettingFile);
            BuildSetting buildSetting = JsonUtility.FromJson<BuildSetting>(buildJson);

            PlayerSettings.companyName = buildSetting.CompanyName;
            PlayerSettings.productName = buildSetting.ProductName;

            PlayerSettings.applicationIdentifier = buildSetting.ApplicationIdentifier;

            PlayerSettings.Android.keystoreName = EightPath.UnityProjetPath + buildSetting.KeyStoreName;
            PlayerSettings.Android.keystorePass = buildSetting.KeyStorePassword;

            PlayerSettings.Android.keyaliasName = buildSetting.KeyAliasName;
            PlayerSettings.Android.keyaliasPass = buildSetting.KyeAliasPassword;

            PlayerSettings.Android.bundleVersionCode = GetBundleVersionCode();

            AndroidSDKFolder.Path = buildSetting.AndroidSDKFolderPath;
            _currentApkExportPath = buildSetting.ApkExportPath;

            EditorUserBuildSettings.androidBuildSystem = buildSetting.BuildSystemOption;
            _currentBuildSetting = buildSetting;
        }

        [MenuItem("BuildUtil/BuildAPK")]
        static void BuildAPK()
        {
            //유닛 테스트 실패 시 빌드 금지
            // if (!IsUnitTestsPassed()) return;

            ImportBuildSetting();
            BuildAllAssetBundles();

            if (_currentBuildSetting == null)
                return;

            DirectoryInfo directoryInfo = new DirectoryInfo(_currentApkExportPath);
            if (directoryInfo.Exists)
            {
                try
                {
                    Directory.Delete(_currentApkExportPath, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Debug.Log("^^7");
                }
                directoryInfo.Create();
            }
            else
            {
                directoryInfo.Create();
            }

            string currentTimeText = System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

            string exportBuildLogPath = string.Format("{0}/{1}_Log.txt", _currentApkExportPath, PlayerSettings.productName);
            string exportBuildApkPath = string.Format("{0}/{1}.apk", _currentApkExportPath, "release");
            string exportBuildObbPath = string.Format("{0}/{1}.obb", _currentApkExportPath, "release.main");
            string changeObbFile = string.Format("{0}/{1}.{2}.{3}.obb", _currentApkExportPath, "main", PlayerSettings.Android.bundleVersionCode, "com.eightstduio.projectrd");

            string buildStartLog = "\n Build APK Start : " + currentTimeText;
            File.AppendAllText(exportBuildLogPath, buildStartLog);

            Debug.Log("Current Game Version : Ver" + Application.version);
            BuildReport errorMsg = BuildPipeline.BuildPlayer(_currentBuildSetting.Scenes, exportBuildApkPath, BuildTarget.Android, _currentBuildSetting.BuildOption);

            string buildEndLog = string.Format("\n Build APK End : [{0}] {1}", errorMsg, currentTimeText);
            File.AppendAllText(exportBuildLogPath, buildEndLog);

            FileInfo fileRename = new FileInfo(@exportBuildObbPath);
            if (fileRename.Exists)
            {
                fileRename.MoveTo(@changeObbFile); //이미있으면 에러
            }

            // 독립적으로 실행됨
            // //aws에 Assetbundle 업로드
            // UploadAssetBundles();

        }

        //유닛 테스트 성공 여부 확인
        static bool IsUnitTestsPassed()
        {
            bool isPassed = false;

            StreamReader sr = new StreamReader(Directory.GetCurrentDirectory() + "\\..\\TestLogs\\unity_unittests_results.xml");
            bool isXML = false;

            while (sr.Peek() >= 0)
            {
                string str = sr.ReadLine();
                if (str == null) break;

                if (str.StartsWith("<?xml"))
                {
                    isXML = true;
                    continue;
                }

                if (isXML && str.StartsWith("<test-run id="))
                {
                    isPassed = str.Contains("result=\"Passed\"");
                }

                if (isPassed) break;
            }

            sr.Close();

            return isPassed;

        }

        [MenuItem("BuildUtil/UploadAssetBundles")]
        static void UploadAssetBundles()
        {
            string assetBundleFolderPath = $"{Directory.GetCurrentDirectory()}\\ServerData\\Android";

            string commandStr = string.Format("aws s3 sync \"{0}\" s3://projectrd/master/Android --exclude \"*.meta\" --acl public-read  --quiet", assetBundleFolderPath, EightPath.VersionOfAWSFolder);

            Debug.Log("Synchronizing Assetbundles for s3 server.\n(src = " + assetBundleFolderPath + ")\n(dst = s3://lucidhearts/AssetBundle/" + EightPath.VersionOfAWSFolder + ")");

            System.Diagnostics.Process cmd = new System.Diagnostics.Process();
            cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();

            cmd.StandardInput.WriteLine(commandStr);
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();

            Debug.Log("Synchronized.");
            //Console.WriteLine(cmd.StandardOutput.ReadToEnd());
        }

        // [MenuItem("BuildUtil/UpdatedDBAssetBundles")]
        // static void UpdatedDBAssetBundles()
        // {
        //     CreateBundleCRCListCSV.CreateCRCList();
        //     var jsonData = new LitJson.JsonData();
        //     var jsonBundleDataList = new LitJson.JsonData();
        //
        //     string crcPath = Application.dataPath + "/AssetBundles/CRCData.csv";
        //     if (File.Exists(crcPath) == false)
        //     {
        //         Debug.Log("Not have CRCData.csv Files");
        //         return;
        //     }
        //
        //     FileInfo file = new FileInfo(crcPath);
        //     StreamReader sr = new StreamReader(file.OpenRead());
        //     string datas = sr.ReadToEnd();
        //
        //     string[] csvStringArray = Regex.Split(datas, CSVUtil.LineSplitRe);
        //     if (csvStringArray.Length <= 1)
        //         return;
        //
        //     string[] csvNames = Regex.Split(csvStringArray[0], CSVUtil.SplitRe);
        //
        //     for (int i = 1; i < csvStringArray.Length; ++i)
        //     {
        //         if (csvStringArray[i].Length <= 0)
        //             continue;
        //
        //         string[] csvParts = Regex.Split(csvStringArray[i], CSVUtil.SplitRe);
        //         int csvIndex = 0;
        //
        //         var jsonBundleData = new LitJson.JsonData();
        //         jsonBundleData[csvNames[csvIndex]] = StringUtil.ConvertInt(csvParts[csvIndex++]);
        //         jsonBundleData[csvNames[csvIndex]] = csvParts[csvIndex++];
        //         jsonBundleData[csvNames[csvIndex]] = csvParts[csvIndex++];
        //         jsonBundleData[csvNames[csvIndex]] = csvParts[csvIndex++];
        //
        //         jsonBundleDataList.Add(jsonBundleData);
        //     }
        //     
        //     sr.Close();
        //
        //     //var info = LocalBundleInfo.LoadInfo(crcPath);
        //     //foreach (BundleType type in Enum.GetValues(typeof(BundleType)))
        //     //{
        //     //    var jsonBundleData = new LitJson.JsonData();
        //     //    jsonBundleData["id"] = (int)type;
        //     //    jsonBundleData["name"] = type.ToString();
        //     //    jsonBundleData["crc"] = info.GetCRC(type).ToString();
        //     //    jsonBundleData["size"] = info.
        //
        //     //    jsonBundleDataList.Add(jsonBundleData);
        //     //}
        //
        //     //string A = "123456";
        //     //jsonBundleDataList["A"] = A;
        //
        //     jsonData["bundleData"] = jsonBundleDataList;
        //
        //     InstantPostWebServer("https://lucid.eightstudio.net/", 
        //         EightPath.IsDebugBundle ? "bundle/updateTestBundles" : "bundle/updateBundles", jsonData.ToJson());
        // }
        //
        [MenuItem("Assets/Build AssetBundles")]
        static void BuildAllAssetBundles()
        {
            AddressableAssetSettings.BuildPlayerContent();
            UploadAssetBundles();
        }

        // static void InstantPostWebServer(string serverPath, string function, string sendString)
        // {
        //     List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        //     UnityWebRequest unityWebRequest = UnityWebRequest.Post(serverPath + function, formData);
        //     unityWebRequest.SetRequestHeader("Content-Type", "application/json");
        //
        //     var sendData = System.Text.Encoding.UTF8.GetBytes(sendString);
        //     unityWebRequest.uploadHandler = new UploadHandlerRaw(sendData);
        //
        //     unityWebRequest.SendWebRequest();
        // }
    }
}